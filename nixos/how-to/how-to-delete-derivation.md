## 刪除套件的操作

## 以 nix-channel 添加的套件為例

- 以 `nix-channel --add https://github.com/nix-community/home-manager/archive/release-24.05.tar.gz home-manager` 為例

- 執行 `nix-channel --update` 會建立以下的 derivation，/nix/store/4xba12s6ah3dma690lzl13cwi4njgrn8-home-manager-24.05.tar.gz.drv

- 查詢是否被其他derivation依賴，`nix-store --query --referrers /nix/store/4xba12s6ah3dma690lzl13cwi4njgrn8-home-manager-24.05.tar.gz.drv`

- 查詢是否被其他命令依賴，`nix-store --query --roots /nix/store/4xba12s6ah3dma690lzl13cwi4njgrn8-home-manager-24.05.tar.gz.drv`，得到以下

  ```
  /nix/var/nix/profiles/per-user/root/channels-2-link -> /nix/store/3dr8jkfymnq3kdkis67vyhd5ykq3z82x-user-environment
  ```

  根據路徑 root/channels-2-link，可以推測出是由 nix-channel 命令安裝建立的依賴，
  channels-2-link 對應實際的檔案是 /nix/store/3dr8jkfymnq3kdkis67vyhd5ykq3z82x-user-environment，
  
  表示 /nix/store/3dr8jkfymnq3kdkis67vyhd5ykq3z82x-user-environment 的檔案中引用了 
  /nix/store/4xba12s6ah3dma690lzl13cwi4njgrn8-home-manager-24.05.tar.gz.drv

  透過`nix derivation show /nix/store/3dr8jkfymnq3kdkis67vyhd5ykq3z82x-user-environment`可以驗證此結果

  ```nix
  {
    "/nix/store/gmpb4sr642lgj5yj58d9yk1z66yi837k-user-environment.drv": {
      ...
      "env": {
        "allowSubstitutes": "",
        "builder": "builtin:buildenv",
        
        # 引用 /nix/store/4xba12s6ah3dma690lzl13cwi4njgrn8-home-manager-24.05.tar.gz.drv
        "derivations": "true 5 1 /nix/store/bw00afh3z6d4dklw47vkbsgf0nxkxlzk-home-manager-24.05.tar.gz true 5 1 /nix/store/9jlzd3yp3pm79xx37f6a60y7yi41piad-nixos-24.05",
        ...
      },
      ...
    }
  }
  ```

  從上述可以看出 /nix/store/gmpb4sr642lgj5yj58d9yk1z66yi837k-user-environment.drv 引用了
  /nix/store/bw00afh3z6d4dklw47vkbsgf0nxkxlzk-home-manager-24.05.tar.gz 卻不是
  /nix/store/4xba12s6ah3dma690lzl13cwi4njgrn8-home-manager-24.05.tar.gz.drv

  透過`nix derivation show /nix/store/4xba12s6ah3dma690lzl13cwi4njgrn8-home-manager-24.05.tar.gz.drv`可以看出兩者的差異

  ```
  {
    "/nix/store/4xba12s6ah3dma690lzl13cwi4njgrn8-home-manager-24.05.tar.gz.drv": {
      "args": [],
      "builder": "builtin:unpack-channel",
      "env": {
        "builder": "builtin:unpack-channel",
        "channelName": "home-manager",
        "name": "home-manager-24.05.tar.gz",
        "out": "/nix/store/bw00afh3z6d4dklw47vkbsgf0nxkxlzk-home-manager-24.05.tar.gz",
        "preferLocalBuild": "1",
        "src": "/nix/store/d07v47y7c3sbpxz9c7mrvsy7d15gzha1-release-24.05.tar.gz",
        "system": "builtin"
      },
      "inputDrvs": {},
      "inputSrcs": [
        "/nix/store/d07v47y7c3sbpxz9c7mrvsy7d15gzha1-release-24.05.tar.gz"
      ],
      "name": "home-manager-24.05.tar.gz",
      "outputs": {
        "out": {
          "path": "/nix/store/bw00afh3z6d4dklw47vkbsgf0nxkxlzk-home-manager-24.05.tar.gz"
        }
      },
      "system": "builtin"
    }
  }
  ```

  從上述可以得到，
  /nix/store/bw00afh3z6d4dklw47vkbsgf0nxkxlzk-home-manager-24.05.tar.gz 是
  /nix/store/4xba12s6ah3dma690lzl13cwi4njgrn8-home-manager-24.05.tar.gz.drv 的產出物，兩者是同源的

- 因為被依賴，因此不會出現在 gc 的名單中，
  - `nix-store --gc --print-dead | grep 4xba12s6ah3dma690lzl13cwi4njgrn`
  - `nix-store --gc --print-dead | grep bw00afh3z6d4dklw47vkbsgf0nxkxlzk`

- 透過手動的方式移除依賴，注意，此方式只影響channel的引用，而不會自動清理環境
  ```
  nix-channel --remove home-manager
  nix-channel --update
  ```

- 透過 `nix-collect-garbage -d` 完整移除
