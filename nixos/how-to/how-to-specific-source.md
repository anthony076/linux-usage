## how-to-specific-source

- 對於 flake 的命令
  - 語法，`nix shell flake源#套件名`，
    例如，$ nix shell nixpkgs#hello

  - 語法，`nix shell git-repo名/branch名#套件名`，
    例如，$ nix shell nixpkgs/nixos-24.05#python3

  - 語法，`nix shell github:用戶名/git-repo名#套件名`，
  - 例如，$ nix shell github:foo/mypackage

- 對於 flake.nix 中的 inputs屬性

  - 語法，"github:用戶名/repo名/branch名"

    例如，`inputs.nixpkgs.url = "github:Mic92/nixpkgs/master"`

  - 語法，利用索引值指定 nixpkgs 的版本，"github:nixos/nixpkgs/索引值"

    例如， inputs.nixpkgs.url = "github:nixos/nixpkgs/2bb5cbf7f8b99a8d1d6646abe5ab993f6823212f";

    索引值可以透過[nixhub](https://www.nixhub.io/)查詢

  - 語法，github 的 tartball URL (commit)

    例如，`inputs.example.url = "https://github.com/用戶名/repo名/archive/<commit-hash>.tar.gz"`
  
  - 語法，使用 https/ssh 協議的 git-repo
    
    例如，`inputs.mypackage.url = "git+https://git.somehost.tld/user/path?ref=branch"`

  - 語法，使用 ssh 協議的 git-repo
    
    例如，`inputs.mypackage.url = "git+ssh://git@github.com/ryan4yin/nix-secrets.git?shallow=1"`

  - 語法，指定壓縮檔
    
    例如，`inputs.mypackage.url = "https://codeberg.org/solver-orgz/treedome/archive/master.tar.gz"`

  - 語法，github 的 tartball URL (Tag)
    
    例如，`inputs.example.url = "https://github.com/user/repo/archive/refs/heads/main.tar.gz"`
  
  - 語法，指定遠端子目錄

    例如，`inputs.example.url = "github:foo/bar?dir=shu"`

  - 語法，指定本地目錄

    例如，`inputs.example.url = "path:/path/to/repo"`

  - 語法，指定commit版本

    例如，`inputs.example.url = "github:vlaci/nix-doom-emacs?rev=238b18d7b2c8239f676358634bfb32693d3706f3"`

  - 語法，指定非flake源

    ```nix
    inputs.bar = {
      url = "github:foo/bar/branch";
      flake = false;
    }
    ```

  - 語法，使當前flake的inputs.nixpkgs保持一致

    ```nix
    inputs.bar = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    }
    ```

## 獲取 sha245 或 hash 的方法

- 透過 `nix flake metadata <URL>`
  
  ```nix
  root@nixos ~ [1]# nix flake metadata github:nixos/nixpkgs/nixos-24.05

  Resolved URL:  github:nixos/nixpkgs/nixos-24.05
  Locked URL:    github:nixos/nixpkgs/7e1ca67996afd8233d9033edd26e442836cc2ad6
  Description:   A collection of packages for the Nix package manager
  Path:          /nix/store/6m5f91y3vxhn5jlik4gzildl0xrrvpds-source
  Revision:      7e1ca67996afd8233d9033edd26e442836cc2ad6
  Last modified: 2024-12-01 01:25:24
  ```

- 透過 `nix-prefetch-url --unpack <壓縮檔URL>`

  ```nix
  # 取得原始hash值
  $ nix-prefetch-url --unpack https://github.com/casey/just/archive/3ddd1b168308db2b88daac9ea70a3b63629ef034.tar.gz
  path is '/nix/store/m7ri4np8h32pjhas2zn7fmmg0jg9valf-3ddd1b168308db2b88daac9ea70a3b63629ef034.tar.gz'
  1032dnza2i4wzq6jxgmdrpmqa5f0pn9qpzisscx30xkiiwaykl0r

  # 轉換為 SRI 格式
  nix hash to-sri --type sha256 1032dnza2i4wzq6jxgmdrpmqa5f0pn9qpzisscx30xkiiwaykl0r
  # 得到 sha256-GdDpFY9xdjA60zr+i5O9wBWF682tvi4N/pxEob5tYoA=
  ```

## 指定pkgs的版本

- 以 just 為例

  - step，到 nixpks-repo 中，確認
    [套件的版本]https://github.com/NixOS/nixpkgs/blob/master/pkgs/by-name/ju/just/package.nix
  
    在頁面左上角的 Switch branches/tags 中可以看到，
    - `release-24.05 Branch`預設安裝`just-v1.28.0`的版本
    - `nixos-24.05 Branch`預設安裝`just-v1.28.0`的版本
    - `24.05 Tags`預設安裝`just-v1.26.0`的版本

  - step，回到nixpkgs-repo主頁，透過tag
    [導航到24.05的頁面](https://github.com/NixOS/nixpkgs/releases/tag/24.05)，

    在頁面中可以查詢到nixpkgs-24.05對應的commit為`63dacb46bf939521bdc93981b4cbb7ecb58427a0`

  - step，建立 pkgs 實例時，將`<nixpkgs>`替換為fetchTarball，並指定nixpkgs的版本
  
    ```nix
    # shell.nix
    let

      # pkgs = import <nixpkgs> {}

      pkgs2105 = import (
        fetchTarball {
          url = "https://github.com/NixOS/nixpkgs/archive/63dacb46bf939521bdc93981b4cbb7ecb58427a0.tar.gz";
        }
      ) {};

    in
      pkgs2105.mkShell {
        buildInputs = [
          pkgs2105.just  # just v1.x.0
        ];
      }
    ```

  - step，進入shell後，透過 just --version 可以看到使用的是 just v1.26.0 版本

- 以 nix-shell 為例

  > nix-shell -I nixpkgs=https://github.com/NixOS/nixpkgs/archive/refs/tags/23.05.tar.gz -p hello

## 查詢releases.nixos.org中的版本

參考，https://releases.nixos.org/?prefix=nixpkgs/