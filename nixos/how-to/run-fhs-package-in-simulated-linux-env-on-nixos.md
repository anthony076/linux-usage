## run-fhs-package-in-simulated-linux-env-on-nixos

- 為什麼FHS套件無法直接在nixos中使用
  
  遵從FHS規範的傳統linux中使用的套件，會在檔案系統中建立檔案，
  
  NixOS的檔案系統，是不使用FHS的規範的不可變系統(Immutable-System)，大部分的檔案或套件需要透過各種配置文件才能建立或修改，

  因此，當FHS套件對nixos的檔案系統修改時(建立檔案時)就會拋出錯誤

- 幾種執行FHS套件的方法
  - 方法，手工編譯，沒有任何的補丁可使用
    - step，需要找到合適的加載器
    - step，需要透過ldd命令找到需要加載的Library
    - step，若缺少需要的Library，透過 nix-index 搜尋並安裝包含這些Libray的包
    - step，透過 LD_LIBRARY_PATH 將Library的位置傳遞給加載器，由加載器加載Library後，就可以正常運行

  - 方法，使用補丁
    - 使用`patchelf`
    - 使用`patchelf`+nix-derivation
    - 使用`autoPatchElf`
  
  - 方法，產生一個`支援FHS的linux-shell模擬環境`，並透過手工執行

    - 參考，[透過pkgs.buildFHSUserEnv建構FHS環境](#方法-使用pkgsbuildfhsuserenv模擬傳統linux的執行環境為一般套件提供符合fhs規範的檔案結構)
    - 缺，比補丁需要更多資源，應用程式的啟動時間大幅增加

  - 方法，[將(支援FHS的linux-shell模擬環境)和(執行檔)一起打包](https://web.archive.org/web/20240125095859/https://reflexivereflection.com/posts/2015-02-28-deb-installation-nixos.html)
  
  - 方法，使用第三方的
    - [nix-alien](https://github.com/thiagokokada/nix-alien)
    - [nix-autobahn](https://github.com/Lassulus/nix-autobahn)，
      和nix-alien類似，運行並自動下載依賴，需要 find、fzf、nix-index、nix-ld (optional)

    - [steam-run](https://mynixos.com/nixpkgs/package/steam-run)
    - [nix-ld](https://github.com/nix-community/nix-ld)

  - 方法，透過 docker
    - docker
    - [distrobox](https://github.com/89luca89/distrobox)，基於docker的第三方應用，依賴 docker 或 podman

      參考，[distrobox的使用](../home-manerger/sw-config-examples/distrobox.md)

  - 方法，使用 flatpack 或 appimage

    此方法僅適用於GUI應用的安裝，參考，[flatpack的使用](../home-manerger/sw-config-examples/flatpak.md)

## [方法] 使用`pkgs.buildFHSUserEnv`模擬傳統linux的執行環境，為一般套件提供符合FHS規範的檔案結構

- 範例，簡單範例

  ```nix
  
  # 建構，nix build -f fhs.nix 或 nix-build fhs.nix
  
  { pkgs ? import <nixpkgs> {} }:

  pkgs.buildFHSUserEnv {
    name = "simulated-linux-env";
    
    # 設置 fhs 環境中要使用的套件
    targetPkgs = pkgs : [
      pkgs.hello
      pkgs.nom
      pkgs.nvd
    ];

    # 設置預設的shell環境
    #　runScript = "bash";
    runScript = "${pkgs.bash}/bin/bash"; # 設定啟動腳本，默認啟動 bash shell
  }
  ```

  執行，result/bin/fhs
  檔案會被放置在 /usr/bin 的路徑中

  ```shell
  [root@nixos:~/fhs]# hello
  Hello, world!

  [root@nixos:~/fhs]# which hello
  /usr/bin/hello

  [root@nixos:~/fhs]# ls /usr/bin | egrep 'hello|nvd|nom'
  hello
  nom
  nvd
  ```

- 範例，指定套件 + 啟動腳本 + 環境變數設置

  ```nix
  # 建構，$ nix-build codeql-fhs.nix
  # 進入環境，$ result/bin/codeql-fhs

  { pkgs ? import <nixpkgs> {} }:

  pkgs.buildFHSEnv {
    name = "codeql-fhs";

    targetPkgs = pkgs: with pkgs; [
      codeql
      gcc
      gnumake
      glibc
    ];

    runScript = "bash";

    shellHook = ''
      export NIXPKGS_ALLOW_UNFREE=1
    '';
  }
  ```

- 範例，建立FHS環境，並添加到 configuration.nix 中作為套件之一，方便調用

  輸入 fhs 後就可以進入 fhs 的環境

  ```
  { config, pkgs, lib, ... }:

  {
    environment.systemPackages = with pkgs; [
      ... 
      ( let 
          base = pkgs.appimageTools.defaultFhsEnvArgs; 
        in
          pkgs.buildFHSUserEnv (base // {
            name = "fhs";

            targetPkgs = pkgs: 
              # - pkgs.buildFHSUserEnv 只提供一個最小的 FHS 環境，缺少很多常用軟件所必須的基礎包，直接使用可能會報錯
              # - pkgs.appimageTools 提供了大多數程序常用的基礎包，所以我們可以直接用它來補充
              (base.targetPkgs pkgs) ++ (with pkgs; [
                pkg-config
                ncurses
              ]
            );
            
            # 設置環境變數
            profile = "export FHS=1";

            runScript = "bash";

            extraOutputsToInstall = ["dev"];
          })
      )
    ];
  }
  ```

## [方法] 手動建立編譯腳本 (打包)

- 手動編譯的編譯環境 (自定義套件的編譯腳本)

  需要自定義套件的編譯腳本時，nix套件管理器會`在一個隔離的環境中進行軟件打包`，該環境是一個斷網、限制權限，且只允許訪問固定路徑的隔離環境
  
  在編譯過程中，任何`訪問隔離環境外部路徑`或者`連網操作`都會失敗，只能使用 Nix 編譯腳本中事先指定的依賴
  因此，編譯出來的程序完全不會依賴於其它文件

- 打包大型套件時，將 /tmp 設置為非內存碟

  編譯時，預設 nix 會將臨時文件存在 /tmp 目錄下，

  若 /tmp 是內存盤，打包大型軟件容易出現內存不足的狀況，需設置`boot.tmpOnTmpfs = true`

## ref 

- [運行非FHS的二進制文件](https://nixos-and-flakes.thiscute.world/zh/best-practices/run-downloaded-binaries-on-nixos)

- [10種運行非FHS二進制執行檔的方法，Different methods to run a non-nixos executable on Nixos](https://unix.stackexchange.com/questions/522822/different-methods-to-run-a-non-nixos-executable-on-nixos)

- [為套件建立手動編譯腳本(打包) + 多個打包實際範例](https://lantian.pub/article/modify-computer/nixos-packaging.lantian/)