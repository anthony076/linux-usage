## 如何利用模組化組織複雜的配置

- 規則，若`屬性值`是一個屬性集，就可以將屬性值寫入檔案中，透過`import 檔案`的方式匯入屬性值

- 規則，將配置進行分類，分散到不同的nix文件中
  - 依照`主機`進行分類
  - 依照`使用場景`進行分類
  - 依照`功能`進行分類，例如，`shell類`、`桌面類`

- 規則，將需要的所有nix-module放在同一個目錄中，透過 default.nix 可以一次性加載所有需要的模組

  ```
  # flake.nix 
  
  home-manager.nixosModules.home-manager
  {
    ...
    home-manager.users.ryan = import ./home;    # 加載 ./home/default.nix 的內容
  }

  # =================

  # ./home/default

  { config, pkgs, ... }:
  {
    imports = [
      ./fcitx5
      ./i3
      ./programs
      ./rofi
      ./shell
    ];
    ...
  }
  ```

- 規則，將性質相近的nix檔，放在同一個目錄中，透過 default.nix 一次性導入
  - 在`let 區塊`中重新定義新屬性
  - 透過`inherit 語法`取出需要的屬性 
  - 透過`// 語法`重新組合不同的屬性值
  
  - 範例，重組屬性範例，必要屬性 + 自定義屬性

    - [來源](https://github.com/ryan4yin/nix-config/tree/main)
    - `(args // {system = "x86_64-linux";})`，將兩個屬性集合併
    - `import ./x86_64-linux (args // {system = "x86_64-linux";})`，
      將兩個屬性集合併後，作為輸入參數傳遞給 ./x86_64-linux
    
    ```
    # 用於linux
    nixosSystems = {
      x86_64-linux = import ./x86_64-linux (args // {system = "x86_64-linux";});
      # aarch64-linux = import ./aarch64-linux (args // {system = "aarch64-linux";});
      # riscv64-linux = import ./riscv64-linux (args // {system = "riscv64-linux";});
    };

    # 用於macos
    darwinSystems = {
      aarch64-darwin = import ./aarch64-darwin (args // {system = "aarch64-darwin";});
      x86_64-darwin = import ./x86_64-darwin (args // {system = "x86_64-darwin";});
    };

    # 將兩者合併
    allSystems = nixosSystems // darwinSystems;
    ```

- 規則，透過 nix-modules 將配置寫成一鍵開關的形式

  導入 nix-modules 有兩種方式，

  - 方法，透過 imports 導入

    詳見，[anthony076/nix-config](https://gitlab.com/anthony076/nix-config/-/commit/89d0364c78977505454d83b96dbff13e47c702d8)

  - 方法，透過頂層的 flake.nix 導入，要需要的地方透過 inputs 取用，例如，

    在 flake.nix 的 outputs 屬性中自定義 homeManagerModule 屬性，

    ```nix
    # flake.nix

    {
      # ...
      outputs = { nixpkgs, ... }@inputs: {
        homeManagerModules.default = ./homeManagerModules;
      };
    }
    ```

    在 home-manager 的配置文件中取用

    ```nix
    { inputs, ... }: {
      home-manager = {
        extraSpecialArgs = { inherit inputs; };
        users = {
          # "username" = import ./home.nix;
          "username" = {
            imports = [
              ./home.nix
              inputs.self.outputs.homeManagerModules.default
            ];
          };
        };
      };

    }
    ```

    詳見，[Modularize NixOS and Home Manager | Great Practices](https://www.youtube.com/watch?v=vYc6IzKvAJQ) 

- 規則，若遇到配置選項發生重複定義時，可透過以下函數進行控制，
  參考，[屬性值衝突處理函數](../functions/functions-set-value-conflix.md)

## 模組化配置範例

- [home-manager 模組化範例](https://github.com/ahonn/dotfiles/tree/master/modules/home-manager)

- [模塊化nixos配置](https://nixos-and-flakes.thiscute.world/nixos-with-flakes/modularize-the-configuration)
  
- [模組化配置的入門範例 @ Misterio77](https://github.com/Misterio77/nix-starter-configs)
  - 在 flake.nix 中使用 template 產生不同配置的 flake.nix

- [配置範例 @ wimpysworld](https://github.com/wimpysworld/nix-config)
  - 使用 flake(主要) + default.nix(次要) 的方式管理
  - 區分多個使用者、適用不同的主機

- [複雜配置範例 @ ryan4yin](https://github.com/ryan4yin/nix-config)

