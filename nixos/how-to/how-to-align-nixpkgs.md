## how-to-align-nixpkgs.md

- 目的，使`<nixpkgs>`和`nix run nixpkgs#套件名`中的nixpkgs，都指向同一個來源

  - `<nixpkgs>`中的nixpkgs的實際位置，是由$Nixpkgs變數所提供

  - `nix run nixpkgs#套件名`中的nixpkgs的實際位置，是由`https://github.com/NixOS/flake-registry/blob/master/flake-registry.json`

  - 兩者同樣是 nixpkgs，但實際上可能指向不同的位置

- 使`<nixpkgs>`和`nix run nixpkgs#套件名`中的nixpkgs，都指向當前系統的版本

  ```
  {lib, nixpkgs, ...}: {

  # 使 nix run nixpkgs#套件名 中的nixpkgs，使用當前文件傳入的 nixpkgs
  nix.registry.nixpkgs.flake = nixpkgs;
  
  # 禁用 nix-channel，改使用 flake 版本的 nixpkgs
  nix.channel.enable = false; # remove nix-channel related tools & configs, we use flakes instead.

  # but NIX_PATH is still used by many useful tools, so we set it to the same value as the one used by this flake.
  # Make `nix repl '<nixpkgs>'` use the same nixpkgs as the one used by this flake.
  
  # 建立"nixpkgs=/etc/nix/inputs/nixpkgs"的配置文件，該文件記錄當前文件傳入的 nixpkgs
  environment.etc."nix/inputs/nixpkgs".source = "${nixpkgs}";
  
  # 讓$Nixpkgs變數指向該文件
  nix.settings.nix-path = lib.mkForce "nixpkgs=/etc/nix/inputs/nixpkgs";
  }
  ```