## 驗證配置是否正確

## 調試 flake.nix 的幾種方法

- 注意，以下命令需要在有 flake.nix 的目錄中執行

- 查看 inputs 屬性，`$ nix flake metadata`，可查看依賴套件的實際下載位址，或 commit
  
  ```
  root@nixos ~/nixbuntu-samples (main)# nix flake metadata
  Resolved URL:  git+file:///root/nixbuntu-samples
  Locked URL:    git+file:///root/nixbuntu-samples?ref=refs/heads/main&rev=53209b77b9725d248cf745f5763f2316389331db
  Path:          /nix/store/788ilzxp3r4jcl1r7ayacc7731ls70zf-source
  Revision:      53209b77b9725d248cf745f5763f2316389331db
  Revisions:     12
  Last modified: 2023-11-12 16:03:51
  Inputs:
  ├───flake-parts: github:hercules-ci/flake-parts/8c9fa2545007b49a5db5f650ae91f227672c3877
  │   └───nixpkgs-lib: github:NixOS/nixpkgs/0cbe9f69c234a7700596e943bfae7ef27a31b735?dir=lib
  └───nixpkgs: github:lheckemann/nixpkgs/5a4f40797c98c8eb33d2e86b8eb78624a36b83ea
  ```

- 透過 `$ nix flake show`，可打印出 flake.nix 的 output 屬性

- 透過 `$ nix build --print-out-paths`，執行建構並打印出 drv 檔案的位置

- 獲取 drv 檔案的內容，`$ nix derivation show /nix/store/<hash>-ubuntu-20.04-focal-amd64`

- 在 nix repl 中
  - 方法，透過 `repl> :lf /path/to/flake.nix`，加載 flake.nix 的內容
  - 方法，透過 `repl> flake = builtins.getFlake "/path/to/flake.nix"`，加載 flake.nix 的內容

## 以 home-manager 的 helix 配置為例

- 考慮以下配置

  驗證`command = "${inputs.nil.packages.${system}.default}/bin/nil";`，是否正確
  
  ```nix
  # helix.nix

  { config, pkgs, lib, inputs, system, ... }: {
    programs.helix = {
      enable = true;
      package = inputs.helix.packages."${system}".helix;
      settings = { theme = "solarized_dark"; };
      languages = [{
        name = "nix";
        auto-format = true;
        language-server = {
          # 
          command = "${inputs.nil.packages.${system}.default}/bin/nil";
        };
      }];
    };
  }
  ```

- 在 nix repl 中獲取套件的 drv 檔

  ```
  nix-repl> pkgs = import <nixpkgs> {}
  nix-repl> pkgs.nil
  «derivation /nix/store/d7q2398ip4ynb7srdl74y61a7j2b447y-nil-2024-08-06.drv»
  ```

- 透過 nix derivation 獲取 drv 檔案的內容

  ```
  $ nix derivation show /nix/store/d7q2398ip4ynb7srdl74y61a7j2b447y-nil-2024-08-06.drv

  # 可以查看依賴
  "nativeBuildInputs": "/nix/store/m9mqqchvsayi6bc06jw7hkb6rpz7x1dq-nix-2.24.8 
    /nix/store/7yyjp09578244pvmslp17gcwpjv6czy9-auditable-cargo-1.77.2 
    /nix/store/baimybqv63556b2x2f1c5i72kql2w7m9-cargo-build-hook.sh 
    /nix/store/yy5446w88qxhwisk26qicvrmfcjlyghh-cargo-check-hook.sh 
    /nix/store/iqvdvvh58990mlsg1n1zpq32l6j0bpfj-cargo-install-hook.sh 
    /nix/store/6hk6yaisxsfk6dfajdfj7l58bhv964nj-cargo-setup-hook.sh 
    /nix/store/wv8qrls5a9bb4kb997avkwidbxmaiysx-rustc-wrapper-1.77.2",

  # 可以查看建構後的結果
  "out": "/nix/store/plzwv0dq9ipdan8iw97l89cy2vmmfjx1-nil-2024-08-06",
  ```

- 透過 nix-build 手動執行建構

  ```
  $ nix-build /nix/store/d7q2398ip4ynb7srdl74y61a7j2b447y-nil-2024-08-06.drv
  /nix/store/plzwv0dq9ipdan8iw97l89cy2vmmfjx1-nil-2024-08-06
  ```

- 檢視建構後的結果

  ```
  $ ll /nix/store/plzwv0dq9ipdan8iw97l89cy2vmmfjx1-nil-2024-08-06
  $ /nix/store/plzwv0dq9ipdan8iw97l89cy2vmmfjx1-nil-2024-08-06/bin/nil
  ```

## 打印配置文件中的屬性值

- 基本概念
  - builtins.trace 語法 : `builtins.trace 訊息 變數`

  - builtins.trace 只會捕獲有求值的變數，
    - 錯誤範例，`home.username = builtins.trace "home.username = ${home.username}" "root"`
      - "root" 不是變數
      - home.username 必須先設置值，才能被 builtins.trace 捕獲，此處是先捕獲才設置值，因此獲取不到
  
  - 在配置文件中，可用於輸出的屬性是固定的，無法新建新的輸出屬性，因此無法在輸出屬性中，自定義新的調試子屬性，
    但是可以在 let 中定義新的中間變數
    - 錯誤範例，`traceA = builtins.trace "home.username = ${home.username}" `

  - 正確流程
    - step，先將屬性值的表達式，傳遞給 let 區塊的中間變數
    - step，透過 `配置屬性 = builtins.trace "要打印的訊息 : ${中間變數名}" 中間變數名`打印
  
  - 若配置文件沒有變更，builtins 就不會再次被執行

  - 其他常用的[debug函數](../functions/functions-builtins.md#用於-debug-的內建函數)

- 範例，以 home.nix 為例

  ```
  { config, pkgs, ... }:

  let
    files = import ./files;
    sessionPath = [ "$HOME/.local/bin" "/foo/path"];
  in
  {
    home.username = "root"; 
    home.homeDirectory = "/root";
    home.stateVersion = "24.05";

    home.sessionVariables = { EDITOR = "hx"; };
    
    # 打印List
    home.sessionPath = builtins.trace "[sessionPath] = ${toString sessionPath}" sessionPath;

    # 打印屬性集
    home.file = builtins.trace "[files] = ${builtins.toJSON files}" files;

    home.packages = with pkgs; [ helix fish ];
    programs.home-manager.enable = true;
    programs.helix = import ./packages/helix.nix { inherit pkgs; };
    programs.fish = import ./packages/fish.nix;
  }
  ```

- $ home-manager switch 會在 ~/.nix-profile 建立 derivation 的 link
  
  - 查看當前的 profile 鏈接指向的 derivation，
    - 方法，$ readlink -f ~/.nix-profile，得到 /nix/store/jy4avh606n5b02a9aam5jqrjac9372s2-user-environment
    - 方法，$ nix-store --query $(readlink -f ~/.nix-profile)，得到 /nix/store/jy4avh606n5b02a9aam5jqrjac9372s2-user-environment

    - 方法，$ nix profile list，得到 /nix/store/ngqwpr2jjc790zqx3zspw1ml0b6f4pv7-home-manager-path
  
