## secret-management

- [加密公私鑰範例，透過 agenix](https://github.com/ryan4yin/nix-config/tree/main/secrets)
  - 使用 agenix 加密，並保存在單獨私人的 git-repo 中
  - 在 flake.nix 中引用該git-repo
  - 加密是使用主機公鑰完成的，並且只能配置的任何主機上解密，且只能由 root 讀取，
    私鑰永遠不會離開主機