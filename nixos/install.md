## [install] 在 qemu 中安裝 nixos-minimal-iso

- step，下載[nixos-minimal-iso](https://channels.nixos.org/nixos-23.11/latest-nixos-minimal-x86_64-linux.iso)
  - [舊版本nixos-iso下載](https://releases.nixos.org/?prefix=nixos/)
  - [下載網頁](https://nixos.org/download)
  - 檔名，nixos-minimal-23.11.5097.880992dcc006-x86_64-linux

- step，建立 qemu 硬碟檔

  > qemu-img create -f qcow2 nixos_disk.qcow2 20G

- step，啟動並安裝 nixos

  > qemu-system-x86_64 -m 4096 -smp 4 -boot d -cdrom nixos-minimal-23.11.5097.880992dcc006-x86_64-linux.iso -drive file=nixos_disk.qcow2,format=qcow2

- step，切換到root，`$ sudo su`

- step，檢查網路是否正常
  - `$ ip a`
  - `$ curl www.google.com`

- step，建立磁碟分區
  - 檢查磁碟路徑，`$ fdisk -l`
  - 對第一個硬碟進行分區，`$ fdisk /dev/sda`
    - 建立分區: n p 1 2048 +512M
    - 建立分區: n p 2 1050624 `<Enter>`
    - 調正分區類型: t 1 EF 
    - 調正分區類型: t 2 linux 
    - 寫入 w
    - 退出 q
  
  - 將分區1格式化為 fat32 的檔案系統，`$ mkfs.fat -F 32 -n boot /dev/sda1`
  - 將分區2格式化為 btrfs 的檔案系統，`$ mkfs.btrfs -L nixos /dev/sda2`
  - 掛載分區
    - 掛載分區2到/mnt，`$ mount /dev/sda2 /mnt`
    - 掛載分區1(開機用磁區)到/mnt/boot
      - `$ mkdir /mnt/boot`
      - `$ mount /dev/sda1 /mnt/boot`

- step，生成配置文件，`$ nixos-generate-config --root /mnt`

- step，修改配置文件，`$ vim /mnt/etc/nixos/configuration.nix`
  - 指定開機分區的掛載路徑，`boot.loader.efi.efiSysMountPoint = "/boot"`
  - 指定掛載的磁碟，`boot.loader.grub.device = "/dev/sda"`
  - 預設安裝套件
    ```nix
    enviroment.systemPackages = with pkgs; [
        vim
        wget
        fish
    ];
    ```
  - 啟用 openssh
    ```nix
    # 啟用 openssh-server
    services.openssh.enable = true;
    
    # 允許使用 root 帳號登入
    services.openssh.settings.PermitRootLogin = "yes";

    # 允許使用密碼登入
    services.openssh.settings.PasswordAuthentication = true;
    ```

- step，安裝nixos系統，`$ nixos-install --show-trace`，
  - 注意，安裝完成後，會提示輸入root密碼
  - 若重新修改 `/etc/nixos/configuration.nix`，需要重新執行`$ nixos-rebuild switch`

- step，啟動 nixos
  - 注意，登入後 nixos 後，使用 root 登入和自定義的密碼登入

  - 方法1，透過虛擬硬碟啟動 nixos (無法透過 ssh 連接)
    > qemu-system-x86_64 -m 4096 -smp 4 -drive file=nixos_disk.qcow2,format=qcow2

  - 方法2，透過 SSH 連接，需要改用已下命令啟動 qemu
    > qemu-system-x86_64 -m 4096 -smp 4 -drive file=nixos_disk.qcow2,format=qcow2 -net nic -net user,hostfwd=tcp::2222-:22

    其中，
    - `-net nic` : 為虛擬機器建立一個網路接口，使其能夠連接到網路。NIC是虛擬機器與網路之間的橋樑，允許虛擬機器發送和接收網路封包
    - `-net user` : 為虛擬機器建立一個使用者模式網路堆疊，讓虛擬機器透過主機的網路連接到外部網路，允許主機（即Windows系統）透過連接埠轉送與虛擬機器進行通訊
    - `hostfwd=tcp::2222-:22` : 設置端口轉發

    在 windows 中透過內建的 ssh 命令進行連接，`$ ssh -p 2222 root@localhost`

- step，透過`scp命令`傳輸檔案

  - 格式 : `scp 本地檔案路徑 帳號@遠端IP:遠端儲存路徑`
  
  - 狀況1，將 guest-os 中的檔案複製到 host-os (host <-- guest)
    > scp localhost:/etc/nixos/configuration.nix .

  - 狀況2，將 host-os 中的檔案複製到 guest-os (host --> guest)
    > scp ./ttt.nix root@localhost:/root

- step，[啟用實驗性命令和flask](flake/flake-overview.md#啟用實驗性命令和flakes)

## [ssh] 設置遠端用戶使用ssh-key登入nixos

- 在 windows-host 上建立公私鑰對，`$ ssh-keygen -t ed25519 -N "" -f ~/.ssh/mykey`

- 在 windows-host 上，透過`Git for bash`傳送公鑰給 nixos 的 sshd-server，
  `$ ssh-copy-id -i ~/.ssh/mykey -p 1234 ttt@localhost`

- 在 nixos 中，可透過`~/.ssh/authorized_keys`查看接收的結果

- (選用) 修改`/etc/nixos/configuration.nix`，將密碼登入改用公私鑰登入

  僅能使用公私鑰登入
  
  ```nix
  services.openssh.enable = true;
  services.openssh.settings.PermitRootLogin = "yes";
  services.openssh.settings.PasswordAuthentication = false;
  ```

  可使用公私鑰或密碼登入，取決於ssh登入時是否添加 -i 參數，若是，代表使用公私鑰登入

  ```nix
  services.openssh.enable = true;
  services.openssh.settings.PermitRootLogin = "yes";
  services.openssh.settings.PasswordAuthentication = true;
  ```

  `$ nixos-rebuild switch`，進行變更

  `$ reboot`，重啟nixos系統以重啟 sshd-service

- 在 windows-host 上，以公私鑰的方式登入
  - `$ ssh -p 埠號 帳號@遠端IP`，預設讀取 `~/.ssh/id_rsa` 的私鑰檔案
  - `$ ssh -p 埠號 帳號@遠端IP -i 私鑰檔案位置`，手動指定私鑰檔案

## [install] 在虛擬機中安裝 nixos

- 參考，[在vm中安裝nixos](https://nixos.org/manual/nixos/stable/)，可借用部分設定
  - 調整1，在 `hardware-configuration.nix` 中設置 `hardware.enableAllFirmware = true`，
  - 調整2，Remove the fsck that runs at startup, i twill always fail to run，`boot.initrd.checkJournalingFS = false;`
  - 調整3，Shared folders in configuration.nix
    ```
    { config, pkgs, ...} :
    {
      fileSystems."/virtualboxshare" = {
        fsType = "vboxsf";
        device = "nameofthesharedfolder";
        options = [ "rw" "nofail" ];
      };
    }
    ```

- 參考，[需要手動關閉UEFI的安全啟動，hyperV默認是開啟的](https://nixos-cn.org/tutorials/installation/VirtualMachine.html)

## [install] 在docker中使用nixos

- 注意，官方提供的 [nixos-image](https://hub.docker.com/r/nixos/nix) 主要是提供最小化的 nix 環境，
  並提供盡可能簡單的服務，因此下列命令需要手動安裝
  - nixos-build
  - clear

- 添加官方 channel 並更新
  - `$ nix-channel --add https://nixos.org/channels/nixos-23.11 nixos`
  - `$ nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs`

  - `$ nix-channel --update`

- [啟用實驗性命令和flakes](flake/flake-overview.md#啟用實驗性命令和flakes)

- 安裝套件
  - fish-shell，`$ nix-env -iA nixos.fish`
  - 基礎工具包，`$ nix-env -iA nixos.ncurses`

