## nix-language 的使用

- nix-lang 是一種函數式編程的語言

- nix文件也是可以執行的函數，文件名 == 函數名，
  在nix文件中`編寫任意nix表達式`後，對nix文件求值會有輸出，詳見，[nix文件的幾種執行方式](#nix文件的幾種執行方式)

- nix文件基本格式，`{輸入參數1, 輸入參數2, ...} : { 輸出函數體 }`
  
  以 configuration.nix 為例，configuration.nix 是函數名為configuration函數
 	
  { config, pkgs, ... }: 是輸入參數集，其中的 config、pkgs、都是輸入參數

- 惰性求值和立即求值
  - 狀況，在 nix repl 中
    - 預設為惰性求值
      
      > 輸入 nix-repl> { a.b.c = 1; }，會得到 { a = { ... }; }

   	  在調用前 a 都還不會有實際值，因此，頂級集合a的子集合(b、c)也都不會有值，
      因此只會有頂級的集合a，且a為空集合，也不會有b、c兩個子集合，當中的`...`代表空集合中的佔位符號
      
    - 透過`:p`改為立即求值
    
      > 輸入 nix-repl> :p { a.b.c = 1; }` ，會得到 { a = { b = { c = 1; }; }; }
    
  - 狀況，在文件求值中
    - `nix-instantiate --eval file.nix`為惰性求值，會得到 `{ a = <CODE>; }`
    - ` nix-instantiate --eval --strict file.nix`為立即求值，會得到`{ a = { b = { c = 1; }; }; }`

- nix 語言沒有變量的概念

  考慮以下函數，輸入參數a若為1，會帶入函數體中所有的a發生的位置
  ```
  # 在 nix repl 中
  foo = a: {
    a = a + 1;
    b = a + 2;
  }
  
  #調用foo函數
  foo(1)	# 得到 { a = 2; b = 3; }
  ```

## nix文件的幾種執行方式

- <font color=red>注意，nix文件內應該返回值，不應該是獨立的賦值語句，賦值語句不是值</font>

  nix文件也是可以執行的函數，因此，nix文件內應該返回值，
  由於賦值語句無法返回值，若nix文件中只有獨立的賦值語句，就會造成錯誤

  詳見[範例](#import-attribute-set)

- `方法`，透過`$ nix repl`執行

  詳見[nix repl的使用](commands.md#命令-nix-repl執行nix-language的互動式介面配置debug查詢配置值)

  - 範例，導入函數

    file.nix
    ```
    {a, b} : a + b
    ```

    在 nix repl 中
    ```
    repl> add = import ./file.nix
    repl> add {a=1;b=2;}
    ```
  
  - 範例，<a id="import-attribute-set">導入屬性集</a>

    file.nix
    ```
    # 錯誤寫法，nix文件內應該返回值，不應該是賦值語句，賦值語句不是值
    #foo = {
    # one = 1;
    # two = 2;
    # three = 3;
    #}

    # 正確寫法，
    {
      one = 1;
      two = 2;
      three = 3;
    }
    ```

    在 nix repl 中
    ```
    repl> :l file.nix
    repl> one
    1
    ```

- `方法`，透過`nix-instantiate`執行

  - 範例，評估表達式

    用於執行文件中的表達式
    ```
    # ttt.nix
    1+2
    ```
    
    `$ nix-instantiate --eval ttt.nix`，打印出 3
    `$ nix-instantiate --eval --strict ttt.nix`，文件求值，並立刻對惰性表達是求值

  - 範例，執行函數

    file.nix
    ```
    {a, b}: a+b
    ```
    
    執行，`$ nix-instantiate --eval --arg a 10 --arg b 20 file.nix`，打印出 20

  - 範例，執行函數

    file.nix
    ```
    {
      fn = {a, b} : a + b;
    }
    ```

    執行，`$ nix-instantiate --eval --expr '(import ./file.nix).fn {a=10;b=20;}'`

- `方法`，在命令行，透過`nix eval`調用flake.nix的函數

  注意，此方法有使用限制
  - 預設讀取 flake.nix 的檔案內容
  - 需要遵守 flake 配置文件的格式

  在當前目錄建立 flake.nix
  ```
  {
    outputs = { self }: {
      foo = "bar";
    };
  }
  ```

  在命令行中
  `$ nix eval .#foo`

## 內建變數 和 共用環境變數

- 環境變數
  - 參考，[共用環境變數](https://nixos.org/manual/nix/stable/command-ref/env-common)

  - 環境變數在 shell 中，透過`echo $變數名`，可以檢視變數值 

  - [NIX_PATH變數](https://nixos.org/manual/nix/stable/command-ref/env-common#env-NIX_PATH)
    - 用於路徑表達式的搜索路徑，詳見，[檔案路徑表達式](#語法-檢索路徑表達式路徑別名)
    - 當 NIX_PATH 的內容為空時，會造成`<nixpkgs>`求值失敗，導致 error: file 'nixpkgs' was not found in the Nix search path 的錯誤
    - 在使用[nix-shell命令](commands.md#命令-nix-shell根據nix表達式建立隔離的shell環境)時，可以透過`-I`臨時性修改NIX_PATH變數中的值

- nix-language內建變數
  - 參考，[內建變數列表](https://nixos.org/manual/nix/stable/language/builtin-constants#built-in-constants)

  - 內建變數中，透過 `$ nix repl`，可以檢視內建變數值
    - 例如，`$ nix-repl> builtins.langVersion`
    - repl 有內建 auto-complete 的功能，按下 TAB 可以使用自動完成

## 繼承屬性的幾種方式

- 方法，透過`//`，常用於全部繼承
- 方法，透過`inherit`，常用於部分繼承
- 方法，透過`rec`，遍例屬性

- 以上方法，常用於從另一個屬性集合中繼承屬性，避免手動重複寫入屬性

## [語法] 變數和基礎數據類型

- 賦值語法
  
  `原始數據類型、列表、屬性集、函數`，都可以被當作值，使用 `=` 為變數名綁定值(賦值)，然後用`分號`分隔賦值語句，

  例如，具名變數，foo = 123;

  例如，匿名變數，
  ```
  {
    number = 12345;
      isOpen = true;
      name = "david";
      
  }
  ```

- 列表的使用(list)
  - list 可以包含不同類型的元素
  - 對象使用`;`來區隔不同的元素，list使用`空白`來區隔元素
  - 若list中的元素為屬性集或函數，需要添加`小括號`來與其他元素進行區隔
    
    例如
    ```
    list = [1 2 ( {aa=11; bb=22;} )]
    ```

    例如
    ```
    lsit = [ 123 ./foo.nix "abc" ( f { x = y; } ) ]
    ```

- 屬性集(類似 json 或python的對象)的兩種寫法
  
  - 屬性集(attribute-set)，是多個元素的集合，每個元素是由鍵名和鍵名組成的對，集合內的鍵名被稱為這個集合的屬性

  - 建立屬性集
  
    以下3種寫法是等效的
  
    - 方法1，寫成 json 的形式

      ```nix
      # 將共用的部分提取出來，寫成.的形式
      services.openssh = {
        enable = true;
        permitRootLogin = "yes";
      };
      
      # 或者這種，完全寫成json的形式
      services = {
        openssh = {
          enable = true;
          permitRootLogin = "yes";
        };
      };
      ```
    
    - 方法2，完全展開，不寫成 json 的形式
    
      ```
      services.openssh.enable = true;
      services.openssh.permitRootLogin = "yes";
      ```

  - 屬性集和json的差異
    - 鍵名不需要添加"" (鍵名不是字串)
    - array 使用空白來區分不同的元素
    - json 不能添加註解，屬性集可以
  
  - 匿名集合和具名集合
    - 具名集合，`foo = { bar };`
    - 匿名集合，`{ bar };`

- 字串插值(在字串中插入變數值)

  在字串中，透過 `${變數名}` 進行插值

  ```
  repl> foo = "world"
  repl> bar = "hello ${foo}"
  repl> bar
  "hello world"
  ```

  注意，字符串插值語法只支持字符串類型，${變數名} 中調用的`變數必須是字串類型`

- 字串轉譯符號
  - 針對特殊符號，例如，`"`或`\`或`$`或`{`或`\n`或`\t`，可以使用`\`進行轉譯，轉換為純字串符號 
  
  - `狀況`，用於單行，使用`\`進行轉譯，轉換為純字串符號，例如

    ```
    "this is a \"string\" \\"  # 結果是: this is a "string" \
    ```

  - `狀況`，對於多行，使用`''`進行轉譯，轉換為純字串符號，例如，

    ```
    ''
    This is the first line.
    This is the second line.
    This is the third line.
    ''
    ```

  - `狀況`，對於多行，若需要打印`''`，可改用`'''`，例如，

    ```
    ''
    This is the '''first''' line.
    ''
    ```

- 路徑類型
  - 路徑不是字串類型，路徑在 nix-lang 中是獨立的數據類型
  
  - 分為兩種 : `一般路徑`和`檢索路徑`

  - 一般路徑
    - `建立方法`，`路徑類型`的文件路徑，`不加引號`

      > file1 = ./myfile1;，會被複製成 /nix/store/[哈希值]-somefile.txt

      - 會在解析配置階段被複製到 /nix/store 中，並從該路徑進行調用
      - 一般用來指定和配置文件一起管理的文件
      - 一般用來指定獨立管理的文件，例如，網站程序代碼
      
    - `建立方法`，`字串類型`的文件路徑，`會加引號`

      > file2 = "./myfile2";
    
      - 不會被nix讀取或處理，指定的文件不會被複製，因此該文件的內容不會成為配置文件的一部分
      - 該路徑可以被原樣寫進配置文件，並被最終執行的程序讀取

    - 限制，一般路徑`至少要包含一個斜槓`才能被識別為路徑：

  - 檢索路徑 (lookup paths)，又稱為方尖路徑 <路徑>

    參考，[檢索路徑表達式](#語法-檢索路徑表達式expr檔名)

- `src = ./` 和 `src = ./.` 兩者的差異，詳見[pkg.derivation](commands_flake.md#常用函數)
  
## [語法] let ... in ... 區域變數的使用

- 語法
  
  ```
  let
    區域變數1 = 值1;
    區域變數2 = 值2;
    ...
  in
    返回值(或讀取區域變數的值進行運算)
  ```

  - `let 區塊`，用於定義區域變數
  
  - `in 區塊`，用於返回值
    - in 後面只能跟隨一個表達式
    - 可以調用let區塊中定義的變數 
    - 在 in 區塊中可以定義新的 object 並返回 object (object也是值的一種)，或進行求值

- 範例，求值範例

  定義 ./file.nix
  ```
  let
    x = 5;
    y = 10;
    sum = x + y;
  in
    sum
  ```

  調用，`$ nix-instantiate --eval --strict file.nix`
  
- 範例，返回 object

  定義 ./file.nix
  ```
  let
    x = 10;
    y = 5;
    sum = x + y;
  in
  {
    xValue = x;
    sumValue = sum;
  }
  ```

  調用1，`$ nix-instantiate --eval --strict file.nix`，得到對象的內容 { sumValue = 15; xValue = 10; }
  調用2，`$ nix-instantiate --eval --expr '(import ./file.nix).xValue'`，得到 10

- 範例，錯誤使用範例，跨作用域取用區域變數值
  ```
  {
    a = let x = 1; in x;
    b = x;  # x 只能限定在上述表達式中使用
  }
  ```

- 範例，錯誤使用範例，跨作用域取用區域變數值
  ```
  let
    x = 10;
    y = 5;
    sum = x + y;
  in
  result = {
    # 當前作用域在 let 外部，因此無法取用 x 和 sum
    xValue = x;
    sumValue = sum;
  }

  result
  ```

- 範例，錯誤使用範例，錯誤調用 ojbect 的值

  ```
  let
    x = 10;
    y = 5;
    sum = x + y;
  in
  {
    result = sum + 5;
  }

  # 在 in 之後是一個 object，object 不能像函數一樣直接調用
  result
  ```

  <font color=blue>正確返回 result 的方法</font>
  ```
  let
    x = 10;
    y = 5;
    sum = x + y;
  in
  {
    result = sum + 5;
  }.result
  ```

## [語法] with 的用法，縮短屬性集的調用

- 用於縮短前綴的屬性名

  例如，pkgs.stdenv.mkDerivation()，透過 with pkgs，就可以簡化為 stdenv.mkDerivation()
  
  注意，with 可以寫在函數中，但是不能寫在屬性集中，可以寫在屬性集之前，例如，
  ```
  nix-repl> a = {aa=123;}
  nix-repl> with a;
            { bb=aa;}   # 錯誤，aa 是變數名，不是變數值
  nix-repl> with a;
            [aa]   # 正確
  ```

- 範例
  
  ```
  { pkgs ? import <nixpkgs> {} }:

  with pkgs;

  # 透過 with pkgs，將 pkgs.stdenv.mkDerivation 簡化為 stdenv.mkDerivation 
  stdenv.mkDerivation rec {
    name = "my-program-${version}";
    version = "1.0";

    src = ./.;

    buildPhase = "echo Building my program";
  }
  ```

- 範例

  file.nix
  ```
  let
    a = {
      x = 1;
      y = 2;
      z = 3;
    };
  in
    with a; 
    [ x y z ]  # 等價 [ a.x a.y a.z ]
  ```

  在 nix repl 中，
  ```
  repl> foo = import ./file.nix
  repl> foo
  [ 1 2 3 ]
  ```

## [語法] // 和 inherit，快速添加屬性

- 注意，合併運算子不是遞歸的

  例如，
  ```
  nix-repl> foo = { bar = { baz = "foo"; }; }
  nix-repl> :p (foo // { bar = { spam = "eggs"; }; })
  { bar = { spam = "eggs"; }; }
  ```

  例如，
  ```
  nix-repl> foo = { a=1; }
  nix-repl> bar = { a=2; b={aa=1; bb=2;}; }
  nix-repl> :p foo // bar
  { a = 2; b = { aa = 1; bb = 2; }; }   # a 被 bar 覆寫
  ```

- `範例`，繼承全部屬性，並建立新屬性

  ```
  repl> foo = {
    aa = 1;
    bb =2;
  }

  repl> bar = foo // { cc =3; }
  repl> bar
  { aa = 1; bb = 2; cc = 3; }
  ```

- `範例`，可用於將屬性及分離，見，[利用container建立nginx](flake/flake-examples/nixpkgs.lib.nixosSystem_container_nginx.nix)

- `技巧`，在配置檔中，// 可用於分離`derivation函數`的`內建屬性`和`自定義屬性`
  - derivation函數指的是會產生 drv 檔案的函數
  
  - 若將自定義屬性，`直接添加derivation函數的輸入屬性集中`，會造成每次執行nix-build後
    ，就會產生新的derivation(產生新的hash值)

  - 若`先讓derivation函數產生輸出屬性集後`，再透過 // 與`自定義屬性集`合併，
    就不會每次變更都產生新的derivation，因為derivation函數沒有增加任何新的屬性
  
  - 範例

    ```nix
    nix-repl> derivation { name="test"; builder="/bin/sh"; system="x86_64-linux"; }
    # 全都是內建屬性，執行後hash為y1s2f
    «derivation /nix/store/`y1s2f`iq89v2h9vkb38w508ir20dwv6v2-test.drv»

    nix-repl> derivation { name="test"; builder="/bin/sh"; system="x86_64-linux"; foo=11;}
    # 添加額外的foo屬性，執行後hash為sg8g6，變更了
    «derivation /nix/store/`sg8g6`n80nrpph4skmspxmj073ycrwv2w-test.drv»

    nix-repl> derivation { name="test"; builder="/bin/sh"; system="x86_64-linux"; } // {foo=11;}
    # 透過 // 添加額外的foo屬性，hash為y1s2f，與未添加額外屬性前一致
    «derivation /nix/store/`y1s2f`iq89v2h9vkb38w508ir20dwv6v2-test.drv»
    ```

## [語法] inherit，快速添加屬性

- inherit的使用，在`屬性集`中展開並`建立`對應的`鍵和值`

- 語法，全部繼承
  - `{ inherit 要繼承的屬性集名a }`，建立指定屬性集的鍵和值
  - 例如，
    ```
    let
      x = 123;
    in 
    {
      inherit x;    # 展開為 x = x ，等價於 x = 123
    }
    ```

- 語法，部分繼承
  - `{ inherit (要繼承的屬性集名a) 要繼承的子屬性名b 要繼承的子屬性名c }`，部分繼承，僅繼承a的子屬性b和子屬性c
  - `{ inherit (要繼承的屬性集名) 要繼承的子屬性名b 要繼承的子屬性名c { 新的屬性名d = ... } }`，部分繼承並添加新屬性
  - 例如，
    ```
    let 
      x = {
        a = 123;
        b = 456
      }
    in {
      inherit (x) a   # 展開為 a = x.a ，等價於 a = 123
    }
    ```

- 限制，inherit 不能用於`賦值`，只能用於在屬性集內，用於建立屬性集的子屬性

- `範例`，錯誤的使用範例，不能用於賦值

  ```
  nix-repl> foo = {
    a = 1;
    b = 2;
  }

  # 不能用於賦值
  repl> bar = inherit (foo);  # 錯誤，error: syntax error, unexpected '{'

  # 沒有指名要繼承 foo 屬性集的任何屬性，因此 bar 是一個空集合
  repl> bar = { inherit (foo) } # bar = {}

  # 正確用法
  repl> bar = { inherit (foo) a} # bar = { a=1; }
  ```

- `範例`，部分繼承，並添加新屬性

  ```
  nix-repl> foo = {
    a = 1;
    b = 2;
  }

  nix-repl> bar = {
    # 僅繼承 foo.a
    inherit (foo) a;
    
    # 添加新屬性
    aa = 11;
  }

  nix-repl> bar
  { a = 1; aa = 11; }
  ```

- `範例`，建立同名的新屬性，並獲取所有的子屬性作為值

  ```
  nix-repl> foo
  { x = 1; y = 2; }

  nix-repl> bar
  { m = 3; n = 4; }

  nix-repl> attrs = {
    inherit foo;      # 展開為 foo = foo，等校於 foo = { x = 1; y = 2; }
    inherit bar;      # 展開為 bar = bar，等校於 bar = { m = 3; n = 4; }
  }
            
  nix-repl> :p attrs
  { bar = { m = 3; n = 4; }; foo = { x = 1; y = 2; }; }
  ```


## [語法] rec，宣告為遞歸屬性集，允許函數或屬性集調用自身

- 用途，允許函數或屬性集調用自身
  - 用於`屬性集`，將當前屬性宣告為`遞歸屬性`，允許在屬性集中調用當前屬性集的其他子屬性
  - 用於`函數`，將當前函數宣告為`遞歸函數`，允許函數中調用當前函數(遞歸函數)

- 範例，透過 rec 宣告為遞歸屬性，並調用自身的其他屬性

  ```
  {
    rec myAttrs = {
        a = 1;
        b = 2;
        c = myAttrs.a + myAttrs.b;
    };
  }
  ```

  另一個範例
  ```
  { stdenv, fetchurl }:

  stdenv.mkDerivation rec{
    pname = "foo";
    version = "1.0";

    src = fetchurl {
      # 調用自身的 version 屬性
      url = "https://foo-${version}.tar.gz";
      sha256 = "...";
    };
  }
  ```

  若不使用 rec ，可以改用 let ... in ... 的語法，提前定義 version 屬性
  ```
  { stdenv, fetchurl }:
  let
    version = "1.0";
  in
  stdenv.mkDerivation {
    pname = "foo";
    inherit version;

    src = fetchurl {
      url = "https://foo-${version}.tar.gz";
      sha256 = "...";
    };
  }
  ```

- 範例，透過 rec 宣告為遞歸函數，並在函數中調用自身函數

  ```
  rec factorial = n: 
  # 遞歸調用自身函數  
  if n == 0 then 1 else n * factorial (n - 1);
  ```

## [語法] |> 管道操作 (pipes-operattor)

- 限制 : 需要 nix v2.24 以上的版本，才能透過 experimental-features 開啟，
  參考，[升級nix](commands.md#使用範例-升級系統--升級-nix)

- 啟用 pipe 功能
  - 方法，在 nix.config 中添加，`extra-experimental-features = pipe-operators`
  - 方法，在 configuration.nix 中添加，`nix.settings.experimental-features = [ pipe-operators ];`
  - 方法，在 命令參數中添加 `--extra-experimental-features pipe-operators`
    - 例如，`$ nix repl --extra-experimental-features pipe-operators`
    - 例如，`$ nix eval --extra-experimental-features pipe-operators --file default.nix`

- 語法: `值或列表 >| 接受輸入的函數1 |> 接受輸入的函數2 ...` 

- 語法: 使用 pkgs.lib.pipe
  
  ```nix
  pkgs.lib.pipe 值或列表 [
    接受輸入的函數1
    接受輸入的函數2
    ...
  ]
  ```

- 範例，簡單範例

  ```
  5.5 |> (a: a+4)  等效於  (a: a+4) 5.5
  ```

  ```
  (a: a*5) (builtins.floor ((a: a+4) 5.5))   
  等效於 5.6 |> (a: a+4) |> builtins.floor |> (a: a*5)
  ```

  在 nix repl 中使用
  ```
  nix-repl> pkgs = import <nixpkgs> {}
  nix-repl> pkgs.lib.pipe 10 [
              (x: x+2)
              (x: x*5)
            ]

  nix-repl> pkgs.lib.pipe [ 5 20 35 79 41 1 19] [
              (builtins.filter (n: n>50))
              (builtins.sort builtins.lessThan)
              (map (n: n+10))
            ]
  
  # 等效於
  [ 5 20 35 79 41 1 19] 
    |> (builtins.filter (n: n>50)) 
    |> (builtins.sort builtins.lessThan) 
    |> (map (n: n+10))
  ```

## [語法] 一般函數

- 建立函數，
  - 語法，`輸入參數 : 輸出表達式`
  
  - 語法，`{輸入參數名1, 輸入參數名2} : 輸出表達式`

  - 語法，帶有預設值的輸入參數，`{輸入參數名1, 輸入參數名2 ? 默認值} : 輸出表達式`
  
  - 語法，過`...`，使輸入參數接受額外的參數，允許傳入額外參數，`{輸入參數名1, 輸入參數名2, ...} : 輸出表達式`
    - 只有明確寫出輸入參數名的變數，才能在輸出表達式中使用
    - 錯誤用法，`{ a, b, ...}: a + b + c`，輸入參數c無法用於輸出表達式中
    - 參考，[@為輸入參數添加額外的屬性](#語法-屬性集名)

- 注意，在 nix 文件或 nix repl 中，函數體和冒號之間需要至少一個空白，否則該函數會被視為是字串

  - 正確寫法 : `(a: a+4) 5`
  - 錯誤寫法 : `(a:a+4) 5`，冒號後方沒有空格

- `範例`，建立單輸入函數並調用

  在 nix repl 中
  
  <font color=blue>lambda範例(單行代碼)</font>
  ```
  # 定義
  foo = a : a*2

  # 調用 
  foo 5 # 得到 10
  ```

  <font color=blue>多行代碼範例</font>
  ```
  # 定義
  foo = a : {
    a = a + 1;
    b = a + 2;
  }

  # 調用 
  foo(1)  # 正確調用方式
  foo 1  # 正確調用方式
  #foo {a=1;} # 錯誤調用方式
  ```

- `範例`，建立多輸入函數並調用

  在 nix repl 中
  ```
  # 定義
  { a, b }: {
    a = b;
    b = a;
  }

  # 正確調用方式
  foo {a=1; b=2;}
  
  #foo(1,2) # 錯誤調用方式，需要以屬性集的方式，傳入輸入參數
  #foo {1, 2} # 錯誤調用方式，輸入參數需要具名指定值
  #foo {a=1, b=2} # 錯誤調用方式，需要以分號區隔
  ```

- `範例`，建立並調用匿名函數
  
  - 透過()將函數和輸入參數區隔，格式，`(函數) 輸入參數`
  - (x:x+1)，建立匿名函數
  - (x:x+1) 1，建立匿名函數，並調用函數，且輸入參數為1

  ```
  (x:x+1) 1
  ```

## [語法] 柯里化函數

- 柯里化函數

  在 nix repl 中
  ```
  # 定義柯里化函數
  # a : b 代表第一次調用，接收一個輸入參數a，並返回一個接受輸入參數b的函數
  # b : {} 代表第二次調用，接收一個輸入參數b，並可在該函數中使用前一次已傳入的輸入變數a
 
  bar = a: b: {
    product = a * b;
    quotient = a / b;
  };

  # 調用
  (bar 10) 2 # 得到 { product = 20; quotient = 5; }
  ```

- 範例

  ```
  f = x: y: x + y
      ^^            傳入第1個參數x，並返回一個包含參數x的函數
         ^^         傳入第2個參數y，並返回一個包含參數y的函數
            ^^^^^   最後的函數體為 x+y
  
  # 調用方法1
  (f 1) 2
  ^^^^^             1:1+y，第1次調用
        ^^          2:1+2，第2次調用

  # 調用方法2
  f 1 2  
  ```

- 範例，[利用 nix文件編寫一個簡單的函數，並在 nix repl 中調用](commands.md#命令-nix-repl執行nix-language的互動式介面配置debug查詢配置值)

## [語法] 檢索路徑表達式，`<路徑別名>`

- `<檔案名>`，專門用於檢索路徑表達式的求值，其值必定是`檔案路徑`的字符串
  
  - 類似環境變數，`<路徑別名>` 依賴 `$NIX_PATH` 環境變數中設置的路徑尋找實際的路徑

  - 利用<路徑別名>取得實際路徑，可以用於 `nix 文件`、`命令行`、`nix表達式`

- 解析`<路徑別名>`實際路徑的搜尋順序
  - 絕對路徑：直接使用尖括號內的路徑，
    例如，`<path/to/directory>`，檢查該路徑是否存在

  - $NIX_PATH：按配置順序查找，
    例如，執行`$ echo $NIX_PATH`，會得到`nixpkgs=/path/to/nixpkgs`，定義了 nixpkgs 的具體位置

  - Nix Channels：從用戶和系統的 channels 目錄中查找，
    例如，執行`$ nix-channel --list`，會得到`home-manager https://github.com/nix-community/home-manager/archive/release-23.11.tar.gz`

  - Nix Profiles：查找用戶或系統層級的 profiles，
    - 通過 nix-env 安裝了特定的 channel 後。這些 paths 可能位於 /nix/var/nix/profiles/per-user/ 目錄下
    - 例如，`$ ll /nix/var/nix/profiles/per-user/root`

  - Nix Registry（用於 Flakes）：通過 Flake registry 查找路徑（如果使用 Flakes），
    例如，執行`nix registry list`，得到`global flake:nixos-homepage github:NixOS/nixos-homepage`

  - 系統配置路徑，`/nix/var/nix/profiles/system`

  - 網絡或 Nix Store：最終可能通過網絡拉取或從 Nix store 中查找

- 以 `<nixpkgs>` 為例

  當nixos遇到`<nixpkgs>`時，會搜尋`NIX_PATH環境變數`指向的路徑，並在該路徑中尋找指定目標 (在本例為`nixpkgs`這個檔案)
  
  <font color=blue>檢視 NIX_PATH 環境變數</font>

  - 方法1，在 shell 中，輸入 `$ echo $NIX_PATH` 會得到以下結果
  
    ```shell
    [root@nixos:~]# echo $NIX_PATH | tr : '\n'
    /root/.nix-defexpr/channels
    nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos
    nixos-config=/etc/nixos/configuration.nix
    /nix/var/nix/profiles/per-user/root/channels
    ```

  - 方法2，在 nix repl中，輸入 `repl> :p builtins.nixPath`，會得到以下的結果

    ```nix-repl
    [ 
      { path = "/root/.nix-defexpr/channels"; prefix = ""; }
      { path = "/nix/var/nix/profiles/per-user/root/channels/nixos"; prefix = "nixpkgs"; } 
      { path = "/etc/nixos/configuration.nix"; prefix = "nixos-config"; }
      { path = "/nix/var/nix/profiles/per-user/root/channels"; prefix = ""; } ]
    ```
  
  <font color=blue>利用 <路徑別名> 取得實際的檔案路徑</font>

  - 由上述可知，NIX_PATH環境變數 包含了以下幾個路徑，每一個路徑以`:`區隔
    - 路徑1，/root/.nix-defexpr/channels (目錄)
    - 路徑2，nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos (目錄)
    - 路徑3，nixos-config=/etc/nixos/configuration.nix (檔案)
    - 路徑4，/nix/var/nix/profiles/per-user/root/channels (目錄)
  
  其中，部分路徑帶有路徑別名(prefix)的屬性，就是該路徑的路徑別名，例如，
  - 別名`<nixpkgs>`，對應的實際路徑為 /nix/var/nix/profiles/per-user/root/channels/nixos
  - 別名`<nixos-config>`，對應的實際路徑為 /etc/nixos/configuration.nix

  利用 `<路徑別名>` 就可以得到實際的檔案路徑，
  例如，在 nix-repl 中，輸入 `<nixpkgs>`，
  等效於執行 `$ nix-repr> builtins.findFile builtins.nixPath "nixpkgs"`，兩者結果是一樣的

- 盡量避免使用檢索路徑表達式來指定其他相對路徑，例如，`<nixpkgs/lib>`，避免路徑的汙染
  
  此方式將路徑指定為`<nixpkgs>`下的lib，nixpkgs依賴於系統的環境變數`$NIX_PATH`，
  因此，`<nixpkgs>`的路徑變動或失效，有可能造成`<nixpkgs/lib>`失效或連結到錯誤的路徑
  
- 範例，<font color=blue>以 \<nixpkgs\路徑2> 為例</font>

  若NIX_PATH環境變數指向 /aa/bb/cc/:/dd/ee 兩個目錄，
  - /aa/bb/cc/路徑2
  - /dd/ee/路徑2
  
  nixos 會依序尋找上述兩個路徑，並返回第一個命中的檔案路徑

- 範例，應用於套件安裝
  
  例如
  > $ nix-env -p /nix/var/nix/profiles/system -f '<nixpkgs/nixos>'
  > -I nixos-config=/etc/nixos/configuration.nix
  > -iA system`

  - 概念，[配置文件和模塊的差異](concept.md#特性-配置文件和模塊的差異)

  - 安裝套件，`-f '<nixpkgs/nixos>'`，指定nix表達式文件的路徑，該文件包含了 system 套件的定義，
    nix-env 會從這個文件中讀取 system 套件的定義檔，並根據這個定義檔來安裝或更新 system 套件

  - 安裝系統，`-I nixos-config=/etc/nixos/configuration.nix`，指定了 NixOS 配置文件的路徑，
    此文件包含系統的配置，例如啟用哪些服務、安裝哪些軟件包等，代表 nix-env 會根據這個配置文件來建立新的系統環境

  - 定義作用範圍，`-p /nix/var/nix/profiles/system`，指定要操作的用戶環境的路徑，在此例子中，指向系統環境，這意味著這個命令將影響整個系統的配置

  - 指定要安裝的套件名，`-iA system`，指定要安裝的套件

  - 總的來說

    -iA 指定的要安裝的套件，-f 指定的安裝套件的定義檔位置，-I 指定了nixos配置文件的位置，-p 定義了新環境影響的範圍是整個系統，透過上述的命令，會先到 nixpkgs/nixos 尋找 system套件的定義檔並根據定義檔建構並安裝後，根據configuration.nix建立新的系統環境，並將新的環境作用於整個系統

## [語法] @屬性集，子屬性引用的擴展語法，用於精簡子屬性的調用

- 功能
  - `功能1`，精簡子屬性的調用
  - `功能2`，屬性集解構
  - `功能2`，模式匹配

- 範例，額外屬性添加在@前方|後方的區別

  兩者本質上<font color=red>沒有區別</font>，寫在@的前方或後方效果都是一樣的，
  本質上都是將所有的子屬性放在同一個屬性集合中

  `args@{ a, b, ... }` 和 `{ a, b, ... }@args`，兩者是等價的，代表
  - 將 args.a 擴展為 a
  - 將 args.b 擴展為 b
  - 此語法是將屬性集(此處為args)中的元素(例如，args.a)擴展新的符號(例如，a)，
    方便精簡後續的使用

  <font color=blue>額外屬性添加在@的前方</font>
  ```
  # args@{ a, b, ... }，代表將 a、b 兩個參數添加到args屬性集中
  # 在函數中，可以直接使用 a、b，也可以使用 args.a 或 args.b
  
  # 以下兩種方法是等效的
  repl> fn = args@{ a, b, ... }: a + b + args.c             # 擴展後，可以精簡調用方式
  repl> fn = args@{ a, b, ... }: args.a + args.b + args.c   # 擴展後，仍然可以使用原來的調用方式
  
  # 調用
  repl> paras = {a=1; b=2; c=3;}
  repl> fn paras
  6
  ```

## 常用函數

- `nixpkgs.lib.genAttrs`，將列表中元素逐次傳遞給函數，並產生屬性集

  語法，`nixpkgs.lib.genAttrs [元素1 元素2 元素3] 函數`
  
  注意，nixpkgs.lib.genAttrs 是一個遞歸函數， `nixpkgs.lib.genAttrs :: [ String ] -> (String -> Any) -> AttrSet`，
  可以分兩次傳遞輸入參數，也可以一次性將所有的輸入參數傳遞給 nixpkgs.lib.genAttrs

  產生的屬性集為
  ```
  {
    # 建立元素1的鍵，元素1的值為調用函數結果，調用的同時會將元素1帶入函數中
    元素1: (函數 元素1);
    元素1: (函數 元素1);
    元素1: (函數 元素1);
  }
  ```

  語法，`nixpkgs.lib.genAttrs [元素1 元素2 元素3] ( 元素:{ key1=元素, key2=value, ...} )`

  產生的屬性集為
  ```
  {
    元素1:{ key1=元素1, key2=value, ...};
    元素2:{ key1=元素2, key2=value, ...};
    元素3:{ key1=元素3, key2=value, ...};
  }
  ```

  - 範例，pkgs.lib.genAttrs 的簡易使用

    ```
    repl> pkgs = import <nixpkgs> {};
    repl> :p pkgs.lib.genAttrs ["apple" "banana" "orange"] (fruit: { name = fruit; color = "red";} )
    
    { apple = { color = "red"; name = "apple"; }; 
      banana = { color = "red"; name = "banana"; }; 
      orange = { color = "red"; name = "orange"; };
    }
    ```

## ref

- [官方nix語言基礎](https://nix.dev/tutorials/nix-language)
- [官方nix語言文檔 @ nixos中文](https://nixos-cn.org/tutorials/lang/)
- [簡單的nix-language語法介紹](https://lantian.pub/article/modify-website/nixos-initial-config-flake-deploy.lantian/)
- [Nix語言複習](https://juejin.cn/post/7165305697561755679)
- [nix語言 @ nix詳解](https://www.rectcircle.cn/posts/nix-3-nix-dsl/)

- 檔案路徑表達式
  - [what does the \<nixpkgs> mean in nix](https://stackoverflow.com/questions/47379654/what-does-the-nixpkgs-string-value-mean-in-nix)
  - [官方，Lookup path](https://nixos.org/manual/nix/stable/language/constructs/lookup-path)
