## 常見問題

- `error: getting status of '/nix/store/0ccnxa25whszw7mgbgyzdm4nqc0zwnm8-source/flake.nix': No such file or directory`

  此為 nix 特殊的機制 : 執行命令時，僅將git索引過的檔案複製到儲存中。

  因此，對於包含.git目錄的flake專案(有flake.nix的目錄)，若運行命令時，需要的檔案是新增檔案，
  則需要將該檔案加入追蹤(加入git索引)，例如，`$ git add 新檔案`，或 `$ git add *`

- `error: file 'nixpkgs' was not found in the Nix search path (add it using $NIX_PATH or -I)`

  ```
  root@nixos ~ [1]# echo $NIX_PATH
  /root/.nix-defexpr/channels 
  nixos-config=/root/.config/nix-config/nixos/configuration.nix 
  nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixpkgs
  ```

  雖然$NIX_PATH有添加`nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixpkgs`，
  但此目錄是由 nix-channel 中的 nixpkgs 所建立的，

  若只在$NIX_PATH添加nixpkgs，但 nix-channel 中卻未添加 nixpkgs 時，就會造成此問題

  解決方法1

  ```
  nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs
  nix-channel --update
  ```

  解決方法2，推薦，詳見，[升級系統](commands.md#使用範例-升級系統--升級-nix)中的說明
  ```
  export NIX_PATH=nixpkgs=/nix/var/nix/profiles/per-user/$USER/channels/nixos:$NIX_PATH
  ```

