
# from [build-qemu-vm-only-runs-inside-nios-host](build-vm-examples/nixos.qcow2-only-run-on-nixos-host.nix)

# ===============================================

# 設置systemConfig
systemConfig=/nix/store/cm3vjqbzy28c510fgi0yzvdawl1jlgrg-nixos-system-nixos-25.05.20241203.566e53c
# 設置 HOME 和 PATH 環境變數
export HOME=/root PATH="/nix/store/b1wvkjx96i3s7wblz38ya0zr8i93zbc5-coreutils-9.5/bin:/nix/store/ndqpb82si5a7znlb4wa84sjncl4mvgqm-util-linux-2.39.4-bin/bin"

if [ "${IN_NIXOS_SYSTEMD_STAGE1:-}" != true ]; then
    # Process the kernel command line.
    # cat /proc/cmdline 得到以下
    # - BOOT_IMAGE=(hd0,msdos1)//kernels/ky4ws19vayymrp956spiyijkypsvi9mj-linux-6.6.54-bzImage 
    # - init=/nix/store/slbf8bdqi3qh837z28969vr0hh03ndj8-nixos-system-nixos-24.11.20241009.5633bcf/init 
    # - loglevel=4
    for o in $(</proc/cmdline); do
        case $o in
            boot.debugtrace)
                # Show each command.
                set -x
                ;;
        esac
    done

    # Print a greeting.
    echo
    echo -e "\e[1;32m<<< NixOS Stage 2 >>>\e[0m"
    echo


    # Normally, stage 1 mounts the root filesystem read/writable.
    # However, in some environments, stage 2 is executed directly, and the
    # root is read-only.  So make it writable here.
    if [ -z "$container" ]; then
        mount -n -o remount,rw none /
    fi
fi

# Likewise, stage 1 mounts /proc, /dev and /sys, so if we don't have a
# stage 1, we need to do that here.
if [ ! -e /proc/1 ]; then
    specialMount() {
        local device="$1"
        local mountPoint="$2"
        local options="$3"
        local fsType="$4"

        # We must not overwrite this mount because it's bind-mounted
        # from stage 1's /run
        if [ "${IN_NIXOS_SYSTEMD_STAGE1:-}" = true ] && [ "${mountPoint}" = /run ]; then
            return
        fi

        install -m 0755 -d "$mountPoint"
        mount -n -t "$fsType" -o "$options" "$device" "$mountPoint"
    }
    source /nix/store/pksi98qf4qkdzfi5acb1hcsn8g7aqz7r-mounts.sh
fi

if [ "${IN_NIXOS_SYSTEMD_STAGE1:-}" = true ] || [ ! -c /dev/kmsg ] ; then
    echo "booting system configuration ${systemConfig}"
else
    echo "booting system configuration $systemConfig" > /dev/kmsg
fi

# Make /nix/store a read-only bind mount to enforce immutability of
# the Nix store.  Note that we can't use "chown root:nixbld" here
# because users/groups might not exist yet.
# Silence chown/chmod to fail gracefully on a readonly filesystem
# like squashfs.
chown -f 0:30000 /nix/store
chmod -f 1775 /nix/store
if [ -n "1" ]; then
    if ! [[ "$(findmnt --noheadings --output OPTIONS /nix/store)" =~ ro(,|$) ]]; then
        if [ -z "$container" ]; then
            # 將本地的 /nix/store 掛載到VM中的 /nix/store 
            mount --bind /nix/store /nix/store
        else
            mount --rbind /nix/store /nix/store
        fi
        # 掛載 /nix/store 子目錄
        mount -o remount,ro,bind /nix/store
    fi
fi


if [ "${IN_NIXOS_SYSTEMD_STAGE1:-}" != true ]; then
    # Use /etc/resolv.conf supplied by systemd-nspawn, if applicable.
    # cat /etc/resolv.conf 得到 
    # - nameserver 10.0.2.3
    # - options edns0
    if [ -n "" ] && [ -e /etc/resolv.conf ]; then
        resolvconf -m 1000 -a host </etc/resolv.conf
    fi


    # Log the script output to /dev/kmsg or /run/log/stage-2-init.log.
    # Only at this point are all the necessary prerequisites ready for these commands.
    # 紀錄腳本輸出到 /dev/kmsg 或 /run/log/stage-2-init.log
    exec {logOutFd}>&1 {logErrFd}>&2
    if test -w /dev/kmsg; then
        exec > >(tee -i /proc/self/fd/"$logOutFd" | while read -r line; do
            if test -n "$line"; then
                echo "<7>stage-2-init: $line" > /dev/kmsg
            fi
        done) 2>&1
    else
        mkdir -p /run/log
        exec > >(tee -i /run/log/stage-2-init.log) 2>&1
    fi
fi

# Required by the activation script
# 為 activation-script 設置權限
# - 設置 /etc 的權限
# - 設置 /etc/nixos 的權限
# - 設置 /tmp 的權限

install -m 0755 -d /etc
if [ ! -h "/etc/nixos" ]; then
    install -m 0755 -d /etc/nixos
fi
install -m 01777 -d /tmp

# Run the script that performs all configuration activation that does
# not have to be done at boot time.

echo "running activation script..."
# 執行 activate-script
$systemConfig/activate

# Record the boot configuration.
# 為保存在 /nix/store 中的 $systemConfig 目錄，在VM中建立連結 (/run/booted-system/activat)，內容為
#   activate               boot.json           etc                 init                    kernel          nixos-version   system
#   append-initrd-secrets  configuration-name  extra-dependencies  init-interface-version  kernel-modules  specialisation  systemd
#   bin                    dry-activate        firmware            initrd                  kernel-params   sw
ln -sfn "$systemConfig" /run/booted-system

# Run any user-specified commands.
/nix/store/p6k7xp1lsfmbdd731mlglrdj2d66mr82-bash-5.2p37/bin/bash /nix/store/nkg88py8y33cbblps4jrxzqmca7c74jm-local-cmds

# No need to restore the stdout/stderr streams we never redirected and
# especially no need to start systemd
if [ "${IN_NIXOS_SYSTEMD_STAGE1:-}" != true ]; then
    # Reset the logging file descriptors.
    exec 1>&$logOutFd 2>&$logErrFd
    exec {logOutFd}>&- {logErrFd}>&-

    # Start systemd in a clean environment.
    echo "starting systemd..."
    exec /run/current-system/systemd/lib/systemd/systemd "$@"
fi


