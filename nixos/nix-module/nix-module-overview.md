## nix-module 的使用

- nix 文件分為以下兩種類型，`可以求值的nix文件` 和 `專門返回配置屬性集的nix-module`，
  注意，nix-module 是nix文件的子集，是包含特定屬性的特殊nix文件

  - 類型1，可以求值的`nix文件`
    
    nix 是基於函數式的，任何表達式都必須能返回無副作用的值

    nix文件通常包含可以返回值的`表達式 或 函數 或 變量定義`，
    無論是哪一種，最終的目的，是對nix文件進行評估(evaluate)後，最終一定可以獲取返回值

  - 類型2，專門返回配置屬性集的`nix-module`

    nix-module `也是其中一種可以用於求值的nix文件`，
    只是nix-module返回的值，<font color=blue>是專門用於配置的屬性集</font>，是一種特殊形式的nix文件
    
    因此，nix-module 返回的值需要具備某些用於配置的屬性，例如，imports屬性 或 options屬性 或 config屬性，

    因此，nix-module <font color=red>必須是用於配置的nix文件</font>，才能被稱為nix-module

- nix-module 內建屬性和功能

  - 功能，`導入外部配置選項`的`imports屬性或import屬性`
    - 用於匯入外部的nix-module，將外部nix-module提供的配置選項，匯入到當前nix-module中

  - 功能，`建立配置選項`的`options屬性`
    - 用於建立`新的配置選項`，配置選項會自動成為config屬性的子屬性，使得config屬性中可以使用新建立的配置選項
    - 新配置選項也可以用於自動設置其他的配置(config屬性)，
      例如，建立 sshEnable 的選項，當使用者將 sshEnable 設置為 true，就會自動添加 ssh 的相關配置屬性

  - 功能，`設置配置選項值`的`config屬性`
    - options屬性只是用於建立配置屬性，並對配置屬性進行限制，例如，屬性值的類型、屬性值的預設值 ... 等，
      config屬性才是設置配置屬性值的地方
    - 根據imports屬性匯入的配置選項 + 和optional屬性建立的新配置選項，形成最後公開且可供他人使用的`實際配置選項`
    - 使用者根據 nix-module 提供的選項進行客製化設置，設置後特定的配置就會自動生效
    - 透過 nix-module 可以實現配置的組合和重用，使用者透過配置參數選項，
      對應的實際配置屬性就會自動生效，實現簡化配置的目的

  - 功能，僅在內部使用的`其他私有屬性`，私有屬性僅在當前nix-module中使用，不會出現在config屬性中

- 特性，module中的屬性具有`合併屬性的性質`，此特性造就了以下的限制，詳見，[評估module的過程](#導入模塊的過程--evalmodules-的使用)

- imports屬性、options屬性、config屬性的使用場景，三者`都是選配不是必需的`
  - 情景，若用於`設置配置選項`，但`配置選項來自其他nix module`，就會使用到imports屬性

  - 情景，若用於`設置配置選項`，配置選項來自其他nix module`，
    且配置選項還需要提供給其他人使用，就會使用到imports屬性和config屬性

  - 情景，若`只是建立配置選項`，且該nix-module一定會被導入時，可以只使用options屬性

  - 情景，若用於`建立特定配置屬性`，且`配置選項一部分來自其他nix-module，一部分需要建立新配置選項`，
    就會使用到 imports屬性、options屬性、config屬性

  - 情景，若當前模塊會`配置選項來自其他nix module`

## [特性] 模組系統的輸入參數自動注入

- 實現輸入參數自動注入的方法
  - 方法，透過 pkgs.lib.evalModules()的specialArgs屬性
  - 方法，透過 imports屬性
  - 方法，nixpkgs.lib.nixosSystem函數的specialArgs參數

  - 注意，`imports屬性`和`nixpkgs.lib.nixosSystem函數`實際上都會調用pkgs.lib.evalModules()的specialArgs屬性，
    是pkgs.lib.evalModules()的specialArgs屬性得重新包裝

  - 方法，透過 _module.args
  - 參考，[傳遞非默認參數到模塊中](https://nixos-and-flakes.thiscute.world/zh/nixos-with-flakes/nixos-flake-and-module-system#pass-non-default-parameters-to-submodules)

- pkgs.lib.evalModules()的`specialArgs屬性`，實現`模組間的輸入參數共享`，和`輸入參數的自動注入`，
  
  在specialArgs中定義的屬性，在每一個模組的輸入參數都可以直接使用，例如，

  ```
  pkgs.lib.evalModules {
    modules = [
      
      # 每一個模組的輸入參數，都可直接使用specialArgs屬性中定義的 customValue

      # 模組1
      { customValue, ... }: {
          ...
      };
      
      # 模組2
      { customValue, ... }: {
          ...
      };        
    ];

    specialArgs = {
      customValue = "Hello, NixOS!";
    };
  };
  ```

- 用戶在specialArgs中`自定義的共享屬性`，都會傳遞給modules屬性中，所有的nix-modules的輸入參數

- 除了用戶自定義的共享屬性外，以下為`specialArgs內建子屬性`，是`不需要宣告也會直接傳遞給所有的nix-modules`
  - specialArgs.`config屬性` : 已解析的系統配置樹，供模組讀取當前系統的完整配置狀態
  - specialArgs.`options屬性` : 所有 NixOS 可用的配置選項及其結構
  - specialArgs.`system屬性` : 當前系統的架構，例如，x86_64-linux
  - specialArgs.`configDir屬性` : 指向當前 NixOS 配置檔案所在的目錄，用於引用配置相關的外部檔案
  - specialArgs.`pkgs屬性` : 與當前系統對應的 Nixpkgs 軟體包集合 (`pkgs = import <nixpkgs> {}`)
  - specialArgs.`lib屬性` : 指向 pkgs.lib，為 Nixpkgs 的標準函數庫，供處理資料結構和邏輯操作使用 (`lib = pkgs.lib`)
  - specialArgs.`modulesPath屬性` : 指向 Nixpkgs 的 nixos/modules  (`modulesPath = pkgs.nixos.modules`) 
  - [specialArgs源碼](https://github.com/NixOS/nixpkgs/blob/68bd364831a89859b40d67bf94618e6163486cae/lib/modules.nix#L176)

- imports屬性實現的輸入參數自動注入

  - imports屬性的內容，會被展開並附加到 evalModules 的 modules 列表中，例如，

    ```
    # main.nix
    { pkgs, lib, ... }:

    {
      imports = [
        ./networking.nix
        ./users.nix
      ];
    }

    # 相當於

    pkgs.lib.evalModules {
      modules = [
        { pkgs, lib, ... }: { imports = [ ./networking.nix ./users.nix ]; }
        ./networking.nix
        ./users.nix
      ];
    }
    ```

  - imports屬性沒有specialArgs屬性，但是很容易實現共享輸入參數

    ```
    { pkgs, lib, ... }:
    let
      customValue = "Wrapped value!";
    in 
      {
        imports = [
          (import ./foo.nix { inherit customValue; })
          (import ./bar.nix { inherit customValue; })
        ];
      }
    ```


## nix-module 結構的兩種寫法

- 兩種結構: 屬性集合型和函數型

  用於配置的nix-module，具有`根據選項`來配置其他屬性的能力，
  因此，通常需要透過函數型的nix-module，將外部的屬性集合傳入函數後才能使用

- `函數型`，需要輸入參數，可導入外部的屬性集合

  - 基本範例
    ```
    { config, pkgs, ... }:
    {
      imports = [ ... 內容省略 ... ];
      options = { ... 內容省略 ... };
      config = { ... 內容省略 ... };
    }
    ```

  - 推薦的函數型nix-module寫法
    
    雖然函數型nix-module在作為模組使用時，會自動注入特定的輸入參數，但nix-module仍然可以當作一般的nix-expr使用
    
    為避免nix-module作為一般的nix-expr使用時沒有自動注入而造成錯誤，透過以下方式可以預防此類型錯誤

    ```
    { pkgs ? <nixpkgs> {}, ...}:
    {
      imports = [ ... 內容省略 ... ];
      options = { ... 內容省略 ... };
      config = { ... 內容省略 ... };
    }
    ```

- `屬性集合型`，不需要傳入輸入參數，透過導入外部的nix-module，可直接使用已定義的選項
    
  - 限制，`必須使用imports導入其它nix-module提供的配置選項`，才可直接使用`已定義的配置選項`

    不能直接在頂層直接定義新配置選項，在頂層設置的屬性`僅僅是內部的私有屬性`，而不是配置選項，
    <font color=blue>只有在config下的屬性才能算是配置選項</font>

    若沒有使用imports導入其它模塊的選項，直接建立config屬性的子屬性就會報錯
      
    config屬性下的子屬性是配置選項，必須是經過options屬性定義的，可以是以下兩種來源
    - 來源，在 imports屬性導入的 nix-module 中，透過options屬性定義過的舊屬性
    - 來源，當前 nix-module 的options屬性定義的新屬性

    若 config屬性下的子屬性沒有經過options屬性定義過，就無法使用

  - 此方法定義的 nix-module，除了import屬性外，所有頂層的屬性都是已經在匯入的nix-module中定義過的選項，因此默認是都是config屬性的子屬性

  - 範例，基本語法
    ```
    {
      imports = [ ./myModule.nix ];
      
      # 直接使用 ./myModule.nix 中定義的選項，
      aa.bb = cc

      # 選用，建立新的參數選
      options = ... ;

      config = ...;
    }
    ```
  
  - 範例，錯誤的寫法
    - 沒有透過 import 導入已定義的選項
    - 沒有透過 option 建立新選項
    ```
    { 
      # config選項的定義
      foo = 123
    }
    ```

  - 範例，正確的寫法
    ```
    { 
      imports = [
        ./myModule.nix
      ];
      
      # config選項的定義
      # foo 必須是在 ./myModule.nix 中，有在options屬性下建立的選項
      foo.bar = 123
    }
    ```

  - 範例，透過 evalModules 驗證錯誤的寫法
    - 執行 `$ nix-instantiate --eval --strict -A config simple.nix`
    - 會得到 `error: The option foo does not exist` 的錯誤
    ```
    #simple.nix

    let
      lib = import <nixpkgs/lib>;
    in
      with lib;
      evalModules {
        modules = [
          # 透過屬性集合建立簡單的模組
          { foo.beta = 123;}
        ];
      }
    ```

  - 範例，透過 evalModules 驗證正確的寫法
    - 執行 `$ nix-instantiate --eval --strict -A config default.nix`
    - 會得到 `{foo = {alpha="hello"; beta=123}}` 的結果
    ```
    # default.nix
    let
      lib = import <nixpkgs/lib>;
    in
      with lib;
      evalModules {
        
        # 寫在 modules 屬性下的模組都會匯入，具有類似 imports 的效果
        modules = [
          # ---- 建立第1個模組 ----
          ( {lib, ...}:{
            options.foo = lib.mkOption {
              type = lib.types.anything;
            };
            
            config.foo = {
              alpha = "hello";
              #baz = lib.mkDefault "hello";
            };
          })
          
          # ---- 建立第2個模組 ----
          # foo 是第1個模組建立的選項，才可以在第2個模組中使用
          { foo.beta = 123;}
        ];
      }
    ```

## import 命令 : 用於匯入nix-expr文件的屬性集合，或對nix-expr文件進行求值

- import 是內置函數，用於導入nix-expr文件和評估(需要手動傳遞參數)

- `import`命令的後方若不是指定檔案，而是指定目錄時，自動返回該目錄下的 `default.nix` 內容

- import 命令用於相對路徑時，例如，`import ./hm/ttt.nix`，其中，`.`代表當前檔案的路徑
  
  例如，在 `/root/test/default.nix`的檔案中，使用到 `import ./hm/ttt.nix` 的語句

  此時，`import ./hm/ttt.nix`對應的實際路徑為`/root/test/hm/ttt.nix`

- import 的行為差異
  - 狀況1，`import <nixpkgs>`
    - 此命令會執行 nixpkgs/default.nix，根據源碼，default.nix 又會指向 nixpkgs/pkgs/top-level/impure.nix，該檔案的內容是函數
    - `import <nixpkgs> {}`，會將 {} 傳遞給 impurt.nix，並得到返回的屬性集

  - 狀況2，`import <nixpkgs/lib>`
    - 此命令會執行 nixpkgs/lib/default.nix，根據源碼，default.nix 返回 lib 屬性集

  - 狀況3，`import /path/to/flake.nix`，返回屬性集，即返回 flake.nix 的所有內容，將 flake.nix 當作一般nix-expr處理

  - 若不確定時，可以在 nix repl 中，透過`:t`查看

- 範例，import命令範例

  ```
  {
    let
      # config為中間屬性
      config = import /etc/nixos/configuration.nix;
    in
    {
      # 透過 config 來調用導入的內容
      boot.loader.grub.enable = config.boot.loader.grub.enable;
    }
  }
  ```

- 範例，import 的檔案若是函數，可以import時立刻傳遞輸入參數，以`獲取值`而不是獲取函數

  ```
  #fn.nix
  {a, b}:a+b
  ```

  - `nix-instantiate --eval --expr --strict 'import ./fn.nix {a=1; b=2;}'`，得到結果為 3

  - `nix-instantiate --eval --expr --strict 'import ./fn.nix'`，得到結果為 `<LAMBDA>`

## [內建屬性] 用於導入其他 nix-module 的 imports屬性

- 注意，imports列表 和 import命令的區別

  - 比較

    |                  | imports屬性，專門用於 nix-module          | import命令                                           |
    | ---------------- | ----------------------------------------- | ---------------------------------------------------- |
    | 範例             | `imports = [./aa.nix ./bb.nix ./cc.nix]`  | `import module.nix`                                  |
    | 導入檔案的限制   | 必須是有提供參數選項的 nix-module         | 沒有限制，任何 nix 表達式的文件皆可                  |
    | 額外輸入參數傳遞 | 使用 specialArgs                          | 使用 _module.args                                    |
    | 使用場景         | 和 evalModules() 搭配使用，用於添加子模組 | 將匯入檔案的內容，添加到當前的環境，或賦值給指定變數 |

    - import 導入或評估nix-expr文件
    - imports 屬性的檔案列表中，預期檔案類型是含有options屬性和config屬性的nix-module，
    - 若 imports 屬性的檔案列表，是nix-expr文件，而不是nix-Module
      - 若nix-expr文件返回屬性集 : 將屬性集的內容合併到當前環境的config屬性中，`Nix會將這些鍵值對視為config的一部分`
      - 若nix-expr文件返回函數或其他非屬性集的值 : 報錯，`imports期望的內容是模塊或屬性集`，因此無法處理函數、列表或其他類型的數據

  - 範例，imports列表範例

    注意，imports屬性用於模組系統中指定子模組的路徑，evalModules()會加載子模組的內容並進行後續處理
    ```
    {
      imports = [
        ./myModule.nix
      ];

      # options.services.othermodule.enable 定義在 ./myModule.nix 檔案中，
      # 不需要透過 config.services.othermodule.enable 調用
      services.othermodule.enable = true;
    }
    ```

  - 範例，imports屬性用於一般的模組(沒有使用evalModules解析模組)，不會去加載子模組的內容

    若imports屬性用於一般的模組(沒有使用evalModules解析模組)，
    解析nix表達式時，imports屬性就是一個單純的屬性，不會去加載子模組的內容

    imports 屬性僅用於描述模組之間的依賴關係，使用 nix-instantiate 或 nix repl 解析nix表達式時，
    並不會自動引入和求值這些模組，如果要使 imports 發揮作用，應該要使用 evalModules()或是其他類似的命令來評估模組

    ```
    #ma.nix
    {aa="aa"}

    #mb.nix
    {bb="bb"}

    #main.nix
    {
      imports = [ ./ma.nix ./ma.nix];
    }
    ```
    
    透過 `$ nix-instantiate --eval --strict ./main.nix` 
    - 得到的結果是 { imports = [ /root/ma.nix /root/ma.nix]; }
    - 而不是 { aa="aa"; bb="bb"; }

- 透過 `modulesPath` 指定 nix-module 的位置

  在 nix-module 中可以傳入 modulesPath 的輸入參數，並透過 modulesPath 來指定 nix-module 的實際位置，

  modulesPath 會讀取系統的 NIX_PATH 環境變數的內容，作為 modulesPath 的值

  例如，

  ```
  { config, pkgs, lib, modulesPath, ... }:
  imports = [
    (modulesPath + "/profiles/qemu-guest.nix")
    (modulesPath + "/virtualisation/digital-ocean-init.nix")
  ];
  ```

  注意，若 NIX_PATH 為空，會導致 `undefined variable 'modulesPath'` 的錯誤發生，
  要使用modulesPath需要確保 NIX_PATH 可用，
  參考，[NIX_PATH](nix-language.md#語法-檢索路徑表達式檔案名)

## [內建屬性] 用於建立選項的options屬性，和用輸出配置的config屬性

- 透過 `options屬性` 建構新選項的方式
  
  - 方法，透過`nixpkgs/lib`提供的函數
    - mkOption(): 用於建立任何類型的選項
    - mkEnableOption(): 用於建立 true/false 的選項，是mkOption()的語法糖，內部會調用 mkOption()
    - mkPackageOption()
    - 更多方法可參考，[nixpkgs/lib/options.nix](https://github.com/NixOS/nixpkgs/blob/master/lib/options.nix)

    以 mkOption() 為例

    ```nix-module
    {lib, ...}:
    {
      options.mySwitch.enable = lib.mkOption {
        type = lib.types.bool;
        default = false;
        description = "option for mySwitch feature";
      }
    }
    ```

  - 方法，直接利用`屬性集合`建立Option，不需要依賴外部的函數

    ```nix-module
    {lib, ...}:
    {
      options.mySwitch.enable = {
        type = lib.types.bool;
        default = false;
        description = "option for mySwitch feature";
      }
    }
    ```

- `options屬性`和`config屬性`的關係

  <font color=blue>options屬性定義選項，config屬性定義選項的值</font>

  在同一個模組中，options屬性中建立的新選項，不能直接存取，必須透過 config屬性，
  透過options屬性建立的選項，在解析時module時，會自動掛載到config屬性下

  例如，建立 options.myFeature.enable，則 config.myFeature.enable 就會是有效的

  在下面的範例中，雖然只有建立 options.aa 的屬性，但在evalModules()評估完成後，可透過 config.aa 存取，無法透過 options.aa 存取，
  
  在評估完 module 後，options.aa 就無法直接存取，必須透過 options.aa.value 存取
  ```nix-repl
  repl> lib = import <nixpkgs/lib>
  repl> ma = { options = {aa = lib.mkOption { type=lib.types.str; default="hello"; }; }; }
  repl> ( lib.evalModules { modules = [ ma ]; } ).config.aa
  "hello"
  ```

  因此，在config屬性中，若需要存取options屬性中新定義的選項，
  就需要將 nix-module 寫成函數型，並在輸入參數中指定config。
  
  以上述為例，需要在輸入參數中定義 config，在 config屬性才能使用 config.myFeature.enable，
  config.myFeature.enable == options.myFeature.enable

  <font color=red>注意</font> (config輸入參數)和(config屬性)的不同
  
  - `config輸入參數`，是模塊系統對所有模塊惰性評估後的結果，包含
    - 所有在evalModules.modules列表中指定的模塊
    - 所有模塊自身的 imports 
    - 因此，config輸入參數是一個全局上下文，包含了所有模塊的options值
  
  - `config屬性`，
    - 每個模塊自己的config屬性，是模塊自身的配置信息
    - 用來公開該模塊的選項值，以供模塊系統進行評估

## 模組中的 self 輸入參數

- nix-module 自身的引用，可用於存取模組中的屬性

## 導入模塊的過程 + evalModules() 的使用

- 用於導入外部 nix-module 的 imports屬性 VS evalModules()

  nixpkgs.lib.evalModules() 可以模擬和執行 imports 屬性導入外部模塊的情況，兩者的結果是一致的，
  
  evalModules()`允許在nix表達式中直接評估模塊`，這樣可以在不直接加載 NixOS 系統配置的情況下檢查和驗證模塊的行為。

  evalModules() 同樣會自動處理模塊之間的依賴和合併

- 透過 evalModules() 開發和評估 nix-modules

  - 工具，透過 `nixpkgs.lib.evalModules()`，可以執行(評估) `nix-modules 的options屬性和config屬性`
    
    - evalModules() <a id="eval-module">評估options屬性和config屬性的過程</a>

      <img src="doc/evalModules-flow.png" width=80% height=auto>

      - step，查看所有imports語句，遞歸收集所有模組
      
      - step，收集所有模組的`所有option屬性下的選項聲明`，並對屬性值進行`類型檢查`與`屬性合併`
        - 若屬性值是一個集合，例如，[] 或是 屬性集合，nixos 會進行合併
      
      - step，(惰性執行) 對於每個選項，透過從所有模組收集其所有定義，
        並根據選項類型將它們合併在一起來評估它，`產生最終的 config 屬性`

    - 合併屬性範例，注意，以下是偽代碼，不保證能執行
      ```nix-repl
      $ nix repl '<nixpkgs/lib>'
      nix-repl> m1 = { foo = [ "hello" ]; }
      nix-repl> m2 = { foo = [ "world" ]; }
      nix-repl> (evalModules { modules = [ m1 m2 ]; }).config.foo
      [ "world" "hello" ]
      ```

    - 對於`輸入參數`的處理
      
      對於公共屬性不需要手動傳遞給modules列表中指定的 nix-module，例如，config輸入參數，
      若不是公共屬性，可以透過 specialArgs 屬性傳遞給 evalModules()，再由evalModules傳遞給需要的 nix-module
      
      - [specialArgs簡易範例](#specialArgs-example)
      - [specialArgs複雜範例](https://nixos.asia/en/nix-modules)

    - 透過`modules屬性`指定要評估的 module
      
      evalModules() 的modules屬性是一個列表，列表中的每個元素是`指向module的路徑`，
      或是`符合module規範的表達式`，例如，

      ```
      pkgs.lib.evalModules {
        modules = [
          ({ config, ... }: { config._module.args = { inherit pkgs; }; }) 
          ./default.nix
        ];
      }
      ```

  - 範例，透過 `nix repl` 執行 evalModules()

    <font color=red> 不推薦此方法，需要手動輸入nix表達式</font>

    <font color=blue>default.nix</font>
    ```
    { lib, ... }: {

      options = {
        scripts.output = lib.mkOption {
          type = lib.types.str;
        };
      };

      config = {
        scripts.output = "hello";
      };
    }
    ```

    <font color=blue>在nix repl中</font>
    ```
    repl> pkgs = import <nixpkgs> { config={}; overlays=[]; }
    repl> result = pkgs.lib.evalModules {
            modules = [ ./default.nix ];
          }
    repl> :p result.config.scripts.output
    ```

    注意，透過`nix repl`測試evalModules()的結果時，只能讀取module的結果，不能對module的內容進行設置
    
    例如，不能執行 `result.config.scripts.output = "aa"` 的語句

  - 範例，透過 `nix-instantiate` 執行 evalModules()
    
    <font color=blue>default.nix</font>
    ```
    { lib, ... }: {

      options = {
        scripts.output = lib.mkOption {
          type = lib.types.str;
        };
      };

      config = {
        scripts.output = "hello";
      };
    }
    ```

    <font color=blue>調用 nix-module 的 eval.nix</font>
    ```
    let
      pkgs = import <nixpkgs> { config={}; overlays=[]; };
    in
      pkgs.lib.evalModules {
        modules = [ ./default.nix ];
      }
    ```

    <font color=blue>透過 nix-instantiate 執行</font>

    > nix-instantiate --eval eval.nix -A config.scripts.output
    
    其中，
    - `--eval eval.nix`，表示要立刻載入並評估eval.nix的內容
    - `-A config.scripts.output`，表示要存取config.scripts.output屬性的值

  - 範例，<a id="specialArgs-example">透過specialArgs屬性，設置module建立的選項</a>

    <font color=blue>編寫 nix-module，example.nix</font>

    ```
    { lib, config, pkgs, ... }:
    {
      options = {
        foo.enable = lib.mkOption {
          default = false;
          type = lib.types.bool;
          description = "Enable/disable foo";
        };
      };

      config =
        let
          cfg = config.foo;
        in
        {
          foo.enable = cfg.enable;
        };
    }
    ```

    <font color=blue>透過 evalModules 評估 nix-module，main.nix</font>

    ```
    let
      pkgs = import <nixpkgs> { config={}; overlays=[]; };
    in
      pkgs.lib.evalModules {
        modules = [ ./example.nix ];
        
        # 將 config.foo.enable 設置為 true
        specialArgs = { config = { foo = { enable = true; }; }; };
      }
    ```

    <font color=blue>透過 nix-instantiate 執行</font>

    > nix-instantiate --eval main.nix -A config.foo.enable
    
    其中，
    - `--eval main.nix`，表示要立刻載入並評估main.nix的內容
    - `-A config.foo.enable`，表示要存取config.scripts.output屬性的值

- evalModules() 的使用限制

  - `限制`，所有的屬性必須放在 options屬性 或 config屬性 下
    
    evalModules() 主要用於處理 nix-module 中的options屬性和config屬性，
    任何不在options屬性或config屬性下的屬性都會報錯
    
    錯誤使用範例
    ```
    lib = import <nixpkgs/lib>
    
    # foo 屬性不能放在頂層，必須掛載在 options 或 config 屬性下
    ma = { foo = "bar"; options = {aa = lib.mkOption { type=lib.types.str; default="hello"; }; }; }
    
    ( lib.evalModules { modules = [ ma ]; } ).config.aa
    ```

  - `限制`，要掛載在config下的子屬性，不能直接寫在config屬性中，必須寫在 options屬性下

    options屬性用於建立選項，config屬性用於建立選項的值，
    子屬性直接寫在config屬性，相當於是只有值沒有選項

    錯誤使用範例
    ```
    lib = import <nixpkgs/lib>
    
    # foo 屬性不能直接寫在 foo 屬性下，必須寫在options屬性下
    ma = { config = {foo = "bar";}; }
    
    ( lib.evalModules { modules = [ ma ]; } ).config
    ```

- 執行 imports 語句後，被加載nix-module中的哪些屬性會被公開，那些屬性會被保留
  - options屬性的內容加載合併和公開，有同名子屬性發生衝突時會報錯
  - config屬性的內容加載合併和公開，有同名子屬性發生衝突時，當前文件的屬性值會覆蓋加載的內容
  - meta屬性
  - nix-module中定義的私有變量、內部邏輯、函數，不會被加載

- 什麼時候需要透過 config 存取子屬性
  - 條件1，在 nix module 中，子屬性掛載在 config 屬性下
  - 條件2，透過 evalModules() 評估上述的 nix module

## 同名配置選項的覆蓋問題

- `鏈式加載`的同名配置選項
  - 考慮 nix-module 的加載鏈為 main.nix <-- module.nix <-- foo.nix
  - 同名屬性會被調用者覆蓋
  - [完整範例](module-examples/attribute-conflic-chain.nix)

- `平行加載`的同名配置選項
  - 考慮 nix-module 的加載鏈為
    - foo.nix 定義選項
    - moduleA.nix 和 moduleB.nix 都加載 foo.nix 並設置 fooOption 的選項
    - main.nix 透過 imports 同時加載 moduleA.nix、moduleB.nix
  
  - 對於同名的配置選項被不同模組賦值，且被 imports 屬性同時加載時，會發生衝突和報錯
    - 使用 lib.mkMerge 解決
  
  - [完整範例](module-examples/attribute-conflic-parallel.nix)

## 各種比較

- 比較，(解析nix-module) 和 (解析nix文件)的差異

  - `相同`，兩者的內容都是 nix-expression，都可以寫成函數型或是屬性集合型
  
  - `差異`，內建屬性的差異

    配置用的nix-module具有特殊的imports屬性、options屬性、config屬性，
    當nix-module作為module使用時，這三種屬性需要特別處理

  - `差異`，調用方式的差異

    nix-module中的函數不能像一般函數一樣的調用，必須透過特殊方式加載

    - 評估nix-expression的命令，例如，`nix repl`、`nix-instantiate`、`nix eval`、
      對於nix-module的特殊的屬性(imports、options)不會特別的處理，因此無法正確加載 nix-module，
      一般函數的調用，只是單純的對nix-expression解析和求值，`對屬性值沒有限制`，也`沒有屬性合併或檢查`等操作
    
    - 加載 nix-module，需要能模擬module-system的命令或函數，才能正確處理用於模組的特殊屬性，例如
      - pkgs.lib.evalModules()
      - nixos-rebuild命令
      - home-manager命令

    - evalModules()會模擬真正的模塊系統，對所有收集到的模塊進行類檢查、屬性合併、求值，<font color=red>且值必須是包含特定屬性的屬性集合</font>

  - `差異`，輸入參數的差異
    
    一般函數的輸入參數是需要`手動傳遞`; 模組函數的輸入參數是模塊系統`自動帶入`的，其中，

    - 全局的輸入參數可以不用手動傳遞
    - 額外的輸入參數可以透過specialArgs屬性傳遞

- 比較，`nixpkgs.lib.evalModules()` 和`imports屬性`的區別
  
  - 相同，兩者都是用於載入nix-module，並對其中的配置函數求值，最終獲取配置屬性集

  - evalModules()
    - evalModules() 是函數，用於`動態`地將模組內的 Nix 表達式求值，
      可以在運行時進行修改和擴展，會再調用 evalModules() 進行屬性合併

  - imports屬性
    - imports屬性是nix-module內建的屬性，用於`靜態`地將其他模組引入到當前配置，
    - 加載後，會將載入模組的內容合併到當前配置中，在運行時無法修改

- 比較，解析(eval)命令和建構(build)命令的差異
  - 解析是評估nix表達式的結果，若nix表達式有實際的檔案產出，就會生成建構用的drv中繼檔，
    例如，nix-instantiate

  - 建構是根據drv中繼檔的內容產生實際輸出，
    例如，nix-build

## 範例集

- 範例，在nix-module中定義簡單函數並直接調用
  
  ```
  # default.nix
  let
    fn = {a, b} : a + b
  in 
    fn {a=10; b=3;}
  ```

  `$ nix-instantiate --eval --strict ./default.nix` 或 `$ nix-instantiate --eval --strict`

- 範例，不推薦的模組寫法 : 不在頂層模組修改子模組的配置

  <font color=blue>不推薦的寫法</font>
  ```
  # ==== moduleA.nix =====
  { config, lib, pkgs, ... }:

  {
    options.services.a = {
      enable = lib.mkEnableOption "service a";
    };

    config = lib.mkIf config.services.a.enable {
      # 模塊 a 的實現
    };
  }

  # ==== moduleB.nix =====
  { config, lib, pkgs, ... }:

  { 
    # 在 moduleB 中導入 moduleA
    imports = [ ./services/a.nix ]; 

    options.b = {
      enable = lib.mkEnableOption "service b";
    };

    config = lib.mkIf config.b.enable {
      
      # 注意，不要在子模塊(moduleB.nix)中，修改其他子模塊(moduleA.nix)的配置
      services.a.enable = true;  # 不要這麼做
      
      # ... 其他配置
    };
  }
  ```

  <font color=blue>推薦的寫法，只在頂層的模組修改子模組的配置</font>
  ```
  # ==== moduleA.nix =====
  { config, lib, pkgs, ... }:

  {
    options.services.a = {
      enable = lib.mkEnableOption "service a";
    };

    config = lib.mkIf config.services.a.enable {
      # 模塊 a 的實現
    };
  }

  # ==== moduleB.nix =====
  { config, lib, pkgs, ... }:

  {
    options.services.b = {
      enable = lib.mkEnableOption "service b";
    };

    config = lib.mkIf config.services.b.enable {
      # 模塊 a 的實現
    };
  }

  # ==== 頂層模塊，main.nix ====
  { config, lib, pkgs, ... }:

  {
    imports = [
        ./a.nix
        ./b.nix
    ];  # 導入模塊 a 和 b

    services.a.enable = true;  # 在系統配置中啟用模塊 a
    b.enable = true;  # 在系統配置中啟用模塊 b
  }
  ```

- 範例，[產生軟連結檔案的範例](functions/functions.md#pkgslinkfarm用於建立軟連結的檔案)

- 範例，利用複雜範例講解常用屬性 : submodule、strMatching、either|enum、

  https://nix.dev/tutorials/module-system/module-system.html#further-styling

## ref

- [nixos modules @ nixos wiki](https://nixos.wiki/wiki/NixOS_modules)

- [模塊系統](https://nixos-cn.org/tutorials/module-system/intro.html#%E6%A8%A1%E5%9D%97%E7%9A%84%E5%B7%A5%E4%BD%9C%E5%8E%9F%E7%90%86)

- [Module system deep dive @ nix.dev](https://nix.dev/tutorials/module-system/module-system.html)

- [Introduction to module system](https://nixos.asia/en/nix-modules)

- [源碼，module中可用的函數](https://github.com/NixOS/nixpkgs/blob/master/lib/modules.nix)

- debug 相關
  - [debugging and tracing NixOS modules](https://www.youtube.com/watch?v=aLy8id4wr-M)
  - [lib.evalModules 的使用](https://nix.dev/tutorials/module-system/module-system.html#evaluating-modules)

- imports屬性相關
  - [import list 和 import 的差異](https://discourse.nixos.org/t/import-list-in-configuration-nix-vs-import-function/11372)

- 其他 nix-module 範例
  - [summer-of-nix-modules](https://github.com/tweag/summer-of-nix-modules/blob/master/default.nix)

- 處理 conflict 的問題
  - [What does mkDefault do exactly](https://discourse.nixos.org/t/what-does-mkdefault-do-exactly/9028)
  - [deal with conflict](https://youtu.be/bV3hfalcSKs?si=klx2WguSZ2TGkOjl&t=1180)

