# 平行加載下的同名屬性衝突

# =======================

# foo.nix

{ lib ? import <nixpkgs/lib>, ...}:
{
  options = {
    # 建立選項
    fooOption = lib.mkOption {
      type = lib.types.str;
      default = "default-foo";
    };
  };
}

# =======================

# moduleA.nix

{
  imports = [
    ./foo.nix
  ];

  config = {
    fooOption = "moduleA-foo";
  };
}

# =======================

# moduleB.nix

{
  imports = [
    ./foo.nix
  ];

  config = {
    fooOption = "moduleB-foo";
  };
}

# =======================

# main.nix

{ lib ? import <nixpkgs/lib> }:
{
  result = lib.evalModules {
    modules = [
      ./moduleA.nix
      ./moduleB.nix
    ];
  }
}

# =======================

# 在 nix-repl 中進行測試
nix-repl> :l ./main.nix
nix-repl> result.config.fooOption   <enter> # 報錯