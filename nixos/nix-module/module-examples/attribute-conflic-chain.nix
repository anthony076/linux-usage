# 鏈式加載下的同名屬性衝突
.
├── foo.nix     # 提供 option
├── module.nix  # 加載 foo.nix，並修改 option
└── main.nix    # 加載 module.nix，並修改 option

# =======================

# foo.nix

{ lib ? import <nixpkgs/lib>, ...}:
{
  options = {
    # 建立選項
    fooOption = lib.mkOption {
      type = lib.types.str;
      default = "default-foo";
    };
  };
}

# =======================

# module.nix

{
  imports = [
    ./foo.nix  # 引入 foo.nix 中的選項
  ];

  config = {
    fooOption = "module-foo";  # 在 module.nix 中設置 fooOption
  };
}

# =======================

# main.nix

{ pkgs ? import <nixpkgs> {}, ... }:
{
  imports = [
    ./module.nix
  ];

  # 在 main.nix 中修改 fooOption 的值
  config = {
    fooOption = "main-foo";  # 覆蓋 module.nix 中的 fooOption
  };
}

# =======================

# 在 nix-repl 中進行測試

# 方法1，透過中間函數
main = import ./main.nix {}
nix-repl> main.config
{ fooOption = "main-foo"; }

# 方法2，透過 :f
nix-repl> :l ./main.nix
Added 2 variables.

nix-repl> config.fooOption
"main-foo"
