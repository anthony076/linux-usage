## 常用命令

- 命令分類，
  - 預設基於`default.nix`的舊的命令，`nix-命令名`

  - 預設基於`flake.nix`的新命令，`nix 命令名`

  - 依照建構過程進行分類

    | step1，解析表達式                      | step2，實例化derivation | step3，建構實際輸出        | step4，執行 |
    | -------------------------------------- | ----------------------- | -------------------------- | ----------- |
    | `nix-instantiate --eval` 或 `nix eval` | `nix-instantiate`       | `nix-build` 或 `nix build` | `nix run`   |

  
  - 使用 default.nix 作為配置文件的命令或指令
    - import 指令
    - nix-shell 命令 (優先使用 shell.nix > default.nix)

  - 使用 flake.nix 作為配置文件的命令或指令
    - nix shell 命令 
    - nix develop 命令

  - 使用到 shell.nix 作為配置文件的命令或指令
    - nix-shell  (優先使用 shell.nix > default.nix)

- 命令總覽
  
  - `nix-channel 命令`: 管理軟體源列表

  - `nix-build 命令`: 用於從drv檔建構實際輸出，但不進入shell環境，需要 flake.nix

  - `nixos-rebuild 命令`: 套用系統配置變更 + 部署遠程的nixos系統 + 安全測試變更
  
  - `nix-collect-garbage 命令`: 垃圾回收指令，用於清理/nix/store中未被使用的Store Objects

  - `nix repl 命令`: 用於執行nix-language的互動式介面，可以用來對nix表達是求值或是編寫nix配置文件的測試腳本
  
  - `nix run 命令`: 建立臨時的shell環境並執行指定命令，但不進入shell環境，直接將輸出結果打印在當前shell中，需要 flake.nix

  - `nix show-derivation 命令`: 查看 drv 檔案的內容

  - 比較，幾種執行命令的方式
  
    | 命令        | 建立新shell環境 | 執行命令  | 停留在shell中 | 輸出結果到當前的shell | 用途                                 |
    | ----------- | --------------- | --------- | ------------- | --------------------- | ------------------------------------ |
    | nix-shell   | V               | 手動/自動 | V             | X                     | 建立獨立的shell環境                  |
    | nix-develop | V               | 手動/自動 | V             | X                     | 搭配 flake.nix 使用                  |
    | nix eval    | V               | 自動      | X             | V                     | 對nix表達式求值                      |
    | nix repl    | V               | 手動      | V             | X                     | 互動式對nix表達式求值                |
    | nix run     | V               | V         | X             | V                     | 快速執行一般命令(隱含執行 nix build) |

  - 比較，可執行 nix-expression的方法
    - nix-eval，專門用於nix-expression求值，需要 flake.nix 一起使用
    - nix-instantiate，解析nix-expression，並`實例化各種產物`，包含 derivate | 表達式的值 | 配置文件
    - nix-shell，
      - `建立隔離環境`，可對nix-expression求值，或執行其他命令，
      - 提供基礎工具包: bash、gcc、binutils、
      - 預設執行 default.nix，若要執行額外檔案，可透過 `$ nix-shell 檔案名.nix` 執行
    - nix repl，互動式對nix-expression求值
    - 注意，只有nix-eval是專門用於求值，其他命令可以用於求值但不限於只能求值，其他命令提供了不同場景下的更多功能

  - 套件管理相關  
    - `nix-env 命令`: 操作或查詢使用者環境
      - 使用者環境是使用者在某個時間點可用的軟體包集，是nix-store中可用程式的綜合視圖
      - 使用者環境可能有很多：不同的使用者可以有不同的環境，個別使用者可以在不同的環境之間切換

    - `nix profile 命令`: 基於使用者的套件管理
    
    - 比較，
      - nix-env 除了套件外還包含整個使用環境，nix profile偏重於套件本身
      - nix profile 更適合於需要管理多個版本的包或需要進行版本回滾的場景，速度快，管理套件更方便
    
    - 限制，nix-env 和 nix profile 不能同時使用
      - 一旦使用了 nix profile，除非刪除`$ XDG_STATE_HOME/nix/profiles/profile`後，才能再使用 nix-env
      - `$ rm -rf "${XDG_STATE_HOME-$HOME/.local/state}/nix/profiles/profile"`
      - 注意，上述命令同時會刪除已經安裝的套件

  - 啟動 shell 環境相關
    - `nix-shell 命令`: 根據nix表達式，建立隔離的shell環境
      - 可以基於nix配置文件，建立虛擬且臨時的 shell 環境結束後該環境不會存在
      - 用於和主要的 shell 環境進行隔離

      例如，`nix-shell -p libjpeg openjdk`，會建立一個虛擬的shell環境，並在該環境中安裝 libjpeg 和 openjdk 兩個套件，
      這些套件僅在虛擬的shell環境有效，該環境結束後，這些套件不會存在於系統中

      nix-shell 會自動解析並安裝指定套件的所有依賴，使得套件的安裝和使用變得更加簡單

      nix-shell 提供一些特定的環境變量和設定選項，可以根據需要自定義 shell 的行為，
      例如，--pure 選項可以啟動一個幾乎清空的環境，只保留了 HOME、USER 和 DISPLAY

      nix-shell 提供了一種隔離的環境使得開發和測試更加方便

    - `nix develop 命令`: 新版的 nix-shell，需要搭配 flake.nix 一起使用，更多地聚焦於軟體開發

  - nix store 相關
    - `nix-store 命令`: 用於管理套件的衍生物
    - `nix path-info 命令`: 用於查詢 store path 的相關資訊

- [查詢可用的命令或套件](https://search.nixos.org/packages)

- [利用nix repl查詢源碼](#repl-e)

## [概念] derivation 的回收機制

- 當 derivation 在完成建構後，會被保存在 /nix/store 的特定路徑中
  - 若有軟連結指向 derivation 的路徑，則該 derivation 是有 root 的，軟連結就是derivation的root
  - nix-build 命令，預設會自動添加 derivation-root-link，
  - nix-instantiate 命令，預設不會自動添加 derivation-root-link，但可以手動透過 `--add-root` 添加

- 透過 `nix-collect-garbage` 和 `nix-store --gc` 會檢查 derivation 是否有 derivation-root-link，
  只有沒有 derivation-root-link 的 derivation 才會被刪除

## [技巧] 調試命令執行後，都進行了那些操作

在任何命令的後方添加，--debug 的命令參數

## [命令] nix-channel，管理軟體源列表

- 常用的軟體源
  - channel網址的格式，`https://nixos.org/channels/<channel名>`，channel名可透過[此處](https://channels.nixos.org/)查詢
  
  - 列表1，nixos系統的軟體源(穩定版本)，包含系統內核、系統服務、系統工具、系統安裝ISO
    > https://nixos.org/channels/nixos-版本號1.版本號2

  - 列表2，nixos系統的軟體源(最新版本)
    > https://nixos.org/channels/nixos-unstable
  
  - 列表3，類似AUR的軟體源，
    > https://nixos.org/channels/nixpkgs-unstable

- 第一次安裝 nixos
  - 需要至少設置一個軟體源，且`該軟體源的別名，必須是 nixos`，且必須指向nixos系統的軟體源，該軟體源用於更新系統
    > nix-channel --add https://nixos.org/channels/nixos-版本號1.版本號2 nixos

  - 添加類似AUR的常用軟體源
    > nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs
  
  - 更新頻道列表
    > nix-channel --update

- 基本概念
  - 從 channel 下載的軟體包都放在 /nix/store/ 中
  - 安裝套件，實際上是將函式庫、執行檔連結到 ~/.nix-profile 中，反之，移除套件則是反過來將連結移除

- 常用參數
  - `--add 網址 別名` : 添加 source-list
  - `--remove 別名` : 刪除指定的 source-list

- 範例，常見使用範例
  - 列出所有已添加的 channel，`$ nix-channel --list`
  - 從 channel 下載最新的二進制，`$ nix-channel --update`
  - 添加 sourcce-list，`$ nix-channel --add https://nixos.org/releases/nixpkgs/channels/nixpkgs-unstable 別名`
  - 刪除 sourcce-list，`nix-channel --remove 別名`

## [命令] nix-env，操作或查詢使用者的系統環境

- 注意，不推薦使用 nix-env 尋找和管理套件，速度非常緩慢，建議使用 nix-profile 管理套件
  - nix-env 緩慢的原因，[參考](#命令-nix-search從快取中搜尋套件)
  - nix-env 在查詢套件時，若不指定屬性名稱(-A)，就會評估所有頂層套件並檢查是否匹配，
    此方法在 vanilla Nix 中沒有緩存或數據庫，因此每次查詢都需要重新評估所有套件，而導致查詢速度慢
  - nix-env 的某些操作會使用大量記憶體，例如 nix-env -qa 可能會使用大約 500M 的 RES 記憶體
  - 建議使用 nix search 快速查詢套件，

- nix-env 安裝套件的流程
  - 從 binary server 下載依賴和軟件包到`/nix/store`中
  - 根據當前的 profile(/nix/var/nix/profiles/per-user/$username/profile)，和安裝或卸載的套件包的情況，
    生成一個新的 profile 目錄，存放到 `/nix/store/$hash-user-environment`
  - 創建一個軟鏈`/nix/var/nix/profiles/per-user/$username/profile-$序號-link`指向上一步的 profile目錄
  - 修改軟鏈`/nix/var/nix/profiles/per-user/$username/profile`指向`profile-$序號-link`

- 常用命令
  - 列出可用的套件，`$ nix-env --query --available`或`$ nix-env -qa`

  - 尋找已安裝的套件，`$ nix-env -q 套件名`

  - 安裝新套件，`$ nix-env --install --attr nixos.nano`或`$ nix-env -iA nixos.nano`
    
    注意，套件名，nixos.nano，可在 [nixos-search](https://search.nixos.org/packages) 找到

- 範例，安裝套件範例，`$ nix-env -p /nix/var/nix/profiles/system -f '<nixpkgs/nixos>' -I nixos-config=/etc/nixos/configuration.nix -iA system`

  - 概念，[安裝套件的定義檔，和一般配置文件的差異](concept.md#特性-安裝套件的定義檔和一般配置文件的差異)

  - 安裝套件，`-f '<nixpkgs/nixos>'`，指定nix表達式文件的路徑，該文件包含了 system 套件的定義，
    nix-env 會從這個文件中讀取 system 套件的定義檔，並根據這個定義檔來安裝或更新 system 套件

  - 安裝系統，`-I nixos-config=/etc/nixos/configuration.nix`，指定了 NixOS 配置文件的路徑，
    此文件包含系統的配置，例如啟用哪些服務、安裝哪些軟件包等，代表 nix-env 會根據這個配置文件來建立新的系統環境

  - 定義作用範圍，`-p /nix/var/nix/profiles/system`，指定要操作的用戶環境的路徑，在此例子中，指向系統環境，這意味著這個命令將影響整個系統的配置

  - 指定要安裝的套件名，`-iA system`，指定要安裝的套件

  - 總的來說

    -iA 指定的要安裝的套件，-f 指定的安裝套件的定義檔位置，-I 指定了nixos配置文件的位置，-p 定義了新環境影響的範圍是整個系統，透過上述的命令，會先到 nixpkgs/nixos 尋找 system套件的定義檔並根據定義檔建構並安裝後，根據configuration.nix建立新的系統環境，並將新的環境作用於整個系統

- 範例，系統快照相關
  - 列出所以系統快照，`$ nix-env --list-generations`
  - 激活指定系統快照，`$ nix-env --switch-generation 4`
  - 刪除指定系統快照，`$ nix-env --delete-generations 5`

- 範例，回滾系統
  - 回滾到上一個版本，`$ nix-env --rollback --profile /nix/var/nix/profiles/system`進行回滾，
    回滾後需要重新開機，`$ sudo reboot`

  - 回滾到指定版本，`$ nix-env --switch-generation 2 --profile /nix/var/nix/profiles/system`進行回滾，
    回滾後需要重新開機，`$ sudo reboot`

## [命令] nix profile，基於用戶的套件管理

- 參考，[nix profile](https://nixos.org/manual/nix/stable/command-ref/new-cli/nix3-profile.html)的使用

- 觀念，什麼是 profile
  - profile 是基於使用者的目錄，該目錄存放指向套件實際路徑的軟鏈結

  - 每次安裝新套件時，會建立新的profile目錄，並更新更新當前使用者的`~/.nix-profile`，
    使其指向新的目錄

  - 每個使用者都擁有自己獨立的profile，以及自己的一組已安裝軟體包，
    透過`$ ls -l ~/.nix-profile`可以查看當前帳號的 profile 路徑

  - 預設位置
    - 對於`系統`，profile 一般位於 `/nix/var/nix/profiles/system`
    - 對於 `root 用戶`，profile 一般位於 `$NIX_STATE_DIR/profiles/per-user/root`
    - 對於 `一般 用戶`，profile 一般位於 `$XDG_STATE_HOME/nix/profiles`

- 常用命令

  - 安裝套件，`$ nix profile install nixpkgs#套件名`，
    安裝後的套件實際會儲存在 `/nix/store/` 中，但是會在 `~/.nix-profile/bin` 中建立軟連結
  
  - 列出已安裝的套件，`$ nix profile list`
  
  - 升級套件
    - 升級指定套件，`$ nix profile upgrade legacyPackages.x86_64-linux.nano`，
      需要指定Flake attribute名
    
    - 升級指定套件，
      - `$ nix profile list`，查看套件的 index值
      - `$ nix profile upgrade index值`

    - 升級所有套件，`$ nix profile upgrade '.*'`
    
    - 回滾套件
      - 查看版本，`$ nix profile history`
      - 回到上一個版本，`$ nix profile rollback`
      - 回到指定版本，`$ nix profile rollback --to 版本號`

  - 安裝不同版本的軟體到指定的 profile 目錄中，
    `$ nix profile install --profile 新profile目錄 nixpkgs#套件名`

    - 例如，`$ nix profile install --profile ~/ttt nixpkgs#nano`，
      將 nano 安裝到 ~/ttt 的目錄中
  
  - 刪除套件，`$ nix profile remove 套件名`
  
  - 刪除不需要的 profile 目錄中
    - 刪除 profile 目錄，`$ rm -rf profile目錄`
    - 清除，`$ nix store gc`，
    - 當執行上述命令後，profile 目錄裡面的軟連結會被刪除，
      透過 nix store gc 會將沒有軟連結的套件刪除
    - 刪除指定時間的profile，`$ nix profile wipe-history --older-than 30d`
    
## [命令] nix search，從快取中搜尋套件

- 原理

  <font color=blue>為什麼 nix-env 這麼慢</font>

  Nix 使用簡單的、解釋性的、純函數式語言來描述套件依賴圖和 NixOS 系統配置。
  
  因此，為了獲得有關這些事情的任何信息，Nix 首先需要評估一個實質性的 Nix 程序。
  這涉及解析潛在的數千個 .nix 檔案並運行圖靈完備的語言。

  例如，`$ command time nix-env -qa | wc -l`，顯示 Nixpkgs 中可用的軟體包
  
  對第一次運行的命令，`$ command time nix-shell --command 'exit 0'`，也會需要花費較多的時間

  同樣的命令在第二次運行後，因為有 cache 的存在，才能加速不少

  nix evalue 的時間和下載時間、建構時間無關，取決於輸出是否有出現在 nix store 過，
  若有出現在 nix store 過，nix 就不會進行下載和建構，但是仍然會重新進行評估

  <font color=blue>nix search 使用 cache 來取代 nix-env -qa </font>
  
  nix search 使用臨時緩衝來儲存 package 的 metadata，但仍然會有快取失效的問題，
  
  發生此問題時，可以手動透過 --update-cache 強制重新緩存

- 限制，目前只能用於搜尋 nixpkgs 中的包

- 範例，搜尋nixpkgs中所有可用的套件(output)，`$ nix search nixpkgs`
- 範例，搜尋nixpkgs中的指定套件，`$ nix search nixpkgs#vim`
- 範例，搜尋nixpkgs中的指定關鍵詞，`$ nix search nixpkgs vim`
- 範例，搜尋nixpkgs中的多個關鍵詞，`$ nix search nixpkgs vim|firefox`
- 範例，搜尋nixpkgs中的指定關鍵詞，並排除特定關鍵詞，
  `$ nix search nixpkgs vim --exclude 'gui|ai'`
- 範例，列出當前目錄中的所有包，`$ nix search. ^`

## [命令] nix-shell，根據nix表達式，建立隔離的shell環境

- 特點，
  - 提供 pkg.stdenv 的環境，會自動安裝常用的套件

  - nix-shell 不是容器，容器使用的套件在退出後會消失，
    但nix-shell 建立的套件會消失，但任何`修改的配置`或`建立的文件`會被`保留`

  - 使用 nix 文件指名需要安裝的套件和其他環境設置，
    但提供簡易模式，使用`-p 套件名`時，nix-shell 自動生成一個臨時的配置文件

- 配置文件 : 優先找 shell.nix，若無，找 default.nix

- 限制，部分參數需要 default.nix

  <font color=blue>用於建立shell的default.nix</font>
  ```
  { pkgs? import <nixpkgs> {} }:

  pkgs.mkShell {
    buildInputs = [ ];
  }
  ```

- nix-shell的命令行參數
  - 語法，`nix-shell [參數] 配置文件路徑`
    - 若沒有指定配置文件路徑，預設尋找當前目錄的 shell.nix 或 default.nix
    - 配置文件路徑沒有檔名或附檔名的限制
  
  - `--pure`，使用最乾淨的 bash 環境
    
    預設會建立 pkgs.stdenv 環境，該環境會自動添加 gcc 套件，建立速度較慢

    或使用 stdenvNoCC
    ```
    { pkgs ? import <nixpkgs> {} }:

    pkgs.stdenvNoCC.mkDerivation {
      name = "shell";
      nativeBuildInputs = [
      ];
    }
    ```

  - `-I 覆蓋NIX_PATH變數中的設定值`

    ```
    root@nixos ~/mkAlpine# echo $NIX_PATH
    /root/.nix-defexpr/channels nixos-config=/root/.config/nix-config/nixos/configuration.nix nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos
    ```

    從上述命令，可以得到在$NIX_PATH中，`nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos`
    透過 `nix-shell -I nixpkgs=12345`，可以將 $NIX_PATH 中的 nixpkgs 變數，由 /nix/var/nix/profiles/per-user/root/channels/nixos 臨時變更為 12345

    被-I添加的路徑不是環境變量，而是$NIX_PATH中的設定值，比 NIX_PATH 具有更高的優先權，但只是臨時性的，不會永久變更

    例如，`nix-shell -I nixpkgs=channel:nixos-21.05 --packages nixUnstable`

    例如，`nix-shell -I nixpkgs=https://github.com/NixOS/nixpkgs/archive/refs/tags/23.05.tar.gz -p hello`

    [指定pkgs的版本](how-to/how-to-specific-source.md#指定pkgs的版本)
  
  - `-p|--packages 套件1 套件2`，將指定的套件添加到建立的 shell 中，且`不需要 shell.nix 或 default.nix`，
    預設會使用簡易模式，自動產生臨時的配置文件

  - `--command 命令`，在進入interactive-shell後，自動執行的命令
    - 命令執行後會直接離開 interactive-shell，`nix-shell --command "echo hello"`
    - 命令執行後停留在 interactive-shell，
      
      在命令中添加 return，例如，
      `nix-shell --command "echo hello; return"`

  - `--run 命令`，在進入none-interactive-shell後，自動執行的命令

- 和 nix-shell 搭配使用的函數，參考，[mkShell](functions/functions.md#函數-pkgsmkshell建立-shell-環境)

- 比較，[nix-shell 和 nix shell 的差異](#比較-nix-shell-和-nix-shell)

- 範例，啟動一個隔離環境，並使指定套件僅在該環境生效

  > nix-shell -p nyancat

  注意，指定套件在離開環境後失效，但仍然保留在 nix store 目錄中

- 範例，啟動一個乾淨的shell環境，只保留 HOME、USER、DISPLAY、三個環境變數

  > nix-shell --pure

- 範例，執行特定命令並退出

  > nix-shell --run "echo Hello"

- 範例，透過自定義配置文件啟動 nix-shell

  自定義nix配置文件，my-environment.nix
  ```
  { pkgs ? import <nixpkgs> {} }:

  let
  myEnv = pkgs.mkShell {
      buildInputs = [
        pkgs.libjpeg
        pkgs.openjdk
      ];
  };
  ```

  執行，`$ nix-shell my-environment.nix -A myEnv`

  上述命令等效於 `$ nix-shell -p libjpeg openjdk`

## [命令] nix shell，根據flake.nix，建立隔離的shell環境，無法設置環境
    
- 特性
  - 基於 flakes 機制的命令，套件名前需要加上`nixpkgs#套件名`來指定 Flake 的來源和包名
    - `nixpkgs`， 是flake的來源，指向 https://github.com/NixOS/nixpkgs
    - `#hello`，代表flake來源下的其中一個套件
    - 實際位置為 (hello來源)[https://github.com/NixOS/nixpkgs/tree/master/pkgs/by-name/he/hello]

  - 預設使用 flake.nix 作為配置文件

    類似 nix-shell 的配置文件 shell.nix 或 default.nix，
    nix shell 使用的配置文件為 flakes.nix，

    執行 nix shell 時，會自動尋找當前目錄的 flakes.nix

  - 只會安裝指定套件，不像 nix-shell 會包含基本工具

- 兩種建立 shell 的方式，
  - 方法，透過命令
  - 方法，透過配置文件
  - 兩種方法都一定會使用到 flake.nix ，因此，不會自動產生臨時配置文件
    - 透過命令時，flake.nix 來自網路
    - 透過透過配置文件時，當前目錄一定要有 flake.nix

- nix shell 的命令行參數
  
  - 語法，`nix shell flake源#套件名`，透過命令建立包含指定套件的shell環境
    
    以官方套件庫 nixpkgs 為例，`nix shell nixpkgs#hello`

    nix shell 會直接解析該套件的 flake 文件，因此在指定套件的時候，需要帶上完整地引用，
    例如，nix shell nixpkgs#hello，代表尋找並安裝nixpkgs庫中hello套件，

    預設會讀取hello套件中的 flake.nix

    因此，使用 nix shell 需要加上 nixpkgs# ，避免多個套件名相同但來源不同

  - 語法，`nix shell git-repo名/branch名#套件名`

    例如，`$ nix shell nixpkgs/nixos-24.05#python3`

  - 語法，`nix shell github:用戶名/git-repo名#套件名`

    例如，`$ nix shell github:foo/mypackage`

  - 注意，第一次運行 `nix shell nixpkgs#套件名` 會從頭開始解析和構建該套件所有依賴，花費時間較長

    第一次運行 nix shell nixpkgs#hello 時，Nix 需要從頭開始解析和構建所有依賴

- 和 nix shell 搭配使用的函數，參考，[mkShell的使用](functions/functions.md#pkgsmkshell建立-shell-環境)

- nix shell 解析 flake.nix 的過程，以 nixpkgs#hello 為例
  - step，檢查本地是否已經有 nixpkgs 的快取（flake.lock 文件），若有，從本地加載
  - step，若無，從 github 找到 nixpkgs 庫，下載到本地，並解析根目錄下的 flake.nix
  - step，透過 flake.nix 輸出的 outputs.packages 查找有無 hello 套件的相關訊息
    
    例如，
    ```
    outputs = { self, nixpkgs }: {
      packages.x86_64-linux = { hello = nixpkgs.hello; };
    };
    ```

  - step，查找指定套件的目錄
    
    在 outputs.packages.x86_64-linux.hello 找到指定套件後，
    
    由於所有包都是由 Nix 表達式構建的，且這些表達式位於(nixpkgs/pkgs)[https://github.com/NixOS/nixpkgs/tree/master/pkgs] 的目錄下，
    
    例如，[hello 套件的實際位置](https://github.com/NixOS/nixpkgs/tree/master/pkgs/by-name/he/hello)

  - step，依照目錄中的nix表達式來建構包

- 比較，[nix-shell 和 nix shell 的差異](#比較-nix-shell-和-nix-shell)
- 比較，[nix develop 和 nix shell 的差異](#比較-nix-shell-和-nix-develop)

- 範例，建立包含指定套件的隔離 shell 環境，`$ nix shell nixpkgs#hello`
  
- 範例，利用 flake.nix 配置檔 + nix shell 建立隔離的shell環境

  建立 flake.nix
  
  ```
  {
    description = "A simple flake that provides the hello package";

    inputs = {
      nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    };

    outputs = { self, nixpkgs }: let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
    in {
      defaultPackage.${system} = pkgs.hello;
      
      devShell.${system} = pkgs.mkShell {
        buildInputs = [ pkgs.hello ];
      };
    };
  }
  ```

  執行 `$ nix shell`

## [命令] nix develop，根據flake.nix，建立隔離的shell環境，可以設置環境

- 功能 :
  - nix shell 不像 nix-shell 會自動包含開發工具，nix develop 解決了此一困難 
  - nix develop 預設包含開發工具，因此，`比 nix shell 更適合開發者使用`，特別是需要設置復雜的開發環境
  - 比較，[nix develop 和 nix shell 的差異](#比較-nix-shell-和-nix-develop)

- 和 nix develop 搭配使用的函數，
  - [mkShell的使用](functions/functions.md#pkgsmkshell建立-shell-環境)
  - [nix develop使用範例](flake/flake-examples/mkShell-manully-call-buildPhase.nix)

## [命令] nix repl，執行nix-language的互動式介面，配置debug|查詢配置值

- 常用 :command 

  ```
  <expr>        Evaluate and print expression
  <x> = <expr>  Bind expression to variable
  :a <expr>     Add attributes from resulting set to scope (添加 attributes)
  :b <expr>     Build a derivation (建構 derivation)
  :bl <expr>    Build a derivation, creating GC roots in the working directory
  :e <expr>     Open package or function in $EDITOR
  :i <expr>     Build derivation, then install result into current profile
  :l <path>     Load Nix expression and add it to scope (加載 nix 表達式)
  :lf <ref>     Load Nix flake and add it to scope (加載 flake)
  :p <expr>     Evaluate and print expression recursively (執行 nix 表達式，立刻求值)
  :q            Exit nix-repl
  :r            Reload all files
  :sh <expr>    Build sdependencie of derivation, then start nix-shell
  :t <expr>     Describe result of evaluation
  :u <expr>     Build derivation, then start nix-shell
  :doc <expr>   Show documentation of a builtin function
  :log <expr>   Show logs for a derivation
  :te [bool]    Enable, disable or toggle showing traces for errors
  ```

- [內建常數](https://nixos.org/manual/nix/stable/language/builtin-constants)

- [內建函數](https://nixos.org/manual/nix/stable/language/builtins)

- 比較，`:l` 和 `import` 的差別 : 有沒有中間變數

  |                    | `:l 檔案`                             | `import 檔案`              | `中間變數 = import 檔案`         |
  | ------------------ | ------------------------------------- | -------------------------- | -------------------------------- |
  | 對檔案內容的限制   | 限定用於可返回`屬性集合`的nix文件     | 任意類型的nix表達式        | 任意類型的nix表達式              |
  | 對函數的處理       | 無法傳遞輸入參數，需要nix文件自行處理 | 可以傳遞函數的輸入參數     | 可以傳遞函數的輸入參數           |
  | 對檔案內容如何處理 | 將屬性集合中的屬性，`保存在當前環境`  | 直接打印求值結果，但不保存 | 將求值結果賦值給變數，供後續使用 |
  | 調用屬性集合       | 不需要中間變數，直接調用              | 無法調用                   | 透過中間變數調用                 |

  - 可透過 `<tab>鍵` 可查看當前環境下所有的屬性
  - 若`檔案路徑是目錄`，預設會找當前工作目錄下的 `default.nix`
  - 對於`:l 檔案`，若檔案內容是`函數`，
    - 處理方法，必須`在檔案中調用該函數，並加結果保存在屬性集合中`，該檔案才能被 :l 加載
    - 處理方法，對函數輸入參數進行額外處理，例如，`pkg ? import <nixpkgs> {}`
  - [import的使用](nix-module/nix-module-overview.md#import-命令--用於匯入nix-expr文件的屬性集合或對nix-expr文件進行求值)

- <a id="repl-e">利用 nix repl 的 :e 查詢源碼</a>
  
  - `例如`，查詢 lib.mkDefault
    ```
    repl> lib = import <nixpkgs/lib>
    repl> :e lib.mkDefault

    # 或者
    repl> :e (import <nixpkgs/lib>).mkDefault
    ```

  - `例如`，查詢 flake.nix 中，nixosSystem 屬性的定義 
    ```
    repl> :e (import <nixpkgs/flake.nix>).outputs
    ```
  
- 注意，在 nix 文件或 nix repl 中，函數體和冒號之間需要至少一個空白，否則該函數會被視為是字串

  - 正確寫法 : `(a: a+4) 5`
  - 錯誤寫法 : `(a:a+4) 5`，冒號後方沒有空格

- 範例，查詢表達式的值，`$ nix-repl> <nixpkgs>`，查詢檔案路徑表達式的值

- 範例，查詢某個配置的值，

  ```
  nix-repl> :l <nixpkgs/nixos>                  # 印出 Added 6 variables.
  nix-repl> config.hardware.enableAllFirmware   # 印出 false
  ```

- 範例，利用 nix文件編寫一個簡單的函數，並在 nix repl 中調用

  file.nix
  ```
  {a, b} : a + b
  ```

  在 nix repl 中
  ```
  repl> add = import ./file.nix
  repl> add {a=1;b=2;}
  ```

## [命令] nix-instantiate，將表達式實例化為衍生物或值或配置文件的屬性集後，建立產生drv檔

- 功能 : 透過 nix-expression 進行各種實例化
  - 如果 nix-expr 的內容，是一般的求值表達式，nix-instantiate 會將求值表達式的結果計算出來，
    例如，`$ nix-instantiate --eval --expr '{aa=1;bb=2;}'`

  - 如果 nix-expr 的內容，是調用會建構檔案的函數，例如，pkgs.writeShellScriptBin，
    nix-instantiate 會執行建構函數，並在/nix/store下產出 *.drv檔案

  - 注意，nix-instantiate 對建構函數求值的結果就是產出 *.drv，並不會根據 drv 的內容進一步構造檔案，
    若要構造實際檔案，需要改用 nix-build 或 nix build，才會既產出 *.drv，又構造實際檔案

- 執行 nix-instantiate 命令後，預設會尋找當前目錄的 `default.nix` ，也可以指定nix文件

- 語法
  - `nix-instantiate --eval nix文件`，載入nix文件中的表達式，但不求值
  - `nix-instantiate --eval --strict nix文件`，載入nix文件中的表達式，並立即求值
  - `nix-instantiate --eval --expr 'nix表達式'`，立刻對nix表達式求值

- 命令行參數，
  - `--add-root <link路徑>` : 執行 nix-instantiate 命令後，是否建立用於指向產出drv檔的軟連結
    - 例如，`$ nix-instantiate --add-root ./tmp`，會建立 ./tmp 的軟連結並指向 drv 檔
    - 注意，只要軟連結存在，`$ nix-collect-garbage` 或 `$ nix-store --gc` 就不會將該 drv 檔刪除，
      因此，--add-root 參數常用於drv檔避免被回收機制刪除

  - `--indirect` : 通常和 --add-root 一起使用，指示是否將軟連結設置為間接引用，間接引用的軟連結能夠在 Nix 的垃圾回收機制中被自動清理

- `範例`，透過 nix-instantiate 執行文件中的函數

  詳見，[nix文件的幾種執行方式](nix-language.md#nix文件的幾種執行方式)

- `範例`，透過 nix-instantiate 實例化配置文件，以生成nginx配置文件為例

  nginx.nix
  ```
  { serverName, port }:

  let
    nginxConfig = ''
      server {
          listen       ${toString port};
          server_name  ${serverName};

          location / {
              root   /usr/share/nginx/html;
              index  index.html index.htm;
          }
      }
    '';
  in 
  nginxConfig
  ```

  透過以下命令生成配置文件，`$ nix-instantiate --arg serverName "example.com" --arg port 8080 nginx.nix`

- `範例`，將nix文件轉換為其他格式文件輸出

  ```
  nix-instantiate --eval --xml -E '1 + 2'
  ```

- `範例`，產生 derivate 範例
  
  ```
  # 得到 derivate
  $ nix-instantiate test.nix
  /nix/store/cigxbmvy6dzix98dxxh9b6shg7ar5bvs-perl-BerkeleyDB-0.26.drv

  # 構建最終產物
  $ nix-store -r $(nix-instantiate test.nix)
  nix-store -r $(nix-instantiate test.nix)
  ```

- `範例`，測試 nix-module 範例

  > nix-instantiate --eval eval_module.nix -A config.scripts.output
  
  其中，
  - `--eval eval.nix`，表示要立刻載入並評估eval.nix的內容
  - `-A config.scripts.output`，表示要存取config.scripts.output屬性的值

  完整測試流程，見 [nix-module 的使用](nix-module/nix-module-overview.md)

## [命令] nix eval，(nix-instantiate --eval) 的替代品
  
- 以下兩個命令是等效的
  - `$ nix-instantiate --eval -A tests`
  - `$ nix eval -f default.nix tests`，或好閱讀的版本，`$ nix eval -f default.nix tests --json | jq`

- 語法，`nix eval -f <nix文件> <要評估的屬性>`

- 比較，`nix eval`與`nix-instantiate --eval`的差異
  - nix eval 預設讀取 flake.nix，需要透過 -f 參數變更
  - nix eval 預設為立即求值，nix-instantiate 需要透過 --strict 參數開啟立即求值
  - nix eval 不需要透過 -A 指定要評估的屬性
  - nix eval 有 --json 參數將輸出結果轉換為 json 格式，更容易檢視結果

## [命令] nix-build，根據drv檔產生輸出 (產生output)

- 功能，
  - nix-build 隱含了執行 nix-instantiate 的功能，先透過 nix-instantiate 產生 drv 檔，再執行建構的操作
  - 和 nix-instantiate 一樣，nix-build 執行並建構defalult.nix中的建構函數，並產生 *.drv檔案
  - 和 nix-instantiate 不同的是，產生 *.drv檔案後，
    nix-build 會根據 *.drv的檔案內容實際建構檔案，並將檔案輸出至 ./result 的目錄中

- 注意，可以先使用 nix-instantiate 測試nix文件是否會產生 drv 檔案，有產生 drv 檔案才能夠使用 nix-build 命令

- 範例，[產生軟連結檔案的範例](functions/functions.md#函數-pkgslinkfarm用於建立軟連結的檔案)

## [命令] nix build，用於執行並建構flake.nix中的建構函數，但不進入shell環境

- 範例，[使用 callPackage + nix-build 建構 shell-script](functions/functions-callPackage.md#範例集)

## [命令] nix run，臨時執行命令，但不進入shell中

## [命令] nix show-derivation，查看 drv 檔案的內容

- 語法，`$ nix show-derivation drv檔路徑`

- 範例，[產生軟連結檔案的範例](functions/functions.md#pkgslinkfarm用於建立軟連結的檔案)

## [命令] nix derivation，用於查看 *.drv 的內容

- 使用範例
  ```
  nix derivation show dev檔案路徑
  nix derivation show nixpkgs#套件名
  ```

## [命令] nix log，用於查看建構過程所有的輸出訊息

- 使用範例
  ```
  nix log /nix/store/0lg84958dzslcgsarrf9zlk9gbyh4ab3-systemd-minimal-libs-256.8.drv
  ```

## [命令] nix-store，管理 derivation

- 範例，建構 drv 檔案，`$ nix-store --realise /nix/store/p90spbh4qx6f3cibm494i857xh48xi4i-Packages.xz.drv`

- 範例，刪除不再使用的衍生物

  > nix-store --gc

  注意，`$ nix-collect-garbage` 具有同樣的效果，且 nix-collect-garbage 可以刪除不用的系統快照

- 範例，刪除指定套件

  套件路徑可透過 nix path-info 查詢
  > nix-store --delete --ignore-liveness /nix/store/4snlzcmjr4wfxn9vix2l7y2i06b9hllq-hello-world

- 範例，查看deivation的依賴樹，
  > nix-store --query --tree /nix/store/<hash>-hello-2.12.1
  
- 範例，查詢指定的derivation，`被其他derivations或generations依賴`的狀況
  > nix-store --query --referrers /nix/store/<hash>-user-environment

- 範例，查詢指定的derivation，`被profile或其他系统配置文件依賴`的狀況 (查看deivation依賴的軟連結)
  > nix-store --query --roots /nix/store/<hash>-nixos-23.11/nixos/pkgs/tools/package-management/home-manager

- 範例，列出需要被回收的derivation，
  - `$ nix-store --gc --print-dead`
  - 注意，此命令不會列出所有要回收的derivation，
    例如，$ nix-collect-garbage 此命令在執行後才會將不要的套件從系統的generation中移除，
    未執行此命令前，透過 $ nix-store --gc --print-dead 是看不出來的

- 範例，[刪除套件的查詢與操作](how-to/how-to-delete-derivation.md)

## [命令] nix path-info，查詢套件在store中的路徑

- 參考，[nix path-info](https://nixos.org/manual/nix/stable/command-ref/new-cli/nix3-path-info)的使用

- 範例，查詢(套件在store中的路徑)和(所有依賴的路徑)

  例如，執行`$ nix shell github:nixos/nixpkgs/nixpkgs-unstable#hello`，
  臨時建立了可執行hello執行檔的臨時環境後，hello 執行檔仍然會保存在 nix store 中

  透過以下命令，可查詢套件在 nix store 的實際路徑或所有依賴套件的路徑
  - `$ nix path-info nixpkgs#hello`
  - `$ nix path-info -r nixpkgs#udev`
  - `$ nix path-info -r github:lheckemann/nixpkgs/5a4f40797c98c8eb33d2e86b8eb78624a36b83ea#udev`

## [命令] nix show-config，顯示 nix.conf 的配置

- 功能
  - 用於檢查 Nix 的一些全局設置。
  - 注意，直接查詢某個特定環境變量的修改細節不適用此命令

## [命令] nixos-option，查看configuration.nix定義的配置選項和當前值

- 注意，nixos-option 並`不總是能夠正確解析所有選項的子屬性`，
  - 例如，環境設置的變數，environment.variables.EDITOR，不是由 configuration.nix 設置就看不到
  - 例如，系統運行時，在 shell 中設置的環境變量也不會被 nixos-option 捕獲

- 範例，查看 EDITOR 變量的詳細情況，
  
  執行 `$ nixos-option environment.variables`，

  得到
  ```
  Value:
  {
    EDITOR = "nano";
    GTK_A11Y = "none";
    NIXPKGS_CONFIG = "/etc/nix/nixpkgs-config.nix";
    NIX_PATH = "nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos:nixos-config=/etc/nixos/configuration.nix:/nix/var/nix/profiles/per-user/root/
  }

  Description:
  ''
    A set of environment variables used in the global environment.
    These variables will be set on shell initialisation (e.g. in /etc/profile).
    The value of each variable can be either a string or a list of
    strings.  The latter is concatenated, interspersed with colon
    characters.
  ''

  Declared by:
  [ "/nix/store/ba2hgyz4k3vcqjc8xsl6h2d7ffq95pli-nixos-23.11/nixos/nixos/modules/config/shells-environment.nix" ]

  Defined by:
  [
  "/nix/store/ba2hgyz4k3vcqjc8xsl6h2d7ffq95pli-nixos-23.11/nixos/nixos/modules/programs/ssh.nix"
  "/nix/store/ba2hgyz4k3vcqjc8xsl6h2d7ffq95pli-nixos-23.11/nixos/nixos/modules/programs/less.nix"
  "/nix/store/ba2hgyz4k3vcqjc8xsl6h2d7ffq95pli-nixos-23.11/nixos/nixos/modules/programs/environment.nix"
  "/nix/store/ba2hgyz4k3vcqjc8xsl6h2d7ffq95pli-nixos-23.11/nixos/nixos/modules/config/shells-environment.nix"
  ]
  ```

- 範例，查看config.services.openssh.hostKeys的值，`$ nixos-option services.openssh.hostKeys`

## [命令] nix-collect-garbage，手動管理快取

- 範例，刪除不用的快照與套件衍生物，`$ nix-collect-garbage`

- 範例，刪除所有未被引用的包，`$ nix-collect-garbage -d`，推薦使用

## [命令] nixos-rebuild，重新建構系統

- 功能
  - 根據新的配置文件重建系統
  - 管理 NixOS 系統狀態的各種其他任務

- 常用命令
  
  - 重新建構系統
    - `$ sudo nixos-rebuild switch`，修改 configuration.nix 後，需要執行此命令來重建系統
      
      此命令會建立當前配置的 config.system.build.toplevel derivation，
      將結果derivation添加到 /nix/var/nix/profiles 中的系統配置文件中，即在系統配置文件中創建一個新的生成

    - `$ nixos-rebuild switch --flake flake.nix#mySystem`，執行並建構 flake.nix 中特定屬性的配置函數
      - 僅執行特定的屬性，而不是重新建構整個系統
      - 若不使用 --flake，預設會尋找`configuration.nix`
      - 若使用 --flake，且使用預設的 flake.nix，可簡化為 `$ nixos-rebuild switch --flake .nix#mySystem`

    - `$ nixos-rebuild switch boot`，建置配置並將其設為預設啟動選項，但在下次重新啟動之前不要啟動它，
      代表新的配置在下次重啟後才會生效

    - `$ nixos-rebuild switch -I nixos-config=./configuration.nix`，若配置文件不在預設位置，需要手動指定位置
    
  - 列出所有generation紀錄
    - 方法，`$ nixos-rebuild list-generations`
    - 方法，`$ nix profile history --profile /nix/var/nix/profiles/system`
  
  - 刪除 nixos-rebuild 建立的 generation
    - 例如，刪除所有舊的紀錄，只保留新的，`$ nix-collect-garbage --delete-old`
    - 例如，刪除14天前的紀錄，`$ nix profile wipe-history --profile /nix/var/nix/profiles/system --older-than 14d`
    - 例如，刪除指定的 generation，`$ nix-env --delete-generations 序號`

  - 版本管理
    - `$ nixos-rebuild switch --rollback`，回到上一代的配置
    - `$ nixos-rebuild switch --upgrade`，在建置新配置之前，先更新root使用者的channel

## [命令] Flake registry，管理 flake 的符號註冊表

- [什麼是符號註冊表](concept.md#特性-使用標識符註冊表和url語法來簡化調用)

- 預設會從 https://github.com/NixOS/flake-registry/blob/master/flake-registry.json，獲取官方預先註冊的registry

- 常用命令
  - 列出已註冊的符號，`$ nix registry list`
  - 添加新符號/替換舊符號，`$ nix registry add`
  - 移除已註冊的符號，`$ nix registry remove`

## [命令] nix log，檢視特定套件或drvPath的建構紀錄

- 語法，`$ nix log <drvPath>`

## [比較] nix-shell 和 nix shell

|                | nix-shell                                  | nix shell                                |
| -------------- | ------------------------------------------ | ---------------------------------------- |
| 版本           | 較舊                                       | 較新                                     |
| 安裝套件       | nix-shell -p 套件名                        | nix shell nixpkgs#套件名                 |
| 預設配置文件   | shell.nix 或 default.nix                   | flake.nix                                |
| 配置文件複雜度 | 較簡單                                     | 較複雜，但是享有 version-control 的優點  |
| 基本開發工具   | 自動將基本開發工具添加到環境中，造成啟動慢 | 只會安裝指定套件和其依賴                 |
| 臨時配置文件   | 會產生臨時配置文件                         | 不會產生臨時配置文件，flake.nix 是必要的 |
| 優缺點         | 沒有版本鎖定                               | 支援版本鎖定                             |

## [比較] nix shell 和 nix develop

- 相同 : 
  - 都可以透過命令或flake.nix建構隔離的shell，並使用指定的套件
  - 兩者都可以在 flake.nix 中調用 mkShell()
  - 兩者都可以透過 pkgs.mkShell.shellHook 配置環境變數

- 相異 : 
  - `$nix shell + mkShell()`，用於建立<font color=blue>簡單的</font>shell環境
    - 只提供明確加載的包，但不包含常見的開發工具，若只加載了運行時依賴，可能缺少編譯過程需要的工具和庫
    - 不會自動加載一些通用的開發工具，例如，pkg-config、gcc、autoconf 或開發頭文件，
      需要時必須手動將它們加入buildInputs屬性中
    - mkShell.shellHook 可以自定義建立環境變數或執行命令，但不會自動設置開發相關的環境變量，
      例如，CFLAGS、LDFLAGS、PKG_CONFIG_PATH，這些變量通常對於構建和編譯過程至關重要

  - `$nix develop + mkShell()`，用於建立<font color=blue>完全的</font>開發環境
    - 自動包含一些常見的開發工具和依賴，以及需要的頭文件和庫
    - 會自動加載一些通用的開發工具，例如，pkg-config、gcc、autoconf、編譯器、鏈接器、以及其他依賴庫和相關的頭文件
    - 除了mkShell.shellHook 可以自定義建立環境變數或執行命令外，
      還會自動設置開發相關的環境變量，確保編譯、鏈接和工具開箱及用，且能夠正常運行
  
  - nix develop 指定套件不一定能直接執行
    - `$ nix shell` : 會自動將套件的路徑添加在 Path 中，
    - `$ nix develop` + flake.nix : flake.nix 中的 mkShell.packages 會自動將套件添加到 PATH 變數中
    - `$ nix develop nixpkgs#套件名` : 因為沒有 flake.nix，因此不會將套件添加到PATH 變數中
  
    - 因此，
      - `nix shell -p 套件名` 或 `nix shell + flake.nix`，兩者都可以直接執行套件

      - `$ nix develop nixpkgs#hello` 執行後，無法直接執行指定的套件
      - `$ nix develop` 執行後，可以直接執行指定的套件

- nix develop 提供的開發環境
  - 自動加載一些通用的開發工具，例如，pkg-config、gcc、autoconf 或開發頭文件
  - 安裝用戶指定的套件
  - 自動設置開發相關的環境變量，執行與開發環境相關的鉤子或設置，例如，變量的配置、掛載文件系統等

## [比較] nix-collect-garbage 和 nix-store --gc

- 相同
  - 兩者都會刪除 nix store 中未使用的物件
  - 從 GC 根（主要是配置文件和符鏈）開始，移除沒有被任何活躍代碼引用的大型物件
  - 都會移除沒有被任何活躍代碼引用的大型物件

- 相異
  - nix-store --gc 用於手動觸發垃圾收集，是一個更基本的命令，主要關注是否有活躍引用，
    但不會特別關注或刪除不相關的符鏈

  - nix-collect-garbage 是一個更高級的命令，提供更多控制選項，
    
    例如，--delete-old和--delete-older-than，可以刪除老的配置文件，
    等同於 nix-env --delete-generations 的命令

  - nix-collect-garbage 是更好的選擇，可能會刪除更多不必要的物件
    - 更積極地清理不相關的符鏈
    - 使用 --delete-older-than 選項可以刪除特定時間之前的代碼生成，包括舊版的 Nix 環境和套件
    - 包括舊版的 Nix 環境和套件
    - 未使用的依賴

## [使用範例] 查詢套件在 nix store 中的實際路徑

- 方法，透過[nix path-info](#命令-nix-path-info查詢套件在store中的路徑)

- 方法，透過第三方的 nix-locate 套件
  - 需要安裝 nix-index 和 nix-locate 套件，安裝後，先透過`$ nix-index`建立索引 (耗時長，對所有套件進行索引)
  - 建立索引後，執行`nix-locate 套件名`，例如，`$ nix-locate '/bin/hello'`

- 方法，透過which執行
  - `$ nix shell github:nixos/nixpkgs/nixpkgs-unstable#hello`
  - `$ which hello`

- 方法，透過find或其他第三方套件
  - `$ find /nix/store -type f -name 套件名`

- 方法，透過 ls 命令查詢，`$ ls /nix/store | grep 套件名`

## [使用範例] 安裝套件的幾種方式

- 方法，將套件名添加到 `/etc/nixos/configuration.nix`，
  - 通常是系統級別的軟件

- 方法，`nix profile install nixpkgs#套件名`
    - 適用於使用者級別的套件，且用於長期使用
    - 套件在離開 nix shell 後就不可用，但套件仍保存在 nix store 中
    - 不方便管理套件，需要手動查詢才知道系統中已經安裝過此套件

- 方法，`nix-shell -p 套件名`，
  - 用於臨時使用
  - 套件在離開 nix shell 後就不可用，但套件仍保存在 nix store 中
  - 不方便管理套件，需要手動查詢才知道系統中已經安裝過此套件

## [使用範例] 查詢套件(被引用的狀況 | 完整的依賴樹)

```
# 查詢套件被引用的狀況
nix-store -q --references $(nix-instantiate '<nixpkgs>' -A hello)
# 查詢套件完整的依賴樹
nix-store -q --tree $(nix-instantiate '<nixpkgs>' -A hello)
```

## [使用範例] 升級系統 | 升級 nix

- 觀念
  - 升級 nix 需要和系統一起升級

  - 升級後的系統，再回到自己的配置目錄，並執行`$ nixos-rebuild switch --flake .`，
    就可以使系統回到原始的版本，此時，nix依舊會保持在最新的版本

  - 系統的`快取`有可能造成系統升級後的版本不如預期，升級系統前先清除快取

- 升級操作

  版本1，<font color=red>同時設置nixos和nixpkgs</font>，不推薦使用

  ```
  # ===== 設置 nix-channel =====

  # 刪除舊版本的 nixos channel
  nix-channel --remove nixos

  # 添加新版本的 nixos channel 和 nixpkgs channel
  
  # 或指定特定版本的nixos
  nix-channel --add https://nixos.org/channels/nixos-unstable nixos
  
  # 或指定特定版本的nixpkgs
  # nix-channel --add https://github.com/NixOS/nixpkgs/archive/63dacb46bf939521bdc93981b4cbb7ecb58427a0.tar.gz nixpkgs
  nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs
  
  nix-channel --update

  # ===== 設置 $NIX_PATH =====

  # 添加 configuration.nix 的位址
  # nixos-rebuild switch --upgrade 需要使用 $NIX_PATH
  # 因為 configuration.nix 可以在任意位置，因此，需要透過 $NIX_PATH 指定實際位置
  # 以下範例，nixos-config 指向的 configuration.nix，不是常見的 /etc/nixos/configuration.nix
  
  # 為了修復$NIX_PATH格式錯誤，因此不需要加上 :$NIX_PATH，
  export NIX_PATH=nixos-config=$HOME/.config/nix-config/nixos/configuration.nix
  
  # 此路徑是由nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs添加
  export NIX_PATH=nixpkgs=/nix/var/nix/profiles/per-user/$USER/channels/nixpkgs:$NIX_PATH

  root@nixos ~# echo $NIX_PATH
  nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixpkgs nixos-config=/root/.config/nix-config/nixos/configuration.nix

  nixos-rebuild switch --upgrade
  ```

  版本2，<font color=red>只需要設置nixos</font>，推薦使用
  - 設置簡單
  - nixos 和 nixpkgs 都使用同一版本，減少錯誤
  ```
  # ===== 設置 nix-channel =====

  # 刪除舊版本的 nixos channel
  nix-channel --remove nixos

  # 只添加新版本的 nixos channel
  
  # 或指定特定版本的nixos
  # nix-channel --add https://nixos.org/channels/nixos-25.11 nixos
  nix-channel --add https://nixos.org/channels/nixos-unstable nixos
  
  nix-channel --update

  # ===== 設置 $NIX_PATH =====

  # 添加 configuration.nix 的位址
  # nixos-rebuild switch --upgrade 需要使用 $NIX_PATH
  # 因為 configuration.nix 可以在任意位置，因此，需要透過 $NIX_PATH 指定實際位置
  # 以下範例，nixos-config 指向的 configuration.nix，不是常見的 /etc/nixos/configuration.nix
  
  # 為了修復$NIX_PATH格式錯誤，因此不需要加上 :$NIX_PATH，
  export NIX_PATH=nixos-config=$HOME/.config/nix-config/nixos/configuration.nix
  
  # 設置為/nix/var/nix/profiles/per-user/$USER/channels/nixos，此路徑下同樣有nixpkgs
  export NIX_PATH=nixpkgs=/nix/var/nix/profiles/per-user/$USER/channels/nixos:$NIX_PATH

  root@nixos ~# echo $NIX_PATH
  nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos nixos-config=/root/.config/nix-config/nixos/configuration.nix

  nixos-rebuild switch --upgrade
  ```

- 注意，由於 system.stateVersion 的設置，系統升級後，套件依舊會保持在舊版本，
  參考，[system.stateVersion](configuration.md#configurationnix-system-屬性) 的說明

- 注意，設置`$ nix-channel --add https://nixos.org/channels/nixos-24.05 nixos`，在執行`$ nixos-rebuild switch --upgrade`，
  並不一定會將系統降級至24.05，
  
  - 透過 $ nixos-version 和 $ nix --version 可以查詢nixos升級後的版本

  - NixOS 使用快取來加速包的下載和安裝。如果快取包含更新或更舊的版本，都有可能會造成執行`$ nixos-rebuild switch --upgrade`後系統不是24.05的版本，
    升級前，可以透過`$ rm -rf ~/.cache`可以刪除快取，強制重新下載24.05的版本

  - 除了`$ nix-channel --add https://nixos.org/channels/nixos-24.05 nixos`外，
    添加的`$ nix-channel --add https://github.com/NixOS/nixpkgs/archive/<commit-hash>.tar.gz nixpkgs` 也必須是24.05版本

    可以透過 https://github.com/NixOS/nixpkgs 的 tags 查看 commit 值

- 參考，[另一個升級系統的操作範例 + 系統升級原理](https://github.com/nixos-cn/NixOS-FAQ/blob/main/answers/how-to-upgrade-nixos-version.md)

## ref

- nix commands 
  - [nix command 官方文檔](https://nixos.org/manual/nix/stable/command-ref/main-commands)

- nix repl
  - [Inspecting values with Nix REPL](https://github.com/justinwoo/nix-shorts/blob/master/posts/inspecting-values-with-repl.md)
