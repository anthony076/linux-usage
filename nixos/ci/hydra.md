## hydra 的使用

- hydar 是基於 nix 的CI框架，可以用於自動測試或自動建構套件

## [安裝] 透過配置文件安裝和設置 hydra

- step，在configuration.nix中添加以下

  ```nix

  # 若透過 vm 存取，需要手動開放 port 3000，nixos 預設僅開放需要的 port
  networking.firewall.enable = true;
  networking.firewall.allowedTCPPorts = [ 3000 ];

  # 安裝並啟用 hydra-server
  services.hydra = {
    enable = true;
    hydraURL = "http://localhost:3000";       # web-ui
    notificationSender = "hydra@localhost";   # 發送通知的信箱
    buildMachinesFiles = [ ];
    useSubstitutes = true;
    extraConfig = ''                          # 若出現 timeout 的錯誤，可以添加此配置選項
      <git-input>
        timeout = 3600
      </git-input>
    '';
  };
  ```

  執行 `$ nixos-rebuild switch`，完成後，會自動建立 hydra 的用戶

- 切換到 hydra 用戶，`$ su - hydra` 

- 透過命令建立 web-ui 的登入用戶，
  
  > hydra-create-user 用戶名 --full-name 用戶完整名稱 --email-address 用戶信箱 --password-prompt --role admin

  - `--full-name`，設置用戶完整名稱
  - `--email-address`，設置用戶信箱
  - `--password-prompt`，提示設置密碼
  - `--role`，設置用戶群組
    - admin
    - create-projects
    - restart-jobs
    - bump-to-front
    - ancel-build
    - eval-jobset

## [安裝] 在本地編譯 hydra

參考，[Building And Developing](https://github.com/NixOS/hydra?tab=readme-ov-file#building-and-developing)

- `$ git clone https://github.com/NixOS/hydra`

- `$ nix-build`，在本地編譯並建構 hydra

- `$ nix-shell`，進入編譯 hydra 的shell環境，並透過以下命令執行編譯
  - $ autoreconfPhase
  - $ configurePhase
  - $ make

## [範例] 運行 hydra 的最小範例

- 進入`http://localhost:3000/`後，利用 hydra-create-user 建立的帳戶登入

- 點擊[create a project](http://localhost:3000/create-project)
  - 在 Identifier 欄位輸入 : hello-project
  - 在 Display name 欄位輸入 : hello
  - 在 Description欄位輸入 : hello-project
  - 完成輸入後，點擊下方的 `Create project` 按鈕

- 跳轉到[Project hello-project](http://localhost:3000/project/hello-project)的頁面後，點擊 `Actions > Create jobset`，進入jobset的設置畫面

- 跳轉到 [Creating jobset in project hello-project](http://localhost:3000/project/hello-project/create-jobset)

  - 在 State 欄位選擇 : Enable
  - 勾選 Visible 欄位
  - 在 Identifier 欄位輸入 : hello-project
  - 在 Type 欄位選擇 : Legacy
  - 在 Nix expression 欄位輸入 : examples/hello.nix in hydra

    - 表示會從 inputs 中尋找 hydra 的欄位，並找到對應的 repo 網址
    - examples/hello.nix 表示
      
      要進行評估的 nix-expression 為 repo 中的 examples/hello.nix 檔案，
      因此，實際檔案位置為[github:nixos/hydra/examples/hello.nix](https://github.com/NixOS/hydra/blob/master/examples/hello.nix)

  - 在 Check interval 欄位輸入 : 60，表示每60秒檢查一次

  - 在 Scheduling shares 欄位輸入 : 1

  - 在 Input 欄位輸入

    | 鍵名    | 類型         | 鍵值                                         |
    | ------- | ------------ | -------------------------------------------- |
    | nixpkgs | Git checkout | https://github.com/NixOS/nixpkgs nixos-24.05 |
    | hydra   | Git checkout | https://github.com/nixos/hydra               |

    其中，
    - nixpkgs鍵，為 hello.nix 中使用的依賴
    - hydra鍵，用於告知 hello.nix 儲放的 repo 位址

  - 完成輸入後，點擊下方的 `Create jobset` 按鈕

- (選用) 手動觸發 jobset 

  若 jobset 沒有自動觸發，點擊[Evaluations](http://localhost:3000/jobset/hello-project/hello-project)頁面後，點擊 `Actions > Evaluate this jobset`，帶設置的時間到達後，才會開始執行

- 建構完成後，
  - 在[Evaluations](http://localhost:3000/jobset/hello-project/hello-project)的頁面中會顯示
    eval結果的連結，例如，http://localhost:3000/eval/2
  
  - 透過 Summary 中的 Actions 按鈕，可以看到建構結果在本機中的實際位置，例如，
    `nix-env -i /nix/store/ccs8597k5ji5h7ad94wfr329xcxydbla-hello-2.12.1`

## [範例] 其他範例 

- [release.nix](https://hydra.nixos.org/build/281326333/download/1/hydra/projects.html#building-jobs)

- [release.nix @ wiki](https://wiki.nixos.org/wiki/Hydra)

- [An example illustrating declarative hydra projects](https://github.com/shlevy/declarative-hydra-example)
  - 用戶透過 spec.json 進行 project、josbset、和其他必要配置的設置
  - hydra 根據 spec.json 自動建立 jobset 並執行建構

## issues 

- 出現`"error: RPC failed; curl 92 HTTP/2 stream 5 was not closed cleanly"`的錯誤

  - 原因，
    - Git 使用的 curl 可能在處理 HTTP/2 請求時出現問題
      > 強制使用http1.1，執行 `$ git config --global http.version HTTP/1.1` (有效)
    
    - Git 對大文件的處理出現問題
      > 增加緩衝區大小，`$ git config --global http.postBuffer 1048576000`

    - 代理配置不正確
      > git config --global --unset http.proxy

      > git config --global --unset https.proxy

    - 網絡連接不穩定 :  確保使用穩定的網路

## ref

- [github-hydra](https://github.com/NixOS/hydra)

- [官方文檔](https://hydra.nixos.org/build/281326333/download/1/hydra/installation.html)

- [hydra @ wiki](https://wiki.nixos.org/wiki/Hydra)

- [Known Issues](https://wiki.nixos.org/wiki/Hydra)
