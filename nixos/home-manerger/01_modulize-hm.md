## 模組化使用 home-manager

- 此方法`不會也不需要`安裝 home-manager-cli，透過`$ nixos-rebuild switch`使home.nix檔案中的配置生效

- 使用流程 + 加載 home.nix 的方法
  - `step`，添加 home-manager-channel，使`<home-manager/nixos>`語句生效

    ```nix
    nix-channel --add https://github.com/nix-community/home-manager/archive/release-24.05.tar.gz home-manager
    nix-channel --update
    ```

  - `step`，必要，在 configuration.nix中，透過`imports = [<home-manager/nixos>]`導入home-manager-module
  
  - `step`，透過 home-manager-module 加載 home.nix
    - 透過`home-manager.users.用戶名 = hm的配置函數`，加載 home.nix 的內容
    - [home.nix的使用](00_hm-overview.md#配置-home-manager-配置文件homenix-的使用)

    - 範例，
      
      <font color=blue>方法，透過 import</font>

      ```nix
      imports = [ <home-manager/nixos> ];

      home-manager.users.root = import /path/to/home.nix;
      ```

      <font color=blue>方法，不透過 import</font>

      ```
      imports = [ <home-manager/nixos> ];

      home-manager.users.ching = { pkgs, ...} : {
        home.packages = [ pkgs.hello ];
        home.stateVersion = "23.11";
      };
      ```

      以上兩種方法是等價的 home.nix 的內容就是 nix-module 的內容

  - `step`，套用變更
    - 透過`$ nixos rebuild switch`應用 home.nix 的內容
