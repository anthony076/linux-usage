## home-manager 的使用

- 使用場景

  configuration.nix 用於`系統級別`的配置，home-manage 用於`用戶級別`的配置
  - nixos預設配置文件的位置，`/etc/nixos/configuration.nix`，屬於`全局級別`的配置
  - home-manager配置文件的位置，`/etc/nixos/home.nix`，屬於`特定用戶級別`的配置

  例如，
  - 在 configuration.nix 中，`environment.systemPackages`，指定系統級別的套件，是所有使用者都可以使用的
  - 在 configuration.nix 中，`users.users.用戶名.packages`，指定用戶可以使用那些系統套件
  - 在 home.nix 中，`home.packages`，指定當前用戶可以使用那些套件
  
  home-manager 適用於特定用戶的環境設置，
  可以取代 configuration.nix 中，用於建立用戶的`users.users.用戶名.*`屬性，配置上會更為簡便

- 注意，當使用 home-manager 管理用戶環境時，建議避免在configuration.nix中
  同時使用 `users.users.<username>.packages`，有可能會導致衝突和混淆

- 多種使用方式 : 模塊化 or 獨立化 or 混合模式 or flake化

  - 相同，使用相同的home-manager配置文件，home.nix
    - 全局級別，`/etc/nixos/home.nix`
    - 用戶級別，`~/.config/home-manager/home.nix`

  - 差異，
    - 是否需要 home-manager-cli
    - 加載 home.nix 方式的不同
      - 由於掛載的屬性不同，對應返回屬性值的函數也會不同，造成加載 home.nix 方法的差異，詳見
      - [模塊化](01_modulize-hm.md)，適合 nixos 的作業系統
      - [獨立化](01_independent-hm.md)，適合非 nixos 的作業系統，
      - [混和模式 = 模塊化 + 獨立化](01_mixed-hm.md)
      - [flake化](01_flake-hm.md#使用流程--加載-homenix-的方法)
        - hm + flake 模組化
        - hm + flake 獨立化

- home-manager配置文件，[home.nix的使用](#配置-home-manager-配置文件homenix-的使用)

## home-manager-cli 的使用

- 切換到新配置，
  - 適用於 flake.nix，`$ home-manager switch --flake '.#exampleHomeConfig`
  - 適用於 ~/.config/home-manager/home.nix，`$ home-manager switch`

- 查詢`非flake版本的`home-manager 的配置選項
  > home-manager option home.username

- 移除 home-manager，
  - 方法，`$ home-manager uninstall`
    - 徹底移除 Home Manager 所做的改動，包含 Home Manager 管理的配置和環境
    - 刪除 Home Manager 創建的 symlinks 和配置文件
    - 通過 Home Manager 管理的配置和軟件包將被移除，且 Home Manager 本身也會從系統中卸載
  
  - 方法，`$ nix profile remove github:nix-community/home-manager/release-23.11#packages.x86_64-linux.default`
    - 移除 Home Manager 在 Nix profile 中的條目，但不影像配置本身
    - 用於移除通過 nix profile 安裝的Home Manager，但保留 Home Manager 管理的配置
  
  - 注意，上述兩個方法不會自動將 Home Manager 相關的內容從 Nix store 中移除

- 列出所有 generations，$ home-manager generations
- 刪除 generations，
  - $ home-manager expire-generations "2024-10-13 13:23"
  - $ home-manager remove-generations 5 6 7 8 9 10 11 12 13 14

## [配置] home-manager 配置文件，home.nix 的使用

- home.nix 必須加載或透過命令才能使用

  只有獨立型的home-manager的 home.nix，才可以直接被 home-manager-cli 讀取配置並應用

  (模組化的home-manager)和(flake化的home-manager)都需要將 home.nix 導入後才能使用
  - 模組化的home-manager 需要在 configuration.nix 中導入 home.nix
  - flake化的home-manager 需要在 flake.nix 中導入 home.nix

  導入後，需要`使屬性和特定函數互相搭配`，才能正確加載 home.nix 的內容，
  以實現用戶級別的配置

- home.nix 是一個 nix-module，和 nix-module `相同`之處
  - 和 configuration.nix 依樣，home.nix內建多個核心nix-module提供大量預設的配置選項，
    用戶只需要引用和設置這些選項即可
  
  - 預設的配置選項由 home-manager 的核心模塊提供，參考，
    [home-manager的核心模組](https://github.com/nix-community/home-manager/blob/master/modules/modules.nix)

- home.nix 和一般 nix-module 的`相異`之處

  - 不使用 `config 屬性`配置選項
    - 一般的 nix-module，例如，configuration.nix，可以透過 import 指令導入 nix-module 後，
      透過合併的 config 屬性進行配置，參考，[nix-module的使用](../nix-module/nix-module-overview.md)
    
    - home-manager 不使用 config 屬性進行配置，`config 屬性屬於全局系統性的配置`，
      home-manager 的定位在於`用戶級別的配置選項`，因此，改用 home 屬性為命名空間進行配置

  - 不使用 `import 命令`或 `pkgs.lib.evalModules()` 導入 home.nix
    - 一般的 nix-module 可以透過 import 或 pkgs.lib.evalModules() 導入nix-module，屬於全局系統性的導入

    - 模組化的 home-manager 必須使用 `home-manager.lib.homeManagerConfiguration 函數`來載入 home.nix，
      參考，[home-manager 核心模塊的加載過程](#home-manager-核心模塊的加載過程)，
      透過此函數實現用戶級別的加載，此函數負責解析用戶的配置並將其應用到用戶的環境中

    - 注意，在 home.nix 中仍然可以使用 import 語句，來加載用戶自定義的 nix-module

- 不是所有用戶相關的配置都可以使用 home-manager 進行配置
  - 不能`建立使用者`，建立用戶依然需要在configuration.nix中處理

  - 不能變更用戶預設的shell，指定shell屬於全局配置，無法透過hm變更，例如，
    - 在configuration.nix中，可以設置`users.users.用戶名yourname.shell = pkgs.zsh;`
    - 在home.nix中，無法使用`users.users.用戶名yourname.shell = pkgs.zsh;`，沒有users的選項

## [配置] home.nix 使用範例

- 建立檔案
  - 透過 home.files 建立檔案
  - home.files 預設路徑為 $HOME
  - 範例，透過`text屬性`直接寫入檔案內容
    ```
    home.file = {
      ".local/bin/hello.sh".text = ''
        #!/bin/bash 
        echo "hello"
      '';
    };
    ```
  - 範例，透過`source屬性`讀取外部檔案作為寫入檔案的來源
    ```
    home.file = {

      # 建立 $HOME/.local/bin/myhello 的檔案
      ".local/bin/myhello" = {
        source = ./scripts/myhello; # 指定外部檔案
        executable = true; # 設置檔案為可執行
      };

    };
    ```

- 設置環境變數
  - home.sessionVariables 屬性會建立 hm-session-vars.sh
  - 確保 shell 的配置文件中有加入，`source "${HOME}/.nix-profile/etc/profile.d/hm-session-vars.sh"`的語句，設置的變數才是有效的
  - home.sessionPath 會將新路徑添加到 hm-session-vars.sh 檔案中，
    是添加到 PATH 變數的語法糖
  - 範例
    ```
    home.sessionVariables = {
      EDITOR = "hx";
      FOO = "123";
    };

    # 添加新路徑到 PATH 環境變數
    # 此命令，
    home.sessionPath = [ "$HOME/.local/bin" ];
    ```

- 安裝套件
  
  - 要查詢安裝的套件版本，或指定套件版本，
    參考，[安裝系統套件](../configuration.md#configurationnix-environment-屬性)

  ```
  {
    home.packages = with pkgs; [
      just
    ];
  }
  ```

  指定套件的版本

  ```
  {
    home.packages = with pkgs; [
      (pkgs.lib.versioned.pkgsSpecificVersion "vim" "8.2.3456")
    ];
  }
  ```

## [配置] 套件配置範例

- [helix](https://gitlab.com/anthony076/nix-config/-/blob/main/home-manager/helix.nix)
- [git](sw-config-examples/config-git.md)
- [fish](sw-config-examples/config-fish.md)

## home-manager 核心模塊的加載過程

- home-manager 核心模塊提供 home-manager 預設的配置選項，參考，[hm核心模塊源碼](https://github.com/nix-community/home-manager/tree/master/modules)

- 狀況，加載模組化home-manager的核心模塊
  - step，在 configuration.nix 中，
    使用 [home-manager.lib.homeManagerConfiguration 函數](https://github.com/nix-community/home-manager/blob/master/flake.nix#L41) 
    來加載 home.nix

  - step，執行 `nixos-rebuild命令`時，home-manager.lib.homeManagerConfiguration 函數會
    [載入 home-manager/modules 中定義的核心模塊](https://github.com/nix-community/home-manager/blob/master/flake.nix#L78)

- 狀況，加載獨立化home-manager的核心模塊
  - 使用`$ home-manager switch`時，自動加載 home-manager/modules 的核心模塊，不需要額外調用 home-manager.lib.homeManagerConfiguration 函數
  - home-manager 命令會自動加載核心模塊，並讀取和應用 home.nix 中的配置

- home-manager.lib.homeManagerConfiguration 函數實際進行的操作
  - 解析配置：函數會讀取 home.nix 中的配置項
  - 模塊合併：將用戶的配置與 home-manager 的默認配置合併，確保用戶的設置可以覆蓋默認值。
  - 生成配置：創建最終的配置對象，這個對象將用於實際的環境配置。
  - 應用配置：執行相應的操作，比如安裝包、創建文件、設置環境變量等。

## 在 nix repl 中進行 debug

- 查詢 home-manager 的配置選項
  - 方法，[透過網頁查詢](https://home-manager-options.extranix.com/)

  - 方法，透過 `home-manager 命令` 查詢，
    - 未使用 flake.nix，例如，`$ home-manager option home.username`，
    - 有使用 flake.nix，例如，`$ home-manager --flake ~/.config/home-manager/flake.nix option home.username`，?? 待驗證
    - 注意，若出現出現 `Can't inspect options of a flake configuration`的錯誤，表示使用了 flake.nix
  
  - 方法，透過 `nixos-option 命令` 查詢，例如，`$ nixos-option home-manager.users`

- 查詢，透過 nix repl 中，查詢 `home.nix` 的內容
  - 注意，home.nix是一個nix-module，但home.nix中的配置選項都定義在核心模組中，
    必須透過特殊函數才能加載核心模組，無法透過 import 語句加載，詳見，[home-manager 核心模塊的加載過程](#home-manager-核心模塊的加載過程)
    
    模組化的hm與 configuration.nix 搭配使用，可以利用 configuration.nix 將home.nix的內容加載到 configuration.nix 的 config 中，
    透過 config 讀取 home.nix 的內容

  - step，模組化安裝 home-manager
  - step，準備 home.nix
    ```nix
    # ~/.config/home-manager/home.nix
    { config, pkgs, ... }:

    {
      home.username = "root";
      home.homeDirectory = "/root";
      home.stateVersion = "23.11"; # Please read the comment before changing.
      home.packages = [];
      home.file = {};
      home.sessionVariables = {
        # EDITOR = "emacs";
      };

      programs.home-manager.enable = true;
    }
    ```
  - step，修改 configuration.nix
    ```
    # /etc/nixos/configuration.nix

    { config, lib, pkgs, ... }:
    {
      imports =
        [ 
          <home-manager/nixos>
        ];
      
      # 加載hm 
      home-manager.users.root = import /root/.config/home-manager/home.nix;

    }
    ```
  - step，執行 `nixos-rebuild switch` 讓配置生效，讓 configuration.nix 加載 home.nix
  - step，進入 nix repl 後，執行以下
    ```nix
    
    # 方法1
    nix-repl> nixos = import <nixpkgs/nixos> {}
    nix-repl> config.home-manager.users.root.home.username

    # 方法2
    :load <nixpkgs/nixos>
    config.home-manager.users.root.home.username
    ```

- 查詢，透過 nix repl 查詢 [home-manager/default.nix](https://github.com/nix-community/home-manager/blob/master/default.nix)的內容

  ```
  nix-repl> pkgs = import <nixpkgs> {}
  nix-repl> hm = import <home-manager> { pkgs = pkgs; }
  nix-repl> hm.nixos {}
  ```

- 查詢，透過 nix repl 查詢 [home-manager/flake.nix](https://github.com/nix-community/home-manager/blob/master/flake.nix)的內容
  
  - 方法，透過 `:lf`，沒有中間變數

    ```
    nix-repl> :lf home-manager
    nix-repl> :e lib.homeManagerConfiguration
    ```

  - 方法，透過 `builtins.getFlake`，有中間變數
    
    ```
    nix-repl> hmFlake = builtins.getFlake "github:nix-community/home-manager"
    nix-repl> :e hmFlake.lib.homeManagerConfiguration
    nix-repl> hmFlake.defaultPackage.x86_64-linux.name
    ```

## 注意事項

- (hm的配置)和(nixos配置)應該分開

  因為(hm提供的配置選項)和(官方提供的配置選項)，兩者大部分雷同，但是不一定完全一樣

  例如，同樣是 programs.fish 
  - 在 configuration.nix `沒有 programs.fish.functions` 的配置選項
  - 在 home.nix `有 programs.fish.functions` 的配置選項

- 若當前目錄中有`.git子目錄`，必須先提交變更(git add)後，才能執行套用變更的命令
  
  例如，當前目錄中新增了 foo.nix，若沒有提交 foo.nix，
  則執行 `$ home-manager switch --flake .` 就會出現`foo.nix : No such file or directory`的錯誤

  對於有 .git 子目錄，代表當前目錄是一個 github-repo，
  home-manage 強制要求提交變更後才能執行 home-manager-cli 的命令

## ref

- [Home Manager Manual](https://nix-community.github.io/home-manager/index.xhtml)

- [Adding Modularity to your NixOS Config with home-manager](https://www.youtube.com/watch?v=bV3hfalcSKs)

- [home-manager的使用](https://jia.je/software/2022/06/07/nix-cookbook/#home-manager)

- [開始使用 home-manager](https://nixos-and-flakes.thiscute.world/nixos-with-flakes/start-using-home-manager)

- [nix and home-manager](https://zhuanlan.zhihu.com/p/638004747)

- [home-manager @ nix cookbook](https://jia.je/software/2022/06/07/nix-cookbook/#home-manager)


https://zhuanlan.zhihu.com/p/565586166