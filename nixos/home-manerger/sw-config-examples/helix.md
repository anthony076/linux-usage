## helix 的配置

- 官方提供的[lsp配置範例](https://github.com/helix-editor/helix/blob/master/languages.toml)
  
## [DAP] - lldb

- 是用語言 : c、cpp、zig、rust

- step，安裝必要套件

  ```
  home.packages = with pkgs; [
    helix
    lldb          # for lldb、lldb-dap 命令
    clang-tools   # for clangd 命令
  ];
  ```

- step，配置 languages.toml

  - 方法，手動建立`~/.config/helix/languages.toml`檔案

    建立 hm/files/language.toml 檔案

    ```
    [[language]]
    name = "c"
    scope = "source.c"
    auto-format = true
    file-types = ["c", "h"]
    language-servers = ["clangd"]

    [language-server.clangd]
    args = ["--background-index", "--clang-tidy", "--completion-style=detailed"]
    command = "clangd"

    [language.debugger]
    name = "lldb-dap"
    transport = "stdio"
    command = "lldb-dap"

    [[language.debugger.templates]]
    name = "binary"
    request = "launch"
    completion = [ { name = "binary", completion = "filename" } ]
    args = { console = "internalConsole", program = "{0}" }

    [[language.debugger.templates]]
    name = "attach"
    request = "attach"
    completion = [ "pid" ]
    args = { console = "internalConsole", pid = "{0}" }
    ```

    在配置文件中，添加以下
    ```
    # helix-LSP 的配置文件
    ".config/helix/languages.toml" = {
      source = hm/files/language.toml;
    };
    ```

    執行`home-manager switch --flake .`

  - 方法，透過 `programs.helix.languages`

    ```nix
    programs.helix.languages = {
      language = [
        { # 通用配置
          name = "*";
          scope = "*";
          file-types = [];
          indent = { 
            # 插入兩個單位的unit
            tab-width = 2;
            # 實際插入的符號為空白
            unit = " ";
          };
        }

        {  # for nix-language
          name = "nix";
          auto-format = true;
          #formatter.command = "${pkgs.nixfmt}/bin/nixfmt";
          language-servers = [ "nix" ]; # 指向 language-server.nix
        }
        
        {  # for c-language
          name = "c";
          scope = "source.c";
          file-types = ["c" "h"];
          auto-format = true;
          # 指向language-server.clangd的內容
          language-servers = [ "clangd" ];  
          
          # 指向debugger.lldb-dap的內容
          debugger = {  
            name = "lldb-dap";
            transport = "stdio";
            command = "lldb-dap"; 
            templates = [
              {
                name = "binary";
                request = "launch";
                completion = [ { name = "binary"; completion = "filename"; } ];
                args = { console = "internalConsole"; program = "{0}"; };
              }

              {
                name = "attach";
                request = "attach";
                completion = [ "pid" ];
                args = { console = "internalConsole"; pid = "{0}"; };
              }
            ];
          }; 
        }
      
      ];

      language-server.nix = { command = "${pkgs.nil}/bin/nil"; };

      language-server.clangd = {
        command = "clangd";
        args = [
          "--background-index"
          "--clang-tidy"
          "--completion-style=detailed" ];
      };
    };
    ```

    執行`home-manager switch --flake .`

- step，在 helix-ide 中使用

  建立 hello.c
  
  ```c
  #include <stdio.h>

  #define hello "123"

  int main(void) {
      printf("Hello from Nix!\n");
      printf(hello);

      return 0;
  }
  ```

  建立帶有 debug 符號的執行檔，`$ gcc -g -o hello hello.c`

  進入 helix，`$ hx hello.c`

  `<Space>鍵` : 進入選單
  - `<G>鍵` : 進入Debug頁面
  - `<b>鍵` : 建立breakpoint
  - `<l>鍵` : 執行launch-debug-target
    - 選擇 binary
    - 輸入 hello
