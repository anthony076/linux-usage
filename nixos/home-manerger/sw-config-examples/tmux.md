## 利用 home-manager 配置 tmux

- 啟用 tmux 和 tmuz-fzf 
  - sessionInit 允許你在 tmux 啟動時執行命令，這裡用來自動安裝 tmux-fzf

  - 獲取 sainnhe/tmux-fzf 的 sha256 值
    ```shell
    nix-prefetch-url https://github.com/sainnhe/tmux-fzf/archive/refs/tags/v3.6.0.tar.gz
    ```

  - 配置範例

    ```
    { config, pkgs, ... }:

    let
      # Define tmux-fzf as a custom package if it is not available in the default nixpkgs
      tmuxFzf = pkgs.fetchFromGitHub {
        owner = "sainnhe";
        repo = "tmux-fzf";
        rev = "v3.6.0";  # 確保這是你想使用的版本
        sha256 = "sha256-xxxxxx";  # 請自行更新對應的 sha256
      };
    in {
      programs.tmux = {
        enable = true;

        # Example tmux configuration
        extraConfig = ''
          set -g mouse on
          set -g prefix C-a
          bind C-a send-prefix
        '';

        # shell = "${pkgs.zsh}/bin/zsh";
        
        # If you want to set up plugins
        plugins = with pkgs; [
          tmuxPlugins.tpm # tmux plugin manager
          tmuxPlugins.yank 
          tmuxPlugins.jump
          tmuxPlugins.vim-tmux-navigator
          tmuxPlugins.tmux-thumbs
          tmuxPlugins.tmux-fzf
          tmuxPlugins.sessionist
          tmuxPlugins.pain-control
          tmuxPlugins.resurrect
          tmuxPlugins.continuum
        ];

        # Add tmux-fzf installation script
        sessionInit = ''
          # Install tmux-fzf after tmux session starts
          [ ! -d ~/.tmux/plugins/tmux-fzf ] && git clone https://github.com/sainnhe/tmux-fzf ~/.tmux/plugins/tmux-fzf
          ~/.tmux/plugins/tmux-fzf/bin/install
        '';
      };

      # 如果你也想啟用 tmux 的 home-manager 模組相關的其他設定，可以在這裡繼續配置
    }
    ```

  - 配置範例
    
    ```
    programs.tmux = {
      enable = true;
      extraConfig = ''
        # 使用 sainnhe/tmux-fzf 插件
        set -g @plugin 'sainnhe/tmux-fzf'
        
        # 設置 tmux-fzf 的排序優先級
        TMUX_FZF_ORDER="session|window|pane|command|keybinding|clipboard|process"
        
        # 設置 tmux-fzf 的預覽行為
        TMUX_FZF_PREVIEW=1
        TMUX_FZF_PREVIEW_FOLLOW=1
      '';
    }
    ```

## ref 

- [Setting Up Tmux With Nix Home Manager](https://haseebmajid.dev/posts/2023-07-10-setting-up-tmux-with-nix-home-manager/)

- [tmux-config @ ahonn](https://github.com/ahonn/dotfiles/blob/master/modules/home-manager/programs/tmux.nix)

- [hm官方配置源碼](https://github.com/nix-community/home-manager/blob/release-24.05/modules/programs/tmux.nix)
