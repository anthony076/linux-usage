## 利用 home-manager 配置 fish

- 可用子屬性
  - enable屬性 : 為fish添加配置
  - interactiveShellInit屬性 : 為 ~/.config/fish/config.fish 添加內容
  - functions屬性 : 建立執行`多行命令`的函數 (由 funcions 命令建立，可透過 funcions -a 檢視)
  - shellAbbrs屬性 : 建立執行`單行命令`的縮寫 (由 abbr 命令建立，可透過 abbr --list 檢視)
  - shellAliases屬性 : 為`命令名稱`建立縮寫 (由 alias 命令建立，可透過)
  - plugins屬性 : 安裝並使用插件

- 插件
  - [pkgs.fishPlugins.tide](https://github.com/IlanCosman/tide) : fish 樣式優化
  - [pkgs.fishPlugins.pure](https://github.com/pure-fish/pure) : fish 樣式優化，從 zsh 移植過來的

  - [pkgs.fishPlugins.sponge](https://github.com/meaningful-ooo/sponge) : 保持 fish-history 乾淨，自動清理錯誤的歷史命令
  - [pkgs.fishPlugins.puffer-fish](https://github.com/nickeb96/puffer-fish) : 自動擴展輸入過的命令 
  - [pkgs.fishPlugins.hydro](https://github.com/jorgebucaran/hydro) : 顯示 git 狀態
  - [pkgs.fishPlugins.fifc](https://github.com/gazorby/fifc) : fish 功能增加，將多個功能與fzf整合後，為fish添加更多功能

  - [pkgs.fishPlugins.fzf-fish](https://github.com/PatrickF1/fzf.fish) : 為 fish 擴充 fzf 的功能 
  - [pkgs.fishPlugins.forgit](https://github.com/wfxr/forgit) : 將 git 命令已 fzf 整合到 fish 中


  - [pkgs.fishPlugins.done](https://github.com/franciscolourenco/done) : 長程序在背景執行完成後，會在 fish 中顯示`took x s`的提示 
  - [pkgs.fishPlugins.colored-man-pages](https://github.com/PatrickF1/colored_man_pages.fish?tab=readme-ov-file) : 在 fish 中對 man 的內容彩色化
  - pkgs.fishPlugins.colored-man-pages : 各種括號的自動補全

- 配置
  - [kanagawa](https://github.com/rebelot/kanagawa.nvim/blob/master/extras/fish/kanagawa.fish) : 優化 fish 樣式

    透過以下方式添加

    ```nix
    programs.fish.interactiveShellInit = ''
      ... 內容省略 ...

      ${pkgs.lib.strings.fileContents (pkgs.fetchFromGitHub {
          owner = "rebelot";
          repo = "kanagawa.nvim";
          rev = "de7fb5f5de25ab45ec6039e33c80aeecc891dd92";
          sha256 = "sha256-f/CUR0vhMJ1sZgztmVTPvmsAgp0kjFov843Mabdzvqo=";
        }
        + "/extras/kanagawa.fish")}
    '';
    ```

- 範例，基礎配置範例
  
  ```
  programs.fish.enable = true;
  programs.fish.shellAliases = {
    a = "b";
  };
  programs.fish.shellInit = ''
    # Rust
    set -x PATH ~/.cargo/bin $PATH
  '';
  ```

- 範例

  ```
  programs.fish = {
    enable = true;

    interactiveShellInit = ''
      ${pkgs.any-nix-shell}/bin/any-nix-shell fish --info-right | source

      ${pkgs.lib.strings.fileContents (pkgs.fetchFromGitHub {
          owner = "rebelot";
          repo = "kanagawa.nvim";
          rev = "de7fb5f5de25ab45ec6039e33c80aeecc891dd92";
          sha256 = "sha256-f/CUR0vhMJ1sZgztmVTPvmsAgp0kjFov843Mabdzvqo=";
        }
        + "/extras/kanagawa.fish")}

      # for wsl-nixos
      fish_add_path --append /mnt/c/Users/ching/scoop/apps/win32yank/0.1.1

      set -U fish_greeting  # 禁止歡迎詞
    '';

    # 建立多行命令
    functions = {
      refresh = "source $HOME/.config/fish/config.fish";
      take = ''mkdir -p -- "$1" && cd -- "$1"'';
      ttake = "cd $(mktemp -d)";
      show_path = "echo $PATH | tr ' ' '\n'";
      posix-source = ''
        for i in (cat $argv)
          set arr (echo $i |tr = \n)
          set -gx $arr[1] $arr[2]
        end
      '';
      clean = ''
        nix-collect-garbage -d
        nix-store --optimise -vv

        rm -rf /tmp/*
        rm -rf /var/tmp/*
        rm -rf /var/log/*
        rm -rf ~/.cache
      '';
    };

    # 為單行命令建立縮寫
    shellAbbrs =
      {
        gc = "nix-collect-garbage --delete-old";
      }
      # navigation shortcuts
      // {
        ".." = "cd ..";
        "..." = "cd ../../";
        "...." = "cd ../../../";
        "....." = "cd ../../../../";
      }
      # git shortcuts
      // {
        gapa = "git add --patch";
        grpa = "git reset --patch";
        gst = "git status";
        gdh = "git diff HEAD";
        gp = "git push";
        gph = "git push -u origin HEAD";
        gco = "git checkout";
        gcob = "git checkout -b";
        gcm = "git checkout master";
        gcd = "git checkout develop";
        gsp = "git stash push -m";
        gsl = "git stash list";
      };

    #為命令名稱建立縮寫
    shellAliases = {
      jvim = "nvim";
      lvim = "nvim";
      pbcopy = "/mnt/c/Windows/System32/clip.exe";
      pbpaste = "/mnt/c/Windows/System32/WindowsPowerShell/v1.0/powershell.exe -command 'Get-Clipboard'";
      explorer = "/mnt/c/Windows/explorer.exe";

      # To use code as the command, uncomment the line below. Be sure to replace [my-user] with your username.
      # If your code binary is located elsewhere, adjust the path as needed.
      # code = "/mnt/c/Users/[my-user]/AppData/Local/Programs/'Microsoft VS Code'/bin/code";
    };

    plugins = [
      {
        inherit (pkgs.fishPlugins.autopair) src;
        name = "autopair";
      }
      {
        inherit (pkgs.fishPlugins.done) src;
        name = "done";
      }
      {
        inherit (pkgs.fishPlugins.sponge) src;
        name = "sponge";
      }
    ];
  };

  ```

## ref

- fish 可用選項
  - [@ configuration.nix](https://github.com/NixOS/nixpkgs/blob/nixos-unstable/nixos/modules/programs/fish.nix)
  - [@ home-manager](https://github.com/nix-community/home-manager/blob/master/modules/programs/fish.nix)
  - [@ nixos-wiki](https://nixos.wiki/wiki/Fish)
  - [@ nixos-search](https://search.nixos.org/options?channel=unstable&show=programs.fish.enable&size=50&sort=relevance&type=packages&query=fish)