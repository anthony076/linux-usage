## 在 nixos 中使用 distrobox

- 利用 distrobox 可以建立 fhs 環境
  
  distrobox是docker的第三方應用，

  distrobox 容器與主機系統無縫整合，可以直接訪問主機的文件系統、網路和裝置，
  使得在容器中執行的應用程式可以像本地應用一樣操作，
  因此，distrobox可以直接像nix-shell一樣修改本地檔案而不需要指定 volume
  
- 優，distrobox 使用上更為簡潔，通過簡單的命令即可創建、管理和銷毀容器，降低了學習曲線

- 缺，distrobox 的安全性較差
  
  因為預設可以存取家目錄、USB 裝置、X 伺服器/Wayland、SSH 設定檔、系統日誌等，因此不算完全隔離的檔案系統

## 安裝 distrobox

- 注意，distrobox 必須依賴 podman 或 docker，必須在 docker-deamon 啟動下才能正常運作，
  因此無法透過 nix-shell -p distrobox 來使用

- 在 configuration.nix 中添加以下配置

```nix

# 安裝 docker 套件
environment.systemPackages = with pkgs; [ 
  docker
  distrobox
];

# 啟用 docker-darmon
virtualisation.docker.enable = true;

# 將用戶添加到額外的 docker 群組，使該用戶能夠使用 docker 相關命令
users.users.root.extraGroups = [ "docker" ];
```

## 範例，利用 distrobox 建立fhs環境，並執行 python 應用

```shell
# 確保 docker daemon 執行中
(nixos)systemctl start docker

# 創建具有fhs環境的 alpine 容器
(nixos)distrobox create --name myalpine --init --image alpine:latest

# 進入 alpine 容器中
(nixos)distrobox enter myalpine

(alpine)apk update
(alpine)apk add uv

(alpine)uv init bbb
(alpine)cd bbb
(alpine)uv add numpy
(alpine)python main.py
```

注意，上述範例中，uv 用於創建python的虛擬環境，是選用的命令，

在alpine容器中已經是fhs的環境，也可以直接安裝和使用python，
不一定要透過 uv

## ref

- [distrobox使用教學](https://ivonblog.com/posts/distrobox-usage/)