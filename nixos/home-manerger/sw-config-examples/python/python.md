## 在 nixos 中使用 python

- 安裝 python 3.13，`$ nix profile install nixpkgs#python313` 或 `$ nix-shell -p python313`

- python-module 需要 fhs 環境

  python/pip 是依賴 FHS 環境的
  
  要安裝pip，透過`$ nix profile install nixpkgs#python313Packages.pip`，雖然可以安裝pip命令，但是透過此方法是`無法安裝 python module`，

  因為pip安裝的module若依賴其他的庫或套件時，這些庫或套件預設是基於FHS的

  例如，當執行 pip install aiohttp 時，
  會試圖對 nixos-immutable-system 的檔案系統進行變更(因為要寫入硬碟)，
  此行為是被禁止的，因為會跳出以下的訊息

  ```
  $ pip install aiohttp

  × This environment is externally managed
  ╰─> This command has been disabled as it tries to modify the immutable
      `/nix/store` filesystem.

      To use Python with Nix and nixpkgs, have a look at the online documentation:
      <https://nixos.org/manual/nixpkgs/stable/#python>.

  note: If you believe this is a mistake, please contact your Python installation or OS distribution provider. You can override this, at the risk of breaking your Python installation or OS, by passing --break-system-packages.
  ```

  例如，執行 import numpy as py 的語句時，會調用`/lib/libstdc++.so.6`，
  在 nixos 上，該檔案是保存在`/nix/store/<hash>-/lib/libstdc++-6`目錄中，此時，會因為找不到該檔案而發生錯誤

## 安裝python-module的幾種方法

- 方法，透過 shell.nix 將`依賴庫`添加到環境變數，使外部庫生效

  注意，此方法僅適合python執行時，若套件依賴外部庫時可以透過此方法，
  `將外部庫位於 /nix/store 的路徑添加到環境變數`，使外部庫生效，
  <font color=red>但此方法不適合利用pip安裝 python-module</font>

  shell.nix 會建立自定義的shell環境，在 shell.nix 中指定的套件可以手動添加到環境變數中

  ```nix
  #shell.nix

  { pkgs ? import <nixpkgs> {} } :
  pkgs.mkShell {
    packages = [
      pkgs.python3 # 或 pkgs.python3l3
    ];

    # 將第三方的庫添加到環境變數中
    env.LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath [
      pkgs.stdenv.cc.cc.lib
      pkgs.libz
    ];
  }
  ```

- 方法，透過 shell.nix 安裝 python-module (含依賴的外部庫)

  注意，此方法
  - 可以透過環境變數使外部庫生效

  - 可以直接指定需要的 python-module 而不需要透過命令安裝，
    包含 python-module 依賴的外部庫也會一併安裝和配置

  - 適合有收錄於nixpkgs中的python-module，
    若python-module未收錄於nixpkgs中時，此方法不適用

  以下透過`$ nix-shell`執行
  ```nix
  #shell.nix

  { pkgs ? import <nixpkgs> {} } :
  pkgs.mkShell {
    packages = [
      # 安裝 python3 和依賴的 python-module
      (pkgs.python3.withPackages( pypkgs: [
        pypkgs.numpy
        pypkgs.requests
        pypkgs.pandas
      ]))
    ];

    # 將第三方的庫添加到環境變數中
    env.LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath [
      pkgs.stdenv.cc.cc.lib
      pkgs.libz
    ];
  }
  ```

- 方法，shell.nix + pip2nix，適用於未被nixpkgs收錄的python-module

  利用[pip2nix](https://github.com/nix-community/pip2nix)自動產生`python-packages.nix`，
  此套件會下載和解析python-module的源碼，並轉換用於`建構python-module的nix-expressions(nix檔案)`

  注意，python-packages.nix 的內容是一個 override 函數，需要
  1. 透過 `pkgs.callPackge` 進行建構
  2. 透過 `pkgs.python3.override { inherit packageOverrides }` 添加到python的derivation實例中

  - step，產生python-packages.nix
    - 方法，指定pypi套件名，`$ nix run github:nix-community/pip2nix -- generate requests`
    - 方法，指定requirements.txt，`$ nix run github:nix-community/pip2nix -- ./requirements.txt`
    - 方法，pip-nix 提供了 shell 環境，也可以在該shell環境中建立 python-packages.txt
      
      ```shell
      $ nix shell github:nix-community/pip2nix
      pip2nix generate "requests==2.32" numpy
      ```
  
  - step，在 shell.nix 中加載 python-packages.nix

    以下透過`$ nix-shell`執行

    ```nix
    #shell.nix

    { pkgs ? import <nixpkgs> {} } :
    let
      # 建構 override 函數中的 packages
      packageOverrides = pkgs.callPackge ./python-package.nix {}

      # 添加到 python derivation 實例中，對pkgs.python3中的套件進行替換
      python = pkgs.python3.override { inherit packageOverrides }
    in
      pkgs.mkShell {
        packages = [
          (python.withPackages(pypkgs : [
            # 指定需要用到的套件
            pypkgs.requests
            pypkgs.numpy 
          ]))
        ];
      }
    ```

- 方法，使用第三方的 devenv 套件，用於創建各個語言的虛擬環境，可以設置外部庫+安裝python-module

  - 為什麼虛擬環境會有效

    因為虛擬環境不會將套件保存在 fhs 的環境，例如，
    - /usr/lib/pythonX.Y/site-packages/
    - /usr/local/lib/pythonX.Y/site-packages/ 

    而是將套件安裝在專案目錄下的 .venv 目錄中，從而繞開了fhs環境的問題

  - 簡單範例

    完整代碼，[use-devenv](use-devenv/)

    ```shell
    nix-shell -p devenv
    devenv init           # 自動產生 devenv.nix devenv.yaml .envrc .gitignore
    # 修改 devenv         # 參考 use-devenv/devenv.nix
    devenv shell         # 進入虛擬環境
    python main.py        # 參考 use-devenv/main.py
    ```

    在 devenv.nix 中，可以指定外部庫，也可以指定要使用的python-module

    ```nix
    # 提供 dev-shell 環境中使用到的套件或外部庫
    packages = [ 
      pkgs.zlib    # 提供 numpy 依賴的外部庫 libz.so
    ];

    # 提供 python 環境的設置和需要安裝的python-module
    languages.python = {
      enable = true;
      venv.enable = true;
      venv.requirements = ''
        numpy
      '';
    };
    ```


- 方法，使用虛擬環境套件，UV，取代 pip

  不推薦使用此方法，以下步驟會出現動態連結錯誤

  ```shell
  $ nix-shell -p uv
  $ uv init

  error: Querying Python at `/root/.local/share/uv/python/cpython-3.13.1-linux-x86_64-gnu/bin/python3.13` failed with exit status exit status: 127

  [stderr]
  Could not start dynamically linked executable: /root/.local/share/uv/python/cpython-3.13.1-linux-x86_64-gnu/bin/python3.13
  NixOS cannot run dynamically linked executables intended for generic
  linux environments out of the box. For more information, see:
  https://nix.dev/permalink/stub-ld
  ```

  錯誤表示 NixOS 系統嘗試運行一個需要使用動態鏈接的執行檔，由於該執行檔是針對傳統 Linux 環境（而不是 NixOS）編譯的。

  在 NixOS 上，通常會遇到動態鏈接的程式無法運行，因為 NixOS 使用了不同的方式來處理依賴關係和動態鏈接的庫。
  因此需要額外的處理才能正常運行

  uv 也需要 fhs 的環境，因此無法直接透過 nix-shell -p uv 執行，
  - 可以透過 shell.nix 建立 fhs 環境後執行
  - 可以透過第三方的distrobox執行，參考，[利用distrobox使用uv](../distrobox.md#範例利用-distrobox-建立fhs環境並執行-python-應用)

- 方法，使用 docker 容器

- 方法，使用 distrobox (第三方docker的應用，依賴docker或podman)
  
  參考，[distrobox的使用](../distrobox.md)