import numpy as np

# 建立一個 NumPy 陣列
a = np.array([1, 2, 3, 4, 5])

# 執行基本運算
b = a * 2  # 每個元素乘以 2
c = a + 10  # 每個元素加 10

# 計算統計數據
mean_value = np.mean(a)  # 平均值
sum_value = np.sum(a)  # 總和
max_value = np.max(a)  # 最大值
min_value = np.min(a)  # 最小值

# 印出結果
print("原始陣列:", a)
print("乘以 2:", b)
print("加 10:", c)
print("平均值:", mean_value)
print("總和:", sum_value)
print("最大值:", max_value)
print("最小值:", min_value)
