{ pkgs, lib, config, inputs, ... }:

{
  # https://devenv.sh/basics/
  env.GREET = "devenv";

  # 提供 dev-shell 環境中使用到的套件或外部庫
  packages = [ 
    pkgs.zlib    # 提供 numpy 依賴的外部庫 libz.so
  ];

  # 提供 python 環境的設置和需要安裝的python-module
  languages.python = {
    enable = true;
    venv.enable = true;
    venv.requirements = ''
      numpy
    '';
  };

}
