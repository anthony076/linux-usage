## 在 nixos 中使用 flatpak

- flatpak主要用於安裝 GUI 應用，對於 cli 應用一般無法直接安裝

## 安裝 flatpak

- 在 configuration.nix 的配置文件中，添加以下配置

  ```nix
  environment.systemPackages = with pkgs; [ 
    flatpak
  ];

  services.flatpak.enable = true;
  xdg.portal.enable = true;
  xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  ```

  套用變更，`$ nixos-rebuild switch`，
  重啟系統，`$ reboot`

- 初始化 flatpak

  ```shell
  # 添加 remote
  flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

  # 確認 remote 有正確被添加
  flatpak remote-list

  # 更新資料庫
  flatpak update
  ```
