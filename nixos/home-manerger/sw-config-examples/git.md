## 利用 home-manager 配置 git

- 範例

  ```
  programs.git.enable = true;
  programs.git.lfs.enable = true;
  programs.git.userName = "Someone";
  programs.git.userEmail = "mail@example.com";
  programs.git.extraConfig = {
    core = {
      quotepath = false;
    };
    pull = {
      rebase = false;
    };
  };
  programs.git.ignores = [
    ".DS_Store"
  ];
```