## flake化使用 home-manager

- 使用此版本的 home-manager，需要先[啟動flake](../commands_flake.md#啟用實驗性命令和flakes)

- 和模塊化hm|獨立化hm的概念類似，flake化的hm有兩種加載 home.nix 的方法
  - 獨立化hm+flake : 適合用於非 nixos 系統，需要 home-manager-cli
  - 模組化hm+flake : 適合用於 nixos 系統，不需要 home-manager-cli

- 比較表 : 獨立化hm+flake VS 模組化hm+flake

  |                             | 獨立化hm+flake                                  | 模組化hm+flake                               | all-in-one  |
  | --------------------------- | ----------------------------------------------- | -------------------------------------------- | ----------- |
  | 使用場景                    | 非nixos系統                                     | nixos系統                                    | 非nixos系統 |
  | 加載configuration.nix的屬性 | 不加載                                          | (e) nixosConfigurations屬性                  | 同 (e)      |
  | 加載configuration.nix的函數 | 不加載                                          | (f) nixpkgs.lib.nixosSystem()                | 同 (f)      |
  | 加載home.nix的屬性          | (a) homeConfigurations屬性                      | (b) nixpkgs.lib.nixosSystem()的modules屬性   | 同 (a)      |
  | 加載home.nix的函數          | (c) home-manager.lib.homeManagerConfiguration() | (d) home-manager.nixosModules.home-manager() | 同 (c)      |
  | 執行應用的命令              | home-manager-cli                                | nixos-rebuild-cli                            | 兩者兼具    |

- [安裝](#安裝-hm--flake-版本的-home-manager-cli)

- [加載 home.nix](#加載-homenix)

- 在 nix repl 中查看 homeManager/flake.nix 源碼
  注意，需要先安裝 home-manager

  ```
  # 測試 homeManager 是否已經下載到本機
  repl> <home-manager>

  # 加載 home-manager/flake.nix
  repl> :l <home-manager> # 錯誤用法，只會加載 home-manager/default.nix
  repl> :lf <home-manager> # 錯誤用法，加載 flake.nix 需要指定文件路徑
  repl> :lf github:nix-community/home-manager # 正確用法

  # (選項) 或加載本地的 home-manager
  # 將安裝的 home-manager 添加到 NIX_PATH 變數中
  # export NIX_PATH="home-manager=/nix/var/nix/profiles/per-user/root/home-manager:$NIX_PATH"
  repl> :lf home-manager

  # 查看和測試
  repl> lib.homeManagerConfiguration # 測試是否有此函數
  repl> :e lib.homeManagerConfiguration # 查看源碼
  repl> :e outputs.lib.homeManagerConfiguration # 查看源碼
  ```

- [範例集](#範例)

## 安裝 hm + flake 版本的 home-manager-cli

- 注意，透過以下方法產生的 `flake.nix 無法直接使用`，缺少 nixosConfigurations 屬性的設置

- 狀況1，僅產生home-nix和flake.nix，但不安裝home-manager-cli，適合 nixos 系統
  - 使用最新版，`$ nix run home-manager/master -- init`
  - 指定版本，`$ nix run home-manager/release-24.05 -- init`
  - 過程 : 
    - 尋找並解析 nix-community/home-manager/flake.nix
    - 執行 nix build 之後，建立 home-manager 執行檔，
    - 執行 home-manager 命令並傳遞參數 switch

- 狀況2，產生home-nix和flake.nix，並安裝home-manager-cli，
  - 使用最新版，`$ nix run home-manager/master -- init --switch`
  - 指定版本，`$ nix run home-manager/release-24.05 -- init --switch`
  - 上述命令會建立以下檔案
    - 執行檔，安裝 home-manager-cli 於 /root/.nix-profile/bin/home-manager
    - 配置檔，建立 home-manager 配置文件於 ~/.config/home-manager/home.nix
    - 配置檔，建立用於導入 home.nix 的 flake.nix 於 ~/.config/home-manager/flake.nix

- 狀況3，先產生home-nix和flake.nix，編輯配置檔後，安裝hom-manager-cli並執行switch
  - `$ nix run home-manager/master -- init`
  - 編輯配置檔
  - `$ nix run home-manager/master -- init --switch`，會重新生成配置檔，若配置檔已存在，會掠過生成配置檔的步驟

- 狀況4，也可以透過`nix-shell` + 官方提供的`install.nix`安裝home-manger-cli，
  [參考](01_independent-hm.md#安裝-home-manager-cli)
  
- 狀況5，只產生 flake.nix，`$ nix flake new /etc/nixos -t github:nix-community/home-manager#nixos`，
  此命令會將 flake.nix 輸出到 /etc/nixos 目錄中
  
## 使用流程 + 加載 home.nix 的方法

- `加載方法1` : 獨立使用，適合非 nixos 系統
  - 需要安裝 home-manager-cli，且不需要 configuration.nix，只需要 flake.nix 和 home.nix

  - 需要在`flake.nix`中，將 home-manager 添加到 inputs 中

    ```nix
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    ```

  - flake.nix 的`homeConfigurations.使用者`屬性為 home-manager 專用的配置屬性
  
  - homeConfigurations 的屬性值，由`home-manager.lib.homeManagerConfiguration{}函數`提供，並利用此函數加載 home.nix

    ```nix
    homeConfigurations."root" = home-manager.lib.homeManagerConfiguration {
      inherit pkgs;
      modules = [ ./home.nix ];
    };
    ```

  - 套用變更
    - 方法，`$ home-manager switch`，此命令預設讀取 ~/.config/home-manager/flake.nix

    - 方法，`$ home-manager switch --flake ~/.config/home-manager#root`，用於手動指定flake.nix的目錄

    - 錯誤方法，`$ home-manager switch --flake ~/.config/home-manager#homeConfigurations."root"`，配置引用中的鍵名不應該加引號

    - 錯誤方法，`$ home-manager switch --flake ~/.config/home-manager#homeConfigurations.root`，不需要添加 homeConfigurations屬性

    - 錯誤方法，`$ home-manager switch --flake ~/.config/home-manager#homeConfigurations.root.activationPackage`，
      此表達式在評估之後才會出現，在 home.nix 中沒有

  - 完整範例，[flake_hm_independent-basic](hm-examples/flake_hm_independent-basic.nix)

- `加載方法2` : 依賴於 nixos，適合 nixos 系統

  - 不需要安裝 home-manager-cli，但需要`configuration.nix + flake.nix + home.nix`等配置文件
    - 透過`$ nix run github:nix-community/home-manager init`產生配置文件
    - 將產生的flake.nix和home.nix複製到 /etc/nixos 的目錄中，與 configuration.nix 放在同一個目錄

  - 需要在`flake.nix`中，將 home-manager 添加到 inputs 中

    ```nix
    inputs = {
      nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
      home-manager.url = "github:nix-community/home-manager";
      home-manager.inputs.nixpkgs.follows = "nixpkgs";
    };
    ```

  - 不使用 homeConfigurations的屬性值，改用 flake 為 nixos 提供的`nixosConfigurations.用戶名`屬性，
    並利用`nixpkgs.lib.nixosSystem {} 函數`返回屬性值

  - 在 nixpkgs.lib.nixosSystem {} 函數中，
    並利用`home-manager.nixosModules.home-manager {} 函數`返回 nix-module 給 modules 屬性，並透過此函數加載 home.nix

    ```nix
    outputs = inputs@{ nixpkgs, home-manager, ... }: {
      nixosConfigurations = {
        root = nixpkgs.lib.nixosSystem {
          ...
          modules = [
            ...
            home-manager.nixosModules.home-manager
            {
              ...
              home-manager.users.root = import ./home.nix;
            }
          ];
        };
      };
    };
    ```

  - 注意，此方法利用 nixpkgs.lib.nixosSystem() 同時加載 configuration.nix 和 home.nix

  - 套用變更，透過`$ nixos-rebuild switch --flake '包含flake.nix的目錄的絕對路徑#用戶名'`

  - 完整範例，[flake_hm_module-basic](hm-examples/flake_hm_module-basic.nix)

- `加載方法3` : all-in-one，推薦，所有的配置都在一起
  - 需要 home-manager-cli 和 nixos-rebuild-cli，依照要進行的操作決定執行哪個命令

  - 需要在`flake.nix`中，將 home-manager 添加到 inputs 中

    ```nix
    inputs = {
      nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
      home-manager.url = "github:nix-community/home-manager";
      home-manager.inputs.nixpkgs.follows = "nixpkgs";
    };
    ```
  - 使用 nixosConfigurations屬性 和 nixpkgs.lib.nixosSystem() 加載 configuration.nix

  - 使用 homeConfigurations屬性 和 home-manager.lib.homeManagerConfiguration() 加載 home.nix

  - 注意，和`加載方法3`的差異
    - 加載方法3，利用 nixpkgs.lib.nixosSystem() 同時加載 configuration.nix 和 home.nix
    - 此方法加載 configuration.nix 和 home.nix是分開獨立的兩個函數
  
  - 套用變更
    - 套用 home.nix 的變更，`$ home-manager switch --flake .#root 或 home-manager switch --flake .`
    - 套用 configuration.nix 的變更，`$ nixos-rebuild switch --flake .`

  - 完整範例，[flake_hm_all-in-one](hm-examples/flake_hm_all-in-one.nix) 

- 比較，
  - 差異，在 flake.nix 中使用的屬性不同
    - 非nixos系統，使用獨立的`homeConfigurations.使用者`屬性，由home-manager提供
    - nixos系統，使用flake為nixos提供內建的`nixosConfigurations.主機名`屬性，為flake內建屬性

  - 差異，返回屬性值的函數不同
    - 非nixos系統，使用`home-manager.lib.homeManagerConfiguration {} 函數`
    - nixos系統，使用`home-manager.nixosModules.home-manager {} 函數`，返回 nix-module

  - 差異，執行命令不同，
    - 非nixos系統，使用獨立的`home-manager-cli`
    - nixos系統，使用內建的`nixos-rebuild-cli`

- [home.nix的使用](00_hm-overview.md#配置-home-manager-配置文件homenix-的使用)

## 範例

- 利用 `home-manager.lib.homeManagerConfiguration 函數`，建立 homeConfigurations屬性值
  
  - [homeManagerConfiguration函數的源碼](https://github.com/nix-community/home-manager/blob/e3ad5108f54177e6520535768ddbf1e6af54b59d/flake.nix#L41)
  
  - 輸入屬性集 (v22.10)
    - configuration屬性函數: 可以透過nix文件導入函數
    - system屬性: 指定使用平台
    - homeDirectory屬性: 指定家目錄的路徑
    - username屬性: 指定使用者姓名
    - stateVersion屬性: 指定nixos的版本，設置版本後 nixos 就不會自動升級到最新版
  
  - 輸入屬性集 (v22.11)

- 範例

  (選項) step，在 /etc/nixos/flake.nix 中，導入 /etc/nixos/home.nix

  讓 nixos-rebuild switch 时，home-manager 配置也会被自动部署
  https://dev.leiyanhui.com/nixos/3base-install-home/

  https://zhuanlan.zhihu.com/p/463469697
  https://zhuanlan.zhihu.com/p/565586166

  https://nixos-and-flakes.thiscute.world/zh/nixos-with-flakes/start-using-home-manager

- 範例，獨立化的 hm + flake，用於非 nixos 的系統

  ```nix
  # flake.nix

  inputs = {
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs, home-manager, ... }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
    in 
    {
      # homeConfigurations 是 home-manager-flake 提供的屬性
      # 並透過 home-manager-flake 提供的函數，來產生homeConfigurations屬性值 
      homeConfigurations."root" = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        modules = [ ./home-root.nix ];  # 加載 home-manager 的配置文件，預設為 home.nix，可以自定義為其他檔名
      };
    };
  ```

- 範例，模組化的 hm + flake，適合nixos使用

  ```
  inputs = {
    ...
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    ...
  };

  outputs = inputs@{ nixpkgs, home-manager, ... }: {
    nixosConfigurations = {
      # nixos 為主機名
      nixos = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./configuration.nix

          # - 使用 home-manager 提供的 nix-module
          # - 和 nixos 搭配使用時 (使用 nixosConfigurations屬性)，
          #   透過 home-manager.nixosModules.home-manager {} nix-moduele 的內容
          home-manager.nixosModules.home-manager
          {
            # home-manager 自身的配置
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            # 讀取用於配置系統的 home.nix
            home-manager.users.root = import ./home.nix;
          }
        ];
      };
    };
  }
  ```

- 範例，hm + flake.nix 的版本，適合非nixos使用
  
  ```
  {
    description = "A simple flake for managing home configurations";

    inputs.home-manager.url = "github:nix-community/home-manager";

    outputs = { self, nixpkgs, home-manager }: {

      homeConfigurations.exampleHomeConfig = let
        system = "x86_64-linux"; # 使用适合你的系统架构
        pkgs = import nixpkgs { inherit system; };
        hm = home-manager.lib.homeManagerConfiguration {
          
          # 也可以將 conifguration 函數寫在獨立的檔案中
          #configuration = import ./home.nix  

          configuration = { config, lib, pkgs,... }: {
            home.username = "yourUsername"; 
            home.homeDirectory = "/home/yourUsername"; 
            home.packages = with pkgs; [
              htop
              curl
            ];

            programs.emacs = {
              enable = true;
              package = pkgs.emacs;
            };
          };
        };
      in
      hm.config;
    };
  }
  ```

  執行 `$ home-manager switch --flake '.#exampleHomeConfig'` 或 
  `$ home-manager switch --flake '.#homeConfigurations.exampleHomeConfig'`

- 範例，[適用於 home-manager v22.10](hm-examples/hm-v22.10-example.nix)
- 範例，[適用於 home-manager v22.11](hm-examples/hm-v22.11-example.nix)