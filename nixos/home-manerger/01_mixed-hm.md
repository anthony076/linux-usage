

## 混和模式下的 home-manager

- 安裝方法

  執行，$ nix-channel --add https://github.com/nix-community/home-manager/archive/release-24.05.tar.gz home-manager

  執行，$ nix-channel --update
      
  在 configuration.nix中

  ```nix
  {
    imports = [
      <home-manager/nixos>  # 導入 home-manager-module
    ];

    home-manager.users.<your-username> = { ... };

    environment.systemPackages = with pkgs; [
      home-manager  # 安裝 home-manager-cli
    ];
  }
  ```

- 使用流程
  - 需要安裝home-manager-cli
  - 需要在`configuration.nix`中，使用`imports = [<home-manager/nixos>]`
  - 可以透過`$ nixos rebuild switch`應用 home.nix 的內容
  - 可以透過`$ home-manager rebuild switch`應用 home.nix 的內容

- 優點
  - 只想更新應用程序、shell 配置、環境變量等用戶層配置時，不需要進行整個系統重建，可減少重建時間
  - 允許快速測試用戶配置變更而不需要觸碰系統層的設置

- 缺點
  - 如果 home-manager 和 nixpkgs 使用的版本不同步，可能會出現一些兼容性問題