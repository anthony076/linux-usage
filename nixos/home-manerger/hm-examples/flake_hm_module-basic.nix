# /etc/nixos
.
├── configuration.nix
├── flake.nix
└── home.nix

# =============================================

# /etc/nixos/configuration.nix
{ config, lib, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
    ];

  boot.loader.grub.enable = true;
  boot.loader.efi.efiSysMountPoint = "/boot";
  boot.loader.grub.device = "/dev/sda";

  users.defaultUserShell = pkgs.fish;

  nix.settings.experimental-features =  [ "nix-command" "flakes"];

  environment.systemPackages = with pkgs; [
     vim
     wget
     fish
     git
  ];

  programs.fish.enable = true;

  programs.fish.interactiveShellInit = ''
    set fish_greeting
  '';

  services.openssh.enable = true;
  services.openssh.settings.PermitRootLogin = "yes";
  services.openssh.settings.PasswordAuthentication = true;

  system.stateVersion = "24.05";

}

# =============================================

# /etc/nixos/home.nix

{ config, pkgs, ... }:

{
  home.username = "root";
  home.homeDirectory = "/root";

  home.stateVersion = "24.05";
  home.packages = [
    # pkgs.hello
  ];

  home.file = {};

  home.sessionVariables = {
    EDITOR = "hx";
  };

  programs.home-manager.enable = true;
}

# =============================================

# /etc/nixos/flake.nix

{
  description = "NixOS configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs@{ nixpkgs, home-manager, ... }: {
    nixosConfigurations = {
      root = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./configuration.nix

          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.root = import ./home.nix;

          }
        ];
      };
    };
  };
}

# nixos-rebuild switch --flake '/etc/nixos#root'