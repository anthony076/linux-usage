# flake.nix

homeManagerConfiguration {
  pkgs = nixpkgs.legacyPackages.${system};
  modules = [
    ./home.nix
    ./some-extra-module.nix
    {
      home = {
        username = "jdoe";
        homeDirectory = "/home/jdoe";
        stateVersion = "22.05";
      };
    }
  ];
}

# home.nix

{ config, pkgs, ... }:

{
  home.username = "jdoe";
  home.homeDirectory = "/home/jdoe";
  home.stateVersion = "23.11";
  home.packages = [
    pkgs.htop
    pkgs.fortune
  ];

  programs.home-manager.enable = true;
  programs.套件名 = {
    ...
  };
}


# 執行 $ home-manager switch