# 產生配置檔，$ nix run home-manager/master -- init
# 根據需求修改配置檔，$ vi ~/.config/home-manager/home.nix
# 建構+啟用 home-manager-cli，並執行 switch 套用配置，$ nix run home-manager/master -- init --switch

# =============================

# ~/.config/home-manager/home.nix

{ config, pkgs, ... }:

{
  home.username = "root";
  home.homeDirectory = "/root";
  home.stateVersion = "24.05"; # Please read the comment before changing.
  home.packages = [
    pkgs.hello
  ];

  home.file = {
  };

  home.sessionVariables = {
    # EDITOR = "emacs";
  };

  programs.home-manager.enable = true;
}

# =============================

# ~/.config/home-manager/flake.nix

{
  description = "Home Manager configuration of root";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs, home-manager, ... }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      homeConfigurations."root" = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        modules = [ ./home.nix ];
      };
    };
}

# =============================

# 套用變更
# 正確方法，$ home-manager switch (預設讀取 ~/.config/home-manager/flake.nix)

(evaluating derivation 'path:/root/.config/home-manager#homeConfigurations."root".activationPackage')

# 正確方法，$ home-manager switch --flake ~/.config/home-manager#root (手動指定flake.nix的目錄)
# 注意，此命令會找到homeConfigurations."root"屬性後，執行home-manager.lib.homeManagerConfiguration
# 並得到 homeConfigurations."root".activationPackage，直接指定是不行的，要經過評估才會得到此結果

# 錯誤用法，$ home-manager switch --flake ~/.config/home-manager#homeConfigurations."root"
# Home Manager flake 配置引用中的鍵名不應該加引號

# 錯誤用法，$ home-manager switch --flake ~/.config/home-manager#homeConfigurations.root
# 不需要添加 homeConfigurations屬性

# 錯誤用法，$ home-manager switch --flake ~/.config/home-manager#homeConfigurations."root".activationPackage
# 此表達式在評估之後才會出現，在 home.nix 中沒有