[nix-shell:~/nix-config]# tree
.
├── flake.nix
├── home.nix
├── nixos
├── configuration.nix
├── hardware-configuration.nix
└── packages
    └── helix.nix

# =============================

# ~/nix-config/nixos/configuration.nix

{ config, lib, pkgs, ... }:

{
  imports = [ ./hardware-configuration.nix ];

  boot.loader.grub.enable = true;
  boot.loader.efi.efiSysMountPoint = "/boot";
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  users.defaultUserShell = pkgs.fish;

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  environment.systemPackages = with pkgs; [ vim wget fish git ];

  programs.fish.enable = true;

  programs.fish.interactiveShellInit = ''
    set fish_greeting 
  '';

  services.openssh.enable = true;
  services.openssh.settings.PermitRootLogin = "yes";
  services.openssh.settings.PasswordAuthentication = true;

  system.stateVersion = "24.05";

}

# =============================

# ~/nix-config/home.nix

{ config, pkgs, ... }:

{
  home.username = "root";
  home.homeDirectory = "/root";
  home.stateVersion = "24.05"; # Please read the comment before changing.
  home.packages = with pkgs; [ helix ];

  home.file = { };

  home.sessionVariables = { EDITOR = "hx"; };

  programs.home-manager.enable = true;
  programs.helix = import ./packages/helix.nix { inherit pkgs; };

}

# =============================

# ~/nix-config/flake.nix

{
  description = "Home Manager configuration of root";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs, home-manager, ... }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
    in {

      # system-configuration
      # used by $ nixos-rebuild switch --flake .
      nixosConfigurations.nixos = nixpkgs.lib.nixosSystem {
        inherit system;
        modules = [ ./nixos/configuration.nix ];
      };

      # user-confignixosConfigurations
      # used by $ home-manager switch --flake .
      homeConfigurations."root" = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        modules = [ ./home.nix ];
      };
    };
}

# =============================

# 套用 home-manager 的變更
# cd ~/nix-config
# home-manager switch --flake .#root 或 home-manager switch --flake .
# 注意，上述命令只對 homeConfigurations 屬性有效，不會檢查 nixosConfigurations 屬性的正確性

# 套用 nixos 的變更
# cd ~/nix-config
# nixos-rebuild switch --flake .
