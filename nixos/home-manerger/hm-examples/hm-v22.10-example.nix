# flake.nix

{
  outputs = { self, nixpkgs, nixpkgs-unstable, home-manager, ... }@inputs:
  let
    # define system, username etc...
    pkgs = import nixpkgs-unstable {
      inherit system;
      config.allowUnfree = true;
      config.allowUnsupportedSystem = true;
      overlays = [
        inputs.neovim-nightly-overlay.overlay
      ];
    };
  in 
  {
    homeConfigurations.${username} = home-manager.lib.homeManagerConfiguration {
      inherit system username pkgs;
      configuration = import ./home.nix;  # 透過 configuration 輸入引述導入 home.nix
      system = "x86_64-linux";
      username = "jdoe";
      homeDirectory = "/home/jdoe";
      stateVersion = "22.05";
      extraModules = [ ./some-extra-module.nix ];
    };
  }
}