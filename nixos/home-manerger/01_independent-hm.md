## 獨立化使用 home-manager

- [安裝 home-manager-cli](#安裝-home-manager-cli)
- [使用流程 + 加載 home.nix 的方法](#使用流程--加載-homenix-的方法)
- [home.nix的使用](00_hm-overview.md#配置-home-manager-配置文件homenix-的使用)

- 優，套用變更時，home-manager switch 比 nixos-rebuild switch 更快

- 缺，對於 configuration.nix 的設置無法感知，部分hm的設置可能會失效，例如，
  
  對於獨立版的hm，若設置了 home.sessionVariables 時，此屬性會將設置的變數寫入 
  "${HOME}/.nix-profile/etc/profile.d/hm-session-vars.sh" 的檔案中

  若使用 configuration.nix 安裝系統級的 fish-shell 時，
  hm 無法感知 fish-shell 的存在，就不會自動將 hm-session-vars.sh 添加到 
  ~/.config/fish/config.fish 中，使得 home.sessionVariables 中的設置無效

  需要改用 hm 安裝 fish-shell，才會自動將 hm-session-vars.sh 添加到 fish-shell 的配置中

## 安裝 home-manager-cli

- home-manager-cli 的多種安裝方法
  - 方法，透過`configuration.nix`的`environment.systemPackages`
  - 方法，透過`nix profile`命令
  - 方法，透過`nix-shell` + 官方提供的`install.nix`

  - 注意，以上方法只會安裝home-manager-cli，因為沒有在`imports = [<home-manager/nixos>]`導入home-manager-module，
    因此，需要手動執行 home-manager-cli ，才能使 home-manager 配置文件 home.nix 生效

- 安裝方法，透過`configuration.nix`的`environment.systemPackages`安裝`home-manager-cli`
  - 在`/etc/nixos/configuration.nix`中添加以下
    ```
    environment.systemPackages = with pkgs; [
      home-manager
    ];
    ```
  - 系統級別安裝，會安裝 home-manager-cli + home.nix 配置檔

- 安裝方法，透過`nix profile`
  - 在 shell 中執行，
    - 安裝最新版本，`$ nix profile install home-manager`
    - 安裝指定版本，`$ nix profile install github:nix-community/home-manager/release-23.11`
  - 用戶級別安裝
  - 安裝到`~/.nix-profile/bin/home-manager`的路徑中
  - 會建立`~/.config/home-manager/home.nix`的配置檔
  - 優，可透過 nix profile 管理不同版本的 home-manager，可隔離不同用戶的配置

- 安裝方法，推薦，透過`nix-shell` + 官方提供的`install.nix`
  - 執行以下
    ```
    # 使 <home-manager> 生效
    nix-channel --add https://github.com/nix-community/home-manager/archive/release-24.05.tar.gz home-manager
    nix-channel --update
    
    # 執行 home-manager-cli的安裝
    nix-shell '<home-manager>' -A install

    # 上述命令會執行 home-manager/default.nix 中的 install 屬性
    https://github.com/nix-community/home-manager/blob/master/default.nix
    
    先透過 home-manager/home-manager/default.nix 建構 home-manger
    https://github.com/nix-community/home-manager/blob/master/home-manager/default.nix

    再調用 home-manager/home-manager/install.nix 安裝到系統
    https://github.com/nix-community/home-manager/blob/master/home-manager/install.nix
    ```

  - 系統級安裝，會配置相關的 NixOS 服務或使用者配置
  - 安裝在`~/.nix-profile/bin/home-manager` 的路徑中
  - 會建立`~/.config/home-manager/home.nix` 的配置檔
  - 利用此方法安裝的home-manager-cli，`即使退出nix-shell，仍然可以執行 home manager`
    且由於是安裝到nix profile的路徑中，可以透過 nix profile 刪除

## 使用流程 + 加載 home.nix 的方法

- 使用流程，
  - 需要安裝home-manager-cli
  - 不需要在configuration.nix 中加載 home.nix
  - 透過`$ home-manager rebuild switch`應用 home.nix 的內容

- home-manger配置文件，home.nix 位置，`~/.config/home-manager/home.nix`

## 範例集

https://kaleo.blog/2023/06/manage-workspace-with-home-manager/