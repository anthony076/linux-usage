## customize package of rust-package

- 定製套件方法有三種
  - [定製套件基礎](readme.md)
  - 方法，透過overrideAttrs修改buildRustPackage的建構參數，[參考](#定製方法-透過overrideattrs修改buildrustpackage的建構參數)
  - 方法，透過overlays修改pkgs中rust套件，[參考](#定製方法-透過overlays修改pkgs中rust套件)
  - 方法，指定pkgs的版本，[參考](#定製方法-指定pkgs的版本)

## [定製方法] 透過overrideAttrs修改buildRustPackage的建構參數

- rust-package會使用`pkgs.rustPlatform.buildRustPackage函數`進行建構

  透過源碼可以查看是否使用buildRustPackage，
  例如，[just建構源碼](https://github.com/NixOS/nixpkgs/blob/nixos-24.11/pkgs/by-name/ju/just/package.nix)

- buildRustPackage 函數中，除了 version 指定版本外，
  還需要透過`src屬性`和`pkgs.fetchFromGitHub函數`指定正確的github-repo和源碼壓縮檔的hash值，用於確保源碼的正確性

- 建構過程中，會透過`cargo.lock`和`vendor目錄`檢查依賴套件的版本和檔案是否正確，以確保建構出來的套件與源碼一致，
  如果不一致，一般會在`cargoSetupPostUnpackHook`階段報錯

  - vendor 目錄，
    - 是使用`$ cargo vendor`產生vendor目錄，cargo會將所有依賴的都下載到vendor目錄中
    - vendor目錄用於製作vendor-tarball，且vendor-tarball的檔名具有`套件名-x.x.x-vendor.tar.gz`格式
    - vendor-tarball必須包含cargo.lock檔案
  
  - cargo.lock，
    - 用於紀錄依賴套件的`套件來源`、`套件版本`、`套件checksum值`
    - 一般套件源碼都會提供，不需要重新產生
  
  - 透過比對vendor目錄和cargo.lock可以驗證下載的依賴套件和cargo.lock中的紀錄是否一致

- buildRustPackage 函數中，用於驗證依賴的屬性，
  - 注意，驗證依賴時，以下屬性不需要全部都提供，只需要擇一選擇即可
  - cargoVendorDir屬性 : 指定`源碼內`vendor目錄的路徑
  - cargoDeps屬性 : 指定`本地(非源碼內)的vendor目錄`或`vendor-tarball`位置或`cargo.lock`
  - cargoHash屬性|cargoSha256屬性 : 用於驗證 vendor-tarball 正確性的hash值

- 注意，不需要使用`$ cargo generate-lockfile`產生Cargo.lock，<font color=blue>會破壞Cargo.lock原始的內容</font>
  
  $ cargo vendor 會根據現有的 cargo.lock 產生 vendor目錄中的內容，

  若產生vendor目錄後，又執行 $ cargo generate-lockfile，由於預設 $ cargo generate-lockfile 會從crate.io獲取套件最新版本的資訊後，
  進一步更新 cargo.lock 的內容，此時，`更新後的 cargo.lock 和 vendor目錄內的版本就有可能不一致`

  因此，若cargo.lock遺失，寧可重新下載源碼也不要透過 $ cargo generate-lockfile --offline 重新產生 cargo.lock，
  因為無法保證產生的 cargo.lock 與原始的一致

## [屬性] buildRustPackage的`src屬性` : 指定套件來源

- 可使用
  - `fetchFromGitHub` : 獲取git上的套件源碼

    ```
    src = fetchFromGitHub {
      owner = "BurntSushi";
      repo = pname;
      rev = version;
      hash = "sha256-+s5RBC3XSgb8omTbUNLywZnP6jSxZBKSS1BmXOjRF8M=";
    };
    ```

  - `fetchCrate` : 獲取crate.io上的套件源碼

    ```
    src = fetchCrate {
      inherit pname version;
      hash = "sha256-aDQA4A5mScX9or3Lyiv/5GyAehidnpKKE0grhbP1Ctc=";
    };
    ```
  
  - `fetchPypi` : : 獲取pypi上的套件源碼

    ```
    src = pkgs.python3.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "b865af9696a71aeca17c2e926c262a9160191aa247c98c3d75860d3ff34bdc67";
    };
    ```

## [屬性] buildRustPackage的`cargoVendorDir屬性` : 指定`源碼內`vendor目錄的路徑

- 設置cargoVendorDir屬性後，會在下載源碼的根目錄下尋找vendor目錄(/build/vendor)

  缺點，vendor 目錄需要源碼有提供才有，不是每個專案源碼都有，
  
  若設置了cargoVendorDir屬性但源碼實際上沒有提供時，建構過程就會報錯

## [屬性] buildRustPackage的`cargoDeps屬性` : 指定`本地(非源碼內)的vendor目錄`或`vendor-tarball`位置或`cargo.lock`

- 使用場景 : 如果已經有vendor.tar.gz或vendor目錄，通過cargoDeps屬性可以
  - 狀況，直接使用vendor目錄
  - 狀況，手動設置fetchCargoTarball函數，避免下載錯誤的版本
  - 狀況，直接使用Cargo.lock檔案

- 比較，`cargoLock屬性`和`利用cargoDeps屬性指定cargoLock檔案`的區別

- 範例，cargoDeps屬性=`本地(非源碼內)的vendor目錄`，
  參考，[cargoDeps-folder](cpkgs-examples/just-cargoDeps-folder.nix)

- 範例，cargoDeps屬性=`套件名-套件版本-vendor.tar.gz`，
  參考，[cargoDeps-tarball](cpkgs-examples/just-cargoDeps-tarball.nix)

  - 一般會使用`pkgs.rustPlatform.fetchCargoTarball`來自定義並下載vendor-tarball

  - 自製vendor-tarball容易失敗，例如，
  
    ```
    wget https://github.com/casey/just/archive/3ddd1b168308db2b88daac9ea70a3b63629ef034.tar.gz -O just-1.28.0.tar.gz
    tar -xzf just-1.28.0.tar.gz
    cd just-3ddd1b168308db2b88daac9ea70a3b63629ef034
    cargo vendor
    cp Cargo.lock ./vendor/ 
    tar -czf just-1.28.0-vendor.tar.gz vendor
    cd ..
    mv just-3ddd1b168308db2b88daac9ea70a3b63629ef034/just-1.28.0-vendor.tar.gz .
    
    # 將 cargoDeps 設置為 cargoDeps = builtins.path { path = /root/customize-package/just-1.28.0-vendor.tar.gz; };
    ```

- 範例，cargoDeps屬性=`cargo.lock`，
  參考，[cargoDeps-lockfile](cpkgs-examples/just-cargoDeps-lockfile.nix)

## [屬性] buildRustPackage的`cargoHash屬性|cargoSha256屬性` : 用於驗證 vendor-tarball 正確性的hash值

- cargoHash屬性是cargoSha256屬性的前身，用於指定非 SRI 格式的 hash

- 使用場景
  - 若源碼內沒有提供vendor目錄
  - 若不希望手動準備vendor目錄
  - 不適合(套件安裝版本)和(預設安裝套件版本)不同的系統

- 目的，用於驗證fetchFromGitHub下載的vendor-tarball的正確性

- 透過此方法會
  - `使用fetchCargoTarball`下載vendor.tar.gz
  - 解壓縮vendor.tar.gz後，計算 hash 值
  - 將hash值與`cargoHash屬性|cargoSha256屬性`進行比對

- 對於 cargo.lock 中有 git 來源的依賴，需要額外添加`useFetchCargoVendor = true;`

  - 判斷是否是git來源的依賴
    在 cargo.lock 中，若source參數中使用`git+...`的字樣，代表是git依賴

    以下的套件源不是git依賴，是從crates.io的registry中獲取的依賴
    ```
    [[package]]
    name = "windows_x86_64_msvc"
    version = "0.48.5"
    source = "registry+https://github.com/rust-lang/crates.io-index"
    checksum = "ed94fce61571a4006852b7389a063ab983c02eb1bb37b47f8272ce92d06d9538"
    ```

    是git依賴
    ```
    [[package]]
    name = "windows_x86_64_msvc"
    version = "0.48.5"
    source = "git+https://github.com/rust-lang/crates.io-index"
    checksum = "ed94fce61571a4006852b7389a063ab983c02eb1bb37b47f8272ce92d06d9538"
    ```

  - 範例

    ```
    {
      useFetchCargoVendor = true;
      cargoHash = "sha256-RqPVFovDaD2rW31HyETJfQ0qVwFxoGEvqkIgag3H6KU=";
    }
    ```

- 缺點，cargoHash屬性|cargoSha256屬性，在套件版本有差異的系統，容易造成 fetchCargoTarball 的錯誤

  詳見，[安裝套件版本與系統預設套件版本不同會造成fetchcargotarball的錯誤](#issue-安裝套件版本與系統預設套件版本不同會造成fetchcargotarball的錯誤)

- 範例，[cargoHash使用範例](cpkgs-examples/just-cargoHash.nix)

## [屬性] buildRustPackage的`cargoLock屬性` : 用於指定 Cargo.lock 在本機上的位置

- cargoLock屬性是cargoDeps屬性的語法糖，
  參考，[buildRustPackage源碼](https://github.com/NixOS/nixpkgs/blob/a56d8a9a8a6747338d56f8b5b1aaded544f83722/pkgs/build-support/rust/build-rust-package/default.nix#L69)

- 不推薦使用，指定cargoLock屬性後，依舊會自動調用 fetchCargoTarball，對於版本不相容的系統，依舊會造成錯誤，
    
  詳見，[安裝套件版本與系統預設套件版本不同會造成fetchcargotarball的錯誤](#issue-安裝套件版本與系統預設套件版本不同會造成fetchcargotarball的錯誤)

- 若 Cargo.lock 不包含 git 來源

  ```nix
  cargoLock = {
    lockFile = ./Cargo.lock;
  };
  ```

- 若 Cargo.lock 包含 git 來源，需要為該來源添加上`outputHashes屬性`或`allowBuiltinFetchGit=true;`

  ```nix
  cargoLock = {
    lockFile = ./Cargo.lock;

    outputHashes = {
      "finalfusion-0.14.0" = "17f4bsdzpcshwh74w5z119xjy2if6l2wgyjy56v621skr2r8y904";
    };

    # allowBuiltinFetchGit = true;
  };
  ```

## [進階] 手動計算 cargoHash 值

`$ cargo vendor ...`，會將專案需要的依賴下載到vendor目錄中，再對vendor目錄進行hash計算

以 https://github.com/casey/just/archive/3ddd1b168308db2b88daac9ea70a3b63629ef034.tar.gz 為例

```shell
wget https://github.com/casey/just/archive/3ddd1b168308db2b88daac9ea70a3b63629ef034.tar.gz -O just-1.28.0.tar.gz

tar -xzf just-1.28.0.tar.gz

cd just-3ddd1b168308db2b88daac9ea70a3b63629ef034

mkdir .cargo

cargo vendor > .cargo/config.toml

nix hash path ./vendor

#得到 sha256-TY3Y0HgbD9AR39pGZEa5KKLtze2d6Nm7jpcpl4dXVE0=
```

## [進階] cargoSetupPostUnpackHook 檢查過程

- 參考，[cargoSetupPostUnpackHook的源碼](https://github.com/NixOS/nixpkgs/blob/a56d8a9a8a6747338d56f8b5b1aaded544f83722/pkgs/build-support/rust/hooks/cargo-setup-hook.sh)

- 概述，cargoSetupPostUnpackHook 用於檢查用戶是否提供vendor目錄，
  - 若無，透過 fetchCargoTarball下載vendor.tar.gz，並自動生成 cargo.lock
  - 若有，利用提供的vendor目錄尋找 cargo.lock
  - 利用 cargo.lock 驗證依賴

- step，判斷$cargoVendorDir是否為空，
  - 若是，判斷 $cargoDeps" 是否是目錄
    - 是目錄，設置 `dest=$(stripHash "$cargoDeps")`
    - 不是目錄，解壓縮`$cargoDeps`
  - 若否，設置`cargoDepsCopy="$(realpath "$(pwd)/$sourceRoot/${cargoRoot:+$cargoRoot/}${cargoVendorDir}")"`

- step，設置`config="$cargoDepsCopy/.cargo/config.toml"`

- step，比較`cargoDepsLockfile="$cargoDepsCopy/Cargo.lock"`和`srcLockfile="$(pwd)/${cargoRoot:+$cargoRoot/}Cargo.lock"`兩者是否一致

## [fetchCargoTarball] 如何製作 vendor-tarball

- 參考，[fetchCargoTarball的源碼](https://github.com/NixOS/nixpkgs/blob/master/pkgs/build-support/rust/fetch-cargo-tarball/default.nix)

- 為什麼使用 fetchCargoTarball
  
  因為在sandbox建置中不允許網路訪問，因此需要各種fetcher在建構前透過各種方式取得依賴，
  並在建構前提供給sandbox

  - FetchCargoTarball可以`提供 crate 的所有依賴項`，並建立 vendor-tarball
  - 如果 Cargo.lock 中有`git依賴`，需要將 fetchCargoTarball 替換為`fetchCargoVendor`

## [issue] (安裝套件版本)與(系統預設套件版本)不同，會造成fetchCargoTarball的錯誤

在不同版本的系統中，使用fetchCargoTarball有可能下載錯誤的版本，造成依賴驗證錯誤
  
使用以下屬性驗證依賴時，nix 會自動調用 fetchCargoTarball 下載並製作 vendor.tar.gz，
- cargoHash屬性|cargoSha256屬性
- cargoLock屬性

當安裝的版本和系統預設的版本不一致時，容易造成錯誤，例如，

|      | 安裝裝的套件版本       | 系統預設安裝的套件版本   | Note                          |
| ---- | ---------------------- | ------------------------ | ----------------------------- |
| 相同 | just.1.28(nixos-24.05) | just.1.28(nixos-24.05)   | 安裝正常                      |
| 不同 | just.1.28(nixos-24.05) | `just.1.36(nixos-24.11)` | fetchCargoTarball下載錯誤版本 |

因此，若版本不同時，只推薦使用`cargoDeps屬性 + fetchCargoTarball 函數`，
可以手動設置fetchCargoTarball下載 vendor-tarball 的版本

## [定製方法] 透過overlays修改pkgs中rust套件

- 以 just 為例

  ```nix
  let
    my-overlay = self: super: {
      just = super.just.overrideAttrs (oldAttrs: rec {

        version = "1.28.0";

        src = super.fetchFromGitHub {
          owner = "casey";
          repo = "just";
          rev = "3ddd1b168308db2b88daac9ea70a3b63629ef034";
          hash = "sha256-GdDpFY9xdjA60zr+i5O9wBWF682tvi4N/pxEob5tYoA=";
        };

        cargoDeps = super.rustPlatform.fetchCargoTarball {
          inherit src;
          hash = "sha256-lw+qD620e+wsQoyLa6KQd83J0awIlUbemJ3Itc6jjyI=";
        };
      });
    };

    mypkgs = import <nixpkgs> { overlays = [ my-overlay ]; };
  in
    mypkgs.mkShell {
      buildInputs = [ mypkgs.just];
    }
  ```

## [定製方法] 指定pkgs的版本

參考，[指定pkgs的版本](../how-to/how-to-specific-source.md#指定pkgs的版本)
  
## ref

- [官方rustPlatform提供各種函數的說明](https://github.com/NixOS/nixpkgs/blob/master/doc/languages-frameworks/rust.section.md)

- rust-package
  - [buildRustPackage的定義](https://github.com/NixOS/nixpkgs/blob/nixos-24.11/pkgs/development/compilers/rust/make-rust-platform.nix)
  
  - [buildRustPackage的源碼](https://github.com/NixOS/nixpkgs/blob/a56d8a9a8a6747338d56f8b5b1aaded544f83722/pkgs/build-support/rust/build-rust-package/default.nix)
  
  - [cargoDeps屬性的源碼](https://github.com/NixOS/nixpkgs/blob/master/pkgs/build-support/rust/build-rust-package/default.nix#L70)

- cargoDeps屬性使用範例-tarball
  - [rust.section.md](https://github.com/tsurucapital/nixpkgs/blob/1f1ed641dee4eaa3dbabf6adec2faadd98171a7f/doc/languages-frameworks/rust.section.md?plain=1#L399)
  - [buildRustPackage.md](https://github.com/nixos-asia/website/blob/d51660e4b295808856884ca82c05c6f10f789ae4/en/buildRustPackage.md?plain=1#L26)
  - [qiskit-flake.nix](https://github.com/Panadestein/nixos-config/blob/bd751bc681cce8df43c5d001bf981d50aa985438/utils/flakes/qiskit-flake.nix#L45)
