## customize package 定製套件

- 要定製套件 可以透過以下幾種方式
  
  - 方法，使用 override 或 overrideAttrs 修改建構參數
    
    參考，[override 或 overrideAttrs的使用](../functions/override-and-overlay.md#overrideoverrideattrs-的使用)

  - 方法，使用 overlay 改寫 pkgs 的內容

    參考，[overlays的使用](../functions/override-and-overlay.md#overlays-的使用)

  - 方法，多個nixpkgs實例 (使用特定版本pkgs)

    - 範例，在 shell.nix 中
    
      ```nix
      let
        oldPkgs = import (fetchTarball {
          url = "https://github.com/NixOS/nixpkgs/archive/<commit_hash>.tar.gz";
        }) {};
      in
      
      mkShell {
        buildInputs = [ oldPkgs.套件名 ];
      }
      ```

    - 範例，在 flake.nix 中

      ```
      {
        description = "Shell with just 1.28.0";

        inputs = {
          nixpkgs.url = "github:NixOS/nixpkgs";
          oldpkgs.url = "github:NixOS/nixpkgs/<commit_hash>";
        };

        outputs = { self, nixpkgs, oldpkgs }: {
          devShells.default = import nixpkgs {
            overlays = [
              (final: prev: {
                just = oldpkgs.legacyPackages.x86_64-linux.just;
              })
            ];
          };
        };
      }
      ```

  - 完整範例，見[客製化rust套件](customize-rust-packages.md)

- 優缺點

  - override 或 overrideAttrs

    由於修改是局部的，對效能的影響相對較小，但對於多個套件需要修改時，
    此方法會增加代碼的複雜度
 
  - overlay
    - overlays 不推薦在nixpkgs源碼中使用，
      
      在源代碼中使用了`import <nixpkgs>`的語句，會導致 Nixpkgs 被重新評估，
      [會增加不必要的開銷](https://nixos.org/manual/nixpkgs/stable/#sec-overlays-argument)

      詳見，[overlays的使用](../functions/override-and-overlay.md#overlays-的使用)

      通常建議在源代碼中使用`pkgs.extend`或`pkgs.appendOverlays`，
      但如果多次調用，同樣會增加不必要的開銷，因為每執行一次就需要重新建立nixpkgs的實例
    
    - overlay是直接修改全局的nixpkgs實例，如果overlays修改多個底層的套件，最終可能會影響到其他模塊
    
    - overlay修改了A套件，所有依賴A套件的套件也會因為二進制緩存失效了，而需要重新編譯，
      若影響的套件多了，會導致大量的本地編譯

  - 推薦使用，多個nixpkgs實例 (使用特定版本pkgs)
    
    - 通常不建議在代碼中直接使用`import nixpkgs {...}`的語句建立多個nixpkgs實例

      nixpkgs的實例化相對而言消耗的資源較多，而每次使用`import nixpkgs {...}`的語句，
      都會重新建立新的nixpkgs實例

    - 透過傳參傳遞nixpkgs實例
      
      一般會統一建立nixpkgs的實例，在透過特定屬性(例如，specialArgs屬性)或傳參的方式引用已經建立好的nixpkgs的實例，
      以避免重複建立nixpkgs實例的開銷

## ref

- [降級與升級軟件包](https://nixos-and-flakes.thiscute.world/zh/nixos-with-flakes/downgrade-or-upgrade-packages)
- [1000 instances of nixpkgs](https://zimbatm.com/notes/1000-instances-of-nixpkgs)