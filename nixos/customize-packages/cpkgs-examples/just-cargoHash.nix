# 執行，nix-shell

{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    (pkgs.just.overrideAttrs (oldAttrs: rec {
      version = "1.28.0";
      src = pkgs.fetchFromGitHub {
        owner = "casey";
        repo = "just";
        
        #rev = "refs/tags/1.28.0";
        rev = "3ddd1b168308db2b88daac9ea70a3b63629ef034"; # from https://github.com/casey/just/releases/tag/1.28.0

        # 直接從源碼獲取，from https://github.com/NixOS/nixpkgs/blob/nixos-24.05/pkgs/by-name/ju/just/package.nix
        #hash = "sha256-GdDpFY9xdjA60zr+i5O9wBWF682tvi4N/pxEob5tYoA=";

        # 或手動計算
        # step1，獲取原始hash值，
        # $ nix-prefetch-url --unpack https://github.com/casey/just/archive/3ddd1b168308db2b88daac9ea70a3b63629ef034.tar.gz
        # 得到 1032dnza2i4wzq6jxgmdrpmqa5f0pn9qpzisscx30xkiiwaykl0r
        
        # step2，將原始 hash 值轉換為 base32 格式
        # nix hash to-sri --type sha256 1032dnza2i4wzq6jxgmdrpmqa5f0pn9qpzisscx30xkiiwaykl0r
        # 得到 sha256-GdDpFY9xdjA60zr+i5O9wBWF682tvi4N/pxEob5tYoA=    與官方提供的 hash 值相同
        hash = "sha256-GdDpFY9xdjA60zr+i5O9wBWF682tvi4N/pxEob5tYoA="; # nix-prefetch-url 計算值
      };

      # from https://github.com/NixOS/nixpkgs/blob/nixos-24.05/pkgs/by-name/ju/just/package.nix
      #cargoHash = "sha256-Cvl4EY57TanJK1XGVahPHGtuEAIR44qwGEPDkXfgw5I=";

      # 從 just v1.28.0 源碼重新計算 sha256 
      # - wget https://github.com/casey/just/archive/3ddd1b168308db2b88daac9ea70a3b63629ef034.tar.gz -O just-1.28.0.tar.gz
      # - tar -xzf just-1.28.0.tar.gz
      # - cd just-3ddd1b168308db2b88daac9ea70a3b63629ef034
      # - cargo vendor > .cargo/config.toml
      # - nix hash path ./vendor
      # - 得到 sha256-TY3Y0HgbD9AR39pGZEa5KKLtze2d6Nm7jpcpl4dXVE0=
      cargoHash = "sha256-TY3Y0HgbD9AR39pGZEa5KKLtze2d6Nm7jpcpl4dXVE0=";
    }))
  ];
}







