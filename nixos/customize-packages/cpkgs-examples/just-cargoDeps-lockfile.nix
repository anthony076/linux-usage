
{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    #pkgs.just    # just v1.36.0

    (pkgs.just.overrideAttrs (oldAttrs: rec {
      version = "1.28.0";
      src = pkgs.fetchFromGitHub {
        owner = "casey";
        repo = "just";
        rev = "3ddd1b168308db2b88daac9ea70a3b63629ef034";
        hash = "sha256-GdDpFY9xdjA60zr+i5O9wBWF682tvi4N/pxEob5tYoA="; 
      };

      # 前置作業
      # - step，$ wget https://github.com/casey/just/archive/3ddd1b168308db2b88daac9ea70a3b63629ef034.tar.gz -O just-1.28.0.tar.gz
      # - step，$ tar -xzf just-1.28.0.tar.gz
      # - step，$ cp just-3ddd1b168308db2b88daac9ea70a3b63629ef034/Cargo.lock .
      cargoDeps = pkgs.rustPlatform.importCargoLock {
        lockFile = ./Cargo.lock;
      };
    }))
  ];
}

