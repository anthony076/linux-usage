# 執行，nix-shell

{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    #pkgs.just    # just v1.36.0
    
    (pkgs.just.overrideAttrs (oldAttrs: rec {
      version = "1.28.0";
      src = pkgs.fetchFromGitHub {
        owner = "casey";
        repo = "just";
        rev = "3ddd1b168308db2b88daac9ea70a3b63629ef034";
        hash = "sha256-GdDpFY9xdjA60zr+i5O9wBWF682tvi4N/pxEob5tYoA="; 
      };
      
      cargoDeps = pkgs.rustPlatform.fetchCargoTarball {
        # 使用上述提供的 src 屬性
        inherit src;
        
        # 若不知道 hash 值，可以先設置為 hash = ""，執行後會打印出當前值，再進行替換
        hash = "sha256-lw+qD620e+wsQoyLa6KQd83J0awIlUbemJ3Itc6jjyI=";  
      };

    }))
  ];
}





