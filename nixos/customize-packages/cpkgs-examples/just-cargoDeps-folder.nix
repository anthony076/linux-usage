# 執行，nix-shell

{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    #pkgs.just    # just v1.36.0
    
    (pkgs.just.overrideAttrs (oldAttrs: rec {
      version = "1.28.0";
      src = pkgs.fetchFromGitHub {
        owner = "casey";
        repo = "just";
        rev = "3ddd1b168308db2b88daac9ea70a3b63629ef034";
        hash = "sha256-GdDpFY9xdjA60zr+i5O9wBWF682tvi4N/pxEob5tYoA=";
      };

      # 前置作業
      # - 注意，vendor 目錄內必須要有cargo.lock檔案
      # - step，下載套件源碼
      # - step，在rust專案的根目錄下使用`$ cargo vendor`產生vendor目錄，cargo會將所有的依賴都下載到vendor目錄中
      # - step，不需要再透過`$ cargo generate-lockfile`重新產生`cargo.lock`檔案，使用原有的Cargo.lock，避免Cargo.lock被更新
      # - step，將源碼提供的cargo.lock複製一份到到vendor目錄中，`$ cp ./cargo.lock ./vendor`
      # - step，設置`cargoDeps=./vendor`
      cargoDeps = "./vendor";
    }))
  ];
}







