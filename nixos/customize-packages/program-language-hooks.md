## program-language-hooks

- 參考，[Hooks reference](https://nixos.org/manual/nixpkgs/stable/#chap-hooks)，包含以下語言或語言相關的hook

  ```
  Autoconf、Automake、autoPatchelfHook、aws-c-common、bmake、breakpointHook、CERNLIB、cmake、desktop-file-utils、gdk-pixbuf、GHC、GNOME platform、haredo、installShellFiles、just、libiconv, libintl、libxml2、Meson、mpiCheckPhaseHook、ninja、patchRcPath hooks、Perl、pkg-config、postgresqlTestHook、Premake、Python、scons、cargo-tauri.hook、teTeX / TeX Live、unzip、validatePkgConfig、versionCheckHook、wafHook、zig.hook、xcbuildHook、xfce.xfce4-dev-tools
  ```
