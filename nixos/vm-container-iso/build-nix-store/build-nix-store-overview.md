
## build-nix-store-overview

- 用於重構nixos時，利用現有的nix-store製作類似 chroot 的套件環境，但是是基於 nix-store 的，  

  該環境會在安裝nixos時使用，在安裝媒體中提供簡易版的nix-store環境
  - 用於建構一個由多個nix套件組成的合併環境，且遵循Nix的隔離模型(沒有全局的/usr或/lib)
  - 用於將多個包合併成單一的輸出目錄
  - 不會模擬傳統的 FHS 架構
  - 適合在純 Nix 環境中構建和管理依賴

- `範例`，利用 pkgs.buildEnv 實現新的 nix-store 環境

  考慮要在nixos的安裝媒體中，提供 pkgs.hello、pkgs.nom、pkgs.nvd 三個套件

  - step，合併前，檢視套件的檔案結構

    <font color=blue>pkgs.hello</font>

    ```nix
    # nix build --print-out-paths nixpkgs#hello | xargs tree
    /nix/store/yb84nwgvixzi9sx9nxssq581pc0cc8p3-hello-2.12.1
    ├── bin
    │   └── hello
    └── share
        ├── info
        │   └── hello.info
        └── man
            └── man1
                └── hello.1.gz
    ```
  
    <font color=blue>pkgs.nvd</font>

    ```
    # nix build --print-out-paths nixpkgs#nvd | xargs tree
    /nix/store/hz45mycj860n5fkdbvpi2c7kl5ywdxl8-nvd-0.2.4
    ├── bin
    │   └── nvd
    └── share
        └── man
            └── man1
                └── nvd.1.gz
    ```
    
    <font color=blue>pkgs.nom</font>
    
    ```
    # nix build --print-out-paths nixpkgs#nom | xargs tree
    /nix/store/fqabcamavmjcaxklrgr41fxhj6wfd0vg-nom-2.6.1
    └── bin
        └── nom
    ```

  - step，透過 pkgs.buildEnv 建立一個只包含 hello、nvd、nom 的 nix-store 環境

    new-store.nix

    ```nix
    # nix build --print-out-paths -f new-store.nix | xargs tree

    { pkgs ? import <nixpkgs> {} }:
    pkgs.buildEnv {
      name = "myLinuxRoot";
      paths = with pkgs; [
        hello
        nom
        nvd
      ];
    }
    ```

    執行 `nix build --print-out-paths -f new-store.nix | xargs tree`
    
    得到 `/nix/store/60nl5rw48ffsylfphwgbxbcbf2ciry79-myLinuxRoot`

  - step，建構結果只有連結到nix-store的軟連結
    
    檢視 `$ tree /nix/store/60nl5rw48ffsylfphwgbxbcbf2ciry79-myLinuxRoot`

    從以下結果可以看出，
    
    輸出的檔案結構依舊是依據 nix-store 的方式儲存的，並且，建構的結果只有連結到nix-store的軟連結，
    並沒有實際的輸出檔案

    ```
    # tree /nix/store/60nl5rw48ffsylfphwgbxbcbf2ciry79-myLinuxRoot

    /nix/store/60nl5rw48ffsylfphwgbxbcbf2ciry79-myLinuxRoot
    ├── bin
    │   ├── hello -> /nix/store/3acqrvb06vw0w3s9fa3wci433snbi2bg-hello-2.12.1/bin/hello
    │   ├── nom -> /nix/store/mn14aw2nyj0m2dgbv51vjp1s7zxs2drv-nom-2.2.3/bin/nom
    │   └── nvd -> /nix/store/gfqpfdb1qrvxrddzz6q1g0f985lr4g82-nvd-0.2.3/bin/nvd
    └── share
        ├── info -> /nix/store/3acqrvb06vw0w3s9fa3wci433snbi2bg-hello-2.12.1/share/info
        ├── locale -> /nix/store/3acqrvb06vw0w3s9fa3wci433snbi2bg-hello-2.12.1/share/locale
        └── man
            └── man1
                ├── hello.1.gz -> /nix/store/3acqrvb06vw0w3s9fa3wci433snbi2bg-hello-2.12.1/share/man/man1/hello.1.gz
                └── nvd.1.gz -> /nix/store/gfqpfdb1qrvxrddzz6q1g0f985lr4g82-nvd-0.2.3/share/man/man1/nvd.1.gz
    ```

  - step，將建構結果複製到目標目錄(./newStore)中
    
    ```
    mkdir newStore
    cp -r /nix/store/60nl5rw48ffsylfphwgbxbcbf2ciry79-myLinuxRoot/* ./newStore

    # 檢視結果
    cd ./newStore
    eza --long --tree --level 3
    .
    ├── newStore
    │  ├── bin
    │  │  ├── hello -> /nix/store/3acqrvb06vw0w3s9fa3wci433snbi2bg-hello-2.12.1/bin/hello
    │  │  ├── nom -> /nix/store/mn14aw2nyj0m2dgbv51vjp1s7zxs2drv-nom-2.2.3/bin/nom
    │  │  └── nvd -> /nix/store/gfqpfdb1qrvxrddzz6q1g0f985lr4g82-nvd-0.2.3/bin/nvd
    │  └── share
    │     ├── info -> /nix/store/3acqrvb06vw0w3s9fa3wci433snbi2bg-hello-2.12.1/share/info
    │     ├── locale -> /nix/store/3acqrvb06vw0w3s9fa3wci433snbi2bg-hello-2.12.1/share/locale
    │     └── man
    ```
    
    注意，此時，複製的檔案仍然只有軟連結，需要透過 nix copy 將 nix-store 的實際檔案複製到目標目錄中

  - step，透過`nix copy`將 nix-store 的實際檔案，複製到目標目錄中 (實際為複製到安裝的媒體上，例如usb)

    `nix copy`和一般的cp不同，`專門用於將nix-store中的檔案複製到外部`，
    
    除了複製指定的檔案外，也會包含nix-store的資料庫和相關依賴，
    確保新的nix-store能正常使用

    以複製到 ./newStore 目錄為例
    - `--to`，指定目標目錄
    - `./result`，為來源目錄，nix copy 會解析來源目錄的內容，並將`實際的檔案`和`相關依賴`複製到目標目錄
    - `--no-check-sigs`，用於解決 `error: cannot add path '/nix/store/<hash>-myLinuxRoot' because it lacks a signature by a trusted key` 的問題
    
    ```
    # 將建構結果複製到 ./result 目錄，並忽略 trusted key 的問題
    nix copy --no-check-sigs --to ./newStore ./result

    # 將建構結果的源碼複製到 `目標目錄/nix/store/` 下
    ls newStore/nix/store/
    
    # 得到以下，除了自定義的 hello、nom、nvm 的套件外，還包含建構套件需要的依賴

    18b6frnaw53mwlcqssndd9n7z09x4f3d-openssl-3.0.14      9wpphykh00zkl9z0fw45mxydra908v8v-sqlite-3.45.3      mn14aw2nyj0m2dgbv51vjp1s7zxs2drv-`nom`-2.2.3
    27zw68xsh197kgxmhskgjxrnv65vfsav-libffi-3.4.6        aip1v5f43kv1kb7r04a1nb4zgaffdnsx-xz-5.4.7           ms8sm35jwp1jfcad7nq2g91rhf3mv9s5-bzip2-1.0.8
    2y852kcvb7shrj8f3z8j22pa0iybcbgj-xgcc-13.2.0-libgcc  b60a6findsk5fv5080xkp7fzld280z4i-gcc-13.2.0-libgcc  q6b70rikb7k6licvj1h24f0x98kpxn04-iana-etc-20240318
    3acqrvb06vw0w3s9fa3wci433snbi2bg-`hello`-2.12.1        f3kj4p536mkydhwxl5zlinn0ns29pnfk-mpdecimal-4.0.0    qdaj8h6gwplx42q8r6cjd0whvrg2h83p-ncurses-6.4
    5v1yxrzh73850swg1sjid0nkjv6zdlla-mailcap-2.1.53      gfqpfdb1qrvxrddzz6q1g0f985lr4g82-`nvd`-0.2.3          sr5k62mwnwk1vwjkwsp8jima13pi7grq-readline-8.2p10
    6l8h1h4rzp6xnw7wflgb17dw10xnx33q-zlib-1.3.1          gwxqkkagvqcbf5ac4jm246c6mg7nbp3h-gcc-13.2.0-lib     syl4snn859kpqvn9qh91kr7n9i4dws04-bash-5.2p32
    7znw23zq8smibf7ha8jirbp9zf3axknn-expat-2.6.3         i31yrfi2hqxq845fh68nya7z3g8kfcq5-gdbm-1.23          vvhd7zgrvvfb6576fws25q3bbaqcanba-python3-3.11.10
    9jivp79yv91fl1i6ayq2107a78q7k43i-libidn2-2.3.7       kn5v0qznv2l0ywy9znrh6pplqw4cxhfl-libxcrypt-4.4.36   zvwpisszhpkkk8spqyya8n3bpm7wj39p-libunistring-1.1
    9kcxc7476dghdz3rpfi32mz0pzclsxbj-tzdata-2024b        kpy2cyd05vdr6j1h200av81fnlxl1jw0-glibc-2.39-52
    ```

  - 最後結果
    
    - 在 newStore的目錄中有類似 FHS 的結構，實際上是連結到 newStore/nix/store 中的檔案
    - 若是執行 nixStore/bin/hello ，也能夠正常執行

    ```nix
    # cd nixStore 
    # eza --long --tree --level 3

    drwxr-xr-x - root  9 Dec 15:21 .
    dr-xr-xr-x - root  9 Dec 13:48 ├── bin
    lrwxrwxrwx - root  9 Dec 13:48 │  ├── hello -> /nix/store/3acqrvb06vw0w3s9fa3wci433snbi2bg-hello-2.12.1/bin/hello
    lrwxrwxrwx - root  9 Dec 13:48 │  ├── nom -> /nix/store/mn14aw2nyj0m2dgbv51vjp1s7zxs2drv-nom-2.2.3/bin/nom
    lrwxrwxrwx - root  9 Dec 13:48 │  └── nvd -> /nix/store/gfqpfdb1qrvxrddzz6q1g0f985lr4g82-nvd-0.2.3/bin/nvd
    drwxr-xr-x - root  9 Dec 15:21 ├── nix
    drwxrwxr-t - root  9 Dec 15:22 │  ├── store
    dr-xr-xr-x - root  1 Jan  1970 │  │  ├── 3acqrvb06vw0w3s9fa3wci433snbi2bg-hello-2.12.1
    dr-xr-xr-x - root  1 Jan  1970 │  │  ├── gfqpfdb1qrvxrddzz6q1g0f985lr4g82-nvd-0.2.3
    dr-xr-xr-x - root  1 Jan  1970 │  │  ├── mn14aw2nyj0m2dgbv51vjp1s7zxs2drv-nom-2.2.3
    ... 內容省略 ...
    dr-xr-xr-x - root  1 Jan  1970 │  │  ├── vvhd7zgrvvfb6576fws25q3bbaqcanba-python3-3.11.10
    dr-xr-xr-x - root  1 Jan  1970 │  │  └── zvwpisszhpkkk8spqyya8n3bpm7wj39p-libunistring-1.1
    drwxr-xr-x - root  9 Dec 15:21 │  └── var
    drwxr-xr-x - root  9 Dec 15:21 │     └── nix
    dr-xr-xr-x - root  9 Dec 13:48 └── share
    lrwxrwxrwx - root  9 Dec 13:48    ├── info -> /nix/store/3acqrvb06vw0w3s9fa3wci433snbi2bg-hello-2.12.1/share/info
    lrwxrwxrwx - root  9 Dec 13:48    ├── locale -> /nix/store/3acqrvb06vw0w3s9fa3wci433snbi2bg-hello-2.12.1/share/locale
    dr-xr-xr-x - root  9 Dec 13:48    └── man
    dr-xr-xr-x - root  9 Dec 13:48       └── man1
    
    root@nixos ~/f/newStore# bin/hello 
    Hello, world!
    ```

  - step，若目標系統沒有nix，將套件包添加到環境變數中就可以直接使用，
    `$ export PATH=/path/to/newStore/bin:$PATH`

  - step，若打包為壓縮檔，`$ tar -czf myNixStore.tar.gz newStore`

## ref

- [transfer the packages built in Nix into the root file system image file](https://discourse.nixos.org/t/building-a-non-nixos-linux-image-using-nix/55652)