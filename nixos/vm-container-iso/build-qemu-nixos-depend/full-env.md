## full-env

- 基礎，[simple-env 和 full-env 的差異](../vm-container-overview.md#函數vm-nixpkgsnixoslibmake-disk-imagenix--用於建構各種格式-disk-image-的函數)

- 和 build-iso 類似，透過`nixpkgs/nixos/default.nix` 可以用於建構qemu的disk-image

- 根據 import 的模組不同
  - 若要建構iso，使用`<nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix>`
  - 若要建構qemu的disk-image
    - 若要建立精簡系統，使用 `cptofs`
    - 若要建立完整系統，使用 `<nixpkgs/nixos/modules/profiles/qemu-guest.nix>`

## 範例，利用 nixpkgs/nixos/default.nix + nixos/modules/profiles/qemu-guest.nix

- 代碼

  ```
  # 建構，nix-build '<nixpkgs/nixos>' -A vm -I nixos-config=./machine-config.nix
  # 透過環境變數設置 qemu 的啟動參數，export QEMU_OPTS="-nographic -no-acpi" 
  # 執行，result/bin/run-nixos-vm 

  { pkgs, lib, ... }:

  # with lib;

  {
    imports = [
      <nixpkgs/nixos/modules/profiles/qemu-guest.nix>
    ];

    config = {
      fileSystems."/" = {
        device = "/dev/disk/by-label/nixos";
        fsType = "ext4";
        autoResize = true;
      };

      boot.growPartition = true;

      boot.kernelParams = [
        "console=ttyS0"
        "noapic"
      ];

      boot.loader.grub.device = "/dev/vda";
      boot.loader.timeout = 0;

      users.extraUsers.root.password = "";

    };
  }
  ```

- 建構後的qemu啟動腳本 (result/bin/run-nixos-vm)

  ```
  #! /nix/store/syl4snn859kpqvn9qh91kr7n9i4dws04-bash-5.2p32/bin/bash

  export PATH=/nix/store/hazsx60lrysd393fw7z7vpy4g6gn4acd-coreutils-9.5/bin${PATH:+:}$PATH
  set -e

  # 建立 nixos.qcow2 的函數
  createEmptyFilesystemImage() {
    local name=$1
    local size=$2
    local temp=$(mktemp)
    /nix/store/3vqq963sx00klpabf4v6lcpsa7kmqdxr-qemu-host-cpu-only-8.2.7/bin/qemu-img create -f raw "$temp" "$size"
    /nix/store/rmdr0csdvm6h1jcarqk5h2ym29h3ds0w-e2fsprogs-1.47.0-bin/bin/mkfs.ext4 -L nixos "$temp"
    /nix/store/3vqq963sx00klpabf4v6lcpsa7kmqdxr-qemu-host-cpu-only-8.2.7/bin/qemu-img convert -f raw -O qcow2 "$temp" "$name"
    rm "$temp"
  }

  # 檢視是否有nixos.qcow2，並設置NIX_DISK_IMAGE變數
  NIX_DISK_IMAGE=$(readlink -f "${NIX_DISK_IMAGE:-./nixos.qcow2}") || test -z "$NIX_DISK_IMAGE"

  if test -n "$NIX_DISK_IMAGE" && ! test -e "$NIX_DISK_IMAGE"; then
      echo "Disk image do not exist, creating the virtualisation disk image..."
      createEmptyFilesystemImage "$NIX_DISK_IMAGE" "1024M"
      echo "Virtualisation disk image created."
  fi

  # 建立建構過程中的臨時目錄
  # Create a directory for storing temporary data of the running VM.
  if [ -z "$TMPDIR" ] || [ -z "$USE_TMPDIR" ]; then
      TMPDIR=$(mktemp -d nix-vm.XXXXXXXXXX --tmpdir)
  fi

  # Create a directory for exchanging data with the VM.
  mkdir -p "$TMPDIR/xchg"

  cd "$TMPDIR"

  # QEMU 啟動腳本
  exec /nix/store/3vqq963sx00klpabf4v6lcpsa7kmqdxr-qemu-host-cpu-only-8.2.7/bin/qemu-kvm -cpu max \
      -name nixos \
      -m 1024 \
      -smp 1 \
      -device virtio-rng-pci \
      -net nic,netdev=user.0,model=virtio -netdev user,id=user.0,"$QEMU_NET_OPTS" \
      -virtfs local,path=/nix/store,security_model=none,mount_tag=nix-store \
      -virtfs local,path="${SHARED_DIR:-$TMPDIR/xchg}",security_model=none,mount_tag=shared \
      -virtfs local,path="$TMPDIR"/xchg,security_model=none,mount_tag=xchg \
      -drive cache=writeback,file="$NIX_DISK_IMAGE",id=drive1,if=none,index=1,werror=report -device virtio-blk-pci,bootindex=1,drive=drive1,serial=root \
      -device virtio-keyboard \
      -usb \
      -device usb-tablet,bus=usb-bus.0 \
      -kernel ${NIXPKGS_QEMU_KERNEL_nixos:-/nix/store/114r7kxpxfdhixk7svmwjzv9knf3afg2-nixos-system-nixos-24.05.6850.7e1ca67996af/kernel} \
      -initrd /nix/store/yxs2hz3wbxnkxhaqch2nkyc4cpy4lw8j-initrd-linux-6.6.63/initrd \
      -append "$(cat /nix/store/114r7kxpxfdhixk7svmwjzv9knf3afg2-nixos-system-nixos-24.05.6850.7e1ca67996af/kernel-params) init=/nix/store/114r7kxpxfdhixk7svmwjzv9knf3afg2-nixos-system-nixos-24.05.6850.7e1ca67996af/init regInfo=/nix/store/lgy3aidjyy6rapfwaj9gjf80j67jp5jr-closure-info/registration console=ttyS0,115200n8 console=tty0 $QEMU_KERNEL_PARAMS" \
      $QEMU_OPTS \
      "$@"
    ```

## ref 

- [build-qcow2.nix](https://gist.github.com/tarnacious/f9674436fff0efeb4bb6585c79a3b9ff)