# myconfig.nix

{ config, lib, pkgs, ... }: {
  # customize kernel version
  #boot.kernelPackages = pkgs.linuxPackages_5_15;

  users.groups.admin = {};
  users.users = {
    vm = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      password = "vm";
      group = "admin";
   };
 };

  virtualisation.vmVariant = {
    # following configuration is added only when building VM with build-vm
    virtualisation = {
      memorySize = 1024; # Use 2048MiB memory.
      cores = 2;
      graphics = false;
    };
  };

  services.openssh = {
    enable = true;
    settings.PasswordAuthentication = true;
  };

  networking.firewall.allowedTCPPorts = [ 22 ];
  environment.systemPackages = with pkgs; [
    fish
  ];

  system.stateVersion = "24.05";
}

# ===========================================

# 建構命令，nixos-rebuild build-vm --flake .#test
# 測試
# - export QEMU_NET_OPTS="hostfwd=tcp::2221-:22"    設置額外的啟動參數
# - result/bin/run-nixos-vm   執行啟動腳本
# - ssh vm@localhost -p 2221    或透過ssh連接

# flake.nix
{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  outputs = { self, nixpkgs, ... }:
    let
      system = "x86_64-linux";
    in
    {
      # test is a hostname for our machine
      nixosConfigurations.test = nixpkgs.lib.nixosSystem {
        inherit system;
        modules = [
          ./myconfig.nix
        ];
      };
    };
}