# from [virtualisation + build-vm + flake](build-vm-examples/nixos.qcow2-only-run-on-nixos-host.nix)
# root@nixos ~/qemu-flake# cat result/bin/run-nixos-vm

# ===========================================================

#! /nix/store/p6k7xp1lsfmbdd731mlglrdj2d66mr82-bash-5.2p37/bin/bash
export PATH=/nix/store/b1wvkjx96i3s7wblz38ya0zr8i93zbc5-coreutils-9.5/bin${PATH:+:}$PATH
set -e

# Create an empty ext4 filesystem image. A filesystem image does not
# contain a partition table but just a filesystem.
# 建立 nixos.qcow2 的映像檔 + 對檔案系統格式化為 ext4
createEmptyFilesystemImage() {
  local name=$1
  local size=$2
  local temp=$(mktemp)
  /nix/store/lxl7zad8lqvdz386pi8cpmac4wsc1wwg-qemu-host-cpu-only-9.1.1/bin/qemu-img create -f raw "$temp" "$size"
  /nix/store/l643ccd2bb9pij63y1id3f4wv395pwp7-e2fsprogs-1.47.1-bin/bin/mkfs.ext4 -L nixos "$temp"
  /nix/store/lxl7zad8lqvdz386pi8cpmac4wsc1wwg-qemu-host-cpu-only-9.1.1/bin/qemu-img convert -f raw -O qcow2 "$temp" "$name"
  rm "$temp"
}

# 設置 NIX_DISK_IMAGE 的位置
NIX_DISK_IMAGE=$(readlink -f "${NIX_DISK_IMAGE:-./nixos.qcow2}") || test -z "$NIX_DISK_IMAGE"

if test -n "$NIX_DISK_IMAGE" && ! test -e "$NIX_DISK_IMAGE"; then
    echo "Disk image do not exist, creating the virtualisation disk image..."

    createEmptyFilesystemImage "$NIX_DISK_IMAGE" "1024M"

    echo "Virtualisation disk image created."
fi

# 建構VM運行時，用於儲存暫時資料的目錄，位於 tmpfs 中
# Create a directory for storing temporary data of the running VM.
if [ -z "$TMPDIR" ] || [ -z "$USE_TMPDIR" ]; then
    TMPDIR=$(mktemp -d nix-vm.XXXXXXXXXX --tmpdir)
fi

# 建立共享目錄
# Create a directory for exchanging data with the VM.
mkdir -p "$TMPDIR/xchg"

# 進入運行VM時的臨時目錄
cd "$TMPDIR"

# Start QEMU.   啟動 nixos-qemu-vm 

exec /nix/store/<hash>-qemu-host-cpu-only-9.1.1/bin/qemu-kvm \
    # ===== 基本參數 =====
    -cpu max -name nixos \
    -m 1024 \
    -smp 2 \

    # ===== 設備參數(硬體參數) =====

    # 添加一個虛擬隨機數生成器設備，用於加速隨機數生成，以改善性能，尤其對加密工作負載有益。
    -device virtio-rng-pci \
    
    # 設置網路硬體，透過 $QEMU_NET_OPTS 可以額外設置自定義的網路設備
    -net nic,netdev=user.0,model=virtio -netdev user,id=user.0,"$QEMU_NET_OPTS" \ #設置網路設備
    -device virtio-blk-pci,bootindex=1,drive=drive1,serial=root \ #設置開機設備
    -device virtio-keyboard \ #設置鍵盤
    -usb -device usb-tablet,bus=usb-bus.0 \ #設置滑鼠

    # ===== 存儲和文件系統配置 =====

    # 將宿主機的 /nix/store 資料夾掛載到虛擬機，掛載點標籤為 nix-store
    # - 共享 nixos-host 的 Nix store 給虛擬機，減少重複構建
    -virtfs local,path=/nix/store,security_model=none,mount_tag=nix-store \

    # 將共享目錄掛載到虛擬機，標籤為 shared
    -virtfs local,path="${SHARED_DIR:-$TMPDIR/xchg}",security_model=none,mount_tag=shared \
    -virtfs local,path="$TMPDIR"/xchg,security_model=none,mount_tag=xchg \

    # 配置虛擬硬盤，使用 $NIX_DISK_IMAGE 作為硬盤文件
    -drive cache=writeback,file="$NIX_DISK_IMAGE",id=drive1,if=none,index=1,werror=report 

    # ===== 啟動內核相關配置 =====

    # 指定虛擬機內核映像文件，直接使用指定內核啟動虛擬機，避免從硬盤啟動
    -kernel ${NIXPKGS_QEMU_KERNEL_nixos:-/nix/store/cm3vjqbzy28c510fgi0yzvdawl1jlgrg-nixos-system-nixos-25.05.20241203.566e53c/kernel} \

    # 指定初始 RAM 磁碟映像文件
    -initrd /nix/store/yb88jwbfmjx4ij86d4p694da78xvk1ld-initrd-linux-6.6.63/initrd \

    # 傳遞內核命令行參數，包括初始化程序、控制台設置，為內核提供啟動時的配置信息
    -append "$(cat /nix/store/cm3vjqbzy28c510fgi0yzvdawl1jlgrg-nixos-system-nixos-25.05.20241203.566e53c/kernel-params) 
    
    init=/nix/store/cm3vjqbzy28c510fgi0yzvdawl1jlgrg-nixos-system-nixos-25.05.20241203.566e53c/init 
    
    # regInfo用於指定系統啟動時的最小依賴套件清單
    # - 在 /nix/store 還不可用、或是在有限環境、或在期望能加速進入系統時，透過 regInfo 指定一個內核可以快速定位和需要的套件列表
    # - regInfo 充當了一個啟動時的最小依賴清單，提供內核啟動所需的基礎資源路徑
    regInfo=/nix/store/fspaq3dhhs407jbpskl22xb4l2rpgxrj-closure-info/registration console=tty0 console=ttyS0,115200n8 $QEMU_KERNEL_PARAMS" \

    -nographic \
    $QEMU_OPTS \    # 其他用戶自定義的 qemu 參數
    "$@"            # 其他用戶自定義的參數
