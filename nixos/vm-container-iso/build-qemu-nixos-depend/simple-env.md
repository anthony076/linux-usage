## simple-env

- nixos-qemu-vm 的 disk 檔案小，基礎的 nixos.qcow2 佔用空間約20M

- 僅能運行在建立 nixos-qemu-vm 時的 nixos-host
  
  nixos-qemu-vm的套件會直接使用 nixos-host 的套件，若運行在其他 host 上，
  例如，在 windows-host 上使用 nixos.qcow2 就會失敗，因為找不到 nixos-host 上的套件

- 若要建構 nixos-qemu-vm 的磁碟映像 (本例為nixos.qcow2)，需要`在配置檔中`使用以下方法對qemu進行配置

  - 方法，make-disk-image函數，並設置`onlyNixStore=true`

    範例見，[make-disk-image函數的使用](../vm-container-overview.md#函數vm-nixpkgsnixoslibmake-disk-imagenix--用於建構各種格式-disk-image-的函數)

  - 方法，`virtualisation屬性` + `$ nixos-rebuild build-vm`命令進行建構

    - `$ nixos-rebuild build-vm` 中的 build-vm 會讀取virtualisation屬性的設置，並建構qemu的磁碟映像
  
    - 範例，[virtualisation屬性 + build-vm + flake](build-qemu-examples/nixos.qcow2-only-run-on-nixos-host.nix)

    - 建構後，會在`result/bin/run-nixos-vm`建立起動 nixos-qemu-vm 的腳本，
      若要另外設置啟動參數，需要在 nixos-host 上透過`$QEMU_NET_OPTS`或`$QEMU_OPTS`兩個環境變數添加自定義的啟動參數

      例如，

      ```
      nixos-rebuild build-vm --flake .#test   # 建構
      export QEMU_NET_OPTS="-nographic -no-acpi"
      export QEMU_NET_OPTS="hostfwd=tcp::2221-:22"    # 透過環境變數設置自定義的啟動參數
      result/bin/run-nixos-vm   # 執行啟動 nixos-qemu-vm 的啟動腳本
      ssh vm@localhost -p 2221  $ 或透過 ssh 進行連接
      ```

    - 也可以手動啟動 qemu-nixos-vm

      ```
      cp result/nixos.qcow2 .
      chmod 644 nixos.qcow2
      qemu-kvm -name nixos \
        -m 4G \
        -smp 2  \
        -drive cache=writeback,file=nixos.qcow2,id=drive1,if=none,index=1,werror=report \
        -device virtio-blk-pci,bootindex=1,drive=drive1 \
        -nographic
      ```

## nixos-qemu-vm 啟動參數

參考，[nixos-qemu-vm 的啟動參數](build-qemu-examples/run-nixos-vm的啟動參數.sh)

## ref

- [透過virtualisation屬性進行建構，with flake，Setting up qemu VM using nix flakes](https://gist.github.com/FlakM/0535b8aa7efec56906c5ab5e32580adf)
