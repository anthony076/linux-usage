
```shell
# (在windows-host中)

docker run -it --privileged nixos/nix
docker run -it --privileged nixos/nix:2.18.5

# (在docker-container中)

# 安裝 vim
nix profile install nixpkgs#vim --extra-experimental-features nix-command --extra-experimental-features flakes

# 建立自定義的配置
cat > ~/myconfig.nix << EOF
{ config, lib, pkgs, ... }:

{
  imports = [
    <nixos-wsl/modules>
  ];

  wsl.enable = true;
  wsl.defaultUser = "nixos";

  environment.systemPackages = with pkgs; [
    git
    vim
    hello
  ];

  system.stateVersion = "24.05"; # Did you read the comment?
}
EOF

# 添加 nixos-wsl
nix-channel --add https://github.com/nix-community/NixOS-WSL/archive/refs/heads/main.tar.gz nixos-wsl
export NIX_PATH=nixos-wsl=https://github.com/nix-community/NixOS-WSL/archive/refs/heads/main.tar.gz:$NIX_PATH

# 建構 nixos-system-nixos
nix-build '<nixpkgs/nixos>' -A system -I nixos-config=./myconfig.nix

ls /nix/store | grep nixos-system-nixos
# 得到 81c00fann4xm23ng5nd135gpllysl3k1-nixos-system-nixos-24.05pre-git

# 進入具有依賴套件的環境
nix-shell -p coreutils e2fsprogs gnutar nixos-install-tools pigz util-linux

export nixosSystem=/nix/store/81c00fann4xm23ng5nd135gpllysl3k1-nixos-system-nixos-24.05pre-git

export root=$(mktemp -p "${TMPDIR:-/tmp}" -d nixos-wsl-tarball.XXXXXXXXXX)
chmod o+rx "$root"

echo $root
# 得到 /tmp/nix-shell-8197-0/nixos-wsl-tarball.FpPuoPTX5f

# 安裝nixos系統到指定目錄
nixos-install --root $root --no-root-passwd --system $nixosSystem --substituters "" --no-bootloader

# (類似chroot) 進入系統並寫入 channel
nixos-enter --root "$root" --command 'source /etc/profile && HOME=/root nix-channel --add https://github.com/nix-community/NixOS-WSL/archive/refs/heads/main.tar.gz nixos-wsl'

# 寫入配置
install -Dm755 ./myconfig.nix "$root/etc/nixos/configuration.nix"

# 封裝為 tarball
tar -C "$root" -c --sort=name --mtime='@1' --owner=0 --group=0 --numeric-owner . | pigz > "nixos-wsl-tarball.tar.gz"

# (在windows-host中)

# 將 docker 中的 tarball 複製到 windows-host

docker ps

# CONTAINER ID   IMAGE       COMMAND                  CREATED          STATUS          PORTS     NAMES
# f15bd6a4f8dd   nixos/nix   "/root/.nix-profile/…"   37 minutes ago   Up 37 minutes             charming_archimedes

docker cp f15bd6a4f8dd:/root/nixos-wsl-tarball .
```