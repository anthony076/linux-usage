# from https://github.com/nix-community/NixOS-WSL/blob/main/modules/build-tarball.nix

# 執行方式
#   下載源碼，git clone https://github.com/nix-community/NixOS-WSL && cd NixOS-WSL
#   產生建立tarball的腳本，nix-build -A nixosConfigurations.default.config.system.build.tarballBuilder --extra-experimental-features flakes
#   執行腳本，sudo result/bin/nixos-wsl-tarball-builder，必須透過 sudo 執行

{ lib, pkgs, ... }:
{ config, pkgs, lib, ... }:
with builtins; with lib;
let
  cfg = config.wsl.tarball;

  # 定義預設的配置文件
  defaultConfig = pkgs.writeText "default-configuration.nix" ''
    { config, lib, pkgs, ... }:
    {
      imports = [
        # include NixOS-WSL modules
        <nixos-wsl/modules>
      ];

      wsl.enable = true;
      wsl.defaultUser = "nixos";
      
      # lib.optionalString 用於條件判斷，如果條件為真，則返回第二個參數，否則返回空字符串
      # 若 config.wsl.nativeSystemd=false，添加 wsl.nativeSystemd = false 的配置
      ${lib.optionalString (!config.wsl.nativeSystemd) "wsl.nativeSystemd = false;"}

      system.stateVersion = "${config.system.nixos.release}";
    }
  '';
in
{
  options.wsl.tarball = {
    configPath = mkOption {
      type = types.nullOr types.path;
      default = null;
      description = "Path to system configuration which is copied into the tarball";
    };
  };
  
  config = mkIf config.wsl.enable {
    
    # 建立名稱為 nixos-wsl-tarball-builder 的腳本
    # 該腳本用於產生 wsl-distro 的 tarball
    system.build.tarballBuilder = pkgs.writeShellApplication {
      name = "nixos-wsl-tarball-builder";

      # 指定腳本執行時依賴的套件，
      # 在腳本中，會將套件的實際位置寫入腳本中，並透過 export 讓腳本可以訪問這些應用
      runtimeInputs = [
        pkgs.coreutils  # for mktemp
        pkgs.e2fsprogs  # for mktemp
        pkgs.gnutar # for --sort=name
        pkgs.nixos-install-tools  # for nixos-install
        pkgs.pigz # for compression
        config.nix.package  # nix-2.18.8
      ];

      # 定義腳本內容
      text = ''
        # $EUID 是一個環境變量，表示當前用戶的有效用戶ID，用於判斷是否是root用戶
        if ! [ $EUID -eq 0 ]; then
          echo "This script must be run as root!"
          exit 1
        fi

        # $1 是腳本的第一個參數，如果沒有指定，則默認為 nixos-wsl.tar.gz
        out=''${1:-nixos-wsl.tar.gz}

        # 創建一個臨時目錄
        # -p 指定臨時目錄的父目錄
        # -d 創建一個目錄，其中，XXXXXXXXXX 會被替換為隨機字符串
        # 例如，/tmp/nixos-wsl-tarball.b4xxtnRLU8
        root=$(mktemp -p "''${TMPDIR:-/tmp}" -d nixos-wsl-tarball.XXXXXXXXXX)

        # trap 命令用於建立捕獲信號，並在捕獲到信號時執行指定的命令
        trap 'chattr -Rf -i "$root" || true && rm -rf "$root" || true' INT TERM EXIT

        # 確保根目錄對所有人都是可訪問的
        # o 表示其他用戶，+rx 表示可讀可執行
        chmod o+rx "$root"

        echo "[NixOS-WSL] Installing..."
        
        # nixos-install 安裝一個NixOS系統，該命令會將系統安裝到root目錄中
        nixos-install \
          --root "$root" \  # 指定安裝的根目錄
          --no-root-passwd \  # 不設置root密碼
          --system ${config.system.build.toplevel} \  # 指定要安裝的系統
          --substituters "" \  # 不使用替代源

        echo "[NixOS-WSL] Adding channel..."
        
        # nixos-enter 進入一個系統後執行添加channel的命令
        # nixos-enter --root /mnt/nixos --command 'HOME=/root nix-channel --add
        # --root /mnt/nixos 指定要進入的系統根目錄
        # --command 'HOME=/root nix-channel --add 指定要執行的命令
        # --add 添加一個新的channel
        nixos-enter --root "$root" --command 'HOME=/root nix-channel --add https://github.com/nix-community/NixOS-WSL/archive/refs/heads/main.tar.gz nixos-wsl'

        echo "[NixOS-WSL] Adding default config..."

        ${if cfg.configPath == null then ''
          # install 命令，用於 copy 文件並且設置文件的權限
          # 格式 install -Dm權限 檔案來源 目的地
          # -D 選項會創建所有必要的父目錄，m 選項用於指定文件的權限
          install -Dm644 ${defaultConfig} "$root/etc/nixos/configuration.nix"
        '' else ''
          mkdir -p "$root/etc/nixos"

          # lib.cleanSource 用於清理源代碼，刪除所有不必要的文件，
          # 例如，.git目錄、swap文件、編譯後的文件、symbolic link、result文件，只保留源代碼
          cp -R ${lib.cleanSource cfg.configPath}/. "$root/etc/nixos"

          # 將$root/etc/nixos權限設置為可寫，但是需要 root 權限
          chmod -R u+w "$root/etc/nixos"
        ''}

        echo "[NixOS-WSL] Compressing..."
        tar -C "$root" \
          -c \
          --sort=name \
          --mtime='@1' \
          --owner=0 \
          --group=0 \
          --numeric-owner \
          . \
        | pigz > "$out"
      '';
    };
  };
}
