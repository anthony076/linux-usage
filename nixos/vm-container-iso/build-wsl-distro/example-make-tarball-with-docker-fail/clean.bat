@echo off

docker rm -f my-nixos-container
docker rmi my-nixos-image

del /F my-wsl-nixos.tar

wsl -t mynixos 
wsl --unregister mynixos

