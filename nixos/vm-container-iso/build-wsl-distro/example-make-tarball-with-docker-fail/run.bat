
@echo off

echo "Building NixOS image"
docker build -t my-nixos-image .

echo "Creating NixOS container"
docker run --name my-nixos-container my-nixos-image

echo "Exporting container to my-wsl-nixos.tar"
docker export my-nixos-container -o my-wsl-nixos.tar

echo "Importing tarball into WSL"
wsl --import mynixos . my-wsl-nixos.tar --version 2

echo "Starting wsl-mynixos"
wsl -d mynixos