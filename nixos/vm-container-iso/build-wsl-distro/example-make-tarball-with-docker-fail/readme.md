## 將官方提供的 nixos 容器轉換為 wsl-nixos (失敗)

- step，建立nixos容器，`$ docker run -it --name mynixos nixos/nix`

- step，將容器封裝為 wsl 用的 tarball，`$ docker export mynixos -o nixos.tar`

- step，從 tarball 加載 wsl-distro，`$ wsl --import mynixos . mynixos.tar`

  - 錯誤 : windows 磁區掛載錯誤
    ```
    <3>WSL (9) ERROR: UtilTranslatePathList:2852: Failed to translate c:\users\ching\.local\bin
    <3>WSL (9) ERROR: UtilTranslatePathList:2852: Failed to translate C:\Users\ching\GoogleDrive\app\ffmpeg-7.0.2-full_build\bin
    <3>WSL (9) ERROR: UtilTranslatePathList:2852: Failed to translate c:\Users\ching\AppData\Roaming\Code\User\globalStorage\github.copilot-chat\debugCommand
    Processing fstab with mount -a failed.
    Failed to mount C:\, see dmesg for more details.
    ```

  - 錯誤 : 無法大多數的命令
    
    無法使用，whoami、clear、ls、df、cat
    可以使用，env

    根據官方的 docker-nixos-container，需要將 /root/.nix-profile/bin 添加到 PATH 變數
    -bash-5.2# /root/.nix-profile/bin/ls /etc
    group  hostname  hosts  mtab  nix  passwd  resolv.conf  shadow  ssl

## 透過 Dockerfile 建立 wsl-nixos (失敗)

- step，根據 Dockerfile 建立 docker-image，`$ docker build -t my-nixos-image .`

- step，建立nixos容器，`$ docker run -it --name my-nixos-container my-nixos-image`

- step，將容器封裝為 wsl 用的 tarball，`$ docker export my-nixos-container -o my-wsl-nixos.tar`

- step，從 tarball 加載 wsl-distro，`$ wsl --import mynixos . my-wsl-nixos.tar --version 2`

- step，啟動 mynixos，`$ wsl -d mynixos`

  - 環境變數不一致

    ```
    # Dockerfile中
    ENV PATH="/root/.nix-profile/bin:$PATH"
    
    # wsl-mynixos中
    /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/lib/wsl/lib
    ```
    
    ENV 是設置 docker container 啟動的環境變數，並不適用於 wsl
    
    wsl-distro 在啟動時會友自己的啟動腳本