## wsl-nixos

## [範例] 官方推薦的使用方式

- 注意事項
  - from [github:NixOS-WSL](https://github.com/nix-community/NixOS-WSL)
  - 內建 wslg，可執行需要具有gui的應用，例如，vlc
  - 配置文件中，無法手動配置root，會與 wsl-distro.nix 中的配置衝突，
    使用 lib.mkDefault 或 lib.mkForce 也無效
  - 源碼見，[wsl-distro.nix](https://github.com/nix-community/NixOS-WSL/blob/main/modules/wsl-distro.nix)
  - 完整範例見，[example-official-2405](example-official-2405/readme.md)

- step，(optional) 下載 nixos-wsl.tar.gz

  ```
  mkdir nixos-wsl && cd nixos-wsl

  nix-shell -p aria2
  
  # check latest version of nixos-wsl.tar.gz
  # https://github.com/nix-community/NixOS-WSL/releases
  aria2c -x 5 https://github.com/nix-community/NixOS-WSL/releases/download/2405.5.4/nixos-wsl.tar.gz
  ```

- step，從tarball加載wsl-distro

  ```
  # (optional) copy nixos-wsl.tar.gz from qemu-nixos-vm

  scp -P 2222 -r root@localhost:/root/nixos-wsl/nixos-wsl.tar.gz .
  
  # 從 tarball 加載 wsl-distro
  wsl --import NixOS $env:USERPROFILE\NixOS\ nixos-wsl.tar.gz --version 2

  # 啟動 nixos-on-wsl
  wsl -d NixOS
  ```

- step，第一次進入 Nixos-on-WSL

  - 更新 channel，`$ sudo nix-channel --update`

  - 建立配置
    
    參考，[myconfig.nix](example-official-2405/myconfig.nix)

  - 重建系統，`$ sudo nixos-rebuild switch -I nix-config=./myconfig.nix && sudo halt`
  
  - 套用配置，
    
    ```shell
    wsl -t NixOS
    wsl -d NixOS
    ```

## [範例] 利用 docker 自製 wsl-nixos-tarball

- 失敗範例，[example-make-wsl-nixos-with-docker-fail](example-make-tarball-with-docker-fail/readme.md)
- 成功範例，推薦，[example-make-wsl-nixos-with-docker](example-make-tarball-with-docker/readme.md)

## [範例] 利用官方的NixOS-WSL 自定義 wsl-nixos-tarball

- 完整代碼 + 源碼說明，[example-make-wsl-tarball-with-NixOSWsl](example-make-wsl-tarball-with-NixOSWsl/build-tarball.nix)

- step，下載源碼，git clone https://github.com/nix-community/NixOS-WSL && cd NixOS-WSL

- step，產生建立tarball的腳本，`$ nix-build -A nixosConfigurations.default.config.system.build.tarballBuilder --extra-experimental-features flakes`

- step，執行腳本，`$ sudo result/bin/nixos-wsl-tarball-builder`，必須透過 sudo 執行

## ref

- NixOS-WSL
  - [github:NixOS-WSL](https://github.com/nix-community/NixOS-WSL)
  - [documentation of NixOS-WSL](https://nix-community.github.io/NixOS-WSL/install.html)
  - [Building your own system tarball](https://nix-community.github.io/NixOS-WSL/building.html)
  - [NixOS-WSL/modules/default.nix](https://github.com/nix-community/NixOS-WSL/blob/main/modules/default.nix)
  - [A minimal root filesystem for running NixOS on WSL](https://github.com/nix-community/NixOS-WSL/tree/32fd863dda65cfa718cd85b088a00fb3bdef42ba)
  - wsl模組提供的配置選項
    - [配置選項文檔](https://nix-community.github.io/NixOS-WSL/options.html)
    - [wsl-distro.nix](https://github.com/nix-community/NixOS-WSL/blob/main/modules/wsl-distro.nix)

- [nixos-wsl-starter](https://github.com/LGUG2Z/nixos-wsl-starter/tree/master)

- [Creating a proper NixOS distrbution for WSL2](https://discourse.nixos.org/t/creating-a-proper-nixos-distrbution-for-wsl2/7456)

- [build wsl-alpine from docker](https://gist.github.com/K0IN/485b6c9e442d10aa666c28672d621604)

- [Rootless Docker on WSL2 NixOS](https://www.richtman.au/blog/rootless-docker-on-wsl2-nixos/)

- [Config example for NisOS-WSL](https://www.reddit.com/r/NixOS/comments/1eguj7b/nisoswsl_is_awesome/?rdt=63931)