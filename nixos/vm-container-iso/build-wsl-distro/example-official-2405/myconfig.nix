{ config, lib, pkgs, ... }:
{
  imports = [
    <nixos-wsl/modules>
  ];

  wsl.enable = true;
  wsl.defaultUser = "nixos";

  environment.systemPackages = with pkgs; [
    vim
    fish
  ];

  programs.fish = {
    enable = true;
    interactiveShellInit = ''
      set -x PATH $PATH /run/current-system/sw/bin
      set fish_greeting ""
    '';
  };

  users.users.nixos = {
    isNormalUser = true;
    shell = pkgs.fish;
  };

  system.stateVersion = "24.05";
}
