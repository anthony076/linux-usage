@echo off
setlocal enabledelayedexpansion

set vmPath=%USERPROFILE%\NixOS\
set tarFileName=nixos-wsl_2405.5.4.tar.gz

if NOT EXIST "!vmPath!" (
  mkdir !vmPath!
)

:: import distro
wsl --import NixOS !vmPath! !tarFileName! --version 2
:: start distro
wsl --distribution NixOS