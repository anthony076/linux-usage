## wsl-nixos-2405-official

- tarball from [2405.5.4 Upset Uakari](https://github.com/nix-community/NixOS-WSL/releases/tag/2405.5.4)

- 使用
  - `00_setup-and-run.bat` : 從 tarball 加載 distro 並運行
  - `./01_run_after_nixos_start.sh` : 進入 nixos 執行 post-install
  - `wsl -t Nixos` : 停止 nixos
  - `wsl --unregister Nixos` : 移除 nixos
