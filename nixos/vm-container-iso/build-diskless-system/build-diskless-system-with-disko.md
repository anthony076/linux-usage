## build diskless system with disko


- 概念
  - disko 會建立三個磁碟分區 : boot、swap、esp、root
  
  - root磁碟分區會被設置為lvm磁碟，並建立三個邏輯卷
    - `/root 邏輯卷`，並掛載到系統的`/`
    - `/persist 邏輯卷`，並掛載到系統的`/persist`
    - `/nix 邏輯卷`，並掛載到系統的`/nix`
  
  - 如何實現 diskless
    
    在 configuration.nix 中設置的 boot.initrd.postDeviceCommands 命令，
    會在每次reboot後，在 initrd 階段，自動將 /root 備份到 /old_roots 目錄中，藉此清空/root的內容

    相當於每次 reboot ，就將 / 的內容清除，並重新建立新的 root

    清除後，進入系統後透過`$ ls /`，仍然可以看到 `bin  boot  dev  etc  home  lib  lib64  nix  persist  proc  root  run  srv  sys  tmp  usr  var` 等目錄
    
    nixpkgs 和相關服務會確保系統目錄結構完整性，通常由 stage-2 引導腳本重建或重新綁定到實際內容，
    NixOS 會通過配置文件重新建立或設置系統的目錄結構

## 流程

- [完整代碼](diskless-examples/diskless-with-disko/)

- step，創建 qcow2 磁碟映像，`$ qemu-img create -f qcow2 nixos-disk.qcow2 15G`

- step，用於安裝的qemu啟動腳本，[00_install.bat](diskless-examples/diskless-with-disko/00_install.bat)

  ```
  qemu-system-x86_64 ^
  -m 4096 -smp 4 ^
  -boot d -cdrom nixos-minimal-24.11.710905.a0f3e10d9435-x86_64-linux.iso ^
  -drive file=nixos-disk.qcow2,format=qcow2 ^
  -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
  ```

- step，變更root密碼，`$ sudo passwd root`

- step，透過 windows-host 的 ssh 連接到 nixos-vm，`$ ssh -p 2222 root@localhost`

- step，建立硬碟配置，參考，[/tmp/disko.nix](diskless-examples/diskless-with-disko/disko.nix)

  > vi /tmp/disko.nix

- step，根據硬碟配置建立磁碟分區

  ```
  cd /tmp
  nix-shell -p disko
  disko --version       (1.9.0)
  disko --mode disko disko.nix --arg device '"/dev/sda"'
  ```

  建構完成後，輸入 $ lsblk，會得到以下的結果

  ```shell
  NAME             MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
  sda                8:0    0   15G  0 disk
  ├─sda1             8:1    0    1M  0 part
  ├─sda2             8:2    0  500M  0 part /mnt/boot
  ├─sda3             8:3    0    4G  0 part [SWAP]
  └─sda4             8:4    0 10.5G  0 part
    └─root_vg-root 254:0    0 10.5G  0 lvm  /mnt/persist
                                            /mnt/nix
                                            /mnt
  ```

- step，產生配置

  透過 disko 配置文件，disko.nix，自定義磁碟分區的配置，

  就不需要自動產生磁碟分區的相關配置，因此添加 --no-filesystems

  > nixos-generate-config --no-filesystems --root /mnt

- step，將 disko 移動到與 configuration.nix 同一個目錄

  disko 配置文件，disko.nix，需要在 configuration.nix 中加載，

  因此，將 disko.nix 移動到和 configuration.nix 同一個目錄

  > mv disko.nix /mnt/etc/nixos/
  
- step，建立 flake.nix

  需要在 flake.nix 中設置
  - 加載 disko
  - 加載 disko 的硬碟配置文件，disko.nix

  > cd /mnt/etc/nixos/

  flake.nix 的內容，參考，
  - [flake-before-nixos-install.nix](diskless-examples/diskless-with-disko/flake-before-nixos-install.nix)
  - `$ nix flake init --template github:vimjoyer/impermanent-setup --extra-experimental-features "nix-command flakes"`

  > vi flake.nix

- step，修改 configuration.nix

  <font color=blue>完整 configuration.nix 的內容</font>

  詳見，[configuration-before-nixos-install.nix](diskless-examples/diskless-with-disko/configuration-before-nixos-install.nix)

  > vi /mnt/etc/nixos/configuration.nix

  <font color=blue>添加 boot.initrd.postDeviceCommands 命令</font>，用於每次開機後自動刪除不用的數據

  ```nix
  # 自動清除 /dev/root_vg/root/root ，
  # 相當於每次開機，自動清除 /root 的內容
  boot.initrd.postDeviceCommands = lib.mkAfter ''
    mkdir /btrfs_tmp
    
    # 將 /dev/root_vg/root 這個 lvm-disk 的內容掛載到 /btrfs_tmp 目錄
    # 掛載後，ls /btrfs_tmp 得到 nix  old_roots  persist  root
    mount /dev/root_vg/root /btrfs_tmp

    if [[ -e /btrfs_tmp/root ]]; then
        # 建立一個具有時間戳的目錄，例如，/btrfs_tmp/old_roots/2024-12-17_06:22:44
        mkdir -p /btrfs_tmp/old_roots
        timestamp=$(date --date="@$(stat -c %Y /btrfs_tmp/root)" "+%Y-%m-%-d_%H:%M:%S")
        
        # 將 /btrfs_tmp/root 的內容移動到 /btrfs_tmp/old_roots/2024-12-17_06:22:44
        # 相當於清除 /btrfs_tmp/root
        mv /btrfs_tmp/root "/btrfs_tmp/old_roots/$timestamp"
    fi

    delete_subvolume_recursively() {
        IFS=$'\n'
        
        # btrfs subvolume list -o /btrfs_tmp/old_roots/2024-12-17_06:22:44
        # 得到 ID 259 gen 82 top level 258 path old_roots/2024-12-17_06:22:44/srv

        for i in $(btrfs subvolume list -o "$1" | cut -f 9- -d ' '); do
            delete_subvolume_recursively "/btrfs_tmp/$i"
        done
        btrfs subvolume delete "$1"
    }

    # 檢查 /btrfs_tmp/old_roots/ 下的子目錄，若時間超過30天，自動刪除該子目錄
    for i in $(find /btrfs_tmp/old_roots/ -maxdepth 1 -mtime +30); do
        delete_subvolume_recursively "$i"
    done

    # 重新建立新的 /btrfs_tmp/root
    btrfs subvolume create /btrfs_tmp/root

    # 卸載 /btrfs_tmp 
    umount /btrfs_tmp
  '';
  ```

  <font color=blue>需要明確的設置 root 用戶 </font>，由於系統的設置未完成，/ 根分割區尚未正確掛載，需要手動設置才能確保root帳號可用

  ```nix
  users.users.root = {
    isNormalUser = false;
    initialPassword = "root";
  };
  ```

- step，將配置檔保存在永久保存的區域

  將 /mnt/etc/nixos 目錄下的所有內容複製到 /mnt/persist 目錄中，備份用

  因為 boot.initrd.postDeviceCommands，
  若沒有備份 /mnt/etc/nixos，在重新啟動後 /etc/nixos 中的內容會自動被清除，可透過`cd /persist`找到備份
  
  > cp -r /mnt/etc/nixos /mnt/persist

- step，安裝 nixos 系統，
  
  > nixos-install --root /mnt --flake /mnt/etc/nixos#default

- step，改用以下的腳本啟動，[01_run.bat](diskless-examples/diskless-with-disko/01_run.bat)

  ```shell
  qemu-system-x86_64 ^
  -m 4096 -smp 4 ^
  -accel whpx,kernel-irqchip=off ^
  -machine vmport=off ^
  -display sdl ^
  -drive file=nixos-disk.qcow2,format=qcow2 ^
  -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
  ```

- step，測試

  此時，每次nixos重新啟動都會清除 /root 的內容

  ```
  touch /root/ttt.log
  ls
  reboot
  cat /root/ttt.log   # 檔案不存在，已經被刪除
  ```

- step，透過第三方的[impermanence模組](https://github.com/nix-community/impermanence)，
  設置需要永久保存的數據

  重開機後，/etc/nixos 已經被刪除，切換到先前備份的目錄中

  > cd /persist/nixos/

  修改[flake.nix](diskless-examples/diskless-with-disko/flake-after-nixos-install.nix)，
  添加並加載第三方的impermanence模組

  > vi /persist/nixos/flake.nix

  在 flake.nix 中，添加 impermanence

  ```nix
  impermanence = {
    url = "github:nix-community/impermanence";
  };
  ```

  在 flake.nix 中，加載 impermanence

  ```nix
  nixosConfigurations.default = nixpkgs.lib.nixosSystem {
    specialArgs = {inherit inputs;};
    modules = [
      inputs.disko.nixosModules.default
      (import ./disko.nix { device = "/dev/sda"; })
      ./configuration.nix

      # 加載 impermanence 配置
      inputs.impermanence.nixosModules.impermanence
    ];
  };
  ```

  修改[configuration.nix](diskless-examples/diskless-with-disko/configuration-after-nixos-install.nix)，設置要保留的目錄和檔案

  > vi /persist/nixos/configuration.nix

  ```nix
  # 在 initial ramdisk 階段自動掛載 /persist
  # 需要在initial ramdisk階段，掛載impermanence模組設置的檔案系統才能正常boot
  fileSystems."/persist".neededForBoot = true;

  # 透過第三方的impermanence模組，設置需要保留的檔案或目錄
  # environment.persistence 屬性由impermanence模組提供
  # 詳見，https://github.com/nix-community/impermanence
  environment.persistence."/persist/system" = {
    hideMounts = true;
    # 要保留的目錄
    directories = [
      "/etc/nixos"
      "/var/log"
      "/var/lib/bluetooth"
      "/var/lib/nixos"
      "/var/lib/systemd/coredump"
      "/etc/NetworkManager/system-connections"
      { directory = "/var/lib/colord"; user = "colord"; group = "colord"; mode = "u=rwx,g=rx,o="; }
    ];

    # 要保留的檔案
    files = [
      "/etc/machine-id"
      #{ file = "/var/keys/secret_file"; parentDirectory = { mode = "u=rwx,g=,o="; }; }    # error cannot open passwd database
    ];
  };
  ```

- step，建立目錄

  <font color=red>注意</font>，配置 impermanence 後，第一次重啟後，在開機過程中會在指定的位置中(本例為/persist/system)
  建立要保留的目錄和檔案，
  
  對於部分目錄(例如，/var/log 或 /var/lib/nixos)在建立時，`會因為建立目錄失敗`，而出現`No such file or directory`的錯誤，例如，

  ```shell
  waiting for device /mnt-root/persist/system/var/lib/nixos to appear.......................
  Timed out waiting for device /mnt-root/persistent/system/var/lib/nixos, trying to mount anyway.
  mounting /mnt-root/persistent/system/var/lib/nixos on /var/lib/nixos...
  mount: mounting /mnt-root/persistent/system/var/lib/nixos on /mnt-root/var/lib/nixos failed: No such file or directory

  An error occurred in stage 1 of the boot process, which must mount the
  root filesystem on `/mnt-root` and then start stage 2. Press one
  of the following keys:

    r) to reboot immediately
    *) to ignore the error and continue
  ```

  要避免出現此類錯誤，可以在`$ nixos-rebuild boot --flake /persist/nixos#default`命令之前，
  提前手動建立好，就可以避免該問題

  ```shell
  mkdir -p /persist/system
  mkdir -p /persist/system/var/log
  mkdir -p /persist/system/var/lib/nixos
  ```

  套用新的配置並重新建構系統，

  > nixos-rebuild boot --flake /persist/nixos#default

- (optional) step，使用 efi-bootloader 的版本

  - 在 configuration.nix 中

    需要將 
    ```
    boot.loader.grub.enable = true;
    boot.loader.grub.efiSupport = true;
    boot.loader.grub.efiInstallAsRemovable = true;
    ```

    變更為
    ```
    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = true;
    ```

  - 在 qemu 的啟動參數
    - 需要到[clearlinux](https://github.com/clearlinux/common)下載OVMF.fd檔案
    - 在qemu的啟動參數中，添加`-bios OVMF.fd`