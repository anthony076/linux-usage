## manual build diskless-system after nixos installation 

- 此範例中
  - 將內存碟(tmpfs)，掛載到`路徑/`，
    - 每次重開機，/的內容都會被清空，
    - 在後續階段，nix會根據配置的內容，由 system 和 其他服務還原

  - 將原本實體硬碟，掛載到`路徑/nix`，
    - 使得原本`/nix/store`的存取路徑，變成`/nix/nix/store`，會造成重新啟動後找不到 init 的問題
    - 需要透過live-cd進入系統後，手動修改為正確的路徑
  
  - 由於路徑/被掛載為內存碟，造成每次重開機後，/etc/passwd檔案不存在，
    需要在 configuration.nix 中明確指定使用者，nix才會根據配置重新建立使用者

  - 使用`/nix/persist`做為永久備份的目錄

  - 永久目錄的使用方式
    - step，將檔案複製到 /nix/persist 目錄中，
    - step，在配置文件中，透過 fileSystems 將檔案從 /nix/persist 掛載到原來檔案的位置

- 缺點，
  - 打包大型套件時，可能會有[內存不夠用的狀況](https://lantian.pub/article/modify-computer/nixos-impermanence.lantian/)

## 流程

- step，在安裝完 nixos 系統後，hardware-configuration.nix 會有如下的配置

  完整的[hardware-configuration.nix](diskless-examples/manual-diskless-after-installation/hardware-configuration-origin.nix)

  ```nix
  fileSystems."/" =
    { device = "/dev/disk/by-uuid/02a92a0f-63f6-4976-a2fa-d7b038b202e0";
      fsType = "btrfs";
    };
  ```

- step，修改 /etc/nixos/hardware-configuration.nix

  - [修改後的 hardware-configuration.nix](diskless-examples/manual-diskless-after-installation/hardware-configuration-impermanence.nix)
  
  - 將 fileSystems."/" 的類型變更為 tmpfs，並設置權限

    ```
    fileSystems."/" = {
      device = "tmpfs";
      fsType = "tmpfs";
      
      # 必須設置 mode=755，否則默認的權限將是 777，導致 OpenSSH 報錯並拒絕用戶登錄
      # relatime 用於優化訪問時間戳（atime）的更新
      #   - atime（訪問時間，access-time）表示文件最後一次被訪問的時間
      #   - 預設每次讀取文件時，系統都會更新該時間戳，而導致額外的寫入操作
      #   - 設置 relatime 後 : atime < mtime | ctime 或 上次訪問超過24小時，只有在這兩個條件下才更新
      options = [ "relatime" "mode=755" ];
    };
    ```

  - 新增 fileSystems."/nix" ，用於掛載原來的 device = "/dev/disk/by-uuid/02a92a0f-..."

    ```nix
    fileSystems."/nix" = {
      device = "/dev/disk/by-uuid/02a92a0f-63f6-4976-a2fa-d7b038b202e0";
      fsType = "btrfs";
    };
    ```

- step，修改 /etc/nixos/configuration.nix

  由於以下的配置，

  ```
  fileSystems."/" = {
    device = "tmpfs";
    fsType = "tmpfs";
    options = [ "relatime" "mode=755" ];
  };
  ```

  使得 root 分區已經改成內存碟，/etc/passwd 會在重開機後會被清除，因此，`需要明確的在 configuration.nix 中指定用戶`，

  讓系統可以在啟動的過程中，根據 configuration.nix 的內容重新建立用戶，以避免無法登入的問題

  完整的[configuration.nix](diskless-examples/manual-diskless-after-installation/configuration-impermanence.nix)
  
  ```nix
  users.users."root" = {
    isNormalUser = false;
    initialPassword = "root";
  };
  ```

- step，此時，若執行 $ nixos-rebuild boot，和 $ reboot 會出現以下的錯誤

  ```
  mounting tmpfs on /
  mounting /dev/disk/by-uuid/02a92a0f-63f6-4976-a2fa-d7b038b202e0 on /nix
    stage 2 init script (/mnt-root//nix/store/<hash>-nixos-system-nixos-24.xx.xx.xx/init not fuound)
  ```

  由於修改後的設置

  ```nix
  fileSystems."/nix" = {
    device = "/dev/disk/by-uuid/02a92a0f-63f6-4976-a2fa-d7b038b202e0";
    fsType = "btrfs";
  };
  ```

  將原來的`/`變更為`/nix`，

  使得原本的 /nix/store/<hash>-nixos-system-nixos-24.xx.xx.xx/init 檔案，
  已經變更到 /nix/nix/store/<hash>-nixos-system-nixos-24.xx.xx.xx/init 的位置

  造成找不到原本的 init 而啟動失敗

  因此，在尚未正確移動檔案和目錄前，不要透過硬碟開機

- step，套用配置，但是使用live-CD光碟開機，並將store目錄移動到正確位置

  - 執行 `$ nixos-rebuild boot`

  - 執行 `$ halt`

  - 透過live-cd重新啟動 nixos

    完整腳本，參考，[00_install.bat](diskless-examples/manual-diskless-after-installation/00_install.bat)

    ```shell
    qemu-system-x86_64 -m 4096 -smp 4 ^
    -boot d -cdrom nixos-minimal-24.11.711349.394571358ce8-x86_64-linux ^
    -drive file=nixos_disk.qcow2,format=qcow2
    ```

  - 進入系統後，將 store 移動到正確位置

    ```
    # 進入 root
    sudo passwd root
    su -

    # 掛載並進入原來的 root 分區
    mkdir -p /mnt/sda2_tmp
    mount /dev/sda2 /mnt/sda2_tmp
    cd /mnt/sda2_tmp

    # 建立用於備份的 persist 目錄
    mkdir -p persist

    # 備份並刪除 var 目錄
    cp -r var persist/
    chattr -i var/empty
    rm -rf var

    # 備份 etc
    mv etc persist/

    # 移動 store
    mv nix/* .   # 將原來的root分區下的/nix/*.* 移動到正確的位置

    # 刪除其他不相關的目錄
    rm -rf bin boot dev home lib lib64 proc root run srv sys tmp usr nix

    ls # 得到 persist store var

    # 關機
    halt
    ```

- step，修正完 nix-store 的正確路徑後，改用硬碟開機
  
  完整腳本，參考，[01_run.bat](diskless-examples/manual-diskless-after-installation/01_run.bat)

  ```shell
  qemu-system-x86_64.exe ^
  -m 4096 -smp 4 ^
  -accel whpx,kernel-irqchip=off ^
  -machine vmport=off ^
  -display sdl ^
  -drive file=nixos_disk.qcow2,format=qcow2 ^
  -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
  ```

- step，測試 impermanence

  登入系統後

  ```
  echo "123" > /root/test.log
  reboot
  echo /root/test.log   # 檔案已消失
  ```

- step，手動配置要永久保存的目錄或檔案

  將檔案或目錄複製到永久備份目錄中，例如，

  ```shell
  cp -r /etc/nixos /nix/persist/etc/nixos

  mkdir -p /nix/persist/etc/ssh
  cp /etc/ssh/ssh_host_ed25519_key /nix/persist/etc/ssh/ssh_host_ed25519_key
  cp /etc/ssh/ssh_host_ed25519_key.pub /nix/persist/etc/ssh/ssh_host_ed25519_key.pub
  cp /etc/ssh/ssh_host_rsa_key /nix/persist/etc/ssh/ssh_host_rsa_key
  cp /etc/ssh/ssh_host_rsa_key.pub /nix/persist/etc/ssh/ssh_host_rsa_key.pub
  ```

  修改 $ vi /nix/persist/etc/nixos/hardware-configuration.nix，
  重新啟動後，nixos 會自動將 /nix/persistent/etc/nixos 掛載到 /etc/nixos

  ```nix
  fileSystems."/etc/nixos" = {
    device = "/nix/persistent/etc/nixos";
    options = [ "bind" ];
  };

  fileSystems."/etc/ssh/ssh_host_ed25519_key" = {
    device = "/nix/persistent/etc/ssh/ssh_host_ed25519_key";
    options = [ "bind" ];
  };

  fileSystems."/etc/ssh/ssh_host_ed25519_key.pub" = {
    device = "/nix/persistent/etc/ssh/ssh_host_ed25519_key.pub";
    options = [ "bind" ];
  };

  fileSystems."/etc/ssh/ssh_host_rsa_key" = {
    device = "/nix/persistent/etc/ssh/ssh_host_rsa_key";
    options = [ "bind" ];
  };

  fileSystems."/etc/ssh/ssh_host_rsa_key.pub" = {
    device = "/nix/persistent/etc/ssh/ssh_host_rsa_key.pub";
    options = [ "bind" ];
  };
  ```

  套用修改，

  ```shell
  nixos-rebuild boot -I nixos-config=/nix/persist/etc/nixos/configuration.nix
  ```

- step，(optional) 若不想手寫 fileSystems 的掛載，可以使用 [impermanence模組](https://github.com/nix-community/impermanence)

  直接使用impermanence模組，可以簡化 fileSystems 的配置