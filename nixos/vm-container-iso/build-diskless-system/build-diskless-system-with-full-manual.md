## build-diskless-system-with-full-manual

## 流程

- step，透過 qemu 啟動安裝系統

  建立磁碟映像，`$ qemu-img create -f qcow2 nixos-disk.qcow2 10G`

  透過以下命令啟動 live-cd

  ```shell
  "C:\Program Files\qemu\qemu-system-x86_64.exe" -m 4096 -smp 4 ^
  -boot d -cdrom nixos-minimal-24.11.711349.394571358ce8-x86_64-linux.iso ^
  -drive file=nixos-disk.qcow2,format=qcow2 ^
  -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
  ```

  進入系統後，變更root密碼，`$ sudo passwd root`

  改在 windows-host 上透過ssh連接，`$ ssh -p 2222 root@localhost`

- step，建立硬碟分區

  ```
  export DISK=/dev/sda

  # ==== 指定分區表類型 ====

  #parted "$DISK" -- mklabel gpt  # 創建 GPT 分區表
  parted "$DISK" mklabel msdos  # 創建 MBR 分區表

  # ==== 建立 bios-boot-partition ====

  # - GPT 分區表通常與 UEFI 配合，Legacy BIOS 使用MBR
  # - Legacy BIOS 引導不需要 FAT32 分區，不需要將分區設置為 bios_grub
  #   並使用 ext4 分區格式

  # GPT version
  #parted "$DISK" --script --align=optimal mkpart ESP fat32 1MiB 1GiB
  #parted "$DISK" -- set 1 boot on
  #mkfs.vfat "$DISK"1

  # MBR version
  parted "$DISK" --script --align=optimal mkpart primary ext4 1MiB 513MiB
  mkfs.ext4 "${DISK}1"
  
  # ==== 建立 swap-partition ====

  # GPT version
  # 在 GPT 分區表中，parted 支援直接使用類型來指定分區用途
  #parted "$DISK" --script --align=optimal mkpart Swap linux-swap 513MiB 2.5GiB
  
  # MBR version
  # 在 MBR 分區表中，parted 不支援直接使用類型來指定分區用途
  parted "$DISK" --script --align=optimal mkpart primary linux-swap 513MiB 2.5GiB
  
  mkswap -L Swap "$DISK"2
  swapon "$DISK"2

  # ==== 建立 primary-partition ====

  parted "$DISK" --script --align=optimal mkpart primary 2.5GiB 100%
  mkfs.btrfs -L Butter "$DISK"3

  # 建立 primary-partition 的 subvolume
  mount "$DISK"3 /mnt
  btrfs subvolume create /mnt/root
  btrfs subvolume create /mnt/home
  btrfs subvolume create /mnt/nix
  btrfs subvolume create /mnt/persist
  btrfs subvolume create /mnt/log
  #  btrfs subvolume list /mnt

  # 建立 /mnt/root 的快照
  # btrfs subvolume snapshot 來源 目的
  # -r 設置 read-only
  btrfs subvolume snapshot -r /mnt/root /mnt/root-blank
  # btrfs subvolume list /mnt/root-blank

  # 手動掛載到 /mnt 底下，否則預設是掛載到/，
  # 在安裝nixos前，需要掛載到 /mnt ，透過 nixos-install --root /mnt ，/mnt 會變成新的 /

  mount -o subvol=root,compress=zstd,noatime "$DISK"3 /mnt

  mkdir /mnt/home
  mount -o subvol=home,compress=zstd,noatime "$DISK"3 /mnt/home

  mkdir /mnt/nix
  mount -o subvol=nix,compress=zstd,noatime "$DISK"3 /mnt/nix

  mkdir /mnt/persist
  mount -o subvol=persist,compress=zstd,noatime "$DISK"3 /mnt/persist

  mkdir -p /mnt/var/log
  mount -o subvol=log,compress=zstd,noatime "$DISK"3 /mnt/var/log

  mkdir /mnt/boot
  mount "$DISK"1 /mnt/boot
  ```

- step，產生配置，`$ nixos-generate-config --root /mnt`

  產生的/mnt/etc/nixos/hardware-configuration.nix

  其中， fileSystems."/" 會重複兩次，需要手動刪除其中一個

  [完整的 hardware-configuration.nix](diskless-examples/full-manual/hardware-configuration.nix)

- step，修改 /mnt/etc/nixos/configuration.nix

  - 所有檔案系統都添加 "compress=zstd" "noatime"
  - /var/log 和 /persist 添加 neededForBoot = true
  - 添加用戶
  - 設置 ssh

  ```nix
  # /mnt/etc/nixos/configuration.nix
  
  { config, lib, pkgs, ... }:

  {
    imports =
      [
        ./hardware-configuration.nix
      ];

    boot.loader.grub.enable = true;
    boot.loader.grub.device = "/dev/sda";

    fileSystems."/".options = [ "compress=zstd" "noatime" ];
    fileSystems."/home".options = [ "compress=zstd" "noatime" ];
    fileSystems."/nix".options = [ "compress=zstd" "noatime" ];
    fileSystems."/persist".options = [ "compress=zstd" "noatime" ];
    fileSystems."/persist".neededForBoot = true;

    fileSystems."/var/log".options = [ "compress=zstd" "noatime" ];
    fileSystems."/var/log".neededForBoot = true;

    environment.systemPackages = with pkgs; [
      vim
    ];

    users.users."root" = {
      isNormalUser = false;
      initialPassword = "root";
    };

    services.openssh.enable = true;
    services.openssh.settings.PermitRootLogin = "yes";
    services.openssh.settings.PasswordAuthentication = true;

    system.stateVersion = "24.11"; # Did you read the comment?
  }
  ```

- step，安裝系統，

  ```shell
  nixos-install --root /mnt
  halt
  ```

  安裝完成後，在重啟前，檢查grub配置文件是否正確

  ```
  cat /mnt/boot/grub/grub.cfg
  ```

  若grub配置文件丟失或有問題，手動重新生成

  ```
  # 重新安裝 GRUB： 安裝 GRUB 到指定磁碟（如 /dev/sda）
  grub-install --root-directory=/mnt /dev/sda

  # 更新 GRUB 配置文件： 在根分區中生成 GRUB 配置文件
  chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
  ```

  透過以下命令重啟

  ```shell
  "C:\Program Files\qemu\qemu-system-x86_64.exe" ^
  -m 4096 -smp 4 ^
  -accel whpx,kernel-irqchip=off ^
  -machine vmport=off ^
  -display sdl ^
  -drive file=nixos-disk.qcow2,format=qcow2 ^
  -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
  ```

- step，在 /etc/nixos/configuration.nix 中，`設置開機刪除/root目錄`

  - 完整範例，參考，[configuration-renew-root-and-impermanence.nix](diskless-examples/full-manual/configuration-renew-root-and-impermanence.nix)

  - 注意，此步驟在重開機後，<font color=blue>configuration.nix 和 hardware-configuration.nix 會被刪除</font>，
    需有需要，需要提前備份

  - 方法，透過`boot.initrd.postResumeCommands`命令

    ```nix
    ... 內容省略 ...
    boot.initrd = {
      enable = true;
      supportedFilesystems = [ "btrfs" ];

      postResumeCommands = lib.mkAfter ''
        mkdir -p /mnt
        
        # 掛載主硬碟
        mount -o subvol=/ /dev/sda3 /mnt
    
        # 為了避免刪除/root並從/root-blank建立snapshot時，
        # 由於 /root 被其他 subvolumes 汙染，而造成 `btrfs subvolume delete` 失敗
        # 因此先提前刪除

        # 刪除/root子目錄和/root
        btrfs subvolume list -o /mnt/root |
        cut -f9 -d' ' |
        while read subvolume; do
          echo "deleting /$subvolume subvolume..."
          btrfs subvolume delete "/mnt/$subvolume"
        done &&
        echo "deleting /root subvolume..." &&
        btrfs subvolume delete /mnt/root
        
        # 從/root-blank還原/root
        echo "restoring blank /root subvolume..."
        btrfs subvolume snapshot /mnt/root-blank /mnt/root
        
        # 卸載 
        # Once we're done rolling back to a blank snapshot,
        # we can unmount /mnt and continue on the boot process.
        umount /mnt
      '';
    };
    ```

  - 設置完成後，套用變更，`$ nixos-rebuild boot`，boot 代表 重開機才會生效

  - 測試，建立以下檔案後，reboot 之後檔案應該會消失

    ```
    echo "123" > /root/ttt.log
    ```

    若出現錯誤，可透過`$ dmesg`檢視開機過程的訊息

- step，在 /etc/nixos/configuration.nix 中，`透過impermanence模組指定要保留的檔案`

  - 完整範例，參考，[configuration-renew-root-and-impermanence.nix](diskless-examples/full-manual/configuration-renew-root-and-impermanence.nix)

  - 設置要保留的檔案

    ```
    # 使用第三方的 impermanence 模組
    impermanence = builtins.fetchTarball "https://github.com/nix-community/impermanence/archive/master.tar.gz";'

    # 加載 impermanence 模組
    imports =
      [ 
        "${impermanence}/nixos.nix"
        ./hardware-configuration.nix
      ];

    # 透過 impermanence 模組設置要永久保存的目錄或檔案
    environment.persistence."/persist" = {
      directories = [
        "/etc/nixos"
      ];
      files = [
        "/etc/machine-id"
        "/etc/ssh/ssh_host_ed25519_key"
        "/etc/ssh/ssh_host_ed25519_key.pub"
        "/etc/ssh/ssh_host_rsa_key"
        "/etc/ssh/ssh_host_rsa_key.pub"
      ];
    };
    ```

  - 手動將要備份的檔案和目錄，複製到永久保存的目錄中

    ```
    mkdir -p /persist/etc
    cp -r /etc/nixos /persist/etc/ 
    cp /etc/machine-id /persist/etc/machine-id

    mkdir -p /persist/etc/ssh
    cp /etc/ssh/ssh_host_ed25519_key /persist/etc/ssh/ssh_host_ed25519_key
    cp /etc/ssh/ssh_host_ed25519_key.pub /persist/etc/ssh/ssh_host_ed25519_key.pub
    cp /etc/ssh/ssh_host_rsa_key /persist/etc/ssh/ssh_host_rsa_key
    cp /etc/ssh/ssh_host_rsa_key.pub /persist/etc/ssh/ssh_host_rsa_key.pub
    ```

- step，套用變更

  ```
  nixos-rebuild boot
  reboot
  ```

## 修改為 EFI 版本

- 在建立磁碟分區時

  將 grub 的分區

  ```
  parted "$DISK" --script --align=optimal mkpart BOOT fat32 1MiB 3Mib
  parted "$DISK" -- set 1 bios_grub on
  ```

  替換為 EFI 的分區

  ```
  parted "$DISK" --script --align=optimal mkpart ESP fat32 1MiB 51MiB
  parted "$DISK" -- set 1 boot on
  mkfs.vfat "$DISK"1
  ```

- 在 /mnt/etc/nixos/configuration.nix 中

  將以下grub的配置

  ```nix
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";
  ```

  替換為EFI的配置

  ```nix
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  ```

- 透過 qemu 啟動 nixos-vm 時，添加`-bios OVMF.vd`的啟動參數

## ref

- [NixOS as a server, part 1: Impermanence](https://guekka.github.io/nixos-server-1/)