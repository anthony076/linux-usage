## build-diskless-system-overview

- 目錄分析

  在`ls /`中的目錄，`bin  boot  dev  etc  home  lib  lib64  nix  proc  root  run  srv  sys  tmp  usr  var`，
  透過`ls -alh /etc`可以得到以下結果

  - 由配置文件管理的目錄
    - /etc : 大部分配置文件都只是到 /etc/static 的軟鏈接，而 /etc/static 本身則被鏈接到 /nix/store，都由 NixOS 的配置文件管理統一管理
    - /bin : 只有一個 /bin/sh，被軟鏈接到 /nix/store 裡的 Bash
    - /usr : 只有一個 /usr/bin/env，被軟鏈接到 /nix/store 裡的 Coreutils

  - 空目錄或虛擬目錄或內存目錄
    - /mnt、/srv : 默認是空的，一般不存數據，只用來放其它分區的掛載點
    - /dev、/proc、/sys : 是存放硬件設備和系統狀態的虛擬文件夾
    - /run 和 /tmp : 是存放臨時文件的內存盤

  - 需要真正寫入硬盤數據的目錄
    - /boot : 存放啟動引導器
    - /home : 和 /root 存放各個用戶的家目錄
    - /nix : 存放 NixOS 的所有軟件包
    - /var : 存放系統軟件的數據文件

  - 實際上，NixOS 本身只需要 `/boot` 和 `/nix` 就可以正常啟動

- diskless 的目標
  - 在硬盤上只保留 /boot，/home，/nix，/root，/var 等必要文件夾的數據
  - 把 / 根目錄配置成一個內存盤，再把這幾個文件夾的數據掛載到對應位置
  - diskless 只會把指定的檔案或目錄保存在硬盤上，剩餘的或沒有指定保存的檔案或目錄都會在重啟之後被丟棄

- 建立 diskless 基本流程
  - 建立磁碟分區
    - 可以將每次都要清除的硬碟分區，
      - 設置成內存碟(例如，tmpfs)
      - 或設置boot.initrd.postDeviceCommands命令，每次開機時自動刪除，[有資料遺失的風險](https://discourse.nixos.org/t/what-does-impermanence-add-over-built-in-functionality/27939/16)
      - 或設置boot.initrd.postResumeCommands命令，每次開機時自動刪除

    - 可以建立獨立的分區，用來保存需要永久保存的檔案或目錄
  
  - 保存數據的方法
    - 可以將檔案複製到用永久保存的目錄後，透過fileSystems屬性實現開機期間自動掛載
    - 可以使用第三方的impermanence模組簡化fileSystems屬性的配置

## 範例，manual build diskless-system after nixos installation 

參考，[manual-build-diskless-system-after-nixos-installation.md](manual-build-diskless-system-after-nixos-installation.md)

## 範例，build diskless-system with full manual

參考，[build-diskless-system-with-full-manual.md](build-diskless-system-with-full-manual.md)

## 範例，build diskless-system with disko

參考，[build-diskless-system-with-disko.md](build-diskless-system-with-disko.md)
  
## ref

- [推薦，Impermanence Setup](https://www.youtube.com/watch?v=YPKwkWtK7l0)

- [impermanence @ nixos-wiki](https://nixos.wiki/wiki/Impermanence)

- [建構無狀態系統](https://lantian.pub/article/modify-computer/nixos-impermanence.lantian/)

- [What does impermanence add over built-in functionality](https://discourse.nixos.org/t/what-does-impermanence-add-over-built-in-functionality/27939/2)

- [Impermanent NixOS: on a VM + tmpfs root + flakes + LUKS](https://willbush.dev/blog/impermanent-nixos/)

- [Activating home-manager with impermanence](https://discourse.nixos.org/t/activating-home-manager-with-impermanence/31734)

- [NixOS as a server, part 1: Impermanence](https://guekka.github.io/nixos-server-1/)

- [在configuration.nix中使用impermanence模組](https://nixos.wiki/wiki/Impermanence)
  
- mount: mounting /mnt-root/persistent/... on /mnt-root/.... failed: No such file or directory
  - [Mounting paths needed for boot can fail #121](https://github.com/nix-community/impermanence/issues/121)
  
  - [Issue when installing NixOS with impermanence #34](https://github.com/nix-community/impermanence/issues/34)

  - [nixos: Create persisted dirs marked as needed for boot before mount #172](https://github.com/nix-community/impermanence/pull/172)
