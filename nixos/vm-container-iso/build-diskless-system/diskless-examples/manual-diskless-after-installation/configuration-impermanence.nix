{ config, lib, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  boot.loader.grub.enable = true;
  boot.loader.efi.efiSysMountPoint = "/boot";
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  users.defaultUserShell = pkgs.fish;
  nix.settings.experimental-features =  [ "nix-command" "flakes"];

  environment.systemPackages = with pkgs; [
     vim
     wget
     fish
     git
  ];

  users.users."root" = {
    isNormalUser = false;
    initialPassword = "root";
  };

  programs.fish.enable = true;
  programs.fish.interactiveShellInit = ''
    set fish_greeting
  '';

  services.openssh.enable = true;
  services.openssh.settings.PermitRootLogin = "yes";
  services.openssh.settings.PasswordAuthentication = true;

  system.stateVersion = "24.05"; # Did you read the comment?

}