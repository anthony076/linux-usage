"C:\Program Files\qemu\qemu-system-x86_64.exe" ^
-m 4096 -smp 4 ^
-accel whpx,kernel-irqchip=off ^
-machine vmport=off ^
-display sdl ^
-drive file=nixos_disk.qcow2,format=qcow2 ^
-net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
