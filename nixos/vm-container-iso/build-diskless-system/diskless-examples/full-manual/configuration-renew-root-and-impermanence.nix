{ config, lib, pkgs, ... }:

let
  impermanence = builtins.fetchTarball "https://github.com/nix-community/impermanence/archive/master.tar.gz";
in 
  {
    imports =
      [ 
        "${impermanence}/nixos.nix"
        ./hardware-configuration.nix
      ];

    boot.loader.grub.enable = true;
    boot.loader.grub.device = "/dev/sda";

    # Use the systemd-boot EFI boot loader.
    #boot.loader.systemd-boot.enable = true;
    #boot.loader.efi.canTouchEfiVariables = true;

    fileSystems."/".options = [ "compress=zstd" "noatime" ];
    fileSystems."/home".options = [ "compress=zstd" "noatime" ];
    fileSystems."/nix".options = [ "compress=zstd" "noatime" ];
    fileSystems."/persist".options = [ "compress=zstd" "noatime" ];
    fileSystems."/persist".neededForBoot = true;

    fileSystems."/var/log".options = [ "compress=zstd" "noatime" ];
    fileSystems."/var/log".neededForBoot = true;

    environment.systemPackages = with pkgs; [
      vim
    ];
    
    # 用於開機自動清除/root，並還原成空的/root
    # boot.initrd.postResumeCommands version
    boot.initrd = {
      enable = true;
      supportedFilesystems = [ "btrfs" ];

      postResumeCommands = lib.mkAfter ''
        mkdir -p /mnt
        
        # 掛載主硬碟
        mount -o subvol=/ /dev/sda3 /mnt
    
        # 為了避免刪除/root並從/root-blank建立snapshot時，
        # 由於 /root 被其他 subvolumes 汙染，而造成 `btrfs subvolume delete` 失敗
        # 因此先提前刪除
 
        # /root contains subvolumes:
        # - /root/var/lib/portables
        # - /root/var/lib/machines
        #
        # I suspect these are related to systemd-nspawn, but
        # since I don't use it I'm not 100% sure.
        # Anyhow, deleting these subvolumes hasn't resulted
        # in any issues so far, except for fairly
        # benign-looking errors from systemd-tmpfiles.

        # 刪除/root子目錄和/root
        btrfs subvolume list -o /mnt/root |
        cut -f9 -d' ' |
        while read subvolume; do
          echo "deleting /$subvolume subvolume..."
          btrfs subvolume delete "/mnt/$subvolume"
        done &&
        echo "deleting /root subvolume..." &&
        btrfs subvolume delete /mnt/root
        
        # 從/root-blank還原/root
        echo "restoring blank /root subvolume..."
        btrfs subvolume snapshot /mnt/root-blank /mnt/root
        
        # 卸載 
        # Once we're done rolling back to a blank snapshot,
        # we can unmount /mnt and continue on the boot process.
        umount /mnt
      '';
    };

    # 配置 impermanence ，指定要保留的檔案
    environment.persistence."/persist" = {
      directories = [
        "/etc/nixos"
      ];
      files = [
        "/etc/machine-id"
        "/etc/ssh/ssh_host_ed25519_key"
        "/etc/ssh/ssh_host_ed25519_key.pub"
        "/etc/ssh/ssh_host_rsa_key"
        "/etc/ssh/ssh_host_rsa_key.pub"
      ];
    };


    users.users."root" = {
     isNormalUser = false;
     initialPassword = "root";
    };

    services.openssh.enable = true;
    services.openssh.settings.PermitRootLogin = "yes";
    services.openssh.settings.PasswordAuthentication = true;

    system.stateVersion = "24.11"; # Did you read the comment?

  }
