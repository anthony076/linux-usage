  {
    description = "Nixos config flake";

    inputs = {
      nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

      disko = {
        url = "github:nix-community/disko";
        inputs.nixpkgs.follows = "nixpkgs";
      };

    };

    outputs = {nixpkgs, ...} @ inputs:
    {
      nixosConfigurations.default = nixpkgs.lib.nixosSystem {
        specialArgs = {inherit inputs;};
        modules = [
          # 加載 disko 模組
          inputs.disko.nixosModules.default

          # 加載硬碟配置文件，此文件會覆寫 disko 模組中的預設配置
          (import ./disko.nix { device = "/dev/sda"; })

          ./configuration.nix

        ];
      };
    };
  }