{ config, lib, pkgs, ... }:

{
  imports =
    [ 
      ./hardware-configuration.nix
    ];

  # for grub-bootloader
  boot.loader.grub.enable = true;
  boot.loader.grub.efiSupport = true;
  boot.loader.grub.efiInstallAsRemovable = true;

  # for efi-ootloader
  #boot.loader.systemd-boot.enable = true;
  #boot.loader.efi.canTouchEfiVariables = true;
  
  boot.initrd.postDeviceCommands = lib.mkAfter ''
    mkdir /btrfs_tmp
    mount /dev/root_vg/root /btrfs_tmp

    if [[ -e /btrfs_tmp/root ]]; then
        mkdir -p /btrfs_tmp/old_roots
        timestamp=$(date --date="@$(stat -c %Y /btrfs_tmp/root)" "+%Y-%m-%-d_%H:%M:%S")
        mv /btrfs_tmp/root "/btrfs_tmp/old_roots/$timestamp"
    fi

    delete_subvolume_recursively() {
        IFS=$'\n'
        for i in $(btrfs subvolume list -o "$1" | cut -f 9- -d ' '); do
            delete_subvolume_recursively "/btrfs_tmp/$i"
        done
        btrfs subvolume delete "$1"
    }

    for i in $(find /btrfs_tmp/old_roots/ -maxdepth 1 -mtime +30); do
        delete_subvolume_recursively "$i"
    done

    btrfs subvolume create /btrfs_tmp/root
    umount /btrfs_tmp
  '';

  environment.systemPackages = with pkgs; [
    vim
  ];

  users.users."root" = {
    isNormalUser = false;
    initialPassword = "root";
  };

  services.openssh.enable = true;
  services.openssh.settings.PermitRootLogin = "yes";
  services.openssh.settings.PasswordAuthentication = true;

  # 在 initial ramdisk 階段自動掛載 /persist
  # 需要在initial ramdisk階段，掛載impermanence模組設置的檔案系統才能正常boot
  fileSystems."/persist".neededForBoot = true;

  # 透過第三方的impermanence模組，設置需要保留的檔案或目錄
  # - environment.persistence 屬性由impermanence模組提供，詳見，https://github.com/nix-community/impermanence
  # - 配置 impermanence 後第一次reboot會遇到 mount: mounting /mnt-root/persistent/... on /mnt-root/.... failed: No such file or directory 的錯誤
  #   部分目錄或檔案會被提示  No such file or directory
  #   r) to reboot immediately
  #   *) to ignore the error and continue
  #   按下 * 略過後，會進入系統，再次進入就不會出現此問題
  
  #   此提示只會出現在配置後第一次 root，發現 No such file or directory 後會自動建立
  #   若要避免此錯誤，可以提前手動建立容易出錯的目錄

  # - reboot 後，會出現問題的目錄，例如，/var/lib/nixos，需要提前手動建立
  environment.persistence."/persist/system" = {
    hideMounts = true;
    # 要保留的目錄
    directories = [
      "/etc/nixos"
      "/var/log"
      "/var/lib/bluetooth"
      "/var/lib/nixos"
      "/var/lib/systemd/coredump"
      "/etc/NetworkManager/system-connections"
      { directory = "/var/lib/colord"; user = "colord"; group = "colord"; mode = "u=rwx,g=rx,o="; }
    ];

    # 要保留的檔案
    files = [
      "/etc/machine-id"
      { file = "/var/keys/secret_file"; parentDirectory = { mode = "u=rwx,g=,o="; }; }
    ];
  };

  system.stateVersion = "24.11";

}