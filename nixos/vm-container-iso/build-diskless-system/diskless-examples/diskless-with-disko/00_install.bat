::https://channels.nixos.org/nixos-24.11/latest-nixos-minimal-x86_64-linux.iso

"C:\Program Files\qemu\qemu-system-x86_64.exe" ^
-m 4096 -smp 4 ^
-boot d -cdrom nixos-minimal-24.11.710905.a0f3e10d9435-x86_64-linux.iso ^
-drive file=nixos-disk.qcow2,format=qcow2 ^
-net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
