{ config, lib, pkgs, ... }:

{
  imports =
    [ 
      ./hardware-configuration.nix
    ];

  # for grub-bootloader
  boot.loader.grub.enable = true;
  boot.loader.grub.efiSupport = true;
  boot.loader.grub.efiInstallAsRemovable = true;

  # for efi-ootloader
  #boot.loader.systemd-boot.enable = true;
  #boot.loader.efi.canTouchEfiVariables = true;

  # 自動清除 /dev/root_vg/root/root ，
  # 相當於每次開機，自動清除 /root 的內容
  boot.initrd.postDeviceCommands = lib.mkAfter ''
    mkdir /btrfs_tmp
    
    # 將 /dev/root_vg/root 這個 lvm-disk 的內容掛載到 /btrfs_tmp 目錄
    # 掛載後，ls /btrfs_tmp 得到 nix  old_roots  persist  root
    mount /dev/root_vg/root /btrfs_tmp

    if [[ -e /btrfs_tmp/root ]]; then
        # 建立一個具有時間戳的目錄，例如，/btrfs_tmp/old_roots/2024-12-17_06:22:44
        mkdir -p /btrfs_tmp/old_roots
        timestamp=$(date --date="@$(stat -c %Y /btrfs_tmp/root)" "+%Y-%m-%-d_%H:%M:%S")
        
        # 將 /btrfs_tmp/root 的內容移動到 /btrfs_tmp/old_roots/2024-12-17_06:22:44
        # 相當於清除 /btrfs_tmp/root
        mv /btrfs_tmp/root "/btrfs_tmp/old_roots/$timestamp"
    fi

    delete_subvolume_recursively() {
        IFS=$'\n'
        
        # btrfs subvolume list -o /btrfs_tmp/old_roots/2024-12-17_06:22:44
        # 得到 ID 259 gen 82 top level 258 path old_roots/2024-12-17_06:22:44/srv

        for i in $(btrfs subvolume list -o "$1" | cut -f 9- -d ' '); do
            delete_subvolume_recursively "/btrfs_tmp/$i"
        done
        btrfs subvolume delete "$1"
    }

    # 檢查 /btrfs_tmp/old_roots/ 下的子目錄，若時間超過30天，自動刪除該子目錄
    for i in $(find /btrfs_tmp/old_roots/ -maxdepth 1 -mtime +30); do
        delete_subvolume_recursively "$i"
    done

    # 重新建立新的 /btrfs_tmp/root
    btrfs subvolume create /btrfs_tmp/root

    # 卸載 /btrfs_tmp 
    umount /btrfs_tmp
  '';

  environment.systemPackages = with pkgs; [
    vim
  ];

  # 設置 root 用戶
  # 必要，disko 的設置未完成，/ 根分割區尚未正確掛載，需要手動設置才能確保root帳號可用
  users.users.root = {
    isNormalUser = false;
    initialPassword = "root";
  };

  services.openssh.enable = true;
  services.openssh.settings.PermitRootLogin = "yes";
  services.openssh.settings.PasswordAuthentication = true;

  system.stateVersion = "24.11";

}