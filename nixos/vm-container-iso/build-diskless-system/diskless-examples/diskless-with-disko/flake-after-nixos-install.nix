  {
    description = "Nixos config flake";

    inputs = {
      nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

      disko = {
        url = "github:nix-community/disko";
        inputs.nixpkgs.follows = "nixpkgs";
      };

      # 添加第三方的 impermanence 
      impermanence = {
        url = "github:nix-community/impermanence";
      };

    };

    outputs = {nixpkgs, ...} @ inputs:
    {
      nixosConfigurations.default = nixpkgs.lib.nixosSystem {
        specialArgs = {inherit inputs;};
        modules = [
          inputs.disko.nixosModules.default
          (import ./disko.nix { device = "/dev/sda"; })

          ./configuration.nix

          # 加載 impermanence 配置
          inputs.impermanence.nixosModules.impermanence
        ];
      };
    };
  }