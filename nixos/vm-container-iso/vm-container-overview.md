## vm-container 概述和常用工具函數

- 概念
  - vm或container都會使用映像檔(image)，將`系統環境`或`套件環境`壓縮，透過image可需要的依賴和環境打包，便於傳播和使用
    
    image 根據使用的場景，具有以下2種類型

    - 類型，容器化的映像檔(container-image，ct-image)，例如，
      - oci-image : 通用容器鏡像規範，能和多種容器的runtime兼容，例如，containerd、runc
      - docker-image : 是 oci-image 的實現之一，適用於 docker、podman
      - appimage : 用於封裝桌面應用程式，以ISO-9660文件系統為基礎，內部包含 SquashFS 映像
  
      - 特點，以上映像檔具有體積小、啟動快速、但需要和與宿主機共享核心的特點，提供的功能是精簡且有限的
  
    - 類型，虛擬機用的映像檔(virtual-machine-image，vm-image)，例如，
      - qemu-disk-image
      - vertualbox-disk-image
      - vmware-disk-image

      - 特點，以上映像檔具有體積大、啟動緩慢、不需要和與宿主機共享核心的特點，提供的功能是相對完整的

## 建構具有依賴性的 qemu-nixos-vm (build-qemu-nixos-depend)

- 使用場景
  
  在 nixos 上，建立`依賴nixos-host`的虛擬機用磁碟映像檔(qemu-vm-image)，
  `建構`和`使用`都必須依賴nixos-host上的nixStore，因此，只能在 nixos 上運行，建立的磁碟映像檔無法在其他主機上使用

  因為需要依賴於 nixos-host 上的 nixStore，此方法建立的`qemu啟動命令`通常更為複雜，需要由套件一起產生

- 此類的vm有以下兩種類型
  - 類型，`沒有開機過程`的簡化環境(simple-env)，參考，[建構 simple-env 的精簡系統](build-qemu-nixos-depend/simple-env.md)
    - 方法，make-disk-image函數，並設置`onlyNixStore=true`
    - 方法，透過 `virtualisation屬性` + `$ nixos-rebuild build-vm` 建構W

  - 類型，`有開機過程`的完整環境(full-env)，[建構 full-env 的完整系統](build-qemu-nixos-depend/full-env.md)
    - 方法，透過 `nixpkgs/nixos/default.nix` + `nixos/modules/profiles/qemu-guest.nix` 建構

## 建構獨立的 qemu-nixos-vm (build-qemu-nixos-independ)

- 方法，手動建立

- 方法，純粹使用 make-disk-image 函數，但不設置`onlyNixStore=true`
  
  範例見，[example-pure-make-disk-image](build-qemu-nixos-independ/example-pure-make-disk-image/readme.md)

- 方法，`<nixpkgs/nixos>` + `make-disk-image函數`
  
  `<nixpkgs/nixos>` 的工作原理，見 [nixpkgs/nixos/default.nix](build-nixos-iso/build-nixos-iso-overview.md#方法-利用nixpkgsnixosdefaultnix函數產生iso)解析

  範例，[example-nixos+make-disk-image](build-qemu-nixos-independ/example-nixos+make-disk-image/build-qcow2.nix)

- 方法，`flake.nix` + `make-disk-image函數`

  範例，[https://tarnbarford.net/journal/building-nixos-qcow2-images-with-flakes/](https://tarnbarford.net/journal/building-nixos-qcow2-images-with-flakes/)

- 方法，透過第三方的[nixos-generators](build-qemu-nixos-independ/3rd_nixos-generators_usage.md)
  - 手動建立的自動化

  - 建構時會依賴nixos-host上的nixStore，但`使用時不依賴nixos-host上的nixStore`，
    因此，建立的磁碟映像檔可在windows上執行

  - qemu的啟動命令相對簡單

## 建構 nixos-iso

參考，[build nixos iso](build-nixos-iso/build-nixos-iso-overview.md)

## 建構其他distro的image

- 參考，
  - [buid-distro-disk-image](buid-distro-disk-image/readme.md)
  - [lfs-on-linux](lfs-on-nixos/readme.md)

- 建構時，會利用 nix-store 中的源碼安裝編譯後的套件

- 建構完成後會產生 disk-image.qcow2，且 disk-image.qcow2 可以在任何平台上透過qemu客戶端執行，
  運行時不依賴 nix-store

## 建構 docker-image

- [build docker vm](build-docker/)

## 建構無碟系統

參考，[建構無狀態的nixos系統(無碟系統)](build-diskless-system/readme.md)

## 建構可轉移的nix-store

參考，[將nix-store的內容轉移到安裝媒體上，build new nix store](build-nix-store/build-nix-store-overview.md)

## [函數][vm] vmTools : 一組用於提供與VM相關的工具函數集合

- `vmTools.createEmptyImage` : 產生指定目標磁碟映像的 bash-script
  - size屬性 : disk-image 的大小，MB
  - fullName屬性 : disk-image 的完整名稱 (${destination}/nix-support/full-name.)
  - destination屬性 : disk-image 的寫入位置 (選用，預設為$out)

- `vmTools.runInLinuxVM` : 
  - 利用qemu建立linux-vm後，在linux-vm中執行指定的程式
  - 可接受套件名，或指定derivation(先建構再執行)
  - 預設狀況下為無碟模式，沒有使用 disk-image，改用使用memory-file-system(tmpfs)，
    並直接取用host上的/nix/store

  - preVM屬性 : 在linux-vm執行前要執行的shell-command
  - memSize屬性 : linux-vm 的記憶體大小
  - diskImage屬性 : 被掛載到 linux-vm 下 /dev/sda 位置的檔案系統映像檔，
    注意，只需要是檔案系統，不需要是一個完整包含partition-table的diskImage

  - [源碼](https://github.com/NixOS/nixpkgs/blob/26b95a465665c50f0b2dfaf2ad678b4b4940da53/pkgs/build-support/vm/default.nix#L340)

    - runInLinuxVM = drv: lib.overrideDerivation drv ();
    - 使用 runInLinuxVM ，需要傳入一個drv，透過 lib.overrideDerivation 修改drv的內容
    - lib.overrideDerivation 可以修改drv原來的屬性，也可以添加新的屬性

- `vmTools.runInLinuxImage` : 
  - 和vmTools.runInLinuxVM類似，但不使用nix-store中的stdenv
  - 改由掛載filesystem-image提供的FHS檔案系統，例如，/bin 或 /usr/bin 來執行

- `vmTools.extractFs` : 讀取具有壓縮性質的檔案，例如，ISO檔，將其解壓縮的內容保存在 nix-store 中

- `vmTools.extractMTDfs` : 和vmTools.extractFs類似，但解壓縮的內容保存在記憶體中

- `vmTools.makeImageTestScript` 
  - 建立用於啟動diskImage的腳本，透過腳本啟動VM後，會直接進行交互式的會話
  - 例如，`pkgs.vmTools.makeImageTestScript diskImages.ubuntu2004x86_64`，

- `vmTools.diskImageFuns` : 
  - 將pkgs的套件和一組預製好的VM一起打包成獨立的package，
    或為預製好的VM添加新內容

  - 預製好的VM，包含 Fedora、CentOS、Ubuntu、Debian、... 等

  - 將套件與指定的虛擬機一起打包成一個獨立的package，
    使套件不會依賴於宿主系統上的其他庫或二進制文件，而是將所有必要的組件打包在一起，
    使得在不同的 Linux 發行版之間移植和部署變得更加方便

  - `範例`，為預製好的VM添加新內容

    ```
    # 為ubuntu2004x86_64添加firefox
    { pkgs }: with pkgs; with vmTools;
    diskImageFuns.ubuntu2004x86_64 { extraPackages = [ "firefox" ]; size = 8192; }
    ```

  - `範例`，將pkgs的ncurses，和使用deb的centos7-VM一起打包成一個獨立的package

    ```
    let
      pkgs = (import ./default.nix {});
      vm    = pkgs.vmTools.diskImageFuns.centos7x86_64 {};
      args = {
        diskImage = vm;
        diskImageFormat = "qcow2";
        src       = pkgs.ncurses.src;
        name      = "ncurses-deb";
        buildInputs = [];
        meta.description = "No descr";
      };
    in pkgs.releaseTools.debBuild args
    ```

- `vmTools.diskImageExtraFuns`
  
  vmTools.diskImageFuns.<attr> { extraPackages = ... } 的簡化版

- `vmTools.diskImages`
  
  vmTools.diskImageFuns.<attr> { } 的簡化版

## [函數][vm] nixpkgs/nixos/lib/make-disk-image.nix : 用於建構各種格式 disk-image 的函數

- [make-disk-image.nix源碼](https://github.com/NixOS/nixpkgs/blob/master/nixos/lib/make-disk-image.nix)
  - 建立磁碟映像
  - 建立磁碟分區
  - 安裝bootloader
  - 建構qemu-vm
  - 啟動qemu-vm
  - pso

- make-disk-image.nix 是各個vm或container的底層函數，由更高階的應用調用，例如，

  [qemu-vm.nix 調用make-disk-image.nix建構disk-image](https://github.com/NixOS/nixpkgs/blob/708ebaf7d99d45ca8272a60c59e2c570e064a8ec/nixos/modules/virtualisation/qemu-vm.nix#L293)

  [oci-image.nix 調用make-disk-image.nix建構disk-image](https://github.com/NixOS/nixpkgs/blob/564f029dc459a2cb424fe00900ef5bfa30d75e05/nixos/modules/virtualisation/oci-image.nix#L26)

  [virtualbox-image.nix 調用make-disk-image.nix建構disk-image](https://github.com/NixOS/nixpkgs/blob/564f029dc459a2cb424fe00900ef5bfa30d75e05/nixos/modules/virtualisation/virtualbox-image.nix#L219) 

- 支援建立以下格式的 disk-img
  - raw 
  - QCOW2 (QEMU)
  - QCOW2-Compressed (compressed version)
  - VDI (VirtualBox)
  - VPC (VirtualPC)

- make-disk-image 函數可以建立以下類型的disk-image

  - `類型`，使用`cptofs`的`精簡環境`(simple-env)，需要依賴 nixos-host 才能執行 (qemu-nixos-depend)
    
    例如，web-server、只需要用到nix-store一起工作的應用等，需要快速建構和啟動時使用

  - `類型`，具有傳統開機流程的`完整環境`(full-env)，需要依賴 nixos-host 才能執行 (qemu-nixos-depend)
    
    例如，要測試 Nixos 的早期啟動和生命週期時，需要完整的nixos功能時使用

  - `類型`，具有傳統開機流程的`完整環境`(full-env)，不依賴 nixos-host 可獨立運行 (qemu-nixos-independ)

  - 注意，make-disk-image 沒有限制只能建立nixos系統的disk-image，disk-image的內容是自定義的，也可以用於建立其他distro的disk-image，但主要用建立nixos系統

  - `比較表`
    
    | 項目           | qemu-nixos-depend的simple-env(使用cptofs) | qemu-nixos-depend的full-env          | qemu-nixos-independ的full-env |
    | -------------- | ----------------------------------------- | ------------------------------------ | ----------------------------- |
    | bootloader     | `X`                                       | V                                    | V                             |
    | partitionTable | `X`                                       | V                                    | V                             |
    | kernel         | `X`                                       | V                                    | V                             |
    | initramfs/init | `X`                                       | V                                    | V                             |
    | init           | V                                         | V                                    | V                             |
    | nixStore deps  | V                                         | V                                    | `X`                           |
    | weight         | light                                     | medium                               | heavy                         |
    | speed          | fast                                      | medium                               | slow                          |
    | 啟動           | 從 nixStore複製文件到虛擬文件系統         | 啟動VM，載入系統映像，執行導引       | 同左                          |
    | 初始化         | 只執行必要的初始化腳本                    | 執行完整初始化過程，含服務和守護進程 | 同左                          |
    | 完整性         | 精簡系統                                  | 完整系統                             | 同左                          |

    cptofs 方法
    - 直接從nix-store提取必要的文件和依賴項
    - 在簡化環境中，只執行必要的初始化腳本，用於設置環境變數和啟動基本服務
    - 使用者可以進入一個簡化的 shell 環境，可能缺少某些工具和功能，但足以支持基本的測試需求

    在 cptofs 的簡化環境中
    - 僅啟動必要的基本服務，無法啟動所有系統服務和守護進程
    - 資源管理(如內存、CPU 和 I/O)可能不夠完善，導致性能測試不準確
    - 可能無法訪問所有硬件功能，限制了對某些硬件相關功能的測試
    - 網絡配置可能不如一般系統完整，可能會影響到網絡相關服務的測試
    - 可能缺乏安全特性，使得某些安全測試無法進行
    - 日誌記錄功能可能有限，影響問題診斷的能力

  - 若配置中，設置了`virtualisation.useBootLoader = false`時，會使用 cptofs 的方法
    為設置的情況下，默認值為 false

- 範例，使用`cptofs`的`nix-store-deps-disk-image`
  
  ```nix
  let
    pkgs = import <nixpkgs> {};
    lib = pkgs.lib;
    make-disk-image = import <nixpkgs/nixos/lib/make-disk-image.nix>;
  in
    make-disk-image {
      inherit pkgs lib;
      config = {};
      additionalPaths = [ ];
      format = "qcow2";
      onlyNixStore = true;          # 使用 cptofs 方法
      partitionTableType = "none";
      installBootLoader = false;    # cptofs 不需要 bootloader
      touchEFIVars = false;         # cptofs 不需要 bootloader
      diskSize = "auto";
      additionalSpace = "0M"; # Defaults to 512M.
      copyChannel = false;
    }
  ```

- 範例，使用`vm`的`nix-store-free-disk-image`

  ```nix
  let
    pkgs = import <nixpkgs> {};
    lib = pkgs.lib;
    make-disk-image = import <nixpkgs/nixos/lib/make-disk-image.nix>;
    evalConfig = import <nixpkgs/nixos/lib/eval-config.nix>;
  in
    make-disk-image {
      inherit pkgs lib;
      inherit (evalConfig {
        modules = [
          { # 完整系統安裝，需要手動配置掛載點
            fileSystems."/" = { device = "/dev/vda"; fsType = "ext4"; autoFormat = true; };
            boot.grub.device = "/dev/vda";
          }
        ];
      }) config;
      format = "qcow2";
      onlyNixStore = false;   # 透過VM安裝完整系統，不需要依賴於nix-store
      partitionTableType = "legacy+gpt";    # 需要建立磁碟分割表
      installBootLoader = true;   # 需要bootloader
      touchEFIVars = true;    # 需要bootloader
      diskSize = "auto";
      additionalSpace = "0M"; # Defaults to 512M.
      copyChannel = false;
      memSize = 2048; # Qemu VM memory size in megabytes. Defaults to 1024M.
    }
  ```

## [函數][ct] pkgs.dockerTools : 一組用於建立docker-image的工具函數集合

- `pkgs.dockerTools.buildImage` : 用於單層docker-image
  - 用於將docker-image包裝為與docker相容的docker-repo-tarball
  - 此函數會將所有的檔案和依賴單獨創建一個layer，且只複製現有層中尚未存在的新相依性
  - 若為要建立多個layer，改用 buildLayeredImage 或 streamLayeredImage
  - [官方buildImage文檔 + 範例](https://nixos.org/manual/nixpkgs/stable/#ssec-pkgs-dockerTools-buildImage)

- `pkgs.dockerTools.buildLayeredImage` : 用於多層docker-image
  - 用於為檔案和依賴建立 multi-layers
  - buildLayeredImage 會調用 streamLayeredImage 提供的腳本，
    來建立壓縮的Docker-compatible-repository-tarball

- `pkgs.dockerTools.streamLayeredImage` : 用於多層docker-image
  - streamLayeredImage 用於創建腳本，該腳本會將一個包含單層的docker-image的`docker-compatible-repository-tarball`，
    串流到 stdout，透過multi-layers改善image之間的共用

  - streamLayeredImage 不會將image輸出到nix-store中，而只是建立影像的腳本，可以節省IO和磁碟/快取空間，特別適合大的image

  - [官方streamLayeredImage文檔 + 範例](https://nixos.org/manual/nixpkgs/stable/#ssec-pkgs-dockerTools-buildImage)

- `pkgs.dockerTools.pullImage` : 提供類似 $ docker image pull 的功能

- `pkgs.dockerTools.exportImage` : 提供類似 $  docker container export 的功能

- `pkgs.dockerTools.buildNixShellImage` : 建立docker-image > 打包為 tarball > 保存在 nix-store 中

  - 實際操作
    - 底層會調用streamNixShellImage建立的shell-script
    - 該script將`套件`和`建構套件的環境`建構成docker-image，
    - 將docker-image打包為可以透過`$ docker image load`命令直接加載的`docker-compatible-repository-tarball`，
    - 將`docker-compatible-repository-tarball`保存在nix-store中

- `pkgs.dockerTools.streamNixShellImage` : 建立docker-image > 打包為 tarball
  - 和 pkgs.dockerTools.buildNixShellImage 描述的操作類似，但不保存在 nix-store 中
  - streamNixShellImage 用於建立實際操作的shell-腳本
  - 此函數可以節省IO和disk的儲存空間，特別適合大型的 docker-image

## [函數][ct] pkgs.ociTools.buildContainer : 一組用於建構 oci-runtime-image 的函數集合

## [函數][ct] pkgs.appimageTools : 一組用於包裝或解壓 AppImage 內容的函數集合

## [套件] disko的使用

參考，[利用disko套件建立分割硬碟](disko-usage/readme.md)

- [利用disko建構qemu-nixos-disk-image](https://github.com/nix-community/disko/blob/master/docs/disko-images.md)

## ref

- [各種VM|container的源碼](https://github.com/NixOS/nixpkgs/tree/nixos-24.11/nixos/modules/virtualisation)

- [vmTools](https://nixos.org/manual/nixpkgs/stable/#sec-vm-tools)

- [make-disk-image源碼](https://github.com/NixOS/nixpkgs/blob/master/nixos/lib/make-disk-image.nix)


- build-qemu-nixos-independ
  - [build-qcow2.nix](https://gist.github.com/tarnacious/f9674436fff0efeb4bb6585c79a3b9ff)