# 建構，nix-build default.nix
# 啟動，qemu-system-x86_64 -m 2048 -smp 2 -hda nixos.qcow2 -nographic -no-acpi

{ pkgs ? import <nixpkgs> {}}:
let
  image = (import <nixpkgs/nixos/lib/eval-config.nix> {
    modules = [
      # expression that exposes the configuration as vm image
      ({config, lib, pkgs, ...}: {
        system.build.qcow2 = import <nixpkgs/nixos/lib/make-disk-image.nix> {
          inherit lib config pkgs;
          diskSize = 8192;
          format = "qcow2-compressed";
          configFile = ./configuration.nix;
        };
      })
      ./configuration.nix
    ];
  }).config.system.build.qcow2;
in
{
  inherit image;
}