## example-by-make-disk-image

- 建構，`$ nix-build default.nix`
- 啟動
  
  ```
  cp /nix/store/ls20ql057d1rfgbmbqiyb8wzbgxnd9x2-nixos-disk-image/nixos.qcow2 .
  qemu-system-x86_64 -m 2048 -smp 2 -hda nixos.qcow2 -nographic -no-acpi
  ```

  可在 windows-host 上執行

## ref 

- [nixos vm qcow2](https://gist.github.com/andir/88458b13c26a04752854608aacb15c8f)