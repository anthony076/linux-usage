
{
  imports = [
    # configure the mountpoint of the root device
    ({
      fileSystems."/".device = "/dev/disk/by-label/nixos";
    })

    # configure the bootloader
    ({
      boot.loader.grub.extraConfig = ''
        serial --unit=0 --speed=115200
        terminal_output serial console; terminal_input serial console
      '';
      boot.kernelParams = [
        "console=tty0"
        "console=ttyS0,115200n8"
      ];
      boot.loader.grub.device = "/dev/sda";
      boot.loader.grub.version = 2;
    })

    # openssh and user configuration
    ({
      services.openssh.enable = true;
      users.users."root".initialPassword = "root";
    })
  ];
}