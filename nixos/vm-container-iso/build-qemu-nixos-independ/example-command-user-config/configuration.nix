{ config, lib, pkgs, ... }:
{

  environment.systemPackages = with pkgs; [
    vim
    hello
  ];

  users.users.root = {
    isNormalUser = false;
    initialPassword = "root";
  };

  services.openssh.enable = true;
  services.openssh.settings.PermitRootLogin = "yes";
  services.openssh.settings.PasswordAuthentication = true;

  system.stateVersion = "24.11";
}