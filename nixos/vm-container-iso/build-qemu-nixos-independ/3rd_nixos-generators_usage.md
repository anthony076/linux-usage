## nixos-generators 的使用

- 專門用於`產生nixos系統`的`磁碟映像檔`，不依賴於nixos-host上的nixStore，可獨立運行

- 支援的磁碟映像檔格式
  - 完整支援的格式見[supported-format](https://github.com/nix-community/nixos-generators?tab=readme-ov-file#supported-formats)
  - qcow : qemu 的 qcow2 格式
  - qcow-efi : 支援 efi 開機的 qemu-qcow2
  
  - install-iso : 用於安裝 NixOS 的 ISO 鏡像，包含安裝所需的所有工具和配置
  - iso : 更通用的 iso 鏡像，可用於、測試、開發環境、和安裝，但提供不完整的安裝功能或特定配置
  
  - vagrant-virtualbox : 可用於 Vagrant 的 virtualbox 格式
  - virtualbox : 一般的virtualbox格式 (*.ova)
  - vmware : 一般的vmware格式 (*.vmdk)
  
  - vm : 優先使用 qemu-kvm 命令，注意，沒有 kvm 也能執行，會自動切換成其他相容模式
  - vm-bootloader : 和 vm 一致，但使用real-bootloader取代netbooting
  - vm-nogui : 和 vm 一致，但沒有gui

## 注意事項

- 測試前增加記憶體，避免記憶體不足的問題

- 注意，透過命令建構時，有機會遇到 noapic 的問題，重試即可

- 透過 `ctrl+a` > `x` 可以關閉vm

## 配置文件中不加載hardware-configuration.nix

hardware-configuration.nix 用於掛載磁碟分區，與磁碟映像檔中的磁碟分區配置有關

不指定hardware-configuration.nix時，掛載磁碟分區的相關配置，
會在建構vm的過程中，由nix-generators根據實際磁碟映像檔的狀況自動產生

若手動指定hardware-configuration.nix，若`掛載磁碟分區配置`與`實際磁碟映像檔中的狀況不同`，
容易產生錯誤造成無法開機

例如，若加載的hardware-configuration.nix中有以下設置

```
fileSystems."/boot" =
  { device = "/dev/disk/by-uuid/E6F1-31C6";
    fsType = "vfat";
    options = [ "fmask=0022" "dmask=0022" ];
  };
```

而創建vm過程中，磁碟分區中沒有 /dev/disk/by-uuid/E6F1-31C6 的分區，而是其他UUID的分區時，
就會造成配置錯誤而無法啟動系統

## [使用] 以`命令方式`使用nix-generators

- 基本概念

  - nix-generators的建構流程
    - step，建立 qemu 的 disk-image
    - step，啟動qemu並加載nixos-installer-rom
    - step，套用配置
    - step，完成 qcow的建立
  
  - 產出的nixos.qcow2，可在windows-host上執行

  - 建構完成後，只會打印 nixos.qcow2 在 nixStore 中的位置，
    不會在當前目錄建立 result 目錄，也不會建立指向nixos.qcow2位置的連結檔

  - 建構的 nixos.qcow2 是read-only狀態，無法直接使用
 
    由於nix-generators用於`建立獨立不依賴nixos-host的disk-image`(qemu-nixos-independ)，
    因此，建構完成後不會產生qemu的啟動命令，需要手動啟動vm

    手動執行啟動命令`不能直接使用nixos.qcow2在nixStore中的路徑`，例如，以下命令是錯誤的
    > qemu-system-x86_64 -m 2048 -smp 2 -hda /nix/store/<hash>-nixos-disk-image/nixos.qcow2 -nographic -no-acpi

    因為在nixStore路徑中的nixos.qcow2是唯獨的，無法寫入資料，

    而qemu啟動vm後，是需要往硬碟中寫入數據的，例如，寫入log、添加套件、在硬碟中添加檔案等，
    對於唯獨屬性的nixos.qcow2是無法寫入，因此無法直接使用nixStore路徑中的nixos.qcow2
    
    必須將nixos.qcow2額外保存在可讀寫的路徑中，例如，`$ cp /nix/store/<hash>-nixos-disk-image/nixos.qcow2 .`
  
- 常用參數
  - `-f`，format : 必要，用於指定磁碟映像檔的格式
  - `-c`，config
    - 選用，用於指定`用戶自定義的nixos配置`
    - 若未提供時，透過`$ nixos-generate --help`可以看到預設的nixos配置，
      例如，/nix/store/lwfh1ghlk1f6jfa56kg43nbxkwpqfymv-nixos-generators-1.8.0/share/nixos-generator/configuration.nix

- qemu 的啟動命令，
  
  > qemu-system-x86_64 -m 2048 -smp 2 -hda nixos.qcow2 -nographic -no-acpi

  - `-no-acpi` : 避免出現 acpi 的問題，但加入此參數仍然有機會遇到，重試即可
  - `-net nic -net user,hostfwd=tcp::2222-:22` : 選用，用於ssh連接

- 範例，`$ nixos-generate -f qcow`，建立 qemu-qcow2 格式的磁碟映像，並使用官方預設的nixos配置

  - 使用的預設nixos配置

    ```
    services.sshd.enable = true;
    services.nginx.enable = true;
    networking.firewall.allowedTCPPorts = [80];
    users.users.root.password = "nixos";
    services.openssh.settings.PermitRootLogin = lib.mkDefault "yes";
    services.getty.autologinUser = lib.mkDefault "root";	# 自動登入
    ```

- 範例，`$ nixos-generate -c ./configuration.nix -f qcow`，使用自定義的配置

  [完整nixos配置](example-command-user-config/configuration.nix)

## [使用] 以`模組方式`使用nix-generators