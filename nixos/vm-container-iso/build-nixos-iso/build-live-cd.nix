# 建構， $ nix-build '<nixpkgs/nixos>' -I nixos-config=iso.nix -A config.system.build.isoImage

{ config, pkgs, ... }:
{
  imports = [
    
    # 產生config.system.build.isoImage的建構屬性
    <nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix>
    # 添加預設的 nixos channel
    <nixpkgs/nixos/modules/installer/cd-dvd/channel.nix>
  ];

  # 配置 live-cd 中的環境
  environment.systemPackages = [ pkgs.fish ];
}