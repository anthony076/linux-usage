## 建構 nixos iso

  - 和命令式建構iso的差異，
    - nix 會對nixpkgs/nixos/default.nix進行評估(evaluation)後產生derivation(*.drv)，
    - 根據derivation，依序建構需要的依賴，包含套件、路徑、配置檔案等
  
  - 兩種產生iso的方法

    - 以下兩種方法都需要加載`installation-cd-minimal.nix`模組，
      - installation-cd-minimal.nix是建構iso的實際屬性，
      - 兩個方法的差異在於加載installation-cd-minimal.nix的方式不同

    - 方法，利用`nixpkgs/nixos/default.nix函數`產生iso
      
      - 透過 [default.nix 函數](https://github.com/NixOS/nixpkgs/blob/master/nixos/default.nix) 的 configuration 輸入參數，
        會利用[from-env函數](https://github.com/NixOS/nixpkgs/blob/master/nixos/lib/from-env.nix)
        從環境變數中獲取`nix-config`變數值，因此，使用者需要透過`-I nixos-config=iso.nix`覆寫$NIX_PATH中預設的nixos-config變數值
       
      - nix-config 指向用戶自定義配置檔的位置

      - 在用戶自定義配置檔(iso.nix)中，手動加載installation-cd-minimal.nix

    - 方法，利用flake的`nixpkgs.lib.nixosSystem函數`產生iso
      
      直接將 installation-cd-minimal.ni x以 nix-modules 的方式加載

## [方法] 利用`nixpkgs/nixos/default.nix函數`產生iso

- 以 live-cd 的建構命令為例，`$ nix-build '<nixpkgs/nixos>' -I nixos-config=iso.nix -A config.system.build.isoImage`

  - [完整代碼](build-live-cd.nix)

  - step1，`$ nix-build '<nixpkgs/nixos>'`，執行 `<nixpkgs/nixos>/default.nix`
    - nixpkgs/nixos/default.nix 提供了所有建構系統需要的屬性，但是實際上不會建構任何產出，僅僅是提供預設的配置選項
    - 在 nixpkgs/nixos/default.nix 的函數中，定義了`configuration ? import ./lib/from-env.nix "NIXOS_CONFIG" <nixos-config>`的輸入參數，用於讀取配置檔
    - ./lib/from-env.nix 獲取環境變數中的nix-config變數，該變數用於指向配置檔的路徑
    - [nixpkgs/nixos/default.nix源碼](https://github.com/NixOS/nixpkgs/blob/master/nixos/default.nix)

  - step2，使用者需要透過`-I nixos-config=iso.nix`覆寫$NIX_PATH變數中的nixos-config變數值

  - step3，在iso.nix中
    - 目的，導入的`<nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix>`，
      讓系統可以產生 config.system.build.isoImage 的屬性，該屬性是實際建立iso檔的建構屬性

    - 目的，自定義進入live-cd系統中的環境配置，例如，讓 live-cd 中有 fish-shell 可用

  - step4，透過`-A config.system.build.isoImage`，讓 nix-build 建構 config.system.build.isoImage

## [方法] 利用flake的`nixpkgs.lib.nixosSystem函數`產生iso

- 以 live-cd 的建構命令為例，`$ nix build .#nixosConfigurations.iso.config.system.build.isoImage`

  - [完整代碼](build-live-cd-flake.nix)

  - 不像 nixpkgs/nixos>/default.nix 的建構函數，透過 flake 不需要提供 -I 參數來覆寫建構函數的輸入參數

  - 將 iso 的配置屬性傳遞給 nixpkgs.lib.nixosSystem 的 modules 屬性即可

## [方法] 利用第三方的 nixos-generators

參考，[nixos-generators的使用](../build-qemu-nixos-independ/3rd_nixos-generators_usage.md)
  
## [進階] installation-cd-minimal.nix 源碼解析

- [installation-cd-minimal.nix 源碼](https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix)

- 導入鏈 : installation-cd-minimal.nix
  - [../../profiles/minimal.nix](https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/profiles/minimal.nix)
    - 定義small-configuration的配置選項
    - 不包含任何 graphical 的配置選項
    - documentation : 預設關閉
    - environment.defaultPackages : 預設為空
    - boot.enableContainers : 關閉 container

  - [./installation-cd-base.nix](https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/installer/cd-dvd/installation-cd-base.nix)
    - [./iso-image.nix](https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/installer/cd-dvd/iso-image.nix)
      - 根據提供的 nixos 配置，用於建立一個可啟動的 iso-image
      - Grub 相關設置
      - syslinux 相關設置
      - 建立 isoImage 的內容
        - kernelFile @ /boot
        - initrdFile @ /boot
        - nixos.lable @ /version.txt
        - splashImage @ /isolinux/background.png
        - isolinuxCfg; @ /isolinux/isolinux.cfg
        - /isolinux
        - /boot/efi.img
        - /EFI
        - /boot/grub
        - /EFI/BOOT/efi-background.png
        - /boot/memtest.bin
        - /EFI/BOOT/grub-theme
        - 透過[../../../lib/make-iso9660-image.nix](https://github.com/NixOS/nixpkgs/blob/master/nixos/lib/make-iso9660-image.nix)建構iso

    - [../../profiles/all-hardware.nix](https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/profiles/all-hardware.nix)
      - 用於iso中，提供所有nixos支援硬體的firmware

    - [../../profiles/base.nix](https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/profiles/base.nix)
      - 提供 minimal-installation-CD 中預設安裝的套件
  
    - [../../profiles/installation-device.nix](https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/profiles/installation-device.nix)
      - 提供安裝設備的基礎配置
        - documentation
        - live-cd 中的root用戶和相關設置
        - live-cd 中的openssh設置
        - live-cd 中的網路設置、環境設置

      - [../installer/scan/detected.nix](https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/installer/scan/detected.nix)
        - 為偵測到的硬體，提供 hardware.enableRedistributableFirmware = true 選項，
          優先安裝兼容性高，具有允許分發許可協議的專用firmware

      - [../installer/scan/not-detected.nix](https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/installer/scan/not-detected.nix)
        - 為偵測到的硬體，提供 hardware.enableRedistributableFirmware = true 選項，
          優先安裝兼容性高，具有允許分發許可協議的專用firmware

      - [./clone-config.nix](https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/profiles/clone-config.nix)
        - 用於建構 configuration.nix 

      - [../installer/cd-dvd/channel.nix]()
        - 添加預設的 nixos channel
      
## ref 

- [How to build, install and update a nixos iso/system from a single (remote) flake?](https://discourse.nixos.org/t/how-to-build-install-and-update-a-nixos-iso-system-from-a-single-remote-flake/50098/1)

- [How to create an ISO with config files](https://www.reddit.com/r/NixOS/comments/y1xo2u/how_to_create_an_iso_with_my_config_files/?rdt=42628)

- [Creating a NixOS live CD](https://nixos.wiki/wiki/Creating_a_NixOS_live_CD)

- [How To Create A Custom NixOS ISO](https://haseebmajid.dev/posts/2024-02-04-how-to-create-a-custom-nixos-iso/)

- [Build Your Own NixOS Installer ISO](https://www.youtube.com/watch?v=-G8mN6HJSZE)