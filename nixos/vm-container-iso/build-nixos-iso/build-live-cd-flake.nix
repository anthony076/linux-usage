# 建構，nix build .#nixosConfigurations.iso.config.system.build.isoImage

# nixpkgs.lib.nixosSystem 函數中
# - modules 屬性 : 用於加載 nix-modules
# - specialArgs 屬性 : 用於modules屬性中，所有nix-module都可以使用的共享輸入參數和參數值
# - config、options、pkgs、lib、modulesPath、system、configDir 預設都會透過 specialArgs 傳遞給模組，作為共享的輸入參數
#   模組的輸入參數可直接使用

# 源碼 : https://github.com/NixOS/nixpkgs/blob/68bd364831a89859b40d67bf94618e6163486cae/lib/modules.nix#L176
{
  description = "Minimal NixOS installation media";
  
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

  outputs = { self, nixpkgs, ... }: {
    nixosConfigurations = {
      iso = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ({ pkgs, modulesPath, ... }: {
            # modulesPath 指向 nixpkgs/nixos/modules
            # imports = [ (modulesPath + "/installer/cd-dvd/installation-cd-minimal.nix") ];

            imports = [
              "${nixpkgs}/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix"
            ];

            environment.systemPackages = [ pkgs.fish ];
          })
        ];
      };
    };
  };
}