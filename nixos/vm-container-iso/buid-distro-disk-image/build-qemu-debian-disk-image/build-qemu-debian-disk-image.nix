# 建構命令， nix-build build-debian-vm.nix -A stretch.x86_64.vim
# - 建構完成後，ls -al 得到 result -> /nix/store/xh8kal0pdp7kjxp7q8pvcsvmj6giiddg-debian-12.2-bookworm-amd64
# - 複製到本地，cp /nix/store/xh8kal0pdp7kjxp7q8pvcsvmj6giiddg-debian-12.2-bookworm-amd64/disk-image.qcow2 .

# 測試
# - nix-shell -p qemu
# - qemu-system-x86_64 -hda disk-image.qcow2 -m 512M -serial mon:stdio -nographic

{ pkgs ? import <nixpkgs> {}}:
let
  lib = pkgs.lib;

  makeImageFromDebDist =
    { name, fullName, size ? 4096, urlPrefix
    , packagesList ? "", packagesLists ? [packagesList]
    , packages, extraPackages ? [], postInstall ? ""
    , extraDebs ? []
    , QEMU_OPTS ? "", memSize ? 512
    , createRootFS }:

    let

      # 建立 ${name}.nix
      # 執行命令 : 將 ${packagesLists} 添加到 ./Packages 檔案中
      # 利用 perl 執行 deb/deb-closure.pl 並傳遞 ./Packages 、${urlPrefix}、${packages}
      expr = pkgs.vmTools.debClosureGenerator {
        inherit name packagesLists urlPrefix;
        packages = packages ++ extraPackages;
      };
    in

      # - 執行runInLinuxVM、createEmptyImage
      # - 在 buildCommand 中
      #  - 調用 ${createRootFS}
      #  - 解壓縮 debs
      #  - 將/nix/store中的 deb 套件掛載到VM中，並安裝到VM中
      #  - 執行 postInstall
      (pkgs.vmTools.fillDiskWithDebs {
        inherit name fullName size postInstall QEMU_OPTS memSize createRootFS;
        debs = import expr {inherit (pkgs) fetchurl;} ++ extraDebs;
      }) // {inherit expr;};


  partitionDisk = disk: ''
    sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | ${pkgs.utillinux}/bin/fdisk ${disk}
      o # clear the in memory partition table
      n # new partition
      p # primary partition
      1 # partition number 1
        # default - start at beginning of disk
        # default, extend partition to end of disk
      a # make a partition bootable
      1 # bootable partition is partition 1 -- /dev/sda1
      p # print the in-memory partition table
      w # write the partition table
      q # and we're done
    EOF
    ${pkgs.e2fsprogs}/bin/mkfs.ext4 ${disk}1
    mkdir /mnt
    ${pkgs.utillinux}/bin/mount -t ext4 ${disk}1 /mnt
    touch /mnt/.debug
    mkdir /mnt/proc /mnt/dev /mnt/sys
  '';

  withGrub = expr: args: let
    baudRate = builtins.toString 115200;
    grub-script = pkgs.writeScript "install-grub" ''
      #!/bin/sh
      set -ex
      export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
      grub-install /dev/vda

      echo "UUID=$1  / ext4 defaults 0 0" > /etc/fstab
      sed -e 's/quiet//g' -i /etc/default/grub

      cat - <<EOF >> /etc/default/grub
        GRUB_CMDLINE_LINUX_DEFAULT=""
        GRUB_CMDLINE_LINUX="console=ttyS0,${baudRate}"
        GRUB_TERMINAL="console serial"
        GRUB_SERIAL_COMMAND="serial --speed=${baudRate} --unit=0 --word=8 --parity=no --stop=1"
      EOF

      update-grub2
      sed -e "s|root=/dev/vda1|root=UUID=$1|g" -i /boot/grub/grub.cfg
    '';
    g = {
      postInstall = ''
        ${lib.optionalString (args?postInstall) args.postInstall}
        P=$(${pkgs.utillinux}/bin/blkid --output export /dev/vda1)
        echo -n $P
        eval $(echo $P)
        cp ${grub-script} /mnt/install-grub
        chroot /mnt /install-grub $UUID
        rm /mnt/install-grub
      '';
    };
  in (expr (args // g));

  withExtraPackages = pkgs: expr: args: let
      packages = args.extraPackages or [];
    in expr (args // { extraPackages = packages ++ pkgs; });

  withPartition = expr: args: let
    script = partitionDisk "/dev/vda";
    newargs = args // { createRootFS = script; };
    in expr (builtins.trace (builtins.toJSON newargs) newargs);

  withScript = script: expr: args: let a.postInstall = ''
    ${lib.optionalString (args?postInstall) args.postInstall}
    ${script}
  ''; in  expr (args // a);

  withGrub64 = expr: withExtraPackages ["grub-pc" "linux-image-amd64"] (withGrub (withPartition expr));
  withGrub32 = expr: withExtraPackages ["grub-pc" "linux-image-i386"] (withGrub (withPartition expr));

  # - 傳入 pkgs.vmTools.debDistros 屬性集後，透過映射函數 (name: as: as2: makeImageFromDebDist (as // as2)  產生新的屬性值
  # - pkgs.vmTools.debDistros 的定義，參考，https://github.com/NixOS/nixpkgs/blob/master/pkgs/build-support/vm/default.nix#L855
  #   - packagesList，指定軟體源列表的位置，例如，mirror://ubuntu/dists/trusty/main/binary-i386/Packages.bz2
  #   - urlPrefix 下載軟體源列表網址的前綴，例如，mirror://ubuntu
  #   - packages，要安裝的套件名列表，例如，[ "diffutils" "libc-bin" ]
  diskImageFuns = (lib.mapAttrs (name: as: as2: makeImageFromDebDist (as // as2)) pkgs.vmTools.debDistros);

  basePackages = [
    "systemd"
    "systemd-sysv"
    "apt"
    "apt-transport-https"
    "apt-utils"
    "e2fsprogs"
    "iproute2"
    "nano"
    "iputils-ping"
    "psmisc"
    "bash"
    "bash-completion"
    "isc-dhcp-client"
    "tasksel"
    "netscript-2.4"
    "ca-certificates"
    "curl"
  ];

  withoutRootPassword = withScript "chroot /mnt /usr/bin/passwd -d root";

  withDebianSources = release: withScript ''
    echo "deb http://httpredir.debian.org/debian/ ${release} main" > /mnt/etc/apt/sources.list
    echo "deb http://httpredir.debian.org/debian/ ${release}-updates main" >> /mnt/etc/apt/sources.list
    echo "deb http://security.debian.org/ ${release}/updates main" >> /mnt/etc/apt/sources.list
  '';

  # - 流程
  #   step1，獲取 diskImageFuns.debian12x86_64 屬性集
  #   step2，為debian12x86_64 添加 basePackages ()
  #   step3，為debian12x86_64 添加 grub64
  #   step4，為debian12x86_64 添加 roootPassword 的腳本
  #   step5，為debian12x86_64 添加 DebianSource
  # - withExtraPackages、withGrub64、withoutRootPassword、withDebianSources都返回 expr，(expr = diskImageFuns.debian12x86_64)
  #   因此 withExtraPackages、withGrub64、withoutRootPassword、withDebianSources 等函數，都在為 debian12x86_64 添加新的屬性
  # - withxxxx = 新輸入參數 : 返回屬性 : 可選輸入屬性 : 代碼塊
  # - 只有 debian12x86_64 中的 makeImageFromDebDist 函數，才是真正的構建磁碟映像的函數
  
  stretch.x86_64 = withDebianSources "stretch" (withoutRootPassword (withGrub64 (withExtraPackages basePackages diskImageFuns.debian12x86_64)));

  myPackages = [
    "vim"
  ];
in
  {
    stretch.x86_64 = {
      vanilla = stretch.x86_64 {};
      vim = stretch.x86_64 { extraPackages = myPackages; };
    };
  }
