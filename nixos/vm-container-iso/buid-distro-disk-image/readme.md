## buid-distro-disk-image

- 注意， vmTools.makeImageFromDebDist 可能可以用於建立 none-Debian-distro 的磁碟映像

  (待確認)

  vmTools.makeImageFromDebDist 並不僅限於 Debian 發行版。
  雖然 Alpine Linux 基於 musl libc 和 BusyBox，而不是 Debian 的 APT 系統，但這個工具的設計目的是為了簡化不同 Linux 發行版的虛擬機映像創建過程。

## 範例，在 nixos 上建立 qemu-debian-disk-image

- [完整代碼](build-qemu-debian-disk-image/build-qemu-debian-disk-image.nix)
  - 建構時，會利用 nix-store 中的源碼安裝編譯後的套件
  - 建構完成後會產生 disk-image.qcow2，且 disk-image.qcow2 可以在任何平台上透過qemu客戶端執行，
    運行時不依賴 nix-store

## 範例，在 nixos 上建立 qemu-ubuntu-disk-image

- [完整代碼](build-qemu-ubuntu-disk-image/readme.md)

## 範例，在 nixos 上建立 qemu-alpine-disk-image (失敗)

- 流程

  ```shell
  # 建立磁碟映像
  qemu-img create -f qcow2 alpine-disk.img 4G
 
  # 建立nbd裝置
  modprobe nbd max_part=8

  # 掛載nbd裝置
  qemu-nbd -c /dev/nbd0 alpine-disk.img

  # 為 ndb 裝置建立分區
  fdisk /dev/nbd0

  # 將nbd分區格式化為ext4
  mkfs.ext4 /dev/nbd0p1

  # 建立 ndb 裝置的掛載點 並掛載
  mkdir /mnt/alpine
  mount /dev/nbd0p1 /mnt/alpine/

  # ==== 安裝 apk 套件管理器 ====
  # 最新版本的 apk 沒有 --initdb 選項 (apk v.2.14.1)
  nix-shell -p apk-tools  

  # 指定版本，也沒有 --initdb (apk v.2.12.1)
  nix-shell -I nixpkgs=https://github.com/NixOS/nixpkgs/archive/refs/tags/22.05.tar.gz -p apk-tools

  # apk 命令無法使用
  apk --root /var/alpine --update-cache --arch x86_64 add alpine-base

  出現以下錯誤
  ERROR: Unable to lock database: No such file or directory
  ERROR: Failed to open apk database: No such file or directory

  # apk 命令無法使用
  apk --root /var/alpine cache clean
  
  出現以下錯誤
  ERROR: '--root' is not an apk command. See 'apk --help'.
  ```

  - apk 命令無法使用的原因
    
    在 NixOS 上通過 nix-shell -p apk-tools 提供的 apk 命令的版本，與 Alpine Linux 官方版本不同，並且一些參數可能已被修改或移除，
    造成 apk 命令無法使用 --initdb 參數，也就無法使用 apk 安裝套件

  - TODO 

    nix 可以安裝在其他作業系統

    改在 alpine 上安裝nix後，裡面alpine上的 apk 和 nix 建立 qemu-alpine-disk-image

    參考，[在alpine上使用nix](alpine+nix.md)


