@echo off

cd %~dp0

qemu-system-x86_64.exe -m 4G -smp 4 ^
-bios OVMF.fd ^
-drive file=disk-image.qcow2,format=qcow2 ^
-net nic -net user,hostfwd=tcp::2222-:22

::-accel whpx,kernel-irqchip=off ^
::-machine vmport=off ^
::-display sdl ^

@REM -m 4G -smp 4 : 0130分脫離黑畫面，0202(+32s)完全進入系統
@REM -m 8G -smp 4 : 0121分脫離黑畫面，0158(+37s)完全進入系統
@REM -m 8G -smp 4 + accel: 0245分脫離黑畫面，0259(+15s)完全進入系統
