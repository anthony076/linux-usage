

{ lib, vmTools, udev, gptfdisk, util-linux, dosfstools, e2fsprogs }:
vmTools.makeImageFromDebDist {
  inherit (vmTools.debDistros.ubuntu2004x86_64) name fullName urlPrefix packagesLists;

  # 要安裝在 qemu-ubuntu-vm 中的套件
  # 建構過程中，會先將源碼下載到nixos，在安裝到 qemu-ubuntu-vm 中
  packages = lib.filter (p: !lib.elem p [
    "g++" "make" "dpkg-dev" "pkg-config"
    "sysvinit"
  ]) vmTools.debDistros.ubuntu2004x86_64.packages ++ [
    "systemd" # init system
    "init-system-helpers" # satisfy undeclared dependency on update-rc.d in udev hooks
    "systemd-sysv" # provides systemd as /sbin/init

    "linux-image-generic" # kernel
    "initramfs-tools" # hooks for generating an initramfs

    "e2fsprogs" # initramfs wants fsck
    "grub-efi" # boot loader

    "apt" # package manager
    "ncurses-base" # terminfo to let applications talk to terminals better
    "openssh-server" # Remote login
    "dbus" # networkctl
  ];

  size = 8192;

  # 建立磁碟分割
  createRootFS = ''
    # ==== 建立分割區 ==== 
    disk=/dev/vda
    
    # 分割區 1 (ESP)：大小 100MB，類型為 EFI 系統分割區 (ef00)，標籤為 esp
    # 分割區 2 (root)：使用磁碟剩餘空間，類型為 Linux 文件系統 (8300)，標籤為 root
    ${gptfdisk}/bin/sgdisk $disk \
      -n1:0:+100M -t1:ef00 -c1:esp \
      -n2:0:0 -t2:8300 -c2:root
    
    # 更新內核的分割區表以反映新的分割區。
    ${util-linux}/bin/partx -u "$disk"
    
    # ===== 建立文件系統 =====
    
    # (/dev/vda1) ESP 分割區：格式化為 FAT32 文件系統，標籤為 ESP
    ${dosfstools}/bin/mkfs.vfat -F32 -n ESP "$disk"1

    # (/dev/vda2) root 分割區：格式化為 EXT4 文件系統，標籤為 root
    part="$disk"2
    ${e2fsprogs}/bin/mkfs.ext4 "$part" -L root
    
    # ==== 掛載點創建與掛載 ====
    
    # Root 分割區 掛載到 /mnt
    mkdir /mnt
    ${util-linux}/bin/mount -t ext4 "$part" /mnt
    
    # 創建必要目錄
    mkdir -p /mnt/{proc,dev,sys,boot/efi}
    
    # ESP 分割區 掛載到 /mnt/boot/efi
    ${util-linux}/bin/mount -t vfat "$disk"1 /mnt/boot/efi
    
    touch /mnt/.debug
  '';

  postInstall = ''
    # update-grub needs udev to detect the filesystem UUID -- without,
    # we'll get root=/dev/vda2 on the cmdline which will only work in
    # a limited set of scenarios.

    # - systemd-udevd 啟用 udev Kernel Device Manager 服務，用於監聽 kernel uevents
    # - udevadm : udev 管理工具，用於控制 systemd-udevd 的行為
    #   - udevadm trigger : 触发 udev 重新处理设备事件的命令，用于在没有实际硬件插拔的情况下，模擬設備重新插拔的行為
    #   - udevadm settle : 進入檢視並等待 udev 事件处理完成的狀態，當所有udev event queue中的事件都被處理後，退出watch狀態
    
    ${udev}/lib/systemd/systemd-udevd &
    ${udev}/bin/udevadm trigger
    ${udev}/bin/udevadm settle

    ${util-linux}/bin/mount -t sysfs sysfs /mnt/sys

    # 切換 root  
    chroot /mnt /bin/bash -exuo pipefail <<CHROOT
    export PATH=/usr/sbin:/usr/bin:/sbin:/bin

    # update-initramfs needs to know where its root filesystem lives,
    # so that the initial userspace is capable of finding and mounting it.

    # [issued]
    # echo LABEL=root / ext4 defaults > /etc/fstab
    echo "LABEL=root / ext4 defaults 0 1" > /etc/fstab
    
    # 生成初始內存映像(initramfs)，actually generate an initramfs
    # -k all : 對系統上已安裝的內核版本都進行操作，而不是僅針對特定內核版本
    # -c : 表示 create(創建)，新的 initramfs 會被保存到 /boot/initrd.img-<kernel_version>，
    #      例如，update-initramfs: Generating /boot/initrd.img-5.15.0-73-generic
    # 此命令會根據 /etc/initramfs-tools/initramfs.conf 中的配置，
    # 將 /lib/modules/<kernel_version>/ 中對應內核模組添加到產出的initramfs檔案中
    update-initramfs -k all -c
    
    # ==== 添加apt軟體源====
    
    # APT sources so we can update the system and install new packages

    # 原始版本
    #cat > /etc/apt/sources.list <<SOURCES
    #deb http://archive.ubuntu.com/ubuntu focal main restricted universe
    #deb http://security.ubuntu.com/ubuntu focal-security main restricted universe
    #deb http://archive.ubuntu.com/ubuntu focal-updates main restricted universe
    #SOURCES

    # Taiwan 版本
    cat > /etc/apt/sources.list <<SOURCES
    deb http://tw.archive.ubuntu.com/ubuntu/ focal-updates multiverse
    deb http://tw.archive.ubuntu.com/ubuntu/ focal-backports main restricted universe multiverse
    deb http://security.ubuntu.com/ubuntu focal-security main restricted
    deb http://security.ubuntu.com/ubuntu focal-security universe
    deb http://security.ubuntu.com/ubuntu focal-security multiverse
    SOURCES
    
    # ==== 安裝 boot-loader 到 efi 分區 ==== 

    # 修改 grub 配置，修改 boot-loader 啟動 kernel 的啟動參數
    # 移除 quiet 才能在開機時看到過程
    # Install the boot loader to the EFI System Partition
    # Remove "quiet" from the command line so that we can see what's happening during boot

    # 若添加 noapic 後仍遇到 Kernel panic - not syncing: IO-APIC + timer doesn't work!  Boot with apic=debug and send a report.  Then try booting with the 'noapic' option.
    # 可以選擇重試，有機會可以通過
    
    cat >> /etc/default/grub <<EOF
    GRUB_TIMEOUT=5
    #GRUB_CMDLINE_LINUX="console=ttyS0"  # original-version
    GRUB_CMDLINE_LINUX="console=ttyS0 noapic root=LABEL=root"
    #GRUB_CMDLINE_LINUX="console=ttyS0 noapic"
    GRUB_CMDLINE_LINUX_DEFAULT=""
    EOF

    # 刪除 /etc/default/grub 檔案中，包含TIMEOUT_HIDDEN 字樣的行
    sed -i '/TIMEOUT_HIDDEN/d' /etc/default/grub

    # 修改 grub 的配置文件後，需要執行update-grub套用新的配置
    update-grub

    # 安裝 bootloader
    # [錯誤] /usr/lib/grub/x86_64-efi/modinfo.sh doesn't exist. Please specify --target or --directory.
    # 需要 grub-efi 套件來建立 x86_64-efi 的目錄
    grub-install --target x86_64-efi
    
    # ==== 設置網路服務 ==== 
    
    # Configure networking using systemd-networkd
    ln -snf /lib/systemd/resolv.conf /etc/resolv.conf
    
    systemctl enable systemd-networkd systemd-resolved
    
    cat >/etc/systemd/network/10-eth.network <<NETWORK
    [Match]
    Name=en*
    Name=eth*

    [Link]
    RequiredForOnline=true

    [Network]
    DHCP=yes
    NETWORK
    
    # ==== open-ssh 相關 ====
    # 刪除 SSH host keys
    # Remove SSH host keys -- the image shouldn't include
    # host-specific stuff like that, especially for authentication purposes.
    rm /etc/ssh/ssh_host_*

    # 建立在 sshd 啟動前，重新產生 SSH host keys的服務
    # But we do need SSH host keys, so generate them before sshd starts
    cat > /etc/systemd/system/generate-host-keys.service <<SERVICE
    [Install]
    WantedBy=ssh.service
    [Unit]
    Before=ssh.service
    [Service]
    ExecStart=dpkg-reconfigure openssh-server
    SERVICE
    
    # 啟用產生金鑰的服務
    systemctl enable generate-host-keys

    # 設置root密碼
    echo root:root | chpasswd

    # 建立用戶自定義的公鑰
    # Prepopulate with my SSH key
    mkdir -p /root/.ssh
    
    chmod 0700 /root
    
    cat >/root/.ssh/authorized_keys <<KEYS
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN3EmXYSXsimS+vlGYtfTkOGuwvkXU0uHd2yYKLOxD2F linus@geruest
    KEYS
    CHROOT

    # 卸載
    ${util-linux}/bin/umount /mnt/boot/efi
    ${util-linux}/bin/umount /mnt/sys
  '';
}
