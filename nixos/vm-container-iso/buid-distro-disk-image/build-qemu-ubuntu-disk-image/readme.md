## 使用方式

- 查看輸出，`$ nix flake show`

- 建構命令，`$ nix build .#packages.x86_64-linux.build-ubuntu`

- 檢視建構紀錄，

  ```shell
  ls -al .
  nix log /nix/store/<hash>-ubuntu-20.04-focal-amd64.drv
  ```

## 在 windows-host 上進行測試

- 在 nixos 上，`$ cp result/disk-image.qcow2 /root/build-ubuntu`

- 在 windows 上
  
  ```shell
  # 複製硬碟映像檔
  scp -P 2222 -r root@localhost:/root/build-ubuntu/disk-image.qcow2 .

  # 執行
  # 注意，開機需約 1-2分鐘
  qemu-system-x86_64.exe -m 4G -smp 4 -bios OVMF.fd ^
  -drive file=disk-image.qcow2,format=qcow2 ^
  -net nic -net user,hostfwd=tcp::2222-:22
  ```

## 注意事項

- nixpkgs的版本，造成post-install中`systemd-udevd命令`和`udevadm命令`的缺失

  原作者使用以下版本的 nixpkgs
  > nixpkgs.url = "github:lheckemann/nixpkgs/foreign-distros";
  
  使用原作者提供的flake.lock 可正常建構，透過flake.lock可找到foreign-distros對應的commit。
  
  但是原始的foreign-distros的commit已經被刪除，在沒有使用原作者flake.lock的情況下，建構會遇到錯誤

  錯誤的原因是`新版本的nixpkgs`中的systemdMinimal套件，沒有提供建構過程需要的`systemd-udevd命令`和`udevadm命令`
  
  透過[nixpkgs網頁](https://search.nixos.org/packages?channel=24.11&show=systemdMinimal&from=0&size=50&sort=relevance&type=packages&query=systemd-minimal)中的`Programs provided`可以看到該套件提供的命令

  - channel=24.11的systemdMinimal(v256.8)，有提供udevadm命令，但沒有提供systemd-udevd命令
  - channel=23.11的systemdMinimal(v254.10)，沒有提供udevadm和systemd-udevd命令
  - channel=23.05的systemdMinimal，有提供udevadm和systemd-udevd命令

  因此，最終使用了`nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";`進行建構

- 解決`systemd-udevd命令`和`udevadm命令`缺失的其他可能方法
  
  - `方法`，移除systemd-udevd和udevadm
    - systemd-udevd 是 Linux 系統中一個關鍵的守護進程，負責動態管理設備文件

      通過監聽內核發出的設備事件（即 uevents），並根據預定義的 udev 規則處理這些事件，從而在 /dev 目錄中創建或刪除設備節點

    - 對於建立系統，systemd-udevd不是必需的，可以替換成其他套件，或是進入系統後再手動安裝

    - 在[build-qemu-debian-disk-image](../build-qemu-debian-disk-image/build-qemu-debian-disk-image.nix)
      的範例中就沒有使用到此命令

  - `方法`，(有風險) 修改成正確的命令

    可以在配置中添加systemd套件，但`暫時`不調用`systemd-udevd命令`和`udevadm命令`，
    等建構完成後，進入系統透過手動調試出正確的命令後，

    重新修改配置，再重新建立正確的 post-install 命令

    但是有可能有風險，可能會造成其他命令的錯誤

    ```
    + update-grub
    Sourcing file `/etc/default/grub'
    Sourcing file `/etc/default/grub.d/init-select.cfg'
    Generating grub configuration file ...
    # 
    grub-probe: error: unknown filesystem.
    Found linux image: /boot/vmlinuz-5.4.0-26-generic
    Found initrd image: /boot/initrd.img-5.4.0-26-generic
    /usr/sbin/grub-probe: error: unknown filesystem.
    done
    + grub-install --target x86_64-efi
    Installing for x86_64-efi platform.
    grub-install: error: unknown filesystem.
    [ 1804.469625] reboot: Power down
    ```

    根據作者提示
    > update-grub needs udev to detect the filesystem UUID -- without, we'll get root=/dev/vda2 on the cmdline which will only work in a limited set of scenarios.

  - `方法`，替換成等效的命令

    其他和systemd-udevd等校的套件
    - mdev
    - devfs
    - 手動管理  

  - `方法`，在 flake.nix 中利用原作者中的 flake.lock 找到實際的commit，並透過 commit 指定 nixpkgs

    根據原作者提供的 flake.lock
    ```
    "nixpkgs": {
      "locked": {
        "lastModified": 1698575171,
        "narHash": "sha256-Qbxmi5UzUx6jSpeLVbACpFCUcx8KJlV7rB6fPte5Zos=",
        "owner": "lheckemann",
        "repo": "nixpkgs",
        "rev": "5a4f40797c98c8eb33d2e86b8eb78624a36b83ea",
        "type": "github"
      },
      "original": {
        "owner": "lheckemann",
        "ref": "foreign-distros",
        "repo": "nixpkgs",
        "type": "github"
      }
    },
    ```

    將原作者的`nixpkgs.url = "github:lheckemann/nixpkgs/foreign-distros";`

    替換為`nixpkgs.url = "github:lheckemann/nixpkgs/5a4f40797c98c8eb33d2e86b8eb78624a36b83ea";`

- 建構過程的 IO-APIC 的 Kernel panic 錯誤

  若添加 noapic 後仍遇到 `Kernel panic - not syncing: IO-APIC + timer doesn't work!  Boot with apic=debug and send a report. Then try booting with the 'noapic' option.`
  
  可以選擇重試，不需要修改代碼，有機會可以通過

- 預設僅支援 EFI 開機，因此，qemu 的啟動參數必須使用`-bios OVMF.fd`
  
## ref

- Building Ubuntu images in Nix
  - [文章](https://linus.schreibt.jetzt/posts/ubuntu-images.html)
  - [完整代碼](https://github.com/lheckemann/nixbuntu-samples)