{
  inputs = {
    #nixpkgs.url = "github:lheckemann/nixpkgs/foreign-distros";  # WORK，使用原作者提供的 flake.lock 可正常建構，原 foreign-distros commit 已經被刪除

    #nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";  # FAIL
    #nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";     # FAIL
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";      # WORK 
    flake-parts.url = "github:hercules-ci/flake-parts";
  };

  outputs = inputs: inputs.flake-parts.lib.mkFlake { inherit inputs; } ({ lib, ... }: {
    systems = [ "x86_64-linux" ];
    perSystem = { pkgs, ... }: {
      packages = lib.mapAttrs' (name: type: {
        name = lib.replaceStrings [".nix"] [""] name;
        value = pkgs.callPackage ./examples/${name} {};
      }) (builtins.readDir ./examples);
    };
  });
}
