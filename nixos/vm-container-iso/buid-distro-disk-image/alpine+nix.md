
## ref

- [NIX Package Manager Install on Alpine Linux](https://gist.github.com/danmack/b76ef257e0fd9dda906b4c860f94a591)

- [Alpine + Nix](https://github.com/redoracle/nixos)

- [install nix in alpine-docker](https://github.com/fretlink/docker-nix/blob/master/alpine/Dockerfile)