## disko 的使用

- 為什麼需要 disko
  
  利用 disko 實現磁碟相關操作的自動化，包含: 磁區分割、檔案系統格式化、路徑掛載 ... 等操作

  若不使用 disko ，在安裝nixos系統前，需要手動配置磁碟分區，透過 disko 可以簡化此步驟

- 兩種測試disko命令的方法
  - 方法，[在 nixos 安裝過程中測試 disko 命令](#範例在-nixos-安裝過程中測試-disko-命令)
  - 方法，插入 USB，以USB進行disko配置測試
  - 方法，[透過 loop 進行測試](#範例透過-loop-測試-disk-配置)
  - 方法，不推薦，[透過 ramdisk 進行測試](#範例透過-ramdisk-進行測試-不推薦)

## disko 命令的使用

- 語法，`disko --mode 要執行的操作 [其他命令參數] 硬碟配置檔.nix`

- 可用命令參數
  - `--mode`，指定要進行的磁碟操作，可以是`單一操作`或`多個順序操作`，只能是以下的可用值
    - destroy : 卸載掛載的檔案系統，並刪除配置文件中建立的磁碟分區和磁碟分區表
    - format : 若配置文件中的檔案系統不存在，建立該檔案系統
    - mount : 掛在配置文件中指定的檔案系統
    - format,mount : 檢測缺少的檔案系統並掛載
    - destroy,format,mount : 刪除已有的檔案系統，重新建立並掛載

    - disko : 舊版參數，相當於 destroy,format,mount ，在新版disko中已棄用
    - 錯誤用法，destroy,mount : 不是可用值，且不能刪除檔案系統又掛載

  - `--yes-wipe-all-disks`
    - 如果命令參數中有 destroy 的操作，預設會提示用戶輸入yes確認要執行刪除的操作
    - 透過此參數可以直接進行，用戶不需要確認
  
  - `--dry-run` : 對要執行的操作建立shell腳本，但不執行

  - `--flake` : 指定 flake.nix 的檔案，從 flake.nix 獲取 disko 需要的硬碟配置文件

- 免安裝執行 disko

  常用於第一次安裝系統時，還無法使用`nix profile install`或其他方式安裝disko命令

  > nix --experimental-features "nix-command flakes" run github:nix-community/disko/latest -- --mode destroy,format,mount /tmp/disk-config.nix

  其中，
  - `--experimental-features "nix-command flakes"`，啟用 nix command，使得 nix run 有效
  - `github:nix-community/disko/latest`，從網路下載 disko 源碼
  - `-- --mode ...`，第一個`--` 用於指示後方所有的符號都是命令參數

## [配置] 硬碟配置文件

- 範例，[hybrid bootloader : grub + efi](disko-examples/disko-in-nixos-installation/disk-config.nix)

- 範例，建立無需物理設備支持的掛載點 (用於定義虛擬文件系統tmpfs)

  ```
  nodev = {
    "/tmp" = {
      fsType = "tmpfs";
      mountOptions = [
        "size=200M"
      ];
    };
  };
  ```

- 範例，建立 swap 和 lvm 

  目標建立以下的分區
  ```shell
  NAME             MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
  fd0                2:0    1    4K  0 disk
  loop0              7:0    0  1.1G  1 loop /nix/.ro-store
  sda                8:0    0   15G  0 disk
  ├─sda1             8:1    0    1M  0 part
  ├─sda2             8:2    0  500M  0 part /mnt/boot
  ├─sda3             8:3    0    4G  0 part [SWAP]
  └─sda4             8:4    0 10.5G  0 part
    └─root_vg-root 254:0    0 10.5G  0 lvm  /mnt/persist
                                            /mnt/nix
                                            /mnt
  ```

  - disk-config.nix
  - 透過`$  disko --mode disko disko.nix --arg device '"/dev/sda"'` 建立

  ```nix
  {
    device ? throw "Set this to your disk device, e.g. /dev/sda",
    ...
  }: {
    disko.devices = {
      disk.main = {
        inherit device;
        type = "disk";
        content = {
          type = "gpt";
          partitions = {
            boot = {
              name = "boot";
              size = "1M";
              type = "EF02";
            };
            esp = {
              name = "ESP";
              size = "500M";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
              };
            };
            swap = {
              size = "4G";
              content = {
                type = "swap";
                resumeDevice = true;
              };
            };
            root = {
              name = "root";
              size = "100%";
              content = {
                type = "lvm_pv";
                vg = "root_vg";
              };
            };
          };
        };
      };
      lvm_vg = {
        root_vg = {
          type = "lvm_vg";
          lvs = {
            root = {
              size = "100%FREE";
              content = {
                type = "btrfs";
                extraArgs = ["-f"];

                subvolumes = {
                  "/root" = {
                    mountpoint = "/";
                  };

                  "/persist" = {
                    mountOptions = ["subvol=persist" "noatime"];
                    mountpoint = "/persist";
                  };

                  "/nix" = {
                    mountOptions = ["subvol=nix" "noatime"];
                    mountpoint = "/nix";
                  };
                };
              };
            };
          };
        };
      };
    };
  }

  ```

## 範例，透過 loop 測試 disk 配置

- step，創建 image，`$ dd if=/dev/zero of=./virtual_disk.img bs=1M count=100`

- step，創建 loop 設備，`$ losetup -fP ./virtual_disk.img`
  - losetup 是用來管理和設置loopback 設備的命令
  - 此命令將一個檔案（例如 virtual_disk.img）綁定到可用的 loopback 設備
  - `-f` : 查找第一個未被使用的 loopback 設備
  - `-P` : 掃描映像檔內的分區
    - 若映像檔被格式化為帶有分區表的磁碟映像（例如 MBR 或 GPT 分區表），-P 會自動生成對應的分區設備
    - 例如，如果 virtual_disk.img 包含兩個分區，系統會自動創建 /dev/loop0p1 和 /dev/loop0p2 兩個分區
  - 若創建成功，
    - 會自動創建`/dev/loopx`的路徑，`$ ls -al /dev/loop0`
    - 或透過`lsblk`檢視

      ```
      NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
      fd0      2:0    1    4K  0 disk
      loop0    7:0    0  100M  0 loop
      ```

    - 或透過`losetup -a`檢視

- step，格式化，`$ mkfs.ext4 /dev/loop0`

- step，建立硬碟配置文件，disk-config.nix

  ```nix
  {
    disko.devices = {
      disk = {
        main = {
          type = "disk";

          device = "/dev/loop0";

          content = {
            type = "gpt";

            partitions = {
              pa = {
                size = "50M";
                content = {
                  type = "filesystem";
                  format = "ext4";
                  mountpoint = "/mnt/pa";
                };
              };
              pb = {
                size = "40M";
                content = {
                  type = "filesystem";
                  format = "ext4";
                  mountpoint = "/mnt/pb";
                };
              };
            };
          };
        };
      };
    };
  }
  ```

- 執行硬碟分割，`$ disko --mode destroy,format,mount disk-config.nix`

  執行成功後，會建立以下的磁碟分區

  ```shell
  root@nixos ~/dt# lsblk
  NAME      MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
  loop0       7:0    0  100M  0 loop
  ├─loop0p1 259:1    0   50M  0 part /mnt/mnt/pa
  └─loop0p2 259:3    0   40M  0 part /mnt/mnt/pb
  ```

- 刪除磁碟分區，`$ disko --mode destroy disk-config.nix`

- 取消 loop 設備，`$ losetup -d /dev/loop0`

## 範例，透過 ramdisk 進行測試 (不推薦)

- 不推薦原因

  ramdisk 是基於內存的基於內存的設備，會在系統重啟後消失，因此不像實體磁碟那樣具有持久的硬體標籤或分區標籤，

  即使在硬碟配置文件中指定了分區的標籤，disko在掛載時，還是會因為找不到分區標籤而出現以下錯誤

  ```
  + mkfs.ext4 /dev/disk/by-partlabel/disk-main-pa
  mke2fs 1.47.1 (20-May-2024)
  The file /dev/disk/by-partlabel/disk-main-pa does not exist and no size was specified.
  ```

- disk-config.nix

  ```nix
  disko.devices = {
    disk = {
      main = {
        type = "disk";
        device = "/dev/ram0";
        content = {
          type = "gpt";
          partitions = {
            pa = {
              size = "50M";
              label = "disk-main-pa";
              content = {
                type = "filesystem";
                format = "ext4";
                mountpoint = "/mnt/pa";
              };
            };
            pb = {
              size = "40M";
              label = "disk-main-pb";
              content = {
                type = "filesystem";
                format = "ext4";
                mountpoint = "/mnt/pb";
              };
            };
          };
        };
      };
    };
  };
  ```

- 測試

  ```
  # 建立 ramdisk
  modprobe brd rd_nr=1 rd_size=102400
  
  # 確認 ramdisk 被建立
  la -al /dev | grep ram

  # 注意，需要添加 -a 才看的到 /dev/ram0 的裝置
  lsblk -a

  # 格式化 ramdisk
  mkfs.ext4 /dev/ram0
  
  # 透過 disko 對 ram0 建立分區
  # 錯誤，會出現 The file /dev/disk/by-partlabel/disk-main-pa does not exist and no size was specified. 的錯誤，而無法掛載
  disko --mode format,mount disk-config.nix

  # 移除 ramdisk
  rmmod brd
  ```

## 範例，在 nixos 安裝過程中測試 disko 命令

- [完整代碼](disko-examples/disko-in-nixos-installation/)

- 概念
  - 傳統 nixos 安裝過程中，執行`$ nixos-generate-config --root /mnt`，會產生以下兩個檔案
    - `/mnt/etc/nixos/hardware-configuration.nix`，包含硬碟的相關設置
    - `/mnt/etc/nixos/configuration.nix`
  
  - 由於要使用disko自定義硬碟配置，因此需要取消硬碟的相關設置，改用
    `$ nixos-generate-config --no-filesystems --root /mnt`

  - 使用disko自定義硬碟配置時，nixos 的安裝流程變成
    - 編寫硬碟配置文件(disk-config.nix)
    - 透過disko命令產生檔案系統
    - 透過nixos-generate-config產生nixos系統配置 (需要添加 --no-filesystems，以取消硬碟相關的自動配置)
    - 在配置文件(configuration.nix或flake.nix)中，添加`disko套件`和`硬碟配置檔案`
    - 執行nixos安裝

- step，[qemu的啟動參數](disko-examples/disko-in-nixos-installation/00_install.bat)

- step，設置root密碼，並切換到root帳號，並在 windows-host 上改用 ssh 登入
  
  ```
  sudo passwd root
  su -
  ```

  在 nixos 的安裝程序中，默認openssh是開啟的，透過以下命令連接到nixos的安裝程序，
  <font color=blue>注意，要先設置密碼才能登入</font>

  > ssh -p 2222 root@localhost 

- step，查看 disk-name，

  以下為例，disk-name 為 sda

  ```
  lsblk

  NAMT  MAJ:MIN   RM  SIZE RO TYPE MOUNTPOINTS
  sda     8:0     0   10G  0  disk
  ```

- step，建立硬碟配置文件
  
  ```
  cd /tmp
  
  # 從官方下載範例
  curl https://raw.githubusercontent.com/nix-community/disko/master/example/hybrid.nix -o /tmp/disk-config.nix

  vi disk-config.nix
  ```

  修改[disk-config.nix的內容](disko-examples/disko-in-nixos-installation/disk-config.nix)

- step，執行，透過 disko 建立分區

  > nix --experimental-features "nix-command flakes" run github:nix-community/disko/latest -- --mode destroy,format,mount /tmp/disk-config.nix

  執行後會建立以下的磁碟分區 (by fdisk -l)
  
  ```
  Device      Start     End       Sectors   Size    Type
  /dev/sda1   2048      4095      2048      1M      BIOS boot
  /dev/sda2   4096      1052671   1048576   512M    EFI System
  /dev/sda3   1052672   20969471  19916800  9.5G    Linux filesystem
  ```

- step，產生nixos配置文件(configuration.nix)

  由於要使用disko自定義硬碟配置，因此加上`--no-filesystems`，避免
  hardware-configuration.nix 自動產生硬碟相關的配置

  > nixos-generate-config --no-filesystems --root /mnt

- step，將硬碟配置檔(disk-config.nix)移動到/mnt，並在 configuration.nix 中加載硬碟配置檔
  
  - 移動硬碟配置檔，`$ mv /tmp/disk-config.nix /mnt/etc/nixos/`

  - 在 configuration.nix 中，加載硬碟配置檔，

    ```nix
    # configuration.nix

    { config, lib, pkgs, ... }:

    {
      imports =
        [       
            ./hardware-configuration.nix

            # 載入 disko 
            (import "${builtins.fetchTarball {url="https://github.com/nix-community/disko/archive/master.tar.gz";}}/module.nix")
            
            # 載入 硬碟配置
            ./disk-config.nix
    ];

      boot.loader.grub.enable = true;
      boot.loader.grub.efiSupport = true;
      boot.loader.grub.efiInstallAsRemovable = true;
      # boot.loader.efi.efiSysMountPoint = "/boot/efi";

      # 不需要設置 boot.loader.grub.device , disko 會自動處理此配置選項
      # Define on which hard drive you want to install Grub.
      # boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

      # Set your time zone.
      # time.timeZone = "Europe/Amsterdam";

      environment.systemPackages = with pkgs; [
        #vim
        #wget
      ];


      # Enable the OpenSSH daemon.
      services.openssh.enable = true;
      services.openssh.settings.PermitRootLogin = "yes";
      services.openssh.settings.PasswordAuthentication = true;

      system.stateVersion = "24.11";

    }
    ```

- step，安裝系統 (套用配置)，`$ nixos-install`

- step，`$ halt`

- step，重開機的qemu啟動腳本
  
  ```
  qemu-system-x86_64 ^
  -m 4096 -smp 4 ^
  -accel whpx,kernel-irqchip=off ^
  -machine vmport=off ^
  -display sdl ^
  -drive file=nixos-disk.qcow2,format=qcow2 ^
  -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
  ```

## ref 

- 官方文檔
  - [disko quick-start](https://github.com/nix-community/disko/blob/master/docs/quickstart.md)
  - [完整文檔](https://github.com/nix-community/disko/blob/master/docs/INDEX.md)
  - [使用disko建立disk-image](https://github.com/nix-community/disko/blob/master/docs/disko-images.md)

- 配置文件範例
  - [官方提供硬碟配置範例](https://github.com/nix-community/disko/tree/master/example)
  - [disko-template](https://github.com/nix-community/disko-templates)

