::https://channels.nixos.org/nixos-24.11/latest-nixos-minimal-x86_64-linux.iso

"C:\Program Files\qemu\qemu-system-x86_64.exe" ^
-m 4096 -smp 4 ^
-accel whpx,kernel-irqchip=off ^
-machine vmport=off ^
-display sdl ^
-drive file=nixos-disk.qcow2,format=qcow2 ^
-net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
