{
  disko.devices = {
    disk = {
      main = {
        # 配置類型
        type = "disk";

        # 指定硬碟
        # 格式 device = "/dev/<disk-name>"   <disk-name>，可透過 lsblk 取得
        device = "/dev/sda";

        content = {
          # partitions-table 類型
          type = "gpt"; # gpt=general-partitions-table

          # partitions屬性，建立硬碟分區
          # - boot、ESP、root 是自定義的分區實例名，對結果沒有影響
          # - size屬性 和 type屬性 是必要的
          # - content屬性用於更詳細的自定義內容

          partitions = {
            # 分區1
            boot = {
              size = "1M";
              type = "EF02"; # for grub MBR 
            };
            
            # 分區2
            ESP = {
              size = "512M";  # 分區大小
              type = "EF00";  # 分區類型
              content = {     # 設置分區內容，根據分區類型決定要不要設置content屬性
                type = "filesystem";        
                format = "vfat";            # 分區使用的檔案系統
                mountpoint = "/boot";       # 分區掛載點
                mountOptions = [ "umask=0077" ];      # 設置分區選項，通常用於設置分區選項
              };
            };
            
            # 分區3
            root = {
              size = "100%";    # 設置為剩餘空間的 100%
              content = {
                type = "filesystem";
                format = "ext4";
                mountpoint = "/";
              };
            };
          };
        };
      };
    };
  };
}