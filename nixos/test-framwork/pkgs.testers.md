## pkg.testers 測試框架概述

## pkg.testers 可用函數概述

- pkgs.testers.hasPkgConfigModule : 檢查系統是否有特定的 pkg-config 模組
  ```
  pkgs.testers.hasPkgConfigModule "libxml2"
  ```

- pkgs.testers.hasPkgConfigModules : 檢查系統是否有特定的一組 pkg-config 模組
  ```
  pkgs.testers.hasPkgConfigModules ["libxml2" "libpng"]
  ```

- pkgs.testers.nixosTest : 
  - 用於定義測試環境，測試環境是利用 qemu 建立的虛擬主機，並定義該主機使用的配置以建立虛擬的nixos系統
  - 返回一個可以用來運行具體測試的測試對象
  - 注意，nixosTest只是定義測試環境，不會執行測試，
    - 透過 nix-build 會自動調用 runNixOSTest
    - 若是在 nix repl 中調用 pkgs.testers.nixosTest，還需要手動執行測試??

  ```
  pkgs.testers.nixosTest {
    name = "my-nixos-test";
    system = "x86_64-linux";
    modules = [
      ./modules/my-module.nix
    ];
  }
  ```

- [pkgs.testers.runNixOSTest](pkgs.testers-runNixOSTest.md) : 
  - nixosTest 用於定義，runNixOSTest 用於執行，根據 nixosTest 定義的測試環境，實際執行測試的函數
  - 執行測試並返回測試的結果

- pkgs.testers.runCommand : 執行命令並捕獲輸出

- pkgs.testers.shellcheck : 運行 shellcheck 錯誤檢查

- pkgs.testers.testEqualDerivation : 檢查兩個derivation是否相等
  ```
  pkgs.testers.testEqualDerivation (import ./.) (import ./other-directory/.)
  ```

- pkgs.testers.testMetaPkgConfig : 測試 pkg-config 的 meta 
  ```
  pkgs.testers.testMetaPkgConfig "libxml2"
  ```

- pkgs.testers.testBuildFailure : 測試 derivation 建構失敗的狀況

- pkgs.testers.testVersion : 測試 dervivation 的版本號

- pkgs.testers.testEqualContents : 比較文件或 derivation 的內容是否相等

- pkgs.testers.invalidateFetcherByDrvHash : 使指定的 fetcher 無效
  ```
  pkgs.testers.invalidateFetcherByDrvHash "https://example.com/fetcher"
  ```

- pkgs.testers.lycheeLinkCheck : 利用 lychee 套件檢查連結

- pkgs.testers.override : 覆蓋derivation
  ```
  pkgs.testers.override myPackage {
    version = "1.0.1";
    src = fetchTarball "https://github.com/example/repo/archive/v1.0.1.tar.gz";
  }
  ```

- pkgs.testers.overrideDerivation : 覆蓋derivation
  ```
  pkgs.testers.overrideDerivation myPackage {
    version = "1.0.1";
    src = fetchTarball "https://github.com/example/repo/archive/v1.0.1.tar.gz";
  }
  ```

## ref

- [不透過測試框架進行測試](https://blakesmith.me/2024/03/02/running-nixos-tests-with-flakes.html)

- [測試概述](https://nixcademy.com/posts/nixos-integration-tests/)

- pkgs.testers 使用文檔
  - [源碼](https://github.com/NixOS/nixpkgs/tree/master/pkgs/build-support/testers)
  - [官方完整版，含testscript屬性用法 @ nixpkgs](https://nixos.org/manual/nixos/stable/#ssec-machine-objects)
  - [官方 @ nixpkgs](https://nixos.org/manual/nixpkgs/unstable/#chap-testers)
  - [官方好讀+精簡版](https://ryantm.github.io/nixpkgs/builders/testers/#chap-testers)

- pkgs.testers.runNixOSTest()
  - [introduce : Integration testing with NixOS virtual machines](https://nix.dev/tutorials/nixos/integration-testing-using-virtual-machines.html)

- pkgs.nixosTest()
  - [NixOS Testing library examples](https://nixos.wiki/wiki/NixOS_Testing_library)

- pkgs.testers使用範例
  - [透過 pkg.runCommand 手動測試](https://www.youtube.com/live/aLy8id4wr-M?si=kz3LjF3bW7lBigMM&t=2456)  
  - [透過 passthru.tests 測試框架進行測試](https://www.youtube.com/live/aLy8id4wr-M?si=wGmLtyRnHN6XXghT&t=2640)
  - [各種使用場景demo](https://nixcademy.com/posts/nixos-integration-tests/)
  - [nixpkgs官方用於測試套件的1000個測試源碼](https://github.com/NixOS/nixpkgs/tree/master/nixos/tests)
  - [影片，pkgs.testers + interaction-shell](https://www.youtube.com/watch?v=RgKl8Jue4qM)

- 對 home-manager 進行 nixos 測試
  - [nixos test for a custom nixos home-manager module](https://discourse.nixos.org/t/run-nixos-interactive-tests-on-aarch64-darwin/34534)

  - [home-manager example](https://nixos.wiki/wiki/NixOS_Testing_library)