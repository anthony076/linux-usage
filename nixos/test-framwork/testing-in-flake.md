## testing with flake

- 透過 flake.nix 進行 nixos 測試
  - [NixOS測試](pkgs.testers.md)

  - 概念
    - 是否是NixOS測試，`由使用的函數決定`，而不是使用的屬性決定

    - NixOS測試需要搭配`pkgs.testers.nixosTest函數`或`pkgs.testers.runNixOSTest函數`使用，
      才能進行在虛擬機中執行的NixOS測試

    - 使用`checks屬性`或是`packages屬性`或是`自定義屬性`都可以進行NixOS測試，差別在`執行的命令`不同

  - `checks屬性`
    - 常用於一般性測試，例如，建構測試、語法測試、命令測試等，但也可以執行nixos測試
    - 支援`$ nix flake check`命令，執行時，會執行checks屬性下定義的所有測試，無法指定執行的測試
    - 詳見，[check屬性的使用](#check屬性的使用)

  - `自定義屬性`
    - 不支援`$ nix flake check`命令，執行此命令預設不會自動運行自定義屬性中的測試
    - 需要通過`$ nix build .#自定義屬性名.測試名`來手動執行測試

  - `packages屬性`
    - 不支援`$ nix flake check`命令，執行此命令預設不會自動運行packages屬性中的測試
    - 需要通過`$ nix build`手動執行測試
    - 詳見，[利用flake進行nixos配置的測試](#利用flake進行nixos配置的測試)

## interaction-mode

- 進入 interaction mode，`$ nix build .#default.driverInteractive`

## check屬性的使用

- 語法，`checks.架構.測試名 = derivation`，例如，`$ checks.x86_64-linux.my-check`

- 範例，[checks屬性 + pkgs.runCommand，自訂測試命令](test-examples/flake-check-by-runCommand.nix)
- 範例，[checks屬性 + pkgs.stdenv.mkDerivation，建構測試](test-examples/flake-check-by-mkDerivation.nix)
- 範例，[checks屬性 + pkgs.testers.runNixOSTest，NixOS測試](test-examples/runNixOSTest-flake-version.nix)

## 利用flake進行NixOS配置的測試

- 範例，使用官方提供的範例

  ```
  git clone --depth 1 https://github.com/NixOS/nixpkgs/tree/master
  cd nixpkgs
  nix run '.#nixosTests.nginx.driver' 或 nix run '.#nixosTests.nginx.driverInteractive'
  ```

- 範例，[checks屬性 + pkgs.testers.runNixOSTest，NixOS測試](test-examples/runNixOSTest-flake-check.nix)
  
  - 測試方法，自動執行 checks屬性下所有測試，`$ nix flake check`，此命令隱藏執行過程打印的訊息

  - 測試方法，打印執行過程產生的訊息，
    - `$ nix build '.#checks.x86_64-linux.my-check.driver --print-out-paths'`
    - `$ result/bin/nixos-test-driver'`
    - 其中，.driver 表示建構並保留runNixOSTest產生的test-driver
    - 或直接執行，`$ nix run '.#checks.x86_64-linux.my-check.driver'`

  - 測試方法，建立 interactive-shell，
    - `$ nix build '.#checks.x86_64-linux.my-check.driverInteractive --print-out-paths'`
    - `$ result/bin/nixos-test-driver'`
    - 或直接執行，`$ nix run '.#checks.x86_64-linux.my-check.driverInteractive'`

- 範例，[packages屬性 + pkgs.testers.runNixOSTest，NixOS測試](test-examples/runNixOSTest-flake-packages.nix)

- 完整範例，[my nix-config with NixOSTest](https://gitlab.com/anthony076/nix-config)

- 檢視 log
  - `$ ls -al result`，查看在 /nix/store 中的路徑，例如，`result -> /nix/store/<hash>-vm-test-run-hello-test/`
  - `$ nix log /nix/store/<hash>-vm-test-run-hello-test/`，檢視 log

## ref 

- [Unveiling the Power of the NixOS Integration Test Driver](https://nixcademy.com/posts/nixos-integration-tests-part-2/)

- [Running NixOS Tests with Nix Flakes](https://blakesmith.me/2024/03/02/running-nixos-tests-with-flakes.html)

- [How to Run NixOS Tests (Flake Edition)](https://blakesmith.me/2024/03/11/how-to-run-nixos-tests-flake-edition.html)

- interaction-mode
  - [interaction-mode的用法](https://nixcademy.com/posts/nixos-integration-tests-part-2/)

