## [函數] nios配置文件測試，pkgs.testers.runNixOSTest 的使用

- 使用場景 : 主要用於配置檔 configuration.nix 的測試，runNixOSTest() 會建立qemu-vm，
  並透過 python-shell 與 qemu-vm 進行互動或執行測試

- 基本概念
  - 在 vm 中不會有 /etc/nixos/configuration.nix

    configuration.nix的內容是由 pkgs.testers.runNixOSTest 的 `nodes.<主機名> 屬性`決定，
    若要在VM中修改configuration.nix的配置，可以進入VM後手動建立(不確定是否有效，待確認)
  
  - 在 pkgs.testers.runNixOSTest 中不需要加載 /etc/nixos/hardware-configuration.nix

    參考，[配置文件中不加載hardware-configuration.nix](../vm-container-iso/build-qemu-nixos-independ/3rd_nixos-generators_usage.md#配置文件中不加載hardware-configurationnix)
  
  - runNixOSTest函數中所有 import 語句的檔案，不需要複製到 vm 中

    在runNixOSTest函數中所有的 import 語句，會`在評估期間被解析`並`替換成實際內容`，
    因此，具有 import 語句的檔案，不需要複製到 vm 中，但是建立VM後，日常使用時需要的額外檔案需要複製到VM中

  - vm 啟動後，預設的目錄為 /tmp
  
  - 進入vm後無法切換shell，`或其他執行後會進入互動模式的程式`，因為得不到使用者的回應

- `pkgs.testers.nixosTest()` vs `pkgs.nixosTest()` vs `pkgs.testers.runNixOSTest()`
  - pkgs.testers 是測試框架，提供多種測試函數進行不同場景需求下的測試

  - pkgs.testers.nixosTest()，專用用於 nixos配置文件的測試
    - 更接近 NixOS 本身的測試環境
    - 可以更好地模擬真實的 NixOS 設置
    - 需要額外配置，使用上較複雜

  - pkgs.nixosTest()，基於 pkgs.testers 框架
    - pkgs.nixosTest()，將 pkgs.testers.nixosTest() 中的pkgs設置於當前環境中的pkgs
    - 若使用 pkgs.testers.nixosTest()，需要手動配置和改寫pkgs
    - 優，使用 pkgs.nixosTest() 不需要指定 pkgs，也就不需要重新引入 nixos，配置和使用較簡單
    - 缺，不會使用 NixOS 的完整配置，可能缺少某些功能或環境
  
  - pkgs.testers.runNixOSTest()
    - 會自動調用 nixos-lib.runTest() 執行測試，因此不需要手動取得 nixos-lib
    - pkgs.testers.runNixOSTest() 在建立系統配置上更為靈活

## runNixOSTest執行過程

- 在`nodes.<主機名>屬性`中，建立nixos主機的配置，testers 會根據配置建立對應的虛擬nixos主機
  
- 在`testScript屬性`中，建立測試語句

- 測試執行時，利用 `qemu 啟動虛擬的nixos主機`用於測試配置文件的內容是否正確，

- 測試執行時，利用 `python-shell` 與qemu中的虛擬的nixos主機進行交互和測試，
  - [python-test-driver架構](https://nixcademy.com/posts/nixos-integration-tests/)
  - [python-shell源碼，testing-python.nix](https://github.com/NixOS/nixpkgs/blob/master/nixos/lib/testing-python.nix)

- 透過`nix-build default.nix`完成qemu-vm的建構並執行測試

- 優，在任何類型的linux主機上，就可以透過虛擬機測試nixos的配置

## runNixOSTest函數可用的子屬性

- `name屬性`，自定義測試名

- `nodes屬性`，用於定義主機，可定義多台主機
  - 每一台主機實際上都會透過qemu建立一台nixos主機的實例
  - `<自定義主機名>屬性`，用於定義主機的配置文件內容，`等效於 configuration.nix`，
    需填寫和 configuration.nix 一樣的內容，都是函數形式

- `skipLint屬性`，停用程式碼檢查器，用於加速測試，可用值 true|false

- `skipTypeCheck屬性`，跳過類型檢查，用於加速測試，可用值 true|false

- `testScript屬性`，
  - 用於執行測試的`python腳本`，會由`testing-python.nix`定義的python環境執行
  - 可接受函數形式，並傳入nodes屬性中定義的主機，可在腳本中以主機名直接調用測試函數
  - 常用的函數
    - `machine.wait_for_unit("default.target")`，等待vm達到default.target後再繼續執行，用於確保其已完全啟動
    - `machine.succeed(命令)`，測試命令是否正常執行，若否，會拋出錯誤
    - `machine.fail(命令)`，測試命令是否正常失敗，若否，會拋出錯誤
    - `machine.shell_interact()`，進入vm的shell中

  - 範例
    ```
    testScript = ''
      machine.wait_for_unit("default.target")
      machine.succeed("su -- alice -c 'which firefox'")
      machine.fail("su -- root -c 'which firefox'")

      # 檢測輸出是否正確
      assert "Hello World!" in machine.succeed(
          "./HelloWorld"
      ), "HelloWorld does not run properly"
      
    '';
    ```

- `interactive屬性`，設置只有在互動模式下才生效的配置

  - 用途 : 在interactive屬性下的配置，在正常的自動化測試中不會生效，可以專門為手動調試提供方便
  - 語法 : `interactive.node.主機名 = {配置}`
  - 限制 : 使用`interactive.node.主機名 = {配置}`前，確保`node.主機名 = {配置}`已經建立，不能只有`interactive.node.主機名 = {配置}`
  - 文檔 : [interactive屬性的使用](https://nixos.org/manual/nixos/stable/#test-opt-interactive)
  - 範例
    - [benchexec套件的測試](https://github.com/NixOS/nixpkgs/blob/master/nixos/tests/benchexec.nix)
    - [soundcard套件的測試](https://github.com/NixOS/nixpkgs/blob/fcf7e79c02e9ef252ce4110f4313c7b516f1ed98/pkgs/development/python-modules/soundcard/test.nix)

## 加速建構VM過程和其他推薦的配置

- `imports = [ <nixpkgs/nixos/modules/profiles/minimal.nix> ];`，使用 minimal NixOS 配置

- `networking.useDHCP = false;`，關閉 DHCP

- `services.openssh.enable = false;`，關閉 OpenSSH 服務

- `services.xserver.enable = false;`，關閉 xerver 服務

- `virtualisation.memorySize = 512;`，減少內存大小，減少qemu處理資源的分配，以加速VM創建過程

- `defaults.networking.firewall.enable = false;`，禁用防火牆

- 禁用qemu硬體
  ```
  virtualisation.qemu.options = [
    "-nographic"                     # 禁用圖形介面
    "-device" "qemu-xhci"            # 可選：使用 USB 控制器
    "-device" "usb-kbd"              # 添加 USB 鍵盤
    "-nodefaults"                    # 禁用所有默認設備
  ];
  ```
- `boot.kernelParams = [ "noapic" ];`，
  避免出現`Kernel panic - not syncing: IO-APIC + timer doesn't work!`的問題

- 範例，[簡易範例](test-examples/runNixOSTest-simple.nix)
- 範例，[加速vm的建構，更快完成測試](test-examples/runNixOSTest-simple.nix)

## 執行 runNixOSTest

- 兩種方法執行 NixOSTest
  - 方法，runNixOSTest 寫在`default.nix`檔案中，
  
    - Option，執行，`$ nix-build`或`$ nix-build .`或`$ nix-build default.nix`或`$nix-build --check`，
      - 此命令建構runNixOSTest中的 test-driver，並執行 testScript 的內容，執行完畢後會自動清理VM，
        建構完成後，需要透過 result/bin/nixos-test-driver 重新啟動VM並執行 testScript 的內容

      - 執行過一次nix-build之後，`若default.nix沒有任何變更`，再次執行nix-build就不會執行，
        需要改用`$nix-build --check`使nix-build強制再次執行

    - Option，執行，`$ nix-build -A driverInteractive`
      - 此命令建構runNixOSTest中的 test-driver，但不會執行 testScript 的內容，
        建構完成後，需要透過 result/bin/nixos-test-driver 重新啟動VM後直接進入 interactive-shell

      - interactive-shell，參考以下，[使用交互模式進行測試](#使用交互模式進行測試-interactive-shell)
  
  - 方法，runNixOSTest 寫在`flake.nix`檔案中，參考，[testing in flake.nix](testing-in-flake.md)

- 刪除 result 以重新執行測試

  在測試文件(例如，default.nix)沒有變更的狀況下，nix-build命令不會重新執行，因此無法重新執行測試

  需要手動將前一次構建的產物刪除，`$ nix-store --delete ./result`，
  此命令會透過 ./result 找到保存在 nix-store 的 derivation，並將之刪除

  注意，執行 `$ rm -rf ./result` 只會將 result 的軟連節刪除，保存在nix-store中的derivation不會被刪除

## 使用交互模式進行測試 (interactive-shell)

- 比較，交互模式測試(`$ nix-build -A driverInteractive`)和快速測試(`$ nix-build`)的差異

  快速測試(`$ nix-build`)會自動測試，交互模式測試(`$ nix-build -A driverInteractive`)不會自動測試

  |                      | 建構vm | 產出啟動vm的script | 自動執行 TestScript | 使用場景                       |
  | -------------------- | ------ | ------------------ | ------------------- | ------------------------------ |
  | 快速測試($nix-build) | V      | V                  | V                   | 有限環境的測試，用於單純的測試 |
  | 交互模式測試         | V      | V                  | X                   | 提供更完整的測試環境           |
  
  - 快速測試(`$ nix-build`)完成後，可以透過`$ nix log /nix/store/<hash>-vm-test-run-nixos-configuration-test.drv`查看建構紀錄

  - 大部分測試可以快速測試中直接執行，但少部分的測試只能在互動模式下完成，例如，透過 sharedDirectories 分享目錄

  - 交互模式測試需要透過`$ result/bin/nixos-test-driver`重新進入該測試環境

- step，建構啟動VM的Script
  - 將原本的`$ nix-build default.nix`改成`$ nix-build -A driverInteractive default.nix`，
  - driverInteractive 是內建屬性，添加後才會建構`result/bin/nixos-test-driver`的檔案
  - `$ nix-build default.nix`會建構VM的啟動腳本並執行該腳本，自動執行測試，執行完畢後自動刪除VM，所以不會有任何檔案輸出
  - `$ nix-build -A driverInteractive default.nix`會建構VM的啟動腳本但不執行，需要手動啟動VM + 手動執行測試，
    為了能夠手動執行，因此會有`nixos-test-driver`檔案的輸出，nixos-test-driver就適用於執行qemu-vm的命令
  - 查看 nixos-test-driver 腳本的內容，`$ cat result/bin/nixos-test-driver`

- step，進入交互
  
  建構後完成後，需要手動執行`$ result/bin/nixos-test-driver`，
  以進入python建立的interaction-shell中，此shell用於和qemu-vm進行交互

  `$ nix-build -A driverInteractive default.nix`只用於快速建立 nixos-test-driver 的檔案，
  不會實際執行VM，也不會執行任何測試，需要手動執行`$ result/bin/nixos-test-driver` 才會啟動VM，
  並手動進行測試或相關操作

  [driverInteractive 屬性的源碼](https://github.com/NixOS/nixpkgs/blob/master/nixos/lib/testing/interactive.nix#L43)

  或使用`$ (nix-build -A driverInteractive minimal-test.nix)/bin/nixos-test-driver`，
  執行建構並直接進入interaction-shell

  nixos-test-driver命令其他可用參數，可透過`$ result/bin/nixos-test-driver --help`，例如，
  - `--keep-vm-state`，可用於保留VM狀態

- interactive-shell 下常用方法
  - [官方文檔+machine下其他函數的使用](https://nixos.org/manual/nixos/stable/#ssec-machine-objects)

  - `start_all()` 啟動所有的vm

    若 start_all() 之後有調用 machine 的相關操作，也會自動調用 machine.start()，
    因此，若有調用 machine 的相關操作，start_all()可以省略

  - `machine.start()`，啟動machine對應的vm

  - `machine.shared_dir`，顯示本機的共享目錄

    用於 machine.copy_from_host(source, target) 和 copy_from_host(source, target)，
    此兩者使用時，會使用 machine.shared_dir 指向的目錄作為中轉目錄

     在 interactive-shell 中，可透過 machine.shared_dir 查看預設本機共享目錄的實際路徑，`machine.shared_dir=/tmp/shared-xchg`

  - `machine.shell_interact()`，進入vm的shell中

    進入vm-shell後，預設的路徑為 /tmp
    
    <font color=blue>注意，若vm-shell進入後，就無法再進入</font>
    
    若透過 machine.shell_interact() 進入VM的shell後(vm-shell)，
  
    一旦從vm-shell退出後，<font color=red>vm-shell就會自動關閉</font>，之後再執行 machine.execute()、machine.succeed() 等，會使用到shell的命令就無法再執行

    <font color=blue>推薦使用 machine.execute('命令') 執行vm-shell命令，避免透過 machine.shell_interact() 直接進入 vm-shell</font>

  - `machine.execute()`，不用進入vm-shell，在interaction-shell中讓VM執行命令

  - `machine.log()`，與machine.execute()搭配使用，可在VM執行期間進行debug，例如，

    ```
    _, pwd = machine.execute('pwd'); 
    machine.log(f"[pwd] = {pwd}")，
    ```

  - `machine.succeed()`，執行指令，用於驗證VM執行的指令是否正確執行

    此函數預設VM執行的指令會成功，
    若指令返回非0的狀態碼，則 machine.succeed() 會拋出錯誤
    
    ```
    # 驗證指令是否被執行
    machine.succeed("uname")

    # 驗證指令返回的內容是否正確
    if not "Linux" in machine.succeed("uname"):
      raise Exception("Wrong OS")
    ```

  - `machine.fail()`，執行指令，用於驗證VM執行的指令是否發生錯誤

    和 machine.fail() 類似，但預設VM執行的指令會發生錯誤，
    因此，若指令返回0的狀態碼，表示成功執行，與預期相反，則 machine.succeed() 會拋出錯誤

  - 注意，每次執行命令都是獨立的

    <font color=blue>注意，每一個 machine.execute() 都由獨立的vm-shell執行</font>

    每次執行 execute() 都會在虛擬機內啟動一個新的 Shell，
    因此，透過`execute('export AA=123')`執行 export 命令所設置的環境變數，
    無法在後續的 machine.execute() 調用中保留

    可以改用`machine.execute('export JUST_JUSTFILE=/root/.justfile && echo $JUST_JUSTFILE')`，
    
    或者使用多行命令
    ```
    machine.execute('''
     export JUST_JUSTFILE=/root/.justfile
     echo $JUST_JUSTFILEi
    ''')
    ```

    或者使用 with 語句，共享同一個 session
    ```
    with subtest("create user"):
        machine.succeed("useradd -m alice")
        machine.succeed("(echo foobar; echo foobar) | passwd alice")
    ```

    或者將 machine.succeed 拆分成多個命令
    ```
    machine.succeed(
      "mkapfs /dev/vdb",
      "mount -o cknodes,readwrite /dev/vdb /tmp/mnt",
      "echo 'bla bla bla' > /tmp/mnt/Test.txt",
    )
    ```

## 透過 virtualisation.sharedDirectories 共享目錄

- 注意，此方法<font color=blue>僅在<font color=red>nix-build -d driverInteractive</font>中可用</font>

  - nix-build 執行過程是完全隔離的，不允許直接訪問外部目錄，因此共享目錄在 nix-build 執行過程中無法正常掛載進入VM中
  - 透過 result/bin/driverInteractive 來啟動 VM 則提供更完整的功能，使virtualisation.sharedDirectories有效

- 在配置中透過`virtualisation.sharedDirectories.<mount-tag>屬性`可設置自定義的共享目錄，
  
  ```
  virtualisation.sharedDirectories.test = {
    source = "/tmp/shared-xchg";
    target = "/mnt/shared";
  };
  ```

- 預設本機共享目錄
  
  `virtualisation.sharedDirectories.<mount-tag>` 除了掛載用戶指定的共享目錄外，
  
  預設會將本機/tmp/shared-xchg目錄，自動掛載到VM的/tmp/shared路徑中，
  無論是否使用`virtualisation.sharedDirectories.<mount-tag>`

  在 interactive-shell 中，可透過 machine.shared_dir 查看預設本機共享目錄的實際路徑，`machine.shared_dir=/tmp/shared-xchg`

  若將預設的/tmp/shared-xchg目錄透過`virtualisation.sharedDirectories.<mount-tag>`掛載到指定的VM路徑，例如，VM的/mnt/shared，
  則 /tmp/shared-xchg 的內容會同時出現在VM的 /tmp/shared 和 /mnt/shared 

- 限制
  - 本機共享目錄`必須使用絕對路徑`
  - 本機共享目錄若指定為 /root 下的路徑，可能會有`權限問題`而無法掛載

- 範例，[利用sharedDirectories共享目錄](test-examples/runNixOSTest-share-by-sharedDirectories.nix)

- virtualisation.sharedDirectories源碼，https://github.com/NixOS/nixpkgs/blob/nixos-24.05/nixos/modules/virtualisation/qemu-vm.nix

## 透過 nfs 共享目錄

- 使用場景
  
  考慮在 host 進行 runNixOSTest 時，透過 nfs 讓nixos測試過程中建立的 vm，
  可以透過 nfs 直接存取 host 的檔案

- server 相關
  - nfs-server 依賴的進程
    - nfsd (nfsv3、nfsv4) : 負責處理來自客戶端的文件系統請求
    - rpcbind (nfsv3、nfsv4) : 將客戶端的 RPC 請求映射到合適的服務端口
    - rpc.statd | rpc.lockd (nfsv3、nfsv4) 提供狀態監控，跟蹤 NFS 客戶端和服務器之間的文件鎖狀態
    - rpc.mountd (nfsv3) : 處理客戶端的掛載請求，負責檢查 /etc/exports 文件中的共享配置
    - rpc.idmapd (nfsv4) :　NFSv4 使用用戶和組名稱來進行權限控制，
      因此 rpc.idmapd 確保不同主機之間的用戶權限一致
  
  - 目錄，`/var/lib/nfs`，儲存nfs-server需要的`狀態文件`和`鎖文件`的目錄，
    用於跟蹤客戶端的狀態和訪問權限，以保證文件的並發訪問的一致性

    - 鎖文件(lock-files) : sm 和 sm.bak 子目錄
    - 狀態文件(stat-files) :
      - etab：記錄哪些目錄被導出給client
      - rmtab：記錄client掛載的遠程信息

  - 文件，`/etc/exports`，nfs 的配置文件

  - `/etc/exports`可用的掛載選項
    - 格式，`/要共享的目錄 允許存取的IP(掛載選項)`
    - `rw` : 允許用戶讀寫
    - `ro` : 只允許用戶讀取，不允許寫入
    - `sync` : 同步進行變更
    - `insecure` : server 允許高於用戶使用高於1024的port
    - `nohide` : 
    - `no_root_squash` : 不壓縮用戶權限，允許客戶端使用 root 權限存取

      注意，若使用此選項，當用戶使用 root 權限發出請求時，nfs-server會保留用戶的權限，
      依舊使用 root 向檔案系統發出讀寫請求

      但是否能夠存取共享目錄，依舊由共享目錄的使用權限決定，以上為例，
      若共享目錄的使用權限沒有root，用戶透過 root 依舊無法存取共享目錄

    - `no_subtree_check` : 禁用子目錄檢查，可加快存取並減少檢查開銷

  - 透過`services.nfs.server.enable屬性`，添加 nfs-server 相關配置，

    完整源碼，詳見，[nfsd.nix](https://github.com/NixOS/nixpkgs/blob/nixos-24.05/nixos/modules/services/network-filesystems/nfsd.nix)
    
    ```
    # 啟用 rpcbind-service
    services.rpcbind.enable = true;

    # 添加 nfs 檔案系統需要的配置和環境，包含添加 nfs 內核模組
    boot.supportedFilesystems = [ "nfs" ];

    # 建立 /etc/exports 檔案
    environment.etc.exports.source = exports;

    # 建立 nfs-server 服務
    systemd.services.nfs-server = ... ;

    # 建立 nfs-mountd 服務
    systemd.services.nfs-mountd = ... ;
    ```

  - 透過`services.nfs.server.exports屬性`，建立/etc/exports配置文件

    注意，建議在`services.nfs.server.exports屬性`中添加 insecure ，可以避免nfs-client掛載被拒
  
  - `services.nfs.server.createMountPoints屬性`提早檢查並建立掛載點，
    避免nfs-server.service啟動錯誤

  - 注意，`$ systemctl status nfs-server` 顯示 `Active: active (exited)`是正常的，不是啟動錯誤

    nfs-server.service 是單次執行，執行後就會自動脫離，因此顯示 Active: active (exited)
    
    透過 `$ cat /etc/systemd/system/nfs-server.service` 可以找到 
    - Type=oneshot 代表一次性，
    - RemainAfterExit=yes 代表離開後依舊能執行

- client 相關
  - nfs-client 用戶端不需要安裝任何套件

  - 必須透過`virtualisation.fileSystems屬性`在qemu-client-vm中掛載共享目錄

    但需要透過 virtualisation.fileSystems 屬性才會自動安裝和啟用 rpc-statd 服務，
    包含手動調用`mount -t nfs ...`也都需要 rpc-statd 服務

    若不使用`virtualisation.fileSystems屬性`，手動建立目錄後，手動調用
    `mount -t nfs ...`就會出現問題

    語法
    virtualisation.fileSystems."本地路徑" = {
      device = "遠端主機IP:遠端路徑";
      fsType = "nfs";
    };

- 常用命令
  - server 相關命令
    - `$ systemctl status nfs-server`，檢查 nfs-server.service 是否啟動

      nfs-server.service 位於 /etc/systemd/system/nfs-server.service

    - `$ cat /etc/exports`，用於檢查nfs-server 配置檔的內容

    - `$ exportfs -v`，列出服務器當前配置的所有導出目錄及其訪問權限，該訊息直接來自 /etc/exports

    - `$ journalctl | grep rpc`，查看用戶連線紀錄，若nfs-client連線失敗，透過此命令可以查看失敗訊息

    - `$ nfsstat -s`，查看 server端統計資料
    
    - `$ rpcinfo -p <nfs-server-ip> | grep nfs`，透過 rpc 查看 nfs-server 使用的 port 

  - client 相關命令
    - `$ showmount -e` 或 `$ showmount -e <IP>`
      - 此命令由 nfs-utils 提供
      - 此命令會向服務器端的 rpc.mountd 服務發出請求，取得並顯示 NFS-server 導出的共享目錄列表
      - 用於客戶端驗證和診斷，確認服務器是否正確導出目錄

    - `$ mount -t nfs  10.0.2.2:/data /data`，手動掛載nfs提供的共享目錄
      - 在 client 上手動掛載 nfs-server 提供的共享目錄
      - 若 client 上掛載失敗，透過手動掛載可以提供更多的訊息

    - `$ nfsstat -c`，查看 server端統計資料，需要安裝 nfs-utils

- [issue] nfs-server.service 總是顯示 Active: active (exited)

  在 nixos 中，nfs-server.service 是運行後自動退出的類型，實際上仍然可以正常運作

  透過`cat /etc/systemd/system/nfs-server.service`可以找到 nfs-server.service具有以下的內容
  - Type=oneshot 代表一次性，
  - RemainAfterExit=yes 代表離開後依舊能執行

  因此，`$ systemctl status nfs-server`中總是顯示 Active: active (exited) 是正常行為

- 範例集

  - 範例，[dual-vm](test-examples/runNixOSTest-nfs-dualVMs/default.nix)

  - 範例，[host-server + vm-client](test-examples/runNixOSTest-nfs-hostServer-vmClient/readme.md)

  - 範例，[加密的nfs](https://github.com/NixOS/nixpkgs/blob/master/nixos/tests/nfs/kerberos.nix)

  - 範例，指定防火牆的nfs

    [from](https://nixos.wiki/wiki/NFS)
    ```
    services.nfs.server = {
      enable = true;
      # fixed rpc.statd port; for firewall
      lockdPort = 4001;
      mountdPort = 4002;
      statdPort = 4000;
      extraNfsdConfig = '''';
    };
    networking.firewall = {
      enable = true;
        # for NFSv3; view with rpcinfo -p
      allowedTCPPorts = [ 111  2049 4000 4001 4002 20048 ];
      allowedUDPPorts = [ 111 2049 4000 4001  4002 20048 ];
    };
    ```

  - 範例，限制使用 nfsv4

    server 端
    ```
    services.nfs.server.extraNfsdConfig = ''
      UDP=off
      vers2=off
      vers3=off
    '';
    ```

    client 端
    ```
    fileSystems.共享目錄 = { 
      device = "/mnt/${discDir}";
      options = [ "bind" ];
      fsType = "nfs4";
    };
    ```

## flake 版本的 pkgs.testers.runNixOSTest

參考，[testing in flake](testing-in-flake.md)

## 測試 home-manager 配置

- 基本概念
  - home-manager 的配置文件，home.nix，有多種使用方式，但`只有模塊化利於測試nixos測試`
    
    - 參考，[home-manager的使用](../home-manerger/00_hm-overview.md)

    - 方法，`獨立化` : 不推薦使用
      - 條件，`home-manager-cli + home.nix`，
      - 需要額外安裝home-manager-cli
      - 使用 pkgs.testers.runNixOSTest() 時，
        `nodes.<自定義主機名>屬性`需要指定configuration.nix的內容，而不是home.nix的內容
      - 基於上述兩點，此方法不適用於nixos測試

    - 方法，`模塊化` : 推薦使用
      - 條件，`nixos-rebuild命令 + configuration.nix`，
      - 因為nixos-rebuild命令是內建命令，不需要額外安裝，利於測試
      - 在 configuration.nix中，透過 home-manager-module 加載 home.nix，利於測試
    
    - 方法，`flake.nix + 模塊化` : 推薦使用
      - 條件，`configuration.nix + flake.nix + home.nix`，
        需要在 flake.nix 中加載 configuration.nix 和 home.nix
      - 此方法中，configuration.nix 和 home.nix 會被調用兩次
        - 第1次，用於配置系統
        - 第2次，在pkgs.testers.runNixOSTest()被調用，用於測試 

- 範例，[default.nix + home-manager](test-examples/runNixOSTest-default-hm-simple.nix)

- 範例，[default.nix + home-manager + import 語句](test-examples/runNixOSTest-default-hm-modulize-complex/default.nix)

- 範例，[flake.nix + home-manager](test-examples/runNixOSTest-flake-hm-simple.nix)

- 範例，[flake.nix + home-manager，模組化，透過 home-manager.nixosModules.home-manager屬性](test-examples/runNixOSTest-flake-hm-modulize-nixosModules/) 

  此範例在 flake.nix 中`僅加載configuration.nix`檔案的內容，
  而 home-manager 的配置(home.nix)的內容不在flake.nix中加載，而是在configuration.nix中加載

- 範例，[推薦，flake.nix + home-manager，模組化，透過 home-manager.nixosModules.home-manager屬性](test-examples/runNixOSTest-flake-hm-modulize-clear/)

  此範例將home.nix和configuration.nix的內容都在flake.nix中完成加載，
  在最上層的 flake.nix 直接看到加載哪些檔案，對代碼理解較有幫助，推薦使用

## [issue] 在交互模式中執行start_all()後， 沒有正確地創建qemu-vm

在交互模式中執行start_all()後，正常狀況下，本機qemu-nixos-vm會在虛擬的nixos中，創建一個新的用於測試配置的qemu-vm，

若本機qemu-nixos-vm在啟動時，沒有添加`-accel whpx,kernel-irqchip=off`的啟動參數，
在創建測試配置的qemu-vm時就會失敗，而導致本機qemu-nixos-vm重啟

若使用 ssh 連接qemu-nixos-vm，就會看到執行過程卡在`mke2fs 1.47.0`的步驟一段時間後，ssh的連線就會自動中斷，
若直接在 qemu-nixos-vm 的視窗中就可以觀察到，ssh的中斷是由於本機qemu-nixos-vm因錯誤而被重啟所致

因此，本機qemu-nixos-vm在啟動時，必需添加`-accel whpx,kernel-irqchip=off`的啟動參數

其他推薦的啟動參數，參考，[qemu的啟動配置](../qemu_run_nixos.bat)

## ref

- [runNixOSTest官方文檔 + runNixOSTest 可用選項](https://nixos.org/manual/nixos/stable/#sec-test-options-reference)

- nfs
  - 官方文檔，[nfs的使用](https://nixos.wiki/wiki/NFS)
  - [nfs配置源碼](https://github.com/NixOS/nixpkgs/blob/nixos-24.05/nixos/modules/services/network-filesystems/nfsd.nix)
  - [NFS 伺服器@鳥哥](https://linux.vbird.org/linux_server/centos6/0330nfs.php)
  - [nfs-utils的使用](https://wiki.gentoo.org/wiki/Nfs-utils)