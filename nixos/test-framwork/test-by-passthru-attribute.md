## passthru 屬性的使用

- passthru 屬性的功能
  - 用於在`derivation`中，傳遞`額外自定義且非主要的訊息或函數`給外部，而`不影響構建`過程
  - 用於套件建構時，passthru屬性不會影響實際的包構建
  
- passthru 屬性的使用場景
  - 用於測試 : 利用 passthru 屬性添加測試函數或調用訊息，若有多個 tests 子項目，可一次執行多個測試，
  - 用於提供套件訊息給外部 : 利用 passthru 屬性添加套件的相關工具、依賴版本、文檔路徑等額外訊息
  - 用於與其他套件交互 : 可以方便地在模組間共享一些函數或數據，不影響主包的依賴

## 支援 passthru 屬性的函數

- 注意，不支援 passthru 屬性的函數也可以透過 derivation.overrideAttrs 添加 passthru屬性

- 可以在 nix repl 中察看是否支援 passthru 屬性，例如，
  
  ```
  # default.nix

  let 
    pkgs = import <nixpkgs> {};

    # pkgs.runCommand "命令名稱" {屬性集} ''要執行的命令內容'''
    
    result = pkgs.runCommand "test" {} ''
      mkdir -p $out
      echo "hello" > $out/hello.txt
    '';
  in 
    result

  # nix repl
  # nix-repl> m = import ./default.nix
  # 查看是否有 m.passthru 的屬性
  ```

- 支援 passthru 屬性的函數
  - pkgs.stdenv.mkDerivation 
  - pkgs.python3Packages.buildPythonPackage
  - buildRustPackage
  - buildGoModule
  - buildDotnetPackage
  - callPackage
  - pkgs.runCommand

- 不支援 passthru 屬性的函數
  - pkgs.writeScriptBin

## 比較

- 比較，`passthru屬性` VS `自定義屬性`的差異
  - 不會影響建構過程
    - passthru 屬性可以將額外的訊息附加到 derivation，而不會影響其構建過程
    - 自定義屬性可能會被誤解為會影響 derivation 的行為

  - 集中管理
    - passthru 屬性可以將要修改或擴展 derivation 的屬性進行集中管理 (集中在passthru 屬性下)
    - 自定義屬性可能會造成屬性分散和混亂
  
  - 支援度更高 : Nix 工具和函數對passthru屬性的支援更加廣泛
  - 更容易區分 : 讓使用者更好區分出derivation本身的屬性或是附加的屬性

- 比較，`passthru屬性`VS`pkgs.stdenv.mkDerivation的checkPhase屬性`
  - checkPhase 是構建過程的一部分，passthru屬性不是，
    passthru屬性中的函數失敗，並不會影響包的構建結果
  
  - checkPhase 是構建過程的一部分，會自動執行，
    passthru屬性中定義的屬性需要手動執行，且不會影響建構過程

## 範例集

- 範例，[利用passthru屬性暴露訊息的範例](test-examples/passthru-expose-info.nix)
  - 對不支援passthru屬性的函數，需要透過 overrideAttrs 來添加
  - passthru屬性和自定義屬性具有類似的屬性，但透過passthru屬性時存取時，`passthru屬性名可以省略`，
    但自定義屬性則無法省略，例如，以下兩種方式是等價的
    - myhello.passthru.helloPath
    - myhello.helloPath

- 範例，[利用 passthru 屬性添加測試函數並調用 by mkDerivation](test-examples/passthru-test-derivation.nix)
- 範例，[利用 passthru 屬性添加測試函數並調用 by runCommand](test-examples/passthru-test-runCommand.nix)

- 範例，[綜合使用範例](test-examples/passthru-mix.nix)

- 範例，可一次執行多個測試

  透過`$ nix-build -A passthru.tests`，依次可執行以下兩個測試

  ```
  let 
    pkgs = import <nixpkgs> {};

    package = pkgs.runCommand "foo" {
      # 測試1
      passthru.tests.version = pkgs.testers.testVersion {
        package = package;
        version = "1.2";
      };

      # 測試2
      passthru.tests.my = pkgs.testers.testVersion {
        package = package;
        version = "1.3";
      };

      script = ''
        echo "1.2"
      '';

      passAsFile = [ "script" ];
    } ''
      mkdir -p "$out/bin"
      cp "$scriptPath" "$out/bin/foo"
      chmod +x "$out/bin/foo"
    '';
  in
    package
  ```
