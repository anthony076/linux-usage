## pkg.lib.runTests 概述

- 使用場景 : 主要用於nix-expr的測試

- 在 nix repl 檢視 pkg.lib.runTests
  
  ```
  pkgs = import <nixpkgs> {}
  lib = pkgs.lib
  :t lib.runTests   # function
  :e lib.runTests   # 打開runTests源碼 @ lib/debug.nix
  ```

## ref

- [Unit Test Your Nix Code](https://www.tweag.io/blog/2022-09-01-unit-test-your-nix-code/)
