## test on nixos

- 通用測試框架，pkgs.testers，提供以下功能
  - nixos配置文件測試 : 用於測試nixos配置文件的正確性
    - 測試nixos主機上安裝的套件
    - 測試nixos主機上運行的 service/client 交互
    - 例如，
      - [測試瀏覽器](https://github.com/NixOS/nixpkgs/blob/master/nixos/tests/chromium.nix)
      - [測試桌面環境](https://github.com/NixOS/nixpkgs/blob/master/nixos/tests/gnome.nix)
      - [測試遊戲](https://github.com/NixOS/nixpkgs/blob/master/nixos/tests/teeworlds.nix)
      - [測試virtualbox功能測試](https://github.com/NixOS/nixpkgs/blob/master/nixos/tests/virtualbox.nix)
    - [提供各種套件的測試範例](https://github.com/NixOS/nixpkgs/tree/master/nixos/tests)

  - 虛擬機測試 : 利用 QEMU 生成虛擬機來進行系統級別的測試
  - 單元測試 : 用於單個套件或derivation的測試

- 測試nix-language(nix-expr)的測試框架
  - [pkgs.lib.runTests](https://github.com/NixOS/nixpkgs/blob/master/lib/debug.nix#L432)
  - nixt套件 : 以 cli 測試nix-expr，支持多個測試用例
  - Pythonix : python庫，利用py測試nix-expr

- [透過 passthru 屬性為套件測試提供外部調用](test-by-passthru-attribute.md)

## 測試框架的使用

- [pkgs.testers](pkgs.testers.md)
- [pkgs.lib.runTests](pkgs.lib.runTests.md)

## Nixos 配置測試

- [利用runNixOSTest測試配置](pkgs.testers-runNixOSTest.md)
- [testing in flake](testing-in-flake.md)

## 服務器測試範例，多台VM交互測試

- [Integration testing with NixOS virtual machines](https://nix.dev/tutorials/nixos/integration-testing-using-virtual-machines.html)

- [服務器測試範例，How to use NixOS testing framework with flakes](https://blog.thalheim.io/2023/01/08/how-to-use-nixos-testing-framework-with-flakes/)

- [需要搭建服務器的Test範例，Running NixOS Tests with Nix Flakes](https://blakesmith.me/2024/03/02/running-nixos-tests-with-flakes.html)

- [服務器測試範例](https://www.youtube.com/live/RgKl8Jue4qM?si=am87yb5NeQtZUwRA&t=340)
