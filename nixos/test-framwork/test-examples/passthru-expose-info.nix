# 範例，對不支援 passthru 屬性的函數，透過 overrideAttrs 添加 passthru 屬性，
# 存取時，passthru屬性可省略

# default.nix

{ pkgs ? import <nixpkgs> {} }:

let
  helloScript = pkgs.writeScriptBin "hello-script" ''
    #!/bin/sh
    echo "Hello, world!"
  '';
in
{
  hello-script = helloScript.overrideAttrs (oldAttrs: {
    # 添加passthru屬性
    passthru = {
      scriptPath = "${helloScript}/bin/hello-script";
    };
  });
}

#  $ nix eval --raw -f ./default.nix hello-script.passthru.scriptPath    WORK
#  $ nix eval --raw -f ./default.nix hello-script.scriptPath    WORK

# ===============================================

# 範例，對不支援 passthru 屬性的函數，添加自定義屬性 (存取時，passthru屬性可省略)
# default.nix

{ pkgs ? import <nixpkgs> {} }:

let
  helloScript = pkgs.writeScriptBin "hello-script" ''
    #!/bin/sh
    echo "Hello, world!"
  '';
in
{
  hello-script = helloScript.overrideAttrs (oldAttrs: {
    # 添加自定義屬性
    my = {
      scriptPath = "${helloScript}/bin/hello-script";
    };
  });
}

# 額外添加自定義屬性到 pkgs.writeScriptBin
#  $ nix eval --raw -f ./default.nix hello-script.my.scriptPath    WORK
#  $ nix eval --raw -f ./default.nix hello-script.scriptPath    FAIL

# ===============================================

# 範例，對支援 passthru 屬性的函數添加 passthru 屬性

{
  pkgs ? import <nixpkgs> {},
}:

pkgs.stdenv.mkDerivation {
  name = "my-hello";
  src = pkgs.hello.src;

  buildInputs = [ pkgs.hello ];

  # 使用 passthru 屬性 將 derivation 的路徑暴露出來
  passthru = {
    helloPath = "${pkgs.hello}";
  };
}

# 在 nix repl 中察看
# nix-repl> myhello = import ./default.nix {}
# nix-repl> myhello.passthru.helloPath    WORK
# nix-repl> myhello.helloPath    WORK

# 透過 nix eval 命令查看
# $ nix eval -f ./default.nix passthru.helloPath

# ===============================================

# 範例，對支援 passthru 屬性的函數添加自定義屬性
{
  pkgs ? import <nixpkgs> {},
}:

# 定義一個 derivation，將 hello 曝露出來
pkgs.stdenv.mkDerivation {
  name = "my-hello";
  src = pkgs.hello.src;

  buildInputs = [ pkgs.hello ];

  # 使用 自定義 屬性 將 derivation 的路徑暴露出來
  my = {
    helloPath = "${pkgs.hello}";
  };

  installPhase = ''
    mkdir -p $out/bin
    ln -s ${pkgs.hello}/bin/hello $out/bin/my-hello
  '';
}

# 在 nix repl 中察看
# nix-repl> myhello = import ./default.nix {}
# nix-repl> myhello.my.helloPath    WORK
# nix-repl> myhello.helloPath    NOT WORK

# 透過 nix eval 命令查看
# $ nix eval -f ./default.nix my.helloPath    WORK
# $ nix eval -f ./default.nix helloPath    NOT WORK

