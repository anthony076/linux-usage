{ home-manager, hm-config, ... }:
{
  imports = [
    home-manager.nixosModules.home-manager
  ];

  networking.useDHCP = false;

  services.openssh.enable = false;
  services.xserver.enable = false;

  virtualisation.memorySize = 512;
  virtualisation.qemu.options = [
    "-nographic"                     # 禁用圖形介面
    "-device" "qemu-xhci"            # 可選：使用 USB 控制器
    "-device" "usb-kbd"              # 添加 USB 鍵盤
    "-nodefaults"                    # 禁用所有默認設備
  ];

  boot.kernelParams = [ "noapic" ];

  # 必要，將 home.nix 的配置並傳遞給 root
  home-manager.users.root = hm-config;
}