# 相較於 runNixOSTest-flake-hm-modulize-nixosModules
# 此方法將 home.nix 和 myconf.nix 都在 flake.nix 中完成加載，
# 可以在最上層的 flake.nix 直接看到加載哪些檔案，對代碼理解較有幫助

{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs, home-manager, ... }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };

      # 加載home-manager配置
      hm-config = import ./home.nix;
    in
    {
      checks.${system}.my-check = pkgs.testers.runNixOSTest {
        name = "hello-test";

        # 定義主機
        nodes = {
          # 定義 configuration.nix 配置
          machine = import ./myconf.nix {
            inherit home-manager;
            inherit hm-config;
          };
        };

        # 加速測試運行
        skipLint = true;
        skipTypeCheck = true;

        # 定義測試腳本
        testScript = ''
          machine.wait_for_unit("default.target")
          machine.succeed("hello")
          machine.fail("world")
          #machine.fail("hello")
        '';
      };
    };
}