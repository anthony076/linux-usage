# 執行，nix-build 或 nix-build -A driverInteractive

let 
  nixpkgs = builtins.fetchTarball "https://github.com/nixOS/nixpkgs/archive/24.05.tar.gz";
  pkgs = import nixpkgs {};
  home-manager = builtins.fetchTarball "https://github.com/nix-community/home-manager/archive/release-24.05.tar.gz";
  hm-config = import ./hm/home.nix;
in
  pkgs.testers.runNixOSTest {
    name = "nixos-configuration-test";
    
    nodes.machine = import ./test/test-config.nix {
      inherit home-manager;
      inherit hm-config;
    };

    # 加速測試運行
    skipLint = true;
    skipTypeCheck = true;

    # 定義測試腳本
    testScript = ''
      machine.start()
      machine.wait_for_unit("default.target")

      # debug 訊息
      _, pwd = machine.execute('pwd');
      machine.log(f"[pwd] = {pwd}")
      
      _, home = machine.execute('echo $HOME')
      machine.log(f"[$HOME] = {home}")
      
      _, justfile = machine.execute('echo $HOME/.justfile')
      machine.log(f"[justfile] = {justfile}")

      # 測試
      # .justfile 位於 $HOME/.justfile ，VM預設目錄為 /tmp 目錄，因為在不同目錄，因此手動加上 .justfile 路徑
      machine.succeed("just -f $HOME/.justfile sys-info")
      machine.succeed("just -f $HOME/.justfile jwd")
      machine.succeed("just -f $HOME/.justfile jconfig")
      machine.succeed("just -f $HOME/.justfile sys-info")
    '';
  }  
