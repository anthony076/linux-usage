
set shell := ["fish", "-c"]

# 執行命令前,不印出要執行的命令
set quiet := true

# 設置區域變數,僅在justfile內可用
# 透過 {{區域變數名}} 存取
#nix_config_path := env_var('MY_NIX_CONFIG')

# =====================

#[group('global')]
#[doc('$ just --list --unsorted OR $ j')]
default:
    just --list --unsorted

# =====================

#[group('global')]
#[doc("switch to nix-config, then $ home-manager switch --flake .")]
hms:
    #!/bin/sh
    cd ~/.config/nix-config
    home-manager switch --flake .

#[group('global')]
#[doc("switch to nix-config, then $ nixos-rebuild switch --flake .")]
nrs:
    #!/bin/sh
    cd ~/.config/nix-config
    nixos-rebuild switch --flake .

#[group('global')]
#[doc("open nix-config folder with helix")]
#config:
#    hx {{nix_config_path}}

# 範本
# [doc("")]
# [group('global')]
# nrs:
#     #!/bin/sh

# =====================

#[group('just debug')]
#[doc("show just working directory")]
jwd:
    pwd

#[group('just debug')]
#[doc("show .justfile path")]
jconfig:
    echo {{justfile()}}

#[group('just debug')]
#[doc("show system info")]
sys-info:
    echo "architecture = {{arch()}}"
    echo "number of logical CUPs = {{num_cpus()}}"

