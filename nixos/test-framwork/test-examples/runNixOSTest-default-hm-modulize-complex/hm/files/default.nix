{
  # 預設路徑為 $HOME
  # 建立 $HOME/.local/bin/myhello 的檔案
  ".local/bin/myhello" = {
    source = ./myhello;
    executable = true;
  };

  ".local/bin/clean" = {
    source = ./clean;
    executable = true;
  };

  ".justfile" = {
    source = ./.justfile;
  };
}
