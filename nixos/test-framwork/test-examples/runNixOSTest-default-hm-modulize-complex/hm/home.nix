{ config, pkgs, ... }:

{
  home.username = "root";
  home.homeDirectory = "/root";
  home.stateVersion = "24.05";

  # 設置環境變數
  # - 此屬性會建立 hm-session-vars.sh，必須加入shell的配置文件中才會正確加載
  # - 確保 shell 的配置文件中有加入，source "${HOME}/.nix-profile/etc/profile.d/hm-session-vars.sh"
  # - 在 home.nix 中添加 shell 的配置，會自動將 hm-session-vars.sh 添加到 shell 的配置文件中
  # - 對於獨立版的hm，若透過 configuration.nix 安裝系統級的shell ，就不會自動添加到shell的配置文件中
  home.sessionVariables = { 
    EDITOR = "hx";
    JUST_JUSTFILE = "$HOME/.justfile";
  };

  # 設置 PATH 環境變數
  # 此命令會將新路徑添加到 hm-session-vars.sh 檔案中，是添加到 PATH 變數的語法糖
  home.sessionPath = [ "$HOME/.local/bin" ];

  # 建立檔案 (預設路徑為 $HOME)
  home.file = import ./files;

  # 安裝套件
  home.packages = with pkgs; [ 
    helix 
    fish 
    just
  ];
  
  # 配置套件
  programs.home-manager.enable = true;
  programs.helix = import ./packages/helix.nix { inherit pkgs; };
  programs.fish = import ./packages/fish.nix;
}

