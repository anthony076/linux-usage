{
  enable = true;

  interactiveShellInit = ''
    set fish_greeting 
    set -x MY_NIX_CONFIG ~/.config/nix-config
  '';

  shellAliases = {
    hm = "home-manager";
    nr = "nixos-rebuild";
   
    j = "just";
    hms = "just hms";
    nrs = "just nrs";
    config = "just config";
    
    ga = "git add *";
    gs = "git status";
  };

}
