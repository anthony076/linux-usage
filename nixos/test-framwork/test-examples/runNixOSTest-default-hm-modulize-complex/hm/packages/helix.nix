{ pkgs, ... }: {
  # 必要，否則只會安裝執行檔，不會進行環境配置
  enable = true;

  languages = {
    language = [
    { # 通用配置
      name = "*";
      scope = "*";
      file-types = [];
      indent = { 
        # 插入兩個單位的unit
        tab-width = 2;
        # 實際插入的符號為空白
        unit = " ";
      };
    }

    {
      name = "nix";
      auto-format = true;
      #formatter.command = "${pkgs.nixfmt}/bin/nixfmt";
      language-servers = [ "nix" ]; # 指向 language-server.nix
    }
    
    ];

    language-server.nix = { command = "${pkgs.nil}/bin/nil"; };
  };

  settings = {
    theme = "catppuccin_frappe";

    editor = {
      true-color = true;
      line-number = "relative";
      mouse = true;
      cursorline = true;

      soft-wrap = { wrap-indicator = ""; };

      cursor-shape = {
        insert = "bar";
        normal = "block";
        select = "underline";
      };

      file-picker = {
        hidden = false;
        git-ignore = false;
      };

      statusline = {
        left = [
          "mode"
          "spacer"
          "version-control"
          "spacer"
          "separator"
          "file-name"
          "file-modification-indicator"
        ];

        right = [
          "spinner"
          "spacer"
          "workspace-diagnostics"
          "separator"
          "spacer"
          "diagnostics"
          "position"
          "file-encoding"
        ];

        separator = "|";
      };

      whitespace.render = {
        space = "all";
        tab = "all";
        nbsp = "none";
        nnbsp = "none";
        newline = "none";
      };

      indent-guides = { render = true; };

      smart-tab = { enable = false; };

      lsp = { display-messages = true; };
    };

    keys.normal = {
      x = "no_op";
      y = [ "yank_main_selection_to_clipboard" "goto_line_end" ];
      p = [ "paste_clipboard_after" "goto_line_start" ];
      d = [ "yank_main_selection_to_clipboard" "delete_selection_noyank" ];
      A = "insert_at_line_start";
      I = "insert_at_line_end";
      G = "goto_file_end";
      V = "extend_line_below";
      ret = [ "move_line_down" "goto_line_start" ];
      "C-/" = "toggle_comments";

      # 接受字串鍵，但不接受字串鍵中包含. 例如，"C-g.a" 是錯誤的語法
      # "C-g".a = ":sh git add . && git commit --amend --no-edit";
      #  "C-g".f = [ ":sh git push -f" ":reload" ];
      #  "C-g".g = ":sh gitGraph";
      #  "C-g".s = ":sh git status";

      g.e = "goto_line_end";
      g.s = "goto_line_start";
      g.b = "goto_first_nonwhitespace";

      c.o = ":config-open";
      c.r = ":config-reload";

      f.w = ":write!";
      f.q = ":quit!";
    };

    keys.select = {
      y = [
        "yank_main_selection_to_clipboard"
        "normal_mode"
        "move_line_down"
        "goto_line_start"
      ];
      d = [ "yank_main_selection_to_clipboard" "delete_selection_noyank" ];
    };
  };
}
