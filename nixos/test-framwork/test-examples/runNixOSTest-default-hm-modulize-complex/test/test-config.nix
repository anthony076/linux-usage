# - import 路徑時，`.`代表當前檔案的路徑
# - nix-build 在建構前，會進行評估，並執行和讀取所有的 import 的語句
# 因此，對於所有 import 的語句，不需要複製到 vm 中

{ 
  home-manager, 
  hm-config,
  ...
}:
{
  imports = [
    (import "${home-manager}/nixos")
    #home-manager.nixosModules.home-manager  #issued
  ];

  # 以下用於 nixosTest 額外添加的配置，用於加速 vm 的啟動
  
  networking.useDHCP = false;

  services.openssh.enable = false;
  services.xserver.enable = false;

  virtualisation.memorySize = 512;
  virtualisation.qemu.options = [
    "-nographic"                     # 禁用圖形介面
    "-device" "qemu-xhci"            # 可選：使用 USB 控制器
    "-device" "usb-kbd"              # 添加 USB 鍵盤
    "-nodefaults"                    # 禁用所有默認設備
  ];

  # ===== 設置共享目錄 =====

  # virtualisation.sharedDirectories.test = {
  #   #source = "/root/share-test";   #會有權限問題
  #   source = "/tmp/shared-xchg";    #依舊出現no such directory 而無法掛載
  #   target = "/tmp/shared";
  # };

  boot.kernelParams = [ "noapic" ];

  # 必要，加載 home.nix 的內容
  #home-manager.users.root = import ./hm/home.nix;    # 會有加載路徑的問題，改在頂層的 flake.nix 中加載
  home-manager.users.root = hm-config;
}
