# 測試，nix-build -A driverInteractive
# 測試，nix-build

let
  nixpkgs = builtins.fetchTarball "https://github.com/nixOS/nixpkgs/archive/24.05.tar.gz";
  pkgs = import nixpkgs {};
in
  pkgs.testers.runNixOSTest {
    name = "nixos-configuration-test";

    nodes.machine = { pkgs, ... }:
    {

      environment.systemPackages = with pkgs; [
        nfs-utils
      ];

      virtualisation.memorySize = 512;

      # mount -t nfs 10.0.2.15:/data /data
      virtualisation.fileSystems."/data" = {
        device = "10.0.2.2:/data";
        fsType = "nfs";
        # options = [ "nfsvers=3"];  # 確認 Host 和 VM 支援的 NFS 版本相容
      };

      # 用戶端掛載 nfs 時，需要使用到 rpc.statd
      services.rpcbind.enable = true;

      # 關閉防火牆，讓 nfs 可以存取，僅用於測試
      networking.firewall.enable = false;

      boot.kernelParams = [ "noapic" ];
    };

    # 加速測試運行
    skipLint = true;
    skipTypeCheck = true;

    # 定義測試腳本
    testScript = ''
      machine.start()
    '';
  }