
## runNixOSTest-nfs-hostSerVer-vmClient

- 環境
  - Server : NixOS
  - Client : 透過 runNixOSTest 建立的 qemu-nixos-vm 

  - 本範例使用本機作為 nfs-server，在運行 runNixOSTest 函數後，qemu-client-vm 透過 nfs 存取本機上的檔案

- 基本認識
  - qemu-client-vm 可以透過 10.0.2.2 存取 hostServer 上的資源
    
    10.0.2.2 是 QEMU 預設讓VM存取host的IP，且不需要任何的port-forwarding

    qemu 會將 qemu-client-vm 的請求，轉換為 localhost 的請求後，轉發給 hostServer，
    
    因此，hostServer 上看到的請求是來自 127.0.0.1

  - nfs-server 預設的配置是封鎖客戶端來自 port=1024 以上的請求

    在 nfs-server 的配置文件中，需要添加 insecure 的選項避免此問題

- [調試紀錄](debug-history.log)

## 在hostServer上的準備工作
  
- step，建立共享目錄

  ```
  mkdir /data，    建立共享目錄
  chmod 755 /data  變更共享目錄的存取權限
  chown nfsnobody:nfsnobody /data，變更共享目錄的擁有者 (未設置 no_root_squash 才需要)
  ```

- step，在 configuration.nix 中進行 nfs-server 的配置

  [完整的hostServer配置](hostServer.nix)

  ```
  services.nfs.server.enable = true;
  
  # 添加 insecure 避免用戶使用高於 1024 以上的port發出請求時被拒
  services.nfs.server.exports = ''
    /data *(rw,sync,nohide,no_root_squash,no_subtree_check,insecure)
  '';

  services.nfs.server.createMountPoints = true;
  ```

- 套用變更，`$ nixos-rebuild switch --flake .`

## qemu-client-vm 的配置

參考，[client-side](vmClient.nix)

## 執行

- $ nix-build -A driverInteractive
- $ nix-build