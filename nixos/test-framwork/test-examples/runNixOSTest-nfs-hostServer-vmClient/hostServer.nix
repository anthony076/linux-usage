{ config, lib, pkgs, ... }:

{
  imports = [ ./hardware-configuration.nix ];

  boot.loader.grub.enable = true;
  boot.loader.efi.efiSysMountPoint = "/boot";
  boot.loader.grub.device = "/dev/sda";

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  environment.systemPackages = with pkgs; [ vim wget git ];

  services.openssh.enable = true;
  services.openssh.settings.PermitRootLogin = "yes";
  services.openssh.settings.PasswordAuthentication = true;

  # ==== 設置 nfs-server ====

  services.nfs.server.enable = true;
  
  services.nfs.server.exports = ''
    /data *(rw,sync,nohide,no_root_squash,no_subtree_check,insecure)
  '';

  services.nfs.server.createMountPoints = true;
  
  # ========

  networking.firewall.enable = false;

  system.stateVersion = "24.05";
}
