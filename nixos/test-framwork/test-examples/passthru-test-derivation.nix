{ 
  pkgs ? import <nixpkgs> {}
}:

let
  my-package = pkgs.stdenv.mkDerivation {
    pname = "my-package";
    version = "1.0";

    src = ./.;

    # 在 buildPhase 中創建 hello.sh 腳本
    buildPhase = ''
      mkdir -p $out/bin
      echo '#!/bin/sh' > $out/bin/hello.sh
      echo 'echo "hello"' >> $out/bin/hello.sh
      chmod +x $out/bin/hello.sh
    '';

    passthru = {
      # 暴露訊息
      compatibleVersions = [ "2.1" "2.2" "3.0" ];

      # 定義測試函數，直接引用 my-package 的輸出路徑
      test = pkgs.runCommand "test-hello-sh" {} ''
        result="$(${my-package}/bin/hello.sh)"
        if [ "$result" = "hello" ]; then
          echo "test passed" > $out
        else
          echo "test failed" > $out
          exit 1
        fi
      '';
    };
  };
in
{
  inherit my-package;
}

# 執行測試
# $ nix-build -A my-package.passthru.test

# 檢視結果
# cat result