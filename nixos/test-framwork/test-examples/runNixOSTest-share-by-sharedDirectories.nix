# 注意只能使用 $ nix-build -A driverInteractive 進行測試
# $ nix-build 具隔離性，無法掛載共享目錄

let 
  nixpkgs = builtins.fetchTarball "https://github.com/nixOS/nixpkgs/archive/24.05.tar.gz";
  pkgs = import nixpkgs {};
in
  pkgs.testers.runNixOSTest {
    name = "nixos-configuration-test";

    nodes.machine = { pkgs, ... }:
    {

      networking.useDHCP = false;
      services.openssh.enable = false;
      services.xserver.enable = false;
      virtualisation.memorySize = 512;
      virtualisation.qemu.options = [
        "-nographic"                     # 禁用圖形介面
        "-device" "qemu-xhci"            # 可選：使用 USB 控制器
        "-device" "usb-kbd"              # 添加 USB 鍵盤
        "-nodefaults"                    # 禁用所有默認設備
        # "-enable-kvm"
      ];

      virtualisation.sharedDirectories.test = {
        source = "/root/share-test";
        target = "/tmp/shared";
      };

      boot.kernelParams = [ "noapic" ];
    };

    

    skipLint = true;
    skipTypeCheck = true;

    testScript = ''
      machine.start()
      machine.wait_for_unit("default.target")

      _, cp = machine.execute('cp /tmp/shared/hello.log')
      machine.log(f"[cp] = {cp}")

      _, hello = machine.execute('cat /tmp/shared/hello.log')
      machine.log(f"[hello] = {hello}")
    
    '';
  }  
