# flake.nix + checks-attribute + pkgs.testers.runNixOSTest() + home-manager + home.nix

# home.nix

{ config, pkgs, ... }:
{
  home.username = "root";
  home.homeDirectory = "/root";
  home.stateVersion = "24.05";
  home.packages = [
    pkgs.hello
  ];

  programs.home-manager.enable = true;
}

# ===================================================

# flake.nix

{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs, home-manager, ... }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
    in 
    {
      checks.${system}.my-check = pkgs.testers.runNixOSTest {
        name = "hello-test";

        nodes = {
          machine = { pkgs, ... }: 
          {
            imports = [
              home-manager.nixosModules.home-manager
            ];

            networking.useDHCP = false;
            
            services.openssh.enable = false;
            services.xserver.enable = false;
            
            virtualisation.memorySize = 512;
            virtualisation.qemu.options = [
              "-nographic"                     # 禁用圖形介面
              "-device" "qemu-xhci"            # 可選：使用 USB 控制器
              "-device" "usb-kbd"              # 添加 USB 鍵盤
              "-nodefaults"                    # 禁用所有默認設備
            ];

            boot.kernelParams = [ "noapic" ];

            home-manager.users.root = import ./home.nix;
          };
        };

        skipLint = true;
        skipTypeCheck = true;
        testScript = ''
          machine.wait_for_unit("default.target")
          machine.succeed("hello")
          #machine.fail("hello")
        '';
      };
    };
}


# 執行
# - $ nix flake check，自動執行 checks 屬性下所有項目
# - $ nix flake check --debug，打印執行過程

# 結果
# - 若正常運行，不會打印出任何訊息
# - 若遇到失敗，拋出錯誤，並提示檢視方法，例如，nix log /nix/store/klxaiagqpnfhx2khz83aaq3y5dnc98x6-vm-test-run-hello-test.drv