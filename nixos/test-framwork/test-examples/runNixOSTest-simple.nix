# ttt/default.nix

{ pkgs ? import <nixpkgs> {} }:

pkgs.testers.runNixOSTest {
  name = "hello-test";

  nodes = {
    machine = { pkgs, ... }: {
      environment.systemPackages = with pkgs; [ hello ];
    };
  };

  skipLint = true;
  skipTypeCheck = true;
  testScript = ''

    #start_all()
    #machine.start()  如果後續 machine 有操作，就會自動執行 machine.start()
  
    machine.wait_for_unit("default.target")

    # 執行 nix-build後不會有輸出，會打印到 shell 中
    machine.succeed("hello")
    #machine.fail("hello")
  '';
}

# 執行方法1，$ nix-build 或 $ nix-build default.nix，此命令會打印出結果，但是不會有任何輸出

# 執行方法2，$ nix-build -A driverInteractive，會建立 result/bin/nixos-test-driver，
# 完成建構後，需要透過 result/bin/nixos-test-driver 進入 shell 中手動執行 test_script()
