# default.nix + pkgs.testers.runNixOSTest() + home-manager

# 檔案結構
# .
# ├── default.nix
# └── home.nix

# default.nix

let
  nixpkgs = builtins.fetchTarball "https://github.com/nixOS/nixpkgs/archive/24.05.tar.gz";
  pkgs = import nixpkgs {};
  home-manager = builtins.fetchTarball "https://github.com/nix-community/home-manager/archive/release-24.05.tar.gz";
in 
  pkgs.testers.nixosTest {
    name = "hello-test";

    nodes = {
      machine = { pkgs, ... }: 
      {
        imports = [
          (import "${home-manager}/nixos")
        ];
        
        networking.useDHCP = false;
        
        services.openssh.enable = false;
        services.xserver.enable = false;
        
        virtualisation.memorySize = 512;
        virtualisation.qemu.options = [
          "-nographic"                     # 禁用圖形介面
          "-device" "qemu-xhci"            # 可選：使用 USB 控制器
          "-device" "usb-kbd"              # 添加 USB 鍵盤
          "-nodefaults"                    # 禁用所有默認設備
        ];

        boot.kernelParams = [ "noapic" ];

        home-manager.users.root = import ./home.nix;
      };
    };

    testScript = ''
      machine.wait_for_unit("default.target")
      machine.succeed("hello")
      #machine.fail("hello")
    '';
}

# ===================================

# home.nix

{ config, pkgs, ... }:

{
  home.username = "root";
  home.homeDirectory = "/root";
  home.stateVersion = "24.05";
  home.packages = [
    pkgs.hello
  ];

  programs.home-manager.enable = true;
}

# ===================================

# 執行 nix-build