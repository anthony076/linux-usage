let
  pkgs = import <nixpkgs> {};

  package = pkgs.runCommand "foo" {

    passthru.tests.version = pkgs.testers.testVersion {
      package = package;
      version = "1.2";
    };

    passthru.tests.my = pkgs.testers.testVersion {
      package = package;
      version = "1.3";
    };

    script = ''
      echo "1.2"
    '';

    passAsFile = [ "script" ];
  } ''
    mkdir -p "$out/bin"
    cp "$scriptPath" "$out/bin/foo"
    chmod +x "$out/bin/foo"
  '';
in
  package


# nix-build -A passthru.tests   會執行所有測試