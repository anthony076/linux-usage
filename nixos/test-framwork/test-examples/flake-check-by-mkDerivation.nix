# flake.nix + checks-attribute + pkgs.stdenv.mkDerivation()

{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
  };

  outputs = { nixpkgs, ... }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
      };
    in {
      # 透過 nix flake check 測試 hello 建構是否完成
      checks.${system}.my-check = pkgs.stdenv.mkDerivation {
        name = "hello-test";
        src = ./.;
        doCheck = true;
        nativeBuildInputs = [pkgs.hello];
        checkPhase = ''
          mkdir -p $out/log
          hello | tee $out/log/output.log
        '';
      };
    };
}

# 執行
# - $ nix flake check，自動執行 checks 屬性下所有項目
# - $ nix flake check --debug，打印執行過程
# - nix build .#checks.x86_64-linux.my-check，手動測試指定項目
# 注意，沒有 nix flake check .#checks.x86_64-linux.my-check 的命令
