# ttt/default.nix

{ pkgs ? import <nixpkgs> {} }:

pkgs.testers.runNixOSTest {
  name = "hello-test";

  nodes = {
    machine = { pkgs, ... }: {
      imports = [ 
        <nixpkgs/nixos/modules/profiles/minimal.nix> 
      ];
      
      networking.useDHCP = false;
      
      services.openssh.enable = false;
      services.xserver.enable = false;
      
      virtualisation.memorySize = 512;
      virtualisation.qemu.options = [
        "-nographic"                     # 禁用圖形介面
        "-device" "qemu-xhci"            # 可選：使用 USB 控制器
        "-device" "usb-kbd"              # 添加 USB 鍵盤
        "-nodefaults"                    # 禁用所有默認設備
      ];
      
      boot.kernelParams = [ "noapic" ];
      
      environment.systemPackages = with pkgs; [ hello ];
    };
  };

  testScript = ''
    machine.wait_for_unit("default.target")
    machine.succeed("hello")
  '';
}

# 執行方法1，$ nix-build 或 $ nix-build default.nix，此命令會打印出結果，但是不會有任何輸出

# 執行方法2，$ nix-build -A driverInteractive，會建立 result/bin/nixos-test-driver，
# 完成建構後，需要透過 result/bin/nixos-test-driver 進入 shell 中手動執行 test_script()
