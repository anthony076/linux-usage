
{ pkgs, ... }:

{
 
  virtualisation.memorySize = 512;
  # virtualisation.qemu.options = [
  #   "-nographic"                     # 禁用圖形介面
  #   "-device" "qemu-xhci"            # 可選：使用 USB 控制器
  #   "-device" "usb-kbd"              # 添加 USB 鍵盤
  #   "-nodefaults"                    # 禁用所有默認設備
  # ];

  # ==== 在 client 上掛載 nfs 提供的目錄 /data ====

  virtualisation.fileSystems."/data" = {
    device = "server:/data";
    fsType = "nfs";
    options = [ "nfsvers=4"];  # 確認 Host 和 VM 支援的 NFS 版本相容
   };

  # 關閉防火牆，讓 nfs 可以存取，僅用於測試
  networking.firewall.enable = false;

  boot.kernelParams = [ "noapic" ];
}
