{ pkgs, ... }:

{

  virtualisation.memorySize = 512;
  # virtualisation.qemu.options = [
  #   "-nographic"                     # 禁用圖形介面
  #   "-device" "qemu-xhci"            # 可選：使用 USB 控制器
  #   "-device" "usb-kbd"              # 添加 USB 鍵盤
  #   "-nodefaults"                    # 禁用所有默認設備
  # ];

  # ==== setup nfs-server ====

  services.nfs.server.enable = true;

  services.nfs.server.exports = ''
    /data *(rw,sync,nohide,no_root_squash,no_subtree_check)
  '';
  
  # 在開機啟動期間，根據 /etc/exports 的內容建立掛載點
  services.nfs.server.createMountPoints = true;

  # 關閉防火牆，讓 nfs 可以存取，僅用於測試
  networking.firewall.enable = false;

  boot.kernelParams = [ "noapic" ];
}
