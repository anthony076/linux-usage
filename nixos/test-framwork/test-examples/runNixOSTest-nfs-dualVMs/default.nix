# 測試，nix-build -A driverInteractive
# 測試，nix-build
# ref : https://github.com/NixOS/nixpkgs/blob/master/nixos/tests/nfs/simple.nix

# IP
# server : 192.168.1.2 (eth1) | 10.0.2.15 (eth0)
# client : 192.168.1.1 (eth1) | 10.0.2.15 (eth0)

# nix-build 執行過程中，會出現以下訊息，但不影響nfs的掛載
# - rpcbind.service: Referenced but unset environment variable evaluates to an empty string: RPCBIND_OPTIONS
# - agetty[865]: failed to open credentials directory
# - [client] sm-notify[851]: Version 2.6.2 starting
# - [client] sm-notify[851]: Failed to open sm: No such file or directory
# - [client] sm-notify[851]: Failed to open directory sm.bak: No such file or directory

let 
  nixpkgs = builtins.fetchTarball "https://github.com/nixOS/nixpkgs/archive/24.05.tar.gz";
  pkgs = import nixpkgs {};
in
  pkgs.testers.runNixOSTest {
    name = "nfs-dual-vm";

    nodes.server = import ./server-vm.nix;
    nodes.client = import ./client-vm.nix;
    
    skipLint = true;
    skipTypeCheck = true;

    testScript = ''
      server.wait_for_unit("nfs-server")
      server.succeed("systemctl start network-online.target")
      server.wait_for_unit("network-online.target")
    
      start_all()

      client.wait_for_unit("data.mount")
      server.succeed('echo "hello" > /data/hello.log')
      client.succeed('cat /data/hello.log')
    '';
  }  
