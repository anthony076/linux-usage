## runNixOSTest-nfs-dualVMs

- 環境
  - Server : 透過 runNixOSTest 建立的 qemu-server-vm
  - Client : 透過 runNixOSTest 建立的 qemu-client-vm 
  
  - 本範例在運行 runNixOSTest 函數後，會建立`qemu-server-vm`和`qemu-client-vm`，
    並且，兩個 vm 會處於同一個網段中，qemu-client-vm 可以透過 nfs 直接存取 qemu-server-vm 共享的目錄

## qemu-server-vm 的配置

- step，必要，啟用`services.nfs.server.enable = true`

  啟用後會添加以下配置
  ```
  # ref : https://github.com/NixOS/nixpkgs/blob/nixos-24.05/nixos/modules/services/network-filesystems/nfsd.nix

  # 啟用 rpcbind-service
  services.rpcbind.enable = true;
  
  # 添加 nfs kernel-module
  boot.supportedFilesystems = [ "nfs" ]; # needed for statd and idmapd

  # 建立 nfc 配置檔，/etc/exports
  environment.etc.exports.source = ...;

  # 建立 nfs-server.service
  systemd.services.nfs-server = ...;

  建立 nfs-server.service
  systemd.services.nfs-mountd = ...;
  ```

- step，必要，透過`services.nfs.server.exports`建立nfs配置檔(/etc/exports)的內容

  ```
  # * 接受任何IP的用戶連接到 nfs-server
  # rw : 用戶可讀、可寫
  # sync : 變更立刻寫入
  # no_root_squash : 若用戶使用root帳戶，則保留root帳號，不將用戶的帳號壓縮成一般用戶
  # no_subtree_check : 禁用子目錄檢查，可加快存取並減少檢查開銷

  # 其他參數，參考，[/etc/exports 設定檔的語法與參數](https://linux.vbird.org/linux_server/centos6/0330nfs.php#nfsserver_exports)
  
  services.nfs.server.exports = ''
    /data *(rw,sync,nohide,no_root_squash,no_subtree_check)
  '';

  ```

- step，必要，透過`services.nfs.server.createMountPoints = true`，
  在開機啟動期間，根據 /etc/exports 的內容建立掛載點

  啟用時，會在nfs-server.service啟動時自動檢查並創建 /etc/exports 中列出的導出目錄，
  避免啟動nfs-server.service服務時出錯

## qemu-client-vm 的配置

建立共享目錄的掛載點
```
fileSystems."/data" = {
  device = "server:/data";
  fsType = "nfs";
  options = [ "nfsvers=4"];  # 確認 Host 和 VM 支援的 NFS 版本相容
};
```

## 執行方式

```
cd runNixOSTest-nfs-dualVMs
nix-build -A driverInteractive 或 nix-build
```