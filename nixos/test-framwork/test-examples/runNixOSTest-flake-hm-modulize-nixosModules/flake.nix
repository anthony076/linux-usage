# 此方法在 flake.nix 中僅加載 configuration.nix 檔案的內容，
# 而 home-manager 的配置 (home.nix) 不在 flake.nix 中加載，而是在 configuration.nix 中加載

{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs, home-manager, ... }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
    in
    {
      checks.${system}.my-check = pkgs.testers.runNixOSTest {
        name = "hello-test";

        nodes = {
          # 加載 configuration.nix
          machine = import ./myconf.nix { inherit home-manager; };
        };

        skipLint = true;
        skipTypeCheck = true;
        testScript = ''
          machine.wait_for_unit("default.target")
          machine.succeed("hello")
          machine.fail("world")
          #machine.fail("hello")
        '';
      };
    };
}