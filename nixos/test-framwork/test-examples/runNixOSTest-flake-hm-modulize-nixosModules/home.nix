{ config, pkgs, ... }:

{
  home.username = "root";
  home.homeDirectory = "/root";
  home.stateVersion = "24.05";
  home.packages = [
    pkgs.hello
  ];

  programs.home-manager.enable = true;
}