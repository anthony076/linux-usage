{ pkgs ? import <nixpkgs> {} }:

let 
  example = pkgs.stdenv.mkDerivation rec {
    pname = "example";
    version = "1.0.0";

    src = ./.; # 假設源代碼在當前目錄

    buildPhase = ''
      echo "Building ${pname} version ${version}"
      echo "echo 'hello'" > ./hello
    '';

    installPhase = ''
      mkdir -p $out/bin
      cp ./hello $out/bin/
      chmod +x $out/bin/hello
    '';

    passthru = {
      # 1. 暴露內部變量
      inherit version;

      # 2. 提供元數據
      description = "A simple example package";

      # 3. 創建便捷函數
      run = "${example}/bin/hello";

      # 4. 傳遞依賴關係信息
      dependencies = [ pkgs.stdenv.cc ];

      # 5. 提供版本信息
      #updateScript = ./update.sh;

      # 6. 創建測試套件
      tests = {
        simple-test = pkgs.stdenv.mkDerivation {
          name = "example-test";
          buildInputs = [ example ];
          buildCommand = ''
            ${example}/bin/hello > test_output
            grep "hello" test_output
            touch $out
          '';
        };
      };

      # 7. 提供構建選項
      withDebug = args: pkgs.stdenv.mkDerivation (args // {
        pname = "${pname}-debug";
        buildPhase = buildPhase + "\necho 'Debug mode enabled' >> $out";
      });

      # 8. 暴露源代碼位置
      src = src;
    };

  };
in
  example

# 建構命令，$ nix-build
# 測試命令，$ nix-build -A tests.simple-test

# 檢視版本，$ nix-instantiate --eval -A version
# 檢視執行路徑，$ nix-instantiate --eval -A run
# 檢視源碼位置，$ nix-instantiate --eval -A src

# 在 nix repl 中
# nix-repl> example = import ./default.nix
# nix-repl> example.version   得到 "1.0.0"
# nix-repl> example.src   得到 /root/passthru-example
# nix-repl> example.dependencie   得到 [ «derivation /nix/store/xlg144qrggr7dik60cwj315d1cyaiqrd-gcc-wrapper-13.3.0.drv» ]
# nix-repl> example.run   得到 "/nix/store/d4hk9vw2mq3ziaqnhnghm9413jqqmilg-example-1.0.0/bin/hello"

