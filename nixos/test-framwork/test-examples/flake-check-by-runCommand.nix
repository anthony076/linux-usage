# flake.nix + checks-attribute + pkgs.runCommand()

{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
  };

  outputs = { nixpkgs, ... }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
      };
    in {
      packages.${system}.default = pkgs.hello;

      # 透過 nix flake check 測試 hello 建構是否完成
      checks.${system}.my-check = pkgs.runCommand "hello-build-test" { buildInputs = [ pkgs.hello]; } ''
        mkdir -p $out/log
        hello | tee $out/log/output.log
      '';
    };
}

# 執行
# - $ nix flake check，自動執行 checks 屬性下所有項目
# - $ nix flake check --debug，打印執行過程
# - nix build .#checks.x86_64-linux.my-check，手動測試指定項目
# 注意，沒有 nix flake check .#checks.x86_64-linux.my-check 的命令

# 檢視測試過程打印的訊息
# - 一般情況下，若測試成功，就不會打印任何訊息
# - 透過 $ nix flake check --debug 查看 drv 的路徑，例如，/nix/store/<hash>-hello-build-test.drv
# - 透過 $ nix derivation show /nix/store/<hash>-hello-build-test.drv，查看 output 的路徑，例如，/nix/store/<hash>-hello-build-test
# - 透過 $ cat /nix/store/<hash>-hello-build-test/log/output.log 查看測試過程中打印的訊息
# - 或直接透過 nix run 執行，nix run 會先執行 nix build 後直接執行 checks.x86_64-linux.my-check 