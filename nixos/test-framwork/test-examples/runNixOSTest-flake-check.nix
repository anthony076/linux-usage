# flake.nix + checks-attribute + pkgs.testers.runNixOSTest()

{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
  };

  outputs = { nixpkgs, ... }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
    in 
    {
      checks.${system}.my-check = pkgs.testers.runNixOSTest {
        name = "hello-test";

        nodes = {
          machine = { pkgs, ... }: {
            environment.systemPackages = with pkgs; [ hello ];
          };
        };

        skipLint = true;
        skipTypeCheck = true;
        testScript = ''
          machine.wait_for_unit("default.target")
          machine.succeed("hello")
          #machine.fail("hello")
        '';
      };
    };
}

# 執行
# - $ nix flake check，自動執行 checks 屬性下所有項目
# - $ nix flake check --debug，打印執行過程

# 結果
# - 若正常運行，不會打印出任何訊息
# - 若遇到失敗，拋出錯誤，並提示檢視方法，例如，nix log /nix/store/klxaiagqpnfhx2khz83aaq3y5dnc98x6-vm-test-run-hello-test.drv