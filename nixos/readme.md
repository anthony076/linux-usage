## Why nixos

- 一份配置文件就可以建構相同的系統
- 系統可以回滾，不用擔心系統更新
- 模組系統可以簡化配置，使配置更加容易
- 函數式聲明的配置文件，使配置更靈活，可以組合或重複使用配置

## nixos usage

- 本教程基於 nixos v23.11

- 基礎
  - [00_基本概念](concept.md)
  - [01_安裝nix和基本設置](install.md)
  - [02_nix 語言](nix-language.md)
  - [03_模組系統](nix-module/nix-module-overview.md)
  - [04配置文件的使用](configuration.md)
  - [函數的使用](functions/functions.md)
  - [nixos 內建命令的使用](commands.md)
  - [Test框架的使用](test-framwork/readme.md)
  - [認證管理](secret-management.md)

- 套件
  - [flake的使用](flake/flake-overview.md)
  - [使用home-manager進行管理](home-manerger/hm-overview.md)
  - [推薦套件](recommend_nix_package.md)
  - [在nixos執行FHS套件的十種方法](how-to/run-fhs-package-in-simulated-linux-env-on-nixos.md)
  - [如何修改第三方package的內容](customize-packages/readme.md)
  - [VirtualMachine(VM)|container|iso概述 + 常用工具函數](vm-container-iso/vm-container-overview.md)

- 進階
  - [如何完整刪除derivation](how-to/how-to-delete-derivation.md)
  - [如何調試和驗證配置的正確性](how-to/how-to-verify-config.md)
  - [如何利用模組化組織複雜的配置(flake和nix-expr的混用)](how-to/how-to-modulize-complex-config.md)
  - [如何使命令都使用統一版本的nixpkgs](how-to/how-to-align-nixpkgs.md)
  - [如何升級系統|如何升級nix](commands.md#使用範例-升級系統--升級-nix)
  - [如何指定repo或壓縮檔來源](how-to/how-to-specific-source.md)
  - [如何建構nixos-iso](build-iso/how-to-build-installer-iso.md)
  - [跨平台編譯](crossCompile/readme.md)
  - [debug實例](how-to/debug-examples.md)
  - [常見問題](common-issues.md)

- CI
  - [hydra的使用](ci/hydra.md)

- 其他
  - [精簡版的nixos分支](https://github.com/NuschtOS)
  - [遠端部屬](https://nixos-and-flakes.thiscute.world/zh/best-practices/remote-deployment)
  - [跨平台編譯](https://nixos-and-flakes.thiscute.world/zh/development/cross-platform-compilation)
  - [kernel開發](https://nixos-and-flakes.thiscute.world/zh/development/kernel-development)
  - [建構無狀態的nixos](https://lantian.pub/article/modify-computer/nixos-impermanence.lantian/)
  - [製作小記憶體 VPS 的 DD 磁盤鏡像](https://lantian.pub/article/modify-computer/nixos-low-ram-vps.lantian/)
  - [搭建自己的緩存服務器](https://nixos-and-flakes.thiscute.world/zh/nix-store/host-your-own-binary-cache-server)
  - [加速第三方配置文件的建構與調試](https://nixos-and-flakes.thiscute.world/zh/best-practices/accelerating-dotfiles-debugging)

## 以qemu運行nixos

- 推薦的啟動配置

  - 在 lg-gram-14z980 的 nb 上，添加 -M q35，在 reboot 後容易出現 錯誤

  ```
  qemu-system-x86_64.exe ^
  -m 4096 -smp 4 ^
  -accel whpx,kernel-irqchip=off ^
  -machine vmport=off ^
  -drive file=nixos_disk.qcow2,format=qcow2 ^
  -display sdl ^
  -device qemu-xhci,id=xhci ^
  -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22

  -m 4096 -smp 4 -M q35
  ```

## ref

- [線上測試 nixos](https://distrosea.com/select/nixos/)
- [RFC，新功能討論](https://discourse.nixos.org/t/pre-rfc-pipe-operator/28387) 
- [A Tour of Eelco's Nix PhD Thesis](https://www.youtube.com/watch?v=Caao69qWK0A&list=PLa01scHy0YEmg8trm421aYq4OtPD8u1SN&index=84)
- [其他推薦的nixos學習資源](https://nixos-and-flakes.thiscute.world/zh/advanced-topics/)

- 安裝 nixos
  - [官方文獻](https://nixos.org/manual/nixos/stable/#sec-installation)
  - [vmware安裝nixos @ nixos中文](https://nixos-cn.org/tutorials/installation/VirtualMachine.html)
  - [從零開始的 NixOS 教程（安裝篇）](https://blog.askk.cc/2022/09/18/nixos-installation/)
  - [nixos的安装教程](https://blog.csdn.net/qq_33831360/article/details/113449752)
  - [nix on WSL](https://xeiaso.net/blog/nix-flakes-4-wsl-2022-05-01/)

- 系列教學
  - [一份非官方的新手指南](https://nixos-and-flakes.thiscute.world/zh/introduction/)
  - [NixOS系列教學 * 5](https://lantian.pub/article/modify-website/nixos-why.lantian/)
  - [nix tutorials * 22](https://www.youtube.com/playlist?list=PLko9chwSoP-15ZtZxu64k_CuTzXrFpxPE)
  - [nix workshop](https://scrive.github.io/nix-workshop/04-derivations/01-derivation-basics.html)
  - [nix asia](https://nixos.asia/en/nix)
  - [Zero to Nix](https://zero-to-nix.com/concepts)
  - [nix blog @ Fernando Ayats](https://ayats.org/blog/)
  - [Nix from First Principles: Flake Edition](https://tonyfinn.com/blog/nix-from-first-principles-flake-edition/)
  - [NixOS Tutorial series @ Why Does Nothing Work](https://www.youtube.com/playlist?list=PLuRxZ95-8LY1mlotZMYGYib5sXJRw1RxW)
  - [nix系列影片 @ Chris McDonough](https://www.youtube.com/playlist?list=PLa01scHy0YEnsvjvMNNk-JSvTdL_ivsml)
  - [nixos 系列影片 @ NixOS](https://www.youtube.com/@NixOS-Foundation/playlists)
  - [nix系列進階文章 @ nix pills](https://nixos.org/guides/nix-pills/)
  - [nix相關文章集(用戶心得)](https://dev.leiyanhui.com/categories/nixos/)
  - [nix相關文章集*49](https://ianthehenry.com/posts/how-to-learn-nix/)
  - [nix相關影片*95](https://www.youtube.com/playlist?list=PLa01scHy0YEmg8trm421aYq4OtPD8u1SN)
  - [nix相關文章*25](https://haseebmajid.dev/tags/nixos/)
  - [nix問答集](https://github.com/NixOS-CN/NixOS-FAQ)
  
  - nix hour @ Tweag
    - [y2b](https://www.youtube.com/playlist?list=PLyzwHTVJlRc8yjlx4VR4LU5A5O44og9in)
    - [github](https://github.com/tweag/nix-hour/issues)

- 編譯源碼
  - [軟體打包，從入門到放棄](https://lantian.pub/article/modify-computer/nixos-packaging.lantian/)
  - [為自己的項目打包](https://jia.je/software/2022/06/07/nix-cookbook/#%E6%89%93%E5%8C%85)
