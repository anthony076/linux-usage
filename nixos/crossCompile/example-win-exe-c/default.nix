# 執行編譯，$ nix-build

{ pkgs ? import <nixpkgs> {} }:

let
  mingwPkgs = pkgs.pkgsCross.mingwW64;
in
  mingwPkgs.stdenv.mkDerivation {
    name = "hello-windows";
    src = ./.;

    buildPhase = ''
      $CC hello.c -o hello.exe
    '';

    installPhase = ''
      mkdir -p $out/bin
      cp hello.exe $out/bin
    '';
  }