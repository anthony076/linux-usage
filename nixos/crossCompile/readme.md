## crossCompile 跨平台編譯

- 概念，名詞解釋 `build-platform` VS `host-platform` VS `target-platform` 三者的差異
  - build-platform : 建構執行檔的平台
  - host-platform : 執行執行檔的平台
  - target-platform : `當編譯出來的產物`是`編譯器或工具鏈`時，專門指該編譯器適用的平台
  
  - 例如，編譯出來的產物是執行檔 : 在 Ubuntu x86_64 上用 gcc 編譯一個給 Windows x86_64 用的程式
    - build-platform = Ubuntu x86_64
    - host-platform = Windows x86_64
    - target-platform = 無，因為編譯產物不是編譯器
    
  - 例如，編譯出來的產物是編譯器 : 在(Ubuntu x86_64)上編譯一個在(Windows x86_64)上執行的(Windows ARM)的交叉編譯器
    - build-platform = Ubuntu x86_64
    - host-platform = Windows x86_64
    - target-platform = Windows ARM  
  
- 確認可用的 host-platform
    
  - 手動指定host-platform時，需要遵循，`<cpu>-<vendor>-<os>-<abi>`的模板設置，
    - 其中，
      - <vendor> 可以是 unknown
      - <abi> 是選用，可以被省略

    - 例如
      - aarch64-apple-darwin14
      - aarch64-pc-linux-gnu
      - x86_64-w64-mingw32
      - aarch64-apple-ios

  - 選擇合適的 host-platform 
    - 方法，nix 2.19以前，指定`<nixpkgs>`不需要添加 -f

      ```nix
      $ nix repl '<nixpkgs>' -I nixpkgs=channel:nixos-23.11 
      nix-repl> pkgsCross.<TAB>
      nix-repl> pkgsCross.aarch64-multiplatform.stdenv.hostPlatform.config
      ```

    - 方法，nix 2.19以後(含)，，指定`<nixpkgs>`需要添加 -f
      
      ```nix
      $ nix repl -f '<nixpkgs>' -I nixpkgs=channel:nixos-23.11 
      nix-repl> pkgsCross.<TAB>
      nix-repl> pkgsCross.aarch64-multiplatform.stdenv.hostPlatform.config
      ```
    
- 透過`pkgs.pkgsCross.<host-platform>.套件名`指定建構特定平台的套件

  ```nix
  let
    nixpkgs = fetchTarball "https://github.com/NixOS/nixpkgs/tarball/release-23.11";
    pkgs = import nixpkgs {};
  in
    pkgs.pkgsCross.aarch64-multiplatform.hello
  ```
  
## [範例] 在 nixos 上編譯 hello.exe 

- 範例，[example-win-exe-c](example-win-exe-c/)，
  - 執行建構，`$ nix-build`
  - 複製到專案目錄，`$ cp result/bin/hello.exe .`
  - 複製到 windows-host，`$ scp -P 2222 -r root@localhost:/root/crossCompile/hello.exe .`
  - 在 windows-host 上執行，`$ hello.exe`

## [範例] 將 pkgs 的套件編譯為 windows 執行檔

- 以 pkgs.hello 為例

  建構命令，$ nix-build hello.nix
  
  ```nix  
  # hello.nix
  let
    nixpkgs = fetchTarball "https://github.com/NixOS/nixpkgs/tarball/release-24.05";
    pkgs = import nixpkgs {};
  in
    pkgs.pkgsCross.mingwW64.hello
  ```

## ref 

- [Cross compilation @ nix.dev](https://nix.dev/tutorials/cross-compilation.html)

- [Cross_Compiling @ nixos.wiki](https://nixos.wiki/wiki/Cross_Compiling)
