## 推薦 nix 套件

- [nix-filter](https://github.com/numtide/nix-filter):
  用於過濾複製到 nix-store 的檔案

- [direnv](https://direnv.net/docs/hook.html):
  - 用於自動管理 shell 環境的工具，允許根據工作目錄動態地加載和卸載環境變量
  - 當專案需要特定的環境變數時，將該變數添加到專案下的.envrc檔案中，
    當進入專案目錄後，direnv會自動加載該變數，使得該變數僅在專案目錄中可用

- [nix-direnv](https://github.com/nix-community/nix-direnv):
  增強 direnv 功能的工具，改進與Nix集成的性能和持久性
  - 改寫 direnv 的 use_nix 和 use_flake 
  - 增加 nix-shell 運行速度
  - 和 lorri 相比不需要外部守護進程，使用更簡單，且支援 flake
  - 使用[介紹](https://tonyfinn.com/blog/nix-from-first-principles-flake-edition/nix-8-flakes-and-developer-environments/)

- nixpkgs-fmt: 用來格式化 Nix 代碼

- nix-tree: 顯示各個 nix derivation 的硬盤占用和依賴關係

- sops-nix: Nixos Secrets Management
  - [介紹:NixOS Secrets Management](https://www.youtube.com/watch?v=6EMNHDOY-wo&list=PLAWyx2BxU4OyERRTbzNAaRHK08DQ0DD_l&index=4)
  - [sops](https://github.com/getsops/sops)
  - [sops-nix](https://github.com/Mic92/sops-nix)

- nix-output-monitor: 提供nom命令，可以看到log output的更多細節

- [niv](https://github.com/nmattia/niv): 基於 nix-store 的 nix專案的依賴管理器，類似 pip、npm
  - 透過 nix init 建立 source.nix 的依賴配置檔
  - 在nix檔案中，透過`import sources.套件名`來使用依賴

- ide
  - nixvim
    - [Nixvim: Neovim Distro Powered By Nix](https://www.youtube.com/watch?v=b641h63lqy0)

  - neovim
    - [Custom Neovim Code Actions & Diagnostics](https://www.youtube.com/watch?v=q-oBU2fO1H4)
    - [Easiest Way To Write Nix | Code Editor Setup](https://www.youtube.com/watch?v=M_zMoHlbZBY)
  
  - jeezyvim
    
## 推薦 flake 套件

- flake-parts: 簡化 flake 的使用

  - 範例，flake-parts.lib.mkFlake 的使用

    ```
    {
      description = "A simple Nix flake with a helloNixosTests package using flake-parts";

      inputs = {
        nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable"; # 使用最新的 nixpkgs
        flake-parts.url = "github:hercules-ci/flake-parts"; # 引入 flake-parts
      };

      outputs = inputs @ { flake-parts, ... }:
        flake-parts.lib.mkFlake { inherit inputs; } {

          systems = [
            "x86_64-linux"
            "aarch64-linux"
          ];

          perSystem = { pkgs, system, ... }: {
            packages.default = pkgs.writeScriptBin "hello-nixos-tests" ''
              ${pkgs.netcat}/bin/nc -l 3000
            '';
          };
        };
    }
    ```

    上述代碼會建立以下的輸出屬性
    ```
    └─packages
        ├─aarch64-linux
        │   └─default omitted (use '--all-systems' to show)
        └─x86_64-linux
            └─default: package 'hello-nixos-tests'
    ```

- flake-utils

  - 範例，flake-utils.lib.eachDefaultSystem 函數的使用

    ```
    outputs = { self, nixpkgs, flake-utils }:
      flake-utils.lib.eachDefaultSystem (system:
        let
          pkgs = nixpkgs.legacyPackages.${system};
        in
          {
            packages = {
              helloNixosTests = pkgs.writeScriptBin "hello-nixos-tests" ''
              ${pkgs.netcat}/bin/nc -l 3000
              '';
            };
          }
      );
    ```

    上述代碼會建立以下的輸出屬性
    ```
    .
    └─packages
      ├───aarch64-darwin
      │   └───helloNixosTests omitted (use '--all-systems' to show)
      ├───aarch64-linux
      │   └───helloNixosTests omitted (use '--all-systems' to show)
      ├───x86_64-darwin
      │   └───helloNixosTests omitted (use '--all-systems' to show)
      └───x86_64-linux
          └───helloNixosTests: package 'hello-nixos-tests'
    ```

## nix-direnv 的使用

- [Setting up direnv and nix-direnv](https://fasterthanli.me/series/building-a-rust-service-with-nix/part-10#setting-up-direnv-and-nix-direnv)
