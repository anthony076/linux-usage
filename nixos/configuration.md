## 常見配置文件的使用

- 常見配置文件
  - `/etc/nixos/configuration.nix`，用於配置 nixos 系統

  - `/etc/nixos/hardware-configuration.nix`
    
    由 nixos-generate-config 命令，根據系統的硬體設備、硬碟分區等自動生成的配置文件

  - `/etc/nixos/flake.nix`，Flakes 的配置文件

  - `/etc/nix/nix.conf`
    
    用於配置 Nix 包管理器的全局設置，例如，設置 Nix 的緩存大小、啟用或禁用垃圾收集等，適用於所有使用 Nix 的用戶和系統

- 比較，`configuration.nix` 和 `home-manager` 的差異
  - configuration.nix 用於系統級的配置，不支援普通用戶級別的應用配置
  - home-manager 用於用戶級的配置，以用戶身分執行配置和建構
  - 例如，在 configuration.nix 中，以下的配置需要改用 home-manager 進行配置

    ```shell
    # configuration.nix
    users.users.xxx = {
      programs.fish.interactiveShellInit = ''
        set fish_greeting # Disable greeting  # 禁用歡迎訊息
      '';
    }
    ```

    需要透過 home-manager 對個人用戶的應用進行配置

    ```shell
    # 
    home-manager.users.myuser = {

      programs.fish = {
        enable = true;
        interactiveShellInit = ''
          set fish_greeting # Disable greeting  # 禁用歡迎訊息
        '';
      };
    }
    ```

## [hardware-configuration.nix] overview

hardware-configuration.nix 用於設置掛載磁碟分區的相關設定，與磁碟映像檔中的磁碟分區配置有關

在製作vm或容器時，可以省略 hardware-configuration.nix 的加載，避免錯誤產生
詳見，[什麼時候不加載hardware-configuration.nix](vm-container-iso/build-qemu-nixos-independ/3rd_nixos-generators_usage.md#配置文件中不加載hardware-configurationnix)

## [configuration.nix] overview

- `nix-rebuild`，用於 configuration 的命令，參考，[nix-rebuild的使用](functions/functions.md)

- configuration.nix 是一個 nix-module，配置選項會自動加載
  - nix-module的基礎，參考，[nix-module](nix-module/nix-module-overview.md)

  - configuration.nix 中可用的配置選項，都定義在`nixos核心模組`，這些模組不需要手動透過imports屬性導入
  
  - 執行`$ nixos-rebuild switch`或`$ nixos-install`命令，會自動加載nixos核心模組
    - nixos核心模組的源碼，參考，[nixpkgs/nixos/module](https://github.com/NixOS/nixpkgs/tree/master/nixos/modules)
    - 實際加載的nixos核心模組列表，可參考，[module-list.nix](https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/module-list.nix)
    - 其他會自動加載nixos核心模組的命令
      - nixos-rebuild
      - nixos-install
      - nixos-container
      - nixos-option
      - nixos-generate-config
  
  - 若使用`nixos-rebuild switch --flake .`，會改找當前目錄下的flake.nix，透過 imports 可將 configuration.nix 導入

- 配置屬性查詢工具
  - 注意，查詢套件和查詢可用選項的搜尋是分開的
  - [查詢configuration.nix所有可用選項](https://search.nixos.org/options)
  - [查詢套件可用配置選項](https://search.nixos.org/packages)
  - [透過網頁快速查詢configuration.nix所有可用選項](https://nixos.org/manual/nixos/stable/options.html)
  - 在VM中，查詢 configuration.nix所有可用參數，`$ man configuration.nix`

- debug configuration.nix 中所有可用的選項

  - configuration.nix 中可用的配置，都定義在[nixpkgs/nixos](https://github.com/NixOS/nixpkgs/tree/master/nixos)中
  
  - `方法`，透過`nix repl`，快速檢視 configuration.nix 可用的配置選項

    透過 `:l`，將屬性集合中的屬性，保存在當前環境

    ```
    nix-repl> :l <nixpkgs/nixos>
    nix-repl> config.  # 按下 <tab>
    nix-repl> options.  # 按下 <tab>
    ```

    或透過 `import`，將屬性集合中的屬性，保存在中間變數

    ```
    nix-repl> nixos = import <nixpkgs/nixos> {}
    
    # 檢視可用選項
    nix-repl> nixos.config.  # 按下 <tab>
    nix-repl> nixos.options.  # 按下 <tab>

    # 範例
    nix-repl> nixos.config.environment.etc.hostname.  # 按下 <tab>
    ```
  
  - `方法`，透過 `nixos-option` 命令，查詢配置選項的定義源碼、引用、簡易說明

    ```
    nixos-option environment.variables

    得到
    Declared by:
    [ "/nix/store/ba2hgyz4k3vcqjc8xsl6h2d7ffq95pli-nixos-23.11/nixos/nixos/modules/config/shells-environment.nix" ]  
    ```

- 配置屬性總覽

  - `nix.settings.experimental-features`: 用於開啟實驗性功能
  
  - `environment`
    - systemPackages屬性
      - 指定系統要安裝的預設套件，通常是來自 nixpkgs 中的套件
      - 對於非 nixpkgs 的套件，可以使用 NIX_PATH 環境變數，或是 builtins.fetchGit 函數來指定 package 位置
    
  - `networking屬性`: 用於設置網路相關參數 

  - `boot屬性`: 用於設置開機相關參數
  - time屬性
  - console屬性
  - sound屬性
  - hardware屬性
  - system屬性

  - `programs.套件名`: 用於套件進行細部的設置
    - 可使用 environment.systemPackages 安裝 vim 之後，再利用 program.vim 進行細部設置，
    - programs.套件名.enable 本身就有安裝並啟用的功能
  
  - `services.套件名`: 用於啟用和設置系統服務
  
  - `users.users.用戶名`: 用於配置帳戶 : 設置密碼、群組、家用目錄...等
    - 範例，`users.users.alice.packages = [ pkgs.nano ];`，為alice的nano建立alice專屬的配置 
    - 可使用 [home-manager](home-manerger/hm-overview.md)取代`users.users.用戶名`的配置屬性，
      配置上會更為簡便

## [configuration.nix] nix 屬性

- nix.settings
  - trusted-users : 當連接到 nix-daemon 時擁有額外權限的用戶或群組列表
    - 可以指定額外的二進制緩存服務器
    - 他們可以導入未簽名的 NAR 文件（Nix 歸檔文件），那些未經數字簽名的包。可以用於開發或測試目的
    - 透過 @wheel 可以指定群組
  - auto-optimise-store : 定期清理未被引用的獨立套件

- nix.nixPath : 設置 $NIX_PATH 的值

  ```nix
    nix.nixPath = [
      "nixpkgs=${inputs.nixpkgs.outPath}"
      "nixos-config=/etc/nixos/configuration.nix"
      "/nix/var/nix/profiles/per-user/root/channels"
    ];
  ```

- nix.package : 設置 nix 套件的版本

- nix.extraOptions : 添加到 nix.conf 的字串
  - 例如，用於啟用 flake，`nix.extraOptions = ''experimental-features = nix-command flakes'';`

- nix.gc : 設置 garbage-collection

  ```nix
  nix.gc = {
      automatic = true;
      options = "--delete-older-than 7d";
    };
  ```

## [configuration.nix] boot 屬性

- 設置 bootloader

  - 使用 `grub`
    - 條件，磁碟分區需要建立 512M 大小的 BIOS Boot Partition

      注意，一般狀況下不需要手動建立，nixc會根據配置自動建立

      ```shell
      # 創建 MBR 分區表
      parted /dev/sda mklabel msdos

      # 建立開機分區
      parted /dev/sda --script --align=optimal mkpart primary ext4 1MiB 513MiB
      mkfs.ext4 "${DISK}1"

      # (選用) 建立swap分區
      parted /dev/sda --script --align=optimal mkpart primary linux-swap 513MiB 2.5GiB
      mkswap -L Swap /dev/sda2
      swapon /dev/sda2

      # 建立主分區
      parted /dev/sda --script --align=optimal mkpart primary 2.5GiB 100%
      mkfs.btrfs -L Butter /dev/sda3
      ```

    - 設置
      ```
      boot.loader.grub.enable = true;
      boot.loader.grub.device = "/dev/sda";
      ```

  - 使用 `efi`
    - 條件，磁碟分區需要建立 50M大小的 ESP Boot Partition

      注意，一般狀況下不需要手動建立，nixc會根據配置自動建立

      ```shell
      # 創建 GPT 分區表
      parted "$DISK" -- mklabel gpt

      # 建立開機分區
      parted "$DISK" --script --align=optimal mkpart ESP fat32 1MiB 1GiB
      parted "$DISK" -- set 1 boot on
      mkfs.vfat "$DISK"1

      # (選用) 建立swap分區
      parted "$DISK" --script --align=optimal mkpart Swap linux-swap 513MiB 2.5GiB
      mkswap -L Swap "$DISK"2
      swapon "$DISK"2

      # 建立主分區
      parted "$DISK" --script --align=optimal mkpart primary 2.5GiB 100%
      mkfs.btrfs -L Butter "$DISK"3
      ```

    - 條件，若使用qemu建立nixos-vm，啟動參數需要添加`-bios OVMF.fd`

    - 設置，

      ```
      boot.loader.systemd-boot.enable = true;
      boot.loader.efi.canTouchEfiVariables = true;
      ```

## [configuration.nix] fileSystems 屬性

- 用於掛載，例如，

  將裝置"/dev/disk/by-uuid/02a92a0f-63f6-4976-a2fa-d7b038b202e0"掛載到/nix

  ```
  # 掛載裝置
  fileSystems."/nix" =
    { device = "/dev/disk/by-uuid/02a92a0f-63f6-4976-a2fa-d7b038b202e0";
      fsType = "btrfs";
    };

  # 掛載目錄
  fileSystems."/etc/nixos" = {
    device = "/nix/persistent/etc/nixos";
    options = [ "bind" ];
  };

  # 掛載檔案
  fileSystems."/etc/ssh/ssh_host_ed25519_key" = {
    device = "/nix/persistent/etc/ssh/ssh_host_ed25519_key";
    options = [ "bind" ];
  };

  # 設置 neededForBoot，在 initrd 期間自動掛載
  fileSystems."/var/log" =
    { device = "/dev/disk/by-uuid/3ee93e6f-1277-4b4b-917d-f921dc0ed341";
      fsType = "btrfs";
      options = [ "subvol=log" "compress=zstd" "noatime" ];
      neededForBoot = true;
    };
  ```

- 自動產生掛載的配置，例如，

  - step，手動掛載，$ mount /dev/disk/by-uuid/02a92a0f-63f6-4976-a2fa-d7b038b202e0" /nix

  - step，執行 $ nixos-generate-config --root /mnt 後，建立的 hardware-configuration.nix 會自動添加以下的配置
    
    ```
    fileSystems."/nix" =
      { device = "/dev/disk/by-uuid/02a92a0f-63f6-4976-a2fa-d7b038b202e0";
        fsType = "btrfs";
      };
    ```

## [configuration.nix] system 屬性

- system.stateVersion : nixos系統初次安裝的版本號，用於系統升級時，套件安裝的版本

  system.stateVersion 一般指向初次系統安裝時，nixos 最開始安裝的版本號。

  此版本號用於在系統升級時，在選擇要安裝的套件版本時，
  <font color=blue>選擇與system.stateVersion兼容的套件版本</font>

  例如，nix v23.11 版本下，兼容的 vim 為 v1.0，
  
  當系統升級到 nix v25.11 時，根據 system.stateVersion = "23.11"，
  系統仍然會安裝與 nix v23.11 兼容的 vim v1.0 版，而不會安裝與 nix v25.11 版本兼容的 vim 1.3 版

  system.stateVersion是為了避免因為套件版本的變更而導致系統中某些應用程序出現不兼容或故障的情況。

  注意，此屬性只會影響系統升級時，如何選擇套件安裝的版本，
  <font color=blue>變更 system.stateVersion 的值，並無法升級/降級系統</font>

  ```
  system.stateVersion = "23.11";
  ```

## [configuration.nix] environment 屬性，

- environment.variables
  - 用來設置系統級環境變量
  - 這些變量會寫入全局的環境文件中，透過`$ cat /etc/profile`可以查看
  - [environment.variables源碼](https://github.com/NixOS/nixpkgs/blob/nixos-23.11/nixos/modules/config/shells-environment.nix)

- environment.systemPackages : 用於指定要安裝到系統的套件
  - [environment.systemPackages](https://github.com/NixOS/nixpkgs/blob/nixos-23.11/nixos/modules/config/system-path.nix)
  
  - 若要指定套件版本，可以透過 override 屬性指定，
    - 例如，`(pkgs.vim.override { version = "8.2"; })`，注意，有可能沒有override的屬性，取決於套件本身的定義
    - 例如，`(maven.override { jdk = pkgs.jdk11; })`，變更依賴套件的版本
  
  - 若要查詢套件版本，至[nixpkgs](https://github.com/NixOS/nixpkgs)，
    - 選擇對應的分支，例如，[nixos-24.05](https://github.com/NixOS/nixpkgs/tree/nixos-24.05)
    - 選擇要查詢的套件，例如，[just](https://github.com/NixOS/nixpkgs/tree/nixos-24.05/pkgs/by-name/ju/just)
    - 查看配置檔，例如，[package.nix](https://github.com/NixOS/nixpkgs/blob/nixos-24.05/pkgs/by-name/ju/just/package.nix)
    - 從 package.nix 中可以看到，在 nixos-24.05 會安裝 just v1.28 的版本

- environment.etc.檔案名.text 
  - 在 /etc 目錄下建立檔案
  - 例如，`$ environment.etc."tmux.conf".text`

- environment.pathsToLink
  - 為指定目錄在 /run/current-system/sw 建立連結
  - 例如，environment.pathsToLink = [ /share/fish ]

    會在 /run/current-system/sw/share/fish 中建立一個符號鏈接到 /share/fish 目錄

    /run/current-system/sw/share/fish 是一個實際的目錄，包含了符號鏈接到 /share/fish 目錄，
    使得 /share/fish 目錄下的所有檔案和子目錄，都可以從 /run/current-system/sw 目錄中訪問

- environment.shells
  - 允許用戶使用的shell的列表
  - 無論是否添加，/bin/sh 是永遠允許使用的 shell

- environment.enableAllTerminfo = true
  - 是否安裝所有可用的終端信息（terminfo）輸出
  - 告訴系統如何與文本編輯器或命令行界面進行通信，告訴系統如何與各種終端進行通信，
    對於某些需要特定能力或設置的終端來說可能很有用。
  - 一旦啟用，可以在 fish 的配置中使用 set -g TERM xterm-256color

## [configuration.nix] users 屬性，設置用戶

- 注意
  - 透過 useradd 仍然可以建立用戶，不一定要透過 configuration.nix
  - 利用 configuration.nix 的 users.users 建立的使用者，`預設沒有設置密碼`，
    在 nixos-rebuild 後會無法登入系統，可先利用 root 登入後，再透過 passwd 設置新密碼
  - 使用`$ su - 用戶名`取代`$ su 用戶名`來切換使用者
    
    對於不同的使用者會有不同的環境變數和配置內容，`su - 用戶名`才會重置環境變量、工作目錄和 shell 設定為目標用戶的設定，
    使用`su - 用戶名`可以減少應用執行時出錯的問題
  
- 常用屬性
  - `users.users.<name>.createHome`，是否自動建立 home 目錄，預設為`/home/使用者名`
  - `users.users.<name>.home = "/path/to/home"`，手動指定 home 目錄的路徑
  - `users.users.<name>.useDefaultShell`，是否使用預設的 shell
  - `users.users.<name>.shell`，手動指定要使用的shell
  - `users.users.<name>.packages`，設置使用者可使用的應用，應用有權限問題時，需要添加到此處
  - `users.users.<name>.isSystemUser`，設置當前使用者是否是系統用戶
    - 此選項只有在 uid 未設置時有效
    - 此選項會將 uid 設置為系統使用者級別的 uid ( uid < 1000)
  - `users.users.<name>.isNormalUser`，設置當前使用者是否是一般用戶

  - 設置密碼相關
    - `user.mutableUsers`，是否允許使用者變更密碼
      -  user.mutableUsers = `false`，<font color=red>有風險</font>
        - 不允許使用 useradd|groupadd 命令
        - configuration.nix 設置密碼的相關屬性`才有作用` 
        - 系統啟動時，/etc/passwd 和 /etc/group 的檔案中會根據 users.users 的配置完全替換，
          若 users.users 沒有設置系統使用者，<font color=red>root帳戶就會消失</font>
        - 若 root 消失，可以透過[nixos-enter](https://nixos.wiki/wiki/Change_root)重新設置
           
      - user.mutableUsers = `true`，預設值，<font color=blue>較安全</font>
        - 允許使用 useradd|groupadd 命令
        - configuration.nix 設置密碼的相關屬性`沒有作用`，需要使用者手動設置密碼
        - 系統啟動時，users.users 和 users.groups 設置的用戶，
            會被添加到 /etc/passwd 和 /etc/group 的檔案中

    - `users.users.<name>.password`，
      - 為一個`已經存在的用戶`設置明文密碼，若用戶尚不存在，此屬性無效
      - 注意，使用此配置選項會有洩漏密碼的危險，可改用 users.users.<username>.hashedPassword

    - `users.users.<username>.hashedPassword`，為一個`已經存在的用戶`設置加密密碼

      可利用以下方式建立 hashedPassword，
      
      ```shell
      nix-shell -p mkpasswd
      mkpasswd -m sha-512
      ```

    - `users.users.<name>.initialPassword`，為一個`尚不存在的用戶`設置明文密碼
      - 用於 1_創建新用戶時需要自動設置初始密碼的情況，或者 2_在不允許用戶更改密碼的情況下設置初始密碼
      - 由於用戶尚不存在，透過 initialPassword 可以在`$ nixos-rebuild switch`後，
        為用戶提供一個可登入系統的密碼
      - 若用戶已經存在，此屬性無效
  
    - `users.users.<name>.initialHashedPassword`，為一個`尚不存在的用戶`設置加密密碼

- 範例，[系統] 設置預設shell

  參考，[command shell](https://nixos.wiki/wiki/Command_Shell)

  ```
  users.defaultUserShell = pkgs.zsh;
  ```

- 範例，[系統] 透過 configuration.nix 設置用戶

  ```nix
  users.defaultUserShell = pkgs.fish; # 預設 shell
  users.mutableUsers = true;  # 可使用 useradd
  users.users.ching = {
    createHome = true;    # 建立家目錄
    isNormalUser = true;  # 一般使用者
    useDefaultShell = true; 
    shell = pkgs.fish;
    # 添加到 sudo 群組 
    extraGroups = [ "wheel" ];
    packages = [
      pkgs.fish
    ];
  };
  ```
  
  `$ nixos-rebuild switch` 後
  - 使用`$ sudo - ching`切換用戶
  - 透過`passwd命令`設置密碼

- 範例，設置 root 用戶

  ```nix
  users.users.root = {
    isNormalUser = false;
    initialPassword = "root";
  };
  ```

## [configuration.nix] font 屬性，設置屬性

- [設置字形官方文檔](https://nixos.wiki/wiki/Fonts)

- 注意，若是透過 windows terminal，需要安裝的是 windows terminal 的字形，推薦使用 firacode

- 推薦使用的字形
  - [nerdfont快速瀏覽+下載](https://www.nerdfonts.com/font-downloads)
  - [透過icon查找字形](https://www.nerdfonts.com/cheat-sheet)

- 範例，安裝所有的 nerdfonts，`$ fonts.packages = with pkgs; [ nerdfonts ];`

- 範例，僅安裝nerdfonts字形包中特定的字形，推薦
  
  ```nix
  fonts.packages = with pkgs; [
    (nerdfonts.override { fonts = [ "FiraCode" ]; })
    fira-code
  ];
  ```

## [configuration.nix] security 屬性

- 範例，[系統] sudo 設置為 NOPASSWD
  
  ```
  security.sudo.wheelNeedsPassword = false;
  ```

## [configuration.nix] service 屬性

- 範例，[套件] openssh-service
  
  ```nix
  # 啟用 openssh-server
  services.openssh.enable = true;
  
  # 允許使用 root 帳號登入
  services.openssh.settings.PermitRootLogin = "yes";

  # 允許使用密碼登入
  services.openssh.settings.PasswordAuthentication = true;
  ```

- 範例，[套件] XRDP-service

  ```
  services.xrdp.enable = true;
  services.xrdp.defaultWindowManager = "xfce4-session";
  services.xrdp.openFirewall = true;
  ```

## [configuration.nix] programs 屬性

- 配置套件的三種方法
  - 透過 configuration.nix 對系統套件進行配置
  - 透過 home-manager 對用戶級別套件進行配置
  - 手動建構套件時，透過 nix-overlays 進行自定義的配置，
    參考，[nix-overlays的使用](#configurationnix-nixpkgsoverlays用於套件的自定義構建)

- 配置範例，見 [sw-config-examples](home-manerger/sw-config-examples/)

## [configuration.nix] virtualisation 屬性

- virtualisation屬性用於建立VM和容器的相關配置，包含
  - VM類 : qemu、vmware、virtualbox
  - 容器類 : potman、docker、lxd

- `$ nixos-rebuild build-vm`會讀取 virtualisation 屬性並對配置進行建構，
  若是建構VM，也會建立啟動VM的script

- For qemu

  qemu 相關配置

  ```
  virtualisation.qemu = {
    virtioKeyboard : 啟用 virtio-keyboard 裝置
    package : 指定要啟用的 QEMU packages
    options : 設置qemu啟動vm的啟動選項，用於傳遞啟動選項給 qemu 執行檔
    networkingOptions : 用於設置啟動選項中的網路裝置
    guestAgent.enable : 啟用 Qemu guest agent
    diskInterface : 設置虛擬硬碟使用的介面，例如，"virtio", "scsi", "ide"
    consoles : 將輸出控制台設備清單，透過此屬性傳遞給核心命令列
    drivers."裝置名" = {
      name
      file
      driveExtraOpts
      deviceExtraOpts
    }
  }
  ```

  qemu 相關模組
  - [lib/qemu-common.nix](https://github.com/NixOS/nixpkgs/blob/nixos-24.11/nixos/lib/qemu-common.nix)
  - [nixos/modules/qemu-vm.nix](https://github.com/NixOS/nixpkgs/blob/nixos-24.11/nixos/modules/virtualisation/qemu-vm.nix) : 建構和啟動 qemu 的相關配置
  - [nixos/modules/virtualisation/qemu-guest-agent.nix](https://github.com/NixOS/nixpkgs/blob/nixos-24.11/nixos/modules/virtualisation/qemu-guest-agent.nix)
    - 提供qemu-guest-gent相關的配置
    - qemu-guest-gent是運行在虛擬機內部的守護程序，用於增強宿主機與虛擬機之間的交互和管理能力

## [配置] nix.conf，套件管理器配置

- 位置，`/etc/nix/nix.conf`

- 套用配置，修改 nix.conf 後，需要選擇執行下列其中一種方法，以使設置生效
  - 方法1，重啟 Nix 守護進程，`$ systemctl restart nix-daemon`
  - 方法2，重新登入
  
## 衝突屬性的處理

- 基本概念

  多個nix-module可以修改同一份相同的選項，例如，
  - a.nix 將 service.nginx.enable 設置為 true
  - b.nix 將 service.nginx.enable 設置為 false

  為了解決這個問題，NixOS 引入了幾個函數來為某些選項的值指派優先權(priority)，
  存在衝突時，會使用優先權來確定首選哪個值，優先選擇priority低的配置

- 用於設置 priority 的函數
  - 普通的賦值，priority = 100
  - mkOverride()，`屬性 = mkOverride 優先權值 屬性值`
  - mkDefault()，`mkDefault value = mkOverride 1000 value`
  - mkForce()，`mkForce value = mkOverride 50 value`
  - mkOptionDefault()，建立 default 值，並加 priority 設置為 1500

- 範例，會出現衝突的範例
  
  在下述範例中，foo.alpha 會以賦值的方式重複定義，`定義的方式相同，因此具有同樣的 priority`，
  
  透過`不同的方式設置屬性值`，例如，lib.mkDefault 函數，可以設置不同的 priority 從而避免重複定義的問題

  ```
  # default.nix 
  # 執行 nix-instantiate --eval --strict -A config default.nix

  let
    lib = import <nixpkgs/lib>;
  in
    with lib;
    evalModules {
      modules = [
        # ---- 定義第1個模組 ----
        ( {lib, ...}:{
          options.foo = lib.mkOption {
            type = lib.types.anything;
          };
          
          config.foo = {
            alpha = "hello";    # 第1次定義 foo.alpha
            
            # 解決方式
            # alpha = lib.mkDefault "hello";
          };
        })

        # ---- 定義第2個模組 ----
        
        { foo.alpha = "world";}   # 第2次定義 foo.alpha
      ];

    }
  ```

## minimum nixos 的配置

- 官方提供多種在特殊場景下的配置屬性集合，例如，
  - [建立最小化nixos的系統配置](https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/profiles/minimal.nix)
  
  - [最小化nixos安裝的基本套件](https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/profiles/base.nix)
  
  - [用於graphical需要的系統配置](https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/profiles/graphical.nix)

  - [用於雲端服務器的系統配置](https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/profiles/headless.nix)

  - [用於QEMU的系統配置](https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/profiles/qemu-guest.nix)

- 參考配置
  
  ```
  # https://discourse.nixos.org/t/how-to-have-a-minimal-nixos/22652/4
  imports = [
    <nixpkgs/nixos/modules/profiles/headless.nix>
    <nixpkgs/nixos/modules/profiles/minimal.nix>
  ];

  # 只添加必要的 module
  boot.initrd.includeDefaultModules = false;
  # 手動添加 kernelModules
  boot.initrd.kernelModules = [ "ext4" ... ];
  disabledModules =
    [ <nixpkgs/nixos/modules/profiles/all-hardware.nix>
      <nixpkgs/nixos/modules/profiles/base.nix>
    ];
  ```

## ref

- 在 github 中搜尋 : nixos-config
- [MyNixOs，查詢配置用](https://mynixos.com/)
- [configuration配置範例集](https://nixos.wiki/wiki/Configuration_Collection)

- 配置範例
  - [配置範例 @ nix cookbook](https://jia.je/software/2022/06/07/nix-cookbook/#%E5%B8%B8%E7%94%A8%E9%85%8D%E7%BD%AE)
  - [配置範例 @ toraritte](https://github.com/toraritte/nixos-config/blob/master/configuration.nix)
  - [配置範例 @ bobby285271](https://github.com/bobby285271/nixos-config)
  - [配置範例 @ nickjanus](https://github.com/nickjanus/nixos-config)
  - [配置範例 @ librephoenix](https://gitlab.com/librephoenix/nixos-config)
  - [配置範例含說明](https://github.com/xieby1/nix_config/tree/main)
  - [配置範例 @ mpickering](https://gist.github.com/mpickering/a3be96e06105e58355b5659dffb8c47e)
  - Anatomy of a NixOS Config 
    - [video](https://www.youtube.com/watch?v=YHm7e3f87iY&list=PLAWyx2BxU4OyERRTbzNAaRHK08DQ0DD_l&index=1)
    - [github](https://github.com/EmergentMind/nix-config)

- 設置用戶
  - [User Management](https://nlewo.github.io/nixos-manual-sphinx/configuration/user-mgmt.xml.html)