## agenix 的使用

- agenix 用於管理和加密敏感信息

  預設使用 age 命令來加解密機密檔案，並且可透過nixos配置文件在開機後自動將機密檔案解密，
  確保配置文件中不會洩漏機密，簡化安全管理流程

## 安裝 agenix

- 方法 : 透過 configuration.nix 添加
  
  在 /etc/nixos/configuration.nix 中添加
  
  ```nix
  environment.systemPackages = [ 
    (pkgs.callPackage "${builtins.fetchTarball "https://github.com/ryantm/agenix/archive/main.tar.gz"}/pkgs/agenix.nix" {})
  ];
  ```
  
  執行 nixos-rebuild switch

## agenix-cli 的使用

- step，(必要) 產生用於agenix加解密的公私鑰對
  - 語法 : `$ ssh-keygen -t 類型 -N 密碼 -f 保存位置`
  - 例如，`$ ssh-keygen -t ed25519 -N "" -f ~/.ssh/mykey`
	
- step，建立 agenix 配置文件 secrets.nix
  
  ```
	mkdir agenixTutor
	cd agenixTutor
	touch secrets.nix
  ```
  
  在 secretes.nix 中
	- 利用 `符號 = 公鑰` 的方式，指定公鑰
	- 利用 `檔案.publicKeys = 公鑰集合` 的方式，指定檔案對應使用的公鑰

  例如，

  ```nix
  let
    # 設置可加密帳號使用的公鑰，代表帳號root加密時，使用以下公鑰進行加密
    root = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEcHhh3gbPGNHDFMWgKLFekEcP7N+JuFBUk7V8nldZmM";
  in { 
    # 設置機密文件，只有指定的文件可以進行加解密
    "mykeys.age".publicKeys = [ root ]; 
  }
  ```

- step，手動建立機密檔案 *.age 
	- `$ agenix -e secret1.age`，
	- -e 代表開啟編輯器，輸入需要被加密的原始內容
	- 注意，檔名必須與 secretes.nix 中指定的檔名一致
	- 該檔案的內容，會根據secrets.nix中提供的公鑰，對secret1.age 的內容進行加密

- 手動解密*.age檔，`$ agenix -d <age檔案> -i 私鑰文件`
  
  - 預設情況下，age 解密時會直接取用`config.services.openssh.hostKeys`指向的私鑰檔案，默認下分別是
    - /etc/ssh/ssh_host_rsa_key
    - /etc/ssh/ssh_host_ed25519_key

  - 若與`加密公鑰對應的解密私鑰`，不是上述檔案的其中之一的檔案時，就需要手動透過`-i`參數指定私鑰的位置

- 修改*.age檔的內容，`$ agenix -e <age檔案> -i 私鑰密碼`

## 在configuration中引用age的加密文件 

- 範例，以加密 ~/.ssh/authorized_keys 為例

  - 介紹，authorized_keys 用於保存 ssh-client 提供的連線用公鑰，
    - 用戶將公鑰保存在sshd-server後，就可以直接使用ssh公私鑰進行連線
    - 當用戶透過`$ ssh-copy-id -i 公鑰文件 登入帳號@遠端ip`傳遞的公鑰，會保存在此檔案中

  - 測試公鑰
    
    若 ~/.ssh/authorized_keys 已保存了的公鑰
    - 將公鑰寫入檔案，`$ echo "ssh-ed25519 AAAAC3...L2oq96 ching@ching" > /etc/nixos/mykeys`
    - 將原始的公鑰刪除，`$ rm -f ~/.ssh/authorized_keys`
    - 在/etc/nixos/configuration.nix中，指定用戶的公鑰，
      
      ```nix
      # /etc/nixos/configuration.nix
      users.users.root.openssh.authorizedKeys.keyFiles = [ /etc/nixos/mykeys ];
      ```

    - 執行配置變更並重啟，`$ nixos-rebuild switch; reboot;`，測試該公鑰能夠正常運作

  - 產生age加解密用的公私鑰對，並存放於 /etc/agenix/identity/age

    ```shell
    mkdir -p /etc/agenix/identity
    ssh-keygen -t ed25519 -N "" -f /etc/agenix/identity/age
    chmod 600 /etc/agenix/identity
    ```

  - 建立 /etc/nixos/secrets.nix 檔案，並建立以下內容

    ```
    let
      # 剛剛建立用於age解密用的公鑰
      root =
        "ssh-ed25519 AAAAC3...L2oq96";

    in {
      "mykeys.age".publicKeys = [ root ];
    }
    ```

  - 建立加密檔案，mykeys.age
    
    透過命令`$ agenix -e mykeys.age`，並輸入原本 /etc/nixos/mykeys 的內容，
    即 `ssh-ed25519 AAAAC3...L2oq96 ching@ching`

  - 修改 /etc/nixos/configuration.nix，加入對加密檔案的引用

    [完整範例](../example-agenix-ref-secret-in-config/configuration.nix)

    ```nix
    {
      imports = [ 
        # 匯入 agenix 模組
        "${
          builtins.fetchTarball
          "https://github.com/ryantm/agenix/archive/main.tar.gz"
        }/modules/age.nix"
      ];

      environment.systemPackages = with pkgs; [
        # 安裝 agenix-cli
        (pkgs.callPackage "${ builtins.fetchTarball "https://github.com/ryantm/agenix/archive/main.tar.gz"}/pkgs/agenix.nix" {} )
      ];

      # 設置age加解密私鑰位置
      # 若不是在預設位置時，需要手動指定正確位置，才能正確將加密文件解密
      age.identityPaths = [ "/etc/agenix/identity/age" ];

      # - 指定加密age管理的加密文件
      # - 指定的加密文件會在開機後，自動解密到 /run/agenix/ 的目錄中
      age.secrets.mykeys.file = ./mykeys.age;

      # - config.age.secrets.mykeys.file 指向`加密文件`，即 ./mykeys.age
      # - config.age.secrets.mykeys.path 指向`解密後的檔案`，即 /run/agenix/mykeys，該檔案是由 agenix 自動解密後建立
      # - 注意，使用以下語句前，確保/run/agenix/mykey檔案存在
      #   在添加 age.secrets.mykeys.file = ./mykeys.age; 後，在使用以下配置前，執行一次 nixos-rebuild switch，
      #   以確保 /run/agenix/mykey 的檔案被建立
      users.users.root.openssh.authorizedKeys.keyFiles = [ config.age.secrets.mykeys.path ];

    }
    ```

  - 注意，/run/agenix/mykeys 只能透過 nixos-rebuild switch 產生，
    且無法透過`agenix -d mykey.age -i ...`手動建立

    如果未產生 /run/agenix/mykeys 前就引用該檔案，

    例如，在未產生/run/agenix/mykeys檔案前，就在配置中使用 users.users.root.openssh.authorizedKeys.keyFiles =  [ config.age.secrets.mykeys.path ]; 語句，
    那麼 nixos-rebuild switch 就會必然失敗，
    因為config.age.secrets.mykeys.path指向/run/agenix/mykeys，而/run/agenix/mykeys尚不存在

    意思是，必須先執行過一次 nixos-rebuild switch 產生 /run/agenix/mykeys 的檔案後，
    才能在配置文件中使用 users.users.root.openssh.authorizedKeys.keyFiles =  [ config.age.secrets.mykeys.path ]; 的配置，
    然後再次執行 nixos-rebuild switch 就不會報錯

## ref

- [agenix，age-encrypted secrets for NixOS and Home manager](https://github.com/ryantm/agenix?tab=readme-ov-file#install-via-niv)