{ config, lib, pkgs, ... }:

{
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
    "${
      builtins.fetchTarball
      "https://github.com/ryantm/agenix/archive/main.tar.gz"
    }/modules/age.nix"
  ];

  boot.loader.grub.enable = true;
  boot.loader.efi.efiSysMountPoint = "/boot";
  boot.loader.grub.device = "/dev/sda"; # or "nodev" forefi only

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  environment.systemPackages = with pkgs; [
    vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    fish
    git

    #agenix
    (pkgs.callPackage "${
        builtins.fetchTarball
        "https://github.com/ryantm/agenix/archive/main.tar.gz"
      }/pkgs/agenix.nix" { })
  ];

  programs.fish.enable = true;

  programs.fish.interactiveShellInit = ''
    set fish_greeting :
  '';

  users.defaultUserShell = pkgs.fish;

  users.users.root.hashedPassword =
    "$6$YJhWFxODdlUXQ2Kr$y9GLDY2jhLDqvXiuZTl6Oah.yh0ot3fqgn1G00w6ksNuAgOYFqhuD9wNlTleml/4dh0nA9eiMbus5G8XfZHrT/";

  # 設置age加解密私鑰位置
  # 預設 age 加解密的公私鑰位置為 
  #   - /root/.ssh/id_rsa
  #   - /root/.ssh/id_ed25519
  # 若不是在此位置時需要手動指定正確位置，才能正確將加密文件解密
  age.identityPaths = [ "/etc/agenix/identity/age" ];

  # 設置開機後自動解密的檔案
  # 預設此檔案會自動解密到 /run/agenix/ 的目錄中
  #age.secrets.mykeys.file = /etc/nixos/mykeys.age;
  age.secrets.mykeys.file = ./mykeys.age;

  # age.secrets.mykeys = {
  #   file = /etc/nixos/mykeys.age;
  #   mode = "0400";
  #   owner = "root";
  #   group = "root";
  # };

  # 未使用 age 加密ssh公鑰
  # users.users.root.openssh.authorizedKeys.keyFiles = [ /etc/nixos/mykeys ];

  # -使用 age 加密ssh公鑰
  # - 注意 config.age.secrets.mykeys.path 指向 /run/agenix/mykeys，是解密後的檔案
  #   此檔案是開機後由 agenix 自動解密
  # - 引用前，需要在未引用加密檔案前(需要在使用以下語句前，確保/run/agenix/mykey檔案存在)
  #   若以上的配置正確，nixos-rebuild switch 會建立 /run/agenix/mykey 的檔案
  users.users.root.openssh.authorizedKeys.keyFiles =
    [ config.age.secrets.mykeys.path ];

  services.openssh.enable = true;
  services.openssh.settings.PermitRootLogin = "yes";
  services.openssh.settings.PasswordAuthentication = true;

  system.stateVersion = "24.05"; # Did you read the comment?

}

