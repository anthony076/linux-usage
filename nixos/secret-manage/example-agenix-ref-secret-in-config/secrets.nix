let
  root =
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEcHhh3gbPGNHDFMWgKLFekEcP7N+JuFBUk7V8nldZmM";

in { "mykeys.age".publicKeys = [ root ]; }
