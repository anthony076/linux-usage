## flake，更容易使用的套件管理

- 為什麼需要 flakes  
  - 軟體源的配置不歸配置文件configuration.nix管理，為了避免不同時間點創建的nixos會有軟體的版本差問題，因此引入flakes
  - flakes提供類似 package.json，利用配置文件來描述套件之間的依賴關係，
  - 執行 `$nixos-rebuild switch`，會 NixOS 會自動優先讀取 flake.nix 而非 configuration.nix，
    並將系統裡的所有套件升級（或降級）到flake.nix中指定的版本

- 使用 flake.nix 的優點
  - 具有 flake.lock 可以控制依賴套件的版本
  - 兼容 nix文件、nix-module、利用一個 flake.nix
    就可以實現版本控制，也可以具有nix-module和nix文件的各種功能

- 基本概念
  - flake 是基於[nix-module](../nix-module/nix-module-overview.md)，具有自己的特殊屬性

  - flake幫助(管理)和(組織Nix表達式之間的依賴關係)，提高了Nix生態系統的可復現性、可組合性和可用性
  
  - 一個 nix-flake 的套件，指的是一個具有 flake.nix 的專案(目錄)，透過 flake.nix 來管理建構時的依賴
    
    flake 是一個檔案系統樹(file-system-tree)，檔案系統樹的根目錄，就是包含flake.nix文件的目錄

    flake.nix 類似 js 的 package.json 定義了當前專案的依賴關係之外，

    比一般套件管理工具更智能的是，除了定義依賴關係，也定義了建構方式，支援以下幾種建構類型
    - 建構可執行的應用程式
    - 建構任意文件
    - 建構系統全局可調用的第三方庫 (安裝到系統，並設置相關的環境變數)
    - 建構系統核心的套件
    - 建構 nix 命令可調用的輸出函數
    - 建構 nix 命令可調用的模板
    - 建構可暫時使用該套件的shell環境

    一個安裝(或建構)可以包含任意數量的 flake 所組成的，這些flake可以是獨立的，也可以是互相調用的

  - [flake 調用的簡化](#概念-使用標識符註冊表和url語法來簡化flake的調用)

- flake.nix 和 nix文件的差異
  - flake 會檢查 flake.nix 檔案中 flake-schema 的有效性

  - 元屬性欄位(metadata fields)，不可以是任意的 nix 表達式 

  - 評估前，整個 flake 目錄都會被複製到 nix-store 的目錄中，可以有效的評估緩存

  - 不允許任何的<font color=red>外部變數、參數、不純的語言值(impure-language-values)</font>
    - 使得Nix表達式的完全可重複性，並且透過擴展，預設產生的建構指令也具有完全可重複性
    - 禁止消費者對結果進行參數化
    - 詳見，[flake與nix文件的差異](https://nix.dev/concepts/flakes.html)

- flake.nix中，四個頂級屬性
  - `description屬性` : 用於描述flake的內容

  - `inputs屬性` : 
    - 一個具有多個屬性的屬性集，用來`指定當前flake的所有依賴`，這些依賴項可以是其他flakes，也可以是Nix包或NixOS模塊
    - 這些依賴項會在被獲取後，作為參數傳遞給 outputs 函數

  - `outputs屬性` : 
    - 一個函數，接受一個屬性集作為參數，這個屬性集包含了所有實現的輸入，並輸出另一個屬性集
    - 輸出屬性集定義了flake提供的Nix值，如包或NixOS模塊

  - `nixConfig屬性` 

- 查詢
  - [output屬性可用的子屬性](https://nixos-and-flakes.thiscute.world/zh/other-usage-of-flakes/outputs)
  - [利用nix repl查詢源碼](commands.md#repl-e)
  - [利用mynixos建立flake.nix(GUI版)](https://mynixos.com/)

## flake如何查找套件實際位址 (flake-registry命令|flake.lock檔案)

- flake 紀錄套件實際位置的地方
  - `位置`，遠端的 flake-registry.json
    - 來源
      - https://github.com/NixOS/flake-registry/blob/master/flake-registry.json
      - https://channels.nixos.org/flake-registry.json
      - 實際來源可透過`$ nix show-config | grep flake-registry`或`$ cat /etc/nixos/nix.conf`查看

    - 條件，首次解析 flake 名稱
      
      例如，`$ nix run foo#bar`，foo 是未在本地註冊的flake名稱(也是套件名稱)

    - 條件，更新註冊表
      
      如果用戶手動更新了自己的 flake 設定或執行了 nix flake update，Nix 會進行遠端查詢，以確保獲取到最新的地址

    - 條件清空本地緩存
      
      如果用戶清除了本地的 Nix 緩存或刪除了 flake.lock 文件，Nix 可能需要重新查詢 flake registry 來獲取 flakes 的位置

  - `位置`，nix registry命令 

    - nix registry list中記錄的套件地址，來源於 Nix 的註冊表系統
      - 全局註冊表，`$ cat /etc/nix/registry.json`
      - 用戶註冊表，`$ cat HOME/.config/nix/registry.json`，只有在使用者第一次使用 nix registry add 命令時才會建立
      - 使用 nix registry list 命令時，會顯示所有的註冊表內容，發生衝突時，用戶註冊表會覆蓋全局註冊表的值
  
    - 狀況，沒有flake.lock檔案時
      
      常用於命令行，例如，`$ nix run nixpkgs#hello`，
      先查找flake-registry命令中是否有註冊，若無，才透過遠端查詢

    - 狀況，當執行`$ nix flake lock --update-input 套件名`命令時，
      獲取 nix registry 註冊的位置中獲取最新的 commit ，並將內容同步更新於 flake.lock 

      也可以使用 `$ nix flake update`，一次性將 flake.lock 中的所有依賴項，都更新到最新的commit

  - `位置`，flake.lock檔案
    
    常用於 flake 專案的目錄中，有 flake.nix 檔案的地方，flake.lock 會自動產生，
    
    在有 flake.lock 的目錄中，優先權 : flake.lock檔案 > flake-registry命令 > flake-registry.json

- flake.lock 中記錄的套件地址，來源於`flake.nix中inputs屬性`，flake.lock和flake-registry命令`有可能指向不同的位置`，而且是允許的

## flake 的限制

- `限制`，flake 使用`封閉式評估`的原则

  <font color=blue>為什麼使用封閉式評估</font>
  使 Flake 能夠在任何環境下，只依賴於 Flake 文件本身的定義，來進行完整的評估和構建，
  這種原則有助於確保 Flake 項目的可移植性和可重現性，無需依賴外部因素。

  <font color=blue>外部變量有哪些</font>
  
  例如`系統環境`、`用戶配置`或`其他 Flake 文件`之外的因素。
  
  這種依賴性會增加 Flake 項目的複雜性和管理難度，因為用戶在不同環境下可能會遇到不同的行為或問題。

  <font color=blue>output屬性函數的外部變量(輸入參數)，會引入的不確定性</font>

  注意，此處的外部變量指的是除了 `self` 和 `inputs` 之外的其他輸入參數

  例如依賴於系統環境變量或其他外部配置，Flake 的評估結果會變得依賴於這些外部變量的值。
  
  這種依賴性會引入不確定性，因為外部變量的值可能會隨著環境的變化而變化，
  導致 Flake 項目的行為不穩定或不可預測。

- `限制`，不允許 flake.nix 像 nix-module 一樣，透過`外部參數或變量`修改配置，
  
  在 flake.nix 檔案中靜態地定義其規則和行為

  <font color=blue>錯誤的使用範例</font>
  ```
  # 错误的 Flake 文件示例 flake.nix
  {
    config = {
      enableStyleCheck = true;
      enableLinting = true;
    };

    # 嘗試使用變量來根據配置啟用或禁用規則
    rules = {
      checkStyle = config.enableStyleCheck;
      lintCode = config.enableLinting;
    };
  }
  ```

  <font color=blue>正確的使用範例</font>
  ```
  # 正確的 Flake 文件示例 flake.nix
  {
    inputs = ["src" "tests"];
    checkInputs = true;

    # 靜態地指定規則
    rules = {
      checkStyle = true;
      lintCode = true;
      analyzeDependencies = false;
    };
  }
  ```

- `限制`，<a id="system-limit">system 或其他外部變數不可直接傳入 output 屬性的輸入參數中，只可以是 self、inputs屬性的變數</a>

  - [為什麼不能傳入 system 變數](https://github.com/NixOS/nix/issues/3843)
  
  - 什麼時候需要處理 system 變數的問題
    
    不是所有的 output 子屬性都需要處理 system，通常是可用於多個平台的屬性，需要額外指定使用何種system，
    例如，

    - `packages.<system>.<name>`
    - `apps.<system>.<name>`
    - `devShells.<system>.<name>`

  - 解法，如何解決 system 變數的問題
    
    - 方法，透過客製化 help-function 

    - 方法，透過第三方的`flake-utils套件`，或`flake.part套件`
      
      使用 flake-utils 或 flake.part 提供的函數解決，會處理 system 變數的問題，並產出 output 屬性集需要的屬性

    - 方法，手動添加 pkgs 的 system 屬性

      若使用 `pkgs = import nixpkgs {};`，nixpkgs 預設是不知道系統當前的 system 的，
      因此會自動調用 `system = builtins.currentSystem` 的語句，

      若是手動為 pkgs 指定system屬性的值，就不會調用 builtins.currentSystem，
      例如，`system= "x86_64-linux"; pkgs = import nixpkgs { inherit system; };`，

      缺點是，使用當前 flake.nix 只適用於當前的 system

    - 方法，改由 `nixpkgs.legacyPackages.x86_64-linux.mkShell` 取代 `(pkgs = import nixpkgs {}).mkShell`

      使用 <font color=green>nixpkgs.legacyPackages.x86_64-linux.mkShell</font> 時，
      - 代表直接從 Nixpkgs 的特定系統版本（在這個例子中是 x86_64-linux）中導入 mkShell
      - legacyPackages 是為了提供對不同系統的包的訪問而設計的，它允許你直接訪問特定系統的包，而不需要手動指定系統

      使用 <font color=blue>(pkgs = import nixpkgs {}).mkShell</font> 時
      - 它不直接提供對特定系統的包的訪問，代表需要在導入 Nixpkgs 之前指定系統，才能正確地導入和使用特定系統的包。

  - 錯誤範例，system 依賴於 builtins.currentSystem 

    ```
    {
      ...

      inputs = {
        nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
      };

      outputs = {
        self,
        nixpkgs,
        # system 依賴於外部的函數
        system ? builtins.currentSystem }:

      let
        pkgs = import nixpkgs { inherit system; };
      in
        {
          ...
        };
    }
    ```

  - 錯誤範例，output屬性的輸入參數不能傳遞 system ，即使是靜態賦值也不行
    
    以下代碼出現 `error: cannot find flake 'flake:system' in the flake registries` 的錯誤
    ```
    {
      description = "DevShell for Python development";

      inputs = {
        nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
      };

      outputs = { self, nixpkgs, system ? "x86_64-linux" }:
      
      let
        pkgs = import nixpkgs { };
      in
      {
        devShell.${system} = pkgs.mkShell {
          buildInputs = with pkgs.python3Packages; [
            python
            setuptools
          ];

          shellHook = ''
            echo hello
          '';
        };
      };
    }
    ```

  - 錯誤範例，即使是不使用 system 變數，直接指定 system 的值也不行

    對於需要使用 system 變數的屬性，內部會調用 builtins.currentSystem 以獲取 system 變數的值
    ```
    {
      description = "DevShell for Python development";

      inputs = {
        nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
      };

      outputs = { self,nixpkgs, }:

      let
        pkgs = import nixpkgs {};
      in
      {
        # 即使不使用 system 變數，內部也會調用 builtins.currentSystem 以獲取 system 變數的值
        devShell.x86_64-linux = pkgs.mkShell {
          buildInputs = with pkgs.python3Packages; [
            python
            setuptools
          ];

          shellHook = ''
            echo hello
          '';

        };
      };
    }
    ```

  - 正確範例，對 pkg|nixpkgs 指定 system 變數的值

    ```
    {
      description = "DevShell for Python development";

      inputs = {
        nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
      };

      outputs = { self,nixpkgs, }:

      let
        # 手動指定 system 屬性的值，使得 pkg 僅適用於指定的 system
        system = "x86_64-linux";
        pkgs = import nixpkgs { inherit system; };
      in
      {
        devShell.${system} = pkgs.mkShell {
          buildInputs = with pkgs.python3Packages; [
            python
            setuptools
          ];

          shellHook = ''
            echo hello
          '';

        };
      };
    }
    ```

    也可以透過 with 語法省略 pkgs 屬性的宣告
    ```
    {
      description = "DevShell for Python development";

      inputs = {
        nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
      };

      outputs = { self,nixpkgs, }:

      let
        system = "x86_64-linux";
      in
        {
          devShell.${system} =
            # 省略 pkgs 屬性的宣告
            with import nixpkgs {inherit system;};

            mkShell {
              buildInputs = with pkgs.python3Packages; [
                python
                setuptools
              ];

              shellHook = ''
                echo hello
              '';

            };
        };
    }
    ```

  - 正確範例，使用 `nixpkgs.legacyPackages.x86_64-linux.mkShell` 取代 `(pkgs = import nixpkgs {}).mkShell`

    ```
    {
      description = "A simple flake with a mkShell";

      inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

      outputs = { self, nixpkgs }: {
        
        # 透過 nixpkgs.legacyPackages 直接存取 x86_64-linux 系統的 mkShell，就不需要在指定 system 參數
        devShell.x86_64-linux = with nixpkgs.legacyPackages.x86_64-linux; mkShell {
          buildInputs = [ curl ];

          shellHook = ''
            echo "Welcome to the development environment!"
            curl --help
          '';
        };
      };
    }
    ```

  - 正確範例，透過`客製化 nix-function`，以正確使用 system 變數

    見[pkg.mkShell](#常用函數)的用法
  
  - 正確範例，使用第三方的 `flake-utils.lib.eachSystem` 簡化 system 變數的使用

    見[pkg.mkShell](#常用函數)的用法

  - 正確範例，使用第三方的 `flake.part` 簡化 system 變數的使用

    - [參考](https://flake.parts/options/devshell)
    - [參考](https://ayats.org/blog/no-flake-utils/)

    ```
    {
      # ...
      inputs.flake-parts.url = "github:hercules-ci/flake-parts";

      outputs = inputs@{nixpkgs, flake-parts, ...}: flake-parts.lib.mkFlake {inherit inputs;} {
        systems = [
          "x86_64-linux"
          "aarch64-linux"
        ];

        perSystem = {pkgs, system, ...}: {
          packages.default = pkgs.callPackage ./package.nix {};

          # flake-parts will reject this, hooray!
          nixosConfigurations.nixos = ...;
        };
      };
    }
    ```

- `限制`，若有 .git 目錄，需要先將 flake.nix 加入追蹤，`$ git add flake.nix`，
  對 flake.nix 的相關命令才不會出現`git-repo ist no yet added to the repo`的錯誤

## 啟用實驗性命令和flakes

- `狀況1`，對可以使用 nixos-rebuild命令的系統，啟用實驗性命令和flakes
  - step，在 configuration.nix 中添加以下
    ```
    nix.settings.experimental-features = [ "nix-command" "flakes" ];

    environment.systemPackages = with pkgs; [
      # Flakes 通过 git 命令拉取其依赖项，所以必须先安装好 git
      git
      vim
      wget
      curl
    ];
    ```

  - step，執行 `$ nixos-rebuild switch` 或 `$ nixos-rebuild switch --flake /path/to/folder#output路徑`
    - 在啟用了 Flakes 特性後，`$ sudo nixos-rebuild switch`會優先讀取 `/etc/nixos/flake.nix`，
      如果找不到，再嘗試使用 `/etc/nixos/configuration.nix` 中指定的套件

- `狀況2`，對無法使用 nixos-rebuild命令的系統，啟用實驗性命令和flakes
  - 使用場景
    - 非 nixos 的 linux 作業系統
    - nixos 提供的 docker container
  
  - step，安裝 nix

  - step，在`/etc/nix/nix.conf`或`~/.config/nix/nix.conf`，添加以下
    ```
    experimental-features = nix-command flakes
    ```

- `狀況3`，臨時性啟用實驗性命令和flakes

  在命令後方添加 `--extra-experimental-features nix-command`，
  例如，nix derivation show --extra-experimental-features nix-command

- 下載並查看 flake.nix 範例
  - 查看所有可用的flakes模板，`$ nix flake show templates`，會從 `https://github.com/NixOS/templates` 獲取官方提供的所有模板
  - 在當前目錄下建立 flake.nix，`$ nix flake init`
  - 根據模板建立 flake.nix，`$ nix flake init -t templates#full`，此命令會將指定模板提供的所有檔案，都下載到當前目錄
  - 透過 `$ cat flake.nix` 查看
  - 注意，若 flake.nix 當前的目錄中有 .git目錄，需要將檔案加入追蹤，詳見，[flake的限制]

- [使用 flake 取代 configuration.nix](#利用-flake-取代-configurationnix-的最小配置)

## [命令] nix flake 命令

- 建立模板，`$ nix flake init --template templates#go`

- 更新 flake.lock 檔案依賴(inputs)，
  - 更新所有 inputs，`$ nix flake update`
    - 根據 cache 或 flake-registry 的值，更新 flake.lock 的內容
    - 適用於所有的套件
    - 適用 nix 2.19 以後的版本

  - 更新指定的 inputs，
    - `$ nix flake lock --update-input input中的套件名`，
      - 根據 cache 或 flake-registry 的值，更新 flake.lock 的內容
      - 適用於指定的套件
      - 適用 nix 2.19 以前的版本
    - `$ nix flake update input中的套件名`，

- `$ nix flake metadata`，查看inputs屬性相關的訊息，包含
  - URL : 含commit，例如，github:edolstra/dwarffs/09f5b6d21ce35fc1de2fd338faa89d0618534ef1
  - Path : 在 /nix/store 中的路徑
  - Revision : commit值
  - inputs : input 套件的來源
  - 例如，
    - `$ nix flake metadata .`，查看本地flake的inputs訊息
    - `$ nix flake metadata github:edolstra/dwarffs`，查看遠端flake的inputs訊息
  - 注意，nix flake metadata 不會包含 outputs 屬性的訊息

- `$ nix flake show`，查看output屬性
  - 查看(指定flake包)中的所有輸出屬性，`$ nix flake show github:nix-community/nix-index`

  - 查看`./flake.nix`中所有的輸出屬性，`$ nix flake show .` 或 `$ nix flake show`

  - 查看指定flake.nix中所有的輸出屬性，`$ nix flake show <flake.nix路徑>`，路徑可以是github，
    例如，`$ nix flake show github:edolstra/dwarffs`

- 建構
  - 僅建構不執行 flake.nix，`$ nix build`
  - 僅建構非flake.nix的檔案，`$ nix build -f 檔名.nix`
  - 建構指定的包，`$ nix build flake.nix#包名` 或 `$ nix build .nix#包名`

    例如，當前 flake.nix 中 output函數定義了兩個包
    - packages.x86_64-linux.debug
    - packages.x86_64-linux.default

    可以透過 `$ nix build .nix#packages.x86_64-linux.debug` 或 `$ nix build .#debug`

- 建構並執行 flake.nix，`$ nix run .#outputs子屬性名`

- 測試，`$ nix flake check`，執行 flake.nix 中，checks 屬性定義的測試

  參考，[testFramwork的使用](../test-framwork/readme.md)

## [屬性] inputs 屬性

- 基本語法，定義依賴，
  
  <font color=blue>inputs.<依賴套件名></font>
  
  <font color=red>注意</font>，此處僅定義了當前flake依賴的套件名

  定義了依賴後，需要透過以下的方法進一步定義 flake 的實際位置
  
- 幾種指定 flake 位置的方式
  - `方法`，透過參數值
    
    <font color=blue>透過 github-repo</font>
    ```nix
    inputs.nixpkgs = {
      type = "github";
      owner = "NixOS";
      repo = "nixpkgs"
    }
    ```

    <font color=blue>透過已註冊的 nix registry</font>
    ```nix
    inputs.nixpkgs = {
      type = "indirect";
      id = "nixpkgs";
    };
    ```

    <font color=blue>對於沒有 flake.nix 的 git-repo 也可能作為依賴源</font>
    ```
    inputs.grcov = {
      type = "github";
      owner = "mozilla";
      repo = "grcov";
      flake = false;
    };
    ```

    <font color=blue>覆寫依賴源</font>
    ```
    # 將 nixops 的輸入依賴 nixpkgs 套件，改為 github:my-org/nixpkgs
    inputs.nixops.inputs.nixpkgs = {
      type = "github";
      owner = "my-org";
      repo = "nixpkgs";
    };
    ```

  - `方法`，URL語法，用於指定位於網路上的flake
    
    URL語法為，`inputs.套件名.url = "<type>:<owner>/<repo-name>/<reference>"`，
    reference 可以是分支名、commit-id、tag

    例如，`inputs.nixpkgs.url = "github:NixOS/nixpkgs"`

    nixpkgs 在 inputs 中被定義後，就可以在後面的 outputs 函數的輸入參數中，使用此依賴項中的內容了

  - `方法`，檔案路徑語法，用於指定本地的flake

    例如，`inputs.mypkg.url = /home/alice/src/mypkg`

## [屬性] outputs 屬性

- 定義 outputs屬性
  - 格式，`outputs = {輸入屬性集} : {輸出屬性集}`

  - 格式，使用 `self` 讀取當前文件中的其他子屬性
    - 語法，`outputs = {self, nixpkgs, ...} : {輸出屬性集}`
    - 特殊的`self輸入參數` : 
      - 功能，代表當前的 flake.nix 中outputs 函數的返回值，利用self可調用當前文件的output屬性
      - 功能，向當前 flake 的源代碼文件夾的路徑
    
    - 範例，self 的使用範例
  
      from [flake-utils](https://github.com/numtide/flake-utils/blob/main/flake.nix)
      ```
      {
        description = "Pure Nix flake utility functions";

        inputs.systems.url = "github:nix-systems/default";

        # 透過 self 將當前文件定義的輸出屬性集，傳遞給輸出函數
        outputs = { self, systems }: {
          lib = import ./lib.nix {
            defaultSystems = import systems;
          };

          templates = {
            # 透過 self 引用 output.templates.each-system 的內容
            default = self.templates.each-system;

            simple-flake = {
              path = ./examples/simple-flake;
              description = "A flake using flake-utils.lib.simpleFlake";
            };

            each-system = {
              path = ./examples/each-system;
              description = "A flake using flake-utils.lib.eachDefaultSystem";
            };

            check-utils = {
              path = ./examples/check-utils;
              description = "A flake with tests";
            };
          };
        };
      }
      ```
  
  - 格式，使用 `@inputs` 將input屬性一次性添加到output屬性函數中
    - 語法，`outputs = {self, nixpkgs, ...}@inputs : {輸出屬性集}`，
    - @ 用於添加額外屬性，詳見 [@屬性集名的用法](nix-language.md#語法-屬性集名添加額外屬性)
    - 為什麼需要 @
    
      例如，輸入屬性集中定義以下的依賴套件， aa、bb、cc 三個套件
      ```
      # 輸入屬性1
      inputs.aa.url = ...;
      # 輸入屬性2
      inputs.bb.url = ...;
      # 輸入屬性3
      inputs.cc.url = ...
      ```

      若不使用 @，需要手動將上述依賴套件的訊息傳遞給output屬性
      ```
      outputs = {self, nixpkgs, aa, bb, cc, ...} : { }
      ```

      透過 @ 可以簡化手動輸入的過程 
      ```
      outputs = {self, nixpkgs, ...}@inputs : { }
      ```

      以上兩者是等效的，透過 @inputs 傳遞依賴套件會更為簡潔

- output屬性集可用的子屬性 (內建的子屬性)
  - `nixosConfigurations屬性`，由 nixos-rebuild switch | home-manager switch | nixos-container 調用
    - 用途，用於配置實際的NixOS系統，或虛擬的NixOS系統(container)，執行以下命令時會使用此屬性
      - `$ nixos-rebuild switch`
      - `$ nixos-rebuild switch --flake .`
      - `$ home-manager switch --flake .`

    - 語法，`nixosConfigurations.主機名 = ... `，可透過 `$ hostname` 查詢主機名

    - 源碼
      - [nixosConfigurations](https://github.com/NixOS/nix/blob/master/src/nix/flake.cc)

  - `app屬性`，由 nix run 調用
    - 用於定義可以直接運行的應用程序，通常是一些可執行文件，並且可以通過 `$ nix run .#<name>` 命令直接運行
  
    - 語法，`apps.<system>.<name> = {子屬性集}`，其中，子屬性集可以是
      - type屬性: 通常為 app
      - program屬性: 指向應用程序的可執行文件路徑
  
    - 範例，定義 foo 執行檔
      ```
      apps.x86_64-linux.foo = {
      type = "app";
      program = "${self.packages.x86_64-linux.foo}/bin/blender";
      };
      ```
  
  - `packages屬性`，由 nix build 調用，用於建構負責產生檔案輸出

    - 用於定義 nix-package，這些package可以是庫、工具，
      或者其他任何可以通過`$ nix build .#<name>`命令構建的項目

      - 使用 Nix Flakes 的標準方式來定義包，提供更好的版本控制和依賴管理
      - packages 輸出通常是一個衍生物(derivation)
      - 這種輸出類型主要用於構建和安裝包，而不是直接運行

    - 語法，`packages.<system>.<name> = {子屬性集}`
    
    - 範例，
      
      以下內容定義了一個名為 hello-repeater 的包，並且通過 nixpkgs 中的 stdenv.mkDerivation 函數創建的
      ```
      {
        description = "Package the hello repeater.";

        outputs = { self, nixpkgs }: {
          packages.x86_64-linux.hello-repeater =
            let pkgs = import nixpkgs {
                  system = "x86_64-linux";
                };
            in pkgs.stdenv.mkDerivation {
              pname = "hello-repeater";
              version = "1.0.0";
              src = pkgs.fetchgit {
                url = "https://github.com/breakds/flake-example-hello-repeater.git";
                rev = "c++-code-alone";
                sha256 = "sha256-/3tT3jBmWLaENcBRQhi2o3DHbBp2yiYsq2HMD/OYXNU=";
              };

              nativeBuildInputs = with pkgs; [
                cmake
              ];
            };
        };
      }
      ```

  - `legacyPackages屬性`，由 nix build 調用
    - 為了兼容舊版本的 Nix 包定義方式，確保 nix-2.4 之前的 nix-package 
      能夠在新的 Nix Flakes 環境中正常工作
        
  - `formatter屬性`，由 nixfmt 或 nixpkgs-fmt 調用
  
  - `lib屬性`，由 nix eval 調用
    - 用於定義一個可調用的輸出函數，這些函數可以在其他地方使用
    - 語法，`lib = {子屬性集}`
    - 範例，利用lib屬性定義一個 sayHello 輸出函數，並在 nix eval 命令中調用 
      ```
      {
        outputs = { self }: {
          lib = {
            sayHello = name: "Hello there, ${name}!";
          };
        };
      }
      ```

      若上述lib屬性定義在當前目錄的 flake.nix 檔案中，
      就可以透過 `$ nix eval .#sayHello "Hello"` 進行調用

      若上述lib屬性定義在 /root/ttt/flake.nix 檔案中
      就可以透過 `$ nix eval /root/ttt/flake.nix#sayHello "Hello"` 進行調用

    - 範例，利用lib屬性定義一個 sayHello 輸出函數，並在 nix-shell 命令中調用

      在 flake.nix 中，透過 lib屬性 定義了一個輸出函數
      ```
      {
        outputs = { self }: {
          lib = {
            sayHello = name: "Hello there, ${name}!";
          };
        };
      }
      ```

      建立一個 nix-expression 文件，shell.nix
      ```
      { pkgs ? import <nixpkgs> {} }:

      let
        # 導入 flake.nix 中所有的輸出屬性
        flake = import ./flake.nix;
      in
        # 利用 mkShell 建立shell環境
        pkgs.mkShell {
          # 將 lib 中定義的 sayHello 函數添加到 shell 中
          buildInputs = [ flake.lib.sayHello ];
        }
      ```

      透過 nix-shell 啟動特定的 shell，`$ nix-shell shell.nix`，
      在這個 shell 環境中，就可以直接調用 sayHello 函數

  - `templates屬性`，由 nix flake init -t 調用
    - 用於定義模板，且該模板可以通過`$ nix flake init -t <flake名>#<name>`命令進行調用

    - 語法，`templates.<name> = {子屬性集};`，其中，子屬性集包含
      - description : 當前模板的描述
      - path : 要複製的目錄的路徑
      - welcomeText : 當用戶基於此模板初始化新的 flake.nix 時顯示的 Markdown 文本塊

    - 範例
      ```
      outputs = { self }: {
      templates.rust = {
          path = ./rust;
          description = "A simple Rust/Cargo project";
          welcomeText = ''
            # Simple Rust/Cargo Template
            ## Intended usage
            The intended usage of this flake is...

            ## More info
            - [Rust language](https://www.rust-lang.org/)
            - [Rust on the NixOS Wiki](https://nixos.wiki/wiki/Rust)
            - ...
          '';
      };

      templates.default = self.templates.rust;
      }
      ```

      定義上述模板後，可透過 `$ nix flake init -t templates#rust 進行調用`

  - `devShell屬性|devShells屬性`，由 nix develop 調用，用於建立 devShell 環境
    - 用於使用 nix develop 命令來使用該 Output 創建開發環境
    - 語法，`devShell = derivation;`
    - 語法，`devShells."<system>"."<name>" = derivation;`
    - 語法，`devShells."<system>".default = derivation;`
    - 注意，[system 變數的使用限制](#system-limit)

  - `nixosModules屬性`，用於定義 nix-moduele 
    - 語法，`nixosModules.自定義moduleName = ...`

  - `checks屬性` 或 `nixosTests屬性`，參考，[testing with flake](test-framwork/flake-test.md)

- 要建立outputs子屬性的屬性值，請參考[常用函數](#常用函數)

- 範例，`output = inputs@{ self, nixpkgs, ... } : {輸出屬性集}`，使用屬性集擴展語法，不需要中間屬性集

  解釋方式，將前方中間屬性集 inputs 中的所有屬性展開，並添加到後面的輸入屬性集中

  使用 nix 的`屬性集擴展語法`，將 inputs 屬性集中的所有屬性，都擴展到後方 @{} 的屬性集中，
  在函數中，可以直接使用 self 和 nixpkgs 等屬性，而不需要通過 inputs 這個中間屬性集來訪問當中的屬性

  使用方式詳見，[屬性集擴展語法的使用](nix-language.md#語法-屬性集名添加額外屬性屬性集擴展語法)

## 常用函數

- 基本概念
  
  - 調用執行檔需要指定 `nix-store 的實際路徑`
    
    在構建的過程中，flake.nix 添加的依賴套件(buildInputs屬性中添加的套件)也是被安裝到 nix-store 中的，
    
    若要調用套件，需要透過 ${ /path/to/套件名 } 來獲取套件在 nix-store 中的實際路徑

    例如，若要使用 bash 命令，應該透過 ${nixpkgs.legacyPackages."x86_64-linux".bash} 獲取 bash 安裝在 nix-store 的路徑
    `builder = "${nixpkgs.legacyPackages."x86_64-linux".bash}/bin/bash";`

  - 可以`nix build`，但是不一定能過`nix run`
    - 狀況，輸出的檔案必須是執行檔
    - 狀況，執行命令的執行檔路徑不對，例如，輸出的 *.py 的檔案，但是要執行的 python 執行檔路徑不對
      - 方法，指定正確路徑，`${pkgs.python39}/bin/python` 執行
      - 方法，將 python 添加到環境變數中，`export PATH="${pkgs.python39}/bin:$PATH"`
      - 方法，透過 --command 執行，參考，[compile c project with flake](flake-examples/pkg.stdenv.mkDerivation_compile-c-project.nix)
      
      - 方法，`outputs.apps 屬性`建立應用 + `program 屬性`指明執行路徑
        - 透過 outputs.apps 屬性建立 app-derivation，
        - 透過 program 屬性指定執行檔的實際位置
        - 範例
          ```
          {
            inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";

            outputs = { self, nixpkgs,}:

            let
              system = "x86_64-linux";
              pkgs = import nixpkgs {inherit system;};
            in
            {
              apps.${system}.default =
              let
                serv = pkgs.writeShellApplication {
                  name = "serve";
                  text = ''
                    echo hello
                  '';
                };
              in
              {
                type = "app";
                program = "${serv}/bin/serve";
              };
            };
          }
          ```

  - 可以不透過 flake 執行，不一定要寫成 flake.nix 的格式

    也可以像下面範例寫成`一般的nix表達式`後執行，或在`nix repl`中進行測試，
    但不推薦，缺點是無法像 flake.lock 可以鎖定依賴套件的版本

    <font color=blue>flake.nix</font>的版本
    透過 `$ nix build` 執行
    ```
    {
      inputs = {
        nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
      };

      outputs = { self, nixpkgs }: {
        packages."x86_64-linux".default = derivation {
          name = "simple";
          builder = "${nixpkgs.legacyPackages."x86_64-linux".bash}/bin/bash";
          args = [ "-c" "echo foo > $out" ];
          src = ./.;
          system = "x86_64-linux";
        };
      };
    }
    ```

    <font color=blue>notFlake.nix</font>的版本
    透過 `$ nix-build -A simple notFlake.nix` 執行
    ```
    let
      pkgs = import <nixpkgs> { system="x86_64-linux"; };
    in
    {
      simple = pkgs.stdenv.mkDerivation {
        name = "simple";
        builder = "${pkgs.legacyPackages."x86_64-linux".bash}/bin/bash";
        args = [ "-c" "echo foo > $out" ];
        src = ./.;
        system = "x86_64-linux";
      };
    }
    ```

    透過<font color=blue>nix repl</font>測試
    ```
    repl> pkgs = import <nixpkgs> { system="x86_64-linux"; }
    repl> pkgs.stdenv.mkDerivation {
        name = "simple";
        builder = "${pkgs.legacyPackages."x86_64-linux".bash}/bin/bash";
        args = [ "-c" "echo foo > $out" ];
        src = ./.;
        system = "x86_64-linux";
      }
    repl> pkgs.out
    ```

- `nixpkgs.lib.nixosSystem`，為 nixosConfigurations屬性建立屬性值
  - 可用於本機的系統配置、live-cd-iso的系統配置、建立container的系統配置
  - 子屬性
    - system屬性，用於指定適用的平台

    - modules屬性，用於加載多個 nix-modules

    - specialArgs屬性，用於定義模組的共享輸入參數，
      詳見，[模組系統的輸入參數自動注入](../nix-module/nix-module-overview.md#特性-模組系統的輸入參數自動注入)

    - For container
      - boot.isContainer屬性，用於建立 container
      - 注意，用於 container 時，boot.isContainer 的子屬性必須用於 `outputs.nixosConfigurations.container`
    
  - 源碼，[nixosSystem](https://github.com/NixOS/nixpkgs/blob/master/flake.nix#L22)

  - 測試
    ```
    repl> :lf nixpkgs
    repl> :e lib.nixosSystem # 查看源碼
    repl> :e output.lib.nixosSystem 
    ```

  - 範例，見[利用flake取代configuration.nix](#利用-flake-取代-configurationnix)

  - 範例，見[利用container建立apache-web-server](flake-examples/nixpkgs.lib.nixosSystem_container_apache.nix)
  
  - 範例，見[利用container建立nginx](flake-examples/nixpkgs.lib.nixosSystem_container_nginx.nix)

- `pkgs.mkShell`，為 devShell屬性|devShells屬性，建立shell環境
  
  參考，[pkgs.mkShell的使用](../functions/functions-mkShell.md)

- `pkgs.derivation`，為 package屬性建立derivation輸出，用來配置產生package需要的參數

  參考，[pkgs.derivation的使用](../functions/functions-derivation.md)

- `pkgs.stdenv.mkDerivation`，為 package屬性建立derivation輸出，提供更多工具的建構函數

  參考，[pkgs.stdenv.mkDerivation的使用](../functions/functions-mkDerivation.md)

- `pkgs.writeShellApplication`，用於產生 shell-script 的檔案

  參考，[pkgs.writeShellApplication的使用](../functions/functions-writeShellApplication.md)

- `pkgs.writeText`，用於產生文字檔

- `pkgs.writeShellScriptBin`，

- `pkgs.buildFHSUserEnv`，

## 常用技巧 

- 技巧，使用 forAllSystem 時，利用 `//` 區分不需要使用 forAllSystem 的 derivation 

    例如，foaAllSystem方法建立以下各個system的應用aa
    ```
    apps.x86_64-linux.aa
    apps.x86_64-darwin.aa
    apps.aarch64-linux.aa
    apps.aarch64-darwin.aa
    ```

    若應用bb，只需要建立於 apps.x86_64-linux.aa，就不適合寫在 foaAllSystem 中，
    此時，可以透過 // 加以區分
    ```
    {
      apps = forAllSystem { # 需要多平台的屬性集 } // { # 不需要多平台的屬性集 }
      ...
    }
    ```

- 技巧，利用 `follows屬性`，避免重建建立相同的依賴套件

  - `未使用follows屬性前`

    ```
    inputs = {
      nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
      flake-utils.url = "github:numtide/flake-utils";
      rust-overlay.url = "github:oxalica/rust-overlay";
    };
    ```

    會建立以下的 dervivation: 
    - 由於 nixos 允許多的版本的相同套件同時存在，並以不同的HASH避免衝突
    - 有時候，不同的inputs使用相同的依賴套件使允許的，允許多的版本的相同套件同時存在，會造成建構時間和空間的增加
    ```
    ├───flake-utils: github:numtide/flake-utils/3db36a8b464d0c4532ba1c7dda728f4576d6d073 # 會重複的套件1
    ├───nixpkgs: github:NixOS/nixpkgs/28319deb5ab05458d9cd5c7d99e1a24ec2e8fc4b  # 會重複的套件2
    └───rust-overlay: github:oxalica/rust-overlay/3bab7ae4a80de02377005d611dc4b0a13082aa7c
        ├───flake-utils: github:numtide/flake-utils/c0e246b9b83f637f4681389ecabcb2681b4f3af0 # 會重複的套件1
        └───nixpkgs: github:NixOS/nixpkgs/14ccaaedd95a488dd7ae142757884d8e125b3363  # 會重複的套件2
    ```
  
  - `使用follows屬性後`

    ```
    inputs = {
      nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
      flake-utils.url = "github:numtide/flake-utils";
      rust-overlay = {
        url = "github:oxalica/rust-overlay";
        inputs = {
          nixpkgs.follows = "nixpkgs";  # 代表rust-overlay的依賴nixpkgs，與上層的nixpkgs一致
          flake-utils.follows = "flake-utils";  # 代表rust-overlay的依賴flake-utils，與上層的flake-utils一致
        };
    };
    ```

    會建立以下的 dervivation: 
    ```
    ├───flake-utils: github:numtide/flake-utils/3db36a8b464d0c4532ba1c7dda728f4576d6d073
    ├───nixpkgs: github:NixOS/nixpkgs/28319deb5ab05458d9cd5c7d99e1a24ec2e8fc4b
    └───rust-overlay: github:oxalica/rust-overlay/3bab7ae4a80de02377005d611dc4b0a13082aa7c
        ├───flake-utils follows input 'flake-utils'   # 沿用上層的 flake-utils
        └───nixpkgs follows input 'nixpkgs'           # 沿用上層的 nixpkgs
    ```

- 技巧，自定義nix|flake套件的構建參數

## 利用 flake 取代 configuration.nix 的最小配置

- flake.nix`可以獨立使用`，也可以和`configuration.nix整合在一起使用`

  flake.nix 和 configuration.nix 都是 nix-module，

  利用內建的`nixosConfigurations.主機名`屬性和nixpkgs提供的函數，可將configuration.nix的內容導入flake.nix中，
  或在flake.nix中直接對configuration.nix內容進行修改

- 流程
  - step，建立 `/etc/nixos/flake.nix`

  - step，在 `/etc/nixos/flake.nix` 中導入 configuration.nix 的內容
    
    - `nixosConfigurations.主機名`，主機名可透過`$hostname`查詢
      必須是當前系統的hostname才能正確建構系統

    - `nixosConfigurations.主機名`屬性，是 flake 的 output屬性特有的子屬性，可參考，[output屬性一節](#屬性-outputs-屬性)
  
    - `nixpkgs.lib.nixosSystem，`由 nixpkgs 提供，專門為`nixosConfigurations.主機名`提供屬性值的函數，
      用於讀取configuration.nix模組的內容，並進行實際的nixos系統配置，要使用此函數，必須在 input 指定nixpkgs套件
    
    ```
    #最小配置範例
    {
      description = "A simple NixOS flake";

      inputs = {
        # 類似 nixpkgs = import <nixpkgs> 的效果
        nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
      };

      outputs = { self, nixpkgs, ... }@inputs: {
        
        # 詳見output屬性集可用的子屬性一節

        nixosConfigurations.nixos = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            # 匯入 configuration.nix 的配置內容
            ./configuration.nix
          ];
        };
      };
    }
    ```

  - 若有修改 configuration.nix 的配置內容
    - 方法，`$ nixos-rebuild switch`，預設讀取 /etc/nixos/configuration.nix 的內容
    - 方法，`$ nixos-rebuild switch --flake /path/to/your/flake.nix#your-hostname`，
      用於手動指定 flake.nix 的指定屬性

## [套件] python

- [如何正確地使用python和pip](https://www.reddit.com/r/NixOS/comments/q71v0e/what_is_the_correct_way_to_setup_pip_with_nix_to/)

- [模擬fhs環境，並使用py37和pip](https://gist.github.com/luochen1990/030ddc18716e5b8b14374621be524d9a)

## 調試 flake.nix 的幾種方法

參考，[how-to-debug-config](../how-to/how-to-verify-config.md#調試-flakenix-的幾種方法)

## 手動編譯套件

- [手動編譯 nix 包](https://linux.cn/article-16332-1.html)
- [手動編譯自己的套件](https://linux.cn/article-16332-1.html)

## ref

- [查詢配置文件可用的選項](https://nixos.org/manual/nixos/stable/options)
- [flakes @ nixos-wiki](https://nixos.wiki/wiki/Flakes)
- [flakes @ official-flake-doc](https://nixos.org/manual/nix/stable/command-ref/new-cli/nix3-flake.html)
- [使用 Flakes 配置 NixOS](https://milena-blog.vercel.app/2022/08/07/flake-on-nixos/)
- [flakes @ nix cookbook](https://jia.je/software/2022/06/07/nix-cookbook/#flakes)
- [Nix Flake](https://lantian.pub/article/modify-website/nixos-initial-config-flake-deploy.lantian/)
- [flake.nix編寫教學](https://juejin.cn/post/7165305697561755679)
- [flake+home-manager](https://tech.aufomm.com/my-nixos-journey-flakes/)
- [Nix flakes @ zero-to-nix](https://zero-to-nix.com/concepts/flakes)
- [Why you don't need flake-utils](https://ayats.org/blog/no-flake-utils/)
- [Nix Flakes by Example](https://www.breakds.org/post/flake-part-1-packaging/)

- 系列教學
  - [NixOS與Flakes，非官方的新手指南](https://nixos-and-flakes.thiscute.world/zh/nixos-with-flakes/introduction-to-flakes)
  - [NIX FLAKES SERIES *3](https://www.tweag.io/blog/2020-05-25-flakes/)
  - [Practical Nix flake anatomy: a guided tour of flake.nix](https://vtimofeenko.com/posts/practical-nix-flake-anatomy-a-guided-tour-of-flake.nix/)

- inputs屬性相關
  - [input屬性範例](https://nixos-and-flakes.thiscute.world/other-usage-of-flakes/inputs)

- outputs屬性相關
  - [output屬性範例](https://nixos-and-flakes.thiscute.world/zh/other-usage-of-flakes/outputs)

- 範例集
  - [flake編寫範例](https://github.com/NixOS/templates)
  - [使用 Deploy-RS 批量部署](https://lantian.pub/article/modify-website/nixos-initial-config-flake-deploy.lantian/)
  - [用Nix編譯Rust項目](https://jia.je/software/2022/08/02/rust-nix/)

- help-function
  - [手寫help-function](https://discourse.nixos.org/t/declaring-python-packages-in-flake-nix/37186)
  - [手寫help-function](https://zero-to-nix.com/concepts/flakes)
  - [推薦，手寫help-function+解釋](https://jameswillia.ms/posts/flake-line-by-line.html)

- mkShell
  - [範例](https://gist.github.com/jhillyerd/ee0028f6564ab38fa382592f325d4842)
  - [範例](https://www.youtube.com/watch?v=_8DS6IqqrBA)
  - [Making a dev shell with nix flakes](https://fasterthanli.me/series/building-a-rust-service-with-nix/part-10)

- override|overrideAttrs|overlays
    Overlays 的使用
    https://www.phind.com/search?cache=hhjbmwk6iztsf1jxuq92ti8v

    flake.outputs.overlay
    https://nixos-and-flakes.thiscute.world/other-usage-of-flakes/outputs

    flake-outputs-overlays
    https://nixos-and-flakes.thiscute.world/zh/nixpkgs/overlays

    nixpkgs 中的 override, overrideAttrs, overlays
    https://blog.shuf.io/post/the-override-overrideAttrs-and-overlays-in-nixpkgs#:~:text=overlay%20%E6%98%AFnixpkgs%20%E4%B8%AD%E7%9A%84,%E5%88%B0%20override%20%E6%88%96%E8%80%85%20overrideAttrs%20%E4%BA%86%E3%80%82

    Nixpkgs 的高级用法
    https://nixos-and-flakes.thiscute.world/zh/nixpkgs/intro
    https://nixos-and-flakes.thiscute.world/nixpkgs/overlays

    overlay @ NIXOS-WIKI
    https://nixos.wiki/wiki/Overlays

    SOURCE OF nixpkgs.overlays
    https://search.nixos.org/options?show=nixpkgs.overlays

    overlay @ ryantm
    https://ryantm.github.io/nixpkgs/using/overlays/