# 和 pkg.mkShell_2-1_help-function with devShells.nix 相比
# 本範例不需要添加 default 屬性

{
  description = "DevShell for Python development";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  };

  outputs = { self, nixpkgs, }:

  let
    supportedSystems = [ "x86_64-linux" ];

    forAllSystems = contentFn:
      nixpkgs.lib.genAttrs supportedSystems
      (system: contentFn {
        pkgs = import nixpkgs { inherit system; };
        system = system;
      });

  in {
    devShell = forAllSystems ({pkgs, system} :
      pkgs.mkShell {
        description = "DevShell for Python development on ${system}";
        buildInputs = with pkgs.python3Packages; [
          python
        ];

        shellHook = ''
          echo 'print("hello")' > hello.py
          python hello.py
        '';
      }
    );
  };
}