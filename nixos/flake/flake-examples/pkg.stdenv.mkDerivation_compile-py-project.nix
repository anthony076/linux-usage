
# 待修正錯誤，以下代碼中，compileall 命令會將源碼編譯為 pyc 的檔案，而不是執行檔

# 建立 hello.py
# def main():
#   print("hello")
# if __name__ == "__main__":
#   main()


# default.nix

{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation rec {
name = "hello-world";
version = "1.0";

# 指定源碼位置
src = ./hello.py; # Assuming hello.py is in the same directory as default.nix

buildInputs = [ pkgs.python38 ];

buildPhase = ''
    python -m compileall hello.py
'';

installPhase = ''
    mkdir -p $out/bin
    cp hello.py $out/bin/hello
    chmod +x $out/bin/hello
'';

}
