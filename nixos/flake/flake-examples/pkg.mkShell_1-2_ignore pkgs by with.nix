{
  description = "DevShell for Python development";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  };

  outputs = { self,nixpkgs, }:
  let
    system = "x86_64-linux";
  in
  {
    devShell.${system} =

      with import nixpkgs { system = system; };

      mkShell {
        buildInputs = with pkgs.python3Packages; [
          python
        ];

        shellHook = ''
          echo 'print("hello")' > hello.py
          python hello.py
        '';
      };
  };
}