{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, }:
    flake-utils.lib.eachDefaultSystem (system:
      with import nixpkgs {inherit system;};
      {
        # devShell= mkShell {             for 單系統
        devShells.default = mkShell {
          name = "devshell-${system}";
          buildInputs = with pkgs.python3Packages; [
            python
          ];

          shellHook = ''
            echo 'print("hello")' > hello.py
            python hello.py
          '';
        };
      }
    );
}