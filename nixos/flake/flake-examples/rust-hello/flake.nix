# flake.nix
# use $ nix build，to get the output 
# use $ ./result/bin/rust-hello，to test the result

{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs }:

  with import nixpkgs { system="x86_64-linux"; };
  {
    packages."x86_64-linux".default = stdenv.mkDerivation {
      src = ./.;
      name = "rust-hello-1.0";
      system = "x86_64-linux";
      nativeBuildInputs = [ pkgs.cargo ];
      buildPhase = ''
        cargo build --release
      '';
      installPhase = ''
        mkdir -p $out/bin
        cp target/release/rust-hello $out/bin/rust-hello
        chmod +x $out
      '';
    };
  };

}
