{
  description = "DevShell for Python development";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  };

  outputs = { self,nixpkgs, }:

  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
  in
  {
    devShell.${system} = pkgs.mkShell {
      buildInputs = with pkgs.python3Packages; [
        python
      ];

      shellHook = ''
        echo 'print("hello")' > hello.py
        python hello.py
      '';
    };
  };
}

#構建並進入shell環境，`$ nix develop`