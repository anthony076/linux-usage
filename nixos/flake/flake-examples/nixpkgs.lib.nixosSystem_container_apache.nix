# 建立目錄，$ mkdir myFlake && cd myFlake
# 透過模板獲得 flake.nix，$ nix flake init -t templates#simpleContainer
# 建立 container，$ nixos-container create flake-test --flake /path/to/myFlake
# 啟動 container，$ nixos-container start flake-test
# 測試，$ curl http://flake-test/

{
    inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";

    outputs = { self, nixpkgs }: {

        nixosConfigurations.container = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules =
            [ ({ pkgs, ... }: {
                boot.isContainer = true;

                # Let 'nixos-version --json' know about the Git revision
                # of this flake.
                system.configurationRevision = nixpkgs.lib.mkIf (self ? rev) self.rev;

                # 設置網路
                networking.useDHCP = false;
                networking.firewall.allowedTCPPorts = [ 80 ];

                # 啟用並設置 Apache-web-proxy
                services.httpd = {
                enable = true;
                adminAddr = "morty@example.org";
                };
            })
            ];
        };

    };
}
