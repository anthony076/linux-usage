# 和 pkg.mkShell_2-2_help-function with devShell.nix 相比
# - nixpkgs.lib.genAttrs 是一個遞歸函數， genAttrs :: [ String ] -> (String -> Any) -> AttrSet，利用遞歸函數的特性，分兩次傳遞輸入參數
# - 此方法的 forAllSystems 更加簡潔，直接使用 nixpkgs.lib.genAttrs 定義的函數來輸出 devShell 需要的屬性集

{
  description = "DevShell for Python development";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  };

  outputs = { self, nixpkgs, }:

  let
    supportedSystems = [ "x86_64-linux" ];
    # 第1次傳遞輸入參數
    forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
  
  in {
    # 第2次傳遞輸入參數
    devShell = forAllSystems (system :
      with import nixpkgs { system=system; };
      mkShell {
        description = "DevShell for Python development on ${system}";
        buildInputs = with pkgs.python3Packages; [
          python
        ];

        shellHook = ''
          echo 'print("hello")' > hello.py
          python hello.py
        '';
      }
    );
  };
}