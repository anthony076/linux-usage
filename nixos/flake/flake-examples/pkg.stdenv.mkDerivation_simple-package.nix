{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs }:

  with import nixpkgs { system="x86_64-linux"; };
  {
    packages."x86_64-linux".default = stdenv.mkDerivation {
      name = "simple";
      builder = "${nixpkgs.legacyPackages."x86_64-linux".bash}/bin/bash";
      args = [ "-c" "echo foo > $out" ];
      src = ./.;
      system = "x86_64-linux";
    };
  };
}