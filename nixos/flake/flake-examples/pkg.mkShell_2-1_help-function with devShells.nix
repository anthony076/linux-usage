{
  description = "DevShell for Python development";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  };

  outputs = { self, nixpkgs, }:

  let
    supportedSystems = [ "x86_64-linux" ];

    # nixpkgs.lib.genAttrs 的原型為: (nixpkgs.lib.genAttrs system名列表 addPkgsWithSystemFn)，
    # 其中，addPkgsWithSystemFn 用於添加已經指定system屬性的pkgs
    # 因此，nixpkgs.lib.genAttrs 會產生 { x86_64-linux = addPkgsWithSystemFn "x86_64-linux"; ... } 的屬性集

    # 若 addPkgsWithSystemFn = (system : import nixpkgs { inherit system; });
    # nixpkgs.lib.genAttrs supportedSystems (system : import nixpkgs { inherit system; }) 會產生
    # { x86_64-linux = (system : import nixpkgs { inherit system; }) "x86_64-linux"; ... } 
    # { x86_64-linux = import nixpkgs { inherit "x86_64-linux"; } ;  ... } 
    
    # contentFn 為下面 devShells 中定義的函數
    forAllSystems = contentFn: 
      # 調用 nixpkgs.lib.genAttrs 函數來產生已經指定system的pkgs屬性和system屬性
      # 產生正確的pkgs屬性和system屬性後，再傳遞給contentFn，以產生 mkShell 需要的內容
      nixpkgs.lib.genAttrs supportedSystems
      (system: contentFn { 
        pkgs = import nixpkgs { inherit system; };
        system = system;
      });
    
  in {
    # 定義 contentFn
    devShells = forAllSystems ({pkgs, system} : {
      default = pkgs.mkShell {
        description = "DevShell for Python development on ${system}";
        buildInputs = with pkgs.python3Packages; [
          python
        ];

        shellHook = ''
          echo 'print("hello")' > hello.py
          python hello.py
        '';
      };
    });
  };
}