# 建立容器，
#   $ nixos-container create gohello --flake .#container
#   得到 host IP is 10.233.1.1, container IP is 10.233.1.2
# 啟動容器，$ nixos-container start gohello
# 測試，$ curl http://10.233.1.2
# 完整範例，https://xeiaso.net/blog/nix-flakes-3-2022-04-07/

{
  description = "DevShell for Python development";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  };

  outputs = { self, nixpkgs, }:

  let
    supportedSystems = [ "x86_64-linux" ];
    # 第1次傳遞輸入參數
    forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
  
  in {
    # 第2次傳遞輸入參數
    devShell = forAllSystems (system :
      with import nixpkgs { system=system; };
      mkShell {
        description = "DevShell for Python development on ${system}";
        buildInputs = with pkgs.python3Packages; [
          python
        ];

        shellHook = ''
          echo 'print("hello")' > hello.py
          python hello.py
        '';
      }
    );
  } // { # 為了與上方的 forAllSystems 分隔，此處採用 // 進行屬性及串接
    nixosConfigurations.container = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
            ({pkgs, ...}: {
                # 創建 container
                boot.isContainer = true;
                networking.hostName = "gohello";
                networking.firewall.allowedTCPPorts = [ 80 ];
                services.nginx.enable = true;
            })
        ];
    };
  };
}



