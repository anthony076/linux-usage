
# 透過 nix develop 運行以下內容

# 若手動輸入，$ buildPhase，得到 `buildPhase: command not found`，因為該環境並未安裝編譯環境，沒有 make 命令

# 若手動輸入，$ eval "$buildPhase"，得到
#   Running build phase...
#   Building project...

# 若手動輸入，$ $buildPhase，得到 "Running build phase..." # 模擬編譯或構建過程，這裡只是示範打印一個訊息 echo "Building project..."

# --------------------------------------

# flake.nix
{
  description = "A simple flake with mkShell and manual buildPhase";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs }: let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
  in {
    # 定義默認包，這樣 `nix build` 或 `nix run` 有一個默認的包
    defaultPackage.${system} = pkgs.hello;

    # 使用 mkShell 創建開發環境
    devShell.${system} = pkgs.mkShell {
      # 定義開發環境所需的依賴項目
      buildInputs = [ pkgs.hello ];

      # 定義 buildPhase，手動執行的構建步驟
      buildPhase = ''
        echo "Running build phase..."
        # 模擬編譯或構建過程，這裡只是示範打印一個訊息
        echo "Building project..."
      '';

      # 添加 shellHook 用於進入開發環境時的提醒
      shellHook = ''
        echo "You have entered the development shell."
        echo "Run 'eval \"$buildPhase\"' to execute the build phase."
      '';
    };
  };
}
```
