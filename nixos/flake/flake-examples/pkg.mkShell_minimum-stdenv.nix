# default.nix

let
  pkgs = import <nixpkgs> {};
  stdenvMinimal = pkgs.stdenvNoCC.override {
    cc = null;
    preHook = "";
    allowedRequisites = null;
    initialPath = [pkgs.pkgsCross.musl64.busybox];
    shell = "${pkgs.pkgsCross.musl64.bash}/bin/bash";
    extraNativeBuildInputs = [];
  };
in
  pkgs.mkShell.override {stdenv = stdenvMinimal;} {
    name = "shell-minimal";
  }