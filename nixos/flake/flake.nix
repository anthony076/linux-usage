# nixpkgs.lib.nixosSystem 函數，來自
# https://github.com/NixOS/nixpkgs/blob/master/flake.nix

# nixpkgs.lib.nixosSystem 函數的輸入參數，來自
# https://github.com/NixOS/nixpkgs/blob/master/nixos/lib/eval-config.nix

# nixosConfigurations from ??
# TODO : 測試不使用 nixosConfigurations 屬姓名來重建系統

{
  description = "A simple NixOS flake";

  # 使用官方提供的 nixos-23.11 的軟件源
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";

  outputs = { self, nixpkgs, ... }@inputs: {
    # nixosConfigurations.nixos 中的 nixos 為自定義的主機名
    nixosConfigurations.nixos = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        # 導入之前使用的 configuration.nix，使舊的配置文件仍然能夠生效
        ./configuration.nix
      ];
    };
  };
}