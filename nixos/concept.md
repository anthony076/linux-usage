
## [特性] 基本概念

- nix 是套件管理器，nixos 是基於 nix 的聲明式linux-distro

  其他類似 nix 的套件管理器或聲明式linux-distro
  - [lix](https://lix.systems/) : A fork of nix
  
  - guix
  
  - [NuschtOS](https://github.com/NuschtOS)
  
  - [nixbsd](https://github.com/nixos-bsd/nixbsd) : An unofficial NixOS fork with a FreeBSD kernel 

  - [celun](https://github.com/celun/celun)
  
  - [NixNG](https://github.com/nix-community/NixNG) : Lightweight NixOS for containers with multiple non-systemd init systems and a "minimal by default" package set.

- 函數式的管理系統 + 版本控制

  Nix 將每個套件視為一個函數的輸出值，其使用的函式庫則為該函數的傳入值，由於套件間不共用函式庫而避開了相依性的問題

  透過版本控制機制，當使用者安裝|移除套件|修改設定檔時，
  Nix 會自動將當前使用環境設為一個新的版本，出問題時只要退回前一個版本即可解決問題

- 基於nix語言的配置文件，會自動產生對應的系統配置文件，並根據新建立系統配置文件產生新的系統快照，
  確保系統的狀態可以被重現，使得系統管理和部署變得更加簡單和可靠

- 基於nix語言的配置文件，nixos允許多個版本的同一個軟體包同時存在，可以為每個用戶或應用程序指定特定版本的軟體包

- 在 nixos 中，`套件是被隔離`並儲存在單獨的目錄中，並使用符號連結，當升級套件時，NixOS 會調整符號連結來定位新的套件，但不會刪除舊的

- nix-lang 用於 nix文件(*.nix)的編寫，依照nix文件內容可以區分為
  - `模塊文件` : 有特定屬性，包含 imports、options、config 等屬性，提供配置文件可使用的選項
  - `配置文件` : 沒有特定的屬性，根據導入的模塊文件來配置系統或套件，
  - `flake套件配置檔` : 有特定屬性，包含 description、inputs、outputs、nixConfig 等屬性，提供建構套件的依賴、建構方式、環境變數等訊息

## [特性] nixpkgs 是官方套件集合，NUR 是用戶管理的套件集合

- [nixpkgs-repo](https://github.com/NixOS/nixpkgs)

- [nur-repo](https://github.com/nix-community/NUR)

## [特性] 不符合傳統Linux的FHS規範

- nixos把所有軟件都安裝到`/nix/store/`目錄下，對於每次新的配置所產生新的generation，都會透過軟連結連接到特定的套件版本

- 對於需要FHS架構的套件，需要透過nix文件才能正確建立符合FHS的套件

- 對於FHS規範的文件，實際上大部分都是軟連結
  - 例如，`/etc -> /etc/static -> /nix/store`
  - 例如，`/bin/sh -> /nix/store/[Hash]-bash`
  - 例如，`/usr/bin/env -> /nix/store/[Hash]-coreutils`
    
    ---

  - 例外，/boot，和FHS一樣，用於存放 bootloader
  - 例外，/home 和 /root，和FHS一樣，用於使用者的家目錄
  - 例外，/var，和FHS一樣，用於存放系統套件的數據文件
  - 例外，/nix，nixos 的所有套件包

## [特性] 系統快照(Generations) 和 套件衍生物(Derivations)

- 構建專案流程 : nix表達式 -> 產生衍生物(derivation) -> 產生最終的構建目標

- 衍生物(derivation)是最終構建目標的中間產物
  - 類似編譯源碼會產生中間產物，exe/dll/staticlib 才是最終產物
  - 透過快照和衍生物，使用者可以隨時回滾系統或套件
  - 透過衍生物可以確保不同的平台都能到相同的結果產物

- <font color=blue>Generations</font>

  Generations 代表了系統配置的一個快照。每次您使用 nixos-rebuild 命令來應用新的系統配置時，都會創建一個新的快照稱為 generation，

  generation 包含了系統配置的所有衍生物，以及它們的依賴性。

  generation 可以被添加到 boot-menu 中，也可以透過 nixos-rebuild 命令來激活、回滾或比較不同的 generation

- <font color=blue>Derivations</font>

  nix 對 nix表達式進行求值的過程稱為評估，對於`會產生實際檔案的評估`，在完成評估後會產生 derivation 檔案(*.drv)，
  derivations 用於描述輸入到輸出的構建過程，所需要的依賴|環境|輸出和其他必要的訊息
    
  在評估的過程中，nix會驗證nix表達式的正確性，若正確無誤就會產生*.drv檔，
  drv檔會保存所有必要訊息並建立套件的版本

  建立 drv 檔後，就可以透過各種建構命令來產生實際的輸出，
  nix會以目錄的形式將實際輸出結果複製到 nix-store 中，該目錄名會包含一組獨立的HASH值，
  用於區別不同版本的套件，目錄的內容就是實際輸出的結果
  
  並在當前目錄下建立result目錄，reuslt目錄下會建立軟連結，用於指向nix-store的實際輸出位置

  參考，[derivation 的實際操作](how-to/how-to-verify-config.md)

## [特性] 區分新式命令和舊式命令

- 具有 `nix-命令名` 通常是舊式命令，`nix 命令名` 通常是新式命令或實驗性命令，
  新式命令通常依賴於 flake

- 啟用新式命令前，需要[啟用flake](commands_flake.md#啟用實驗性命令和flakes)

## [特性] 套件的屬性路徑和屬性名

- 在套件集合 nixpkgs 中，每一個套件都有唯一的屬性名，該屬性名是一個字符串，用於在nix-expression中識別特定套件的唯一識別字符串

- 屬性路徑是一個由點分隔的`屬性名序列`，用於在 Nix 表達式中唯一標識一個包
  - 例如，xorg.xorgserver 就是一個屬性路徑，它指向 xorg 屬性下的 xorgserver 套件

- 使用者可以透過屬姓名來安装|升级|查询套件，例如，`$ nix-env -f '<nixpkgs>' -iA nixos-install-tools`
  - `-f '<nixpkgs>'`，代表從路徑表達式`<nixpkgs>`找到nixpkgs變數對應的檔案路徑
  - `-iA nixos-install-tools`，在上述路徑中，查找名为 nixos-install-tools 的套件定義，並根據此定義安裝套件

- 透過屬性路徑和屬性名，使用者就不需要記住包的具體文件路徑或其他複雜的信息

- 屬性路徑和屬性名是利用路徑表達式`<expre>`求值得到的結果，詳見，nix-language 的[路徑表達式](nix-language.md#路徑表達式expr)

## [特性] flake.nix，和 nix-module 的差異

- 兩者都是透過[nix-language](nix-language.md)編寫

- [nix-module](nix-module/nix-module-overview.md)
  具有用於配置的特殊屬性(options、config)的nix文件
  
- [flake.nix](commands_flake.md)
  是用於描述依賴關係的nix文件，具有各種用於特殊功能的特製屬性，
  可用於建構套件、配置環境、執行命令，... 等操作

## [特性] 使用(標識符註冊表)和(URL語法)來簡化調用

- 為什麼需要URL語法和標識符註冊表

  傳統上，可以透過類似以下的語法來指定flake的位置(flakerefs)，
  - github:edolstra/dwarffs
  - git+https://github.com/NixOS/patchelf

  缺點是URL可能會非常長，因此flake引入了一個符號標識符的註冊表，用於簡化長URL語法並使用更短的名稱

  例如，標識符 nixpkgs，代表 https://github.com/NixOS/nixpkgs，
  透過此方式可以使用簡短的標識符來代替完整的URL

  利用已註冊的標誌符，就可以在`配置文件`或`命令行`中簡化對 flake 的引用
  
  例如，在命令列中，以下三個命令是等效的
  - `$ nix shell http://github.com/NixOS/nixpkgs#cowsay --command cowsay Hi!`
  - `$ nix shell github:NixOS/nixpkgs#cowsay --command cowsay Hi!`
  - `$ nix shell nixpkgs#cowsay --command cowsay Hi!`
    - 只有在 nix registry 中註冊的符號才能這樣使用
    - 可透過`$ nix registry list`查看或手動建立

  例如，在命令列中，
  - `$ nix build github:NixOS/nixpkgs#hello`，
    - github:NixOS/nixpkgs 是 nixpkgs 這個 flake 的完整位置
    - hello是 nixpkgs套件中，由 flake.nix 定義的輸出參數

  例如，在配置文件中，
  > inputs.nixpkgs.url = "github:NixOS/nixpkgs";

- URL語法，`<type>:<owner>/<repo-name>/<reference>#output屬性中定義的子屬姓名`，
  
  因為 output 屬性實際上是一個輸出函數，可以直接被調用，子屬性通常對應一個可執行的命令或應用程式

- URL語法的實際路徑，例如，`github:nix-community/nix-index#nix-index`

  在進行安裝時，實際上會找到 `https://github.com/nix-community/nix-index/flake.nix`，
  並在 flake.nix 檔案中找到 output 屬性，並根據 output 屬性中，會對`#nix-index`的建構方式和使用方式加以定義

- URL語法的實際使用場景
  - 範例，用於配置文件中，可以簡化依賴套件的位置，例如，`inputs.nixpkgs.url = "github:NixOS/nixpkgs"`
  - 範例，用於命令行，可以簡化要安裝的套件，例如，`$ nix run github:nix-community/nix-index#nix-index`

## [特性] 系統升級後，套件依舊會保持在舊版本

參考，[system.stateVersion](configuration.md#configurationnix-system-屬性) 的說明