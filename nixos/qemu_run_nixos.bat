qemu-system-x86_64.exe ^
-m 4096 -smp 4 ^
-accel whpx,kernel-irqchip=off ^
-machine vmport=off ^
-display sdl ^
-drive file=nixos_disk.qcow2,format=qcow2 ^
-net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22

:: 透過 ssh -p 2222 root@localhost 連接
:: 透過 scp -p 2222 -r root@localhost:/root/ttt/ttt.png . 傳遞檔案