## 用於 debug 的內建函數

- builtins.trace

- builtins.tryEval，嘗試評估一個表達式，並返回一個包含成功狀態和結果的集合

  ```
  let
    result = builtins.tryEval (someExpression);
  in
  {
    myValue = if result.success then result.value else "Error occurred";
  }
  ```

- builtins.typeOf，檢查變量類型

  ```
  let
    myType = builtins.typeOf myValue;
  in
  {
    debugType = builtins.trace "Type of myValue: ${myType}" myType;
  }
  ```

- builtins.toJSON，可用於將屬性集轉換為字串

- builtins.toString，可用於將各種類型轉換為字串

- builtins.typeOf，查看物件類型

- builtins.mapAttrs (pkgs.lib.mapAttrs)，將指定屬性集帶入map函數中，以重新產生新的屬性值

  - [mapAttrs官方文檔](https://nix.dev/manual/nix/2.18/language/builtins.html#builtins-mapAttrs)
  
  - 語法，`mapAttrs map函數 原屬性集`，map函數具有以下的形式 : (屬姓名: 屬性值: 映射代碼塊)

    範例，

    ```nix
    nix-repl> builtins.mapAttrs (name: value: value * 10) { a = 1; b = 2; }
    { a = 10; b = 20; }
    ```

  - 高階語法，`mapAttrs map函數 原屬性集`，
    map函數具有以下的形式 : (原屬性集提供的屬性名name: 原屬性集提供的屬性值as: 函數調用傳入的屬性集as2 : 映射代碼塊)

    其中，
    - name 和 as 由原屬性集中的子屬性提供
    - as2 外部調用時提供

    範例

    ```nix
    let
      # 原始屬性集
      distros = {
        ubuntu = { version = "22.04"; arch = "x86_64"; };
        debian = { version = "11"; arch = "x86_64"; };
      };

      # 用戶自定義屬性集，用於覆蓋原始屬性集的arch屬性，並新增features屬性
      customAttrs = { arch = "aarch64"; features = "secureBoot"; };

      # 產生新值屬性值的處理函數
      # - version 和 arch 來自原始屬性集
      # - features 來自用戶自定義屬性
      makeImage = distro:
        "Image for ${distro.version} on ${distro.arch} with ${distro.features or "no features"}";

      # 映射函數 (name: as: as2: makeImage (as // as2)) 
      diskImageFuns = lib.mapAttrs (name: as: as2: makeImage (as // as2)) distros;
    in
      # 調用 diskImageFuns，將自定義屬性傳入，as2 = customAttrs
      diskImageFuns customAttrs
    ```

    在上述範例中，diskImageFuns = lib.mapAttrs (name: as: as2: makeImage (as // as2)) distros; 返回的是一個屬性集，

    為什麼可以透過 diskImageFuns customAttrs 的形式，將 customAttrs 傳遞給 as2

    實際上 diskImageFuns 返回的屬性集合，是一個`屬性值為函數`的屬性集合，或稱為一個包含函數的屬性集
    
    例如，在未傳遞customAttrs前，diskImageFuns 屬性集的結果如下

    ```
    {
      ubuntu = as2 : makeImage (distros.ubuntu // as2);
      debian = as2 : makeImage (distros.debian // as2);
    };
    ```

    因此，寫成 diskImageFuns customAttrs 實際上是在調用這些函數，並將 customAttrs 傳遞給輸入參數as2，
    使得 diskImageFuns 的最終結果為

    ```
    {
      ubuntu = makeImage (distros.ubuntu // customAttrs);
      debian = makeImage (distros.debian // customAttrs);
    };
    ```
