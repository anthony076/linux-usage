## 常用函數 

- 函數調用路徑是可以簡化的

  例如，https://github.com/NixOS/nixpkgs/blob/master/lib/options.nix 定義的mkOption()函數，

  實際上可以透過以下方式調用
  ```
  lib = import <nixpkgs/lib>
  lib.mkOption()
  ```

  因為 https://github.com/NixOS/nixpkgs/tree/master/lib 的目錄中有 flake.nix 代表該目錄是一個 flake 的套件包，
  所有lib套件包中定義的函數都會被 flake.nix 載入，並開放給外部直接調用，

  因此，不需要`import <nixpkgs/lib/option>`才能調用，只需要 `import <nixpkgs/lib>` 即可

- pkgs參數和lib參數的獲取方式
  - 方法，`pkgs = import <nixpkgs> {}`
  - 方法，`lib = import <nixpkgs/lib>`
  - 兩者的差異，參考，[import 的行為差異](../nix-module/nix-module-overview.md#import-命令--用於匯入nix-expr文件的屬性集合或對nix-expr文件進行求值)

## 查詢函數的使用方法

- 方法，直接在[nixpkgs-repo](https://github.com/NixOS/nixpkgs)中搜尋

  例如，要尋找 pkgs.rustPlatform.buildRustPackage函數 中，cargoDeps 屬性的用法，

  直接搜尋`"cargoDeps ="`

- 方法，透過 nix repl
  
  - <font color=blue>不是每個函數都有文檔說明</font>
  - 支援 tab 自動補全，可快速查詢有哪些函數可用
  - `:e 函數名`，可以打開函數源碼
  - `:t 對象`，可以查看對象的類型
  
  - 注意，nix repl 的自動補全`只能用於屬性集`，如果是函數就無法使用自動補全，例如，
    - `pkgs = import <nixpkgs>`，返回的函數，無法使用自動補全
    - `pkgs = import <nixpkgs> {}`，返回的屬性集，可以使用自動補全
    - `lib = import <nixpkgs/lib>`，返回的是屬性集，可以使用自動補全
    - `lib = import <nixpkgs/lib> {}`，返回的是函數，無法使用自動補全
    - 可以在 nix repl 中，透過`:t 變數`確認返回值是什麼類型

  ```
  nix-repl> pkgs = import <nixpkgs> {}
  
  # 顯示函數文檔，不一定有
  nix-repl> :doc pkgs.mkShell 

  # 打開函數源碼，一定有
  nix-repl> :e pkgs.mkShell
  ```

- 方法，直接透過`nix edit nixpkgs#函數名查詢`，例如，`nix edit nixpkgs#mkShell`

- 方法，若函數是定義在 configuration.nix 中

  可以透過 nixos-option 查詢當前值、出處、和描述，參考，[nixos-option的使用](../commands.md#命令-nix-show-config顯示-nixconf-的配置)

- 方法，透過第三方的 [noogle](https://noogle.dev/) 查詢函數用法

## [技巧] 有效的缺省輸入參數調用

以下函數雖然有三個輸入參數，但調用時只提供兩個輸入參數，但調用依然是有效的

```nix

# 函數定義
withExtraPackages = pkgs: expr: args: let
    packages = args.extraPackages or [];
  in expr (args // { extraPackages = packages ++ pkgs; });

# 函數調用
withExtraPackages basePackages diskImageFuns.debian12x86_64
```

調用withExtraPackages函數時，雖然只提供了 pkgs 和 expr 兩個參數，對於函數中使用 args 的兩個變數
- `packages = args.extraPackages or [];`，若 args 為空，`packages = []`
- `args // { extraPackages = packages ++ pkgs;`，若 args 為空，`extraPackages = [] ++ pkgs = pkgs`，`{} // pkgs = pkgs`
- 若 args 為空，對於上述兩個表達式都不會造成空值，因此不影響函數的運行，args 作為可選輸入參數，是可以省略的

## [函數] pkgs.stdenv.mkDerivation，建構函數，用於產生drv檔，並可透過 nix-build 或 nix build 產生實際檔案

參考，[pkgs.stdenv.mkDerivation的使用](functions-mkDerivation.md)

## [函數] pkgs.callPackage，可自動注入pkgs的參數化構建Derivation工具函數

參考，[pkgs.callPackage的使用](functions-callPackage.md)

## [函數] pkgs.lib.mkIf

lib.mkIf : `屬性 = mkIf true { ... }`，若 mkIf 後方為 true，就建立後方的屬性集

## [函數] pkgs.lib.types.attrsOf，定義屬性集合元素的類型

- lib.types.attrsOf : 若屬性是一個屬性集合，lib.types.attrsOf 定義屬性集合元素的類型
  
- 範例

  ```
  settings = lib.mkOption {
      # settings 是一個屬性集合
      default = { };
      
      # 將 settings 中的元素類型，指定為字串
      type = lib.types.attrsOf lib.types.str;
    };
  ```

## [函數] pkgs.lib.mapAttrsToList，將屬性集合中的元素，轉換為 list

- 語法，`lib.mapAttrsToList (屬性名變數:值變數:返回值表達式) 屬性集變數`

- 範例，
  ```nix repl
  repl> lib = import <nixpkgs/lib>
  repl> foo = { x="aa"; y="bb";}
  repl> lib.mapAttrsToList (name : value : name + value) foo
  [ "xaa" "ybb" ]
  ```

## [函數] pkgs.linkFarm，用於建立軟連結的檔案

- 語法:
  ```
  pkgs.linkFarm drv檔案名 [
    { name="軟連結名"; path="軟連結指向的路徑"; }
    { name="目錄名"; path="軟連結指向的路徑"; }
  ]
  ```

- 範例

  <font color=blue>建立nix-module，mylink.nix</font>
  ```
  # mylink.nix
  {lib, pkgs, ...}:{

    options.mylink = lib.mkOption {
      type = lib.types.package;
      
      # 建立軟連結
      default = pkgs.linkFarm "mylink" [
        {name="link1"; path="/root/default.nix";}
      ];
    };
  }
  ```

  <font color=blue>建立模組評估檔，main.nix</font>
  ```
  let
    pkgs = import <nixpkgs>{};
    lib = pkgs.lib;
  in
    lib.evalModules {
      modules = [
        ./mklink.nix
      ];
      specialArgs = pkgs;
    }
  ```
  
  - 測試，`$ nix-instantiate -A config.mylink main.nix`，檢查是否會產生 *.drv 檔案
    > 產生 /nix/store/[hash]-mylink.drv  

  - 建構，`$ nix-build -A config.mylink main.nix`，產生實際的軟連結檔案
    > 產生 /nix/store/[hash]-mylink

  - 檢查，`$ cd result && ls -al`
    > 產生 link1 -> /root/default.nix

## [函數] pkgs.lib.evalModules，用於加載nix-module的內容

參考，[nix-module的使用](../nix-module/nix-module-overview.md)

## [函數] pkgs.lib.optionalString，用於選擇性輸出特定字串

- 語法 : `pkgs.lib.optionalString <條件> <條件成立時返回的字符串>`

- 範例

  ```
  pkgs.lib.optionalString true "abc"，返回 "abc
  pkgs.lib.optionalString false "abc"，返回 ""
  ```

## [函數] pkgs.runCommand，在建構的過程中執行命令

參考，[pkgs.runCommand的使用](functions-runCommand.md)

## [函數] pkgs.mkShell，建立 shell 環境

- pkgs.mkShell 是 stdenv.mkDerivation 的變體

- [源碼](https://github.com/NixOS/nixpkgs/blob/master/pkgs/build-support/mkshell/default.nix)

- mkShell的使用參數
  - description屬性 : 對shell環境的自定義描述
  - packages屬性 : 指定要包含在開發環境中的nix包，這些被安裝的套件會自動被添加到PATH變數中
  - inputsFrom屬性 : 被 packages 中依賴的套件
  - buildInputs屬性 : 指定需要在開發環境中可用的包，`主要用於編譯相關的包`
  - shellHook屬性 : 進入開發環境時自動執行的腳本或命令，可以用於設置環境變量
  - nativeBuildInputs屬性 : 和 buildInputs 相似，但特別用於構建系統在本地執行時需要的依賴，
    通常是編譯工具或腳本工具
  - unpackPhase、patchPhase、configurePhase、buildPhase、checkPhase、installPhase屬性
    - 功能 :自定義建構時每個階段要執行的命令
    - 每個階段實際進行的操作，參考，[nixpkgs/pkgs/stdenv/generic/setup.sh](https://github.com/NixOS/nixpkgs/blob/master/pkgs/stdenv/generic/setup.sh)

    - `unpackPhase` : 解壓縮源代碼包
      - 解壓縮 src 屬性定義的源代碼文件
      - 若沒有定義此階段，
        - src 若是已經解壓的目錄或文件，則該階段會被跳過
        - 否則，nix 會自動檢測壓縮檔的類型並解壓到當前目錄
  
    - `patchPhase` : 應用補丁對源代碼進行修改
      - 應用補丁，修改源代碼中的某些文件
      - 預設不做任何操作，除非存在patches清單
      - 有提供patches清單的情況下，會透過`$ patch`命令套用補丁
  
    - `configurePhase` : 配置構建環境和參數
      - 根據系統環境生成合適的構建配置文件
      - 未定義 configurePhase，nix 會嘗試自動使用`./configure`或 cmake 等工具
  
    - `buildPhase` : 編譯或構建軟件
      - 運行構建系統的構建命令，如 make、ninja 或 cmake --build 等
      - 在未指定 buildPhase 且 Makefile 存在時，會自動執行 `$ make` 命令
      - nix無法偵測其他的建構系統，除make外的建構系統，需要手動顯示的指定buildPhase的內容

    - `checkPhase` : 運行單元測試或集成測試
      - 預設會執行 `$ make check`，以進行自動測試

    - `installPhase` : 將構建產物安裝到目標目錄
      - 目標目錄由 $out 環境變量指定，通常是 /nix/store 中的一個目錄
      - 預設執行`$ make install`命令

    - 自動執行
      - 滿足以下條件，這些階段才會按預定順序自動執行
      - `條件1`，使用 nix-build 或 nix develop 或 nix-shell
      - `條件2`，調用 stdenv.mkDerivation() 來構建 Nix 表達式時，
      - 執行順序 : unpackPhase > patchPhase > configurePhase > buildPhase > 
        checkPhase > installPhase > fixupPhase > installCheckPhase > distPhase

    - 手動執行
      - 沒有滿足自動執行條件時，必須手動調用才會執行
      - 方法，輸入 `$ buildPhase`，會調用預設的 make 命令
      - 方法，輸入 `$ eval "$buildPhase"`
      - 方法，輸入 `$ $buildPhase`

- [mkShell範例集](../flake/flake-examples/)

- 範例，[手動執行buildPhase範例](../flake/flake-examples/mkShell-manully-call-buildPhase.nix)

## [功能] 優先權相關函數

詳見[衝突屬性的處理](../nix-module/nix-module-overview.md#衝突屬性的處理)

## [功能] 寫入檔案相關函數

- pkgs.writeText : 將字串寫入檔案
  
  語法 : `pkgs.writeText "檔名" ''文件內容''`

- pkgs.writeTextFile : 將字串寫入檔案，並提供更多設置選項

  ```
  pkgs.writeTextFile {
    name = "my-script";
    text = ''
        #!/bin/sh
        echo "Hello, World!"
    '';
    executable = true;
    destination = "/bin/my-script";
  }
  ```
  
- pkgs.writeTextDir : 創建一個包含文本內容的文件，並將其放置在指定的目錄中

  語法 : `pkgs.writeTextDir "目錄/檔名" ''文件內容''`

- pkgs.writeScript : 創建一個預設不可執行的腳本文件

  語法 : `pkgs.writeScript "Script檔名" ''Script內容''`

- pkgs.writeScriptBin : 創建一個預設可執行的腳本文件
  - 語法 : `pkgs.writeScriptBin "Script檔名" ''Script內容''`
  - 預設會放建立在 `/nix/store/<store_path>/bin` 的目錄中

## 函數的分類

- 類型，Library Functions : nixpkgs/lib 提供的各種函數

  - lib.modules: 提供 nix-module 相關的函數
  - lib.asserts: 提用於進行斷言和檢查。它提供了一系列有用的函數來驗證程式碼中的條件
  - lib.attrsets: 包含操作屬性集（attribute sets）的函數，例如創建、修改和訪問屬性集
  - lib.strings: 提供了多種字符串操作的函數，如字符串轉換、搜索、替換等
  - lib.versions: 用於處理版本字符串的函數，通常與依賴管理相關
  - lib.trivial: 包含一些簡單但實用的函數，可能包括數學運算、時間處理等
  - lib.fixedPoints: 用於實現明確遞歸的函數，這是一種特殊的函數設計模式
  - lib.lists: 提供了對列表的操作函數，如遍歷、過濾、排序等
  - lib.debug: 用於在開發階段進行調試和診斷的函數
  - lib.options: 專門用於處理NixOS和nixpkgs的選項設置
  - lib.path: 包含操作路徑字符串的函數，如解析、合成等
  - lib.filesystem: 提供了對文件系統的操作函數，可能包括文件和目錄操作
  - lib.fileset: 用於處理文件集合的函數
  - lib.sources: 用於過濾和處理源代碼的函數
  - lib.cli: 用於將Nix表達式序列化為命令行格式的函數
  - lib.gvariant: 專門處理GVariant格式字符串的函數，這通常與D-Bus通信相關
  - lib.meta: 提供了對導向元數據的操作函數，例如創建、修改和訪問導向元數據
  - lib.derivations: 包含一些專門用於derivations的函數，如依賴管理、配置等
  - lib.customisation: 提供了自定義與derivations相關函數，或自定義derivations，或自定義屬性集的函數

- 類型，Build helpers : 專門用於產生 derivation 的函數
  - Fetchers : Fetchers 是用於從遠程源獲取軟件包的函數。它們負責下載、驗證和解壓縮原始碼或二進制文件
  - Trivial build helpers : 用於簡單的構建過程的函數。它們通常只需執行少量命令或操作即可
  - Testers : 是用於執行測試的函數。它們可以自動化軟件包的測試過程
  - Special build helpers : 為特定類型的軟件包設計的特殊函數。它們提供了更高級的構建選項和控制
  - Images : 用於創建容器映像的函數。它們允許開發者輕鬆地打包應用程序及其所有依賴
  - Hooks reference : 是一種特殊的 build helper，它允許開發者定義自訂的構建步驟
  - Languages and frameworks : 專門為特定語言或框架設計的 build helper。提供對該語言或框架的特殊支持
  - Packages : 允許在 Nix 中輕鬆地添加、配置和管理軟件包

## ref

- [Functions reference](https://nixos.org/manual/nixpkgs/stable/#chap-functions)
- [Nixpkgs Library Functions](https://nixos.org/manual/nixpkgs/stable/#sec-functions-library)
