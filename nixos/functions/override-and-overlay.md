## Override and Overlay

- 定製套件各種方法，參考，[定製套件](../customize-packages/readme.md)

- Override & Overlay 的使用場景

  |          | .override                                          | overlays                                                 |
  | -------- | -------------------------------------------------- | -------------------------------------------------------- |
  | 主要功能 | 修改單一package的內容                              | 修改nixpkgs實例，全局修改或新增nixpkgs實例中的package    |
  | 修改範圍 | 僅影響具體的package實例，局部作用                  | 疊加多個 overlay，按順序應用，並影響所有使用該包的地方   |
  | 影響範圍 | 單次覆蓋，僅影響該覆蓋操作所生成的package          | 持續影響，影響所有對該包的引用，直到 overlay 被移除      |
  | 操作對象 | 只適用於函數定義的package（例如接受參數的package） | 適用於任何包，無論是否有參數                             |
  | 適用場景 | 當只需要在當前上下文中修改package的行為時使用      | 當需要全局覆蓋包行為或新增自定義包時使用                 |
  | 性能影響 | 局部覆蓋，性能影響微乎其微                         | 重構 nixpkgs，若覆蓋範圍廣可能影響性能                   |
  | 語法     | 透過 .override 函數修改                            | `以函數形式`建立 overlay 函數，並傳遞給 nixpkgs.overlays |

## Override/OverrideAttrs 的使用

- 基本概念
  - 不是每個 package實例 都一定會有 .override 屬性，有的 package實例`有可能會沒有.override屬性`

  - 只有在 package 的建構函數中，其`輸入參數有自定義的參數`，此時，產生的 package實例 才會有 .override屬性

  - package建構函數產生的`package實例`才會有 .override 屬性

  - 必須透過`callPackage函數`創建package實例，callPackage函數才會為package實例添加.override屬性，
    手動調用package建構函數建立的package實例，並不會有.override屬性

  - .overrideAttrs 是`修改函數名為 mkXXXX 實際建構函數的建構參數`，而不是修改自定義建構函數的輸入參數
    
    例如，修改 mkDerivation 中的 name 參數，

    `無論是手動或是透過callPackage創建的package實例`，都可以使用 .overrideAttrs 修改mkDerivation中的任何建構參數

    但是無法修改自定義建構函數的輸入參數

    透過 .override 依舊可以修改包的內容，但需要透過替換 mkDerivation 的建構參數，
    繞過了(手動建立package實例)無法修改(自定義建構函數的輸入參數)的限制，詳見以下範例

  - .override 可以直接修改建構函數的內容，不需要等到package實例建立才修改 (建構1次)

  - .overrideAttrs 需要先建立package實例後，才能透過 `.overrideAttrs，消耗的資源比 .override 更多` (建構2次)

- 範例，對有提供 .override 屬性的 package 進行修改屬性

  ```nix
  myHello = pkgs.hello.override {
    pname = "myHello";
    version = "2.12";
  };
  ```

- 範例，對有提供 .overrideAttrs 屬性的 package 進行修改屬性

  ```nix
  myHello = pkgs.hello.overrideAttrs (oldAttrs: {
    src = pkgs.fetchurl {
      url = "https://ftp.gnu.org/gnu/hello/hello-2.12.tar.gz";
      sha256 = "sha256值";
    };
  });
  ```

- 範例，建立`不具有` .override 屬性的 package

  <font color=blue>不支援 .override 屬性的建構包</font>
  
  package-foo-no-override.nix
  ```nix
  # 執行建構，$ nix-build package-foo-no-override.nix

  # package 的建構函數中，若輸入參數沒有自定義的參數，
  # 則 package 實例就不會有 .override 屬性
  { pkgs ? import <nixpkgs> {} }:

  pkgs.stdenv.mkDerivation {
    name = "hello-script";

    # Skip the unpack phase by not providing a src
    unpackPhase = "true"; 

    installPhase = ''
      mkdir -p $out/bin
      echo '#!${pkgs.stdenv.shell}' > $out/bin/hello
      echo 'echo "hello"' >> $out/bin/hello
      chmod +x $out/bin/hello
    '';
  }
  ```

  default.nix
  ```nix
  # 執行建構，nix-build
  let
    pkgs = import <nixpkgs> {};

    # 利用 callPackage時，可以不手動傳入pkgs參數
    # origin-foo = pkgs.callPackage ./package-foo-no-override.nix {};
    origin-foo = pkgs.callPackage ./package-foo-no-override.nix { inherit pkgs; };
  in
  {
    # 拋出錯誤，沒有 .override 屬性
    modify-foo = origin-foo.override {
      msg = "hi";
    };
  }
  ```

- 範例，建立`具有` .override 屬性的 package

  <font color=blue>不支援 .override 屬性的建構包</font>
  
  package-foo-with-override.nix

  ```nix
  # 執行建構，nix-build package-foo-with-override.nix

  {
    pkgs ? import <nixpkgs> {},
    
    # package 的建構函數中，若輸入參數有自定義的參數，
    # 則 package 實例就會有 .override 屬性
    msg ? "hello"
  }:

  pkgs.stdenv.mkDerivation {
    name = "foo-with-override";

    # Skip the unpack phase by not providing a src
    unpackPhase = "true";

    installPhase = ''
      mkdir -p $out/bin
      echo '#!${pkgs.stdenv.shell}' > $out/bin/hello
      echo 'echo "${msg}"' >> $out/bin/hello
      chmod +x $out/bin/hello
    '';
  }
  ```

  default.nix
  ```nix
  # 執行建構，nix-build
  let
    pkgs = import <nixpkgs> {};
    origin-foo = pkgs.callPackage ./package-foo-with-override.nix { inherit pkgs; };
  in
  {
    # 或 modify-foo = (pkgs.callPackage ./package-foo-with-override.nix { inherit pkgs; }).override {
    #     msg = "hi";
    #    };

    modify-foo = origin-foo.override {
      msg = "hi";
    };
  }

  # 透過 result/bin/hello 測試
  ```

- 範例，手動建立的 package 實例，不會有Override屬性

  package-foo-with-override.nix
  ```nix
  { 
    pkgs ? import <nixpkgs> {},
    msg ? "hello",
  }:

  pkgs.stdenv.mkDerivation {
    name = "foo-with-override";

    # Skip the unpack phase by not providing a src
    unpackPhase = "true"; 

    installPhase = ''
      mkdir -p $out/bin
      echo '#!${pkgs.stdenv.shell}' > $out/bin/hello
      echo 'echo "${msg}"' >> $out/bin/hello
      chmod +x $out/bin/hello
    '';
  }
  ```

  default.nix
  ```nix
  # 執行建構，$ nix-build -A origin-foo，成功產出
  # 執行建構，$ nix-build -A modify-foo，錯誤，沒有 override屬性

  let
    pkgs = import <nixpkgs> {};
    origin-foo = (import ./package-foo-with-override.nix) { inherit pkgs; };
  in 
    {
      origin-foo = origin-foo;

      modify-foo = origin-foo.override {
        msg = "hi";
      };
    }
  ```

- 範例，利用 .overrideAttrs 修改 mkDerivation 的參數

  package-foo-with-override.nix
  ```nix
  { 
    pkgs ? import <nixpkgs> {},
    msg ? "hello",
  }:

  pkgs.stdenv.mkDerivation {
    name = "foo-with-override";

    # Skip the unpack phase by not providing a src
    unpackPhase = "true"; 

    installPhase = ''
      mkdir -p $out/bin
      echo '#!${pkgs.stdenv.shell}' > $out/bin/hello
      echo 'echo "${msg}"' >> $out/bin/hello
      chmod +x $out/bin/hello
    '';
  }
  ```

  default.nix
  ```nix
  # 執行建構，nix-build

  let
    pkgs = import <nixpkgs> {};
    origin-foo = (import ./package-foo-with-override.nix) { inherit pkgs; };
  in 
    {
      # 產出 /nix/store/pwxiyl7rkrp22bdx6q4vipjl2kbhqg42-foo-with-overrid
      inherit origin-foo;

      # 錯誤用法，msg 是建構函數的輸入參數，不是 mkDerivation 的參數
      # modify-foo = origin-foo.overrideAttrs {
      #   msg = "hi";
      # };

      # 正確，name 是 mkDerivation 的參數
      # 產出 /nix/store/zzv78w3ziy1cka25srmy5104kfs2nfxs-im-foo
      # mkDerivation 的 name 參數被改寫了 
      modify-foo = origin-foo.overrideAttrs {
        name = "im-foo";
      };
      
    }
  ```

  透過 .override 依舊可以修改包的內容，但需要透過`替換 mkDerivation 的建構參數`，

  透過此方法繞過了(手動建立package實例)無法修改(自定義建構函數的輸入參數)的限制

  例如，將上述的 modify-foo 改為以下
  ```nix
  modify-foo = origin-foo.overrideAttrs (old:{
    installPhase = ''
      mkdir -p $out/bin
      echo '#!${pkgs.stdenv.shell}' > $out/bin/hello
      echo 'echo "hi"' >> $out/bin/hello
      chmod +x $out/bin/hello
    '';
  });
  ```

  透過 overrideAttrs ，即使是手動建立package實例，也能夠修改package的內容，
  而不是僅僅只能修改建構參數

## Overlays 的使用

- overlay 原理

  <img src="doc/Dram-overlay-final-prev.png" width=80% height=auto>

  上圖中，
  - main 是產生nixpkgs實例的原始建構函數，依序會經過 ext-1 和 ext-2 兩個 overlay 函數

  - main、ext-1、ext-2，三個函數中，final 是最終產物，
    三個final都指向最終的nixpkgs實例

  - `prev` 分別指向前一級的輸出，代表`未套用overlay前的輸出`，
    - main 是第一級，不會有 prev
    - ext-1 的 prev = main 的 output
    - ext-2 的 prev = exit 的 output
  
  - `final`，代表`套用overlay後的輸出`，

- 使用 overlays 的流程

  - step，定義overlay函數
    
    ```nix
    final: prev: {
      # 定義新增的package
      newPackage = prev.stdenv.mkDerivation {
        ...
      };

      # 覆蓋現有包，例如修改版本號
      hello = prev.hello.overrideAttrs (oldAttrs: {
        ...
      });
    }
    ```

  - step，應用overlay函數
    - `方法`，透過`nixpkgs.overlays屬性`
      - 注意，不要在`nixpkgs的源代碼`中使用這樣的語句，在源代碼中使用了`import <nixpkgs>`的語句，
        會[增加效能的開銷](https://nixos.org/manual/nixpkgs/stable/#sec-overlays-argument)
      - 在nixpkgs的源代碼中，改用`pkgs.extend函數`或`pkgs.appendOverlays函數`
    
    - `方法`，透過`pkgs.extend函數`
    
    - `方法`，透過`pkgs.appendOverlays函數`

    - 注意，頻繁調用 pkgs.extend 或 pkgs.appendOverlays函數都會增加效能的開銷

- 範例，(自定義overlay函數) + (在configuration.nix中應用overlay函數)

  step，<font color=blue>定義 overlay 函數</font>

  ```nix
  # my-overlay.nix

  # self 是當前的(派生的) nixpkgs
  # super 是原始的(被繼承的) nixpkgs
  self: super: {

    # 定義新增的package
    myPackage = super.stdenv.mkDerivation {
      name = "my-package";
      src = ./my-source.tar.gz;
      buildInputs = [ super.someDependency ];
      # 其他構建參數...
    };

    # 覆蓋現有包，例如修改版本號
    hello = super.hello.overrideAttrs (oldAttrs: {
      version = "2.12";
      src = super.fetchurl {
        url = "https://ftp.gnu.org/gnu/hello/hello-2.12.tar.gz";
        sha256 = "sha256值";
      };
    });
  }
  ```

  step，<font color=blue>應用自定義的 overlay 函數</font>

  ```
  # /etc/nixos/configuration.nix

  { config, pkgs, ... }:

  {
    # 透過 nixpkgs.overlays 應用自定義的 overlays 函數
    nixpkgs.overlays = [
      (import ./my-overlay.nix)
      (import ./other-overlay.nix)
    ];

    environment.systemPackages = with pkgs; [
      my-hello  # 使用自定義的 my-hello 包
      hello     # 使用覆蓋後的 hello 包
    ];
  }
  ```

- 範例，(自定義overlay函數) + (在flake.nix中應用overlay函數)

  ```
  # 透過 nix shell .#my-hello 使用

  {
    inputs.nixpkgs.url = "github:NixOS/nixpkgs";

    outputs = { self, nixpkgs }: {
      overlays.default = self: super: {
        my-hello = super.stdenv.mkDerivation {
          pname = "my-hello";
          version = "1.0.0";
          src = ./hello-source.tar.gz;
          buildPhase = ''
            echo "Building my custom hello"
            mkdir -p $out/bin
            echo '#!/bin/sh' > $out/bin/my-hello
            echo 'echo "Hello, Flakes!"' >> $out/bin/my-hello
            chmod +x $out/bin/my-hello
          '';
        };
      };
    };
  }
  ```

  或改寫 pkgs 
  ```
  let pkgs = (nixpkgs.legacyPackages.${system}.extend overlay1).extend overlay2
  let pkgs = import nixpkgs { inherit system; overlays = [ overlay1 overlay2 ]; }
  ```

- 範例，(自定義overlay函數) + (透過pkgs.extend應用overlay函數)

  ```nix
  let
    pkgs = import <nixpkgs> {};

    # 定義 overlay 函數
    myOverlay = self: super: {
      myPackage = super.callPackage ./my-package.nix {};
    };

    # 使用 pkgs.extend 應用 overlay
    pkgs_extended = pkgs.extend myOverlay;
  in
    # 得到新的 pkgs 而不是舊的 pkgs
    pkgs_extended
  ```

- 範例，(自定義overlay函數) + (透過pkgs.appendOverlays應用overlay函數)

  ```nix
  let
    pkgs = import <nixpkgs> {};

    # 定義overlay函數1
    overlay1 = self: super: {
      packageA = super.callPackage ./packageA.nix {};
    };

    #定義overlay函數2
    overlay2 = self: super: {
      packageB = super.callPackage ./packageB.nix {};
    };

    # 使用 pkgs.appendOverlays 應用多個overlay函數
    finalPkgs = pkgs.appendOverlays [ overlay1 overlay2 ];
  in
    # 得到新的 pkgs 而不是舊的 pkgs
    finalPkgs
  ```
  
## ref 

- [介紹 override、overrideAttrs、overlays](https://www.youtube.com/watch?v=jHb7Pe7x1ZY)

- [Overlays 原理 + 各種範例 @ nixos.wiki](https://nixos.wiki/wiki/Overlays)

- [overlay @ nixpkgs-manual](https://nixos.org/manual/nixpkgs/stable/#chap-overlays)

- [overlay @ NixOS & Flakes Book ](https://nixos-and-flakes.thiscute.world/zh/nixpkgs/overlays)

- 其他範例
  - [在flake.nix中，透過nix-module應用overlay函數](https://nixos-and-flakes.thiscute.world/zh/nixpkgs/overlays)
  - [Replacing Overlays in a Flakes-Based Config](https://www.youtube.com/watch?v=IoeCzw22SkI)
  - [Creating Custom Package Versions with Overrides and Overlays，STEP by STEP](https://www.youtube.com/watch?v=Gzw1F80Vw7o)