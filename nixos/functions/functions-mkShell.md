## [函數] pkgs.mkShell 的使用

- 使用場景
  - 功能，建立自定義的shell環境
  - 功能，為 flake.nix 的 devShell屬性|devShells屬性，建立shell環境

- 查詢pkg.mkShell用法
  
  ```
  repl> pkgs = import <nixpkgs> { system = "x86_64-linux"; }
  repl> :e pkgs.mkShell
  ```

- 子屬性
  - buildInputs屬性，為devShell環境添加的套件
  - shellHook屬性，是shell會話開始時自動執行的一系列命令，優先於 --command 命令
  - 環境變數 = 值

- 範例，<a id="mkshell">建立可以使用 python 的 devShell 環境</a>
  - [單系統，指定system屬性的pkg](../flake/flake-examples/pkg.mkShell_1-1_specific-pkgs.nix)
  - [單系統，以with省略pkgs屬性的宣告](../flake/flake-examples/pkg.mkShell_1-2_ignore%20pkgs%20by%20with.nix)
  - [多系統，以 devShells 為例](../flake/flake-examples/pkg.mkShell_2-1_help-function%20with%20devShells.nix)
  - [多系統，以 devShells 為例](../flake/flake-examples/pkg.mkShell_2-2_help-function%20with%20devShell.nix)
  - [多系統，推薦最精簡的寫法](../flake/flake-examples/pkg.mkShell_2-3_help-function%20with%20seperated%20function.nix)
  - [單系統/多系統，使用flake-utils.lib.eachDefaultSystem](../flake/flake-examples/pkg.mkShell_3-1_use%20flake-utils.lib.eachDefaultSystem.nix)

- 其他
  - 如何降低mkShell的大小
    - [Smaller stdenv for shells](https://discourse.nixos.org/t/smaller-stdenv-for-shells/28970)
    - [範例](flake-examples/pkg.mkShell_minimum-stdenv.nix)