## [函數] pkgs.stdenv.mkDerivation，建構函數，用於產生drv檔，並可透過 nix-build 或 nix build 產生實際檔案

- 查詢用法
  ```
  repl> pkgs = import <nixpkgs>{}
  repl> :e pkgs.stdenv.mkDerivation
  ```

- mkDerivation 可用的參數
  - `name` : 指定套件的完整名稱

  - `pname` | version : 指定套件的完整名稱的另一個方法
    - pnams : 指定套件的名稱，不包含版本
    - version : 指定套件的版本
    - pname 和 version 組成產出檔案的名稱，例如，`[hash]-${pname}-${version}`
    - 若 pname+version 和 name 同時存在，會保留 name 的設置

  - `src | dontUnpack` : 指定源碼相關
    - src : 設置源碼目錄的路徑
      - 若在當前目錄中，可以設置為 `src = ./.`
      - src 可以是目錄、git-repo、url 等
    - dontUnpack : 是否解壓縮 src 目錄中的壓縮檔，當 src 目錄中的檔案不是壓縮檔時使用

  - `buildInputs | nativeBuildInputs | propagatedBuildInputs` : 指定依賴
    - buildInputs : 
      - 指定`編譯過程`和`執行套件`時都需要的依賴，例如，建構和執行pythonu應用時，需要request套件
      - 由於執行套件時也需要這些依賴，因此在建構完成後，這些依賴會被保留下來
  
    - nativeBuildInputs : 
      - 僅構建過程需要的依賴，但`執行套件時不需要`，例如，gcc
      - 由於執行套件時不需要這些依賴，因此在建構完成後，這些依賴不會被保留到最終環境中

    - propagatedBuildInputs : 建構多個套件時，多個套件都需要的依賴

  - `buildPhase | configurePhase | installPhase | patchPhase | checkPhase` : 自定義建構時每個階段要執行的命令

    - 每一個 Phase 的說明，參考，[pkgs.mkShell的使用](functions.md#函數-pkgsmkshell建立-shell-環境)
  
    - 執行順序 : ar bi trary

      prePhases > unpackPhase > 
      patchPhase > preConfigurePhases > configurePhase > 
      preBuildPhases > buildPhase > checkPhase > 
      preInstallPhases > installPhase > fixupPhase > installCheckPhase > 
      preDistPhases > distPhase > postPhases

  - `phases` : 定義構建過程要執行那些 phase，或控制執行phase的順序
    - stdenv.mkDerivation 預設會載入 setup.sh 並調用當中的 genericBuild() 來進行建構，
      [參考](https://github.com/NixOS/nixpkgs/blob/master/pkgs/stdenv/generic/default.nix)
  
    - 所有可用的 phase 都定義在
      [setup.sh](https://github.com/NixOS/nixpkgs/blob/master/pkgs/stdenv/generic/setup.sh)
  
    - 可用值 
      unpackPhase、patchPhase、configurePhase、buildPhase、checkPhase、installPhase、fixupPhase、installCheckPhase、distPhase

  - `meta` : 定義包的元數據，如描述、作者、維護者、license
  
  - `preBuild | postBuild` : 在 buildPhase 前後執行指定命令
    - preBuild : 用於構建前做一些初始化工作
    - postBuild : 用於構建完成後需要進行一些清理或後處理

  - `preInstall | postInstall` : 在 installPhase 前後執行指定命令，需要在安裝過程中添加額外步驟時使用

  - `doCheck` : 是否啟用測試階段，預設為 true，不需要執行測試時，可以設置為 false
  
  - `doInstallCheck` : 是否在installPhase後執行測試，預設為 false

  - `enableParallelBuilding` : 是否啟用並行構建，預設為 false
  
  - `outputHash | outputHashAlgo` : 自定義輸出檔名的哈希值，和自定義哈希算法

- [在shell中，手動執行各種Phase以進行debug](../flake/flake-examples/mkShell-manully-call-buildPhase.nix)

- 比較，pkg.derivation VS pkgs.stdenv.mkDerivation
  - stdenv.mkDerivation 提供更完整的環境來構建 derivation，包括 GNU 核心工具集、bash、tar、gzip，
    不需要再透過 buildInputs 指定這些工具
  - stdenv.mkDerivation 包含一個內建的構建過程（通常是 ./configure && make），
    以及一些變量讓你覆蓋其中的一部分而無需寫一個新的腳本
  - stdenv.mkDerivation 默認 Bash 作為腳本解析器，derivation 只承諾提供 Bourne 兼容的 shell
  - derivation 則提供了更底層的控制權，允許開發者完全自定義構建過程和環境

## [屬性] overrideAttrs 屬性 : 用於修改|新增derivation的屬性，並建立新的derivation

- [overrideAttrs文檔](https://nixos.org/manual/nixpkgs/stable/#sec-pkg-overrideAttrs)

- 範例，修改pname
  ```
  {
    helloBar = pkgs.hello.overrideAttrs (finalAttrs: previousAttrs: {
      pname = previousAttrs.pname + "-bar";
    });
  }
  ```

- 範例，[為不支援 passthru屬性的函數，增加 passthru屬性](../test-framwork/test-examples/passthru-expose-info.nix)

## 範例集

- 範例，透過 nix-build，將 c 源碼編譯為執行檔

  - hello.c
    ```
    #include <stdio.h>

    int main() {
        printf("Hello, World!\n");
        return 0;
    }
    ```
  
  - default.nix
    ```
    let
      pkgs = import <nixpkgs> {};
    in 
      pkgs.stdenv.mkDerivation {
        name = "hello-world";
        src = ./.;
        buildInputs = [ pkgs.gcc ];
        buildPhase = ''
          mkdir -p $out/bin
          ls -l $out/bin
          ls -l hello.c
          ${pkgs.gcc}/bin/gcc -o $out/bin/hello hello.c
        '';
      }
    ```

  - 透過 nix-build 執行編譯，$ nix-build

  - 測試，$ ./result/bin/hello

- 範例，[mkDerivation的簡單範例](../flake/flake-examples/pkg.stdenv.mkDerivation_simple-package.nix)

- 範例，[利用mkDerivation編譯rust專案](../flake/flake-examples/rust-hello)

- 範例，[將 python-script 打包為執行檔](../flake/flake-examples/pkg.stdenv.mkDerivation_compile-py-project.nix)

## ref

- phases 屬性
  - [phases說明 @ nix-pills](https://nixos.org/guides/nix-pills/19-fundamentals-of-stdenv#the-setup-file)
  
  - [phases說明 @ ryantm](https://ryantm.github.io/nixpkgs/stdenv/stdenv/#sec-stdenv-phases)