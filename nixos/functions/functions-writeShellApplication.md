## pkgs.writeShellApplication 的使用

- 範例，簡易範例

  以下透過 `$ nix-build aa.nix` 執行
  
  ```
  # aa.nix
  let
    pkgs = import <nixpkgs> {};
  in
    hello = pkgs.writeShellApplication {
      name = "hello";
      text = ''
        echo Hello World
      '';
    }
  ```

  產生的檔案內容
  ```shell
  #!/nix/store/r9h133c9m8f6jnlsqzwf89zg9w0w78s8-bash-5.2-p15/bin/bash
  set -o errexit
  set -o nounset
  set -o pipefail
  echo Hello World
  ```