## [函數] pkgs.runCommand，在建構的過程中執行命令

- 用途，
  - 功能，在建構的過程中，執行命令
  - 功能，創建開發環境的函數，允許定義一個包含所需軟件包的 shell wrapper，
    這樣就能直接通過執行運行該 wrapper 來進入到該環境中

- 語法，`pkgs.runCommand "drv檔名" {要傳遞給執行環境的屬性集合} ''要執行的命令''`
  - 要傳遞給執行環境的屬性集合，同時也會出現在最後要製作 drv 的屬性集中

- 限制
  
  在此命令中，`$out`是一個必須存在的目錄或文件，用於存儲構建結果，

  如果在執行命令中沒有創建 $out，
  就會報告 "failed to produce output path for output 'out'" 的錯誤

- 範例，在建構中執行命令

  <font color=blue>default.nix</font>
  ```
  let
    pkgs = import <nixpkgs> {};
    package = pkgs.runCommand "foo" {} ''
      mkdir -p $out
      echo executing echo command
    '';
  in
    package
  ```

  執行，`$ nix-build default.nix`

- 範例，建構開發環境範例

  ```
  {
    description = "A Nix-flake-based Node.js development environment";

    inputs = {
        nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";
    };

    outputs = { self , nixpkgs ,... }: let
        system = "x86_64-darwin"; # 系統應與你運行的系統匹配
        pkgs = import nixpkgs {
          inherit system;
        };
        packages = with pkgs; [
            nodejs_20
            nodePackages.pnpm
            nushell
        ];
    in {
        packages."${system}".dev = pkgs.runCommand "dev-shell" {
          buildInputs = packages;
          nativeBuildInputs = [ pkgs.makeWrapper ];
        } ''
          mkdir -p $out/bin/
          ln -s ${pkgs.nushell}/bin/nu $out/bin/dev-shell
          wrapProgram $out/bin/dev-shell --prefix PATH : ${pkgs.lib.makeBinPath packages}
        '';
    };
  }
  ```
  執行，`$ nix run .#dev` 或 `$ nix shell .#dev --command 'dev-shell'`

## ref

- pkgs.runCommand 範例
  - [runCommand + builder.sh](https://nixos.wiki/wiki/Shell_Scripts)
  - [利用runCommand逐量增加結果，build up a result incrementally](http://www.chriswarbo.net/projects/nixos/scripting_with_nix.html)