## pkgs.fetchFromGitHub 的使用

- pkgs.fetchFromGitHub屬性
  - `owner屬性` : repo的擁有者名，例如，`https://github.com/foo/mypackage`中的foo
  
  - `repo屬性` : repo名，例如，`https://github.com/foo/mypackage`中的mypackage

  - `rev屬性` : 指向目標repo的commit或tag
    - 例如，`rev = "refs/tags/1.28.0";`
    - 例如，`rev = "3ddd1b168308db2b88daac9ea70a3b63629ef034";`

  - `hash屬性` : 目標檔案的hash值，必須是SRI(Subresource Integrity)-base32的格式
    - 例如，`nix-prefetch-url --unpack https://github.com/casey/just/archive/<commit>.tar.gz`，
      - 得到`1032dnza2i4wzq6jxgmdrpmqa5f0pn9qpzisscx30xkiiwaykl0r`，此為原始的hash值
      - 透過`$ nix hash to-sri --type sha256 原始的hash值`，得到 sri-base32格式的值
      - 最後得到，`hash = "sha256-...."`

- 範例

  ```
  pkgs.fetchFromGitHub {
    owner = "casey";
    repo = "just";
    rev = "3ddd1b168308db2b88daac9ea70a3b63629ef034";
    hash = "sha256-GdDpFY9xdjA60zr+i5O9wBWF682tvi4N/pxEob5tYoA=";
  };
  ```
