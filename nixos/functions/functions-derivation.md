## [函數] pkgs.derivation 的使用

- 子屬性
  - `system屬性`，必要，用於指定系統架構

  - `name屬性`，必要，用於指定 derivation 的名稱

  - `builder屬性`，必要，
    - 可提供一個腳本或命令行指令作為 builder 的值，以實現完全控制構建過程的細節
      > 注意，builder屬性預設使用完全獨立的環境，而不是使用預設的 stdenv 構建進行建構，因此，常用命令有可能是失效的
    - 必需要將結果放置到 $out 目錄中

  - src屬性，用於指定`專案根目錄`的位置，可以是`URL`、`本地路徑`或者`內置的Nix數據庫引用`
    - 注意，以`src = ./.`取代`src = ./`，`src = ./` 會造成錯誤
    - `src = ./` : 僅僅代表當前目錄
    - `src = ./.` : 代表當前目錄，並且會重新引用當前目錄，
    - 測試，在 nix repl 中，`./` 會拋出錯誤，`./.`才會返回當前目錄的路徑
    - 原理，
      - `src = ./.`，會使得 nix 複製目錄到 nix-store 中，並將 nix-store 中的路徑傳遞給 src
      - `src = ./`，只會將當前目錄賦值給 src ，不會將 nix-store 中的路徑傳遞給 src
    - 參考，[Can't get rustPlatform to work with `src](https://discourse.nixos.org/t/cant-get-rustplatform-to-work-with-src/13210/5)

  - buildInputs屬性，指定建構 derivation 過程中所需的依賴項，
    這些依賴項會被添加到 build environment 中，以便於編譯或其他操作

  - nativeBuildInputs屬性，與 buildInputs 相似，但依賴項只在本機環境中可用，不會被打包進最終產物中

  - buildPhase屬性，用於構建 derivation 階段要執行的命令，例如，make、gcc

  - installPhase屬性，用於安裝 derivation 到系統階段要執行的命令，例如，cp -r ./usr/local/share/myapp

  - meta屬性，額外的訊息，用於提供關於 derivation 的元數據，如描述、維護者信息等

  - args屬性，傳遞額外的參數給 builder 腳本或命令行指令

  - dontUnpack屬性，不要source屬性指定的檔案進行解壓縮

- 注意，使用過程中必須將檔案輸出到`$out目錄`中

- 範例，[pkg.derivation的簡單範例](../flake/flake-examples/pkg.derivation_simple-package.nix)