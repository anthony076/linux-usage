## [函數] pkgs.callPackage，可自動注入pkgs的參數化構建Derivation工具函數

- 語法，`pkgs.callPackage fn args`

- 優點 : 自動注入 pkgs 

  考慮一個會產生derivation的函數，pkgs.writeShellScriptBin，用於建立 shell-script 檔案
  在 nix repl 中進行測試 pkgs.writeShellScriptBin 

  ```
  nix-repl> pkgs = import <nixpkgs> {}
  
  # pkgs.writeShellScriptBin shellScript檔名 ''Script內容''
  nix-repl> hello = pkgs.writeShellScriptBin "hello" '' echo "hello, xxx!" ''

  nix-repl> hello
  «derivation /nix/store/[hash值]-hello.drv»
  ```

  回到 shell，透過`ls /nix.store | grep hello`，可以查看到`[hash值]-hello.drv`存在

  將產生 hello-derivation 的函數寫入 build-hello.nix 的文件中，
  
  ``` nix
  # build-hello.nix
  { pkgs ? import <nixpkgs> {} }:
  pkgs.writeShellScriptBin "hello" ''echo "hello, xxx!"''
  ```

  一般情況下，要調用該函數(該文件)，`需要透過 import 語句並將輸入參數 pkg 傳入`，
  在 nix repl 中可以進行測試，可以得到同樣的結果

  ```
  nix-repl> pkgs = import <nixpkgs> {} 
  
  # 透過 import 調用，./build-hello.nix，並且需要手動傳遞依賴
  nix-repl> hello = import ./build-hello.nix {inherit pkgs;}

  nix-repl> hello
  «derivation /nix/store/v3k9i7avp1p55x6als5rjkjd2i1ajlpv-hello.drv» 
  ```

  在使用 pkgs.callPackage 的情況下，可以不需要手動傳遞 pkg 參數，
  ```
  nix-repl> pkgs = import <nixpkgs> {}
  
  # 相較於 import ./build-hello.nix {inherit pkgs;}
  # pkgs.callPackage 的輸入參數只需要傳遞空的 attribute-set
  nix-repl> hello = pkgs.callPackage ./build-hello.nix {}

  nix-repl> hello
  «derivation /nix/store/v3k9i7avp1p55x6als5rjkjd2i1ajlpv-hello.drv»
  ```

  使用 pkgs.callPackage 的好處，`調用產生derivation的函數`，可以不用傳遞 pkgs 參數，
  pkgs.callPackage 會自動注入pkgs，實現`自動依賴注入`

  注意，pkgs.callPackage 常用於產生derivation的nix文件，
  因為該文件在執行過程中，往往需要依賴其他套件，也往往依賴 pkgs 中的套件，
  因此，pkgs.callPackage 預設自動注入的 pkgs

- 優點 : 仍然保留參數化構建

  考慮以下代碼

  ``` nix
  # build-hello.nix
  { pkgs ? import <nixpkgs> {},
    msg ? "world"

  }:
  pkgs.writeShellScriptBin "hello" ''
    echo "hello, ${msg}"
  ''
  ```

  在 nix repl 中，
  ```
  nix-repl> pkgs = import <nixpkgs> {}

  nix-repl> pkgs.callPackage ./build-hello.nix {}
  «derivation /nix/store/lngq9iy61yyl30s8pldnv3f3ckqsjq8j-hello.drv»
  
  nix-repl> pkgs.callPackage ./build-hello.nix {msg = "people"; }
  «derivation /nix/store/hdlwl6kvq2rpa11v2bwkiyvy848xcvja-hello.drv»
  ```

  從上述例子可以看出，除了自動注入的pkgs依賴外，然後可以透過參數，客制化derivation的內容

- 優點 : 對於已經建立好的 derivation，可以透過 override 進行參數覆寫，並產生新的 derivation

  詳見，[override屬性的使用](override-and-overlay.md)

  考慮以下代碼

  ``` nix
  # build-hello.nix
  { pkgs ? import <nixpkgs> {},
    msg ? "world"

  }:
  pkgs.writeShellScriptBin "hello" ''
    echo "hello, ${msg}"
  ''
  ```

  在 nix repl 中，
  ```
  nix-repl> pkgs = import <nixpkgs> {}

  nix-repl> hello = pkgs.callPackage ./build-hello.nix {}

  nix-repl> hello
  «derivation /nix/store/lngq9iy61yyl30s8pldnv3f3ckqsjq8j-hello.drv»
  
  # 修改已建立的 derivation 參數，並產生新的 derivation
  nix-repl> folks = hello.override { msg = "folks"; }
  
  nix-repl> folks
  «derivation /nix/store/8n5wwfs108q79mrzyfjl187mx961rqm5-hello.drv»
  ```

  注意，<font color=blue>只有透過 pkgs.callPackage 建立的 derivation 才會有 override 的屬性</font>，
  透過 import 建立的 derivation 沒有override 的屬性，無法修改

- 使用 lib.callPackageWith() 建構自己的 callPackage()

  參考，[https://summer.nixos.org/blog/callpackage-a-tool-for-the-lazy/]

## pkgs.callPackage 內部的操作

- [pkgs.callPackage 源碼](https://github.com/NixOS/nixpkgs/blob/fe138d3/lib/customisation.nix#L101-L121)

- step，判斷 fn 是函數或是nix文件，
  - 若是函數，獲取函數內容
  - 若是nix文件，透過 import 獲取nix文件的函數

- step，透過 lib.functionArgs 獲取函數的輸入參數

- step，`pkgs.callPackage fn args`會將 args 與 pkgs 這個屬性集合進行合併，
  如果存在衝突，args 中的參數會覆蓋 pkgs 中的參數，最後得到新的屬性集合

- step，從新的屬性集合中獲取fn的內容，並執行fn函數，此時的fn函數已經獲得了所有需要的輸入參數

## 範例集

- 範例，調用 pkgs.callPackage 匯入建構檔，並透過 nix-build 建構

  - step，建立用於建構 shell-script 的 build-hello.nix

    ```
    # build-hello.nix

    { pkgs ? import <nixpkgs> {},
      msg ? "world"
    }:
    pkgs.writeShellScriptBin "hello" ''
      echo "hello, ${msg}"
    ''
    ```
  
  - step，建立調用 build-hello.nix 的 default.nix，default.nix 也是 nix-build 預設的配置檔

    透過 default.nix 調用執行 build-hello.nix 中定義的函數，執行完畢後，會產生 *.drv檔
    ```
    let
      pkgs = import <nixpkgs> {};

    in {
      hello = pkgs.callPackage ./build-hello.nix {};
      people = pkgs.callPackage ./build-hello.nix { msg = "people"; };
    }
    ```

  - step，透過 nix-build 命令進行建構
    - 透過 nix-build 會根據*.drv檔建構實際的檔案
    - `$ nix-build -A hello default.nix`，建構 default.nix 輸出屬性集中的 hello 屬性
    - `$ nix-build -A hello`，default.nix 可以省略
    - `$ nix-build`，輸出屬性集中的所有屬性都會被建構

  - step，測試，
    - `$ ./result/bin/hello`
    - `$ cat ./result/bin/hello`

## ref

- [pkgs.callPackage原理說明](https://nixos-and-flakes.thiscute.world/zh/nixpkgs/callpackage)
- [callPackage, a tool for the lazy](https://summer.nixos.org/blog/callpackage-a-tool-for-the-lazy/)
- [callPackage @ nix pills](https://nixos.org/guides/nix-pills/13-callpackage-design-pattern)
