## pacman 的使用

- 更新
  - 更新軟體源，`$ pacman -Sy`，將在地的包數據庫與遠程的倉庫進行了同步
  - 升級系統中所有的軟體，`$ pacman -Su`
  - 更新軟體源後再升級軟體，`$ pacman -Syu`，強制對系統更新

- 查詢
  - 查詢含有關鍵字的軟體，`$ pacman -Ss`
  - 列出已安裝的軟體，`$ pacman -Q`
  - 查詢軟體的訊息，`$ pacman -Qi`
  - 列出軟體相關的文件列表，`$ pacman -Ql`
  - 列出不再作為依賴的軟體包，`$ pacman -Qdt`

- 安裝
  - 安裝軟體，`$ pacman -S 軟體名`
  - 安裝本地包，`$ pacman -U 軟體路徑`
  - 取得最新版本後再安裝軟體，`$ pacman -Sy 軟體名`

- 刪除
  - 只刪除軟體，`$ pacman -R 軟體名`
  - 刪除軟體+依賴，`$ pacman -Rs 軟體名`
  - 刪除軟體+配置，`$ pacman -Rn 軟體名`
  - 刪除軟體+依賴+配置，`$ pacman -Rsn 軟體名`

  - 刪除已安裝的軟體和依賴，`$ pacman -Rcns 軟體名`，若依賴被其他軟體依賴，會報錯
  - 列出不被依賴的軟體，`$ pacman -Qdtq`
  - 刪除所有不被依賴的軟體，`$ pacman -Rsn $(pacman -Qdtq)` 

- 清理
  - 清除未使用軟體的快取，`$ pacman -Sc`
  - 清理所有快取，`$ pacman -Scc`

- 其他
  - 關閉確認，`--noconfirm`
  - 只安裝必要的依賴，`--needed`
  - 最佳化 pacman ，`$ sudo pacman-optimize`

## 第三方軟體倉數 AUR (archlinux-user-repository)

- 存取 AUR 的軟體
  - Yaourt、Trizen、Packer: 停止或有安全疑慮或開發緩慢有安全疑慮 
  - aurman
  - yay: 
  - pakku: 從官方倉庫編譯，並事後移除編譯依賴 
  - aurutils
  - Pikaur: 
    - 最小依賴性的 AUR 助手，可以一次查看所有 PKGBUILD，無需用戶交互即可全部構建
    - 通過控制 pacman 命令來告知 Pacman 要執行的下一個步驟
  - paru
    - 由 yay 主要開發者之一維護
    - 由 rust 編寫的
  
## 手動編譯

- 手動從源碼安裝軟體，可被 pacman 追蹤 

- 以 pakku 為例
  - git clone https://aur.archlinux.org/pakku.git 
  - cd pakku 
  - makepkg -si 

## 推薦 pacman 相關套件

- downgrade，提供舊版本的安裝
- base-devel，提供基礎的編譯工具
- pacman-contrib，提供 pactree，可檢查軟體的依賴

- reflector，用於選擇最快的鏡像站
  - `$ sudo pacman -S reflector`
  - `$ sudo reflector -c "Taiwan" -f 12 -l 10 -n 12 --save /etc/pacman.d/mirrorlist`

- axel，使 pacman 使用多線程下載軟體
  - `$ sudo pacman -S axel`
  - `$ sudo nano -w /etc/pacman.conf`
    ```shell
      ... 內容省略 ...
      #XferCommand = /usr/bin/curl -C - -f %u > %o
      XferCommand = /usr/bin/axel -n 5 -a -o %o %u
      ... 內容省略 ...
    ```

## Trouble-Shooting

- `問題`，安裝套件出現 `gpg: keyserver receive failed: Server indicated a failure`
  - 原因: 未能解析DNS
  - 解決
    ```shell
    sudo nano /etc/resolv.conf，添加以下
      nameserver 8.8.8.8 # Google DNS
      nameserver 1.1.1.1 # Cloudflare
    ```

- `問題`，安裝套件出現 `error: tree-sitter: signature from "XXX <XXX@XXX>" is unknown trust`
  - 原因: 未更新keyring
  - 解決方式
    ```shell
    pacman -Sy archlinux-keyring
    ```