## archlinux 上的推薦套件

- net-tools: 一個包含各種網路工具的庫，像​​ifconfig​​​ 或者​​netstat，官方目前使用​​ip address​​​ 命令來獲取本機的IP位址，
- man-db: 提供man命令
- man-pages: 提供man頁麵內容
- man-pages-zh_cn: 提供man中文頁麵內容，這個包下載下來不能直接用，後麵改別名會提到
- texinfo: info幫助文檔的包
- ntfs-3g: 對NTFS文件係統提供支援
- tree: 以樹形結構顯示目錄中各種文件的依附關係
- pacman-contrib: pacman包管理器的擴展好像是，我主要用裡麵的那個pactree命令
- neofetch: 一個顯示係統信息的工具
- wget: 一個用來下載的工具
- git: 這個就不用說了，做程式員的都知道這個
- usbutils: 檢視係統USB設備
- pciutils: 檢視係統PCI設備
- acpi: 用來檢視電池電量的工具


