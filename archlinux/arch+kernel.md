## arch + kernel

- kernel 基礎
  - [linux 開機流程基礎](../linux-core/01_bootup-flow.md)
  - [BIOS完整啟動流程](../linux-core/01_bootup-flow.md#bios-的完整啟動流程)
  - [內核和驅動](../linux-core/01_bootup-flow.md#系統引導程式bootloader-內核和驅動核心模塊的啟動)

- mkinitcpio 命令，用於建立 kernel 和 initramfs

  - 參考，[在archlinux安裝過程，透過mkinitcpio建立kernel和initramfs的範例](install-arch/overview.md#chroot-產生開機用的虛擬檔案系統initramfs)

## 檢視核心模組(檢視驅動)

- 檢視 initramfs 的內容，`$ lsinitcpio -a /boot/initramfs-linux.img`

- 透過 autodetect 篩選必要的模組，`$ mkinitcpio -M`
  - autodetect 會掃描 sysfs 並建立必要模組的白名單 

- 檢視硬碟驅動，`$ udevadm info --attribute-walk -n /dev/sda1 | grep 'DRIVERS=="[^"]'`
  ```shell
  # 得到以下結果
  DRIVERS=="sd"     # sd_mod 驅動包
  DRIVERS=="ahci"   # ahci 驅動包
  ```

- 檢視PCI裝置，`$ lspci -vk` 或 `$ lspci -tv`
  ```shell
  00:02.0 VGA compatible controller: VMware SVGA II Adapter (prog-if 00 [VGA controller])
          Subsystem: VMware SVGA II Adapter
          Flags: bus master, fast devsel, latency 64, IRQ 18
          I/O ports at c170 [size=16]
          Memory at e0000000 (32-bit, prefetchable) [size=16M]
          Memory at e1400000 (32-bit, non-prefetchable) [size=2M]
          Expansion ROM at 000c0000 [virtual] [disabled] [size=128K]
          Kernel driver in use: vmwgfx  # 使用的驅動
          Kernel modules: vmwgfx        # 使用的內核模組
  ```

- 檢視USB裝置，`$ lsusb -t`

- 顯示當前加載的模組，`$ lsmod`，會印出使用該模組的程序

- 查看指定模組的訊息，`$ modinfo 模組名或驅動名`

- 列出已載入模組的配置選項，`$ systool -v -m 模組名或驅動名`

- 列出指定模組的配置文件，`$ modprobe -c | grep 模組名或驅動名`

- 列出指定模組的依賴關係，`$ modprobe --show-depends module_name`
  - `lsmod` 打印出使用該模組的程序
  - `modprobe --show-depends`，打印出該模組是否依賴其他模組

## 使用 mkinitcpio 產生 initramfs-image 或 unified-kernel-image

- 注意，通常不需要自己創建新的 initramfs
  
  執行 pacstrap 時若安裝 linux，會運行過 mkinitcpio 的命令，
  需要修改開機啟動的模組才需要重新修改和執行mkinitcpio命令

- 概念，BIOS 和 UEFI 在 bootloader、kernel、initramfs 上的差異
  - BIOS

    BIOS 將 bootloader、kernel、initramfs 區分為三個檔案，
    `mkinitcpio 可以用來建立 kernel、initramfs 兩個檔案`，
    bootloader 可以自由選用任意的第三方 bootloader 軟件進行手動安裝

  - UEFI
    
    UEFI 將 bootloader、kernel、initramfs 合併成一個可執行的EFI檔案，
    `新版的 mkinitcpio 可直接產生 EFI 檔案`，產生EFI可執行檔案後，
    再添加到 UEFI 開機項目就可順利載入內核和initramfs

- mkinitcpio 是一個創建 initramfs 和 kernel 的 bash 腳本
  - mkinitcpio 是 linux 套件提供的工具
  - mkinitcpio 將必要的內核模組、驅動程式、系統程式和配置文件，並打包成一個 initramfs 映像文件
  - mkinitcpio 生成initramfs 映像文件，會存儲在 /boot 分區中
  - /etc/mkinitcpio.conf 是主要配置文件，
  - /etc/mkinitcpio.d/linux.preset 是預設配置文件，當 mkinitcpio.conf 沒有設置時，才會從 linux.preset 中取用設置
    
- 主要配置文件，`/etc/mkinitcpio.conf` 的使用，
  - MODULES欄位: 虛擬系統要包含的核心模塊
    - 注意，mkinitcpio.conf 的`MODULES欄位`，和 linux.preset 的`PRESET欄位`是互斥的
    - 若有使用MODULES欄位，就會忽略PRESET欄位的值，
    - 若要實現最小化的 initramfs，應該手動設置 MODULES欄位

  - BINARIES欄位: 虛擬系統要包含的工具
    - 必要的 BINARIES，用於檢查檔案系統
      - for ext4: `BINARIES="fsck fsck.ext[2|3|4] e2fsck"`
      - for vfat: `BINARIES="fsck fsck.vfat dosfsck"`
      - for btrfs: `BINARIES="fsck fsck.btrfs btrfs btrfsck"`
      - for xfs: `BINARIES="fsck fsck.xfs xfs_repair"`

  - FILES欄位: 虛擬系統要包含的檔案

  - HOOKS欄位: 
    - Hook 是一個 shell 腳本，在生成 initramfs 時會被執行，用於`建立 initramfs 時需要執行的動作`，例如，
      - 在 initramfs 中安裝驅動程式
      - 在 initramfs 中安裝其他需要的工具
      - 在 initramfs 中執行特定的腳本，例如，filesystems-hook 來建立檔案系統

    - 注意，HOOK欄位中列出的hook文件會`按照順序執行`

    - 常用的 hook
      - `base`: 設置初始目錄、安裝基本工具，
        - 若使用systemd-hook時，可以省略 base-hook
        - 未使用systemd-hook時，base-hook 應放在所有 hook 中的第一位作為初始化
  
      - `systemd`: 
        - 添加和執行 udev、usr、resume 相關的功能，可以取代 base-hook
        - 會安裝 systemd 相關的模組，有可能會安裝不需要的模組
  
      - `udev`: 
        - 佔用空間大，可被 autodetect-hook 取代
        - 在系統中發現新的硬體設備時自動加載驅動，可被 systemd-hook 取代
        - 部分裝置可能會需要，例如，使用 Logitech Unified Receiver 的羅技滑鼠

      - `autodetect`: 常用，推薦，
        - 用於使 autodetect 後方hook安裝的模組進行精簡安裝，可減少initramfs的體積
          - 若使用 autodetect-hook，autodetect-hook 應放在其他子系統的hook之前，才能發揮最大效用
          - autodetect-hook 之前的 hook 會被完整安裝，autodetect-hook 會被精簡安裝，只安裝必要的驅動
      - `modconf`: 常用，系統啟動時，自動加載 /etc/modules-load.d 下的自定義模塊
      - `kms`: kernel-mode-setting，提供 early-KMS-start 功能，在 kernel 階段就能設置顯示器
      - `block`: 包含了塊設備驅動，如硬碟和光驅驅動
      - `net`: 提供網路設備的驅動
      - `filesystems`: 包含了文件系統驅動，如 ext4 和 ntfs
      - `fsck`: 提供 filesystems 檢查和修復的功能
      - `keyboard`: 包含了鍵盤驅動
      - `keymap`: 提供鍵盤映射的功能
      - `encrypt`: 包含了加密驅動，用來支援加密文件係
      - `sd-vconsole`: 使用shell時能夠使用額外的功能，如控製臺鍵盤配置和字符集設定，需要設置鍵位(/etc/vconsole.conf)
  
    - 官方預設的 HOOKS = (base udev autodetect keyboard keymap modconf block filesystems fsck)
    - 範例 HOOKS = (systemd autodetect)
      - systemd 可能會包含不必要的模組

  - COMPRESSION欄位: 用於壓縮 initramfs 的工具
    - cat: 建立未壓縮的 initramfs-image，initramfs 檔案不壓縮，可以減少解壓縮的步驟，加快開機速度
    - zstd: 預設壓縮方式，速度快，壓縮率高
    - lz4: 壓縮慢，但解壓縮速度最快，適合用來加速開機啟動
    - xz: 壓縮率最高，但解壓速度最慢

- 預設配置文件，`/etc/mkinitcpio.d/linux.preset` 的使用
  
  - `ALL_config欄位`: 
    - 例如，ALL_config="/etc/mkinitcpio.conf"，等效於 "mkinitcpio -c /etc/mkinitcpio.conf"
    - 用於指定 kinitcpio.conf 的路徑

  - `ALL_kver欄位`: 
    - 例如，ALL_kver="/boot/vmlinuz-linux"，等效於 "mkinitcpio -k /boot/vmlinuz-linux"
    - 用於指定 vmlinuz-linux 的路徑 (bios版本的kernel檔案)

  - `PRESETS欄位`: 選擇配置組合組
    - 例如，PRESETS=('default' 'fallback')
      - 每一個 preset 都會產生一個 initramfs 的 image 檔案

    - default-preset
      - 產生 /boot/initramfs-linux.img
      - 使用 base, udev, autodetect, modconf, block, keyboard, keymap, encrypt, filesystems, fsck 等 hook-shell-script
      - 透過 autodetect-hook，只保留精簡且必要的驅動
  
    - fallback-preset: 
      - 產生 /boot/initramfs-linux-fallback.img
      - 用於救援，不會有 autodetect-hook，會保留所有的驅動
      - 是備用的配置組合，在系統啓動時如果遇到問題而無法使用預設配置組合，就會使用 fallback 配置組合
  
  - `default_image欄位`: 
    - 例如，default_image="/boot/initramfs-linux.img"，等效於 "mkinitcpio -g /boot/initramfs-linux.img"
    - 用於指定initramfs 的輸出路徑

  - `default_uki欄位`:
    - 例如，default_efi_image="/boot/EFI/BOOT/BOOTX64.EFI"，等效於 "mkinitcpio -U /boot/EFI/BOOT/BOOTX64.EFI"
    - 用於指定 uefi-kernel-image 的輸出位置，預設輸出為 `/boot/EFI/BOOT/BOOTX64.EFI`，
    - uefi-kernel-image == EFI 可執行檔，BOOTX64.EFI 是一個可執行檔，具有 bootloader 的功能，用來啟動 kernel 和 initramfs
    - default_uki欄位 等效於早期 mkinitcpio 版本的 default_efi_image 欄位

  - `default_options欄位`:
    - 例如，default_options="--cmdline /etc/kernel/cmdline"，等效於 "mkinitcpio --cmdline /etc/kernel/cmdline"
    - 用於指定 kernel 啟動參數的檔案位置，需要手動將啟動參數寫在 /etc/kernel/cmdline，例如，
      ```shell
      echo 'root=LABEL=ROOT resume=LABEL=SWAP rootflags=rw fsck.mode=skip nomodeset quiet' > /etc/kernel/cmdline
      cat /etc/kernel/cmdline
      ```

- 利用 mkinitcpio 產生 `unified-kernel-image`
  - 什麼是 unified-kernel-image

    unified-kernel-image 是一種 Linux 內核組態，它`將多種內核配置選項和驅動程序合併`為一個鏡像文件。
    
    這種配置方式可以讓系統管理員在每個硬件平台上使用相同的內核文件，而`不需要針對每個硬件平台選擇特定的內核配置`，
    可以簡化內核維護和管理的過程。

    Unified-kernel-image 使用 EFI 格式打包，但`不限於只能用於 UEFI 系統`，
    只要在系統上安裝了 EFI 開機管理程序（EFI boot manager），就能載入和執行 unified-kernel-image

  - unified-kernel-image 和一般的 kernel image 的區別
    - Unified kernel image 比一般的 kernel image 大，因為包含了更多的內核配置選項和驅動程序。
    - Unified kernel image 支持更多的硬件平台
    - Unified kernel image 更難定制
    - Unified kernel image 更容易維護

  - 建立 unified-kernel-image 的幾種方法 (建立 BOOTX64.EFI 的方法)
      - 方法1，透過 mkinitcpio
      - 方法2，透過 kernel-install
      - 方法3，透過 dracut
      - 方法4，透過 sbctl
      - 方法5，手動建立
      - 參考，[Unified kernel image](https://wiki.archlinux.org/title/Unified_kernel_image#Preparing_a_unified_kernel_image)

  - 配置 unified-kernel-image 開機選項的幾種方法
    - 方法1，透過 system-boot: 自動尋找 `<ESP分區>/EFI/Linux` 目錄下的EFI可執行文件
    - 方法2，透過 rEFInd
    - 方法3，透過 efibootmgr
    - 參考，[UEFI Booting](https://wiki.archlinux.org/title/Unified_kernel_image#Booting)

- 範例，修改內核配置後，重新產生 initramfs
  ```shell
  # 修改主要配置文件
  echo "
  MODULES=(ext4 ahci)
  BINARIES=(fsck fsck.ext4 e2fsck)
  FILES=()
  # HOOKS=(base systemd autodetect modconf sd-vconsole)
  HOOKS=(base systemd autodetect modconf)
  COMPRESSION='zstd'
  " > /etc/mkinitcpio.conf

  # 修改預設配置文件
  echo "
  ALL_config='/etc/mkinitcpio.conf'
  ALL_kver='/boot/vmlinuz-linux'
  PRESETS=('default')
  default_image='/boot/initramfs-linux.img'
  " > /etc/mkinitcpio.d/linux.preset

  # 產生 initramfs 檔案
  mkinitcpio -P linux
  ```

- 範例，產生 uefi-kernel-image + 透過 efibootmgr 設置 EFI 開機選項
  - EFI可執行啟動程式，類似 bootloader 的作用，可以加載 kernel 和 initramfs
  - 注意，使用 uefi-kernel-image 就不需要再安裝 bootloader，`EFI可執行程式具有 bootloader 的功能`

  - step1，安裝 efibootmgr，`$ pacman -Sy efibootmgr`
  - step2，在 `/etc/mkinitcpio.d/linux.preset` 添加 `default_efi_image="/boot/EFI/BOOT/BOOTX64.EFI"`
  - step3，在 `/etc/mkinitcpio.d/linux.preset` 添加 `default_options='--cmdline /etc/kernel/cmdline"`
  - step4，添加內核的啟動參數，`$ echo 'root=LABEL=ROOT resume=LABEL=SWAP rootflags=rw fsck.mode=skip nomodeset quiet' > /etc/kernel/cmdline`
  - step5，執行 `$ mkinitcpio -P linux` 以產生 /boot/EFI/BOOT/BOOTX64.EFI 的檔案
  - step6，選用，在 chroot 環境下執行 `$ efibootmgr --create --disk /dev/sda --part 1 --loader '/EFI/BOOT/BOOTX64.EFI' --label "Arch Linux"`
    - 此命令會在開機進入 UEFI-bootloader 後，執行 /EFI/BOOT/BOOTX64.EFI，透過此EFI程式載入內核和initramfs
    - 注意，efibootmgr 似乎不是必需的，virtualbox + archlinux-2022.12.01-x86_64.iso 下測試，即使不執行此命令也能進入系統
  
  - 完整範例，見 [packer-vbox-arch-optionC](https://gitlab.com/anthony076/docker-projects/-/tree/main/packer/example/packer-vbox-arch-optionC)

- 範例，[最小化initramfs](install-arch/minimum_bood-speedup.md#範例最小化-initramfs-範例)

## 使 initramfs 包含自定義的驅動

- 在 mkinitcpio 的 HOOKS 選項中指定一個自定義的 hook, 例如 "myhook"。
- 在 /etc/mkinitcpio.d/ 目錄下建立一個名為 myhook 的腳本
- 在腳本中添加 modprobe 指令，例如: `modprobe mydriver`
- 在 mkinitcpio.conf 文件中的 HOOKS 選項中，添加 "myhook" 到適當位置

## Ref

- [linux 的內核啟動參數](https://docs.kernel.org/admin-guide/kernel-parameters.html)
