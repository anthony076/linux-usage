## archlinx 基礎

- 安裝
  - [安裝 archlinux 概論](install-arch/overview.md)
  - [安裝 archlinux on vmware](install-arch/install-arch-on-vmware.md)
  - [最小化arch+開機優化](install-arch/minimum_bood-speedup.md)
  
- 套件
  - [pacman的使用](pacman.md)

- 進階
  - [arch + kernel](arch%2Bkernel.md)

## Ref

- [Archlinux 的啟動流程](https://wiki.archlinux.org/title/Arch_boot_process)

- [Archlinux 的啟動流程-中文](https://wiki.archlinuxcn.org/wiki/Arch_%E7%9A%84%E5%90%AF%E5%8A%A8%E6%B5%81%E7%A8%8B#%E5%BC%95%E5%AF%BC%E5%8A%A0%E8%BD%BD%E7%A8%8B%E5%BA%8F)


