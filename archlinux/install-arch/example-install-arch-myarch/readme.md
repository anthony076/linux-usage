## install-myarch

- 在 initial.sh 中執行 chroot.sh 的方法
  - 原因: chroot.sh 需要放在掛載的路徑才能執行，且執行 mount 指令會請空掛載路徑的內容
  - 解決方式: 將 chroot.sh 先放在臨時的檔案系統中，待 mount 指令執行完畢後，再複製到掛載路徑中
  - step1，將 shell-script 都存放在 archlinux-iso 建立的(臨時的)檔案系統，
  - step2，待 mnt 的指令後，再將 chroot.sh 從臨時檔案系統複製到真實檔案系統(掛載的檔案系統)中
  - step3，執行 arch-chroot 並指定 chroot.sh 真實檔案系統(掛載的檔案系統) 的路徑

- 執行方式

  - step1，virtualbox 的設置
    - 在 virtualbox 中建立 vm，
    - 添加 port-22 的 port-forwarding
    - 設置 enable efi
  - step2，進入安裝介面後，`$ passwd root`，變更密碼後，改在 windows 上透過 ssh 登入
  - step3，`$ nano /tmp/initial.sh`，將 initial.sh 的內容複製到 /tmp/initial.sh
  - step4，`$ nano /tmp/chroot.sh`，將 chroot.sh 的內容複製到 /tmp/chroot.sh
  - step5，執行 `$ bash /tmp/initial.sh` 進行安裝

## 測試
- $ pacman -Rs linux --noconfirm
  - FAIL，linux 與 virtualbox-guest-utils-nox 依賴

- /etc/mkinitcpio.conf
  ```shell
  # 使用此設定值
  MODULES=(ext4 ahci)
  BINARIES=()
  FILES=()
  HOOKS=(base)
  COMPRESSION='lz4'
  ```

  結果
  
  <img src="doc/test1.png" width=800 height=auto>

- /etc/mkinitcpio.conf，
  ```shell
  MODULES=(ext4 ahci)
  BINARIES=()
  FILES=()
  HOOKS=()
  COMPRESSION='lz4'
  ```

  結果，FAIL，no hook found