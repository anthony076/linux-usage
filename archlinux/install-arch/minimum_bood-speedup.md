## 最小化 + 加速啟動

- [利用 bootstrap 安裝最小化的 archlinux 系統](install-bootstrap.md#範例利用-arch-boostrap-安裝最小化的-archlinux)

- 注意，最小化的同時，只保留符合當前硬體的驅動，
  會造成新增硬體時出現錯誤，因為找不到對應的驅動

- 啟動主機的最小驅動(最小 initramfs)，只需要考慮
  - block-device(storage bus)，例如，PATA, SATA, SCSI ...
    - sd_mod module: all SCSI, SATA, and PATA (IDE) devices
    - ahci module: SATA devices on modern AHCI controllers
    - nvme and nvme_core module: NVMe (M.2, PCI-E) devices
    - sata_* module: SATA devices on IDE-mode controllers
    - pata_* module: PATA (IDE) devices
    - ehci_pci and usb_storage module: USB storage devices
    - virtio_blk and virtio_pci module: QEMU/KVM VMs using VirtIO for storage
  
  - character-device(keyboard)
    - atkbd module: AT and PS/2 keyboards, and the emulated keyboard in QEMU/KVM.
    - hid_generic/ohci_pci/usbhid module: normal USB keyboards.
    - hid_logitech_dj/uhci_hcd/usbhid module: 
    - udv module: 使用 Logitech Unified Receiver 的羅技USB鍵盤

  - filesystem: 根據 root 分區選擇
    - ext[2,3,4]
    - xfs
    - jfs
    - reiserfs

- `選項`，選擇合適的檔案系統
  - for HDD: 使用 xfs 檔案系統
  - for SSD: 使用 f2fs 檔案系統
  - for FLASH: 使用 f2fs 檔案系統，且禁用 TRIM 功能

- `選項`，更換 bootloader
  - 方法1，使用 Unified-kernel-image，直接從 UEFI 開機
  - 方法2，若使用 BIOS，改用 systemd-boot 加快啟動速度

- `選項`，修改 /etc/mkinitcpio.conf 和 mkinitcpio.preset
  - 手動指定模組，MODULES欄位為空，mkinitcpio 會添加預設模塊
  - 使用 lz4 進行壓縮，雖然 lz4 壓縮慢，但解壓縮速度最快，適合用來加速開機啟動
  - 不使用 udev modules
  - 不使用 fallback preset
  - 使用 autodetect 自動搜尋需要的模組

- `選項`，透過內核參數，關閉 Staggered spin-up，
  - 檢查是否開啟 staggered spin-up，`$ dmesg | grep SSS`
  - 透過內核參數關閉，`libahci.ignore_sss=1`

- `選項`，透過內核參數，避免 root 分區重新掛載，
  - 修改內核參數，將 `rootflags=ro,其他選項`，改為`rootflags=rw,其他選項`
  - 將 root 分區從 fstab 刪除，或將 systemd-remount-fs.service 屏蔽，以免 root 分區重新掛載仍然有效
  - 注意，若root分區使用 btrfs 的格式，就不需要進行此操作，同時也不需要 fsck-hook
    - step1，添加內核參數，`fsck.mode=skip` 來屏蔽 fsck-hook
    - step2，`systemctl mask systemd-fsck-root.service` 關閉 fsck 的功能
      > 即使沒有initramfs沒有 fsck hook，systemd 仍然會透過 systemd-fsck@.service 對檔案系統進行 fsck 的操作

- `選項`，關閉其他檔案系統的自動掛載(例如，EFI分區)，改由 systemd 在第一次存取時自動掛載
  - 在 fstab 中，添加 noauto,x-systemd.automount 的掛載選項，例如，
    ```shell
    # /dev/sda1 UUID=CDAA-403E
    LABEL=EFI           	/boot     	vfat      	noauto,x-systemd.automount,rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=ascii,shortname=mixed,utf8,errors=remount-ro	0 2
    ```

- `選項`，透過內核參數，啟用靜默啟動
  - 修改內核參數，添加 `quiet loglevel=3`
  - 以 grub 的內核啟動參數為例
    ```shell
    GRUB_CMDLINE_LINUX_DEFAULT="quiet loglevel=3 rd.systemd.show_status=auto rd.udev.log_priority=3 vt.global_cursor_default=0 i915.fastboot=1"

    GRUB_CMDLINE_LINUX="quiet loglevel=3 rd.systemd.show_status=auto rd.udev.log_priority=3 vt.global_cursor_default=0 i915.fastboot=1"
    ```

- `選項`，使用 systemd-analyze blame 分析開機啟動項消耗的時間

- `選項`，透過 `$ dmesg` 查看開機紀錄，查看任何有用的訊息

- [檢視核心模組](../arch+kernel.md#檢視核心模組檢視驅動)

## 系統優化

- `優化`，透過內核參數優化效能
  - `Mitigations=off`，
    - 關閉漏洞緩解
    - 缺點，提高性能但會帶來安全風險

  - `fsck.mode=skip`，關閉檔案系統檢查
    - 對 xfs 檔案系統無效
    - 對 f2fs、ext4 檔案系統推薦開啟 fsck
  
  - `libahci.ignore_sss=1`，一次加載所有 HDD
    - 只對擁有多個HDD的系統才看的到效果
    - 一次加載所有 HDD 會需要大量的電流，若沒有良好電源，不推薦使用

  - `quiet loglevel=0`，靜默啟動，減少TTY輸出造成的延遲
    - 需要保持此順序，quiet 在前，loglevel 在後
  
  - `nowatchdog`，關閉 watchdog

- `優化`，使用自定義內核
  - 使用 Xanmod-kernel 或 liquorix-kernel 或 zen-kernel，取代 stock-kernel
  - 推薦使用 Xanmod > liquorix > zen

- `優化`，停用服務
  - 利用 `$ systemd-analyze blame` 查看開機過程中，每個服務的花費時間
  - 關閉不需要的服務
    - 例如，使用第三方的網路管理器，就不需要啟動 systemd-resolved 服務
    - 例如，若沒有使用 lvm，就不需要啟動 lvm2-monitor 服務

- `優化`，將不需要的 module 添加到 blacklist
  - 修改 `/etc/modprobe.d/blacklist.conf`
    ```shell
    # /etc/modprobe.d/blacklists.conf
    blacklist pcspkr    # speaker
    blacklist joydev    # 搖桿
    blacklist mousedev  # ps2
    blacklist mac_hid   # MAC 用 HID 驅動
    blacklist uvcvideo  # webcam
    ```

- `優化`，強化網路連接速度，添加 goole 和 Cloudflare 的 DNS
  - 以 /etc/resolv.conf 為例，添加以下
    ```shell
    options timeout:1
    options single-request

    nameserver 8.8.8.8
    nameserver 8.8.4.4
    nameserver 1.1.1.1
    ```
  
  - 注意，若使用第三方的網路連接套件，會複寫 resolv.conf 的值，例如，NetworkManager
    - 方法1，停止 NetworkManager 的 DNS 解析功能
    ```shell
    # /etc/NetworkManager
    [main]
    dns=none  # 禁用 NetworkManager 的DNS功能
    systemd-resolved=false  # 關閉 NetworkManager 的DNS服務
    ```

    - 方法2，修改 resolv.conf 的屬性，使 resolv.conf 無法被複寫
      > 執行，chattr +i /etc/resolv.conf

- `優化`，禁用日誌

  - 修改 `/etc/systemd/journald.conf`，添加以下
    ```shell
    Storage=none
    ```
  - 此步驟不會使 journal daemon 失效，但可以減少寫入磁碟的操作

- `優化`，[使用 IO Scheduler](https://www.linkedin.com/pulse/how-make-your-archlinux-faster-sourav-goswami)

## 範例，最小化 initramfs 範例

```shell
# /etc/mkinitcpio.conf

MODULES="ahci sd_mod ext4"    # 指定 module，避免MODULES為空，若 MODULES 為空，會使用 default-preset 安裝模組
BINARIES="fsck fsck.ext4"     # 安裝 root 分區的工具，就不需要 filesystems-hook
HOOKS="base"                  # 只需要安裝基本的模組
```

## Ref
- [How to make your Archlinux faster](https://www.linkedin.com/pulse/how-make-your-archlinux-faster-sourav-goswami)

- [Faster Arch Linux](https://github.com/Th3Whit3Wolf/Faster-Arch)

- [使用 mkinitcpio 優化啟動](https://blog.falconindy.com/articles/optmizing-bootup-with-mkinitcpio.html)