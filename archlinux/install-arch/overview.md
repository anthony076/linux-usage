## install-archlinux-overview

- 安裝方式有兩種
  - `方法1`，自動安裝，透過官方內建的工具
  
    在 iso 檔案中，有內建 archlinux 的 shell-script 可執行自動安裝

    step1，進入shell系統後，輸入 `$ archinstall`

    <img src="doc/install-archlinux/auto-install-1.png" width=80% height=auto>

    step2，透過簡單的配置就可執行自動安裝

    <img src="doc/install-archlinux/auto-install-2.png" width=80% height=auto>
  
  - `方法2`，自動安裝，透過第三方的 archfi-shell-script (arch-fast-installer)
    - [archfi官網](https://github.com/MatMoul/archfi)
    - [archfi使用範例](https://www.youtube.com/watch?v=EJQZKqgajSc)
    - step1，進入 iso-shell 後，透過 `$ wget archfi.sf.net/archfi` 下載 script
    - step2，執行 `$ bash archfi` 進入安裝介面
    - 推薦，可以查看實際執行的命令和流程

  - `方法3`，手動安裝

  - `方法4`，[透過 bootstrap 安裝](install-bootstrap.md)

- 手動安裝流程概述
  - 進入安裝系統，設置 root 密碼，使 ssh 有效
  
  - `階段1`: arch-chroot `之前`的環境準備 
    - (initial-install)，準備環境，才能進行 arch-chroot 的安裝
    - 分割硬碟，建立磁碟分割表(partition-table)
    - 格式化硬碟，建立檔案系統
    - 掛載硬碟，建立掛載表(filesystem-table)
    - 安裝內核和系統工具
    - 清理套件、快取
  
  - `階段2`: arch-chroot 系統安裝        
    - (chroot-install)，受限環境，只能進行基礎系統的安裝，例如，systemctl 的部分命令不可用

    - 注意，階段2的命令都需要進入實際的作業系統環境(實際的root分區)執行
      - 方法1，透過 `arch-chroot /mnt` 進入後，再執行以下命令
      - 方法2，將以下命令寫成 chroot.sh，再透過 `arch-chroot /mnt chroot.sh` 執行

    - 安裝內建第三方軟體
    - 設置主機名、系統時間、時區、語系、鍵盤位置、DNS解析、字型、輸入法、
    - 設置網路連接管理器
    - 設置網路解析(選用)
    - 添加帳戶、設置密碼
    - 設置 ssh
    - 禁用功能
    - 功能優化
    - 建立開機用的虛擬檔案系統(initramfs)
    - 安裝 bootloader

  - `階段3`: arch-chroot `之後`的環境設置，
    - 完整環境，用於 arch-chroot 無法處理的安裝

- [最小化安裝+啟動優化](minimum_bood-speedup.md)

- 安裝範例
  - 最小安裝，[example-install-arch-minimun](example-install-arch-minimun/readme.md)
  - 自定義環境，[example-install-arch-myarch](example-install-arch-myarch/readme.md)
  - 一鍵安裝範例，[packer-vbox-myarch](https://gitlab.com/anthony076/docker-projects/-/tree/main/packer/example/packer-vbox-myarch)

- 注意事項，
  - `initial.sh`: 用於初始化 chroot.sh 的安裝環境
    - mount 的操作，會將 /mnt 路徑的內容清空，有要執行的 script 應該在 mount 之後才放到 /mnt 的路徑
  
    - 注意，arch-chroot `不能直接直接執行命令`，任何命令都必須`寫在 shell-script` 中執行  
      - 若以`root身份` 進入arch-root，`只能透過 bash 執行 shell-script`，
        例如，`$ arch-chroot /mnt bash test.sh`
      
      - 若以`一般使用者身份` 進入arch-root，可以`直接執行 shell-script`，不需要透過 bash，
        例如，`$ arch-chroot su vagranttest.sh`

      - 差異
        
        arch-chroot 的目的是切換到安裝目錄並在安裝目錄中執行命令，
        而`不是直接執行命令`，因此需要提供 shell 環境執行腳本
        
        若進入一般使用者後，預設都會開啟 shell 環境 (執行useradd命令時添加了 shell)，
        因此不需要提供 shell 環境執行腳本

    - 要實現一鍵安裝，
      - 透過`環境變數`傳遞目錄的路徑
      - 先將shell-script暫存在 iso 提供臨時的檔案系統，再透過 cp 指令轉移到實際檔案系統中
  
    - 安裝 paru (aur-helper) `必須使用一般使用者進行安裝`，進入 arch-chroot + 一般使用者有以下限制
      - 限制: arch-chroot 切換到`一般使用者`時，不能直接執行命令，但可以執行 shell-script
      - 錯誤用法: arch-chroot /mnt su vagrant `bash` $MNT_SCRIPTS/aur.sh
      - 正確用法: arch-chroot /mnt su vagrant $MNT_SCRIPTS/aur.sh

  - `chroot.sh`: 用於進入實際檔案系統的安裝

    在此環境下，systemd 的部分工具無法在 chroot 的環境下執行，例如，hostnamectl、localectl、timedatectl ... 等

    任何需要 `actiive-debus-connection` 的工具都無法在 chroot 下執行，[參考](https://wiki.archlinux.org/title/Chroot#Usage)

    可以改在重開機進入系統後再執行，或在 chroot 下，以建立連結的方式執行

  - `chroot.sh` 一定要放到 /mnt 的路徑中才能執行，
    
    進入安裝系統後，ISO 提供的 shell 是臨時的檔案系統，
    實際的檔案系統要經過分區 -> 格式化 -> 掛載到 /mnt 路徑後，
    透過 arch-chroot 進入實際的檔案系統

    arch-chroot 切換後，/mnt 以外的目錄都是無效的，因此，chroot.sh 一定要放到 /mnt 的路徑中才能執行

  - `aur.sh`
    - 必須透過一般使用者執行 aur.sh
    - 進入 arch-chroot，並切換到一般使用者後的限制
      - 不能使用相對路徑，透過 $HOME 存取 /home/使用者，例如
      - 下載需要指定絕對路徑，例如，git clone $HOME/tmp
      - cd $HOME 取得 cd ~
      - rm -rf $HOME/tmp

  - bootloader 的第三方工具，除了內建的 systemd-boot(bootctl install) 外，第三方的 bootloader 需要手動安裝
  
  - 使用 pacstrap 前，新版的 archlinux 必需要安裝GPL驗證工具，以驗證 pacstrap 的安裝

  - pacstrap 必須安裝 linux 包

    mkinitcpio 套件已經從 base 包移到 linux 包，
    而 chroot 中必須調用 mkinitcpio 來產生 initramfs，否則會無法進入系統，
    因此，pacstrap 必須安裝 linux，

    若要減少體積，可於安裝完成後移除 linux 包

## 推薦套件

- boot 相關
  - efimanager: 用於建立 efi/uefi 的開機選單
  - os-probe: 用於偵測多系統的開機選單 (for grub)

- bootloader 相關
  - syslinux: 輕量級的 bootloader 安裝
  - systemd-boot: systemd 提供安裝 bootloader 的工具
  - grub: 安裝 bootloader

- pacman 相關
  - reflector: 使 pacman 選擇最快鏡像
  
- Network Managers (網路連接管理器)
  - 用途: 用於連接網路，負責管理整個系統的網路組態，例如
    - 網路卡的管理 (Network-Interface-Controller，NIC)
    - 有線網路、無線網路的連線、路由、IP 設定等

  - `選擇1`，NetworkManager: 
    - 優缺點
      - 高階的網路管理軟體，用於自動配置網路，不需要手動配置
      - 可以跨越多個不同的網路類型 (如 Wi-Fi、Ethernet、VPN) 進行統一的管理。
      - 自動偵測和連接到可用的網路，並具有圖形化的使用者介面
      - 內建 dhcpcd 
      - 缺，體積占用大
    
  - `選擇2`，netctl: 
    - 配置簡單
    - 類似NetworkManager，systemd 提供管理物理網路連接的方法

  - `選擇3`，systemd-networkd: 推薦，但不支援 PPPoE，需要額外安裝 pppd
    - 高效的網路管理軟體，它是 systemd 的一部分，可以支援多個不同的網路類型
    - 可以支援 VLAN、DHCP、DNS 解析、DNSSEC、DNS over TLS、群播 DNS (mDNS) 及 LLNMR
    - 支援較新的協定，使用上較安全

  - `選擇4`，dhcpcd
    - 較低階的 DHCP 客戶端軟體，用來自動配置 IP 位址和 DNS 伺服器
    - 只能自動配置 IP 位址和 DNS 伺服器，不能管理其他網路設定
    - 缺點，無法自動更新 DNS 解析設定，需要搭配 DNS解析軟體使用，例如，systemd-resolvconf

- DNS快取
  - 用途: 建立 DNS-Server，利用 DNS-Server 提供 DNS 快取的服務，可以減少 DNS解析的次數，用於增加網路連接速度
  - DNSmasq: 使用簡單、高效、資源佔用小，適合小型網絡
  - MaraDNS: 使用簡單、高效、安全、資源佔用小，適合小型網絡
  - Unbound: 使用簡單、高效、安全、資源佔用小，適合小型網絡

- filesystem
  - dosfstools: fat32 file support
  - ntfs-3g: ntfs file support
  - btrfs-progs: btrfs file utils
  - exfat-utils: exfat file support
  - gptfdisk
  - autofs
  - fuse3

- Desktop 
  - archdi: [arch-desktop-install](https://github.com/MatMoul/archdi)

- font
  - noto-fonts
  - ttf-hack: console fonts

- man 
  - man-db: 提供 man 命令和搜尋功能，用於管理 pages 的工具
  - man-pages: 提供 man 的內容

<font color=red>以下代碼僅提供範例，不保證能正常運行</font>

## [initial] 分割硬碟，建立磁碟分區(建立partition-table)

- 參考，[分割硬碟工具的使用](../../linux-core/00_disk-partition.md)

- 參考，UEFI的EFI分區，[可以設置為300MB](https://wiki.archlinux.org/title/Installation_guide_(%E6%AD%A3%E9%AB%94%E4%B8%AD%E6%96%87)#%E5%88%86%E5%8D%80%E7%A4%BA%E4%BE%8B)

- 範例1，for efi
  ```shell
  # 建立 GPT 
  /usr/bin/sgdisk --zap-all /dev/sda
  /usr/bin/dd if=/dev/zero of=/dev/sda bs=512 count=2048
  /usr/bin/wipefs --all /dev/sda
  sgdisk -o -g /dev/sda

  sgdisk -n 1:0:+512M -c 1:"efi" -t 1:ef00 /dev/sda
  sgdisk -n 2:0:+8G -c 2:"root" -t 2:8304 /dev/sda
  sgdisk -n 3:0:0 -c 3:"swap" -t 3:8200 /dev/sda
  ```

- 範例2，for mbr
  ```shell
  /usr/bin/sgdisk --zap-all /dev/sda
  /usr/bin/dd if=/dev/zero of=/dev/sda bs=512 count=2048
  /usr/bin/wipefs --all /dev/sda
  sgdisk -og /dev/sda

  sgdisk -n 1:2048:4095 -c 1:"biosboot" -t 1:ef02 /dev/sda
  sgdisk -n 2:4096:0 -c 2:"root" -t 2:8300 /dev/sda
  ```

- 範例3，parted + grub bootloader
  ```shell
  # 建立標籤
  parted -s /dev/sda mklabel msdos
  # 建立分區
  parted -s -a optimal /dev/sda mkpart primary 0% 100%
  # 設置屬性，設置為可引導分區
  parted -s /dev/sda set 1 boot on

  # 格式化為 ext4
  mkfs.ext4 -F "$PARTITION"
  # 掛載到系統
  mount "$PARTITION" /mnt

  # 產生掛載表
  genfstab -p /mnt >> /mnt/etc/fstab

  arch-chroot /mnt

  # 安裝 grub bootloader
  grub-install --target=i386-pc --recheck /dev/sda
  # 產生配置檔
  grub-mkconfig -o /boot/grub/grub.cfg
  ```

## [initial] 將磁碟分區進行格式化(建立檔案系統)

- 範例，
  - sda1 格式化為 FAT32，`$ mkfs.fat -F32 -n "EFI" /dev/sda1`
  - sda2 格式化為 ext4，`$ mkfs.ext4 -L "ROOT" /dev/sda2`
  - sda3 格式化為 swap，`$ mkswap -L "SWAP" /dev/sda3`
  - 啟用 swap 分區，`$ swapon /dev/sda3`
  - 將結果寫入硬碟，`$ sync`

## [initial] 將磁碟分區掛載到系統路徑 + 建立掛載表(filesystem-table)

- 將磁碟分區掛載到系統路徑
  - <font color=red> 注意 mount 順序，掛載路徑最短的磁碟分區要先掛載 </font>
    - 掛載順序錯誤，會導致 fstab 中缺少分區
    - 因此，sda2 (/mnt) 必須要比 sda1 (/mnt/boot) 先掛載
    - 範例
      ```shell
      mount /dev/sda2 /mnt  # 先掛載短路徑

      mkdir -p /mnt/boot
      mount /dev/sda1 /mnt/boot # 再掛載長路徑
      ```

  - 若是 efi 分區，
    - efi 分區需要掛載到 /boot 路徑
    - 命令，建立路徑，`$ mkdir -p /mnt/boot`
    - 命令，掛載 sda1 分區到 /mnt/boot，`mount /dev/sda1 /mnt/boot`

  - 若是 swap 分區，
    - 參考，[建立swap的兩種方式](../../linux-core/00_disk-partition.md#建立-swap-分區)

    - 命令，建立 swap 分區，`sgdisk -n 3:0:0 -c 3:"swap" -t 3:8200 /dev/sda`
    - 命令，sda3 格式化為 swap，`$ mkswap -L "SWAP" /dev/sda3`
    - 命令，啟用 swap 分區，`$ swapon /dev/sda3`

  - 若是一般分區，
    - 命令，`$ mount /dev/sda2 /mnt`

- 建立掛載表(filesystem-table，fstab)
  - fstab 記錄了系統中所有掛載的文件系統的信息

  - 命令，建立目錄，`$ mkdir -p /mnt/etc`

  - 命令，`$ genfstab -p /mnt >> /mnt/etc/fstab` 或 `$ genfstab -L /mnt >> /mnt/etc/fstab`
    - -p，使用文件系统路径来识别文件系统 (path)，例如，`/dev/sda1  /  ext4  defaults  0  1`
    - -L 或 -U，使用 UUID 来识别文件系统 (label)，例如，`UUID=12345678-1234-1234-1234-1234567890ab / ext4 defaults 0 1`
    - 推薦使用 uuid 取代 path，在更換硬碟或者換了硬碟的分區時，不會因為path不同產生問題

  - 命令，將 /boot 分區的 rw 標誌替換為 rw,noauto,x-systemd.automount
    > sed -i -E '/\/boot/ s/(rw,\S*)/\1,noauto,x-systemd.automount/' /mnt/etc/fstab

- fstab 的內容
  - 格式: <裝置名或檔案名> <掛載目錄>  <掛載裝置類型>  <掛載選項>   <是否需要備份>  <關機檢查>
  - 例如: swap               /tmp      swap           defaults        0           0

  - `可用的掛載選項`
    - 注意，可用的掛載選項需要和檔案系統搭配使用

    - defaults: 使用系統預設選項，啟用 rw suid dev exec auto nouser async 等選項
    - rw: filesystem將被掛載為讀寫模式
    - ro: filesystem將被掛載為僅讀模式
    - relatim：使用相對時間，而不是絕對時間
    - fmask=0022：文件的預設權限是 644，即-rw-r--r-- (file-mask)
    - dmask=0022：目錄的預設權限是 755，即drwxr-xr-x (directory-mask)
    - iocharset=ascii：使用 ASCII 字符集。
    - shortname=mixed,utf8：短文件名支援混合模式和 UTF-8。
    - suid: 可解讀 filesystem 內具有 SET UID 權限的檔案
    - exec: 可執行 filesystem 中的二進制檔
    - async: filesystem 已非同步方式進行IO
    - nouser: 一般 user 無法掛載
    - noauto: filesystem不會在系統啓動時自動掛載
    - noatime: 不更新文件的訪問時間
    - noexec: 禁止在掛載點上執行二進製文件
    - nosuid: 禁止`set-user-ID`和`set-group-ID`位
    - x-systemd.automount: 該filesystem將被 systemd 管理並在需要時自動掛載
    - errors=remount-ro：遇到錯誤時將掛載點重新掛載為隻讀。

  - `可用的是否需要備份選項`
    - 0: 不備份
    - 1: 每1日備份
    - 2: 每2日備份
  
  - `可用的開機檢查選項`
    - 用於讓fsck決定，當系統不正常關機時，檔案系統檢查的順序
    
    - 0: 不需要被檢查
    - 1: 先檢查根目錄
    - 2: 先檢查其他

  - 設置範例: 增加啟動速度的設置範例，noauto 和 x-systemd.automount 的搭配使用
    - noauto: 避免開機時自動掛載該檔案系統，需要手動或透過其他方式掛載檔案系統
    - x-systemd.automount: 提供了設置noauto後，掛載檔案系統的方式，由systemd在需要時自動掛載檔案系統
    - 優點，可以在系統啓動時節省資源,在使用時快速訪問filesystem
    - 若設置`noauto, x-systemd.automount`的選項，就可關閉`systemd-remount-fs.service`，以加快開機速度

## [initial] 安裝系統工具

- 比較，pacstrap 和 pacman 安裝軟體的差別
  - 兩者都是將軟體安裝在新的 root 分區上
  - pacstrap 用於安裝`基本系統和系統工具`，pacman 用於安裝`第三方套件`
  - pacstrap 使用的是ArchLinux預設的軟體源，pacman 使用的是使用者自定義的軟體源
    
    對於同樣的軟體，例如，nano，pacstrap 安裝的不一定是最新版本，透過 pacman 可以將軟體更新為最新版

  - 選擇方式
    - 系統軟體透過 pacstrap 安裝，確保是最新版本的系統軟體
    - 第三方軟體透過 pacman 安裝，確保是最新版本的第三方軟體

- `step1`，修改內建的 mirrorlist，提高 pacstrap 的下載速度
  ```shell
  # 列表參考 https://archlinux.org/mirrors/status/#successful

  mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist_bak

  echo "
  Server = https://mirror.archlinux.tw/ArchLinux/\$repo/os/\$arch
  Server = https://archlinux.cs.nycu.edu.tw/\$repo/os/\$arch
  Server = https://free.nchc.org.tw/arch/\$repo/os/\$arch
  " > /etc/pacman.d/mirrorlist
  ```

- `step2`，使 pacman 使用多線程下載軟體
  - 安裝 axel，`$ pacman -Sy axel --noconfirm`
  - 修改配置，`$ sed -i 's/#XferCommand = \/usr\/bin\/curl -L -C - -f -o %o %u/XferCommand = \/usr\/bin\/axel -n 5 -a -o %o %u/g' /etc/pacman.conf`

- `step3`，安裝GPL驗證工具
  - 安裝內核和系統工具前，需要先安裝 archlinux-keyring，以確保系統可以驗證下載的套件的GPL簽名 
  - 命令，`$ pacman -Sy archlinux-keyring --noconfirm`

- `step4`，安裝內核 + 系統工具
  - 真實系統掛載在 /mnt 的位置
  - 命令，`$ pacstrap /mnt base base-devel linux linux-firmware grub`，官方預設安裝的系統工具
    - base: 必要，基本工具包
    - base-devel: 選用，基本開發工具包
    - linux: 選用，linux 內核，需要修改內核才需要
    - linux-firmwar: 選用，linux kernel drivers，包括驅動程式所需的firmware，最小系統可省略
    - grub: 選用，bootloader 管理

- linux 內核的其他選擇
  - linux:最新，但最不穩定
  - linux-zen: 效能最好，針對性能進行優化
  - linux-lts: 最穩定+最小化 linux 內核
  - linux-hardened: 提供了加强的安全性
  - [介紹](https://www.youtube.com/watch?v=ptjCJNkjJRg)

## [chroot] 推薦第三方套件

- 推薦的第三方套件，`pacman -S nano openssh sudo virtualbox-guest-utils-nox --noconfirm`

## [chroot] 設置主機名

echo 'myarch-pc' > /etc/hostname

## [chroot] 設置同步系統時間

- 概念，兩種同步系統時間的方式，ntp 和 hwclock，差別在`提供同步的時間源`不同

  <font color=blue>hwclock，從硬體時鍾同步系統時間</font>
  
  hwclock 命令可以用來讀取或修改硬體時鍾上的時間。
  
  硬體時鍾是計算機硬體中內置的一個時鍾，是一個獨立於系統運行的時鍾，
  有自己的電源，因此即使系統關機或斷電，硬體時鍾也能正常運作
  
  在系統關機時將系統時間保存在硬體時鐘，並於重啓後，從硬體時鐘讀取系統時間

  硬體時鍾也有一定的誤差，並且長期運行會導緻時間誤差越來越大
  
  <font color=blue>ntp，從網路同步系統時間</font>

  通過連接NTP服務器自動同步系統時間，並且會定期執行

  <font color=blue>選擇方式</font>
  不需要同時使用兩種同步系統時間的機制，有網路時使用NTP，否則使用 hwclock
  
- 方法1，透過網路同步系統時間 (NTP，network-time-protocol)，`$ timedatectl set-ntp true`
  - <font color=red>注意，不能在 arch-chroot 下執行</font>，此命令用於 initial-install 階段，在initial-install同步系統時間
  - ntp，使系統時間能夠自動和 Internet 時間服務器同步
  - 執行後，系統將定時與 NTP 服務器進行通信，以確保時間的準確性。

- 方法2，透過硬體同步系統時間，
  - 將系統時間寫入硬體時鐘，`$ hwclock -systohc`
  - 啟用系統時間同步服務，`$ systemctl enable systemd-timesyncd.service`

## [chroot] 設置時區(timezone)

- 方法1，`$ timedatectl set-timezone Asia/Taipei`，設定系統時區，不能在 arch-root 執行
  - <font color=red>注意，不能在 arch-chroot 下執行</font>，此命令用於 initial-install 階段，在initial-install同步系統時間
  
- 方法2，`$ ln -sf /usr/share/zoneinfo/Asia/Taipei /etc/localtime`，僅設置時區，需要在 arch-root 執行

## [chroot] 產生語系

```shell
echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen
locale-gen
echo 'LANG=en_US.UTF-8' > /etc/locale.conf
```

## [chroot] 設置鍵盤位置

- 以 sd-vconsole 為例，`$ echo 'KEYMAP=us' > /etc/vconsole.conf`

## [chroot] 產生開機用的虛擬檔案系統(initramfs)

- [使用 mkinitcpio 產生 initramfs-image 或 unified-kernel-image](../arch+kernel.md#使用-mkinitcpio-產生-initramfs-image-或-unified-kernel-image)

## [chroot] 安裝第三方的 bootloader

- 注意，安裝 bootloader 前，透過 lsblk 確認 /boot 路徑上已掛載磁碟

- 範例1，透過 syslinux 安裝 MBR
  - 安裝 syslinux，`$ pacman -S syslinux --noconfirm`  
  - 安裝 bootloader，`$ arch-chroot /mnt syslinux-install_update -i -a -m`
    - -i 安裝 Syslinux
    - -a Set Boot flag on boot partiton
    - -m 安裝 Syslinux MBR
  - 修改配置，指定 efi 分區的磁碟分區，`$ sed -i "s|sda3|sda1|" "/mnt/boot/syslinux/syslinux.cfg"`
  - 修改配置，將 TIMEOUT 設置為 10s，`$ sed -i 's/TIMEOUT 50/TIMEOUT 10/' "/mnt/boot/syslinux/syslinux.cfg"`

- 範例2，透過 systemd-boot 將，grub-bootloader 安裝在 EFI 分區
  - 命令，安裝 uefi-bootloader，`$ bootctl install`
    - 此命令利用 systemd-boot 安裝 uefi-bootloader 到 EFI 分區中，
      並創建開機菜單配置文件。它也會創建默認的開機配置文件，以便系統能夠開機
  
  - 命令，刪除 uefi-bootloader 的預設配置，`$ rm -fRv /boot/loader/entries/*`
    - -f: 強制刪除
    - -R: 包含刪除子目錄
    - -v: 顯示訊息

  - 命令，建立/etc/pacman.d/hooks目錄，`$ [[ -d "/etc/pacman.d/hooks" ]] || mkdir -p "/etc/pacman.d/hooks"`

  - 檔案，配置 grub 啟動器，`/boot/loader/loader.conf`
    ```shell
    #　指定預設的選項，使用 arch.conf 進行啟動
    default  arch.conf
    timeout  0
    ```

    或使用單行命令
    ```shell
    echo "
    default  arch.conf
    timeout  0
    " > /boot/loader/loader.conf
    ```

  - 檔案，grub 的配置文件，`/boot/loader/entries/arch.conf`
    ```shell
    # 開機選項的標題
    title   Arch Linux
    # 內核檔案的位置
    linux   /vmlinuz-linux
    # initramfs 檔案的位置
    initrd  /initramfs-linux.img
    # 內核啟動選項
    #   root=LABEL=ROOT 指定根分區
    #   resume=LABEL=SWAP 指定交換分區
    #   rootflags=rw,relatime 設定根分區為讀寫並設定訪問時間
    #   add_efi_memmap 啓用 EFI 記憶體映射
    #   random.trust_cpu=on 使內核信任 CPU 內置的隨機數生成器
    #   rng_core.default_quality=1000 設定隨機數生成器的預設質量
    #   nomodeset 禁用內核模式設定，禁用KMS
    #   nowatchdog 禁用看門狗定時器
    #   mitigations=off 禁用安全增強措施
    #   quiet 抑製大多數日誌消息
    #   loglevel=3 設定日誌級別為3
    #   rd.systemd.show_status=auto 在啓動過程中顯示systemd狀態
    #   rd.udev.log_priority=3 設定 udev 的日誌優先級為3
    options root=LABEL=ROOT resume=LABEL=SWAP rootflags=rw,relatime add_efi_memmap random.trust_cpu=on rng_core.default_quality=1000 nomodeset nowatchdog mitigations=off quiet loglevel=3 rd.systemd.show_status=auto rd.udev.log_priority=3
    ```

    或使用單行命令
    ```shell
    echo "
    title   Arch Linux
    linux   /vmlinuz-linux
    initrd  /initramfs-linux.img
    options root=LABEL=ROOT resume=LABEL=SWAP rootflags=rw,relatime add_efi_memmap random.trust_cpu=on rng_core.default_quality=1000 nomodeset nowatchdog mitigations=off quiet loglevel=3 rd.systemd.show_status=auto rd.udev.log_priority=3
    " > /boot/loader/entries/arch.conf
    ```

  - 檔案，建立自動更新的服務，`/etc/pacman.d/hooks/100-systemd-boot.hook`
    ```shell
    # 當你安裝或升級內核時，100-systemd-boot.hook 會自動運行，
    # 用於更新 systemd-boot 配置文件中的內核信息
    [Trigger]
    Type = Package
    Operation = Upgrade
    Target = systemd

    [Action]
    Description = Upgrading systemd-boot ...
    When = PostTransaction
    Exec = /usr/bin/systemctl restart systemd-boot-update.service
    ```

    或使用單行命令
    ```shell
    echo "
    [Trigger]
    Type = Package
    Operation = Upgrade
    Target = systemd

    [Action]
    Description = Upgrading systemd-boot ...
    When = PostTransaction
    Exec = /usr/bin/systemctl restart systemd-boot-update.service
    " > /etc/pacman.d/hooks/100-systemd-boot.hook
    ```
  
- 範例3，透過 grub 安裝 MBR
  - 安裝 grub，`$ pacman --noconfirm -S grub`
  
  - 安裝 bootloader，`$ grub-install --target=i386-pc --recheck /dev/sda`

    或 `$ grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB`
  
  - 修改預設配置
    ```shell
    # "console=ttyS0" 指定使用 ttyS0 作為預設控制台
    # "earlyprintk=ttyS0" 允許在核心啟動前輸出訊息到 ttyS0
    # "rootdelay=300" 指定在嘗試掛載根文件系統前等待 300 秒
    sed -i 's|GRUB_CMDLINE_LINUX_DEFAULT="quiet"|GRUB_CMDLINE_LINUX_DEFAULT="console=ttyS0 earlyprintk=ttyS0 rootdelay=300"|' /etc/default/grub
    ```
  
  - 重新產生配置檔，`$ grub-mkconfig -o /boot/grub/grub.cfg`
  
  - 查看結果，`$ cat /etc/default/grub`

- 範例4，透過 grub 安裝 UEFI
  - 注意，需要建立EFI分區，並掛載到 /boot 的路徑上
  - --target=x86_64-efi 指定機器類型
  - --efi-directory 指定EFI分區的掛載點或EFI檔案的路徑
  ```shell
  grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub
  grub-mkconfig -o /boot/grub/grub.cfg
  ```

- 範例5，將 uefi 安裝在 efi 分區，不需要安裝 bootloader

  參考，[產生 uefi-kernel-image 範例](#chroot-產生開機用的虛擬檔案系統initramfs)

## [chroot] 設置用戶

- 新增用戶，`$ useradd -m -s /bin/bash -G wheel vagrant`
  
- 設置root和用戶密碼，`$ echo "用戶名:密碼" | chpasswd`

- 添加到 sudo 用戶，
  - 需要安裝 sudo，`$ pacman -S sudo --noconfirm`
  - `$ echo "vagrant ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/vagrant`
  - `$ chmod 440 /etc/sudoers`

## [chroot] 設置 ssh，選用

```shell
# 安裝 openssh
pacman -S openssh --noconfirm

# 建立配置目錄
mkdir -p /home/vagrant/.ssh
chmod 700 /home/vagrant/.ssh

# 寫入公鑰
curl https://raw.githubusercontent.com/hashicorp/vagrant/master/keys/vagrant.pub > /home/vagrant/.ssh/authorized_keys
chown vagrant:vagrant /home/vagrant/.ssh/authorized_keys
chmod 600 /home/vagrant/.ssh/authorized_keys

# 必須放在最後
chown -R vagrant:vagrant /home/vagrant/.ssh

# 變更配置

# 不允許 root 登入，只允許一般用戶登入
sed -i "s/PermitRootLogin yes/PermitRootLogin no/" /etc/ssh/sshd_config

# 允許密碼登入
sed -i "s/#PasswordAuthentication.*/PasswordAuthentication yes/" /etc/ssh/sshd_config

# 變更 port
sed -i "s/#Port.*/Port 22/" /etc/ssh/sshd_config

# 啟動 ssh 服務
systemctl enable sshd.service
```

## [chroot] 設置字形，選用

  - `方法1`，安裝系統字體(WM/DE/app用)，
    - 有要執行GUI才需要，安裝系統字體可以避免執行GUI時出現方塊
    - 字體存放在系統預設的字型目錄，`/usr/share/fonts/TTF/*.tty`
    - 以 ttf-mononoki-nerd 為例，`$ pacman -S ttf-mononoki-nerd`
  
  - `方法2`，安裝 console 字體，
    - console 字體的預設目錄，`/usr/share/kbd/consolefonts/*.gz`
    - step1，查看該目錄下有哪些字體可用，例如，`/usr/share/kbd/consolefonts/sample.psfu.gz`
    - step2，透過 setfont 命令更換，`$ setfont sample`

## [chroot] 設置網路(網路連接管理器)，選用

- 設置 localhost 的DNS解析
  - 設置 IPv4，`$ echo '127.0.0.1 localhost' >> /etc/hosts`
  - 設置 IPv6，`$ echo '::1 localhost' >> /etc/hosts`

- 範例1，使用 systemd-networkd 設置有線網路
  - 檔案，systemd-networkd.service 配置檔，`/etc/systemd/network/20-default-wired.network`
    ```shell
    # 配置預設的有線網路連接，包含了有關網路連接的諸如IP位址、子網掩碼、網關等信息
    [Match]
    Name=en*

    [Network]
    DHCP=ipv4
    LinkLocalAddressing=ipv4
    IPv6AcceptRA=no

    [DHCPv4]
    UseDomains=yes
    ```

    或使用單行命令，
    ```shell
    echo "
    [Match]
    Name=en*

    [Network]
    DHCP=ipv4
    LinkLocalAddressing=ipv4
    IPv6AcceptRA=no

    [DHCPv4]
    UseDomains=yes
    " > /etc/systemd/network/20-default-wired.network
    ```

  - 命令，啟用服務 `$ systemctl enable systemd-networkd.service`
  - 命令，[啟用 dns 解析](#chroot-設置-dns-解析選用)

- 範例2，使用 dhcpcd
  - 命令，安裝，`$ /usr/bin/arch-chroot /mnt pacman -S --noconfirm dhcpcd`
  - 命令，啟用 dhcpcd 服務，`$ /usr/bin/systemctl enable dhcpcd@eth0.service`
    - dhcp-client 用於獲取 IP 位址、子網路遮罩、網關和 DNS 伺服器等網路設定，建議啟用
    - archlinux 預設不會啟用 dhcp-service，需要手動啟動
  - 命令，[啟用 dns 解析](#chroot-設置-dns-解析選用)

## [chroot] 設置 DNS 解析，選用

- 注意，通常 網路連接管理器就`有內建DNS解析`，部分簡易的網路連接管理器才需要手動安裝DNS解析，
  - 例如，dhcpcd 才需要搭配 systemd-resolvconf 使用
  - 例如，只啟用有線網路的配置，未使用網路連接管理器，就需要搭配 systemd-resolvconf 使用
  
- 方法1，透過 systemd-resolved 進行DNS解析
  - 命令，安裝 systemd-resolvconf
  - 命令，啟用服務，`$ systemctl enable systemd-resolved.service`
  - 命令，`$ ln -sf "/run/systemd/resolve/stub-resolv.conf" /mnt/etc/resolv.conf`
    - 將 /mnt/etc/resolv.conf 指向 stub-resolv.conf
    - 使用 systemd-resolved 服務來管理DNS解析，提高DNS解析的效率

## [chroot] 禁用功能，選用

- 禁用KMS
  - KMS，內核設置模式，允許在內核空間而非用戶空間中設置顯示分辨率和深度的方法
  - 在內核啟動參數中設置`nomodeset`以禁用KMS
  - 參考，[內核模式設置](https://wiki.archlinux.org/title/Kernel_mode_setting)

- 禁用 DRM，`$ systemctl mask modprobe@drm`

- 禁用 systemd-remount-fs.service
  ```shell
  # 此命令會關閉 /boot 的自動掛載，必須在 fstab 中設置 /boot 由 systemd 啟動

  # 例如，sed -i -E '/\/boot/ s/(rw,\S*)/\1,noauto,x-systemd.automount/' /mnt/etc/fstab

  # 必須有設置 noauto 和 改由 systemd 掛載，才能關閉  systemd-remount-fs.service
  systemctl mask systemd-remount-fs.service
  ```

## [chroot] 功能優化，選用

- 關閉 makepkg 的多重壓縮

  ```shell
  # disable pkg compression
  # https://github.com/Jguer/yay/issues/765
  sed -i -e "s/PKGEXT='.pkg.tar.xz'/PKGEXT='.pkg.tar'/g" /etc/makepkg.conf
  ```

- 優化 pacman
  ```shell
  # 啟用 CheckSpace，檢查空間是否足夠
  sed -i '/^#.*CheckSpace/s/^#//' /etc/pacman.conf

  # 啟用 Color，控制输出的颜色
  sed -i '/^#.*Color/s/^#//' /etc/pacman.conf

  # 啟用 VerbosePkgLists，用來控製輸出的詳細信息
  sed -i '/^#.*VerbosePkgLists/s/^#//' /etc/pacman.conf

  # 啟用 NoExtract 欄位
  # NoExtract 欄位用於指定套件，該套件不會被下載到系統並解壓縮，也不會在系統中佔用空間。
  sed -i '/^#NoExtract/s/^#//' /etc/pacman.conf

  # 將 pacman-mirrorlist 添加到 NoExtract 欄位中時
  # pacman-mirrorlist 不會保留在系統中，使用預設的軟體源 (/etc/pacman.d/mirrorlist)
  sed -i '/^NoExtract /s/$/ pacman-mirrorlist/' /etc/pacman.conf
  ```

- 使用 reflector 管理 pacman 的鏡像源，自動選擇最快的鏡像站
  - 檔案，reflector配置檔，`/etc/xdg/reflector/reflector.conf`
      ```shell
      --save /etc/pacman.d/mirrorlist
      --protocol https
      --latest 10
      --sort rate
      ```

  - 檔案，建立 reflector服務，`/usr/lib/systemd/system/reflector-firstboot.service`
    ```shell
    [Unit]
    Description=Run Reflector on first boot
    Wants=network-online.target
    After=network-online.target nss-lookup.target
    ConditionPathIsReadWrite=/etc
    ConditionFirstBoot=yes

    [Service]
    Type=oneshot
    RemainAfterExit=yes
    ExecStart=/usr/bin/sh -c 'systemctl start reflector.service'

    [Install]
    WantedBy=multi-user.target
    ```
  
  - 命令，啟動 reflector服務，`$ systemctl enable reflector.timer reflector-firstboot.service`

## [chroot] 安裝 virtualbox-guest-additional，選用

- step1，安裝必要套件
  - VMSVGA 支持 3D 繪圖和高分辨率，並且還可以提供更好的性能
  - VBoxVGA 不支持 3D 繪圖，但是它可以支持較舊的操作系統，並且比 VMSVGA 更省資源
  - 若使用到x桌面系統，安裝 `virtualbox-guest-utils`
  - 若不使用到x桌面系統，安裝 `virtualbox-guest-utils-nox`

- step2，添加到 service 中，`$ sudo systemctl enable vboxservice.service`

- step3，重新啟動後，所有 guest-additional 的功能會自動生效

## [chroot] 安裝 AUR-helper，選用

- 注意，執行以下命令前，需要先建立 vagrant 用戶和安裝 git 套件

```shell
su vagrant
git clone https://aur.archlinux.org/yay.git $HOME/yay
cd $HOME/yay
makepkg -fsri --noconfirm

cd $HOME
rm -Rf $HOME/yay

yay -S --noconfirm pikaur pacaur yay
yes | yay -Scc
```

## [initial] 清理和安裝後處理
- 利用 dd 增加vagrant 壓縮率
  - 概念，利用將未使用的硬碟空間填滿0，打包成 box 時，vagrant會將連續的0進行壓縮，連續的0越多，壓縮率越高，

  - `$ dd if=/dev/zero of=/EMPTY bs=1M`
  - `$ rm -rf /EMPTY`
  - 注意，若不指定 count 參數，有可能會遇到 dd 拋出空間不足的錯誤，
    改用以下方式動態偵測剩餘空間，在執行 dd 可避免
    ```shell
    # 取得 /dev/sda3 剩餘的 1M-block 的數量
    COUNT=$(df -m | grep /dev/sda3 | awk '{printf $4}')
    
    dd if=/dev/zero of=/EMPTY bs=1M count=$COUNT
    rm -f /EMPTY
    ```
  
- 清理未使用的軟體，`$ pacman -Rsn $(pacman -Qdtq)`
- 清除 pacman 快取，`$ pacman -Scc --noconfirm` 和 `$ pacman -Sc --noconfirm`
- 清除 machine-id，`$ rm -f /mnt/etc/machine-id`
- 清除 bash_history 紀錄，`$ rm -f ~/.bash_history`
- 清除 packman 紀錄，`$ rm -f /var/log/pacman.log`

- 卸載掛載的硬碟，`$ umount -R /mnt`
- 卸載 swap 分區，`$ swapoff /dev/sda3`

## [post] 安裝DNS快取

- 方法1，透過 DNSmasq 建立 DNS 快取
  - 安裝 DNSmasq，`$ sudo pacman -S dnsmasq`

  - 配置 DNSmasq，`$ sudo nano /etc/dnsmasq.conf`
    ```shell
    [main]
    plugins=keyfile
    dns=dnsmasq
    ```
  - 關閉 dnsmasq 自啟動的服務，改用 Networkmanager 調用 dnsmasq
    - 命令，關閉 dnsmasq 服務，`$ sudo systemctl disable dnsmasq`
    - 命令，啟用 Networkmanager 服務，`$ sudo systemctl restart NetworkManager`

## [post] 安裝桌面環境

- [桌面系統概述](../../usage/desktop/desktop.md)

- 範例，以 ssh + x11 為例

  - `step1`，啟用 ssh 的 x11-forward-x11
    ```shell
    sed -i 's/^#X11Forwarding no/X11Forwarding yes/1' /etc/ssh/sshd_config
    touch /home/vagrant/.Xauthority
    chown vagrant:vagrant /home/vagrant/.Xauthority
    ```

  - `step2`，啟動 window-host 上的 xserver，例如，vcxsrv

  - `step3`，在 archlinux 上安裝 awesome-vm 和其他依賴，`$ paru -S ttf-mononoki-nerd xorg-xauth --noconfirm`

  - `step4`，透過 ssh -X 或 ssh -Y 進行連線
  
  - `step5`，在 archlinux 上執行 gui 應用
  
- 範例，以 ssh + awesome-vm 為例

  - 注意
    - 不需要安裝 xorg，只需要 xorg-xauth，因為是透過 ssh-x11-portforwarding
    - 若要透過本機顯示器啟動 awesome，安裝 xorg-init
    - 若要透過ssh 啟動 awesome，只需要安裝 xorg-xauth

  - `step1`，啟用 ssh 的 x11-forward-x11
    ```shell
    sed -i 's/^#X11Forwarding no/X11Forwarding yes/1' /etc/ssh/sshd_config
    touch /home/vagrant/.Xauthority
    chown vagrant:vagrant /home/vagrant/.Xauthority
    ```

  - `step2`，啟動 window-host 上的 xserver，例如，vcxsrv
    
    注意，windows 上的 vcxsrv `不要以 multiple-windows 開啟`，背景會破碎化，
    且只有作用的視窗有控制權，因此改用 one-large-window 開啟

  - `step3`，在 archlinux 上安裝 awesome-vm 和其他依賴，
    `$ paru -S ttf-mononoki-nerd awesome xterm xorg-xauth --noconfirm`

    - ttf-mononoki-nerd: 用於GUI應用中正確顯示字型
    - xterm: awesome-vm 預設配置的 terminal
    - awesome: windows manager
    - xorg-xauth: x11 remote forwarding

  - `step4`，透過 ssh -X 或 ssh -Y 進行連線

  - `step5`，在 archlinux 上啟動 awesome-wm，`$ awesome`
  
  - `step6`，修改 awesome-wm 的配置，改為其他 GUI-terminal
    - `/etc/xdg/awesome/rc.lua` 是預設配置
    -  `~/.config/awesome/rc.lua` 是使用者指定配置，會覆蓋預設配置的結果
    - 將 xterm 改為 konsole
      ```shell
      mkdir -p ~/.config/awesome
      cp /etc/xdg/awesome/rc.lua ~/.config/awesome/rc.lua

      # 在 ~/.config/awesome/rc.lua 檔案中
      terminal = konsole
      ```

## [post] 安裝輸入法

參考，[在bspwm中安裝輸入法](../../usage/desktop-wm-sw.md#輸入法-fcitx5)

## 安裝範例集

- [install-archlinux-on-vmware](install-arch-on-vmware.md)
  
- [packer安裝archlinux範例集](https://gitlab.com/anthony076/docker-projects/-/tree/main/packer/example)

- [Arch Linux Full install on UEFI with Cinnamon and LightDM](https://www.youtube.com/watch?v=UHcd7aiXJ5Q)

- [Archlinux安裝到使用筆記](https://jun-iwu.gitbooks.io/archlinux/content/jian-li-zhong-wen-huan-jing.html)

- [Cinnamon Desktop Environment on Arch Linux](https://www.youtube.com/watch?v=EJQZKqgajSc)

- [azure-archlinux-packer](https://github.com/colemickens/azure-archlinux-packer)

- [packer-archlinux-minimal](https://github.com/ChrisOrlando/packer-archlinux-minimal/blob/master/provision.sh)

- [arch-minimal-install](https://github.com/rtxx/arch-minimal-install)

## Ref

- [官方推薦套件](https://wiki.archlinux.org/title/Category:Lists_of_software)
- [官方推薦套件分類](https://wiki.archlinux.org/title/Special:Categories?from=Network)

- [mkinitcpio的使用](https://wiki.archlinux.org/title/Mkinitcpio)

- bootloader
  - [GRUB安装及命令行排错](https://xstarcd.github.io/wiki/Linux/grub_install.html)
  - [透過 grub2 安裝 uefi](https://www.twblogs.net/a/5cbe9df6bd9eee3971137d64)

- 最小化+加速啟動
  - [使用 mkinitcpio 優化啟動](https://blog.falconindy.com/articles/optmizing-bootup-with-mkinitcpio.html)
  - [最小化 initramfs](https://wiki.archlinux.org/title/Mkinitcpio/Minimal_initramfs)
  - [改善啟動流程](https://wiki.archlinux.org/title/Improving_performance/Boot_process)
  - [Improving performance](https://wiki.archlinux.org/title/improving_performance)

- UEFI
  - [How to install Arch linux on UEFI/GPT](https://www.youtube.com/watch?v=xMjI8pypeJE)

  - [archlinux 安裝 uefi](https://lovelinyj.pixnet.net/blog/post/47238984-%E3%80%90linux%E3%80%91arch-linux-uefi%E5%AE%89%E8%A3%9D)

  - [以 systemd-boot 取代 grub on UEFI](https://medium.com/@tsunderechen/%E7%94%A8-systemd-boot-%E5%9C%A8-uefi-%E5%B9%B3%E8%87%BA%E4%B8%8A%E5%AE%89%E8%A3%9D-arch-linux-6cf327a9724d)

  - [use systemd-boot instead of grab on a UEFI system](https://www.youtube.com/watch?v=DfC5hgdtbWY)

  - [設置 uefi 的安全啟動](https://wiki.archlinuxcn.org/wiki/UEFI/%E5%AE%89%E5%85%A8%E5%90%AF%E5%8A%A8)

- Desktop
  - [Install xorg + dwm on Arch Linux](https://www.youtube.com/watch?v=pouX5VvX0_Q)
