## 環境
- Local System: Windows 11
- VMWare WorkStation 16 pro (16.2.2 build-19200509)
- ArchLinux 2022.02.01-x86_64 build

## Arch ISO 下載
- https://archlinux.org/download/

- Taiwan mirrow
    - ftp，http://mirror.archlinux.tw/ArchLinux/iso/2022.02.01/
    - direct-link，http://mirror.archlinux.tw/ArchLinux/iso/2022.02.01/archlinux-2022.02.01-x86_64.iso

## 安裝系統前的硬體配置(VMWare)
- 選擇 `other linux 5x x64`

    ![image](doc/install-archlinux/vm-1.png)

- 選擇 `NAT`

    ![image](doc/install-archlinux/vm-2.png)

- 設置磁碟大小: 8G

    ![image](doc/install-archlinux/vm-3.png)


## 手動安裝
- 選擇第一個安裝選項，`Arch Linux install medium(x86_64, BIOS)`

    ![image](doc/install-archlinux/vm-4.png)

- 設置 root 帳號的密碼

    ![image](doc/install-archlinux/setup-1-change-password.png)

- 測試網路

    ![image](doc/install-archlinux/setup-2-test-network.png)

- 安裝套件
  
  `$ sudo pacman -Sy net-tools` ， for `ifconfig` command

- 設置遠端連線

    ![image](doc/install-archlinux/setup-3-setup-ssh.png)

- 配置磁碟
  - 參考
    - [手動設置swap範例](https://gitlab.com/anthony076/spinalhdl_study/-/blob/main/doc/tool-04-archlinux-on-qemu.md)
    
  - 流程，
    `建立分區` -> `格式化分區` -> `掛載分區`

  - 建立分區

    ![image](doc/install-archlinux/setup-4-create-partition.png)

  - 格式化分區，將 /dev/sda1 格式化為 exe4 格式
    
    命令，`mkfs.ext4 /dev/sda1`

    ![image](doc/install-archlinux/setup-5-format-partition.png)

  - 掛載分區，將分區掛載到 /dev/sda1
    
    命令，`mount /dev/sda1 /mnt` 

    ![image](doc/install-archlinux/setup-5-format-partition.png)

- 配置軟體安裝的鏡像源
  - 更新 pacman 的鏡像源和套件，`$ pacman -Sy`
  
  - 查詢最新的鏡像源    
    https://archlinux.org/mirrorlist/

  - 添加台灣鏡像，`$ nano /etc/pacman.d/mirrorlist`

    ![image](doc/install-archlinux/setup-7-update-mirror.png)
    
- 安裝系統
  
  `$ pacstrap -i /mnt base base-devel linux linux-firmware`

- 產生文件系統表
  
  `$ genfstab -U -p /mnt > /mnt/etc/fstab`

## 進入 chroot 環境並配置系統 
- 基本概念

  linux 限制使用者存取的手段有以下幾種
  - sudo，控制使用者可以執行的`命令`
  - chmod，控制使用者可存取的`檔案`
  - chroot，控制使用者可存取的`目錄`
  - cgroups，控制使用者可存取的`硬體資源`

  <font color=blue> 安裝系統為什麼需要 chroot </font>

  [archlinux 的 chroot 就是將虛擬檔案系統切換到實際的檔案系統](../../linux-core/00_bootup-flow.md)

  chroot 用於設置根目錄

  安裝系統時，是透過光碟或USB啟動安裝程式，並將系統檔案複製到指定的硬碟中，

  此時是`在光碟或USB的目錄中`執行安裝程式，若不透過 chroot 變更根目錄，
  在光碟或USB的目錄中下是`無法執行其他命令`或`修改配置的`

  <font color=blue> arch-chroot 是 chroot 的加強版 </font>

  arch-chroot，是 chroot 的重新包裝，
  - 用於切換目錄、
  - 建構 root 環境需的目錄和檔案
  - 進行 root 目錄的掛載

  arch-chroot 實際上是一個 shell script，可在 [man-page](https://manpages.debian.org/testing/arch-install-scripts/arch-chroot.8.en.html) 上查看用法，或查看 [arch-chroot shell-scipt](https://github.com/archlinux/arch-install-scripts/blob/master/arch-chroot.in) 的內容

- 進入 chroot 環境，注意，以下配置的指令都會在真實系統中生效
  
  `$ arch-chroot /mnt`

- 配置文字編碼
  - 修改系統文字編碼的配置

    `$ nano /etc/locale.gen`

    將中文和 EN 的編碼開放

    ![image](doc/install-archlinux/setup-7-add-encoding.png)

  - 根據 `/etc/locale.gen` 的配置，產生新的文字編碼
  
      `$ locale-gen`

  - 設定預設的文字編碼
  
      `$ echo LANG=en_US.UTF-8 > /etc/locale.conf`

- 配置時區
  - 摻除舊有時區

    `$ rm -f /etc/localtime`
  
  - 將系統時區設置為 Taipei

    `$ ln -s /usr/share/zoneinfo/Asia/Taipei /etc/localtime`
  
  - 設置硬件時間為本地時間

    `$ hwclock --systohc --localtime`

- 設置主機名稱

  `$ echo ching-arch > /etc/hostname`

- 設置 root 的密碼
  
  `$ passwd root`

- 安裝 Chroot 環境的套件 
  - `注意`，以下套件只能在 Chroot 環境中使用
  - ntfs 文件系統，以便訪問 Windows 磁碟
  
    `$ sudo pacman -S ntfs-3g`

  - 安裝網路工具

    `$ sudo pacman -S iw wpa_supplicant wireless_tools net-tools`

  - 安裝 console
  
    `$ sudo pacman -S dialog`

  - 安裝 網路管理工具，networkmanager，會自動配置網路，不需要手動配置

    `$ sudo pacman -S networkmanager`

    設置為 networkmanager 開機自動啟動，`$ systemctl enable NetworkManager`

  - 安裝 ssh

    `$ sudo pacman -S openssh`

    設置為 openssh 開機自動啟動，`$ systemctl enable sshd`

- 新增用戶和修改新用戶密碼
  - 建立新用戶
  
    `$ useradd -G root -m ching`

    > -G，指定用戶的 group

    > -m，為用戶建立 home directory

  - 修改新用戶密碼

    `$ passwd ching`

- 配置 sudo
  - 取消 sudo 文件只讀的功能(添加寫入的權限)
  
    `$ chmod +w /etc/sudoers`

  - 修改 sudo 文件，將新用戶 ching 添加到 sudo文件中

    `$ nano /etc/sudoers`
    
    將新用戶 ching 添加到 sudo 文件中

    ![image](doc/install-archlinux/setup-8-modify-sudo.png)

- 配置引導
  - 安裝引導套件
    
    `$ sudo pacman -S grub`
  
  - 安裝 bios 引導
  
    `$ grub-install --target=i386-pc /dev/sda`
    
    `注意`，需要安裝到主磁碟區，可以透過 lsblk 查看

  - 生成引導套件的配置文件

    `$ grub-mkconfig -o /boot/grub/grub.cfg`
    
- 重新啟動系統
  - `exit`，跳出 chroot 環境

  - `reboot`，重新啟動系統，重啟後，就可以以新的 hostname 和 user 進入系統
  
    ![image](doc/install-archlinux/setup-9-reboot.png)

- 修正 ssh 連線的錯誤
  - 未修正前，在 windows 上使用 `ssh ching@192.168.65.129` 遠端登入，會出現 `identification has changed` 的錯誤
  
    ![image](doc/install-archlinux/setup-10-ssh-error.png)

    因為先前的SSH認證是針對 `root 帳號`，現在改用 `ching 帳號`登入，因此需要修改

  - 修正方式，依照提示，打開  C:\\Users\\ching/.ssh/known_hosts 的檔案，刪除指定 ip 的行，修正後重新登入

    ![image](doc/install-archlinux/setup-11-fix-ssh-error.png)

- 安裝 Gnome 圖形介面
  - 安裝 archlinux-keyring
    - 部分套件的簽名是由維護者簽名的，必須導入密鑰才能驗證簽名，
      透過 archlinux-keyring 自動處理簽名的問題
  
    - `$ pacman -S archlinux-keyring`
  
  - 安裝 gnome
    
    `$ pacman -S gnome gnome-extra` (安裝時使用預設設定即可)

  - 安裝驅動
    
    `$ pacman -S xorg xorg-xinit`

  - 將建立 gnome 連線 的命令，添加到 xinitrc 配置檔中
    在 root 帳號中執行
    `$ [root@ching-arch ching]# echo "exec gnome-session" > ~/.xinitrc`

    輸入 exit 退出 root 帳號後 (進入ching帳號)，再輸入一遍
    `$ [ching@ching-arch ~]$ echo "exec gnome-session" > ~/.xinitrc`

  - 在 VMware 的虛擬機中啟動 Gnome 圖形介面，
    
    `$ startx`，
    
    注意１，SSH 的遠端連線不支援圖形介面，不要在 SSH 中執行 starx

    注意2，若啟動失敗，關閉 archlinux 的虛擬機，增加內存的容量後，重新啟動

  - 開機自動啟動 Gnome 

    `$ systemctl enable gdm`

- 設置直接進入系統，不進入 grub 的啟動畫面，直接顯示 login 畫面

  - 修改 /etc/default/grub

    `$ nano /etc/default/grub`

  - 將 GRUB_TIMEOUT 修改為 0

    ![image](doc/install-archlinux/setup-12-modify-grub.png)

  - 生成新的配置檔
  
    `$ grub-mkconfig -o /boot/grub/grub.cfg`

  - 關閉虛擬機，並重新啟動系統
    
## VMware 自動調整 Archlinux 的分辨率
- 確保 VMWare > Home Tab > View > Autosize > Autofit Guest 開啟
  
  ![image](doc/install-archlinux/resolution-1.png)

- 在虛擬機中安裝以下套件

  `$ sudo pacman -S gtkmm gtkmm3 gtk2 open-vm-tools xf86-input-vmmouse xf86-video-vmware`

- 設定開機自動啟動 vmtoolsd

  `$ systemctl enable vmtoolsd`

- 修改 mkinitcpio.conf，
  
  `$ nano /etc/mkinitcpio.conf`

  將 MODULES=() 改成 MODULES=(vsock vmw_vsock_vmci_transport vmw_balloon vmw_vmci vmwgfx)

  ![image](doc/install-archlinux/resolution-2.png)
  
- 生成初始化環境

  `$ mkinitcpio -p linux`
  
- 重新啟動系統
  `$ sudo reboot`

- 重新進入系統後，變更任意解析度後，再重新啟動系統一次後生效

## VSCode 遠程開發
- 在 archlinux 中安裝 gdb

  `$ sudo pacman -S gdb`

- 在 vscode 中安裝 Remote-SSH 插件，
  [Remote-SSH](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh)

  安裝後，建立連線

  ![image](doc/install-archlinux/remote-dev-1.png)

  連線到遠端資料夾

  ![image](doc/install-archlinux/remote-dev-2.png)

- 在 vscode 中安裝`可用於遠端開發`的 C++ 插件，

  ![image](doc/install-archlinux/remote-dev-3.png)

- 簡易代碼進行測試
  
  ![image](doc/install-archlinux/remote-dev-4.png)

## Reference
- [官方安裝指引](https://wiki.archlinux.org/title/Installation_guide_(%E6%AD%A3%E9%AB%94%E4%B8%AD%E6%96%87))

- [ArchLinux 安裝](https://gitlab.com/anthony076/x86asm/-/blob/main/doc/00_install-arch-on-vmware.md)

- [archlinux 的安裝與配置](https://huangno1.github.io/archlinux_install_part1_basic/)

- [Arch Linux 完全手冊（上）](https://www.readfog.com/a/1661580634707038208)

- [Arch Linux 手册](https://www.freecodecamp.org/chinese/news/how-to-install-arch-linux/)

- [在vmware上安裝arch-linux](https://www.bilibili.com/video/BV1b44y1k7mT)

- [安裝 Archlinux (必要安裝)](http://blog.ccyg.studio/article/4f6cfa0a-ad98-4adb-af08-79a8a5b1d674/)

- [安裝Arch Linux(LXDE)](https://steward-fu.github.io/website/handheld/gpdwin-al/archlinux.htm)

- [安装gnome图形界面](http://blog.ccyg.studio/article/255f0ebd-a9af-40a2-ba94-d45e10baeff1/)

- [Archlinux 可选配置](http://blog.ccyg.studio/article/5c3a4435-dec5-4a15-b30c-3dea4ae35e40/)

- [VMware 自動調整 Archlinux 的分辨率](https://www.bilibili.com/video/BV1tZ4y1w7fR)
