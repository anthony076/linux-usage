## install-boostrap 最小化系統

- 安裝 archlinux 的兩種方式: archlinux-iso 和 archlinux-bootstrap
  - `方法1，archlinux-iso`
    
    提供archlinux完整的安装程序，包括 Arch Linux Live環境、圖形化安裝程式和一系列預裝的軟體包

  - `方法2，archlinux-bootstrap (或稱rootfs)`

    用於安裝Arch Linux最小化版本的壓縮文件，它提供了一個`最小化`的Linux系統的基本文件系統(rootfs)，
    只包含了操作系統最基本的組件和庫，不包括完整的操作系統安裝程式。
    
    使用Arch Linux Bootstrap，需要手動安裝系統所需的軟體包和配置文件，並自行配置系統。

    archlinux 官方有提供的 archlinux-bootstrap，命名為 `archlinux-bootstrap-XXX.tar.gz`，
  [以台灣鏡像站為例](https://mirror.archlinux.tw/ArchLinux/iso/2023.02.01)

- archlinux-bootstrap (或稱rootfs) 也包含了基本的檔案系統、基礎工具，
  <font color=red>但沒有內核和驅動</font>

  rootfs通常是一個記憶體文件系統（ramfs）或可寫的閃存文件系統（如ext4、btrfs等）中的一個，rootfs 它被掛載到Linux系統啓動時的根目錄下。

  當系統啓動時，首先會加載rootfs，再加載其他文件系統，如/proc、/sys和/dev等。
  因此在加載這些文件系統之前，系統必須先有一個根文件系統

  rootfs是Linux系統中的基礎，包含linux 需要的文件結構、檔案系統、基礎工具，
  和所有其他文件系統和文件的掛載點。

- 比較，archlinux-bootstrap `啟動的最小系統` 和 archlinux-iso `安裝的系統`有什麼區別

  `archlinux-bootstrap > archlinux-iso > archlinux`

  |                    | archlinux-bootstrap                  | archlinux-iso                 | archlinux              |
  | ------------------ | ------------------------------------ | ----------------------------- | ---------------------- |
  | 啟動需要的系統資源 | 少，不需要內核、開機引導             | 中，需要光碟的相關資源        | 多，需要最完整的資源   |
  | 內建工具           | 少，以啟動為目的，後續可根據需求添加 | 中，需要啟動 + 安裝環境的工具 | 多，具有完整工具和配置 |
  | 配置               | 少，最少配置                         | 中，使進行安裝系統的相關配置  | 多，執行系統的完整配置 |

  - archlinux-bootstrap 可以用來製作 `live-cd、live-usb` 或`直接在虛擬環境中執行`

  - archlinux-bootstrap` 不包含內核`也可以在虛擬器中啟動，
    > 虛擬器`使用主機系統的內核`啟動容器，即使沒有內核也可以啟動
    > 安裝系統啟動時，透過 主機板的ROM開始，經過開機自檢(POST)等一系列過程，所以需要開機引導和內核

  - archlinux-bootstrap 建立系統的過程中`不需要建立磁碟分區`

  - archlinux-bootstrap 建立系統的過程中`不需要建立開機引導`、不需要重新編譯內核

- archlinux-bootstrap 可以製作 live-cd 或 live-usb
- 在 termux 中利用 proot + bootstrap(rootfs) 安裝特定的 linux

- 安裝基礎系統的 pacstrap 命令參數
  - 若不指定要安裝的套件，預設會安裝 base 套件包，
    > 在 packages 會自動安裝依賴的狀態下，安裝 base 幾乎就等於安裝所有內建套件了
  - 若指定要安裝的套件，就只會安裝指定套件或其依賴

  - -c 表示使用主機上的軟體包緩存，
  - -D 表示跳過pacman對安裝套件的依賴檢查，不使用 -D 時，預設會檢查並安裝套件的依賴
  - -G 表示避免將主機的pacman keyring複製到目標中，
  - -K 表示在目標中初始化一個空的 keyring (可以用 -K 取代 -G 就不需要手動建立 keyring)
  - -M 表示避免將主機的鏡像列錶複製到目標中，
  - -U 表示使用`pacman -U`命令安裝軟體包，使用`本地的套件包路徑`或從`指定網址`安裝套件，
    - 需要手動處理依賴
    - 不會更新 mirrorlist

- 處理套件依賴
  - 方法1，參考該套件[在官方的說明頁](https://archlinux.org/packages/)，檢查該命令是否缺少依賴
  - 方法2，利用 `$ pacstrap -c pacstrap/root.x86_64 套件名`，單獨安裝指定套件
    - 在不使用 -D 的狀況下，pacstrap 會檢查指定套件需要的依賴，並在安裝時顯示依賴

- 推薦套件
  - bash: [必要] 當使用者進入系統後，需要啟動 shell 環境以執行命令
  - pacman：[必要] Arch Linux 的軟件包管理器
  - sed：[推薦] 是一個用於對文本進行替換和編輯的工具集
  - base: 內核、基本工具、驅動，若是透過容器啟動，不需要經過 bootloader
  - filesystem: 建立系統必須的文件系統結構
  - coreutils: 包含了Linux系統上的一些最基本的工具
  - grep：用於在文本文件中搜索指定字符串的工具
  - 其他套件，見 [base套件包有哪些套件](package-list.txt)

## 範例，手動建立 archlinux 的 bootstrap 壓縮檔

- `範例`，建立可啟動最小系統的 bootstrap 壓縮包
  - 無法作為救援光碟或安裝光碟，
    沒有安裝 arch-install-scripts、mkinitcpio，沒有 chroot 環境，無法用於建立新的 linux
  
  - 缺少常用套件

  - 代碼
    ```shell
    # ---- 建立 bootstrap 壓縮檔 ----
    mkdir -p pacstrap/root.x86_64

    # +使用當前linux環境的軟件包快取，[推薦使用]，避免重複下載
    # +自動檢查並下載依賴，[無法客製化] chroot 環境，會自動安裝約113個套件
    # +複製當前linux環境的keyring，[不安全]，應該讓用戶自行建立
    # +複製當前linux環境的pacman mirrorlist，[適用性差]，不一定適用每一個人
    pacstrap -c pacstrap/root.x86_64

    # 建立壓縮檔
    tar -zcf archlinux-bootstrap-x86_64-myarch.tar.gz -C pacstrap .

    # ---- 測試 bootstrap 壓縮檔 ----
    mkdir -p tmp
    tar xzf archlinux-bootstrap-x86_64-myarch.tar.gz -C tmp

    # 進入容器進行測試
    systemd-nspawn -D ./tmp/root.x86_64
    ```

- `範例`，用於建立 chroot 環境的 bootstrap 壓縮包
  - 具有 chroot 環境的bootstrap壓縮包，代表提供安裝 archlinux 系統的相關工具，
    例如，arch-install-scripts、mkinitcpio

  - 代碼
    ```shell
    # ---- 建立 bootstrap 壓縮檔 ----
    mkdir -p pacstrap/root.x86_64

    # 安裝 chroot 環境需要的套件
    #   內核和驅動: base
    #   建立系統用: arch-install-scripts、mkinitcpio
    #   設置環境用: systemd
    pacstrap -c pacstrap/root.x86_64 base arch-install-scripts mkinitcpio systemd

    # (選用) 進入 chroot 環境進行設置，只能用於設置，無法用於安裝軟體
    # arch-chroot ./pacstrap/root.x86_64 /bin/bash -c "echo 123"
    arch-chroot ./pacstrap/root.x86_64

    # 建立壓縮檔
    tar -zcf archlinux-bootstrap-x86_64-myarch.tar.gz -C pacstrap .

    # ---- 測試 bootstrap 壓縮檔 ----
    mkdir -p tmp
    tar xzf archlinux-bootstrap-x86_64-myarch.tar.gz -C tmp

    # 進入容器進行測試
    systemd-nspawn -D ./tmp/root.x86_64
    ```

- `範例`，完全手動，自定義 bootstrap 壓縮包
  - 可在容器中啟動
  - 自定義 bootstrap 壓縮包，需要手動處理下列問題
    - 要完全手動安裝軟體，pacstrap 需要添加 -M、-D、-G 的參數
    - bash: 若使用 `$ pacstrap -D ...`，表示略過套件依賴的檢查，因此需要確保安裝 bash 的依賴，不推薦使用
    - pacman: 若使用 `$ pacstrap -M ...`，表示略過 mirrorlist 的設置，因此需要手動設置 mirrorlist
    - keyring: 若使用 `$ pacstrap -G ...`，表示略過 keyring 的設置，因此需要手動配置 keyring

  - 代碼
    ```shell
    # ---- 建立 bootstrap 壓縮檔 ----
    mkdir -p pacstrap/root.x86_64

    PKG=(bash pacman archlinux-keyring sed)
    
    # 將套件安裝到指定目錄
    #   -c 使用當前系統的軟件包快取進行安裝
    # 
    # 
    pacstrap -c -G -M pacstrap/root.x86_64 ${PKG[@]}
    
    # 啟用 mirrorlist
    arch-chroot ./pacstrap/root.x86_64 /bin/bash -c "echo '
    Server = https://mirror.archlinux.tw/ArchLinux/\$repo/os/\$arch
    Server = https://archlinux.cs.nycu.edu.tw/\$repo/os/\$arch
    Server = https://free.nchc.org.tw/arch/\$repo/os/\$arch
    ' > /etc/pacman.d/mirrorlist
    "

    # 配置 pacman
    arch-chroot ./pacstrap/root.x86_64 /bin/bash -c "sed -i 's/CheckSpace/#CheckSpace/g' /etc/pacman.conf"  # 取消 CheckSpace
    arch-chroot ./pacstrap/root.x86_64 /bin/bash -c "echo 'nameserver 8.8.8.8' > /etc/resolv.conf"  # 加入 dns 解析的server

    # 配置 keyring
    arch-chroot ./pacstrap/root.x86_64 /bin/bash -c "pacman-key --init" # 建立本地的 keyring
    arch-chroot ./pacstrap/root.x86_64 /bin/bash -c "pacman-key --populate archlinux" # 下載 archlinux 官方的 keyring

    arch-chroot ./pacstrap/root.x86_64 /bin/bash -c "pacman -Q > pkglist.x86_64.txt"

    # 測試
    # arch-chroot ./pacstrap/root.x86_64        # 方法1，透過 arch-chroot
    # systemd-nspawn -D ./pacstrap/root.x86_64  # 方法2，透過 systemd-nspawn

    # 建立壓縮文件
    tar -zcf archlinux-bootstrap-x86_64-myarch.tar.gz -C pacstrap .

    # ---- 測試 bootstrap 壓縮檔 ----
    
    mkdir -p tmp
    tar xzf archlinux-bootstrap-x86_64-myarch.tar.gz -C tmp
    
    # 進入容器進行測試
    systemd-nspawn -D ./tmp/root.x86_64
    ```

## 範例，利用 arch-bootstrap 壓縮檔，安裝最小化的 archlinux

- step1，取得 arch-bootstrap 的壓縮檔，
  - 方法1，從台灣鏡像站[下載](https://mirror.archlinux.tw/ArchLinux/iso/2023.02.01)，
    選擇`archlinux-bootstrap-XXX.tar.gz`
  
  - 方法2，透過 pacstrap 建立 bootstrap 的壓縮檔，
    - 若不是在 archlinux 的環境下，需要安裝 `arch-install-scripts 套件`
    - 參考，[手動建立 archlinux 的 bootstrap 壓縮檔](#範例手動建立-archlinux-的-bootstrap-壓縮檔)

- step2，在 linux 環境下解壓縮，
  - 注意，壓縮檔內包含軟連結，winodws 不支援，在windows下解壓縮會出現錯誤
  - `$ mkdir ./tmp`
  - `$ tar -xpf archlinux-bootstrap-x86_64.tar.gz -C ./tmp`

- step3，(選用) 依需求修改以下配置
  - 若 bootstrap 未配置才需要修改，若從官方下載的 bootstrap 通常不需要修改
  - 透過 `$ arch-chroot ./tmp/root.x86_64` 進入 chroot 環境
  - 依需求配置網路，例如，`$ echo 'nameserver 8.8.8.8' > /etc/resolv.conf`
  - 依需求配置 pacman，例如，`$ sed -i 's/CheckSpace/#CheckSpace/g' /etc/pacman.conf`
  - 依需求配置 mirrorlist

- step4，透過 `systemd-nspawn -D ./tmp/root.x86_64`，在容器中啟動

- step5，(選用) 進入後，手動掛載 /proc、/sys、/dev 文件系統
  ```shell
  mount -t proc /proc ./proc
  mount -t sysfs /sys ./sys
  mount -o bind /dev ./dev
  ```

## 範例，利用 archiso 製作 archlinux 的 live-iso 

- live-iso 和 live-usb 的區別: 先製作出 iso，再將 iso 寫入 usb 中

- 此範例不需要事先準備好最小系統(archlinux-boostrap)，archiso 會自動在 airootfs 建立 rootfs
  - 執行 mkarchiso 命令後，
  - step1，讀取 `profiledef.sh` 和 `packages.x86_64` 的配置
  - step2，在 work 目錄建立和 alpinelinux-boostrap 一樣 rootfs
  - step3，將 wokr 目錄的內容打包成 iso

- 若要訂製自己的 iso 檔，
  - mkarchiso 命令是一個 shell-script，依照需求修改 mkarchiso
    參考，[archiso官網](https://gitlab.archlinux.org/archlinux/archiso/)
  
  - 將自定義的檔案放置到 airootfs 的目錄下(任意目錄)，
    執行 mkarchiso 命令後，會將 airootfs 的內容複製到 out 目錄中，
    建立的 iso 的也會包含該檔案

  - 範例，[第三方簡化的archiso](https://github.com/midfingr/arch_livecd/blob/master/build.sh)，
    或參考[源碼](https://t.me/c/1636650313/563)

- 範例
  - step1，安裝 archiso，`$ pacman -S archiso`

  - step2，`$ mkdir archlive && cd archlive`

  - step3，複製安裝模板
    - 安裝 archiso 後，archiso 內建的安裝模板分為兩種
    - `/usr/share/archiso/configs/releng`，用於創建自定義ISO檔
    - `/usr/share/archiso/configs/baseline`，用於創建最低限度的ISO檔
    - 命令，`$ cp -r /usr/share/archiso/configs/releng/ ./myarch`

  - step4，依需求修改開機引導配置，`$ nano myarch/profiledef.sh`

    以下是 profiledef.sh 的預設內容
    ```shell
    #!/usr/bin/env bash
    # shellcheck disable=SC2034
    iso_name="archlinux"
    iso_label="ARCH_$(date +%Y%m)"
    iso_publisher="Arch Linux <https://archlinux.org>"
    iso_application="Arch Linux Live/Rescue CD"
    iso_version="$(date +%Y.%m.%d)"
    install_dir="arch"
    buildmodes=('iso')
    bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito'
              'uefi-ia32.grub.esp' 'uefi-x64.grub.esp'
              'uefi-ia32.grub.eltorito' 'uefi-x64.grub.eltorito')
    arch="x86_64"
    pacman_conf="pacman.conf"
    airootfs_image_type="squashfs"
    airootfs_image_tool_options=('-comp' 'xz' '-Xbcj' 'x86' '-b' '1M' '-Xdict-size' '1M')
    file_permissions=(
      ["/etc/shadow"]="0:0:400"
      ["/root"]="0:0:750"
      ["/root/.automated_script.sh"]="0:0:755"
      ["/usr/local/bin/choose-mirror"]="0:0:755"
      ["/usr/local/bin/Installation_guide"]="0:0:755"
      ["/usr/local/bin/livecd-sound"]="0:0:755"
    )
    ```
  
  - step5，依需求修改要安裝的配置，`$ nano myarch/packages.x86_64`

  - step6，建立 iso，`$ mkarchiso -v -w work -o out myarch`
    - 安裝模板在 myarch 目錄
    - 將建立過程產生的中間檔案放在 work 目錄
    - 最後的結果輸出到 out 目錄

## Ref

- [install distro by proot](https://github.com/dm9pZCAq/TermuxAlpineVNC/blob/master/Alpine.sh)
  
- [alpine-make-rootfs.sh](https://github.com/alpinelinux/alpine-make-rootfs/blob/master/alpine-make-rootfs)

- [不透過套件，手動建立 alpine 的 rootfs](https://github.com/alpinelinux/alpine-make-rootfs/blob/master/alpine-make-rootfs)

- [termux + proot + boostrap 啟動 linux 系統，以alpine為例](https://github.com/dm9pZCAq/TermuxAlpineVNC/blob/master/Alpine.sh)

- 自製 archlinux bootstrap 壓縮包
  - [Archiso](https://wiki.archlinux.org/title/Archiso)
  - [archstrap](https://github.com/palasso/archstrap)
  - [Arch-bootstrap](https://github.com/tokland/arch-bootstrap)
  - [建立 docker 用的 bootstrap](https://github.com/moby/moby/blob/master/contrib/mkimage-arch.sh)
  - [在docker上利用bootstrap安裝archlinux](https://github.com/sebastianlach/archstrap/blob/master/Dockerfile)

  - [archbashstrap](https://github.com/BiteDasher/archbashstrap)
  - [docker-archlinux](https://github.com/terencewestphal/docker-archlinux)
  - [arch-bootstrap](https://github.com/tokland/arch-bootstrap)
  - [利用bootstrap建立docker的baseimage](https://hoverbear.org/blog/arch-docker-baseimage/)

- arch-iso
  - [archiso on github](https://gitlab.archlinux.org/archlinux/archiso)
  - [archiso 的使用](https://wiki.archlinux.org/title/Archiso)
  - [Arch Linux: Create Your Own Installer ISO](https://www.youtube.com/watch?v=-yPhW5o1hNM)
  - [如何製作 Arch ISO](https://samwhelp.github.io/note-about-archlinux/read/build-iso/start-build-arch-iso.html)