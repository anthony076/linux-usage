# clean pacman cache
#pacman -Rn linux
pacman -Rsn $(pacman -Qdtq)
pacman -Scc --noconfirm

# remove records
rm -f /mnt/etc/machine-id
rm -f ~/.bash_history
rm -f /var/log/pacman.log

# unmount
umount -R /mnt
swapoff /dev/sda3

reboot