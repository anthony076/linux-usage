
set -e 

# make partition
sgdisk -o -g /dev/sda
sgdisk -n 1:0:+512M -c 1:"efi" -t 1:ef00 /dev/sda
sgdisk -n 2:0:+8G -c 2:"root" -t 2:8304 /dev/sda
sgdisk -n 3:0:0 -c 3:"swap" -t 3:8200 /dev/sda
sync

# format
mkfs.fat -F32 -n "EFI" /dev/sda1
mkfs.ext4 -L "ROOT" /dev/sda2
mkswap -L "SWAP" /dev/sda3
swapon /dev/sda3

# mount
mount /dev/sda2 /mnt
mkdir -p /mnt/boot
mount /dev/sda1 /mnt/boot

# generate fstab
mkdir -p /mnt/etc
genfstab -U /mnt > /mnt/etc/fstab
sed -i -E '/\/boot/ s/(rw,\S*)/\1,noauto,x-systemd.automount/' /mnt/etc/fstab

# add taiwan source
echo "
Server = https://mirror.archlinux.tw/ArchLinux/\$repo/os/\$arch
Server = https://archlinux.cs.nycu.edu.tw/\$repo/os/\$arch
Server = https://free.nchc.org.tw/arch/\$repo/os/\$arch
" > /etc/pacman.d/mirrorlist

# add verify-tool and axel
pacman -Sy archlinux-keyring axel --noconfirm

# use axel for multi-thread download
sed -i 's/#XferCommand = \/usr\/bin\/curl -L -C - -f -o %o %u/XferCommand = \/usr\/bin\/axel -n 5 -a -o %o %u/g' /etc/pacman.conf

# install system tool and kernel
pacstrap /mnt base linux
