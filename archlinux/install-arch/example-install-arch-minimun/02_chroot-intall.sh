# 以下內容需要再 chroot-environment 中執行
# arch-chroot /mnt bash chroot.sh

set -e 

# set host-name
echo 'myarch-pc' > /etc/hostname

# sync system-clock
hwclock --systohc

# set time-zone
ln -sf /usr/share/zoneinfo/Asia/Taipei /etc/localtime

# set locale
echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen
locale-gen
echo 'LANG=en_US.UTF-8' > /etc/locale.conf

# set hosts
echo '127.0.0.1 localhost' >> /etc/hosts
echo '::1 localhost' >> /etc/hosts

# change root password
echo "root:root" | chpasswd

# disable functions
systemctl mask modprobe@drm
systemctl mask systemd-remount-fs.service

# generate initramfs

echo "
MODULES=(ext4 ahci)
BINARIES=(fsck fsck.ext4 e2fsck)
FILES=()
HOOKS=(base systemd autodetect modconf)
COMPRESSION='zstd'
" > /etc/mkinitcpio.conf

echo "
ALL_config='/etc/mkinitcpio.conf'
ALL_kver='/boot/vmlinuz-linux'
PRESETS=('default')
default_image='/boot/initramfs-linux.img'
" > /etc/mkinitcpio.d/linux.preset

mkinitcpio -P linux

# install and config efi-bootloader
bootctl install

rm -fRv /boot/loader/entries/*

mkdir -p "/etc/pacman.d/hooks"

echo "
default  arch.conf
timeout  0
" > /boot/loader/loader.conf

echo "
title   Arch Linux
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
options root=LABEL=ROOT resume=LABEL=SWAP rootflags=rw,relatime add_efi_memmap random.trust_cpu=on rng_core.default_quality=1000 nomodeset nowatchdog mitigations=off quiet loglevel=3 rd.systemd.show_status=auto rd.udev.log_priority=3
" > /boot/loader/entries/arch.conf

echo "
[Trigger]
Type = Package
Operation = Upgrade
Target = systemd

[Action]
Description = Upgrading systemd-boot ...
When = PostTransaction
Exec = /usr/bin/systemctl restart systemd-boot-update.service
" > /etc/pacman.d/hooks/100-systemd-boot.hook