
## install-minimum-archlinux

- 測試環境
  - virtualbox 
  - archlinux-2022.12.01-x86_64.iso

## 使用方式

- 更簡單的執行方法，參考，[在 initial.sh 中執行 chroot.sh 的方法](../example-install-arch-myarch/readme.md)

- step1，virtualbox 的設置
  - 在 virtualbox 中建立 vm，
  - 添加 port-22 的 port-forwarding
  - 設置 enable efi
  
- step2，進入安裝介面後，passwd root，變更密碼後，改在 windows 上透過 ssh 登入
- step3，將 01_initial-install.sh 內容複製到 ~/initial.sh
- step4，執行 bash initial.sh

- 注意，mount 會清除 /mnt 的資料，必須先執行 initial.sh 後，
  再建立並執行 /mnt/chroot.sh

- step5，將 02_chroot-install.sh 內容複製到 /mnt/chroot.sh
- step6，執行 arch-chroot /mnt bash chroot.sh

- step7，關機後，移除 archlinux-iso > 啟用 efi > 重新啟動