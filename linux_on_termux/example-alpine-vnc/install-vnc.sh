apk add xvfb xorg-server x11vnc xterm ttf-liberation awesome supervisor

# x11vnc -storepasswd

echo "
[supervisord]
nodaemon=true

[program:xvfb]
command=/usr/bin/Xvfb :1 -screen 0 720x1024x24
autorestart=true
priority=100
user=root

[program:x11vnc]
command=/usr/bin/x11vnc -permitfiletransfer -tightfilexfer -display :1 -noxrecord -noxdamage -noxfixes -shared -wait 5 -noshm -nopw -xkb -ncache 10
autorestart=true
priority=200
user=root
[program:GraphicalEnvironment]
environment=DISPLAY=:1
autorestart=true
command=/usr/bin/awesome
priority=300
user=root
EOF
" > /etc/supervisord.conf