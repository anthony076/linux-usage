## linux on termux

- 注意，bluestack 無法安裝 proot-distro，可用 nox-hyper 版進行測試

- 在 termux 中安裝 linux-distro 的幾種方式
  - 方法1，透過 proot-distro 安裝 linux 發行版
    - 安裝 proot-distro，`$ pkg install proot-distro`
    - 透過 proot-distro安裝linux，`$ proot-distro install alpine`
    - 進入 linux，`$ proot-distro login alpine`，進入 alpine 
    - 備份環境，`$ proot-distro backup alpine --output 路徑`
    - 還原環境，`$ proot-distro restore alpine --output 路徑`
    - 變更存儲權限，`$ termux-setup-storage`

  - 方法2，透過 bootstrap，參考，[customize-termux-distro](customize-termux-distro.md)
    
## 範例，TermuxAlpineVNC (boostrap安裝+vnc設置) 使用範例

- [from](https://github.com/dm9pZCAq/TermuxAlpineVNC)
  - Alpine.sh: 在 termux 中手動安裝 alpine
  - SetupVNC.sh: 安裝和設置 VNC 相關套件

- 安裝 alpine，`$ curl -O https://raw.githubusercontent.com/dm9pZCAq/TermuxAlpineVNC/master/Alpine.sh`
- 安裝 VNC，`$ curl -O https://raw.githubusercontent.com/dm9pZCAq/TermuxAlpineVNC/master/SetupVNC.sh`
- 進入 alpine，`$ alpine`

- 設置VNC，
  - 查看可用選項，`$ SetupVNC --help`
    - 選項1，`$ SetupVNC openbox`
    - 選項2，`$ SetupVNC xfce`
    - 選項3，`$ SetupVNC i3`

- 啟動和管理 VNC
  - 啟動VNC相關應用，`$ startvnc`
  - 啟動VNC + 背景執行，`$ startvnc bg`
  - 停止VNC相關應用，`$ startvnc kill`

## 範例，proot-distro 安裝 alpine + VNC + awesome

- 測試環境
  - nox hyperV v7.0.2.5 64bit
  - [下載地址](https://support.bignox.com/ja/else/hypervonbeta)
  
- step1，在 termux 預設的 shell 環境中安裝 alpine-linux
  - 1-1，`$ pkg install proot proot-distro`
  - 1-2，`$ proot-distro install alpine`
  - 1-3，`$ proot-distro login alpine`，進入 alpine 

- step2，在 alpine 的環境中
  - 2-1，`$ apk add xvfb xorg-server x11vnc xterm ttf-liberation awesome`
  - 2-2，設置x11vnc密碼，`$ x11vnc -storepasswd`
  - 2-3，啟動 xvfb，`$ /usr/bin/Xvfb :1 -screen 0 720x1024x24 &`
  - 2-4，啟動 vnc，`$ nohup /usr/bin/x11vnc -permitfiletransfer -tightfilexfer -display :1 -noxrecord -noxdamage -noxfixes -shared -wait 5 -noshm -nopw -xkb -ncache 10 -forever &`

  - 2-5，設置環境變數，`$ export DISPLAY=:1 `
  
  - 2-6，啟動 xcef4，`$ nohup /usr/bin/awesome & `

  - 2-7，(選用) 將啟動命令添加到 supervisor 中，由 supervisor 管理
    - 2-7-1，安裝依賴，`$ apk add supervisor`，
      注意，supervisor 類似 systemd 的功能，可以自動重啟服務，但不具有開機自動啟動的功能

    - 2-7-2，配置 supervisor 要管理的套件
      - supervisor 預設會背景執行，因此不需要加上 nohup 或 & 
      - supervisor 會自動重啟，因此 vnc 不需要 -forever 參數
      - 註冊應用
        ```shell
        echo "
        [supervisord]
        nodaemon=true
        
        [program:xvfb]
        command=/usr/bin/Xvfb :1 -screen 0 720x1024x24
        autorestart=true
        priority=100
        user=root

        [program:x11vnc]
        command=/usr/bin/x11vnc -permitfiletransfer -tightfilexfer -display :1 -noxrecord -noxdamage -noxfixes -shared -wait 5 -noshm -nopw -xkb -ncache 10
        autorestart=true
        priority=200
        user=root
        [program:GraphicalEnvironment]
        environment=DISPLAY=:1
        autorestart=true
        command=/usr/bin/awesome
        priority=300
        user=root
        EOF
        " > /etc/supervisord.conf
        ```
      - 測試
        - 透過 supervisor 啟動所有註冊的應用，`$ /usr/bin/supervisord -c /etc/supervisord.conf >/dev/null 2>/dev/null &`
        - 透過 supervisor 關閉已經啟動的應用，`$ killall supervisord 2>/dev/null`
  
  - 在手機的 vncviewer 中，透過 `localhost:0` 進行連接
  
  - 在電腦中，nox 和 PC 使用同一個網路，可以直接連線，
    - 在手機透過 ifconfig 查詢IP，例如，192.168.43.26
    - 在PC的 vncviewer 直接透過 192.168.43.26 進行連線 (不需要寫成 192.168.43.26:0)

## Ref

- [termux 下載](https://github.com/termux/termux-app/releases/tag/v0.118.0)

- distro
  - [termux-alpine官網](https://github.com/Hax4us/TermuxAlpine)
  - [HOW TO INSTALL LINUX WINDOW MANAGER IN TERMUX ANDROID WITHOUT ROOT](https://www.youtube.com/watch?v=RlLIrXFSrFY)

- desktop
  - [Termux X11：手機上的XWayland伺服器使用教學](https://ivonblog.com/posts/termux-x11/)
  - [Termux GUI - dwm Running On Termux Using XServer XSDL (Android)](https://www.youtube.com/watch?v=4ygode8mF20)
  - [termux-desktop](https://github.com/adi1090x/termux-desktop)
  - 
