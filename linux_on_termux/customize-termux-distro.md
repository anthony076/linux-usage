## customize-termux-distro

- 基礎
  - termux 上的系統，是利用 distro 的 Bootstrap 壓縮檔解壓縮獲得 roots 的環境後，
    透過 proot 進行啟動的

  - [boostrap基礎](../archlinux/install-arch/install-bootstrap.md)
  
  - [利用boostrap啟動系統](../archlinux/install-arch/install-bootstrap.md#範例利用-arch-bootstrap-安裝最小化的-archlinux)

- 範例
  - 基本概念，
    - 利用 proot 建立 distro 的 rootfs，再利用 proot 啟動
    - [proot的使用方法](https://wiki.termux.com/wiki/PRoot)

  - [不透過 termux-distro，手動建立 alpine 的 rootfs @ alpinelinux](https://github.com/alpinelinux/alpine-make-rootfs/blob/master/alpine-make-rootfs)

  - [不透過 termux-distro，手動建立 alpine 的 rootfs @ dm9pZCAq](https://github.com/dm9pZCAq/TermuxAlpineVNC/blob/master/Alpine.sh)

  - [不透過 termux-distro，手動建立 kali 的 rootfs @ Hax4us](https://github.com/Hax4us/Nethunter-In-Termux/blob/master/kalinethunter)

  - [不透過 termux-distro，手動建立 ubuntu 的 rootfs @ Neo-Oli](https://github.com/Neo-Oli/termux-ubuntu)

  - [termux + proot + boostrap 啟動 linux 系統，以alpine為例](https://github.com/dm9pZCAq/TermuxAlpineVNC/blob/master/Alpine.sh)