## make distro by buildroot

- 測試環境 : WSL ubuntu 22.04
  - 流程
    - step，下載到當前目錄並解壓縮
      - `$ wget https://buildroot.org/downloads/buildroot-2024.02.1.tar.gz`
      - `$ tar xf buildroot-2024.02.1.tar.gz` 
      - `$ cd buildroot-2024.02.1`
    
    - step，安裝依賴
      - `$ sudo apt update`
      - `$ sudo apt install g++ make libncurses-dev unzip bc bzip2 libelf-dev libssl-dev extlinux`
    
    - step，建構 buildroot
      - `$ make menuconfig`，進入設置頁面
        - Target options > Target Architecture(i386)
          - 選擇 x86_64
          - `<ESC>` 回到 Target options 選單
          - `<TAB>` 選中 Exit，以跳出 Target options 選單
        
        - Kernel > Linux Kernel + `<SPACE>`，選中 Linux Kernel
          - 選擇 Kernel configuration (Using an in-tree deconfig file)
          - 選擇 Use the architecture default configuration
          - `<TAB>` 選中 Exit，以跳出當前選單
        
        - System configuration
          - Init system 選擇 BusyBox
          - `<TAB>` 選中 Exit，以跳出當前選單
        
        - `<TAB>` 選中 Exit，以跳出當前選單，並按下 Yes 保存設置
    
      - `$ make`，執行建構
        - 注意，在WSL下，環境變數PATH有可能會出現空白而造成錯誤，重新設置 PATH

          > export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/lib/wsl/lib

        - 注意，執行 make 後會執行檢查和下載作業，
          依電腦狀況會有不同的idle時間，只要沒有報錯就是正常現象

    - step，建構完成後，移動產出物
      - `$ cd output/images`
      - `$ cp bzImage rootfs.tar ~ && cd ~`
        - bzImage : linux kernel
        - rootfs.tar : root-filesystem
      - `$ mkdir distro`
      - `$ mv bzImage rootfs.tar distro/`
      - `$ cd distro`
            
    - step，建構 boot-image (*.img)
      - `$ tar xf rootfs.tar`
      - `$ rm -rf rootfs.tar`，
      - 處理到此，當前的distro目錄就是一個未封裝的linux-filesystem
      - `$ cd ..` ，回到上層目錄
      - `$ truncate -s 100MB boot.img`，建構內容為空的 img 檔案
      - `$ mkfs boot.img`，格式化 boot-image

    - step，透過 extlinux 建構 bootloader
      - `$ mkdir mounted`，建立用於掛載 boot-img 的目錄
      - `$ sudo mount boot.img mounted/`，將 boot-image 掛載到系統中
      - `$ sudo extlinux --install mounted/`，安裝 bootloader 到指定路徑
      - `$ sudo cp -r distro/* mounted/`，將建構好，已經位於distro目錄的linux-filesystem 複製到 mounted 目錄中，此時，mounted 目錄包含
        - bootloader
        - kernel (bzImage)
        - linux-filesystem

      - `$ sudo umount mounted`，將準備好的boot.img(mounted目錄)卸載
      - `$ sudo mv boot.img /mnt/c/Users/使用者/Desktop/`，將檔案移動到桌面
      
      - step，透過 qemu 檢查結果
        - 回到 windows os
        - `$ qemu-system-x86_64 -hda boot.img`
        - 輸入 `boot: /bzImage root=/dev/sda`，輸入kernel和根目錄的路徑
 
  - 結果 : 最終的 boot.img 僅占用 95.3M 的磁碟空間

- 測試環境 : docker ubuntu <font color=red>22.04</font>
  - 注意，在 docker 編譯前，進入 Settings > resource > momory，<font color=red>增加記憶體</font>，
    避免記憶體不足造成 swap 需求過高，<font color=blue>高 swap 容易導致 disk 使用率達到 100%，使得編譯卡住不動</font>

  - step，安裝依賴
    - `$ apt update`
    - `$ apt install g++ make libncurses-dev unzip bc bzip2 libelf-dev libssl-dev extlinux`
    - `$ apt install cpio rsync patch`
    - `$ apt install fish wget`
  
  - step，建立一般用戶
    - `$ useradd -ms /usr/bin/fish 用戶名`
    - `$ echo '用戶名:用戶密碼' | chpasswd`
    - `$ su 用戶名 && cd ~`

  - step，下載到當前目錄並解壓縮
    - `$ wget https://buildroot.org/downloads/buildroot-2024.02.1.tar.gz`
    - `$ tar xf buildroot-2024.02.1.tar.gz`
    - `$ rm -rf buildroot-2024.02.1.tar.gz`
    - `$ cd buildroot-2024.02.1`
    
  - step，建構 buildroot

    與`測試環境 : WSL ubuntu 22.04`同

## ref

- [make distro by buildroot](https://youtube.com/watch?v=ey3sKdOmPa8&si=V3BQGUSOJKR-JQer)

- [buildroote官網](https://buildroot.org/)