
## Example

- [推薦，簡易的 distro with buildroot](simple-distro/simple-distro.md) : 開機後進入 shell
- [利用buidroot快速建立root-filesystem](make-distro-by-buildroot/make-distro-by-buildroot.md)
- [具有圖形能力的distro](distro-with-gui/) : 可執行具有 GUI 的應用程式
- [建構最小的 linux-distro](make-smallest-linux-distro/)

## 推薦工具套件

- 產生編譯工具鏈 (產生 gcc、make、g++ ... 等編譯工具)
  - [crosstool-ng](https://crosstool-ng.github.io/)
    - 產生指定平台的編譯工具鏈，例如，產生 raspberry 可用的編譯器
    - 支援各種架構和平台，例如，riscv、arm、...等
  - ct-ng : Crosstool-NG 的一個分支，專注於改進和簡化交叉編譯工具鏈的構建過程
  - xPack GNU Arm Embedded GCC : 針對 ARM Cortex-M 和 Cortex-A 處理器的交叉編譯工具鏈生成器
  - ELLCC : 基於 LLVM 和 Clang 的交叉編譯工具鏈生成器

- 建構 initramfs 中系統工具的提供商
  - busybox: 
    - 體積小、資源佔用少、功能齊全、高度可配置，某些工具功能可能不如完整版本
    - 適合路由器、IoT設備等嵌入式系統、Live CD、容器基礎映像

  - toybox: 
    - 模塊化設計、比BusyBox更小巧輕量、遵循POSIX標準、兼容性好。
    - 適合Android系統、極度受限的嵌入式設備、需要BSD許可證的商業產品
  
  - sbase 和 ubase
    - [sbase](https://github.com/michaelforney/sbase) 提供了 POSIX 兼容的核心 Unix 工具
    - [ubase](https://core.suckless.org/ubase/) 提供了一些額外的 Linux 特定工具
    - 這兩個項目一起可以替代 BusyBox 的大部分功能
  
  - GNU Core Utilities (Coreutils) : 比 BusyBox 更完整，但也更大，或使用 GNU coreutils-tiny 的輕量版

- 建構整個embeded-linux : 建構後可直接燒錄映像到開發版，燒錄後可直接進入 linux
  - 注意，以下套件也可以獨立建立 initramfs 中的工具包，但是提供更多的功能
  - Buildroot : 
    - 簡單、易用、輕量的嵌入式 Linux 構建系統
    - 可以構建完整的根文件系統映像，直接燒錄到開發板上運行
    - 支持多種硬件平台,如 ARM、MIPS、PowerPC 等
    - 提供大量預配置的軟件包
    - 缺，相比 Yocto，定制化選項較少，不包含包管理器，不提供完整的開發環境工具鏈

  - OpenEmbedded/Poky : 是一個嵌入式 Linux 構建框架,Poky 是 OpenEmbedded 的發行版，是 Yocto 的前身

  - Yocto: 
    - 完整的嵌入式Linux開發框架、高度可訂製、跨平台支持好。學習曲線較陡峭
    - 不只是建立 initramfs中的系統套件，`可直接建構整個 embeded-linux`生成開發版的完整系統映像在燒錄後可直接使用 linux，
      範例見，[Flashing Yocto Built Custom Linux OS onto BeagleBone Black](https://www.youtube.com/watch?v=S3NG7ch3Xgw)
    - 適合訂製化嵌入式Linux發行版，用於工業控制、汽車電子等複雜系統
  
  - PTXdist : 
    - 高度模塊化、提供完整的開發環境和工具鏈、提供多層次的配置選項、良好的交叉編譯支持
    - 可以構建訂製化的根文件系統映像
    - 供了豐富的軟件包和配置選項，方便用戶進行訂製
    - 學習曲線可能較buildroot陡峭、社區規模較小、文檔不如build全面、預配置的軟件包可能沒有 Buildroot

## 建立 root-filesystem

- root-filesystem

  root 是預設的使用者，用有最高權限可以存取資源並安裝套件，
  在 init 的檔案中，需要建立 root 的帳號和維持基礎linux運行的基本掛載點

- 必要的掛載點
  - /proc：提供了系統和進程的信息，對於系統監控和管理至關重要。
  - /dev：包含了系統中的設備文件，許多程序和工具需要訪問這些設備文件才能正常運行。
  - /sys：提供了對內核和硬件設備的信息，用於訪問和調整硬件設備的屬性和狀態。
  - /run：用於存儲系統運行時的臨時文件和套接字，許多程序和服務需要在這裡創建臨時文件或套接字進行通信和協調。

- 可選掛載點
  - /tmp：用於存儲臨時文件。許多程序和服務需要在 /tmp 中創建臨時文件，如果沒有這個掛載點，這些程序可能無法正確運行。
  - /boot：包含了引導程序和引導配置文件，是系統啟動時必需的。如果系統使用引導加載程序（boot loader）來引導，那麼 /boot 就是必需的。
  - /etc：包含了系統的配置文件，如網絡配置、用戶配置、服務配置等。這些配置文件在系統運行時需要被訪問和使用，因此 /etc 是必需的。
  - /mnt 和 /media：用於臨時掛載其他文件系統，如移動硬盤、光盤等。如果系統需要臨時掛載其他文件系統來進行操作，則這些目錄也是必需的。

## 簡易流程

- step1，準備 bzImage (linux-kernel)
- step2，製作 init-program
- step3，準備安裝過程中使用的套件 (也可以使用是busybox)
- step4，製作 initrd.img (也稱為 ramfs)
  - 建立 rootfs 目錄作為 initrd.img 的掛載點 ( rootfs目錄 == initrd.img的內容 )
  - 往 rootfs 添加以下內容
    - step1 的 bzImage (linux-kernel)
    - step2 的 init-program
    - step3 的 安裝套件
    - 符合FHS規範的常規掛載點 (常規目錄)
- step5，製作 bootloader，例如，grub 的 boot.img
- step6，製作磁碟映像 (含kernel + bootloader + busybox)

## 建立包管理器

- 在建構 distro 時，通常會在宿主系統(host-linux)上建立相關的檔案，例如，
  - 在自定義alpine-distro時，使用 alpine 作為宿主系統
  - 在自定義nixos-distro時，使用 nixos 作為宿主系統
  - 使用與自定義distro相同的作業系統作為宿主系統通常會更方便，因為提供了建構系統的相關工具

- 在建構自定義distro時，無論是iso或ramfs，套件都是`透過宿主系統上的編譯環境或是相關套件`安裝到自定義distro

  尤其是包管理器，必須透過宿主系統安裝，否則沒有包管理器或是沒有編譯環境的狀況下，無法安裝任何的套件

## Ref

- [打造屬於自己的linux發行版](https://www.cntofu.com/book/46/qemu/da_zao_shu_yu_zi_ji_de_linux_fa_xing_ban.md)

- [建構distro完整範例](https://www.youtube.com/playlist?list=PLLfIBXQeu3aZuc_0xTE2dY3juntHF5xJY)
  - Building a Linux Distro From Scratch: Compiling Linux Kernel
  - Building a Linux Distro From Scratch: BusyBox & Root File System
  - Building a Linux Distro From Scratch: DHCP Networking
  - Building a Linux Distro From Scratch: Compiling & Installing Golang
  - Building a Linux Distro From Scratch: WiFi Drivers & Firmware
  - How to build linux distro from scratch | glibc, busybox & networking support
  - Simple BusyBox Linux Distro Anatomy
  - How to install linux on USB drive ( UEFI boot )
  - Writing distro independent linux package installer
  - Installing Linux Desktop Environment on the distro built from scratch
  - didactic busybox distro with graphical desktop built from scratch
  - Xorg setup pitfalls
  - setting up audio | ALSA | PULSEAUDIO | DBUS

- [Build your own Linux distribution](https://www.youtube.com/playlist?list=PLLfIBXQeu3aZAECUk1qaMCglG9VYD1nRm)
  - How to build Linux Kernel from sources
  - How to build bare minimal Linux system
  - Building Linux system via automated shell script | mkroot by Rob Landley
  - Booting linux kernel directly into interactive Python shell
  - git : https://github.com/maksimKorzh/cmk-linux
    
    - 注意，每一個目錄都是獨立的專案，且都可以在 qemu 上執行
    - [hello-kernel，建構最小的系統](https://github.com/maksimKorzh/cmk-linux/blob/417f4fd3fb9b3460a174bb909974f211d6dadf25/tutorials/hello-kernel.md)
    - [busybox，添加busybox](https://github.com/maksimKorzh/cmk-linux/blob/5bc3edfa22effec6fa39cebb46cbb167849a0b5c/tutorials/busybox/initrd.md)
    - [如何建立root-filesystem](https://github.com/maksimKorzh/cmk-linux/blob/13534d03dc21440055902b720eb8baf1d81c291e/tutorials/rootfs/initrd.md)
    - [最小系統 + busybox + root-filesystem + python](https://github.com/maksimKorzh/cmk-linux/tree/74826a7142cb13c28c92c7b36bfb83cac992bf16/tutorials/pylinux)

- [tiny core based distro with nothing but python and WiFi support](https://www.youtube.com/watch?v=goZ3doZ4kgs&list=PLLfIBXQeu3abFgynDiQHnHf7VrNg6Jy0C)

- crosstool-NG
  - [crosstool-NG的安裝和使用](https://ithelp.ithome.com.tw/articles/10329511)
  - [使用crosstool-ng製作交叉編譯工具鏈](https://blog.csdn.net/lisemi/article/details/91045791)

- Yocto
  - [系列，Yocto Project Tutorial Series (Basic to Advance)](https://www.youtube.com/playlist?list=PLwqS94HTEwpQmgL1UsSwNk_2tQdzq3eVJ)
