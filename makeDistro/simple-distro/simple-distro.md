## make simple distro

- 建立 linux-distro 需要的物件
  - 要件，`linuxk-kernel` : 保存為 bzImage

  - 要件，initramfs: 
    - 提供 fhs 架構的目錄結構
    - 提供 系統基礎的工具 
    - 可由 busybox 提供

  - 要件，bootloader : 由 bootloader 套件提供的命令進行安裝

  - 將上述三者打包成映像檔後，就可以由 qemu 透過指定映像檔的方式來啟動

- make-flow : `kernel` -> `BusyBox` -> `initramfs` -> `BootFile with Bootloader(Syslinux)`

- 開機流程

  - 參考，[完整開機流程](../../linux-core/01_bootup-flow.md)
  
  - boot-flow : `step1_(Boot -> BIOS/UEFI)` -> `step2_Bootloader` -> `step3_(Kernel -> initramfs -> init)` -> `step4_UserSpace`
  
  - step1，讀取EEPROM中的BIOS或UEFI，執行硬體初始化程式
    - 執行以下內容 : 1_初始化系統硬體、2_設定基本的系統參數，3_加載引導程式
    - 初始化程式會從預定義的啓動設備（如硬碟、固態硬碟、光盤、網路等）中加載引導程式，例如，從硬碟的MBR或UEFI分區

  - step2，運行引導程式(bootloader)
    - 執行以下內容 : 1_加載內核到內存中、2_設定內核參數、3_將控制權轉移到內核的入口點(執行內核)
    - bootloader會將必要的參數傳遞給kernel，例如initramfs的位置、kernel的啓動參數等

  - step3，啟動 kernel
    - kernel 將 initramfs 的內容加載到記憶體中，並作為根文件系統進行掛載
    - 使用 initramfs 中的工具和驅動程式執行引導過程中所需的操作，例如，加載必要的驅動程式、識別硬體、執行文件系統檢查和修複等
    - 將實際的根文件系統掛載到相應的位置，並將其作為根目錄
    - 執行 init 進程，init 進程會啓動系統的用戶空間進程
  
  - step4，啟動用戶空間進程，包括系统服务(例如initd)、用户界面等

## Steps

- step1，安裝依賴
  - step，`$ docker run --privileged -it ubuntu`
    > --privileged 提供內部掛載需要的權限

  - step，在 container 中，第一次進入執行系統更新，`$ apt update`

  - step，安裝需要的依賴，
    `$ apt install bzip2 git vim make gcc libncurses-dev flex bison bc cpio libelf-dev libssl-dev syslinux dosfstools`

    - for busybox: bzip2
    - for build process: make、gcc、、、
    - for inter-active menu : libncurses-dev (基於終端的圖形用戶介面應用程式的開發庫)
    - for kernel : flex、bison、bc、libelf-dev、libssl-dev
    - for initramfs : cpio (存檔工具，用於將文件打包成一個單獨的歸檔文件)
    - for bootloader : syslinux

- step2，建構 linux-kernel (bzImage)
  - step，下載 linux-kernel-repo，`$ git clone --depth 1 https://github.com/torvalds/linux.git`

  - step，進入 linux-kernel-repo，並透過 make 建立配置選單程式
    - `$ cd linux`
    - `$ make menuconfig`，建構完成後，會自動開啟 Linux/x86 6.8.0-rc6 Kernel Configuration 的介面，用於調整建構 linux-kernel 的相關參數
      - 不需要修改配置，直接保存
      - 保存成功後，配置檔會保存為 `linux/.config` 的路徑
      - (optional) 若要將 docker 中的 x86_64_defconfig 複製到本機，`$ docker cp <容器ID>:/linux/.config .`

  - step，開始建立 linux-kernel ，`$ meke -j 8` 或更通用的版本，`$ make -j $(nproc)`
    - `-j 4`，使用4個CPU進行建構，可加快建構速度
    - 使用 `$ nproc`，可查看當前電腦的核心數
    - 使用 `$ lscpu`，可查看當前cpu的相關資訊
    - 建構完成後，會得到 kernel-image 檔案，bzImage，位於 `linux/arch/x86/boot/bzImage`
    - bzImage 最初是使用 bzip2 壓縮得到的
    - (optional) 若要將 docker 中的 bzImage 複製到本機，`$ docker cp <容器ID>:/linux/arch/x86/boot/bzImage .`

  - step7，將 bzImage 複製到指定位置
    - `$ mkdir /boot-files`
    - `$ cp arch/x86/boot/bzImage /boot-files`
  
  - step8，回到根目錄，`$ cd /`

- step3，建構 BusyBox
  - 概念，
    
    busybox 是軟體工具箱，使用者進入系統後，可以提供基礎的應用程式合集，適用於嵌入式系統和資源受限的環境，
    例如，ls、cat、grep、sed、awk、tar、sh 等，busybox允許用戶根據需要選擇性地啓用或禁用特定的功能

  - step，下載 busybox-repo，`$ git clone https://git.busybox.net/busybox`

  - step，進入 busybox-repo，並透過 make 建立配置選單程式
    - `$ cd busybox`
    - `$ make menuconfig`
    - 進入配置畫面後， `Settings > Build Options > 勾選 Build Static binary (no shared libs)`，使建構後的busybox不會依賴任何的外部庫
    - 開始建構 busybox，`$ make -j 8` 
  
  - step，建立 initramfs 目錄，並將 busybox 安裝到 initramfs 的目錄中
  
    - `$ mkdir /boot-files/initramfs`

    - `$ make CONFIG_PREFIX=/boot-files/initramfs install`
      - CONFIG_PREFIX參數，用於指定busybox的安裝目錄
      - busybox安裝後，會在指定目錄中`建立bin目錄、sbin目錄、usr目錄、linuxrc檔案`
      - 透過 `$ ls /boot-files/initramfs/bin`，可檢查busybox是否被正確安裝

- step4，建立 initramfs (建立初始的根目錄)

  - [initramfs基礎](../../linux-core/04_initramfs.md)

  - step，進入 initramfs 目錄中，`$ cd /boot-files/initramfs`
  
  - step，將以下內容添加到 `/busybox/boot-files/initramfs/init` 的檔案中，`$ vi init`
    
    kernel 在結束後，會自動調用 initramfs/init，來執行用戶空間相關的程序
    ```shell
    #!/bin/sh
    /bin/sh
    ```
  
  - step，修改 init 的存取權限，`$ chmod +x init`，新增可執行的權限
  
  - step，移除 linuxrc ，`$ rm linuxrc`

  - <font color=red>注意，此範例中沒有在檔案系統中建立`etc、proc、sys`等常見的根目錄</font>
  
  - step，將當前 initramfs 打包為 init.cpio，`$ find . | cpio -o -H newc > ../init.cpio`
    - `-o` ，建立新壓縮檔
    - `-H newc`，壓縮為 newc 格式，此格式是 linux-kernel 支援的壓縮檔類型
  
  - step，會到上一層目錄，`$ cd ..`，執行後，回到 `/boot-files` 的目錄中

- step5，建立 bootfile (即硬碟映像檔)

  - step，安裝 syslinux，`$ apt install syslinux`

  - step，建立空的 bootfile，`$ dd if=/dev/zero of=boot bs=1M count=50`
    - `if=/dev/zero`，/dev/zero是特殊裝置，每次訪問/dev/zero後，/dev/zero都會返回0，常用於填充0
    - `of=boot`，輸出的bootfile，檔名為 boot
    - `bs=1M`，block-size，每次複製 1M bytes 的值
    - `count=50`，總共複製50次
    - 依命令會進行50次的複製，每次複製1M大小的0到 bootfile 中，最後，此命令會建立一個50M且填充0的檔案
    - bootfile 用於建立檔案系統(file-system)，並在該檔案系統中，1_安裝bootloader、2_複製kernel到檔案系統中、3_複製initrams到檔案系統中

  - step，將 bootfile 格式化為 fat 格式的檔案系統 (將 bootfile 變成可掛載的磁碟Image)
    - 參考，[檔案系統基礎](../../linux-core/filesystem.md)
  
    - `$ apt install dosfstools`，dosfstools 提供了 mkfs.fat 命令，用於建立新的 FAT 檔案系統
    - `$ mkfs -t fat boot`，將 bootfile 格式化為 fat 的檔案系統
    
  - 將 syslinux(bootloader) 安裝到 bootfile，
    - 參考，[bootloader基礎](../../linux-core/02_bootloader.md)
    - `$ syslinux boot`

  - 將 kernel(此處為bzImage檔案) 和 initramfs(此處為init.cpio檔案) 複製進bootfile(此處為boot檔案)中
    - 建立一個臨時目錄m，`$ mkdir m`
    - 將 bootfile 的內容掛載到m目錄中，`$ mount boot m`，此時可以透過 `ls ./m` 查看掛載的內容(bootfile的內容)
    - 將 bzImage 和 init.cpio 複製到 bootfile 中，`$ cp bzImage init.cpio m`，此時可以透過 `ls ./m` 查看掛載的內容(bootfile的內容)，具有以下四個檔案
      - bzImage，壓縮的kernel
      - init.cpio，壓縮的 initramfs
      - ldlinux.c32，Syslinux 引導載入程式的一部分，用於啟動 Linux 系統
      - ldlinux.sys，Syslinux 引導載入程式的一部分，但通常用於 FAT 檔案系統
    - 卸載 bootfile 的掛載，`$ umount m`

- step6，(optional) 在qemu中進行測試
  
  - 將 docker 中的 bootfile(此處為boot檔案) 複製出來，在本機中執行，`$ docker cp <容器ID>://boot-files/boot .`
  
  - [Qemu Installer 下載](https://qemu.weilnetz.de/w64/2023/)

  - 在本機中，透過執行 `qemu-system-x86_64.exe 開機映像檔` 載入系統
    - 輸入，`$ qemu-system-x86_64 boot`
    - 看到 boot: 提示後，輸入 `/bzImage -initrd=/init.cpio`
      - `/bzImage`，用於指定 kernel 在 boot-file 中的路徑
      - `-initrd=/init.cpio`，用於指定 initramfs 在 boot-file 中的路徑
    - 若正常開機，bootloader 會自動執行 /init 的程序，進到 shell 的提示命令中
  
  - 參考，[qemu 啟動最小系統的幾種方式](https://gitlab.com/anthony076/docker-projects/-/blob/main/%5B%20Virtual%20Machine%20%5D/qemu/qemu.md#範例)

- step6，在(optional) 在usb中進行測試

  詳細步驟參考，[Build a embedded linux using buildroot](https://youtu.be/ysoi0bn66oM?si=yywQ83nTyuEbcjCP&t=387)

  ```shell
  # 掛載 usb
  sudo mkdir -p /mnt/disk1
  sudo mkdir -p /mnt/disk2a
  sudo mount -t auto /dev/sd${USBDISK}1 /mnt/disk1
  sudo mount -t auto /dev/sd${USBDISK}2 /mnt/disk2a

  # 安裝 grub bootloader
  sudo grub-install --boot-directory=/mnt/disk1/boot /dev/sd${USBDISK}  
  # copy kernel and initrd
  sudo cp -v  ./output/images/bzImage  /mnt/disk1/boot/

  複製 root-filesystem
  # copy FS 
  sudo tar -xvf ./output/images/rootfs.tar -C /mnt/disk2a/
  sync

  # 複製 grub 的配置文件
  #sudo grub-mkconfig -o /mnt/disk1/boot/grub/grub.cfg 
  # ---------------------------------------------------------
  sudo cp   ./grub.cfg  /mnt/disk1/boot/grub/

  # 卸載 usb
  sudo umount -f /dev/sd${USBDISK}1
  sudo umount -f /dev/sd${USBDISK}2
  sudo rm -rf /mnt/disk1
  sudo rm -rf /mnt/disk2a
  ```

  grub.cfg
  ```
  menuentry 'MY-OS'  {
    insmod part_msdos
    insmod ext2
    set root=('hd0,msdos1')
    linux /boot/bzImage root="PARTUUID=0261d7c5-02"
  }
  ```

## ref

- [Making Simple Linux Distro from Scratch](https://www.youtube.com/watch?v=QlzoegSuIzg)
- [Building a Linux Distro From Scratch: BusyBox & Root File System](https://www.youtube.com/watch?v=eGHQU0EUQWQ)
- [Build a minimal Linux with only Busybox in 1 hour(含建構掛載點)](https://www.youtube.com/watch?v=asnXWOUKhTA)

- How to build bare minimal Linux system
  - [y2b @ Monkey see, monkey do](https://www.youtube.com/watch?v=c4j6z2huJxs)
  - [github](https://github.com/maksimKorzh/cmk-linux/blob/8068de3d7ec713c00c7df0442e8d2dad18ade6cc/tutorials/busybox/initrd.md)

- [Build a minimal Linux with only Busybox in 1 hour](https://www.youtube.com/watch?v=asnXWOUKhTA)
  