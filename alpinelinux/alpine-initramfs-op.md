## 在alpine上操作initramfs 

- 測試環境
  - windows 11 23H2 with hyperV enable
  - QEMU emulator version 8.2.0 (v8.2.0-12045-g3d58f9b5c5) 安裝版
  - Alpine Linux v3.20

- initramfs 預設路徑，
  - 方法，`/boot/initramfs-virt`

  - 方法，從 syslinux 配置檔獲取，`$ cat /boot/extlinux.conf`
    > syslinux 是 alpine 預設的 bootloader，一般 bootloader 會記錄 kernel 和 initramfs 的位置
  
- 修改 initramfs
  - step，修改前先切換至 root 帳號，`$ su - root`

  - step，備份原始initramfs檔案，`$ cp /boot/initramfs-virt /boot/initramfs-virt.bak`

  - step，解壓縮 initramfs 檔案，
    
    ```shell
    mkdir /tmp/initramfs
    cd /tmp/initramfs
    
    # 將/boot/initramfs-virt 解壓縮到 /tmp/initramfs
    gzip -dc /boot/initramfs-virt | cpio -idmv
    ```
  - step，修改 /tmp/initramfs 的內容，若要打印訊息，透過 `$ echo "123" > "$ROOT"/dev/kmsg`，打印到 kmsg 才能在`$ dmesg`中看到

  - step，重新打包 initramfs 檔案，`$ find . | cpio --create --format='newc' | gzip > /boot/initramfs-virt`

  - step，(option) 若使用 grub 作為 bootloader，透過`$ update-grub` 更新配置
  
  - step，重新啟動電腦後，透過`$ dmesg`查看 init 檔案打印出來的日志

## ref

- [alpine init檔案解析](https://ithelp.ithome.com.tw/articles/10275295)
- [make of init file by mkinitfs](https://github.com/alpinelinux/mkinitfs)
- [alpine init source](https://github.com/alpinelinux/mkinitfs/blob/master/initramfs-init.in#L66)
