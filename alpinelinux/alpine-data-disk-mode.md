## data-disk-mode 的使用

- 使用 apline 的三種模式
  - 方法，`diskless-mode`: 
    - 使用場景 : 就是一般的 Live-CD 模式，在記憶體中模擬 file-system，`所有的變更無法保存`
    - 使用方法 : 透過 setup-alpine 安裝 alpinelinux 時，`不要`將系統安裝到任何硬碟中 (`Which disk would you like to use ?`，選擇`None`)

  - 方法，`sys-mode`:
    - 使用場景 : 傳統的使用方法，將系統安裝到指定的系統分割硬碟中
    - 使用方法 : 透過 setup-alpine 安裝 alpinelinux 時，`將系統安裝到指定的硬碟中`

  - 方法，`data-disk-mode`:
    - 使用場景 : 可保存變更的diskless-mode，預設在記憶體中模擬 file-system，要保存變更時，需要手動 commit
    
    - 優，和 diskless-mode 一樣運行在記憶體中，運行快速，但是可以手動保存變更，
      diskless-mode 無法保存變更

    - 安裝為 data-disk-mode : 
      - 使用 setup-alpine 前，`將硬碟設置為一般的數據碟(lbu)`，而不是可開機硬碟(boot-disc)
      - 透過 setup-alpine 安裝 alpinelinux 時
        - No disks available. Try boot media /media/cdrom?，輸入`n`  
        - Enter where to store configs ?，輸入`vda1`
    
    - 用於保存變更的數據碟稱為 lbu (local-backup-unit)，配置檔為`lbu.conf`，
      setup-alpine會自動建立`/etc/lbu/lbu.conf`，也可以事後修改內容

## data-disk-mode 的安裝流程 @ qemu

- 測試環境
  - windows 11 23H2 with hyperV enable
  - QEMU emulator version 8.2.0 (v8.2.0-12045-g3d58f9b5c5) 安裝版

- step，qemu [啟動命令](./[example]%20use-data-disk-mode/00_install_and_run.bat)

- step，qemu啟動後，輸入 root 進入系統，<font color=blue>注意</font>，進入系統後，先不要執行setup-alpine，<font color=blue>需要先處理一顆可識別的硬碟出來</font>

- step，建立 data-disk
  - `$ fdisk -l`，確認有未分割的disk

  - `$ fdisk /dev/vda`，建立分割
    
    ```
    n   -> add a new partition
    p   -> primary partition
    1   -> partition number
    63<Enter>         -> first sector
    20971529<Enter>   -> last sector
    w   -> write table to disk and exit
    ```

    建立的分割硬碟是一般的數據碟，而不是可開機硬碟
    
    <img src="./[example] use-data-disk-mode/check-disk.png" width=800 height=auto>

- step，建立檔案系統，`$ mkdosfs /dev/vda1`

- step，將建立的分割，掛載到 `/media/vda1` 的路徑上
  
  ```
  # 建立掛載點
  mkdir -p /media/vda1
  # 設置開機自動掛載
  echo "/dev/vda1 /media/vda1 vfat rw 0 0" >> /etc/fstab
  # 根據 fstab 進行手動掛載
  mount -a
  ```

- step，執行 setup-alpine
  - No disks available. Try boot media /media/cdrom?，輸入`n`  
  - Enter where to store configs ?，輸入`vda1`
  - Enter apk cache directory，輸入`/media/vda1/cache`
  
  - 完成安裝後，
    - setup-alpine 會自動建立`/etc/lbu/lbu.conf`，檢視配置檔的內容，應該會有`LBU_MEDIA=vda1`
    - setup-alpine 會自動建立`/media/vda1/cache`的路徑
    - `$ vi /etc/apk/repositories`， 開放 community
    - `$ apk update`
    - 輸入`$ lbu commit`，保存剛剛安裝的系統
    - 透過 `$ ls -al /media/vda1`，可以看到 commit 後產生 `<hostname>.apkovl.tar.gz` 的檔案

- step，日常使用
  - 每次進入系統都需要`透過ISO開機`，
    因為，在`data-disk-mode`下，硬碟映像檔(*.qcow2) 只是用來儲存data的地方，無法開機

  - 開機後，
    - 系統自動從 `/media/vda1` 載入上次的配置 (/media/vda1/主機名.apkovl.tar.gz)
    - 系統自動從 `/media/vda1/cache` 載入已安裝的軟體

  - 使用的過程中，若要保存變更(更改配置或建立新軟體)，需要手動執行 `$ lbu commit`，否則重開機後會消失

## ref

- [Running Alpine in Live mode in QEMU](https://wiki.alpinelinux.org/wiki/Running_Alpine_in_Live_mode_in_QEMU)

- [How to run Alpine Linux in memory](https://www.youtube.com/watch?v=DM2-5C2SOL0)

- [Data-Disk-Mode @ alpine-wiki](https://wiki.alpinelinux.org/wiki/Installation#Data_Disk_Mode)

- [使用ext4作為數據碟的範例](https://cloud-atlas.readthedocs.io/zh-cn/latest/linux/alpine_linux/alpine_local_backup.html#alpine-linux)

- [lbu命令的使用](https://cloud-atlas.readthedocs.io/zh-cn/latest/linux/alpine_linux/alpine_local_backup.html#lbu)