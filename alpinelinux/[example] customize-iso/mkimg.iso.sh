
profile_iso() {
    profile_standard
    kernel_cmdline="unionfs_size=512M console=tty0 console=ttyS0,115200"
    syslinux_serial="0 115200"
    kernel_addons="zfs"
    apks="$apks iscsi-scst zfs-scripts zfs zfs-utils-py
        cciss_vol_status lvm2 mdadm mkinitfs mtools nfs-utils
        parted rsync sfdisk syslinux util-linux xfsprogs
        dosfstools ntfs-3g"
    local _k _a
    for _k in $kernel_flavors; do
        apks="$apks linux-$_k"
        for _a in $kernel_addons; do
            apks="$apks $_a-$_k"
        done
    done
    apks="$apks linux-firmware"

    # genapkovl-*.sh 的檔案，預設放置在 aports/scripts 目錄中，apkovl變數不需要包含路徑，只需要指定檔名
    apkovl="genapkovl-iso.sh"
}
