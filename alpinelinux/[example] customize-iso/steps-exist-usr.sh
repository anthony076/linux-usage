
doas apk add alpine-sdk alpine-conf syslinux xorriso squashfs-tools grub grub-efi doas

# 添加已經存在的成員，
doas usermod -aG abuild ching
# 察看結果，getent group abuild

doas sh -c 'echo -e "permit :abuild\npermit persist :abuild" >> /etc/doas.d/doas.conf'

echo "set -x PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin" >> ~/.config/fish/config.fish
source ~/.config/fish/config.fish

abuild-keygen -i -a

git clone --depth=1 https://gitlab.alpinelinux.org/alpine/aports.git

doas apk update

mkdir -pv ~/tmp
chmod 700 /home/ching/tmp
echo 'set -x TMPDIR /home/ching/tmp' >> ~/.config/fish/config.fish
echo 'set -x PROFILENAME ching' >> ~/.config/fish/config.fish
source ~/.config/fish/config.fish

hx ~/aports/scripts/mkimg.$PROFILENAME.sh
# 寫入以下內容
# profile_ching() {
    # profile_standard
    # kernel_cmdline="unionfs_size=512M console=tty0 console=ttyS0,115200"
    # syslinux_serial="0 115200"
    # kernel_addons="zfs"
    # apks="$apks iscsi-scst zfs-scripts zfs zfs-utils-py
            # cciss_vol_status lvm2 mdadm mkinitfs mtools nfs-utils
            # parted rsync sfdisk syslinux util-linux xfsprogs
            # dosfstools ntfs-3g"
    # local _k _a
    # for _k in $kernel_flavors; do
            # apks="$apks linux-$_k"
            # for _a in $kernel_addons; do
                    # apks="$apks $_a-$_k"
            # done
    # done
    # apks="$apks linux-firmware"
    
    # # genapkovl-*.sh 的檔案，預設放置在 aports/scripts 目錄中，apkovl變數不需要包含路徑，只需要指定檔名
    # apkovl="genapkovl-ching.sh"
# }

chmod +x ~/aports/scripts/mkimg.$PROFILENAME.sh

cp ~/aports/scripts/genapkovl-dhcp.sh ~/aports/scripts/genapkovl-ching.sh

cd ~/aports/scripts
sh /home/ching/aports/scripts/mkimage.sh --tag edge --outdir ~/ching --arch x86_64 --repository https://dl-cdn.alpinelinux.org/alpine/edge/main --profile ching