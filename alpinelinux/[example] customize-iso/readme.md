## 使用方法

- step1，切換到`使用者iso`，`$ su - iso`

- step2，根據需求，修改 `~/aports/scripts/mkimg.iso.sh` 和 `~/aports/scripts/genapkovl-iso.sh`
  - `~/aports/scripts/mkimg.iso.sh`，用於安裝套件
  - `~/aports/scripts/genapkovl-iso.sh`，用於設置環境

- step3，執行`build-iso.sh`

- 注意，建構時，需要`提供穩定的網路`，且`文件中盡量使用英文`，以避免遇到 XXXX not found 的問題，
  
  例如，`/usr/bin/fakeroot: line 175: /home/iso/.mkimage//home/iso/aports/scripts/genapkovl-iso.sh: not found`