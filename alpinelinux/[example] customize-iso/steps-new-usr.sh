

doas apk add alpine-sdk alpine-conf syslinux xorriso squashfs-tools grub grub-efi doas

doas adduser iso -G abuild

doas sh -c 'echo -e "permit :abuild\npermit persist :abuild" >> /etc/doas.d/doas.conf'

su - iso

#sudo sh -c 'echo "set -x PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin" >> ~/.config/fish/config.fish'
echo "set -x PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin" >> ~/.config/fish/config.fish
source ~/.config/fish/config.fish

abuild-keygen -i -a

git clone --depth=1 https://gitlab.alpinelinux.org/alpine/aports.git

doas apk update

mkdir -pv ~/tmp
chmod 700 /home/iso/tmp
echo 'set -x TMPDIR /home/iso/tmp' >> ~/.config/fish/config.fish
source ~/.config/fish/config.fish


echo 'set -x PROFILENAME iso' >> ~/.config/fish/config.fish
source ~/.config/fish/config.fish

hx ~/aports/scripts/mkimg.$PROFILENAME.sh
# 寫入以下內容
# profile_iso() {
    # profile_standard
    # kernel_cmdline="unionfs_size=512M console=tty0 console=ttyS0,115200"
    # syslinux_serial="0 115200"
    # kernel_addons="zfs"
    # apks="$apks iscsi-scst zfs-scripts zfs zfs-utils-py
            # cciss_vol_status lvm2 mdadm mkinitfs mtools nfs-utils
            # parted rsync sfdisk syslinux util-linux xfsprogs
            # dosfstools ntfs-3g"
    # local _k _a
    # for _k in $kernel_flavors; do
        # apks="$apks linux-$_k"
        # for _a in $kernel_addons; do
                # apks="$apks $_a-$_k"
        # done
    # done
    # apks="$apks linux-firmware"
    
    # # genapkovl-*.sh 的檔案，預設放置在 aports/scripts 目錄中，apkovl變數不需要包含路徑，只需要指定檔名
    # apkovl="genapkovl-iso.sh"
# }


chmod +x ~/aports/scripts/mkimg.$PROFILENAME.sh

cp ~/aports/scripts/genapkovl-dhcp.sh ~/aports/scripts/genapkovl-iso.sh

cd ~/aports/scripts
sh /home/iso/aports/scripts/mkimage.sh --tag edge --outdir ~/iso --arch x86_64 --repository https://dl-cdn.alpinelinux.org/alpine/edge/main --profile iso
