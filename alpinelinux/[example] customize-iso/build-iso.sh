
# 此檔案可以放置在任何路徑中執行
cd ~/aports/scripts

sh /home/iso/aports/scripts/mkimage.sh --tag edge --outdir ~/iso --arch x86_64 --repository https://dl-cdn.alpinelinux.org/alpine/edge/main --profile iso
