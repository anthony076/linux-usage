## alpinelinux

- [利用 qemu 安裝alpine的命令](https://gitlab.com/anthony076/docker-projects/-/blob/main/%5B%20Virtual%20Machine%20%5D/qemu/%5Bexample%5D%20build-alpine-with-qemu/install.bat)

## 安裝 alpine

- step，`localhost login`，輸入`root`登入
- step，輸入 `$ setup-alpine` 執行安裝程序
- step，`Select keyboard layout`，輸入`us`
- step，`Select variant`，輸入`us`
- step，`hostname`，輸入`my-alpine`
- step，`Available interfaces`，輸入`eth0`
- step，`Ip address for eth0`，輸入`dhcp`
- step，`Do you want to do any manual network configuration`，輸入`n`
- step，設置root密碼
- step，`timezone`，輸入`Asia/Taipei`
- step，`http/ftp proxy URL`，輸入 none
- step，選擇時間同步的客戶端(Network-Time-Protocol Client)，輸入`busybox`
  - 選項:BusyBox NTP Client，
    - 優，輕量、適合嵌入式，
    - 缺，需要配置多個NTP服務器才能進行高精度同步
  - 選項:OpenNTPD，
    - 優，開源，能實現高精度同步，
    - 缺，資源消耗多，配置和管理更複雜
  - 選項:Chrony，
    - 優，廣泛被使用，高效率同步，有效減少同步延遲，穩定，支援多種協定
    - 缺，複雜配置，學習曲線高，系統資源消耗高

- step，`APK mirror`，輸入`f`，find and use fastest mirror
- step，`Setup a user`，輸入`no`
- step，`Which ssh server`，輸入`openssh`
- step，`Allow root ssh login` ? 輸入`yes`
- step，`Enter ssh key for root` ? 輸入`none`
- step，`Which disk would you like to use` ? 輸入`vda`
- step，`How would you like to use it` ? 輸入`sys`
- step，`Erase the above disk and continue` ? 輸入`y`
- step，輸入 `$ reboot`，重新啟動系統
- step，啟用 sshd，並設置允許root使用密碼登入，
  - 參考，[openssh的設置](../sw/openssh.md)，設置後需要重啟
  - 在windows上，透過 `ssh -p 連接埠 root@localhost` 進行連接
    
    若出現`WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!`的錯誤，
    到 `C:\Users\ching\.ssh\known_hosts` 將指定行刪除

    編輯後重新登入

    若出現 `Are you sure you want to continue connecting (yes/no/[fingerprint])?`，
    表示修正成功，輸入 `yes`

## 安裝為 data-disk-mode (將alpine運行在記憶體中)

參考，[alpine-data-disk-mode](alpine-data-disk-mode.md)

## 安裝為 uefi

要將 alpine 安裝在 uefi 中，需要使用 uefi-fw 的 bios 來啟動安裝光碟

setup-alpine 會根據 bios-fw 來決定對應的磁碟分區
- 若 bios-fw 為 grub : setup-alpine 自動建立 grub 的分區
- 若 bios-fw 為 uefi : setup-alpine 自動建立 uefi 的分區

- [實際範例](./[example]%20install-with-uefi/00_install.bat)

參考，[如何使qemu使用uefi-bios進行開機](https://gitlab.com/anthony076/docker-projects/-/blob/main/%5B%20Virtual%20Machine%20%5D/qemu/qemu-uefi.md)

參考，[setup-apline如何安裝uefi的源碼](https://github.com/alpinelinux/alpine-conf/blob/4d39126c8de2baf1d399c9f90453e0b3d23bb3f3/setup-disk.in)

## 套件管理 apk 的使用

- 修改 package-index 來源
  - `$ vi /etc/apk/repositories`
  - (optinal) 將 community 行取消註解後保存
  - (optional) 改用 edge 軟件庫，最新的套件都會放在此軟件庫中，有可能會造成系統不穩定
    ```
    http://dl-cdn.alpinelinux.org/alpine/edge/main
    http://dl-cdn.alpinelinux.org/alpine/edge/community
    http://dl-cdn.alpinelinux.org/alpine/edge/testing
    ```
  - 重新更新index以建立索引，`$ apk update`

- 更新
  - 更新使用者套件，`apk update`，此命令只根據使用者安裝的套件索引來更新，不會更新系統套件和其依賴
  - 更新系統，`apk upgrade`，此命令會先執行 apk update，再升級系統中的所有包及其依賴項

- 安裝套件，`apk add 套件名`

- 移除套件，
  - 移除套件，但保留依賴，`apk del 套件名`
  - 完整移除套件，移除套件+依賴，`apk del --purge 套件名`

- 查詢套件，
  - 顯示已安裝套件，`apk info` 或 `apk list --installed`
  - 查詢已安裝套件建立的檔案，`apk info -L 套件名`
  - 顯示指定套件的依賴關係，`apk info -R 套件名`
  - 列出所有套件和其依賴，`apk dot --installed`

## 清理

```
# 清理 apk 緩存
apk cache clean --purge # 清理所有已安装软件的缓存
apk cache clean --purge 套件名 # 清理指定软件的缓存

# 清理临时文件
# 查找並刪除超過 7 天的臨時文件
find /tmp -type f -mtime +7 -exec rm {} \;
```

## 推薦套件

- 用於建構專案的工具包
  - build-base
  - alpine-sdk
  - pkgconfig : 用於管理庫和其依賴關係的工具
    - 提供編譯和鏈接標誌
    - 處理庫的依賴關係
    - 簡化構建腳本

- util-linux，包含多個Linux系統實用程序的集合
  - 文件系統相關
    
    cat、chattr、chmod、chown、chgrp、cksum、cmp、cp、dd、du、df、file、fuser、ln、ls、mkdir、mv、pax、pwd、rm、rmdir、split、tee、touch、type、umask、who、write、

  - 進程管理相關
    
    at、bg、crontab、fg、kill、nice、ps、time、

  - 用戶環境相關
    
    env、exit、logname、mesg、talk、tput、uname、who、

  - 文本處理相關

    awk、basename、comm、csplit、cut、diff、dirname、ed、ex、fold、head、iconv、join、m4、more、nl、paste、patch、printf、read、sed、sort、strings、tail、tr、troff、uniq、vi、wc、xargs

  - 其他實用程序相關
    
    bc、cal、expr、lp、od、sleep

- rust and cargo
  - 方法，手動編譯
    - 安裝基本庫，`$ apk add build-base`
    - 透過 rustup-script 進行安裝 cargo，
      `$ curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`
    - 安裝後需要重啟，透過`$ cargo --help`驗證是否安裝成功
    - 移除，`$ rustup self uninstall`
  
  - 方法，~~透過`$ apk add rust cargo`，不推薦，此方法有可能會缺少其他重要套件~~

- man 相關套件
  - 安裝，`$ apk add mandoc man-pages`
    - mandoc，提供 man 命令
    - man-pages，提供 Linux 系统的手册页集合

  - 安裝缺失套件文檔，`$ apk add 套件名-doc`

- 調整硬碟大小的工具，
  - 安裝，`$ apk add e2fsprogs-extra`，
  - 功能，提供 resize2fs 的命令，透過 qemu 調整硬碟映像檔的大小後，透過此工具可以重新調整硬碟的大小

## shell 相關

- 注意，alphine 預設使用 sh 作為預設的 shell，
  - 透過 `$ echo $SHELL`，顯示預設的shell，使用的是 /bin/sh
  - 透過 `$ cat /etc/passwd`，顯示預設的shell，使用的是 /bin/sh
  - 透過 `$ ls -al /bin`可以看出，實際上是建立 sh 的軟連結到 busybox，
  - 因為是透過 busybox 模擬，因此`.bashrc`的內容是不起作用的，
    但是使用第三方的shell則沒有此限制

- 修改預設 shell
  - 方法，手動修改 /etc/passwd，例如，`vi /etc/passwd`，將`/bin/sh`替換成`/usr/bin/fish`
  - 方法，透過第三方套件，shadow，
    ```
    apk add shadow
    chsh 使用者名
    ```

## 安裝桌面系統

參考，[alpine-desktop](alpine-desktop.md)

## 製作自己的 alpine-linux-iso

參考，[alpine-customize-iso](alpine-customize-iso.md)

## 製作自己的 alpine-qemu-vm

參考，[alpine-make-vm-image](https://github.com/alpinelinux/alpine-make-vm-image/tree/master)

## on-going

- [iso-download](https://www.alpinelinux.org/downloads/)
- [official alpine wiki](https://wiki.alpinelinux.org/wiki/Installation)
- [requirements for installing alpinelinux](https://wiki.alpinelinux.org/wiki/Requirements)
- [在virtualbox上安裝alpinelinux](https://wiki.alpinelinux.org/wiki/Install_Alpine_on_VirtualBox)
- [Alpine Linux: The First Installation](https://www.youtube.com/watch?v=8WYgynP8VJ8)
- [packer-alpine](https://github.com/bobfraser1/packer-alpine/blob/main/http/answers)

## ref 

- install
  - [Installing Alpine Linux as a Desktop OS](https://www.youtube.com/watch?v=YNYtJ3jyMRs)
  - [Alpine Linux: The First Installation](https://www.youtube.com/watch?v=8WYgynP8VJ8)

- desktop
  - [Archlinux+Sway+Windows11完全配置記錄](https://github.com/CelestialCosmic/themeblog/issues/22)
  - [sway配置範例 @ClayMav](https://gist.github.com/ClayMav/ad7ca21b0f8161ceb6fe3c85ac111a5e)
  - [sway配置範例 @FZUG](https://github.com/FZUG/repo/wiki/Sway-WM)
  - [install sway on apline @ alpine-wiki](https://wiki.alpinelinux.org/wiki/Sway)

- sw
  - [What is the alpine equivalent to build-essential?](https://github.com/gliderlabs/docker-alpine/issues/24)

- [Change default shell](https://wiki.alpinelinux.org/wiki/Change_default_shell)