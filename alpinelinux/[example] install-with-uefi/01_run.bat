@echo off

cd %~dp0

:: get fd file
:: [get ovmf.fd from clearlinux](https://github.com/clearlinux/common)

qemu-system-x86_64.exe ^
-m 8G -smp 4 -M q35 ^
-accel whpx,kernel-irqchip=off ^
-machine vmport=off ^
-boot order=c ^
-bios ovmf.fd ^
-drive file=alpine-disk.qcow2,if=virtio ^
-display sdl ^
-device qemu-xhci,id=xhci ^
-usb -device usb-tablet ^
-net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22 ^
-monitor stdio
