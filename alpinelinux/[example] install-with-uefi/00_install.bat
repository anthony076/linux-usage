@echo off

cd %~dp0

:: ENV
:: - windows 11 23H2 with hyperV enable
:: - QEMU emulator version 8.2.0 (v8.2.0-12045-g3d58f9b5c5) 安裝版

:: step，download iso
::    use VIRTUAL-version : 
::    Similar to STANDARD-version. Slimmed down kernel. Optimized for virtual systems.
::    https://dl-cdn.alpinelinux.org/alpine/v3.20/releases/x86_64/alpine-virt-3.20.2-x86_64.iso

:: step，get fd file
:: [get ovmf.fd from clearlinux](https://github.com/clearlinux/common)

:: step，create disk-image
::qemu-img create -f qcow2 alpine-disk.qcow2 5G

:: step，start install process

qemu-system-x86_64.exe ^
-m 8G -smp 4 -M q35 ^
-accel whpx,kernel-irqchip=off ^
-machine vmport=off ^
-boot order=dc ^
-bios ovmf.fd ^
-drive file=alpine-disk.qcow2,if=virtio ^
-cdrom alpine-virt-3.20.2-x86_64.iso ^
-display sdl ^
-device qemu-xhci,id=xhci ^
-usb -device usb-tablet ^
-net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22 ^
-monitor stdio
