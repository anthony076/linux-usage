## 透過 setup-desktop 安裝 sway 

- `setup-desktop` 是alpinelinux`預設的腳本`，位於 `/sbin` 目錄下，

  - 參考，[setup-desktop源碼](https://github.com/alpinelinux/alpine-conf/blob/master/setup-desktop.in)
  
  - 除了setup-desktop還以提供以下的腳本來簡化安裝或配置的流程

    setup-acf、setup-alpine、setup-apkcache、setup-apkrepos、setup-bootable、setup-desktop、setup-devd、setup-disk、setup-dns、setup-hostname、setup-interfaces、setup-keymap、setup-lbu、setup-mta、setup-ntp、setup-proxy、setup-sshd、setup-timezone、setup-user、setup-wayland-base、setup-xen-dom0、setup-xorg-base

- 以 `sway wm` 為例，以system-user執行以下
  
  - setup-desktop 預設安裝的套件
    - `sway` : sway wm

    - `elogind` : elogind 是一個用戶會話管理守護進程、登錄管理功能、電源公里、權限提升代理等功能，
      elogind 是從 systemd 的 logind 提取出來的，專門為不使用 systemd 的系統提供類似功能

    - `polkit-elogind`
      - polkit (PolicyKit) 是一個權限授予框架，它允許在桌面系統中的非特權用戶執行需要超級用戶權限的操作
      - polkit-elogind 是一個與 elogind 集成的 polkit 代理，用於在 elogind 管理的會話中處理權限策略

    - `eudev`
      - eudev 是 udev 的分支，專為提供更好的穩定性和性能而設計，適合非systemd的linux系統使用
  
      - udev 是一個動態設備管理器(device-manager)，負責管理系統中的硬件設備

        udev 是 systemd 提供的功能，

        alphinelinux 預設使用 openbox的mdev，但mdev提供的device-manager功能太過簡單，無法滿足sway的需求，
        因此需要改用eudev或udev

        若要手動安裝sway，可透過`$ setup-devd udev`來卸載mdev並安裝eudev

        參考，[桌面系統對device-manager的要求](../usage/desktop/desktop.md#桌面系統的組成元件-軟體的選擇)

    ---

    - `firefox` : 瀏覽器
    - `dmenu` : 一個快速的動態菜單，允許用戶通過鍵盤快捷方式快速訪問應用程序和文件
    - `font-dejavu` : DejaVu 是一組免費、開源的字體家族，基於 Bitstream Vera 字體
    - `foot` : 一個輕量級的終端模塊，用於 sway 和其他 Wayland 窗口管理器
    - `grim` : 一個截圖工具，專為 Wayland 設計
    - `i3status` : 一個用於 i3wm 的狀態條工具，可以顯示系統資訊如網絡狀態、硬盤使用情況等
    - `swaybg` : Sway 的背景管理工具
    - `swayidle` : 一個用於 sway 的自動休眠和鎖定工具
    - `swaylockd` : 一個用於 sway 的屏幕鎖定守護進程
    - `util-linux-login` : 提供了一系列的系統工具，包括登錄管理
    - `wl-clipboard` : 一個複製和黏貼到剪貼板的工具，適用於 Wayland 環境
    - `wmenu` : 一個用於 Wayland 的菜單工具
    - `xwayland` : 一個兼容 X11 的 Wayland 進程，讓原本設計為 X11 的應用程序可以在 Wayland 環境中運行

  - step，透過 setup-desktop 安裝 sway-vm
    - 輸入 `$ doas setup-desktop` 執行桌面系統的安裝程序
    - Setup a user ?，輸入`no`，已經使用 system-user執行，就不需要另外建立使用者帳號
    - Which desktop environment ?，輸入`sway`

  - step，(選用) 移除不需要的套件

    ```shell
    sudo apk del i3status --purge
    sudo apk del firefox --purge
    ```

  - step，將sway預設配置複製到個人目錄中
    
    ```shell
    mkdir -p ~/.config/sway
    cp /etc/sway/config ~/.config/sway/config
    ```

  - 修改配置
    ```
    # 將mod鍵設置為ALT鍵
    set $mod Mod1
    ```

    <font color=red>重啟系統</font>，`$ doas reboot`，某些配置在reboot後才會生效

  - 進入 sway ，`$ sway`

  - 預設快速鍵
    - `alt+Enter` : 打開終端
    - `alt+Shift+q` : 關閉當前視窗
    - `alt+d` : 打開dmenu
    - `alt+Shift+c` : 重新載入配置
    - `alt+Shift+e` : 離開sway

  - step，(選用) 設置開機自動執行 sway
    
    ```
    # for fish shell，將以下添加到 ~/.config/fish/conf.d/sway.fish
    # 其他終端參考，https://github.com/swaywm/sway/wiki

    set TTY1 (tty)
    [ "$TTY1" = "/dev/tty1" ] && exec sway
    ```

## 手動安裝 sway

```shell

# ===== apk update =====

doas apk update
doas apk upgrade

# ===== install required package =====

doas apk add curl \
sway swaybg swayidle swaylockd util-linux-login wl-clipboard xwayland \
foot dmenu font-dejavu wmenu grim i3status 

doas apk add elogind polkit-elogind

# ===== disable hwdrivers =====

doas rc-service --ifstarted --quiet hwdrivers stop
doas rc-update delete --quiet --quiet hwdrivers sysinit || :

# ===== disalbe mdev =====

doas rc-service --ifstarted --quiet mdev stop
doas rc-update delete --quiet --quiet mdev sysinit || :

# 無法刪除busybox-mdev-openrc，被 alpine-base 依賴
#doas apk del busybox-mdev-openrc --purge

# ===== disable mdevd =====

# 沒有安裝 mdevd
#doas rc-service --ifstarted --quiet mdevd stop
#doas rc-update delete --quiet --quiet mdevd-init sysinit 2>/dev/null || :
#doas rc-update delete --quiet --quiet mdevd sysinit 2>/dev/null || :

# ===== enable udev =====

# udev-init-scripts 是空的
#doas apk add --quiet eudev udev-init-scripts udev-init-scripts-openrc

# udev-init-scripts-openrc 提供 udev-trigger、udev-settle、udev-postmount
doas apk add eudev udev-init-scripts-openrc

doas rc-update add udev sysinit
doas rc-update add udev-trigger sysinit
doas rc-update add udev-settle sysinit
doas rc-update add udev-postmount default

doas rc-service --ifstopped udev start
doas rc-service --ifstopped udev-trigger start
doas rc-service --ifstopped udev-settle start
doas rc-service --ifstopped udev-postmount start

# ===== enable elogind、polkit、dbus =====

doas rc-service elogind start
doas rc-update add elogind

doas rc-service polkit start
doas rc-update add polkit

doas rc-service dbus start
doas rc-update add dbus

# ===== create customize sway config =====

mkdir -p ~/.config/sway
cp /etc/sway/config ~/.config/sway/config

hx ~/.config/fish/config.fish 

# add below line to config.fish
set -x PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# ===== enter sway ====

doas reboot
sway

# ===== ref ====

# 成功進入後，sway的環境變數
MAIL=/var/mail/ching
USER=ching
fish_greeting=
XDG_SEAT=seat0
XDG_SESSION_TYPE=wayland
XCURSOR_SIZE=24
SHLVL=4
SWAYSOCK=/run/user/102/sway-ipc.102.2460.sock
HOME=/home/ching
LC_CTYPE=en_US.UTF-8
COLORTERM=truecolor
WAYLAND_DISPLAY=wayland-1
LOGNAME=ching
XDG_SESSION_CLASS=user
XDG_SESSION_ID=c1
TERM=foot
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
XDG_RUNTIME_DIR=/run/user/102
DISPLAY=:0
SHELL=/usr/bin/fish
XDG_VTNR=1
PWD=/home/ching
I3SOCK=/run/user/102/sway-ipc.102.2460.sock
```