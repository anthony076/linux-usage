## 製作自己的 alpine linux iso

- 基礎

  - 建構流程

    ```mermaid
    flowchart LR
    a[mkimage.sh @ aport] --> b[mkimg.saigo.sh] --> c[genapkovl-saigo.sh]
    ```

    其中

    - `mkimage.sh`，是由 [alpine-aports](https://github.com/alpinelinux/aports) 提供的腳本，
      專門用於構建 iso 檔，欲使用 mkimage.sh，需要使用者定製`mkimg.*.sh`和`genapkovl-*.sh`兩個檔案

    - `mkimg.*.sh`，
  
    - `genapkovl-*.sh`，通常在生成訂製化的 Alpine Linux 鏡像時被調用，是 generate-apk-overlay 的簡稱

      這些腳本用於創建覆蓋文件系統（Overlay File System），這個覆蓋文件系統包含一些系統配置和額外的軟件包，可以在系統啟動時加載，以便自定義 Alpine Linux 的運行時環境。

  - 依賴套件

    ```
    apk add alpine-sdk alpine-conf syslinux xorriso squashfs-tools grub grub-efi doas
    ```

    <font color=blue>若需要EFI，額外需要以下套件</font>
    ```
    apk add mtools dosfstools grub-efi
    ```

  - 需要建立 abuild 群組

    需要建立專門構建ISO專用的使用者，並將該使用者<font color=red>添加到 abuild 的群組中</font>

    <font color=blue>abuild，是 apline 將一般套件轉換為 APK 套件的build-scripts，</font>
    
    使用時，使用者需要添加到abuild群組，才能擁有特定的權限來執行構建和安裝軟件包的操作，例如
    - 需要訪問和修改特定的系統資源，例如構建目錄和包簽名密鑰
    - 使用私有簽名密鑰來簽署軟件包
    - 要求用戶在特定的環境中工作，這個環境可能包括特定的目錄結構和權限設置

    <font color=blue>只有APK套件才能被APK命令進行統一管理</font>，
    abuild 提供了建構APK套件工具，並提供了以下的功能

    - 依賴管理 : `解析並安裝構建所需的依賴項`，確保所有必要的依賴項都已安裝
    - 環境設置 : `在乾淨的 chroot 環境中構建軟件包`，確保構建環境的可重複性和隔離性
    - 構建步驟的自動化 : `abuild 腳本(APKBUILD 文件)`定義了軟件包的構建過程，
      包括獲取源代碼、應用補丁、配置、編譯、測試和安裝等步驟
    - 版本控制 : 記錄軟件包的版本信息，確保軟件包的`版本管理`和發布過程的規範性
    - 簽名和驗證 : 對構建好的軟件包`進行數字簽名`，以確保軟件包的完整性和來源的可信性
    - 生成 APK 檔案 : 根據構建結果`生成.apk`，供系統安裝和分發使用
    - 元數據生成：生成軟件包的`元數據文件`，包括描述、依賴關係、提供的文件列表等信息，供包管理器使用。

## 範例，依照官方教程建立最基礎的ISO

- 測試環境
  - windows 11 23H2 with hyperV enable
  - QEMU emulator version 8.2.0 (v8.2.0-12045-g3d58f9b5c5) 安裝版
  - [alpine-03-sway-basic](https://gitlab.com/anthony076/alpine-vms)

- [完整範例]([example]%20customize-iso/steps-exist-usr.sh)

- 流程
  - step，安裝依賴套件
    
    ```
    doas apk add alpine-sdk alpine-conf syslinux xorriso squashfs-tools grub grub-efi doas
    ```

  - step，建立使用者，`$ doas adduser iso -G abuild`

    建構過程中，會透過 abuild-keygen 產生套件簽章需要的密鑰，
    
    而 abuild-keygen 是由 abuild套件所提供，也是 alpine-sdk 套件組合包的其中一個套件，
    在建構的過程中，只有添加到abuild群組的使用者，才被允許使用abuild-keygen命令
  
  - step，變更 abuild 群組的權限，

    `$ doas vi /etc/doas.d/doas.conf`，輸入以下內容

    ```
    permit :abuild
    permit persist :abuild
    ```
  
  - step，切換使用者，`$ su - iso`
    
  - step，修改 fish 的環境變數，確保相關命令可用

    `$ doas vi /home/iso/.config/fish/config.fish`，輸入以下

    ```
    set -x PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
    ```

    重新載入 config.fish，`$ source ~/.config/fish/config.fish`
    
  - step，建立用於簽章的 private/public-keys
    - 注意，此命令會建立`用戶專屬的檔案`和`寫入用戶的環境變數`，

      若使用 root 執行，會導致環境變數或檔案出錯，造成後續建立ISO時出問題，因此，此步驟不應該使用 root 身分執行

    - 命令，`abuild-keygen -i -a`

      ``` 
      Enter file in which to save the key [/root/.abuild/root-66a3c5d5.rsa]: <Enter>	# 私鑰放置的地方
      Installing /home/iso/.abuild/iso-66a3c5d5.rsa.pub to /etc/apk/keys...			# 公鑰放置的地方
      ```

      [abuild-keygen源碼](https://github.com/alpinelinux/abuild/blob/master/abuild-keygen.in)

    - 執行後的結果
      - 結果1，會在`/etc/apk/keys/`下保存公鑰，例如，/etc/apk/keys/iso-66a3c5d5.rsa.pub"，可透過`$ ls /etc/apk/keys/`檢查公鑰正確被安裝

      - 結果2，會在用戶的`/home/iso/.abuild/abuild.conf`中，設置 PACKAGER_PRIVKEY 變數，
      
      例如，`PACKAGER_PRIVKEY="/home/iso/.abuild/iso-66a3c5d5.rsa"`

      可透過`cat /etc/abuild.conf`或`cat ~/.abuild/abuild.conf`，查看 PACKAGER_PRIVKEY 環境變數

      - [參考](https://wiki.alpinelinux.org/wiki/Abuild_and_Helpers)

  - step，下載[aports-repo](https://gitlab.alpinelinux.org/alpine/aports.git)，裡面有官方提供建立 iso 的工具

    `$ git clone --depth=1 https://gitlab.alpinelinux.org/alpine/aports.git`

  - step，更新 apk 套件，確保iso中的套件是最新的，`doas apk update`
  
  - step，(選用) 確認`/tmp`的空間必須大於 1GB

    - 為什麼需要檢查`/tmp`的空間大小

      建立iso時，alpine會將需要安裝的套件保存在/tmp的路徑中，若安裝的套件超過/tmp空間大小的限制，就有可能造成失敗

    - 查看/tmp的可用空間，`$ df -h`
    
    - 若 /tmp 的空間只有預設的1G空間大小，可手動變更/tmp的路徑

      ```
      mkdir -pv ~/tmp     # 建立 /home/iso/tmp 的路徑
      chmod 700 /home/iso/tmp     # 授予權限
      export TMPDIR=/home/iso/tmp # 暫時變更 tmp 的預設路徑
      
      # 或永久變更 tmp 的預設路徑
      echo 'set -x TMPDIR /home/iso/tmp' >> ~/.config/fish/config.fish
      source ~/.config/fish/config.fish
      ```

  - step，建立 PROFILENAME 變數，
    - 方法，`$ export PROFILENAME=iso`
    
    - 方法，透過`>>`寫入 config.fish
      ```
      echo 'set -x PROFILENAME iso' >> ~/.config/fish/config.fish
      source ~/.config/fish/config.fish
      ```

    - 方法，透過`<<`寫入 config.fish
      ```
      sh -c 'cat << EOF >> ~/.config/fish/config.fish
      set -x PROFILENAME iso
      EOF'

      source ~/.config/fish/config.fish
      ```
      

  - step，建立自定義的 `mkimg.$PROFILENAME.sh`，用於還原套件

    `vi ~/aports/scripts/mkimg.$PROFILENAME.sh`，輸入以下內容

    ```shell
    profile_iso() {
      profile_standard
      kernel_cmdline="unionfs_size=512M console=tty0 console=ttyS0,115200"
      syslinux_serial="0 115200"
      kernel_addons="zfs"
      apks="$apks iscsi-scst zfs-scripts zfs zfs-utils-py
              cciss_vol_status lvm2 mdadm mkinitfs mtools nfs-utils
              parted rsync sfdisk syslinux util-linux xfsprogs
              dosfstools ntfs-3g"
      local _k _a
      for _k in $kernel_flavors; do
              apks="$apks linux-$_k"
              for _a in $kernel_addons; do
                      apks="$apks $_a-$_k"
              done
      done
      apks="$apks linux-firmware"
            
      # genapkovl-*.sh 的檔案，預設放置在 aports/scripts 目錄中，apkovl變數不需要包含路徑，只需要指定檔名
      apkovl="genapkovl-iso.sh"

    }
    ```

    <font color=blue>使 mkimg.$PROFILENAME.sh 生效</font>

    ```shell
    chmod +x ~/aports/scripts/mkimg.$PROFILENAME.sh 
    
    # 或 chmod +x ~/aports/scripts/mkimg.iso.sh
    ```

  - step，建立自定義的`genapkovl-iso.sh`，用於還原環境配置
    - 注意，genapkovl-iso.sh 會在 mkimg.$PROFILENAME.sh 中指定路徑並調用
    - 注意，在genapkovl-iso.sh中執行的命令，必須先安裝過，才能在genapkovl-iso.sh中使用
    - 命令，`$ cp ~/aports/scripts/genapkovl-dhcp.sh ~/aports/scripts/genapkovl-iso.sh`，
      本範例使用預設的環境配置

  - step，執行建構

    使用者自定義的`mkimg.$PROFILENAME.sh`和`genapkovl-$PROFILENAME.sh`，
    需要放置在`~/aports/scripts`的路徑中

    ```shell
    cd ~/aports/scripts
    sh /home/iso/aports/scripts/mkimage.sh --tag edge --outdir ~/iso --arch x86_64 --repository https://dl-cdn.alpinelinux.org/alpine/edge/main --profile iso
    ```

## ref

- Make a Custom Alpine Linux ISO that Boots to Graphics! (mkimage)
  - [y2b](https://www.youtube.com/watch?v=VGxxxABAl9s)
  - [scripts](https://github.com/EHowardHill/Saigocom)

- [How to make a custom ISO image with mkimage @ alpine-wiki](https://wiki.alpinelinux.org/wiki/How_to_make_a_custom_ISO_image_with_mkimage)

- [aport，官方提供建構iso的scripts](https://github.com/alpinelinux/aports/tree/master)
- [abuild，官方提供建構APK的scripts](https://github.com/alpinelinux/abuild)