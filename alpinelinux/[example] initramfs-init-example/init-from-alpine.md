## initramfs-init-script 執行過程

- [完整代碼](./init-from-alpine)

- 測試環境
  - windows 11 23H2 with hyperV enable
  - QEMU emulator version 8.2.0 (v8.2.0-12045-g3d58f9b5c5) 安裝版
  - [alpine-09-initramfs-init](https://gitlab.com/anthony076/alpine-vms)

- 變數值
  - $sysroot=/sysroot，代表未切換到root-filesystem前，root-filesystem在initramfs中的臨時路徑
  - $repofile = /tmp/repositories
  - $KOPT_root = UUID=ab0d37ae-71ec-4dfb-9dd3-2d1c125d0e68
  - $KOPT_modules = sd-mod,usb-storage,ext4
  - $KOPT_rootfstype = ext4
  - $KOPT_init = /sbin/init
  - $KOPT_quiet = yes
  - 注意，其餘未寫明的變數，在測試時為空

---

- step，建立符合FHS的目錄和必要目錄
- step，建立 busybox 各個命令的軟連結
- step，建立 /dev/null 節點
- step，掛載 /sys (-t sysfs)
- step，掛載 /dev (-t devtmpfs 或 -t tmpfs)
- step，建立 /dev/kmsg 節點  
- step，掛載 /proc (-t proc)
- step，建立和掛載 tty 相關的節點
  - 建立節點，/dev/ptmx
  - 建立路徑，/dev/pts
  - 掛載 /dev/pts (-t devpts)
- step，掛載 /dev/shm (-t tmpfs)
- step，讀取 /proc/cmdline 中設置的 kernel-options
- step，將 kernel-options和myopts，轉換為 KOPT_* 的環境變數
- step，打印 initramfs-init 的版本
- step，設置 KOPT_init 變數
- step，設置 keymap
- step，啟動 bootchart 
- step，從 KOPT_alpine_repo 設置 ALPINE_REPO 變數
- step，建立 modprobe 的 blacklist
  (/etc/modprobe.d/boot-opt-blacklist.conf)
- step，利用 `KOPT_ip`、`KOPT_nbd`、`KOPT_apkovl`、`ALPINE_REPO`
  設置 do_networking 變數 

---

- step，若 KOPT_zfcp 不為空，加載zfcp的內核模塊，並啟用zfcp裝置
- step，若 KOPT_dasd 不為空，加載dasd的內核模塊，並啟用dasd裝置
- step，檢查 s390x 裝置，加載相觀模塊並啟用
- step，設置 rootfstype 變數
- step，加載 boot-drivers，
  包含 sd-mod、usb-storage、ext4、loop、squashfs、simpledrm、af_packet、ipv6 等內核模塊
- step，依需求為vmware加載 ata_piix、mptspi、sr-mod 等內核模塊
- step，設置加密相關的cryptopts變數
- step，設置網路，依需求調用 onfigure_ip()、setup_wireguard()、setup_nbd()
- step，加載網路儲存設備需要的AOE相關內核模塊

---

- step，將 /etc/mtab 軟連結到 /proc/mounts
- step，透過 nlplug-findfs 命令，搜尋並掛載 root-filesystem
- step，若 rootfstype 變數設置為 btrfs，透過 /sbin/btrfs device scan 
  掃瞄並掛載 btrfs 檔案系統，確保 btrfs 類型的 root-filesystem 可用
- step，若設置 KOPT_resume，從休眠中回復休眠的設備
- step，若 KOPT_overlaytmpfs = yes，掛載 [overlayfs 檔案系統](../../linux-core/filesystem.md#overlayfs-檔案系統)
- step，將`/proc/mounts`中，所有`非根目錄`、`非$sysroot目錄`的掛載點
  都移動到$sysroot下
- step，將所有檔案系統的寫操作立即寫入磁盤
- step，透過 switch_root 命令，執行切換根目錄的操作
- step，執行 ash-shell

---

<font color=blue>以下第一次安裝系統或緊急救援才會執行，
若已經安裝系統，則不會執行 </font>

- step，設置`nlplug-findfs命令`需要的`repoopts變數`
- step，透過nlplug-findfs檢測可用的啟動媒體(boot-media)
- step，若設置do_networking變數，調用 configure_ip()

- step，設置掛載`tmpfs`、`sysroot`需要的`rootflags變數`
- step，掛載 tmpfs、sysroot

- step，透過 KOPT_apkovl 設置 ovl 變數
- step，透過 KOPT_pkgs 設置 pkgs 變數
- step，初始化 apk
- step，調用 unpack_apkovl() 解壓縮 ovl 指定的檔案解壓縮到 sysroot
  - 若副檔名是 gz，直接解壓
  - 若檔案是其他加密文件，透過 openssl 解密後再進行解壓縮

- step，若檔案`$sysroot/etc/.default_boot_services`存在，
  或"$ovl"檔案不存在，啟動以下的預設服務，包含

  devfs、dmesg、mdev、hwdrivers、modloop、modules、sysctl、hostname、bootmisc、
  syslog、mount-ro、killprocs、savecache、firstboot、sshd、tiny-cloud-boot、
  tiny-cloud-early、tiny-cloud-main、tiny-cloud-final 等服務

  啟動後，刪除 $sysroot/etc/.default_boot_services 文件

- step，設置和顯示啟動畫面
- step，根據 fstab，`重新定位掛載點`和`調整掛載參數`
- step，將 alpine-base 添加到`pkgs變數`中
- step，添加 apk-keys
- step，重新產生 apk-repositories-file(repofile)，在 relocation 後需要重新產生
- step，修正 /etc/apk/arch 的內容，避免和apkovl不一致
- step，為apk產生repo_opt變數
- step，為 root-filesystem 安裝套件
- step，設置 rtc
- step，啟用對 modloop-verification 的支
- step，設置 apk 選項，設置apkflags變數
- step，綁定掛載 $sysroot/sys、$sysroot/proc、$sysroot/dev 後卸載，
  以`更新檔案系統狀態`，或`清理並重新設置一些狀態`
- step，若設置$ovl_unmount，對$ovl_unmount進行卸載
- step，根據 fstab 重新掛載
- step，若 /etc/apk/repositories 不存在，重新產生
- step，調用setup_inittab_console()，以修正 inittab 內容，並設置預設的tty
- step，複製 DNS 的配置檔，resolv.conf
- step，停止 bootchart
- step，若 /sbin/init 不可執行，打印提示，並進入 sh-shell
- step，切換到 new root
  - 將`/proc/mounts`中，所有`非根目錄`、`非$sysroot目錄`的掛載點
  都移動到$sysroot下
  - 將所有檔案系統的寫操作立即寫入磁盤
  - 透過 switch_root 命令，執行切換根目錄的操作
  - 執行 ash-shell