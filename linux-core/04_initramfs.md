## initramfs 基礎

- 什麼是 initramfs

  開機流程 : bios -> bootloader -> kernel -> initrd 或 initramfs -> init-script (in initramfs) -> init-program (/sbin/init)

  在 kernel 啟動後，在加載驅動程式前，需要檔案系統才能存取驅動程式，參考，[kernel 透過虛擬檔案系統才能載入必要的驅動程式](./03_kernel.md)

  與日常的使用的檔案系統不同，initramfs 是臨時的、只存在於內存的、僅用於開機階段的虛擬檔案系統，
  虛擬檔案系統將檔案和資源`以樹狀路徑`的方式供 kernel 存取

- initramfs中的init腳本 (initramfs-init-script，/initramfs/init)

  是放在置 initramfs 中的腳本，位於/initramfs/init，通常由`製作initramfs的套件會一起提供`，
  主要用於為核心提供一些必要的初始化工作，使 kernel 和後續系統能夠正常運作的相關操作，可區分為

  - `建立必要的目錄，建立必要的節點並掛載`
  - `安裝必要的系統套件`
  - `載入必需的驅動程式`：載入與root-filesystem相關的驅動程式，特別是在root-filesystem位於 RAID、LVM 或加密分區時
  - `初始化硬件設備` : 使用 udev 或 mdev 等工具來初始化基本硬件設備
  - `準備root-filesystem` : initramfs 最重要的任務是<font color=red>掛載root用戶的root-filesystem</font>
  - 切換到root-filesystem : initramfs 的 init-script 會從臨時的 initramfs 切換到實際的root-filesystem
  - `執行/sbin/init` : 將控制權交給真正的init應用程式
  
- initramfs-init-script(/initramfs/init) 和 init-program(/sbin/init) 的區別
  - 為核心提供一些必要的初始化工作
  - init-program 是放置在`/sbin/init`的路徑中，用於執行和管理用戶日常的service，例如，openrc、sysv、systemd、... 等

- 建構 initramfs
  - [手動建立initramfs的內容 @ simple-distro](../makeDistro/simple-distro/simple-distro.md)
  - [手動建立initramfs的內容 @ y2b](https://www.youtube.com/watch?v=c4j6z2huJxs)
  - 透過 docker 的 multi-stage 可以簡化 initramfs 的建構，減少編譯系統套件占用的空間，
  參考，[docker-to-initramfs](https://github.com/jqueuniet/docker-to-initramfs)
  - [基於busybox的initramfs](https://digwtx.com/initramfs.html#init)

- 建構 initramfs-init-script
  - [initramfs-init-script實際範例 @ alpine](../alpinelinux/[example]%20initramfs-init-example/)

## initrd 與 initramfs 的區別

- 兩者都是開機用的虛擬檔案系統
  - (舊) initrd = Initial-RAM-Disk
  - (新) initramfs = Initial-RAM-Filesystem

- 比較表 

  | 比較項目  | initrd                                             | initramfs                                |
  | --------- | -------------------------------------------------- | ---------------------------------------- |
  | 縮寫      | initial-ramdisk                                    | initial-ram-filesystem                   |
  | 作用      | 用於將虛擬的檔案系統還原到內存                     | 同左                                     |
  | 內容      | 磁碟分區、配置文件、必要的系統程式、部分驅動模組   | 同左                                     |
  | 優點      | 舊的虛擬檔案系統                                   | 新的虛擬檔案系統，更簡單和高效           |
  | 存續時間  | 在核心檔案載入後卸載，改掛載真實系統地根目錄       | 同左                                     |
  | 檔案      | block-device的鏡像檔，需要額外的檔案系統來掛載     | CPIO格視的壓縮檔，可直接解壓縮到記憶體中 |
  | 儲存文件  | 只能儲存靜態文件                                   | 可儲存靜態文件或動態文件                 |
  | 掛載/卸載 | 手動掛載/卸載，掛載到 ext2                         | 自動掛載/卸載，掛載到 tmpfs              |
  | 優缺點    | 缺，需要額外的檔案系統驅動，需要對block-device緩存 | 優，不需要額外的檔案系統、不浪費記憶體   |

- initrd 或 initramfs 兩者擇一使用，新系統建議使用 initramfs
  
- 即使現在使用的是 initramfs，許多地方仍然使用 initrd 這個名稱，特別是在bootloader中

## initramfs 壓縮檔的內容

- 目錄
  - /bin : 基本的二進制執行檔
  - /sbin : 系統二進制檔案，例如，init, modprobe, switch_root
  - /dev : 基本的設備檔案，例如，null, console, tty, tty1, ram0
  - /etc : 配置檔案，例如，fstab, mtab, passwd, group
  - /lib : 共享庫和內核模組
  - /tmp : 用於臨時檔案
  - /root : root 用戶的家目錄
  - /usr : 包含用戶額外的二進制檔案、庫和數據
  - /var : 用於可變數據，如日誌
  - /mnt 或 /new_root : 用作真實根檔案系統的掛載點
  - /proc : 作為特殊用途的掛載點，提供有關系統進程和系統狀態的信息
  - /sys : 作為特殊用途的掛載點，提供一個結構化的方式來訪問內核對象、設備驅動和其他系統組件
  - /run : 用於運行時數據
  - /init : init程序的實際檔案

- 工具和檔案
  - 基本系統工具 : 輕量級的 shell、基本的系統命令和工具
  - 內核模組 : 檔案系統驅動程式、硬體驅動程式、加密模組
  - 掛載根檔案系統所需的腳本 : init 或其他輔助腳本
  - 設定檔 : 臨時的系統設定
  - 特定發行版的工具和腳本
  - 硬體檢測和設定工具 : 用於早期系統啟動階段的硬體偵測和設定
  - 網路相關工具 : 如果需要通過網路掛載根檔案系統
  - LVM工具 : 如果使用 LVM 管理磁碟
  - RAID 工具 : 如果使用軟體 RAID
  - 加密工具 : 如果使用加密的根檔案系統（如 LUKS）
  - 緊急恢復工具 : 用於系統無法正常啟動時的故障排除

## initramfs 的操作和製作

- [在alpine操作initramfs](../alpinelinux/alpine-initramfs-op.md)

- [在archlinux操作initramfs](../archlinux/arch+kernel.md)

## ref

- [What’s the Difference Between initrd and initramfs](https://www.baeldung.com/linux/initrd-vs-initramfs)
