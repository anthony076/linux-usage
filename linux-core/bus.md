## bus 概述

- 透過 `$ ls -al /sys/bus`，可以查看系統上所有的bus

- /sys/bus 的用途
  - 是 Linux 系統中硬件抽象層的重要組成部分，提供了一個統一的接口來查看和修改設備配置
  - 顯示連接到各種總線的設備的詳細信息
  - 支持熱插拔設備的動態添加和移除，並反映實時的硬件配置狀態
  - 展示設備與其對應驅動程序之間的關係
  - 在系統初始化過程中，內核會掃描和填充 /sys/bus，用於識別和配置已連接的硬件設備
  - 在編寫或調試設備驅動時會訪問此目錄，用於測試驅動程序與設備的交互

- 常見的 bus
  - acpi (Advanced Configuration and Power Interface) : 管理電源和硬件配置
  - auxiliary : 輔助設備的通用總線，連接不屬於其他標準總線的輔助設備
  - clockevents、clocksource : 提供系統計時和事件調度，用於內核定時器、進程調度
  - container : 管理容器化資源
  - cpu : CPU 相關操作和管理
  - dax (Direct Access) : 直接訪問持久內存，常用於高性能存儲系統
  - event_source : 事件源管理，例如，跟踪和調試系統事件
  - hid (Human Interface Device) : 人機接口設備管理，例如，鍵盤、鼠標、遊戲控制器
  - i2c : 低速設備通信協議
  - memory : 內存管理
  - nd (Non-Volatile Memory) : 非易失性內存管理，例如，SSD、持久內存設備
  - nvmem (Non-Volatile Memory) : 小容量非易失性內存管理
  - pci、pci_express : 高速外設連接
  - platform : 平台特定設備管理
  - pnp : 即插即用設備管理
  - scsi (Small Computer System Interface) : 存儲設備接口，例如，硬盤、磁帶機、某些類型的 SSD
  - serial-base、serio : 串行設備管理
  - usb : 通用串行總線設備管理
  - virtio : 虛擬化 I/O 設備
  - workqueue : 內核工作隊列管理
  - xen、xen-backend : Xen 虛擬化支持