## Bootloader 基礎

- bootloader 分為`一階段bootloader`或`兩階段bootloader`
  - 因MBR的空間有限，因此引導程式分為一階段，或兩階段的引導程式
    
    注意，不是每個 bootloader 都是兩階段，
    兩階段bootloader通常會發生在支援多作業系統開機選單的作業系統，
    由第1階段的`MBR中的bootloader`指向第2階段`boot-section中的bootloader`

    無論是一階段或兩階段，最後都會載入/啟動內核，執行驅動，並執行 init 程式

  - 兩階段bootloader
    - `第一階段的bootloader`，保存在MBR中，讓使用者選擇啟動的操作系統，並轉交的第二階段的引導程式處理
    - `第二階段的bootloader(真正的)`，第二階段的引導程式與操作系統相對應，保存在某個磁碟分割區上
      - 用於在初始化(init.d)啟動進程之前，`加載內核`和`加載initial-ramdisk`
      - 第二階段引導程式的選擇，例如，NTLDR、BOOTMGR、GNU-GRUB、LILO(Linux-Loader)、Boot-Manager(Windows)

- [bootloader 是由作業系統安裝建立的](#建立-bootloader)

- bootloader 會進行下的操作
  - step，提供多作業系統`選單`供使用者選擇
  
  - step，bootloader將`kernel`加載到`內存的特定位置`，參考，[kernal](03_kernel.md)
    - 由系統架構的規範和硬件，決定內核實際被加載到特定的內存位置
    - x86/32bit，內核會被加載到內存的 0x100000（1 MB）以上的地址
    - x86/64bit，內核會被加載到內存的 0x1000000（16 MB）以上的地址

  - step，bootloader將`虛擬檔案系統`(initrd或initramfs)，加載到`內存的任意位置`

    參考，[initrd 與 initramfs的差異](./04_initramfs.md#initrd-與-initramfs-的區別)

  - step，將initramfs壓縮檔在內存的位置傳遞給 kernel，並啟動kernel，將控制權交給 kernel

  - step，kernel自動檢測initramfs壓縮檔的壓縮類型，並將initramfs解壓縮

  - step，解壓縮後的initramfs檔案系統，會被掛載為臨時根文件系統 (temorary-root-filesystem)

  - step，內核查找並執行 initramfs/init 腳本，參考，[initramfs](./04_initramfs.md)
    - 負責初始化系統環境，例如，建立必要的目錄，驅動網路、掛載重要的虛擬檔案系統
    - 加載必要的`驅動程序`和`內核模塊`
    - 測試和驅動的外圍裝置
    - 掛載並切換到真正的磁碟根目錄 (root-filesystem)
      - 例如，archlinux 的 chroot 就是將虛擬檔案系統切換到實際的檔案系統的命令
      - 例如，alpine 的 switch_root 就是將虛擬檔案系統切換到實際的檔案系統的命令
    - 執行[/sbin/init程序](05_init-program.md)，用於啟動和管理用戶的服務，進行用戶環境的初始化

- bootloader 的選擇
  - linux 的 grub 或 grub2
  - linux 的 LILO
  - windows 的 ntldr (NT-loader): 用於 windows-vista、windows-server-2008
  - windows 的 bootmgr
  - 第三方的 spfdisk: disk-format + boot-manager

## 建立 bootloader

- 安裝作業系統時建立 bootloader
  - <font color=blue> OS 一定會建立 boot-section 的 bootloader，MRB 中的 bootloader 則是選擇安裝 </font>

  - step1，作業系統安裝程式將 bootloader `寫入boot-section`

    對於多作業系統，每個安裝系統在安裝時，一定會在`當前的磁碟分區`中建立自己的 `boot-section`，
    boot-section 的內容就是bootloader，

  - step2，作業系統安裝程式將 bootloader `寫入MBR`
    
    不是每個作業系統都會直接將 bootloader 直接寫入MBR中 (不是每一個作業系統的 bootlaoder 都提供多作業系統開機選單)

    在多作業系統中，每個作業系統都需要自己的 bootloader 來載入kernel，但 MBR 只能保存在一個 bootloader，因此，
    - 支援多作業系統開機選單，例如，linux，可以選擇是否將 bootloader 寫入MBR，透過選單選擇不同的 bootloader 進行開機
    - 不支援多作業系統開機選單，例如，windows，預設會直接將bootloader 寫入MBR
    - 多作業系統中，若先安裝linux後再安裝windows，就會覆蓋MBR的booloader，而失去開機選單的功能

- 圖解 bootloader 和 bootsection 的關係

  <img src="doc/bootup-flow/mbr_boot_sector.png" width=80% height=auto>

  - 選單1: MBR -> kernel -> booting
  - 選單2: MBR -> boot-section(windows-loader)  --> windows-kernel --> booting
  - 選單3: MBR -> boot-section(linux-loader)  --> linux-kernel --> booting

## 手動修改 GRUB bootloader

- 修改 bootloader，以 GRUB 為例
  - bootloader 的執行內容，會放在 `/boot/grub/grub.cfg` 的檔案中，
    - 在 `grub.cfg` 的檔案中，
      - `linux   /vmlinuz-... root=/dev/mapper/ubuntu--vg-ubuntu--lv ...` 會記錄kernel檔案掛載的路徑，
        `root=` 指的是『 linux 檔案系統中，根目錄是在哪個裝置上
      - `initrd  /initrd.img-5.15.0-52-generic` 會記錄 initramfs 所在的檔名

    - 一般不建議直接修改`grub.cfg`，修改錯誤會導致開機的錯誤
      - 透過檔案1，`/etc/default/grub`，配置 grub.cfg 的行為 (設置 grub.cfg 中的可變變數)
      - 透過檔案2，`/etc/grub.d` 目錄，用來產生 grub.cfg 內容的腳本
      - 執行 `$ grub2-mkconfig` 後，會根據 `/etc/default/grub` 設置和 `/etc/grub.d目錄中的腳本內容` 產生 grub.cfg

  - 檔案1，`/etc/default/grub`，用於修改 `/boot/grub/grub.cfg` 的行為
    ```shell
    GRUB_TIMEOUT=0              # 設置開機選單等待時間,以秒為單位
    GRUB_DEFAULT=0              # 設置默認選擇的開機選項。0表示默認啟動的系統，使用第一個選單開機
    GRUB_TIMEOUT_STYLE=hidden   # 設置是否顯示開機選單，"hidden"表示不顯示開機選項選擇屏幕，直接啟動默認系統
    GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`  # 將系統發行版本的名稱存儲在變量GRUB_DISTRIBUTOR中
    GRUB_CMDLINE_LINUX_DEFAULT="" # 在啟動linux核心時附加的默認啟動參數
    GRUB_CMDLINE_LINUX=""         # 在啟動linux核心時附加啟動參數
    ```

    修改完畢後，需要執行 `$ grub2-mkconfig` 來建立新的 `/boot/grub/grub.cfg` 檔案

  - 檔案2，`/etc/grub.d`，產生 grub.cfg 內容的腳本
    ```shell
    # /etc/grub.d/*.*
    00_header         # 產生grub.cfg文件的頭部，包括配置選項和設置
    05_debian_theme   # 用於設置Debian主題
    10_linux          # 用於設置linux項目
    10_linux_zfs      # 用於設置支持ZFS文件系統的linux項目
    20_linux_xen      # 這個腳本用於設置Xen虛擬化平台上的linux項目
    30_os-prober      # 用於掃描系統中的其他操作系統並在grub.cfg中設置開機選項。
    30_uefi-firmware  # 用於管理UEFI固件相關選項，包括配置固件設置、修復模式和調整固件參數等。
    35_fwupd          # 用於管理固件更新相關的選項，例如檢查固件更新、下載和安裝固件更新等。
    40_custom         # 用於設置自定義開機選項
    41_custom         # 用於設置自定義開機選項
    ```

    修改完畢後，需要執行 `$ update-grub` 來建立新的 `/boot/grub/grub.cfg` 檔案

## 如何編寫 bootloader

- how to write a bootloader
  - [y2b](https://www.youtube.com/playlist?list=PL2-MHfTy2uA2f46IN1-G8YToRzUMSUi52)
  - [code](https://github.com/screeck/YouTube/tree/main/How_to_write_a_bootloader)