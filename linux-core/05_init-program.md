## init-program

- Bootloader 在完成 `硬體偵測 > 載入 kernel > 完成驅動程式`，主機的硬體就已經處於就緒狀態
  
- 處於就緒狀態的 kernel ，會主動呼叫的第一支程式(PID-1，稱為 init程式) 以執行系統的初始化
  - 在`新`版本的 linux 中 init程式為 `systemd`，在`舊`版本的 linux 中 init程式為 `systemV`

  - init程式最主要的功能就是`準備軟體執行的環境`，包含
    - 系統的主機名稱
    - 網路設定
    - 語系處理
    - 檔案系統格式
    - 其他服務的啟動

  - 啟動 systemd 後，會執行預設的 service 來初始化系統，包含以下
    - [圖解systemd啟動過程](https://systemd-book.junmajinlong.com/systemd_bootup.html)

      <img src="doc/bootup-flow/systemd-flow.png" width=60% height=auto>
  
    - step1，`local-fs.target` + `swap.target`： 主要在掛載本機 /etc/fstab 裡面所規範的檔案系統與相關的記憶體置換空間
  
    - step2，`sysinit.target`： 初始化系統，主要在偵測硬體，載入所需要的核心模組等動作
  
    - step3，`basic.target`： 準備系統，載入主要的週邊硬體驅動程式與防火牆相關任務
  
    - step4，`default.target`: 執行 multi-user.target 或 graphical.target，一般系統或網路服務的載入
  
      - default.target只是個軟連結，指向
        - `multi-user.target`: 一般系統或網路服務的載入，並提供 tty 介面的登入服務
        - `graphical.target`: 一般系統或網路服務的載入，並提供 GUI 介面的登入服務
  
      - default.target會執行 `/etc/rc.d/rc.local` 檔案，執行使用者自定義的服務
      - default.target會執行 `getty.target` 及登入服務
  
    - step5，`gdm.service`: 圖形界面相關服務如  等其他服務的載入

    - 參考，[service 的內容和使用](../usage/service/service.md)

- 在虛擬檔案系統的 systemd 和 進入作業系統後的 systemd 執行的內容不同

  <img src="doc/bootup-flow/systemd-in-initramfs-and-os.png" width=60% height=auto>
