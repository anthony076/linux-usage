## LXC Linux Container

- 使用場景

  linux 限制使用者存取的手段有以下幾種
  - sudo，控制使用者可以執行的`命令`
  - chmod，控制使用者可存取的`檔案`
  - chroot，控制使用者可存取的`目錄`
  - cgroups，控制使用者可存取的`硬體資源`

  lxc 是利用 namepace 實現的虛擬環境隔離，透過 namepace 將系統可用資源(文件系統、網路、進程、... 等)，
  隔離到不同的命名空間，使每個虛擬環境看起來像一個完全獨立的系統

  實務上 cgroups 技術控制和限制虛擬環境的資源使用

## Ref 
- [Cgroup与LXC简介](https://blog.51cto.com/speakingbaicai/1359825)