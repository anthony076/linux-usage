## cgroup 的使用
- 用於限制進程可使用的資源

- 範例
  - 查看進程的 cgroup，`$ cat /proc/進程的PID/cgroup`
 
## Ref
- [Linux中控制群組(cgroup)的一些筆記](https://bbs.huaweicloud.com/blogs/304619)
- [使用 systemd 管理 cgroup](https://www.redhat.com/sysadmin/cgroups-part-four)
- [使用 systemd 修改资源隔离配置](https://cloud.tencent.com/developer/article/1666031)