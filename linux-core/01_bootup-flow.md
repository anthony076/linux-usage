## Linux 的啟動流程

- 依照`開機韌體`的不同，開機程序會有差異，區分為
  - 選擇1，`BIOS(Basic-Input-Output-System)`，基本輸入輸出系統
  - 選擇2，`UEFI(Unified-Extensible-Firmware-Interface)`，統一可擴展韌體介面
    - 支援更大的容量的硬碟、
    - 支援 secure-boot

- 比較，bios 和 uefi 的差異
  
  | 比較項目               | 傳統 BIOS (legacy-bios) | UEFI                                    |
  | ---------------------- | ----------------------- | --------------------------------------- |
  | 使用程式語言           | 組合語言                | C 語言                                  |
  | 硬體資源控制           | 使用中斷 (IRQ) 管理     | 使用輪詢                                |
  |                        | 不可變的記憶體存取      |                                         |
  |                        | 不可變得輸入/輸出存取   |                                         |
  | 處理器運作環境         | 16 位元                 | CPU 保護模式                            |
  | 擴充方式               | 透過 IRQ 連結           | 直接載入驅動程式                        |
  | 第三方廠商支援         | 較差                    | 較佳且可支援多平台                      |
  | 圖形化能力             | 較差                    | 較佳                                    |
  | 內建簡化作業系統前環境 | 不支援                  | 支援                                    |
  | 開機速度               | 慢                      | 快                                      |
  | 讀取引導程式           | 從開機磁區的MBR位置讀取 | EFI磁區，大小至少100MB，且必須使用FAT32 |
  | MBR                    | 只能從MRB位置讀取       | 保留MBR，對不支援UEFI的系統可從MBR開機  |

- UEFI不會主動MBR讀取引導程式，雖然UEFI保留了MBR的區域(Protective-MBR)，

  取得代之，UEFI 會從專門的EFI磁區中讀取引導程式，
  
  因此，使用UEFI的開機韌體時，除了根磁區、swap磁區之外，
  還需要額外的EFI磁區(或稱為EFI系統分割區，EFI-System-Partition，ESP)，用於存放
  - UEFI的引導程序
  - 引導管理器
  - UEFI-Shell

- UEFI 若設置了`兼容支持模組`(Compatibility Support Module，CSM)，才會從 `Protective-MBR` 讀取引導程式
  > 注意，Intel 逐漸取消對 CSM 的支持

## BIOS 的完整啟動流程

- 基本流程，依照使用到的組件分類，`bios -> bootlader -> kernel -> initramfs -> initramfs/init -> init 應用`

- step，cpu 載入 ROM 中保存的 bios 程序

- step，bios 執行執行加電自檢(Power-On-Self-Test，POST)程序，包含 硬體初始化 + 硬體測試
  - 功能1，顯示開機畫面
  - 功能2，開機時的硬體初始化，並設置為運行狀態
    - CPU 初始化: CPU 會被啟動，並且會讀取並執行引導韌體。
    - 記憶體初始化: 記憶體會被清空，並且會被分配給引導韌體和操作係統。
    - 硬盤驅動器初始化: 硬盤驅動器會被掃描，並且會讀取引導韌體。
    - 網絡接口卡初始化: 網絡接口卡會被啟動，並且會被設置為計算機的網絡裝置。
    - 輸入輸出設備初始化: 輸入輸出設備會被啟動，包括鍵盤、鼠標、螢幕等。
  - 功能3，跳轉到`開機韌體`的位置

- step，bios將其自身的代碼從ROM複製到RAM，以提高後續的啟動過程

- step，bios尋找並加載bootloader的位置
  - 依照啟動順序設置的順序，依序查找啟動設備
  - 在啟動設備中，第一個 512 bytes 的空間稱為 master-boot-record (MBR)
  - bios會在當前啟動設備中，尋找MBR的空間內是否出現 0x55AA，
    - 第 511 byte 是否是 0x55
    - 第 512 byte 是否是 0xAA
    - 若是，代表第一個 512 bytes 空間的內容是 bootloader，
      bios 會將整個MBR載入記憶體中，並執行bootloader的機械碼

  - bios 透過 INT-13 讀取 MBR

    bios 是透過硬體的 INT-13 中斷功能來讀取 MBR 的內容，
    只要 bios 能夠偵測的到磁碟(不論該磁碟何種介面)，
    
    bios就能透過 INT-13 讀取第一個開機裝置的第一個磁區內的MBR內容，進而執行 bootloader

- step，執行系統引導程式(Bootloader)
  
  參考，[bootloader](02_bootloader.md)

- step，加載並啟動kernel、解壓縮 initramfs或initrd，並執行 initramfs/init，
  以初始化root-filesystem

- step，啟動 init 管理應用程式，以啟動server、並提供使用者登入

## UEFI 的完整啟動流程

- UEFI 的簡要啟動流程
  - 開機程序@ ROM: 硬體初始化 + 跳轉到 UEFI韌體處執行
  
  - UEFI `讀取` NVRAM 中的引導項，從引導項獲得`EFI應用程式名`和`EFI應用程式的位置`後，從EFI磁區`尋找`該EFI應用程式
    
    注意，EFI應用程式不一定總是引導程式，有可能是下列其中一種
    - 選擇1，引導程式
    - 選擇2，其他EFI應用程式(非引導程式)，例如，UEFI-shell、boot-manager(例如，systemd-boot 或 rEFInd)
    - 選擇3，(直接啟動內核) 透過 EFISTUB 啟動的 archlinux-kernel，EFISTUB 可將 archlinux-kernel 當成是EFI應用程式執行，
      詳見 [EFISTUB](https://wiki.archlinux.org/title/EFISTUB)
  
  - UEFI`執行`EFI應用程式(副檔名為.efi)，
    - 獲取啟動內核的參數後
    - 加載內核映像到內存
    - 其餘步驟與 BIOS 同，請參考，[BIOS 完整啟動流程 STEP5](#bios-的完整啟動流程)
    - 注意，若 UEFI 啟用了 secure-boot，在執行EFI應用程式時，會通過簽名，驗證EFI二進位文件的真實性

- BIOS 和 UEFI 在 bootloader、kernel、initramfs 上的差異
  - BIOS

    BIOS 將 bootloader、kernel、initramfs 區分為三個檔案，
    mkinitcpio命令可以用來建立 kernel、initramfs 兩個檔案，
    bootloader可以自由選用任意的第三方 bootloader 軟件進行手動安裝

  - UEFI
    
    UEFI 將bootloader、kernel、initramfs合併成一個可執行的EFI檔案，EFI檔案可以直接透過 mkinitcpio 產生後，
    可搭配 boot-manager類套件，例如，efibootmgr，指定開機要執行EFI開機項 (指定開機要執行的 EFI 執行檔)

## 圖解啟動流程

- [systemV時期的開機流程](https://blog.csdn.net/qq_18312025/article/details/78219986)

  <img src="doc/bootup-flow/boot-flow-2-systemV.png" width=60% height=auto>

- [開機流程圖預覽](https://systemd-book.junmajinlong.com/systemd_bootup.html)

  <img src="doc/bootup-flow/boot-flow-1.png" width=70% height=auto>

## Ref 

- [Linux 啓動流程](https://www.readfog.com/a/1644552701210628096)

- [Arch 啓動流程](https://wiki.archlinuxcn.org/wiki/Arch_%E7%9A%84%E5%90%AF%E5%8A%A8%E6%B5%81%E7%A8%8B#%E5%BC%95%E5%AF%BC%E5%8A%A0%E8%BD%BD%E7%A8%8B%E5%BA%8F)

- [systemV時代的開機啓動流程](https://blog.csdn.net/qq_18312025/article/details/78219986)

- [systemd時代的開機啓動流程(UEFI+systemd)](https://systemd-book.junmajinlong.com/systemd_bootup.html)

- [主機規劃與磁碟分割](https://linux.vbird.org/linux_basic/centos7/0130designlinux.php)

- [UEFI的啓動選單與NVRAM](https://blog.csdn.net/chenqioulin/article/details/119192503)

- [Booloader: grub2 的使用](https://linux.vbird.org/linux_basic/centos7/0510osloader.php#grub)

- GPT
  - [什麼是 GPT 磁碟](https://tw.easeus.com/diskmanager/what-is-gpt.html)
  - [GUID磁碟分割表](https://zh.wikipedia.org/wiki/GUID%E7%A3%81%E7%A2%9F%E5%88%86%E5%89%B2%E8%A1%A8)
  - [The MBR](https://gyires.inf.unideb.hu/GyBITT/20/ch02.html)
