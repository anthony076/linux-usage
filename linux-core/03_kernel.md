## kernel 內核

- 什麼是內核
  - 內核是作業系統的核心。它運行於一個叫「內核空間」的底層上，負責機器硬體和應用程式之間的交流。
  - 為了盡可能充分地壓榨 CPU 性能，內核使用調度器，通過一定的優先級算法將 CPU 按照時間動態的分配給各個程序

- 內核依照功能可以區分為以下的子系統(subsystem)
  - 內核模塊(Kernel Modules)：
    - 允許動態加載和卸載內核模塊，以支持設備驅動程序和文件系統等，用於擴展或卸載特定的內核功能
    - 注意，`設備驅動程序只是內核模塊的一小部分`，內核模塊還可以是任何功能的代碼，
      例如，文件系統模塊、網絡協議模塊、內核安全增強模塊(如 SELinux)、虛擬化模塊
  - 進程調度器(Scheduler)：管理和調度系統中的進程和線程。調度器決定哪個進程在任何給定時間運行。
  - 內存管理(Memory Management)：管理系統的內存，包括內存分配、頁表管理、虛擬內存和物理內存的映射。
  - 進程間通信(Inter-Process Communication, IPC)：支持不同進程間的通信機制，如信號、管道、消息隊列、共享內存和信號量。
  - 文件系統(File Systems)：提供文件系統接口，管理文件和目錄，包括不同文件系統(如 ext4、XFS、Btrfs)的支持。
  - 網絡(Networking)：負責處理所有網絡相關的功能，如網絡協議棧(TCP/IP)、網絡設備驅動程序和防火牆(Netfilter)。
  - 安全性(Security)：包括訪問控制、安全策略、認證機制(如 SELinux、AppArmor)和加密功能。
  - 電源管理(Power Management)：處理系統的電源管理和節能功能，包括休眠、待機、和動態電壓頻率調節。
  - 設備驅動(Device Drivers)：用於與硬件設備(如硬盤、顯卡、聲卡等)進行交互。每種類型的設備通常都有一個對應的驅動程序。
  - 虛擬化(Virtualization)：支持虛擬化功能，例如 KVM(Kernel-based Virtual Machine)和各種容器技術(如 Docker)。
  - 塊設備層(Block Layer)：管理塊設備的輸入輸出操作，提供通用的塊設備接口。
  - 輸入/輸出(I/O)子系統：管理不同類型的 I/O 操作，如字符設備、塊設備和網絡設備的 I/O。
  - 跟蹤和性能監視(Tracing and Performance Monitoring)：提供跟蹤、調試和性能分析工具，如 ftrace、perf 和 eBPF。
  
- 內核模塊化
  - 內核模塊是`可以在運行時動態加載或卸載的代碼片段`，擴展了 Linux 內核的功能，
    允許在`不重新編譯或重啟內核`的情況下增加或卸載內核的功能
  - 內核子系統是可以透過模塊的方式加載的，但`並非所有的子系統都是以模塊的形式加載的`，
    依照必要性可以區分為以下幾個部分: 必要的子系統、選用的子系統、混用的子系統

    - `必要的子系統`: 是內核的核心部分，通常直接編譯到內核映像中，不能被卸載或動態加載，而是在系統啟動時就加載並初始化，例如，
      - 進程調度器
      - 內存管理
      - 進程間通信

    - `選用的子系統`: 可以在運行時動態加載|卸載的子系統，例如，
      - 文件系統
      - 網絡系統
      - 設備驅動程序
      - 虛擬化
    
    - `混用的子系統:` 一些功能被編譯進內核，而其他功能作為模塊提供，例如，
      - 安全性：基本的安全機制通常是內核的一部分，但附加的安全模塊(如 SELinux、AppArmor)可以作為模塊加載。
      - 電源管理：基礎的電源管理功能通常是內核的一部分，但特定硬件的電源管理驅動程序可以作為模塊加載。

  - 範例，[開機期間啟用預設的核心模組](01_bootup-flow.md#系統引導程式bootloader-內核和驅動核心模塊的啟動)
  - 範例，[alpine上依照功能區分的內核模塊](https://github.com/alpinelinux/mkinitfs/tree/master/features.d)

- 內核檢測硬體並自動載入驅動程式模組的過程
  
  - step，當系統啟動時，Linux Kernel 會檢測系統中的所有硬體設備。這個檢測過程包括讀取 PCI、USB、SATA 等總線上的設備，並生成一個硬體數據庫。
  
  - step，udev子系統： udev 是 Linux 系統的設備管理器，負責在`硬體添加`或`硬體移除`時動態地管理設備節點，
    當內核檢測到新的硬體設備時，會生成一個 udev-event，並將這個事件傳遞給 udev 進行處理。
  
  - step，udev 自動載入模組
    
    udev 使用硬體的屬性（如廠商 ID 和設備 ID）來查找合適的驅動程式模組。這些屬性通常存儲在`/lib/modules/$(uname -r)/modules.alias`文件中，
    包含了模組別名和對應的驅動程式模組之間的映射。

    一旦找到合適的模組，udev 就會使用 modprobe 或 insmod 命令來載入驅動程式模組。
    
    modprobe 會解析模組的依賴性，並確保所有需要的模組都已經載入。

  - step，套用模組參數

    某些驅動程式模組可能需要特定的參數才能正常工作。這些參數可以在 /etc/modprobe.d/ 目錄下的配置文件中指定。
    當模組載入時，modprobe 會自動應用這些參數。

- kernel 檔案的位置
  - kernel 檔案會掛載在 /boot 的路徑上，並且取名為 vmlinuz，即 `/boot/vmlinuz`

  - 查看內核
  
    ```shell
    $ ls --format=single-column -F /boot

    config-3.10.0-229.el7.x86_64                # 此版本核心被編譯時選擇的功能與模組設定檔
    grub/                                       # 舊版 grub1 ，不需要理會這目錄了！
    grub2/                                      # 就是開機管理程式 grub2 相關資料目錄
    initramfs-0-rescue-309eb890d3d95ec7a.img    # initramfs-* 虛擬檔案系統檔，rescue 用於救援
    initramfs-3.10.0-229.el7.x86_64.img         # 正常開機下使用到的虛擬檔案系統
    initramfs-3.10.0-229.el7.x86_64kdump.img    # 核心出問題時會用到的虛擬檔案系統
    System.map-3.10.0-229.el7.x86_64            # 核心功能放置到記憶體位址的對應表
    vmlinuz-0-rescue-309eb890d09543d95ec7a*     # 救援用的核心檔案
    vmlinuz-3.10.0-229.el7.x86_64*              # 就是核心檔案
    ```

- kernel 透過`虛擬檔案系統`才能載入必要的驅動程式

  - 載入驅動程式的問題
    
    ```mermaid
    flowchart LR
    驅動 --> 驅動程式 --> 硬碟 --> 硬碟未驅動 --> 無法讀取硬碟的驅動程式 --> 無法驅動
    ```

    將內核檔案的內容讀入記憶體後，接著會載入驅動程式，
    一般`驅動程式`會放在某個硬碟中，並需要掛載在 `/lib/modules` 的路徑
    
    驅動程式一般會放在硬碟中，而硬碟也是外圍硬體之一，硬碟未完成驅動前無法讀取，
    也就無法將驅動掛載到 /lib/modules 的路徑

    因此會因為驅動程式`放在尚未驅動的硬碟`中造成無法驅動的問題

  - 解決方式
    - step1，linux 會將核心驅動程式(必要的驅動程式)放在內核檔案中，而不是放在其他的磁碟分區中，核心驅動包含
    - step2，在記憶體中建立`虛擬的檔案系統(Initial-RAM-Disk 或 Initial-RAM-Filesystem)`，
    - step3，將內核的核心驅動程式掛載到虛擬檔案系統中
    - step4，內核執行位於虛擬檔案系統中的驅動程式

## 編譯系統內核(compile-linux-kernel)

- 什麼時候需要重新編譯內核
  - 新硬體所需要的新驅動程式
  - 在原本的核心底下加入某些特殊功能， 而該功能已經針對核心原始碼推出 patch 補丁檔案
  - 預設核心太過臃腫，重新編譯核心可以簡化內核的占用
  - 針對硬體作最佳化
  - 注意，核心的主要工作是在控制硬體，在編譯核心之前，應先瞭解一下硬體配備和主機的未來功能

- 範例1，使用 `linux-source` 進行編譯

  - 1-1，切換目錄，`$ cd /usr/src`
    
  - 1-2，變更下載目錄的權限，`$ sudo chmod -R 777 /usr/src/`
    - 用途: 避免出現 couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied) 的錯誤
    - 原因: apt使用沙盒式進行操作，並透過用戶 _apt 來下載軟體包，
    - 在 ubuntu 由於權限的限制，即使使用 sudo 或 root 也無法完成 _apt 用戶需要的操作，
      必須變更目錄的擁有者，確保用戶 _apt 可存取源碼下載的目錄
    
  - 1-3，添加源碼的軟體源，`$ sudo nano /etc/apt/sources.list`
    - 將 /etc/apt/source.list 中，將所有 `deb-src ...` 開頭的軟體源取消註解後，執行 `$ sudo apt update`

  - 1-4，安裝必要依賴，`$ sudo apt install build-essential dpkg-dev`

  - 1-5，透過 apt 下載內核源碼
    - 注意，下載內核源碼的目錄，不可以是虛擬機的共享目錄
      - 若下載內核源碼的目錄是虛擬機的共享目錄，會造成 Cannot create symlink to `xxx': Operation not supported 的錯誤
      - 共享目錄與 windows 共用，因為 windows不支援軟連結，因此建立軟連結時會失敗
    
    - 下載內核的源碼，但不包含編譯好的文件，需要自己編譯
      - 方法1，`$ sudo apt source linux-source-$(uname -r)`
        - 若使用虛擬機，$(uname -r) 得到 5.15.0-52-generic 的結果
        - linux-source-5.15.0-52-generic 是無效的版本號，不接受指定 generic 的版本
        - 此方法只接受類似 linux-source-5.15.0 的版本號

      - 方法2，`$ sudo apt source linux-source-5.15.0`，適合 $(uname -r) == 5.15.0-52-generic

  - 1-6，安裝編譯內核的依賴，並變更下載目錄的權限
    - 安裝依賴，`$ sudo apt-get build-dep linux-source-5.15.0`
    - 變更權限，`$ sudo chmod -R 777 linux-source-5.15.0`

  - 1-7，進入含有 Makefile 的源碼目錄中，`$ cd linux-headers-5.15.0-57`

  - 1-8，將當前系統的配置文件，複製到源碼目錄中，`$ sudo cp /boot/config-$(uname -r) .config`
    
  - 1-9，安裝編譯依賴，`$ sudo apt install -y flex bison pkg-config qttools5-dev libelf-dev libncurses5-dev bc libssl-dev fakeroot`
   
  - 1-10，運行配置工具，`$ sudo make oldconfig && sudo make prepare && sudo make scripts`
    - make oldconfig: 使用舊版本的配置檔案來配置要編譯的模組，若有 DISPLAY 可以改用圖形化方式產生配置檔
      - make menuconfig: 基於文本的菜單來配置要編譯的模組
      - make defconfig: 使用預設配置檔案來配置要編譯的模組
      - make xconfig: 使用QT圖形介面來配置要編譯的模組
      - make gconfig: 使用GTK+圖形介面來配置要編譯的模組

  - 1-11，編譯內核，`$ sudo make -j $(nproc)`

- 範例2，使用 `linux-image` 進行編譯

  - 將範例1的 1-5 替換為，`$ sudo apt-get source linux-image-$(uname -r)`

  - 將範例1的 1-5 替換為以下
    - 安裝依賴，`$ sudo apt-get build-dep linux-image-$(uname -r)`
    - 變更權限，`$ sudo chmod -R 777 linux-source-5.15.0`

## 編寫內核模塊(compile-self-defined-kernel-module)

詳見，[driver的撰寫](./driver/21_driver.md)

## 加載內核模塊的控制與管理

- 有多種不同的階段可以控制內核模塊的加載

  - `stage-1`: 編譯內核或模塊時（源代碼階段），在編譯內核時選擇需要內建或模塊化的驅動程序

    - 適合高度定制化的內核配置或在性能優化的情況下使用，例如，嵌入式系統或特定用途的 Linux 內核（如路由器、物聯網設備）

    - 例如，在編譯內核時，透過`$ make menuconfig`或`$ make xconfig` 等命令，
      將模塊配置為內建或者動態加載的模塊

  - `stage-2`: 安裝系統時（Bootloader 階段），通過 Bootloader（例如 GRUB）調整內核參數
    - 在系統啟動之前調整內核的行為，比如禁用或強制加載某些模塊，
      解決驅動程序衝突，或在系統硬體不兼容時使用。
    
    - 例如，以 GRUB為例，可以透過 /etc/default/grub 文件進行調整
      - 在 GRUB_CMDLINE_LINUX 中添加內核參數，如 modprobe.blacklist=mod1,mod2 來禁用模塊
      - 例如，使用 initcall_blacklist=module_name_init 禁用特定模塊的初始化函數
      - 變更配置後，執行`$ update-grub`或`$ grub-mkconfig`更新GRUB配置，使變更生效
  
  - `stage-3`: 使用自定義的 initramfs（初始化內存檔系統）
    - 適合在`掛載文件系統之前`加載特定模塊（如 RAID、LVM 或加密模塊）
    - 例如，使用 `dracut`、`mkinitcpio` 或其他工具來生成自定義的 initramfs，
      並在配置文件中指定需要加載的模塊

  - `stage-4`: 系統啟動過程中（init 系統階段）
    - 適合用於在系統啟動過程中，`控制模塊的加載順序`，或在多用戶目標（multi-user.target）或圖形目標（graphical.target）啟動之前，加載必需的模塊
    - 例如，以 Systemd 為例，透過systemd-modules-load.service的配置文件(/etc/modules-load.d/ 或 /usr/lib/modules-load.d/)，指定要加載的模塊名稱
    - 例如，以 SysVinit 為例，透過/etc/modules或/etc/rc.d/rc.local，添加需要加載的模塊

  - `stage-5`: 系統啟動後（運行時）
    - 適合在系統運行時臨時或動態加載或卸載模塊，用於測試、開發或調試為目的
    - 例如，使用命令行工具（如 modprobe, insmod, rmmod, lsmod）來加載或卸載模塊
      - 加載模塊及其依賴，`$ modprobe <module_name>`
      - 手動加載指定模塊文件，`$ insmod /path/to/module.ko`
      - 卸載模塊，`$ rmmod <module_name>`
      - 列出當前已加載的模塊，`$ lsmod`
      - 透過modprobe禁用模塊
        - 在 /etc/modprobe.d/ 目錄下創建或編輯文件，添加 `blacklist <module_name>`來禁用模塊
        - 例如，在 /etc/modprobe.d/blacklist.conf 中添加 blacklist nouveau 可以禁用 Nouveau 驅動

  - 比較
    - 內核編譯方法，適合需要`高度定制化`和`性能優化`的場景
    - Bootloader 和 initramfs 方法，適合在`系統啟動早期`進行模塊加載控制
    - init 系統方法，適合在`系統啟動過程中`進行更精細的控制
    - 運行時工具方法，適合在`系統運行時`進行臨時或動態調整

## [stage2] 在安裝系統時，透過 GRUB 手動調整內核啟動選項

- 實際範例，見 [packer-vbox-arch-optionB](https://gitlab.com/anthony076/docker-projects/-/blob/main/packer/example/packer-vbox-arch-optionB/readme.md)

- 內核啟動選項
  - 以下內容見，`/boot/loader/entries/arch.conf`

  - root=LABEL=ROOT 指定根分區
  - resume=LABEL=SWAP 指定交換分區
  - rootflags=rw,relatime 設定根分區為讀寫並設定訪問時間
  - add_efi_memmap 啓用 EFI 記憶體映射
  - random.trust_cpu=on 使內核信任 CPU 內置的隨機數生成器
  - rng_core.default_quality=1000 設定隨機數生成器的預設質量
  - nomodeset 禁用內核模式設定
  - nowatchdog 禁用看門狗定時器
  - mitigations=off 禁用安全增強措施
  - quiet 抑製大多數日誌消息
  - loglevel=3 設定日誌級別為3
  - rd.systemd.show_status=auto 在啓動過程中顯示systemd狀態
  - rd.udev.log_priority=3 設定 udev 的日誌優先級為3

## [stage3] 手動建立 initramfs/initrd 虛擬檔案系統

- update-initramfs命令: 重建 `/boot/initrd.img`
  - update-initramfs 會讀取系統上當前安裝的內核信息，按照需要重建 initramfs 文件。
  - 注意，update-initramfs 用於 ubuntu 
  - 注意，`/boot/initrd.img` 是軟連結，指向 `/boot/initrd.img-x.y.z-n-generic` 的實際檔案
  - 注意，此命令也可以用於產生 initrd 的檔案
  - 注意，mkinitramfs命令也可以用於產生 initrd 的檔案，已棄用
  
  - 範例，`$ sudo update-initramfs -u`，更新已經存在的 initramfs
  - 範例，`$ sudo update-initramfs -c -k 2.6.18-1-686`，-c 建立新的 initramfs，-k 指定版本號，
    會建立 `/boot/initrd.img-0.0.1` 的檔案
  - 範例，`$ sudo update-initramfs -c -t`，-c 建立新的 initramfs，-t 產生一個帶有預設配置的配置文件

- 範例，將指定模組添加到 initramfs 中
  - step1，`$ sudo update-initramfs -u -t`，重新產生帶有預設配置的配置文件的 initramfs 文件
  - step2，`$ sudo nano /etc/initramfs-tools/modules`，將模組名添加到 modules 的檔案中
  - step3，`$ sudo update-initramfs -u`，重新產生 initramfs 文件

## [stage4] 透過 systemd 自動啟動額外的核心模塊

- 在 Linux 系統中，內核模塊是`可以動態加載到內核`中的程式，透過 systemd 使用者可以指定自動加載的額外模塊

- 方法，透過設定檔配置內核模塊
  - `/etc/modules-load.d/*.conf`: 設置 Linux 啓動時需要`自動加載的內核模塊的列表`
  - `/etc/modprobe.d/*.conf`: 用於指定內核模塊的`加載參數和規則`
  - 範例
    ```shell
    # vim /etc/modules-load.d/vbird.conf
    nf_conntrack_ftp    # step1，系統啟動時，加載 nf_conntrack_ftp 模塊

    # vim /etc/modprobe.d/vbird.conf
    options nf_conntrack_ftp ports=555    # step2，設置nf_conntrack_ftp 模塊的參數

    # 重新載入modules-load的服務(systemd-modules-load.service)
    systemctl restart systemd-modules-load.service

    # 檢視模塊是否啟動
    lsmod | grep nf_conntrack_ftp
    ```

## [stage5] 管理內核模組的相關命令

- 相關檔案的位置
  - `檔案1`: Linux核心程序和相關驅動程序的存放位置，`/lib/modules/$(uname -r)/kernel`，常用子目錄
    - arch目錄: 與硬體平台有關的項目，例如 CPU 的等級
    - block目錄: 塊設備的驅動程式,如硬盤驅動，SSD驅動
    - crypto目錄: 核心所支援的加密的技術，例如 md5 或者是 des
    - drivers目錄: 所有硬件驅動程序
    - fs目錄: 核心所支援的 filesystems ，例如 vfat, reiserfs, nfs
    - lib目錄: 一些函式庫
    - mm目錄: 有關內存管理的驅動程序
    - sample目錄: 包含樣本驅動程序程序，用於學習和開發
    - ubuntu目錄: 特定於ubuntu的驅動程序和修改
    - net目錄: 與網路有關的各項協定資料，還有防火牆模組 (net/ipv4/netfilter/*)
    - sound目錄: 與音效有關的各項模組
    - kernal目錄: 包含核心的程序和數據文件
  
  - `檔案2`: 紀錄各個模組之間的相依性，`/lib/modules/$(uname -r)/modules.dep`

- 常用的內核模組管理工具
  - lsmod命令: 
    - 列出已載入的內核模組，`$ lsmod`
  
  - modinfo命令: 
    - 列出指定模組的訊息，`$ modinfo 模組名`，或 `$ modinfo 模組檔案路徑`
      - 例如，`$ modinfo hid`
      - 例如，`$ modinfo /lib/modules/5.15.0-52-generic/kernel/drivers/hid/hid.ko`
    
  - depmod命令: 建立 modules.dep 檔案，`$ depmod -A`
  
  - insmod命令: 載入(insert)指定模組，`$ insmod 模組檔案路徑`
    - 注意，insmod 只接受完整的模組檔案路徑，不接受模組名
    - 例如，`$ sudo insmod /lib/modules/5.15.0-52-generic/kernel/fs/fat/msdos.ko`
  
  - rmmod命令: 卸載(remove)指定模組，`$ rmmod -f 模組名`，或 `$ rmmod -f 模組檔案路徑`
    - -f 參數，強制移除模組，無論是否在執行中
    - 例如，`$ sudo rmmod msdos`
    - 例如，`$ sudo rmmod /lib/modules/5.15.0-52-generic/kernel/fs/fat/msdos.ko`

  - modprobe命令: 取代 insmod/rmmod/depmod 命令
    - modprobe命令會搜尋 modules.dep 的紀錄，可以克服模組的相依性問題，且不需要知道該模組的詳細路徑
    - 列出目前系統所有的模組，`$ modprobe -c`
    - 手動加載模組，`$ modprobe 模組名`
    - 卸載模組，`$ sudo modprobe -r 模組名`

      若該模組正在使用中
      ```shell
      $ lsmod | grep hid    # hid 模塊正被 usbhid 和 hid_generic 兩個模塊依賴，
      $ modprobe -r usbhid hid_generic # 移除被依賴的模塊
      $ modprobe -r hid # 移除被目標模塊
      ```

    - 強制加載模組，`$ sudo modprobe -f 模組名`，
    - 檢視模組相依姓，`$ modprobe --show-depends 模組名`

- 範例，檢視目前已載入的驅動程式模組
  - 方法，`$ lsmod`
  - 方法，`$ dmesg | grep -i 'module'`，查看啟動期間載入的驅動程式模組的相關訊息
  - 方法，`$ dmesg | grep -i 'loaded'`，查看查看已載入的模組
  - 方法，`$ cat /proc/modules`，此文件包含了目前所有載入的模組資訊

- 範例，檢視系統上擁有的驅動程式模組，包含已載入和未載入的驅動程式模組
  - 方法，查看`/lib/modules目錄`的內容，例如， `$ find /lib/modules/6.6.36-0-virt/kernel/ -type f -name '*.ko*' | grep hid`
  - 方法，(ubuntu) `$ lsinitramfs /boot/initramfs-$(uname -r) | grep .ko`
  - 方法，使用標準的Linux工具來解壓縮和檢查initramfs映像的內容，initramf存放所有可用的內核模塊
    
    ```shell
    apk add file    # file，用來檢查 initramfs 的壓縮方法
    mkdir -p ~/tmp
    doas cp /boot/initramfs-virt ~/tmp   # 複製 initramfs，避免影響原有的檔案
    doas file ~/tmp/initramfs-virt       # 查看 initramfs 的壓縮方式
    cd ~/tmp
    doas zcat initramfs-virt | cpio -idmv
    ls lib/modules/6.6.36-0-virt/kernel/  # 檢視所有的內核模塊
    ```

- 範例，檢視特定內核模塊的詳細訊息
  - 方法，`$ modinfo hid` 或 `$ modinfo /lib/modules/6.6.36-0-virt/kernel/drivers/hid/hid.ko.gz`

- 範例，列出當前系統所有的硬體設備
  - 方法，列出所有被udev管理的硬體設備，`$ udevadm info --export-db`
  - 方法，列出所有系統硬體設備，無論是否能被udev管理，
    - lshw: 列出系統上的所有硬體設備
    - lspci: 列出所有 PCI (Peripheral Component Interconnect) 線上設備
    - lsusb: 列出所有 USB (Universal Serial Bus) 線上設備
    - lscpu: 提供關於 CPU 的架構信息，包括核心數、線程數、處理器型號
    - lsblk: 列出所有塊設備，包括硬碟、SSD、USB 存儲設備等
    - dmidecode: 用於解析系統的DMI表（桌面管理接口），顯示硬體信息，如 BIOS 版本、製造商、序列號、內存插槽等
    - inxi: 用於顯示系統硬體信息的多功能工具。它提供了簡潔易讀的硬體和系統信息摘要，`$ inxi -F`
    - hwinfo: 更全面的硬體信息工具，`$ hwinfo --all`

## 常見的內核模塊

- in alpine
  - `sd-mod` : 
    - 負責 SCSI 磁盤設備的支持，許系統識別和操作連接到 SCSI 接口的磁盤設備
    - 例如，包括USB 硬碟和固態硬碟

  - `usb-storage` : 
    - 為 USB 大容量存儲設備提供支持，使得系統夠通過 USB 接口讀寫這設備
    - 如 USB 閃存驅動器、外部硬碟和一些數碼相機

  - `ext4` : 支持 Ext4 文件系統的內核模塊，高效且穩定的文件系統，支持大文件和高性能操作
  
  - `loop` : 
    - 允許將普通文件作為塊設備來使用
    - 例如，將 ISO 文件掛載為虛擬光碟或創建加密的文件容器
  
  - `squashfs` : 
    - 支持 SquashFS 文件系統，一種只讀的壓縮文件系統
    - 用於嵌入式系統和 Live CD 等存儲空間有限的環境中

  - `simpledrm` : 
    - Linux 圖形子系統中的一個簡單直接渲染管理器
    - 用於基本的顯示控制，特別是在系統啟動時提供基本的圖形支持

  - `af_packet` : 
    - 供原始套接字接口，用於獲取和發送網絡數據包
    - 也可用於網絡分析和監控工具，如 tcpdump 和 Wireshark
  
  - `ipv6` : 
    - 提供對 IPv6 協議的支持
    - 允許操作系統使用和管理 IPv6 地址和網絡

- for vmware
  - ata_piix : 
    - 用於支持 Intel PIIX/ICH ATA 控制器
    - 負責與vmware虛擬機器中的 ATA 硬盤進行通信

  - mptspi
    - 支持 LSI Logic’s SPI (SCSI Parallel Interface) Host Adapters
    - 用於 vmware 中的虛擬 SCSI 設備，使VM能正確與 SCSI 兼容設備進行通信
  
  - sr-mod
    - 提供對 SCSI CD-ROM 設備支持
    - 使 vmware 的虛擬 CD/DVD 設備能正常運行

## Ref

- kernal 相關
  - [Linux kernel 官方](https://www.kernel.org/doc/html/v6.2-rc5/index.html)
  - [Linux 系統結構詳解](https://www.readfog.com/a/1640970900931186688)
  
  - kernelnewbies
    - [編譯核心](https://kernelnewbies.org/KernelBuild)
    - [FirstKernelPatch](https://kernelnewbies.org/FirstKernelPatch)

- 編譯核心
  - [Linux 核心編譯與管理@鳥哥](https://linux.vbird.org/linux_basic/centos7/0540kernel.php)
  - [ubuntu上的內核編譯+建立內核模塊(Linux 核心編譯與管理)@jserv](https://hackmd.io/@sysprog/linux-kernel-module)
  - [編譯Ubuntu 20.04內核代碼](https://blog.csdn.net/feihe0755/article/details/125424910)
  - [在虛擬機上編譯Linux 內核](https://medium.com/@vishhvak/compiling-a-custom-linux-kernel-on-a-virtual-machine-12be9d32189b)
  - [Ubuntu 下獲取和編譯內核源碼的方法](https://shaocheng.li/posts/2019/09/20/)

- 編寫內核模塊
  - [寫一個簡單的 Linux Kernel Module](https://chriswenyuan.blogspot.com/2017/05/linux-kernel-module_4.html)
  - [撰寫簡單的 hello-module](https://blog.wu-boy.com/2010/06/linux-kernel-driver-%E6%92%B0%E5%AF%AB%E7%B0%A1%E5%96%AE-hello-world-module-part-1/)
