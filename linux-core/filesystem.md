## filesystem 基礎

- 什麼是檔案系統

  檔案系統可以理解為，當硬碟建立分區並被系統識別後，還需要在該分區上建立一個負責管理和組織資料的系統
  這個系統提供一個結構化的方式，讓kernel或使用者能夠方便地`存取磁碟或內存中的檔案或各種資源`
  
  這種系統通常以`樹狀結構`來組織資料，並透過`目錄和檔案的層級關係`實現資源的有序存取，這種管理機制稱為檔案系統
  
  當掛載硬碟時，實際上是在檔案系統中創建了一個存取硬碟資源的路徑，使得操作系統或使用者可以訪問並操作硬碟上的檔案

- [partition-table 和 filesystem-table (fstab)的差異](00_disk-partition.md#partition-table-和-filesystem-table-fstab的差異)

- 檔案系統的類型，
  - `類型1`，用於開機的虛擬檔案系統，[initrd 或 initramfs](./04_initramfs.md)
  
  - `類型2`，日常使用的檔案系統，一般指保存在硬碟上的檔案系統

  - `類型3`，由內核建立的各種虛擬檔案系統，一般是基於內存的，不是保存在硬碟上，可透過 `$ cat /proc/filesystem` 查看，例如，
    
    nodev 代表不需要實際的設備文件，是基於內存的虛擬檔案系統，且大多數是由內核直接創建和管理的

    ```shell
    # cat /proc/filesystem
    nodev   sysfs   # 提供對系統內部構造的訪問，允許用戶空間訪問和配置系統內部結構
    nodev   tmpfs   # 臨時記憶體文件系統，提供一致性的記憶體緩存
    nodev   bdev    # 代表 block device 虛擬文件系統，提供對 block device 的抽象層
    nodev   proc    # 提供系統信息和內核參數的訪問
    nodev   cgroup  # 控制組，允許限制和隔離系統資源使用
    nodev   cgroup2 # cgroup 的第二代版本，提供更好的隔離和限制能力
    nodev   cpuset  # 允許指定哪些 CPU 可以被使用
    nodev   devtmpfs  # 提供對設備文件的快速初始化
    nodev   debugfs   # 用於內核開發和調試的虛擬文件系統
    nodev   tracefs   # 用於追蹤系統調試的虛擬文件系統
    nodev   securityfs  # 用於安全模塊的虛擬文件系統
    nodev   sockfs      # 提供對 socket 的訪問
    nodev   bpf         # 提供 BPF（Berkeley Packet Filter）功能
    nodev   pipefs      # 提供管道的虛擬文件系統
    nodev   ramfs       # 提供對 RAM 的訪問
    nodev   hugetlbfs   # 提供大頁框的虛擬文件系統
    nodev   devpts      # 提供 pseudo-terminal 的虛擬文件系統
    nodev   mqueue      # 提供 message queue 的虛擬文件系統
    nodev   pstore      # 提供對系統錯誤日誌的存儲
    ```
  
  透過 `$ cat /etc/mtab` 可以查看透過 mount 已掛載的節點

## overlayfs 檔案系統

- why overlayfs
  
  overlayfs是一種堆疊檔案系統，它依賴並建立在其它的檔案系統之上（例如ext4fs、xfs），
  並不直接參與磁碟空間結構的劃分，僅僅`將底層檔案系統中不同層的目錄進行合併`，然後向用戶呈現合併後的結果
  
  對於使用者來說，所見到的overlayfs根目錄下的內容，來自於掛載時所指定的不同目錄合併後的結果

  <img src="./doc/filesystem/overlayfs.png" width=80% height=auto>
  
  依上圖為例，按照 Upper -> Lower 的次序一次堆疊與合併，最終用戶只看到最上層的 MergedDir
  - 在merge目錄下將會同時看到來自各lower和upper目錄下的內容
  - 使用者無法感知檔案來自lowerDir或是upperDir

  overlayfs 也被廣泛用於容器中，例如鏡像構建，容器運行，都會用到相應的技術，
  用於一般的發行版，可以有效地節省空間，提高訪問的效能

- 合併規則
  - 規則，上層文件(upperDir)優先
  - 規則，各層目錄中的upper dir是可讀寫的目錄，底層文件系統(lowerDir)只讀

    - 例如，用戶通過mergedDir向upperDir的檔案寫入資料時，
      資料將直接寫入upperDir下的檔案中，刪除檔案也是同理

    - 例如，用戶通過mergedDir向lowerDir的檔案或目錄進行任意操作時，
      lowerDir中的內容均不會發生任何的改變

      實際上，overlayfs首先會的`拷貝一份lowerDir中的檔案副本到upperDir`中，
      後續的寫入和修改操作將會在upperDir下的副本檔案中進行，lowerDir原檔案被隱藏
  
      操作mergedDir對應來自lowerDir的檔案或目錄，lowerDir中的內容均不會發生任何的改變
  
  - 規則，當upperDir和lowerDir存在`同名檔案`時，
    lowerDir的檔案將會被隱藏，使用者只能看見來自upperDir的檔案

  - 規則，不同層的lowerDir也存在相同的層次關係，上層會遮蔽下層的同名檔案

- 使用場景

  在同一個裝置上，使用者A和使用者B有一些共同使用的共享檔案（例如執行程式所依賴的動態連結庫等），一般是只讀的；
  同時也有自己的私有檔案（例如系統配置檔案等），往往是需要能夠寫入修改的；
  最後即使使用者A修改了被共享的檔案也不會影響到使用者B

  因此，並不希望每個使用者都有一份完全一樣的檔案副本，因為這樣不僅帶來空間的浪費也會影響效能，
  因此overlayfs是一個較為完美的解決方案，

  - 將這些共享的檔案和目錄所在的目錄設定為lowerDir(1~n)，
  - 將使用者私有的檔案和目錄所在的目錄設定為upperDir，
  - 掛載到使用者指定的掛載點，
  
  如此，既能夠保證前面列出的需求，同時也能夠保證使用者A和B獨有的目錄樹結構

  並且，使用者A和使用者B在各自掛載目錄下看見的共享檔案其實是同一個檔案，
  能大幅節省磁碟空間，且共享同一份cache而減少記憶體的使用和提高訪問效能，

  只要cache不被回收，只需某個使用者首次訪問時建立cache，後續其他所有使用者都可以通過訪問cache來提高IO效能。

## overlayfs 使用範例

- 範例，[在alpine的init系統中掛載 overlayfs](../alpinelinux/[example]%20initramfs-init-example/init-from-alpine)

- 範例，手動掛載 overlayfs
  
  - lower1、lower2、upper 是用來合併的
  - 空的 worker 文件夾裡面不能有任何內容
  - merged 文件夾，用來作為給用戶呈現的最終文件夾
  
  ```shell
  mkdir lower1
  mkdir lower2
  mkdir upper
  mkdir worker    # 一個用於 overlayfs 操作的工作目錄
  mkdir merged
  touch lower1/lower1.txt
  touch lower2/lower2.txt
  touch upper/upper.txt

  # 掛載 overlayfs
  # lower1 是最底層
  mount -t overlay overlay -o lowerdir=./lower1:./lower2,upperdir=./upper,workdir=./worker ./merged

  # 增加文件
  # 增加文件是直接添加到了 upper 文件夾中
  touch ./merged/merged.txt
  ls ./merged/    # 得到 lower1.txt  lower2.txt  merged.txt  upper.txt
  ls ./upper/     # 得到 merged.txt  upper.txt

  # 刪除文件
  # 刪除後，./merged/lower1.txt 被刪除了，./lower1/lower1.txt 被保留
  rm -f ./merged/lower1.txt
  ls ./merged/    # 得到 lower2.txt  merged.txt  upper.txt
  ls ./lower1/    # 得到 lower1.txt

  ls ./upper/     # 得到 lower1.txt merged.txt upper.txt，其中
  # c--------- 1 root root 0, 0 9月  26 19:13 lower1.txt
  # c 表示此文件已經被刪除了，會將下層的該文件直接忽略
  # c 稱為 whiteout 文件，當掃描到此文件時，會忽略此文件名

  # 修改文件
  echo 'hello' > ./merged/lower2.txt
  cat ./merged/lower2.txt   # 得到 hello
  ls -al ./upper/   # 得到以下
  # -rw-r--r-- 1 root root    6 9月  26 19:17 lower2.txt
  
  cat ./lower2/lower2.txt   
  # 內容仍然為空，代表修改文件是將下層的文件複製到上層來之後再進行修改的
  ```

  掛載後的結果

  <img src="./doc/filesystem/overlayfs-mount-example.png" width=80% height=auto>

## ref

- [寫一個簡單的filesystem](https://maastaar.net/fuse/linux/filesystem/c/2016/05/21/writing-a-simple-filesystem-using-fuse/)

- [內核相關文檔](https://www.kernel.org/doc/)

- [Open Linux Symposium, OLS 開源會議中的論文集](https://www.kernel.org/doc/ols/)

- sysfs
  - [sysfs介紹](https://www.kernel.org/doc/ols/2005/ols2005v1-pages-321-334.pdf)
  - [sysfs @ online man page](https://man7.org/linux/man-pages/man5/sysfs.5.html)

- overlayfs
  - [深入理解overlayfs](https://www.796t.com/content/1541539989.html)
  - [overlayfs 文件系統](https://www.zido.site/blog/2021-09-26-overlayfs-filesystem/)
  - [overlayfs操作範例](https://wiki.friendlyelec.com/wiki/index.php/How_to_use_overlayfs_on_Linux/zh#.E5.A6.82.E4.BD.95.E9.89.B4.E5.88.AB.E7.B3.BB.E7.BB.9F.E6.98.AF.E5.90.A6.E5.B7.A5.E4.BD.9C.E5.9C.A8OverlayFS)