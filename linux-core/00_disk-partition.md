## 硬碟基礎

  - 磁區(sector)、磁軌(track)、磁柱(cylinder)、
    - 磁區(扇區): 每 512Bytes 或 4KBytes 個連續的儲存區域稱為磁區，磁區呈扇形也稱為扇區，
    - 磁軌:多個磁區繞著磁盤旋轉一圈稱為磁盤，越靠近圓心的磁軌包含的磁區越少，多個磁軌組成一個磁盤
    - 磁柱: 多個磁盤組成一個磁柱

  - 硬碟大小 = 磁柱數 * 磁軌 * 磁區 * 磁區大小

## 開機磁區(Main-Boot-Region，MBR)保存bootloader程序的代碼

- 圖解MBR內容

  <img src="doc/bootup-flow/gpt-vs-mbr.png" width=80% height=auto>

- 開機磁區的組成
  - 硬碟的第一個磁區(512Byte)固定為`開機磁區`，或稱為主引導磁區(Master-Boot-Region)
    - 組成1，`系統引導程式(Bootloader)的代碼區`，最大446bytes
    - 組成2，`硬碟分割表(Disk-Partition-Table，DPT)`，用於記錄`每一個磁碟分區的起始位址`，共四個16bytes的子項目，共 64bytes
    - 組成3，MBR 有效標誌位，共 2bytes
    - 446-bytes的Booloader + 64-bytes的磁碟分割表 + 2-bytes的MBR有效標誌位 = 512 byte的開機磁區

- 注意，<font color=red>MBR有可能是512byte的開機磁區，有可能是64byte的磁碟分割表</font>
  - 在描述上需要明確區分兩者的差異
  - 512bytes的開機磁區(Main-Boot-`Region`)
  - 64bytes的分割表(Main-Boot-`Record`)

- 硬碟分割表(DPT)有兩種格式
  - 種類1，`MBR (Master Boot Record) 格式分割表`，占用 64bytes
    - MBR的分割表中，磁盤由若干個分區組成，每個分區都有一個唯一的標識符 (Partition Identifier) 和一個分區表項。
    - 分區表項記錄了該分區的起始扇區和長度，以及該分區的文件系統格式等信息。
    - 可用於 BIOS 或 UEFI
    - 缺點，僅支援2TB硬碟

  - 種類2，`GPT (GUID Partition Table) 格式分割表`，超過 64bytes
    - GPT 硬碟分割表保留了前 512Bytes 作為 Protective-MBR，作為開機用
    - GPT 使用邏輯區塊位址(Logical Block Address, LBA)來記錄分割訊息，
      - 每個LBA具有 512Bytes 大小，可記錄四個磁碟分區的起始位置
      - 整個分割表共有34個LBA區塊，可記錄 128個個磁碟分區的起始位置 (LBA2-LBA33)
    - 優，比 MBR 更高級，支持更大的磁盤和分區
    - 只能用於於 UEFI
    
    - 注意，`GPT分割表不是取代 MBR分割表的位置`，而是保留了整個 512 bytes 的MBR區域外，
      再加上額外的硬碟區域紀錄更詳細的硬碟分割詳情
  
  - 注意，MBR 分割表或是 GPT 分割表的前 512bytes 都是 MBR，都包含引導程式，
    無論是哪一種都不影響使用

## partition-table VS filesystem-table(fstab) VS mount-table(mtab)

- partition-table (分區表)
  - 分區表位於磁碟的最前面的數據結構，用於管理磁碟自身的屬性
  - 描述磁碟的分區佈局，包含磁碟上有多少個分區，每個分區的大小、類型、位置等資訊
  - 不存儲filestsytem的信息
  - 有兩種類型
    - MBR (Master Boot Record): 傳統的分區表格式
      - 支援最多 4 個主要分區（或 3 個主要分區加 1 個擴展分區）。
      - 單個分區的大小最大為 2TB

    - GPT (GUID Partition Table): 現代的分區表格式
      - 支援多達 128 個分區
      - 單個分區大小可以超過 2TB

  - [用於操作Partition-Table的命令](#pt-tools)

- filesystem-table (文件系統表，fstab)
  - fileststem用於`建立目錄結構和對檔案進行管理`，才能進行路徑或檔案的操作，例如，cd、ls ... 等
  - fstab 位於 /etc/fstab，用於管理組織磁盤上的數據的方式
  - 描述文件和目錄在磁盤上的存儲方式，包含如何存儲文件、管理文件權限、處理文件元數據
  - 有各種文件系統類型，例如，ext4、NTFS、xfs、btrfs ... 等
  - [用於操作filesystem-Table的命令](#fstab-tools)

- mount-table (掛載表，mtab)
  - 由 mount 和 umount 命令負責維護的表，紀錄已掛載的節點、掛載來源、掛載方式
  - mtab 位於 /etc/mtab，

## 檔案系統格式基礎

- 為什麼需要格式化: 
  - 每種作業系統保存檔案的格式不同，透過格式化，將硬碟變成`OS能夠利用的檔案系統格式`(filesystem)
  - 每一個磁碟分區原則上只能有一種檔案系統格式
  - 透過 LVM (Logical Volume Manager)，可以將一個分割槽格式化為多個檔案系統

- 檔案系統的選擇
  - FAT32 
  - NTFS (New Technology File System): 用於 Windows 的存儲和管理文件
  - exFAT (Extended File Allocation Table): 用於存儲較大文件的文件系統，它能夠支持更大的磁盤容量和更大的文件
  - HFS+ (Hierarchical File System Plus): 用於 MAC 的存儲和管理文件
  
- 磁碟中的檔案系統必須要掛載(mount)到目錄樹的某個目錄後才能使用
  - 掛載(mount)，是`硬碟`和`指定路徑`的映射關係
  - 在 linux 中，磁碟分割區`沒有固定的磁碟機代號`，可以`掛載到任意的指定路徑`中，該路徑就稱為`磁碟的掛載點`
  - 例如，磁碟分割區 /dev/sda 掛載到 / ，相當於將 /dev/sda 映射到 / ，讀取 / 就相當於讀取 /dev/sda 內的資料
  - 例如，磁碟分割區 /dev/sdb 掛載到 /home ，相當於將 /dev/sdb 映射到 /home ，讀取 /home 就相當於讀取 /dev/sdb 內的資料

- 建立可用磁碟流程: 磁碟分割(建立partition-table) > 檔案系統格式化(format) > 磁碟掛載到系統(建立filesystem-table)
  - step1，磁碟分割
    - 建立磁區: 包含分區編號、分區大小、分區類型、
    - 建立分區表(partition-table)

  - step2，檔案系統格式化: 在分區上建立檔案系統

  - step3，磁碟掛載: 建立檔案系統掛載表(fileststem-table)，用於開機自動掛載

## 磁碟操作相關工具

- 常用命令
  - 查看系統硬碟掛載的情況 `$ df -h`
  - 列出 /dev 下，所有硬碟裝置掛載的路徑，`$ ls /dev/[sh]d*`
  - 查看指定硬碟的邏輯分區和容量，`$ sudo fdisk -l /dev/掛載路徑`
  - 列出系統上的所有磁碟分區列表和磁區資訊，`$ lsblk`
  - 列出裝置的 UUID 等參數，`blkid命令`

- <a id='pt-tools'>磁碟分割工具 (操作partition-table的工具)</a>
  - `fdisk命令`: 只能用於管理使用`MBR格式`的分區表
  - `sfdisk命令`: 
  - `gdisk命令`: GPT 的磁碟分割工具
  - `sgdisk命令`: MBR/GPT 的磁碟分割工具
  - `parted命令`: 支援`MBR格式`和`GPT格式`的分區表。支援對分區進行動態調整（resize）和對齊（alignment）
  - `cfdisk命令`: 基於字符界面的分區工具，提供了一個比 fdisk 更直觀的用戶界面，且支援MBR和GPT，也支援動態調整（resize）
  - `partprobe命令`: 更新 Linux 核心的分割表資訊

- <a id='fstab-tools'>格式化工具 (操作filesystem-table的工具)</a>
  - `mkfs命令`: 高階格式化工具，例如，mkfs -t xfs，實際上執行 mkfs.xfs 命令
  - `mkfs.xfs命令`: 格式化為 xfs 檔案系統
  - `mkfs.ext4命令`: 格式化為 ext4 檔案系統

- 檔案系統的手動掛載和卸載
  - 限制
    - 單一檔案系統不應該被重複掛載在不同的掛載點(目錄)中；
    - 單一目錄不應該重複掛載多個檔案系統；
    - 要作為掛載點的目錄，理論上應該都是空目錄才是
  
  - `mount命令`: 將以格式化的磁碟掛載到目錄樹中
  - `umount命令`: 將磁碟從目錄樹中卸載

- 硬碟磁區掛載表 (自動掛載)
  - 限制
    - 根目錄 / 是必須掛載的﹐而且一定要先於其它 mount point 被掛載進來。
    - 其它 mount point 必須為已建立的目錄﹐可任意指定﹐但一定要遵守必須的系統目錄架構原則 (FHS)
    - 所有 mount point 在同一時間之內﹐只能掛載一次。
    - 所有 partition 在同一時間之內﹐只能掛載一次。
    - 如若進行卸載﹐您必須先將工作目錄移到 mount point(及其子目錄) 之外。

  - 掛載訊息表`/etc/fstab`(filesystem-table)，是 mount 指令進行掛載時， 將所有的選項與參數寫入此檔案中

  - 格式:   `[磁碟裝置檔名]            [掛載點]  [檔案系統]  [檔案系統參數]  [dump]  [fsck]`
    - 例如，`/dev/mapper/centos-root      /       xfs        defaults       0       0  `
    - 磁碟裝置檔名: 磁碟的裝置檔名、UUID、LABEL
    - 掛載點: 掛載的目錄
    - 檔案系統: 檔案系統格式
    - 檔案系統參數: 
    - dump: 能否被 dump 備份指令作用，0 代表不需要進行此動作
    - fsck: 是否以 fsck 檢驗磁區，0 代表不需要進行此動作

## 建立 SWAP 分區

- 用途
  - SWAP磁區用於記憶體不足時，暫時將記憶體的程序拿到硬碟中暫放的記憶體置換空間(swap)
  - 用於伺服器的linux通常會預留SWAP磁區，當收到大量網路請求時，可避免記憶體不足造成問題
  - Linux 進入休眠狀態下，運作中的程式狀態也會保存到 swap

- 建立 swap 的兩種方式
  - 方法1，利用磁碟建立`swap分區(swap-partition)`
  - 方法2，利用檔案建立`swap檔案(swap-file)`

  - 比較: swap-partition VS swap-file

    | 比較項目     | swap分區                             | swap檔案                                      |
    | ------------ | ------------------------------------ | --------------------------------------------- |
    | 管理         | 管理簡單，可透過套件管理             | 管理麻煩，必須手動執行多個命令                |
    | 調整swap大小 | 可動態調整，但有風險，有機會數據丟失 | 可動態調整                                    |
    | 預留磁碟空間 | 需要預留磁碟空間                     | 不須預留磁碟空間                              |
    | 空間占用     | 空間占用大                           | 寫入數據才佔用空間，空間占用小                |
    | 限制         | 大部分檔案系統都支援                 | FAT、NTFS 不支援fallocate命令，但可支援dd命令 |

- 方法1，建立swap分區(swap-partition)
  - step1，建立分區，`$ sgdisk -n 1:0:0 -c 1:"swap" -t 1:8200 /dev/sda`
  - step2，將磁區分割為 swap 格式，`$ mkswap /dev/sda1`
  - step2，啟動 swap 分區，`$ swapon /dev/sda1`
  - step3，設置自動掛載，`$ genfstab -U /mnt >> /mnt /etc/fstab`
  
- 方法2，建立swap檔案(swap-file)
  - step1，dd 或 fallocate 建立 /swapfile (推薦使用 fallocate，佔用空間小)
    - 範例，以dd建立，`$ dd if=/dev/zero of=/tmp/swap bs=1M count=128`
    - 範例，以fallocate建立，`$ fallocate -l 2GB /tmp/swap`
    
  - step2，透過 mkswap 格式化 /swapfile，`$ mkswap /tmp/swap`

  - step3，啟動 swap 分區，`$ swapon /tmp/swap`，或自動激活 `$ swapon -a`

  - step3，需要手動在 /etc/fstab 手動添加 swap-file，讓 swap-file 在開機後自動掛載
    ```shell
    
    # 注意，/tmp/swap 是檔案不是磁區，第一欄不要使用 UUID，而是指定 swap-file 的路徑
    # 格式，<fileststem> <dir> <type> <options> <dump> <pass>
            swap         /tmp   swap  defaults    0      0
    ```

## [命令] fdisk 的使用

- 範例，fdisk 建立新磁碟分區
  ```shell
  fdisk /dev/sda << EOF
  n
  p
  1
  2048
  +1048576
  w
  EOF
  ```

- 範例，fdisk 建立新gpt磁碟分區
  ```shell
  fdisk /dev/sda << EOF
  g
  n
  1
  2048
  +512M
  n
  2
  1050624
  +8G
  n
  3
  17827840
  20969471
  w
  EOF
  ```

- 範例，fdisk 建立新磁碟分區並變更類型 (常用於建立開機的磁碟分區)
  ```shell
  fdisk /dev/sda << EOF
  n         // 新分區
  1         // sda1
  1050624   // start-sector
  +8G       // end-sector
  n         // 新分區
  2         // sda2
  17827840  // start-sector
  20969471  // end-sector
  t         // 變更類型，可輸入 L 列出所有類型
  swap      // 變更為 swap 類型
  w         // 寫入變更
  EOF
  ```

## [命令] sfdisk 的使用

- sfdisk 是 fdisk 套件提供的命令

- 範例，建立gpt磁碟分區
  ```shell
  # -X gpt，建立 gpt LABEL
  # 建立分區的格式 <start-sector>, <end-sector>, <type>, <bootable>
  # ,前方的空白表示 start-sector 使用預設值
  sfdisk -X gpt /dev/sda << EOF
   ,512M 
   ,8G
   ,1.5G
  write
  quit
  EOF
  ```

- 範例，建立磁碟分區並變更類型 (常用於建立開機的磁碟分區)
  
  列出所有可用類型，`$ sfdisk -T`

  ```shell
  # -X gpt，建立 gpt LABEL
  # 建立分區的格式 <start-sector>, <end-sector>, <type>, <bootable>
  # ,前方的空白表示 start-sector 使用預設值
  sfdisk /dev/sda << EOF
   ,512M, U   // 第1個分區: 自動偵測start-sector, 容量+512MB, 標記為 UEFI 類型
   ,8G, L     // 第2個分區: 自動偵測start-sector, 容量+5GB, 標記為 Linux-filesystem 類型
   ,1.5G, S   // 第3個分區: 自動偵測start-sector, 容量1.5GB, 標記為 swap 類型
  write       // 寫入變更
  quit        // 離開互動模式
  EOF
  ```
  
- 範例，刪除所有分區，`$ sfdisk --delete /dev/sda`

- 範例，印出磁碟分區表`$ sfdisk --dump /dev/sda`

- 範例，印出所有硬碟的磁碟分區表，`$ sfdisk -l`

- 範例，快速導出磁碟分區表，並指定檔案快速恢復磁碟分區
  ```shell
  # step1，將/dev/sda硬碟的分區表配置，dump 到 my.layout
  sudo sfdisk -d /dev/sda > my.layout

  # step2，在另一個設備上，從 my.layout 恢復分區
  sudo sfdisk /dev/sdb < my.layout
  ```

## [命令] sgdisk 的使用

- 印出 partition-table，`$ sgdisk -p /dev/sda`

- 印出可用的類型，`$ sgdisk --list-types`

- 清除 partition-table，並將MBR轉換為GPT，`$ sgdisk -o -g /dev/sda`
  - -o，clear partition-table
  - -g，mbr to gpt
  - -m，gpt to mbr

- 建立分區，
  - 格式: sgdisk -n <編號>:<start-sector>:<end-sector> -c <編號>:新名 -t <編號>:<類型> /dev/sda
    - -n，建立新磁碟分區
    - -c，變更磁碟分區名稱
    - -t，變更磁碟分區x類型
  
  - 範例，建立 archlinux 分區
    - 注意，start-sector 和 end-sector 可指定為0，代表第一個可用地址或最後一個可用地址
    - 參考，archlinux 使用的[分區類型代碼](https://wiki.archlinux.org/title/GPT_fdisk#Partition_type)

    - 建立 efi 分區，`$ sgdisk -n 1:0:+512M -c 1:"efi" -t 1:ef00 /dev/sda`
    - 建立 root 分區，`$ sgdisk -n 2:0:+8G -c 2:"root" -t 2:8304 /dev/sda`
    - 建立 swap 分區，`$ sgdisk -n 3:0:0 -c 3:"swap" -t 3:8200 /dev/sda`

- 刪除分區，
  - `$ sgdisk -d 1 /dev/sda`，刪除第一個分區
  - `$ sgdisk --zap-all /dev/sda`，刪除所有分區

- 備份分區/恢復分區
  - 二進制備份，會保存: 
    - protective MBR
    - the main GPT header
    - the backup GPT header
    - the copy of partition table 

  - 建立二進制備份，`$ sgdisk -b=backup-sda.bin /dev/sda`

  - 還原二進制備份，`$ sgdisk -l=backup-sda.bin /dev/sda`

## [命令] parted 的使用

- 建立分區標籤，`$ parted -s /dev/sda mklabel msdos`  

- 建立分割區，並進行優化，`$ parted -s -a optimal /dev/sda mkpart primary 0% 100%`
  - -s 靜默模式下運行 parted，不顯示交互式提示
  - -a optimal 分區創建時使用最佳分區對齊方式
  - mkpart primary 指定創建一個主分區
  - 0% 100% 主分區的起始位置和結束位置

- 設置分區參數，設置為可引導分區，`$ parted -s /dev/sda set 1 boot on`

## [範例] 使用 fdisk | sgdisk | sfdisk 建立磁碟分區

- 磁碟分區規劃
  ```shell
  # EFI分區：這是一個小的分區，用於存儲 EFI-bootloader，使用 FAT32格式，
  # 大小 = 1048577-sector * 512Bytes = 536,871,424 = 512MB
  /dev/sda1 : start=        2048, size=     1048577   # 512M

  # root分區：這是系統的主分區，用於存儲操作係統文件和應用程式。通常使用ext4格式
  # 大小 = 16779265-sector * 512Bytes = 8,590,983,680 bytes = 8GB
  /dev/sda2 : start=     1052672, size=    16779265   # 8G
  
  # SWAP分區：這是一個交換分區，用於交換記憶體。通常使用 linux-swap
  # 大小 = 3135488-sector * 512Bytes = 1,605,369,856 bytes = 1.5GB
  /dev/sda3 : start=    17833984, size=     3135488   # 1.5G
  ```

- 範例，以 fdisk 建立磁碟分區
  ```shell
  fdisk /dev/sda << EOF
  n
  p
  1
  2048
  +512M
  t
  efi
  n
  p
  2
  1052672
  +8G
  n
  p
  3
  17833984
  +1.5G
  t
  swap
  EOF  
  ```

- 範例，以 sfdisk 建立磁碟分區
  - 命令，建立GPT分區

    ```shell
    # -X gpt，建立 gpt LABEL
    # 建立分區的格式: <start-sector>, <end-sector>, <type>, <bootable>

    # ,512M, U   // 第1個分區: 自動偵測start-sector, 容量+512MB, 標記為 UEFI 類型
    # ,8G, L     // 第2個分區: 自動偵測start-sector, 容量+5GB, 標記為 Linux-filesystem 類型
    # ,1.5G, S   // 第3個分區: 自動偵測start-sector, 容量1.5GB, 標記為 swap 類型

    sfdisk -X gpt /dev/sda << EOF
    ,512M, U
    ,8G, L
    ,1.5G, S
    write
    quit
    EOF
    ```
    
  - 命令，查看結果，並匯出磁碟分區表，`$ sfdisk --dump /dev/sda > disk-parts.dump`

- 範例，以 sfdisk + dump檔案 恢復分區
    - 命令，從文件恢複磁碟分區表，`$ sfdisk /dev/sda < $FILES_DIR/disk-parts.dump`
    
    - 命令，將結果寫入硬碟，`$ sync`

    - 可利用 `cat disk-parts.dump` 檢視內容
  
    - disk-parts.dump 的內容
      ```shell
      /dev/sda1 : start=        2048, size=     1048577, type=0FC63DAF-8483-4772-8E79-3D69D8477DE4, uuid=4D8E660C-EF14-C64F-8B6B-50919A5B112A
      /dev/sda2 : start=     1052672, size=    16779265, type=0FC63DAF-8483-4772-8E79-3D69D8477DE4, uuid=80A001BC-064D-4C4B-AA35-309F80D94CEF
      /dev/sda3 : start=    17833984, size=     3135488, type=0FC63DAF-8483-4772-8E79-3D69D8477DE4, uuid=AE1E09E8-A25A-B34F-9D6A-D12D515A04C8
      ```

- 範例，以 sgdisk 建立磁碟分區
  ```shell
  # 清除 partition-table，並將MBR轉換為GPT
  sgdisk -o -g /dev/sda

  # 格式: sgdisk -n <編號>:<start-sector>:<end-sector> -c <編號>:新名 -t <編號>:<類型> /dev/sda
  # start-sector 和 end-sector 可指定為0，代表第一個可用地址或最後一個可用地址

  # 建立 efi 分區
  sgdisk -n 1:0:+512M -c 1:"efi" -t 1:ef00 /dev/sda
  
  # 建立 root 分區
  sgdisk -n 2:0:+8G -c 2:"root" -t 2:8304 /dev/sda

  # 建立 swap 分區
  sgdisk -n 3:0:0 -c 3:"swap" -t 3:8200 /dev/sda
  ```
## [範例] 重新調整分區的大小

範例，[變更磁碟映像的大小](https://gitlab.com/anthony076/docker-projects/-/blob/main/%5B%20Virtual%20Machine%20%5D/qemu/qemu.md)

## Ref 
- [Linux 磁碟與檔案系統管理 @ 鳥哥](https://linux.vbird.org/linux_basic/centos7/0230filesystem.php)
- [Sfdisk Tutorials](https://linuxhint.com/sfdisk-tutorials/)
- [sfdisk命令](https://blog.csdn.net/carefree2005/article/details/120992143)
- [sgdisk的使用](https://www.796t.com/article.php?id=160400)