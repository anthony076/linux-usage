## display system in linux

- 主要用於顯示的子系統
  - `FBDEV`: Framebuffer Device
    - 功能：一個簡單的幀緩衝接口，允許應用程序直接訪問顯存。這是一個相對較舊的接口，主要用於簡單的顯示任務。
    - 優，簡單易用，適合嵌入式系統或需要直接操控顯存的應用。
    - 缺，功能較少，性能不高，不支持現代圖形加速和複雜的顯示管理。

  - `DRM/KMS`: Direct Rendering Manager(DRM) / Kernel Mode Setting(KMS)
    - 功能：是現代 Linux 圖形顯示的基礎設施，提供了對圖形硬件的全面管理和控制。`DRM`負責圖形硬件的資源管理和直接渲染支持，而`KMS`負責顯示模式的設置
    - 優，功能強大，支持現代圖形加速和複雜的顯示管理，提供高性能和穩定性
    - 缺，需要較多的配置和驅動支持

  - `V4L2`: Video For Linux 2
    - 功能：V4L2 主要用於視頻設備的捕捉和處理，例如攝像頭和電視接收卡。提供一套標準接口，用於視頻設備的控制和數據傳輸。
    - 優，專為視頻設備設計，提供豐富的功能和良好的設備兼容性。
    - 缺，主要用於視頻捕捉和處理，不適合通用的圖形顯示任務。

## DRM 和 KMS

- 功能
  - KMS : 負責顯示模式的設置和管理，例如設置屏幕分辨率和刷新率
  - DRM : 負責圖形硬件的管理，包括顯存管理、命令提交和直接渲染支持
  
  - KMS和DRM不需要搭配一起使用，兩者是不同的API，也可以一起搭配使用
    - KMS-api 是 DRM-api 下的一個子集
    - DRM的渲染操作，通過`/dev/dri/renderX`路徑
    - KMS的顯示模式設定，通過`/dev/dri/controlDX`路徑
    - 例如，在嵌入式系統中，有時只需要直接渲染功能，而不需要複雜的顯示模式管理時，可以只使用DRM
    - 例如，科學計算或 AI 訓練，主要利用 GPU 的計算能力，而不需要設置顯示模式時，可以只使用DRM
    - 例如，只需要設置和管理顯示模式，而不需要複雜的渲染功能時，可以只使用KMS
    - 例如，在桌面環境中，KMS 在系統啟動時設置顯示模式，進入系統後則使用DRM來管理圖形硬件資源

- 為什麼需要 KMS

  早期要設置屏幕分辨率和刷新率必須在`用戶空間中(User-space Mode Setting, UMS)`，主要考量到以下幾點
  - 穩定性 : KMS會增加kernel的穩定性，避免kernel的錯誤導致系統崩潰
  - 安全性 : 將顯示模式切換放在高權限的用戶空間，可以安全風險
  - 方便性 : 早期的圖形硬件種類繁多，可以避免頻繁修改內核代碼
  - 方便性 : 用戶空間有豐富的圖形庫通過庫的更新來增加新特性和修復問題，而不需要修改內核

  <font color=blue>UMS帶來的缺點</font>
  - 用戶空間模式切換可能導致啟動過程中屏幕閃爍和延遲
  - 內核模式切換（KMS）可以更高效地管理圖形硬件資源，實現更平滑和高性能的圖形體驗

  <font color=blue>如何解決早期的問題</font>
  - 內核模塊化 : KMS 被實現為可加載的內核模塊，可以獨立加載和卸載，而無需重新編譯整個內核
  - 提供受控的用戶空間接口 : 通過 /dev/dri/cardX 和 /dev/dri/controlDX 文件來與用戶空間通信。
  - 圖形服務器的隔離 : 現代圖形服務器都是在沙盒中運行，限制了其對系統其他部分的訪問
  - 分層架構的設計 : 透過分層設計，兩者可以實現獨立更新和優化
    - 內核負責底層硬件管理和模式設置
    - 用戶空間庫和驅動負責圖形渲染和高級功能

- 常見的庫
  - Mesa : Mesa 是一個用戶空間項目，提供了開源的 OpenGL 實現
  - libdrm : Direct Rendering Manager 的用戶空間庫

- FBDEV device 和 DRM device 的差異

  <img src="doc/display/fbdev-vs-drm.png" width=80% height=auto>

- 元件 : Framebuffer 和 DRM-Framebuffer
  - `Framebuffer`
    - 是一個實體的內存區域，用於存儲將在顯示屏上`顯示的像素數據`，這些數據會被直接發送到顯示設備上
    - 若是集顯，使用系統 RAM 作為 Framebuffer
    - 若是獨顯，使用專用的 VRAM 作為 Framebuffer
  
  - `DRM Framebuffer` : Direct-Rendering-Manager Framebuffer
    - 不僅保存了圖像數據，還比Framebuffer包括更多的管理信息和控制設置。
    - 可以指定顯示內容存放的具體記憶體位置、格式、顯示區域等信息。

- 元件 : CRTC
  - 功能
    - display timing
    - mode setting
    - page flipping

- 元件 : Encoder

- 元件 : Connector 

## 與顯示相關的內核啟動選項

- 注意，以下選項選項可以在啟動時作為引導參數指定，通常在GRUB或其他啟動管理器的配置中進行設置。

- `nomodeset`：禁用內核模式設置，有助於解決一些`啟動時的黑屏或凍結問題`，特別是在顯卡驅動加載時出現兼容性問題時使用。
- `vga=xxx`：指定使用特定的VGA模式。例如，vga=792 表示使用 1024x768 分辨率和 16 位色彩。這種選項在需要手動指定圖形模式時很有用。
- `video=xxx`：這個選項用於指定啟動時的圖形驅動參數，例如分辨率或刷新率。具體的參數值取決於你的顯卡和顯示器支持的配置。
- `fbdev`：強制使用幀緩衝設備作為圖形驅動。幀緩衝設備提供`一種通用的方式來訪問圖形硬件`，適用於一些`不支持或有兼容性問題`的圖形硬件。
- `text`：強制系統進入純文本模式，繞過圖形界面。這對於在啟動時需要進行命令行操作或故障排除時非常有用。
- `xdriver=`：指定使用特定的X Window系統驅動程序，例如 xdriver=vesa 指定使用VESA驅動程序。
- `xforcevesa`：強制系統`使用VESA兼容圖形模式`，通常用於處理一些`不支持高級圖形功能的硬件`。
- `nomode`：類似於 nomodeset，用於禁用特定的顯示模式設置。
- `acpi=off 或 noacpi`：有時禁用ACPI可以解決與圖形相關的問題，儘管這會帶來其他系統功能的限制。

## ref

- [The DRM/KMS subsystem from a newbie’s point of view](https://events.static.linuxfound.org/sites/events/files/slides/brezillon-drm-kms.pdf)

