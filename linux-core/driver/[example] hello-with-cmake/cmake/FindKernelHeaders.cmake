
# Find the kernel release

message("Finding Kernel Headers ...")

# for alpine，KERNEL_RELEASE = 6.6.49-0-virt
execute_process(
        COMMAND uname -r
        OUTPUT_VARIABLE KERNEL_RELEASE
        OUTPUT_STRIP_TRAILING_WHITESPACE
)

# Find kernel-headers

find_path(
  KERNELHEADERS_DIR        # 結果變數

  #include/linux/user.h
  #PATHS /usr/src/kernels/${KERNEL_RELEASE}

  # 尋找目標
  # 找到 /usr/src/linux-headers-6.6.49-0-virt/include/linux/init.h
  # 返回 /usr/src/linux-headers-6.6.49-0-virt，不包含 include/linux
  include/linux/init.h

  # 尋找路徑
  PATHS /usr/src/linux-headers-${KERNEL_RELEASE}
)

message(STATUS "[ Kernel release ] = ${KERNEL_RELEASE}")
message(STATUS "[ Kernel headers ] = ${KERNELHEADERS_DIR}")

# 找到 kernel-headers 的路徑後，建立 KERNELHEADERS_DIR 的變數
if (KERNELHEADERS_DIR)
    set(KERNELHEADERS_INCLUDE_DIRS
        ${KERNELHEADERS_DIR}/include
        ${KERNELHEADERS_DIR}/arch/x86/include
        CACHE PATH "Kernel headers include dirs"
    )
    
    set(KERNELHEADERS_FOUND 1 CACHE STRING "Set to 1 if kernel headers were found")
    
else ()

    set(KERNELHEADERS_FOUND 0 CACHE STRING "Set to 1 if kernel headers were found")
    
endif ()

message(STATUS "[ KERNELHEADERS_INCLUDE_DIRS ] = ${KERNELHEADERS_INCLUDE_DIRS}")
message(STATUS "[ KERNELHEADERS_FOUND ] = ${KERNELHEADERS_FOUND}")

mark_as_advanced(KERNELHEADERS_FOUND)
