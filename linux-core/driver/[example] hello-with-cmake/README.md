## hello-with-cmake 使用方式

- 修改自 [cmake-kernel-module @ thejjw](https://github.com/thejjw/cmake-kernel-module/tree/master)

- 測試環境
  - windows 11 23H2 with hyperV enable
  - QEMU emulator version 8.2.0 (v8.2.0-12045-g3d58f9b5c5) 安裝版
  - Linux alpine 6.6.48-0-virt #1-Alpine SMP PREEMPT_DYNAMIC 2024-08-30 06:20:46 x86_64 Linux

- 安裝依賴

  ```shell
  apk add build-base cmake linux-virt-dev 
  ```

- 使用流程
  
  ```shell
  mkdir build
  cd build
  cmake ../
  make
  ```

- ISSUE，找不到 kernel-headers， KERNELHEADERS_INCLUDE_DIRS 為空
  - 檢查1，安裝 linux-virt-dev 後，檢查 `/usr/src/linux-*` 的linux版本
  - 檢查2，`$ uname -a` 檢視 linux 的版本
  - 若兩者不一致，`$ apk upgrade` 後，重新開機