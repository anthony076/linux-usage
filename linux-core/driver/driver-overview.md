## driver 驅動程式

- driver 也是內核模塊的一種，一種負責與 device 交互的內核模塊，同樣以kernal-module的方式加載到系統中，
  參考，[內核模塊化](../03_kernel.md)

- 自定義內核模塊的組成
  - 組成1，模組加載函數(必須)：當通過insmod命令加載內核模組時，模組的加載函數會自動被內核執行，完成本模組相關初始化工作
  - 組成2，模組卸載函數(必須)：當通過rmmod命令卸載模組時，模組的卸載函數會自動被內核執行，完成與模組加載函數相反的功能
  - 組成3，模組許可證宣告(必須)：模組許可證宣告描述內核模組的許可權限，如果不宣告許可證，模組被加載時將收到內核被汙染的警告
  - 組成4，模組參數(可選)：模組參數是模組被加載的時候可以被傳遞給他的值，它本身對應模組內部的全局變數
  - 組成5，模組導出符號(可選)：內核模組可以導出符號(symbol,對應於函數或變數)，這樣其他模組可以使用本模組中的變數或函數
  - 組成6，模組作者等信息宣告(可選)。  

- 驅動程式一般也是透過[模塊]的方式添加到系統中
- [推薦的 kernel-module 編譯選項](kernel-compile-options.md)

## 範例，編譯模塊 | 加載模塊 | 卸載模塊，以簡單的 hello 為例

[編譯模塊、加載模塊、卸載模塊，以簡單的 hello 為例](doc/編譯模塊、加載模塊、卸載模塊，以簡單的%20hello%20為例.png)

## 範例，最小內核模塊範例 + 開發環境設置範例(alpine + helix)

- 環境
  - windows 11 23H2 with hyperV enable
  - QEMU emulator version 8.2.0 (v8.2.0-12045-g3d58f9b5c5) 安裝版
  - Linux alpine 6.6.48-0-virt #1-Alpine SMP PREEMPT_DYNAMIC 2024-08-30 06:20:46 x86_64 Linux
  - 使用 helix-ide + clangd 進行開發 

- 安裝依賴

  ```shell
  # 確認當前系統內核的版本
  uname -r

  # 確認apk上的內核包(linux-virt|linux-virt-dev)的版本，要和上述是一致的
  apk info -v | grep linux-virt

  # (optional) 若不一致，升級系統到最新
  apk upgrade
  reboot

  # 安裝建構工具和依賴
  
  # - build-base = 基本的建構工具包
  # - linux-virt = 提供內核的二進制文件(例如，/boot/vmlinuz-virt)和模塊文件(例如，/lib/modules/)，但不包含用於開發的頭文件或源代碼
  # - linux-headers = 提供用於(用戶空間程序的頭文件)，不包含用於編譯內核模塊的工具或配置文件，安裝在`/usr/include`的位置
  # - linux-virt-dev = 提供用於(內核模塊和驅動程序開發的完整頭文件和配置文件)，不包含完整的內核源碼樹，安裝在`/usr/src/`的位置

  apk add build-base linux-headers linux-virt-dev linux-virt
  # 透過 uname -a，得到 Linux my-alpine 6.6.48-0-virt，內核版本為virt，要安裝 linux-virt 的版本

  # 確認，內核版本和頭文件的版本要一致
  uname -r
  ls /usr/src | grep linux-headers

  # 確認一致後，再建立符號連結
  sudo ln -sf /usr/src/linux-headers-$(uname -r) /lib/modules/$(uname -r)/build
  ```

- (optional) 使開發內核模塊時，能夠`跳轉到內核函數的實現`
  - 概念
    - linux-headers套件，用於用戶空間程序的頭文件，inux-virt-dev套件，用於核模塊和驅動程序開發的完整頭文件，
      兩者都不包含完整的內核源碼，`若要實現跳轉到函數，需要額外下載內核完整源碼`

    - 以`/usr/src/linux-headers-6.6.48-0-virt/include/linux/random.h`提供的`get_random_bytes()`為例

      get_random_bytes() 函數實際上是在`/usr/src/linux-6.6.48/drivers/char/random.c`中實現
      
      要使 helix-ide 的 lsp-clangd 能夠跳轉到random.c的位置，
      <font color=blue> 需要將random.c 添加到 compile_commands.json 中</font>，
      <<font color=blue>但不推薦這樣做</font>>

      - clangd 使用 compile_commands.json 來理解源文件的編譯上下文，包括編譯標誌、定義的宏、包括的頭文件等
      - 若 random.c 不添加到 compile_commands.json 中，clangd 就無法知道如何編譯該文件，從而無法解析該文件的符號和定義，也就無法正確跳轉
      - 若 random.c 中調用了其他源文件中的函數，那麼這些源文件也需要被添加到 compile_commands.json 中
      - 因此，獨立的內核模塊專案中，直接實現 Helix 跳轉到 Linux 內核源碼中函數的實現代碼，通常是<font color=blue>不建議這麼做</font>

        Linux 內核的源碼是一個非常大的工程，包含了許多依賴、配置和特定的編譯選項。內核模塊和內核源碼之間可能有許多依賴關係和條件編譯的情況。在一個獨立的內核模塊專案中，將所有內核源碼都包括進來，並維護一個完整的 compile_commands.json，可能會極大地增加工程的複雜性和管理成本

  - 推薦做法，將內核源碼的放置在一個單獨的目錄中，`獨立編譯內核源碼後`，透過 ide 的 lsp-server 進行跳轉
    - 內核源碼下載，以 kernel-6.6.48 為例
      - [下載網站](https://www.kernel.org/)
      - [線上瀏覽](https://git.kernel.org/stable/h/v6.6.48)
      - [tarball下載](https://cdn.kernel.org/pub/linux/kernel/v6.x/linux-6.6.48.tar.xz)

      ```shell
      cd /usr/src
      wget https://cdn.kernel.org/pub/linux/kernel/v6.x/linux-6.6.48.tar.xz
      tar -xvf linux-6.6.48.tar.xz
      rm -rf linux-6.6.48.tar.xz
      ```
  
  - 推薦做法，將內核源碼的放置在一個單獨的目錄中，透過ctags|cscope建立索引後，透過 ide 的 ctag 進行跳轉
    - 注意，目前 helix-ide 尚不支援 ctags，而VIM內建支援CTAGS
    - 優點，使用此方法不需要內核源碼即可實現跳轉
    - 詳細範例見，[在gvim中使用ctags和cscope](../../sw/ctags_cscope.md#在gvim中使用ctags和cscope以linux-kernel為例)

- 配置代碼編輯器，以 helix 為例，[透過 clangd 可以實現代碼高亮、跳轉、查找](../../sw/helix/lsp-clangd.md)

- 完整代碼，[hello-mododule](./[example]%20hello-mododule/)

- 使用
  - 編譯成內核模塊，`$ make`
  - 安裝並掛載內核模塊，`$ insmod virtual_temp_sensor.ko`
  - 測試
    - 檢查是否被正確掛載，`$ dmesg | tail`
    - 讀取模塊內容，`$ cat /dev/vtempdev`
  - 卸載，`$ rmmod virtual_temp_sensor`

## 範例，使用 cmake 編譯 kernel-module

- 完整代碼，[hello-with-cmake](./[example]%20hello-with-cmake)
- [cmake的使用](https://gitlab.com/anthony076/prog-lang-study/-/blob/main/Compiler/compiler-cmake.md)

## 範例，實現簡單的字符設備

- 完整代碼，[char-temp-simulation]([example]%20char-temp-simulation/)

- 使用
  - 編譯成內核模塊，`$ make`
  - 安裝並掛載內核模塊，`$ insmod virtual_temp_sensor.ko`
  - 測試
    - 檢查是否被正確掛載，`$ dmesg | tail`
    - 讀取模塊內容，`$ cat /dev/vtempdev`
  - 卸載，`$ rmmod virtual_temp_sensor`

## 範例，在 ubuntu 上簡單測試

- 注意，編譯自定義的內核模塊前，需要編譯環境的配合，會`讀取編譯系統內核產生的庫、工具、設定` ... 等，
  
  因此，編譯自定義的內核模塊前，需要確保已經手動編譯過系統內核

- 建立內核模塊的專案目錄，`$ mkdir ~/hello`

- 進入專案目錄中，`$ cd ~/hello`

- 建立內核模塊源碼，`~/hello/hello.c`

  ```c
  #include <linux/init.h>   // for the macros
  #include <linux/module.h> // for all modules
  #include <linux/kernel.h> // for KERN_INFO

  MODULE_LICENSE("MIT");

  static int myinit(void) {
      printk(KERN_INFO "hello\n");
      return 0;
  }

  static void myexit(void)
  {
      printk(KERN_INFO "bye\n");
  }

  module_init(myinit);
  module_exit(myexit);
  ```

- 建立 makefile，`~/hello/Makefile`
  ```makefile
  PWD := $(shell pwd)

  #KERNEL_DIR = /usr/src/linux-5.15.0/
  #KERNEL_DIR = /usr/src/linux-headers-5.15.0-52-generic/
  
  # 注意，先手動編譯內核後，/lib/modules/$(shell uname -r)/build/ 目錄才會有效，否則是空的，無法編譯
  KERNEL_DIR = /lib/modules/$(shell uname -r)/build/  
  MODULE_NAME = hello

  obj-m := $(MODULE_NAME).o

  all:
    make -C $(KERNEL_DIR) M=$(PWD) modules
  clean:
    make -C $(KERNEL_DIR) M=$(PWD) clean
  ```

- 執行編譯，`$ sudo make`

## ref

- [linux driver 教學，含源碼解說](https://embetronicx.com/tutorials/linux/device-drivers/linux-device-driver-part-1-introduction/)

- [範例，在 alpine 上編譯內核模塊](https://www.cnblogs.com/Hakurei-Reimu-Zh/p/15669701.html)

- kernel-module with cmake
  - [cmake-kernel-module @ enginning](https://github.com/enginning/cmake-kernel-module)
  - [cmake-kernel-module @ thejjw](https://github.com/thejjw/cmake-kernel-module)
  - [將源碼放在src目錄](https://github.com/RomanSereda/cmake-kernel-module)
  - [提供各種高級功能](https://github.com/Kaffeine/cmake_kernel_module)
  - [cmake-kernel-module @ rtimricker](https://github.com/rtimricker/cmake-kernel-module)
  - [cmake-kernel-module-template](https://github.com/devalore/cmake-kernel-module-template)
  - [全手工](https://github.com/xuwd1/yet-another-cmake-kernel-module)