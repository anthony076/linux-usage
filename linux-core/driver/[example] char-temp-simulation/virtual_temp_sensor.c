
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/random.h>
#include <linux/device.h>

#define DEVICE_NAME "vtempdev"
#define CLASS_NAME "vtemp"

// 必要
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Your Name");
MODULE_DESCRIPTION("A simple virtual temperature sensor driver for Alpine Linux");
MODULE_VERSION("0.1");

static int major_number;
static struct class* vtemp_class = NULL;
static struct device* vtemp_device = NULL;

// 透過 cat /dev/vtempdev 會出發以下三個函數

static int device_open(struct inode *inode, struct file *file) {
    pr_info("VTemp: device opened\n");
    return 0;
}

static int device_release(struct inode *inode, struct file *file) {
    pr_info("VTemp: device closed\n");
    return 0;
}

static ssize_t device_read(struct file *filep, char __user *buffer, size_t len, loff_t *offset) {
    int errors = 0;
    char message[20];
    int temperature;
    
    get_random_bytes(&temperature, sizeof(temperature));
    temperature = (temperature % 50) + 10; // Random temperature between 10 and 60 degrees
    
    snprintf(message, sizeof(message), "Temperature: %d C\n", temperature);
    errors = copy_to_user(buffer, message, strlen(message));
    
    if (errors == 0) {
        pr_info("VTemp: Sent %zu characters to the user\n", strlen(message));
        return strlen(message);
    } else {
        pr_info("VTemp: Failed to send %d characters to the user\n", errors);
        return -EFAULT;
    }
}

static struct file_operations fops = {
    .open = device_open,
    .read = device_read,
    .release = device_release,
};


// 必要
static int __init vtemp_init(void) {
    pr_info("VTemp: Initializing the Virtual Temperature Sensor LKM\n");

    // 註冊 char 裝置
    major_number = register_chrdev(0, DEVICE_NAME, &fops);
    if (major_number < 0) {
        pr_alert("VTemp failed to register a major number\n");
        return major_number;
    }

    // 註冊裝置類別
    vtemp_class = class_create(CLASS_NAME);
    if (IS_ERR(vtemp_class)) {
        unregister_chrdev(major_number, DEVICE_NAME);
        pr_alert("Failed to register device class\n");
        return PTR_ERR(vtemp_class);
    }

    // 透過 kernel 創建裝置
    vtemp_device = device_create(vtemp_class, NULL, MKDEV(major_number, 0), NULL, DEVICE_NAME);
    if (IS_ERR(vtemp_device)) {
        class_destroy(vtemp_class);
        unregister_chrdev(major_number, DEVICE_NAME);
        pr_alert("Failed to create the device\n");
        return PTR_ERR(vtemp_device);
    }
    
    pr_info("VTemp: device class created correctly\n");
    return 0;
}

// 必要
static void __exit vtemp_exit(void) {
    device_destroy(vtemp_class, MKDEV(major_number, 0));
    class_unregister(vtemp_class);
    class_destroy(vtemp_class);
    unregister_chrdev(major_number, DEVICE_NAME);
    pr_info("VTemp: Goodbye from the Virtual Temperature Sensor LKM!\n");
}

module_init(vtemp_init);
module_exit(vtemp_exit);
