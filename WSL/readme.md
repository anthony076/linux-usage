## 自定義 wsl-distro

- 和[makeDistro](../makeDistro/overview.md)不同，自製wsl-distro`不需要`額外準備`bootloader`和`linux-kernel`，只需要 rootfs，並在其中安裝需要的套件和配置

  <font color=blue>不需要準備bootloader</font>
  
  傳統的Linux系統中，bootloader(如 GRUB)負責引導操作系統內核。

  在 WSL 中，Windows 直接管理和啟動WSL實例，通過調用 Windows 的子系統來啟動和管理Linux內核。

  <font color=blue>不需要準備linux-kernel</font>

  WSL 1 使用的是微軟實現的 Linux 兼容層，它不運行真正的 Linux 內核

  WSL 2 使用了真正的 Linux 內核，且該內核由微軟提供和維護，
  並且會隨著 Windows 更新自動更新

- 匯入 wsl-linux-distro 的兩種方式
  - 方法，`透過 Launcher.exe`
    - 工具，官方閉源的[WSL-DistroLauncher](https://github.com/Microsoft/WSL-DistroLauncher)

    - 工具，第三方開源的[wsldl](https://github.com/yuk7/wsldl)
      - 優，可以透過 Launcher <font color=blue>執行單次命令</font>
      - 優，提供更多配置選項，可輕鬆安裝和啟動各種 Linux 發行版
      - 優，持通過預構建的包或自定義的 rootfs tarball 創建和管理多個 WSL 實例
      - 優，可將現有的 VHDX 磁盤映像轉換為 WSL2 實例
      - 優，支持多種格式的安裝文件，包括 .tar.gz 和 .ext4.vhdx.gz，
      - 優，同時支持 WSL1 和 WSL2 版本
      - 優，允許更改默認用戶、調整路徑設置、切換默認終端類型，在不同實例之間進行備份和恢復

  - 方法，`透過 wsl --import <rootfs壓縮檔>`

## 自製 wsl-linux-distro 基本概念

- 狀況，透過 docker
  - step，建立 container
  - step，執行 container，`$ docker run -t centos bash ls /`
  - step，在 host 獲取 container-id，並添加到環境變數中
    > $ dockerContainerID=$(docker container ls -a | grep -i centos | awk '{print $1}')
  - step，將 container 匯出成tar檔
    - 格式，`docker export container-id > tar檔匯出路徑`
    - 命令，`$ docker export $dockerContainerID > /mnt/c/temp/centos.tar`

- 狀況，透過 qemu
  - step，透過 qemu 安裝 linux

  - step，將磁碟映像轉換為WSL相容格式，
    - WSL 需要 ext4 才能正確識別和運行 Linux 發行版
    - 命令，`$ qemu-img convert -f qcow2 -O ext4 source.qcow2 target.img`

  - step，將磁碟映像轉換為 wsl-linux-distro 的tar檔
    - 建立一個臨時的distro，用於掛載磁碟映像，`$ wsl --create temp-wsl-distro`
    - 將磁盤映像掛載到臨時的distro，`$ wsl --mount temp-wsl-distro C:/path/to/target.img`
    - 將臨時的distro導出為tar檔，`$ wsl --export temp-nios-distro C:/path/to/wsl-distro.tar`
    - 卸載臨時的distro，`$ wsl --unregister temp-wsl-distro`

  - step，導入自定義的distro，
    - 格式，`wsl --import 自定義distro名 安裝目的地 tar路徑`
    - 命令，`$ wsl --import MyNiosDistro C:/path/to/store/distro C:/path/to/nios-distro.tar`

- 比較，qemu和 docker 方法的區別

  wsl --import 期望的是一個完整的 Linux 發行版 tar 文件或 ext4.vhdx 文件

  tar 文件需要包含了一個完整的 Linux 發行版的文件系統結構和必要的套件，包括 
  - /bin、/etc、/home 等符合FHS的目錄
  - 必要的元數據和配置文件
  - 必要套件
  
  qemu的磁碟映像僅僅是一個獨立的文件系統，並不含完整的FHS結構，
  因次將qemu的磁盤映像打包為tar是無法使用的
  
  docker export 將指定的 container 匯出的tart已經是完整的FHS 
  
## 自製 wsl-distro 實際範例

- 範例，NixOS-wsl的使用
  - 注意，需要將 wsl 升級到最新版，可透過`$ wsl --update`進行升級，
    或透過[windows-update將wsl升級到最新版本](fix-windows-update-issue/01_before-windows-update.bat)

  - 下載tar檔，https://github.com/nix-community/NixOS-WSL/releases/latest
    - 適用於最新版的wsl，[nixos-wsl.tar.gz](https://github.com/nix-community/NixOS-WSL/releases/download/2311.5.3/nixos-wsl.tar.gz)
    - 適用於舊版的的wsl，[nixos-wsl-legacy.tar.gz](https://github.com/nix-community/NixOS-WSL/releases/download/2311.5.3/nixos-wsl-legacy.tar.gz)

  - 安裝，`$ wsl --import Nixos %USERPROFILE%\NixOS\ nixos-wsl-legacy.tar.gz`
  - 啟動，`$ wsl -d Nixos`
  - 進入nixos後，`$ sudo nix-channel --update`
  - 變更root密碼(預設以nixos登入)，`$ sudo passwd root`
  - [可配置選項](https://nix-community.github.io/NixOS-WSL/options.html)
  
- 更多實際範例
  - [自製wsl-alpine](build-wsl-alpine/readme.md)
  - [自製wsl-arch](build-wsl-arch/readme.md)
  - [自製wsl-nixos](../nixos/vm-container-iso/build-wsl-distro/readmd.md)
  - [透過rootfs建立最小的wsl-nixos](wsl-nixos-rootfs/readme.md)

## 常見問題

- 進入 wsl 後，windows-host 複製的文字貼到 wsl 的console後，會有文字缺失的問題

  此問題常見於安裝win32yank套件，`$ scoop install win32yank`，

  進入 wsl 後，在 /bin/sh 或其他shell中執行以下

  ```shell
  echo "alias pbcopy='win32yank.exe -i'" >> /etc/profile
  echo "alias pbpaste='win32yank.exe -o'" >> /etc/profile
  exit  # 關閉 wsl 後重啟，使配置生效
  ``` 

  退出後，在 windows-host 中
  - 關閉 vscode 的 terminal
  - 強制退出，`wsl -t Alpine`
  - 重新進入，`wsl -d Alpine`
