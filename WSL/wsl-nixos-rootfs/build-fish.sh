
#!/usr/bin/env bash

# manually add these two lines if need
#nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs
#nix-channel --update

mkdir -p etc bin usr/bin root/.config/fish

nix-env --profile root/.nix-profile -iA nixpkgs.coreutils nixpkgs.bash nixpkgs.fish nixpkgs.nix nixpkgs.which nixpkgs.gnutar nixpkgs.libuuid nixpkgs.ncurses nixpkgs.vim

export PATH=$(realpath -s root/.nix-profile/bin):$PATH

# 建立並配置bash
cp -P `which bash` bin/bash    # result :  bash -> /nix/store/5mh7kaj2fyv8mk4sfq1brwxgc02884wi-bash-5.2p37/bin/bash
echo "export PATH=/root/.nix-profile/bin:$PATH" > etc/profile

# 建立並配置fish
cp -P `which fish` bin/fish
echo "set -g PATH /root/.nix-profile/bin:$PATH" > root/.config/fish/config.fish

cp -P `which env` usr/bin/env

# 建立 root 帳號
echo "root::0:0:root:/root:/bin/fish" > etc/passwd
chmod 0644 etc/passwd

# 建立 fstab 檔案
touch etc/fstab

# 建立 wsl.conf 
cp wsl.conf etc/

# 建立檔案清單和目錄
nix-store -qR root/.nix-profile > files
echo bin >> files
echo usr >> files
echo etc >> files
echo root >> files

# 建立 tarball
cat files | xargs tar c > rootfs.tar
gzip rootfs.tar

rm -rf bin etc root usr files
