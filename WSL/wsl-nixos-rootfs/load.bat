@echo off

scp -P 2222 -r root@localhost:/root/wsl-nixos-rootfs/rootfs.tar.gz .
wsl --import mynixos . rootfs.tar.gz --version 2
wsl -d mynixos
