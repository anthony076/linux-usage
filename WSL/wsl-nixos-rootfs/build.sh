
#!/usr/bin/env bash

# manually add these two lines if need
#nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs
#nix-channel --update

mkdir -p etc bin usr/bin root

nix-env --profile root/.nix-profile -iA nixpkgs.coreutils nixpkgs.bash nixpkgs.fish nixpkgs.nix nixpkgs.which nixpkgs.gnutar nixpkgs.libuuid nixpkgs.ncurses nixpkgs.vim

export PATH=$(realpath -s root/.nix-profile/bin):$PATH
cp -P `which bash` bin/bash
cp -P `which env` usr/bin/env

# 建立 root 帳號
echo "root::0:0:root:/root:/bin/bash" > etc/passwd
chmod 0644 etc/passwd

# 建立 fstab 檔案
touch etc/fstab

# 建立 shell 的初始腳本
echo "export PATH=/root/.nix-profile/bin" > etc/profile

# 建立 wsl.conf 
cp wsl.conf etc/

# 建立檔案清單和目錄
nix-store -qR root/.nix-profile > files
echo bin >> files
echo usr >> files
echo etc >> files
echo root >> files

# 建立 tarball
cat files | xargs tar c > rootfs.tar
gzip rootfs.tar

rm -rf bin etc root usr files
