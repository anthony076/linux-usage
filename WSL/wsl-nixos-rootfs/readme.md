## 透過 rootfs 建立最小版本的 wsl-nixos

## 流程

- step，建立目錄
  - option，手動建立
  - option，透過命令`$nixos-install --root $root --no-root-passwd --system $nixosSystem --substituters "" --no-bootloader`
  
- step，安裝套件
  
  > nix-env --profile root/.nix-profile -iA nixpkgs.coreutils
    
  此命令會將套件安裝到 root/.nix-profile 目錄中，需要把此路徑添加到shell的配置文件中，
  才能使套件正常執行

- step，透過複製檔案建立配置文件
  - option，透過 `cp命令 + 手動設置權限`
  - option，透過 `install命令`
  
- step，進入偽系統執行命令
  
  若套件不是透簡單的指定位置和配置，而是必須透過執行命令才能完成配置時，
  就需要進入chroot來執行命令，例如

  `$ nixos-enter --root "$root" --command 'source /etc/profile && HOME=/root nix-channel --add https://github.com/nix-community/NixOS-WSL/archive/refs/heads/main.tar.gz nixos-wsl'`

- step，封裝為 tarball

## 使用方式

- 環境 : nixos in qemu-vm

- 在 nixos，執行 `build.sh` 會產出 `rootfs.tar.gz`

- 在 windows-host 中，
  - 執行 `load.bat` 會自動加載並運行 wsl-nixos
  - 退出 wsl-nixos 後，執行 `clean.bat`，會卸載 wsl-nixos 並刪除磁碟映像檔

- Issue

目前版本會有 fstab -a 掛載失敗的問題

## [fix] UtilTranslatePathList:2803: Failed to translate F:\Program Files\Microsoft VS Code\bin

需要在 wsl.conf 添加以下

```
[interop]
appendWindowsPath=false
```

## ref

- [NixWsl](https://github.com/marler8997/nix-notes/blob/5f39795a4b8bd9601e31ddb6937b17e0c61f7ba1/NixWsl.md)

- Failed to translate
  - https://github.com/microsoft/WSL/issues/10454
  - https://github.com/microsoft/WSL/issues/9252
  - https://github.com/microsoft/WSL/issues/9252#issuecomment-1405898498