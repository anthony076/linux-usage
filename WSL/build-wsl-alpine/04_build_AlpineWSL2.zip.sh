#!/bin/bash
set -e

mkdir ziproot

cp Launcher.exe ziproot/Alpine.exe
cp rootfs.tar.gz ziproot/

echo 'Building AlpineWSL2.zip'

cd ziproot
bsdtar -a -cf ../AlpineWSL2.zip *
cd ..