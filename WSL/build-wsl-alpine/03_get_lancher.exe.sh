#!/bin/bash
set -e

ICON_URL=https://github.com/yuk7/wsldl/releases/download/23051400/icons.zip
EXE_NAME=Alpine.exe

# 以下命令用於下載 icons.zip 文件，
# -L 表示跟隨重定向，如果服務器返回重定向響應，那麼 curl 會繼續請求重定向的地址
curl -L $ICON_URL -o icons.zip

echo 'Extracting Launcher.exe...'

# 以下命令用於解壓縮 icons.zip 文件，將 icons.zip 文件中的 Launcher.exe 文件提取出來
# 語法為，unzip 來源檔案 要解壓縮的文件名，注意，第二個參數若不指定，代表解壓縮所有文件
unzip icons.zip $EXE_NAME

# 重新命名 Alpine.exe 文件為 Launcher.exe
mv $EXE_NAME Launcher.exe
