## 使用方式

- 使用[AlpineWSL2](https://github.com/sileshn/AlpineWSL2/tree/main)做為執行環境

  在 wsl 中使用 docker 可以省去檔案複製和清理的麻煩

  下載[AlpineWSL2-20250101](https://github.com/sileshn/AlpineWSL2/releases/tag/20250101)後，

  ```bat
  @REM 將AlpineWSL2添加到wsl
  Alpine.exe  

  @REM 執行並進入 wsl-alpine
  wsl -d Alpine 

  @REM 依序執行以下

  @REM 安裝需要的套件和設置 docker
  @REM 由於 dockerfile 中無法執行啟動服務等命令，因此將設置環境的部分從 dockerfile 中抽離出來
  ./00_setup_env.sh

  @REM 建立 rootfs 需要基礎檔案，包含相關目錄|套件|檔案，並打包為 base.tar
  ./01_build_base.tar.sh

  @REM 建立 wsl 需要的相關目錄|套件|檔案，並打包為 rootfs.tar.gz
  ./02_build_rootfs.tar.gz.sh

  @REM 獲取 Alpine.exe，用於管理 wsl-distro 的 cli 工具 (選用，可以改用手動執行wsl命令替代)
  ./03_get_lancher.exe.sh

  @REM 將Alpine.exe和rootfs.tar.gz打包到同一個壓縮檔案中
  ./04_build_AlpineWSL2.zip.sh
  
  @REM 清理過程產物，僅保留AlpineWSL2.zip
  ./05_clean.sh
  ```
  

## ref

- [sileshn/AlpineWSL2](https://github.com/sileshn/AlpineWSL2/blob/main/Makefile)