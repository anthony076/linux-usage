#!/bin/bash
set -e

# 以下為在AlpineWSL中使用docker所需要的環境設置，
# - 在 AlpineWSL 中，使用 docker 建立 alpine-rootfs
# - AlpineWSL的使用，參考 [AlpineWSL](https://github.com/sileshn/AlpineWSL2)

# 在wsl-alpine中安裝docker
apk add docker libarchive-tools curl

# 啟動 openrc 
sudo openrc
sudo touch /run/openrc/softlevel

# 設置網路
# if [ ! -f /etc/network/interfaces ]; then echo "auto lo" | sudo tee -a /etc/network/interfaces; echo "iface lo inet loopback" | sudo tee -a /etc/network/interfaces;fi;
if [ ! -f /etc/network/interfaces ]; then
    echo "auto lo" | sudo tee -a /etc/network/interfaces
    echo "iface lo inet loopback" | sudo tee -a /etc/network/interfaces
fi

# 啟動網路服務
sudo rc-service networking start

# 啟動docker服務
sudo rc-service docker start

# 確保docker服務已經啟動
service docker status

#設定docker開機自動啟動
sudo rc-update add docker boot

#step，4. 測試docker
docker run hello-world

