#!/bin/bash
set -e

docker build -t alpinewsl .
docker run --name alpinewslContainer alpinewsl
docker export --output=base.tar alpinewslContainer

docker rm -f alpinewslContainer
docker rmi alpinewsl
