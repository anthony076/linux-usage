# 更新最快的5個鏡像
pacman-mirrors --fasttrack 5

# 初始化密鑰環
pacman-key --init

# 填充密鑰環
pacman-key --populate

# 啟用顏色顯示，禁用檢查空間
sed -ibak -e 's/#Color/Color/g' -e 's/CheckSpace/#CheckSpace/g' /etc/pacman.conf

# 禁用忽略包
sed -ibak -e 's/IgnorePkg/#IgnorePkg/g' /etc/pacman.conf

# 設置默認瀏覽器為wslview
echo 'export BROWSER="wslview"' | sudo tee -a /etc/skel/.bashrc >/dev/null 2>&1

# 啟用multilib倉庫
sed -i 's/#\[multilib\]/\[multilib\]/g' /etc/pacman.conf

# 包含multilib倉庫
sed -i '/\[multilib\]/ { n; s/#Include/Include/; }' /etc/pacman.conf

# 同步並更新所有包
pacman --noconfirm -Syyu

# 安裝所需的包
pacman --noconfirm --needed -Sy aria2 aspell base-devel bc ccache dconf dbus dnsutils dos2unix figlet git grep hspell hunspell hwdata inetutils iputils iproute2 keychain libva-mesa-driver libva-utils libxcrypt-compat libvoikko linux-tools lolcat mesa-vdpau nano ntp nuspell openssh pamac-cli procps socat sudo usbutils vi vim wget xdg-utils xmlto yay yelp-tools

# 下載WSL utilities的公鑰
wget https://pkg.wslutiliti.es/public.key

# 添加公鑰
pacman-key --add public.key

# 刪除公鑰文件
rm ./public.key

# 本地簽署公鑰
pacman-key --lsign-key 2D4C887EB08424F157151C493DD50AA7E055D853

# 添加WSL utilities倉庫
echo '[wslutilities]' | sudo tee -a /etc/pacman.conf >/dev/null 2>&1

# 設置WSL utilities倉庫地址
echo 'Server = https://pkg.wslutiliti.es/arch/' | sudo tee -a /etc/pacman.conf >/dev/null 2>&1

# 安裝wslu
pacman -Sy wslu

# 創建hooks目錄
mkdir -p /etc/pacman.d/hooks

# 允許wheel組的用戶使用sudo
echo '%wheel ALL=(ALL) ALL' > /etc/sudoers.d/wheel

# 啟用en_US.UTF-8本地化
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen

# 生成本地化信息
locale-gen

# 設置機器ID
systemd-machine-id-setup

# 刪除舊的DBus機器ID
rm /var/lib/dbus/machine-id

# 生成新的DBus機器ID
dbus-uuidgen --ensure=/etc/machine-id

# 確保DBus UUID生成
dbus-uuidgen --ensure

# 清理包緩存
yes | LC_ALL=en_US.UTF-8 pacman -Scc


