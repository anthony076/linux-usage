#!/bin/bash
set -e

echo 'Cleaning files...'

sudo rm -rf base.tar rootfs.tar.gz icons.zip Launcher.exe
sudo rm -r ziproot rootfs
