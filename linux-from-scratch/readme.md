
## ref 

- 範例，透過 docker 的 multi-stage 可以簡化 initramfs 的建構，減少編譯系統套件占用的空間，
  參考，[docker-to-initramfs](https://github.com/jqueuniet/docker-to-initramfs)

- 教學
  - [Read the Linux From Scratch Book Online](https://www.linuxfromscratch.org/lfs/view/stable-systemd/)

  - [Read the Linux From Scratch Book Online-zh](https://lfs.xry111.site/zh_CN/12.2/)

  - [系列-Build a LinuxFromScratch System，推薦](https://www.youtube.com/playlist?list=PLHh55M_Kq4OAPznDEcgnkQsbjgvG-QFBR)，STEP by STEP，

  - [系列-How to build your own Linux OS on a USB with Linux From Scratch](https://www.youtube.com/playlist?list=PLyc5xVO2uDsCCsisiiWrZJgnHmK18Mr7j)

- scripts
  - [lfs-scripts @ luisgbm](https://github.com/luisgbm/lfs-scripts)
  - [Shell scripts to build Linux From Scratch (version 7.8)](https://github.com/csusi/lfs)，
    依照篇章編寫 scripts
  - [Linux-From-Scratch @ viatsk](https://github.com/viatsk/Linux-From-Scratch)
  - [My Linux From Scratch scripts @ justanoobcoder](https://github.com/justanoobcoder/lfs)
  - [MulitLib-LFS](https://github.com/dslm4515/MultiLib-LFS)

- lfs on docker
  - [lfs-docker @ 0rland](https://github.com/0rland/lfs-docker/tree/master)
  - [lfs-docker @ pbret](https://github.com/pbret/lfs-docker/tree/master)
  - [推薦，lfs](https://github.com/reinterpretcat/lfs/tree/master)
  - [docker-lfs-build](https://github.com/EvilFreelancer/docker-lfs-build)
