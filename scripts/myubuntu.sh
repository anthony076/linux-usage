
# Test on WSL2 Ubuntu 22.04.1 LTS

sudo sed -i 's/archive.ubuntu.com/free.nchc.org.tw/g' /etc/apt/sources.list

sudo apt update && sudo apt upgrade -y
sudo apt install git tig tldr fzf fish ranger python3 python3-pip -y
sudo pip install bs4 lxml

cd ~

# ====
sudo usermod -s /usr/bin/fish ching
sudo sed -i '$a ching ALL=(ALL:ALL) NOPASSWD:ALL' /etc/sudoers

# ====
mkdir -p ~/.config/fish
touch ~/.config/fish/config.fish
echo 'set -g fish_greeting ""' > ~/.config/fish/config.fish

# ====
git clone https://github.com/chestnutheng/wudao-dict
cd ./wudao-dict/wudao-dict
sudo bash setup.sh

# ====
wget https://github.com/zellij-org/zellij/releases/download/v0.34.4/zellij-x86_64-unknown-linux-musl.tar.gz

tar -xvf zellij*.tar.gz
chmod +x zellij
sudo mv zellij /usr/bin/zellij
sudo sed -i '$a if status is-interactive\n  eval (zellij setup --generate-auto-start fish | string collect)\nend' ~/.config/fish/config.fish

sudo rm -rf zellij*.tar.gz
