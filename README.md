## 檔案系統架構

<img src="linux-檔案系統架構(FHS).png" width=100% height=auto>

## Linux 的使用

- [常用命令](usage/common.md)
- [shell-script 語法](usage/shell-syntax.md)
- [service 的使用](usage/service/service.md)
- [安全: 群組分類 + 身分驗證 + 授權管理](usage/security.md)

- [桌面系統基礎](usage/desktop/desktop.md)
  - [和wm搭配使用的套件](usage/desktop/desktop-wm-sw.md)
  - [基於x11架構的wm](usage/desktop/desktop-wm-x11.md)
  - [基於wayland架構的wm](usage/desktop/desktop-wm-waylang.md)

- [常用命令的使用](sw/)
  - [安裝 oh-my-zsh](sw/install_ohmyzsh.sh)
  
  - [安裝 broadway](sw/broadway/install_broadway.sh)
  
  - [安裝 novnc](sw/novnc/readme.md)

  - [遠端登入SSH](https://gitlab.com/anthony076/docker-projects/-/blob/main/ssh_proxy/doc/%E4%BD%BF%E7%94%A8SSH%E9%80%B2%E8%A1%8C%E9%81%A0%E7%AB%AF%E9%80%A3%E7%B7%9A.md)

  - [配置 ssh-x11](https://gitlab.com/anthony076/docker-projects/-/blob/main/ssh_proxy/doc/%E9%80%8F%E9%81%8Essh%E5%9F%B7%E8%A1%8Cgui%E7%9A%84%E6%87%89%E7%94%A8.md)
  
  - [推薦套件](sw/recommend-sw.md)

## 其他 Linux 的使用
- [archlinux相關](archlinux/readme.md)
- [alpinelinux相關](alpinelinux/readme.md)
- [kalilinux相關](kalilinux/readme.md)
- [linux on termux](linux_on_termux/readme.md)
- fedora

## 名詞解釋
- 塊設備與字符設備
  - 塊設備(block-device): 塊設備通過`系統緩存`進行讀取，不是直接和物理磁盤讀取
  - 字符設備(character-device): 字符設備可以`直接物理磁盤讀取`。不經過系統緩存。（如鍵盤，直接相應中斷）

## Linux 核心學習

- [Linux 核心的各種深度主題 @ OSDev.org](https://wiki.osdev.org/Expanded_Main_Page)
- [disk-partition，磁碟規劃](linux-core/00_disk-partition.md)
- [boot-up，開機流程](linux-core/01_bootup-flow.md)
- [kernel，內核基礎](linux-core/00_kernel.md)

- [grub，開機引導加載程序](linux-core/10_grub.md)
- [memory-manager，記憶體管理](linux-core/10_memory-manager.md)
- [scheduling，核心調度](linux-core/10_core-scheduling.md)
- [cgroup，用於控制可使用的硬體資源](linux-core/10_cgroup.md)
- [network，網路系統](linux-core/10_network.md)
- [process-manager，進程管理](linux-core/10_process-manager.md) 
- [ipc，進程通訊](linux-core/10_ipc.md)
- [thread-manager，線程管理](linux-core/10_process-manager.md) 

- [env-variable，環境變量基礎](linux-core/20_env-variable.md)
- [driver，驅動](linux-core/driver/driver-overview.md)
- [lxc，linux 虛擬機](linux-core/22_lxc.md)

## Ref 
- [Linux就該這麼學](https://www.linuxprobe.com/basic-learning-00.html)
- [一口Linux，带你入门嵌入式linux](https://www.zhihu.com/people/tudoujushi/posts)
- [Operating Systems - Lecture Notes](https://gyires.inf.unideb.hu/GyBITT/20/index.html)
- [linux內建工具源碼 @ util-linux](https://github.com/util-linux/util-linux)
- [linux教程](https://www.coonote.com/linux/linux-tutorial.html)
- [linux讀書會](https://hackmd.io/@combo-tw/Linux-%E8%AE%80%E6%9B%B8%E6%9C%83/%2F%40combo-tw%2FHkQIyUAHr)