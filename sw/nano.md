## nano 的使用
- 透過 nano 文件編輯， `$ sudo nano 檔案路徑`

- 刪除當前行，`ctrl+k`

- 刪除多行
  - 進入標記模式，`按住 shift 鍵` 或 `ctrl+6`
  - 執行刪除，`ctrl+k`

- 複製內容並貼上
  - 進入標記模式，`按住 shift 鍵` 或 `ctrl+6`
  - 執行複製，`alt+6`
  - 執行刪除，`ctrl+u`

- 搜尋
  - 進入查詢模式，`ctrl+q`
  - 跳到下一個結果，`alt+w`