## lf 的配置和使用

- 預設不支持圖片預覽，需要手動配置
  - 方法1: 使用 w3m + w3m-images
    - 配置簡單，使用複雜，需要按+鍵才能顯示圖片
    - 使用支援 w3m-image 的 gui-terminal: urxvt
		- 不支援 w3m-image 的 gui-terminal: st、alacritty
		- [install lf with w3m-image](https://youtu.be/QAzaWz2iRoA?t=167)

	- 方法2: 使用 uberzog
  	- 設置複雜，使用簡單，直接預覽，有BUG，只能瀏覽一次
		- 不需要找特定 terminal
		- [install lf with uberzog](https://www.youtube.com/watch?v=mM_OWfaKfcg)

	- 方法3: 使用 lfimg

## Ref

- [The lf File Manager is Ranger But Better](https://www.youtube.com/watch?v=EGBEIb2DgtQ)
