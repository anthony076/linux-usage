## ripgrep 的使用

- 語法，`rg 關鍵字 [路徑]`
  - 關鍵字不需要加引號
  - 若不提供路徑，預設遞歸搜尋當前路徑
  - 若當前目錄有 .gitignore，會根據 .gitignore 的內容忽略搜尋特定檔案

- 範例，`$ rg nix`，在當前目錄下，遞歸搜尋所有包含nix字串的文件

- 範例，`$ rg nix /foo`，在/foo目錄下，遞歸搜尋所有包含nix字串的文件

- 範例，`$ rg nix /foo/bar.txt`，在/foo/bar.txt文件中，搜尋所有包含nix字串的位置

- 範例，`$ rg nix | fzf`，在當前目錄下，遞歸搜尋所有包含nix字串的文件，並將結果顯示於 fzf 中