## ctags 和 cscope 的使用

- ctags 和 cscope 的差異
  - ctags 用於創建標籤文件，包含程序中各種標識符（如函數名、變量名等）的位置信息
    - 支持大量的編程語言
    - 生成一個靜態的標籤文件，需要手動更新
    - 通常與文本編輯器（如 Vim）集成使用
    - 速度較快
  
  - cscope 可查找定義、進行更複雜的搜索，如查找所有引用、定義、函數調用、文本字符串等
    - 主要針對C、cpp、java
    - 創建自己的數據庫，包含更詳細的代碼結構信息，可以在交互模式下動態更新其數據庫
    - 可以作為獨立程序使用，也可以與編輯器集成
    - 速度較慢

## ctags 的使用

- 常用參數
  - -R : 遞歸子目錄，將子目錄中的檔案也加入索引中
  
  - --languages : 針對特定的語言建立標籤，例如，`$ ctags -R --languages=C++,Python .`

  - --語言-kinds=+px : 例如，--c-kinds=+px，為指定語言額外生成特定標籤
    - +p : 包含函數原型/聲明
    - +x: 包含外部和前向聲明的變量 
    - 注意，預設 ctags 已經包括了類、函數定義標籤，此選項是進一步擴展了標籤的範圍
  
  - --extra : 項添加了額外的標籤條目
    - +q: 為每個類的成員函數添加一個額外的類限定標籤條目，
          除了常規的函數標籤外，還會添加一個包含類名的標籤（如 ClassName::memberFunction）

  - --fields : 生成包含`額外信息`的標籤文件
    - +i: 添加繼承信息
    - +a: 添加訪問（或導出）類型的信息（如 public, protected, private）
    - +l: 添加語言字段
    - +n: 添加行號
    - +S: 添加函數簽名
    - +z: 添加種類（如 function, variable 等）
    - 例如，例如，`$ ctags -R --fields=+l+n+S+z .`
  
  - --exclude : 排除特定目錄
    - 例如，`$ ctags -R --exclude=node_modules .`

- 範例
  - 當前目錄中，所有代碼文件都生成tags文件，`$ ctags -R`
  - 為特定文件生成 tags 文件，`$ ctags filename.c filename1.c file.h`
  - 為指定類型的文件生成 tags 文件，`$ ctags *.c *.h`

## cscope 的使用

- 常用參數
  - -R 或 --recurse : 遞歸子目錄，將子目錄中的檔案也加入索引中
  - -b : 只構建數據庫，不進入交互模式，例如，`$ cscope -R -b`
  - -i : 指定要添加數據庫的檔案列表，`$ cscope -b -i cscope.files`
  - -d : 進入交互模式，進入後不會建立新的數據庫，而是直接使用現有的數據庫
  - -q : 啟用快速符號查找，通常用於生成(帶有符號交叉引用)的數據庫
  - -f : 指定數據庫文件名
  - -k : 忽略內核頭文件目錄
  - -L : 在已建立數據庫中查找特定符號的全局定義
    - -0 : 查找符號
    - -1 : 查找全局定義
    - -2 : 查找函數調用
    - -3 : 查找函數被調用的地方
    - -4 : 查找文本字符串
    - -5 : 查找egrep模式
    - -6 : 查找文件
    - -7 : 查找包含這個文件的文件
    - -8 : 查找定義
    - -9 : 查找使用變量
    - 例如，`$ cscope -L -1 main`，查找 main 函數被定義的地方

## 在gvim中使用ctags和cscope，以linux-kernel為例

- 測試環境
  - windows 11 23H2 with hyperV enable
  - QEMU emulator version 8.2.0 (v8.2.0-12045-g3d58f9b5c5) 安裝版
  - Linux my-alpine 6.6.48-0-virt #1-Alpine SMP PREEMPT_DYNAMIC 2024-08-30 06:20:46 x86_64 Linux
  - 使用 gvim 進行開發 

- 下載linux-kernel源碼，並切換到該目錄中，以 kernel-6.6.48 為例
  - [下載網站](https://www.kernel.org/)
  - [線上瀏覽](https://git.kernel.org/stable/h/v6.6.48)
  - [tarball下載](https://cdn.kernel.org/pub/linux/kernel/v6.x/linux-6.6.48.tar.xz)

  ```shell
  cd /usr/src
  wget https://cdn.kernel.org/pub/linux/kernel/v6.x/linux-6.6.48.tar.xz
  tar -xvf linux-6.6.48.tar.xz
  rm -rf linux-6.6.48.tar.xz
  ```

- 安裝ctags、cscope，
  - [切換到 edge 套件庫](../../alpinelinux/readme.md#套件管理-apk-的使用)
  - `$ apk add ctags cscope`

- 建立數據庫
  - 建立 ctags 標籤檔，`$ ctags -R *`

  - 建立 cscope 數據庫，
    
    ```
    find . -name "*.c" -o -name "*.h" -o -name "*.S" > cscope.files
    cscope -b -q -k
    ```

- 啟動時指定 tag 文件的位置，`$ vim -t tag文件位置`

- 在 `~/.vimrc` 中設置 ctags
  - 範例，推薦使用，`set tags=./tags,tags`

  - 範例，`set tags=./tags,tags,$HOME`
    - `./tags`，在當前工作目錄查找名為 tags 的文件
    - `,tags`，在上一級目錄繼續查找
    - `,$HOME`，在$HOME和其上層目錄繼續查找
    - `,$HOME;`，只在$HOME中繼續查找
    - `;`，停止查找上層目錄或停止查找，
      注意，在添加分號;之後的所有路徑，都不會查找該路徑的上層目錄

  - 範例，`set tags=/home/d/code/tags`

- 在 `~/.vimrc` 中設置 cscope

  - `set cscopequickfix`，配置 cscope 的快速修復模式下的命令映射
    - s- : 顯示包含符號的文件名
    - c- : 顯示調用 cscope 的函數或變量的文件名
    - d- : 顯示定義文件
    - i- : 顯示包含 cscope 查找的文件
    - t- : 顯示包含 cscope 查找的文件名
    - e- :顯示 cscope 錯誤信息
  
  ```
  " 配置 cscope
  if has("cscope")
      set cscopequickfix=s-,c-,d-,i-,t-,e-  " 快速修復模式下的命令映射

      if filereadable("cscope.out")

          " 將生成的 cscope.out 文件添加到 cscope 數據庫
          " 或透過命令添加，$ :cs add cscope.out
          cs add cscope.out

          " 允許 :tag 命令使用 cscope 數據庫
          set cscopetag
          
          " 設置 cscope 的超時值為 0，不超時。用於提高 cscope 查找效率
          set csto=0
          
          " 顯示 cscope 的查詢結果，使 查詢結果以窗口的形式顯示
          set cst
      endif
  endif
  ```

- 使用
  - 推薦手動設置以下快速鍵，在 ~/.vimrc 中，
    ```
    nnoremap co :copen<CR>    " 開啟 quickfix 視窗
    nnoremap cc :cclose<CR>   " 關閉 quickfix 視窗
    nnoremap cp :cprev<CR>    " 跳轉到上一個結果
    nnoremap cn :cnext<CR>    " 跳轉到下一個結果
    ```

  - `<g]>` : 跳轉到光標位置的標籤
  - `<ctrl+o>`或`<c->p>`: 回到上一個結果
  - `<ctrl+i>`或`<c->n>` : 回到下一個結果

  - `:tag 符號` 或 `:ta <symbol>`： 使用標籤跳轉
    - 若只有一個結果會自動跳轉
    - 按下 space 進入輸入模式
    - 輸入 # 下的 id 後，會自動跳到該行

  - `:tselect 目標` 或 `:ts 目標` : 列出所有匹配的標籤

- `:cscope` 或 `cs` ： 開啟 cscope 功能選單
  - `:cs find d <symbol>` ： 查找被此函數調用的函數
  - `:cs find c <symbol>` ： 查找調用此函數的函數
  - `:cs find s <symbol>` ： 查找 C 符號
  - `:cs find g <symbol>` ： 查找定義
  - `:cs find t <symbol>` ： 查找文本字符串
  - `:cs find e <symbol>` ： 查找 egrep 模式
  - `:cs find f <symbol>` ： 查找文件
  - `:cs find a <symbol>` ： 查找此符號被賦值的地方

## ref

- [Use vim with ctags](https://kulkarniamit.github.io/whatwhyhow/howto/use-vim-ctags.html)
- [vim + ctags + cscope ](https://blog.csdn.net/weixin_42910064/article/details/113522043)
- [ctags的使用](https://www.cnblogs.com/leaves1024/p/9891103.html)