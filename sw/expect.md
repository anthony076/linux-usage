## expect 的使用

- 利用 echo 和 | 可以實現同樣的功能，[參考](common.md#利用-echo--e-和--實現互動命令的自動化安裝)

- 簡易使用
  - step1，安裝: `$ sudo apt install expect`
  - step2，錄製: `$ autoexpect 命令`
  - step3，回放: `$ expect 123.exp` 或直接執行 `$ ./123.exp`

- expect 語句的注意事項
  - -exact參數
  - 若預期字串中含有特殊符號，特殊符號需要加上 \，例如，[name] 要改成 \[name\]
    > 特殊符號列表 [ ]

- 注意，若手寫 expect ... send 有問題，可以透過 autoexpect 錄製，並檢查差異

## [範例] expect 用於互動式命令
- `<<EOF ... EOF`，使 expect 進入連續輸入模式，避免bash跳出
- `spawn`，利用 spawn 執行需要互動的程式
- `\r`，用於模擬Enter按鍵，實際輸入不包含\r
- `expect eof`，確認程式的輸出已結束再退出

- 代碼
  ```shell
  #! /bin/bash

  pw="useruser"

  /usr/bin/expect << EOF

  spawn sudo -u user /usr/bin/vncpasswd

  expect "Password:"
  send "$pw\r"

  expect "Verify:"
  send "$pw\r"

  expect "Would you like to enter a view-only password (y/n)?"
  send "n\r"

  EOF
  ```

## [範例] 特殊符號需要加上 \

```shell
#! /bin/bash

cmd_genssl="sudo openssl req -x509 -nodes -newkey rsa:2048 -keyout /etc/ssl/novnc.pem -out /etc/ssl/novnc.pem -days 365"

/usr/bin/expect << EOF

spawn $cmd_genssl

expect -exact "Country Name (2 letter code) \[AU\]:"
send -- "TW\r"

expect -exact "State or Province Name (full name) \[Some-State\]:"
send -- "\r"

expect -exact "Locality Name (eg, city) \[\]:"
send -- "Taichung\r"

expect -exact "Organization Name (eg, company) \[Internet Widgits Pty Ltd\]:"
send -- "NA\r"

expect -exact "Organizational Unit Name (eg, section) \[\]:"
send -- "NA\r"

expect -exact "Common Name (e.g. server FQDN or YOUR name) \[\]:"
send -- "user\r"

expect -exact "Email Address \[\]:"
send -- "123@example.com\r"

expect eof

EOF
```
