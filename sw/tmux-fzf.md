## tmux-fzf 的安裝與配置

## 透過 tpm 安裝 tmux-fzf

- 相關路徑和檔案
  - tmux配置文件，`~/.tmux.conf`
  - tmu 插件的安裝位置，`~/.tmux/plugins`

- step，下載TPM到特定位置，`$ git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm`
  
- step，建立 tmux配置檔，~/.tmux.conf，並輸入以下內容

  ```
  # ~/.tmux.conf

  set -g @tpm_plugins '          \
    tmux-plugins/tpm             \
    tmux-plugins/tmux-sensible   \
    tmux-plugins/tmux-resurrect  \
  '

  set -g @plugin 'sainnhe/tmux-fzf'

  TMUX_FZF_LAUNCH_KEY="f"

  TMUX_FZF_MENU=\
  "foo\necho 'Hello!'\n"\
  "bar\nls ~\n"\
  "sh\nsh ~/test.sh\n"

  run '~/.tmux/plugins/tpm/tpm'
  ```

- step，進入 tmux，使tmux重新加載配置檔內容，`$ tmux source ~/.tmux.conf`
  - 每次修改.tmux.conf後都需要重新加載
  - 在 shell 中輸入，不是在 tmux 的命令行中輸入
  - tmux命令，用於與 tmux-server 進行交互，tmux-server 必須進入 tmux 後才會啟動

- step，執行 tpm 安裝 : `按下 prefix (Ctrl-B) -> I (按下shift+i)`，若正常會看到以下輸出

  ```
  TMUX environment reloaded.
  Done, press ESCAPE to continue.
  ```

- step，按下 prefix (Ctrl-B) -> f  啟動 tmux-fzf 視窗

## ref

- [安裝tpm](https://github.com/tmux-plugins/tpm/)
- [安裝tmux-fzf](https://github.com/sainnhe/tmux-fzf)
- [tpm not working](https://github.com/tmux-plugins/tpm/blob/master/docs/tpm_not_working.md)