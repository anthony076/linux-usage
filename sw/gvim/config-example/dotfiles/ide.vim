
" 顯示一般行號
"set number

" 顯示相對行號
set relativenumber

" 輸入pattern時就進行即時搜尋，並高亮搜尋到的結果
set hlsearch 
set incsearch

" 啟用語法高亮
syntax on

" 啟用文件類型檢測, 根據副檔名自動啟用對應的插件
filetype plugin on

" 啟用縮排
set tabstop=2
set shiftwidth=2
set expandtab       " 將tab轉換為space

" 自動對齊縮排
set ai

" 啟用智能命令行, 啟用命令行的自動補全功能
set wildmenu

" 隱藏未保存的緩衝區
set hidden

" 設置忽略的文件類型
set wildignore=*.exe,*.dll,*.pdb

" 設置顏色主題
" 非預設主題，需要將 colorshema 檔案放在指定路徑中
" 在 windows 使用 gvim，指定路徑為 指定路徑為 C:\Program Files\Vim\vim91\colors
color monokai

" 設置游標所在行高亮顯示
set cursorline

" 游標所在行的背景設置為白色
highlight LineNr cterm=bold ctermfg=DarkGrey ctermbg=NONE       " 將行號設置為暗色
highlight CursorLine ctermfg=NONE ctermbg=NONE                  " 當前行前景為空，背景為空
highlight CursorLineNr cterm=bold ctermfg=green ctermbg=NONE    " 當前行的行號高亮為綠色 

" 啟用拼字檢查 (中文註解會被視為錯誤)
"set spell

" 在搜尋字串只有小寫時,搜尋結果忽略大小寫
set ignorecase

" 在搜尋字串含有大寫時,搜尋結果才區分大小寫
set smartcase

" 顯示狀態欄
set ruler

" 啟用 backspace
set backspace=2

" 啟用滑鼠選取
set mouse=a

" 設置GUI的字體大小
set guifont=Fixedsys:h20

" 顯示特殊符號
set listchars=space:.,tab:→\ ,nbsp:␣,trail:•,extends:⟩,precedes:⟨
set list

" split 預設開啟的位置
set splitright
set splitbelow