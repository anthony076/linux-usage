

" 設置快速鍵
nnoremap nl :set nohlsearch<CR>

" 小括號自動補全
inoremap ( ()<Esc>i

" 中括號自動補全
inoremap [ []<Esc>i

" 花括號自動補全
inoremap { {}<Esc>i

" 箭號自動補全
inoremap < <><Esc>i

" 代碼塊自動補全
inoremap {<CR> {<CR>}<Esc>ko<tab>

