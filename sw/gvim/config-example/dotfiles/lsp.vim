
function! s:on_lsp_buffer_enabled() abort
  " 將當前文件的自動補全函數，由vim-lsp的lsp#complete函數提供
  setlocal omnifunc=lsp#complete
  " 啟用當前文件的標記欄
  setlocal signcolumn=yes
endfunction

augroup lsp_install
  " 清除之前可能存在的同名自動命令組
  autocmd!
  
  " 設置自動命令
  " 當vim-lsp插件自定義的 lsp_buffer_enabled 事件觸發時，執行 s:on_lsp_buffer_enabled()     
  autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END

" =========================

let g:lsp_settings = {
\ 'pylsp-langserver': {
\   'config': {
\     'pylsp.plugins.pycodestyle.enabled': v:false,
\     'pylsp.plugins.pydocstyle.enabled': v:false,
\     'pylsp.plugins.pyflakes.enabled': v:false,
\     'pylsp.plugins.pylint.enabled': v:false,
\     'pylsp.plugins.mccabe.enabled': v:false,
\   }
\ }
\}

