
" 調用 vim-plug 提供的功能，用於指定vim-plug管理第三方插件的存放路經
" - 在 windows 上使用絕對路徑
" - 路徑中 \ 變更為 /
" - 路徑不要指定為 plugin，plugin 是vim內建插件存放的位置，此處是vim-plug插件存放的位置

" 透過 plug#begin() 可指定第三方插件保存的位置
" 若未指定，預設路徑為 C:\Users\ching\vimfiles\plugged

" 輸入:PlugInstall時，自動安裝以下插件
call plug#begin()

" ===== lsp 相關 =====
Plug 'prabirshrestha/vim-lsp' 
Plug 'mattn/vim-lsp-settings'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'

" ===========
" 提供 Markdown 語法 Highlight 和一些 Markdown 相關的拓展功能
Plug 'plasticboy/vim-markdown'

" 進行 vim-plug 的初始化
call plug#end()

