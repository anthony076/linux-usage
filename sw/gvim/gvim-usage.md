## gvim on windows

- 安裝與設置
  - [下載網址](https://www.vim.org/download.php)

  - 添加到環境變數，使 vim 生效
    - `HOME=C:\Users\使用者`
    - `path=C:\Program Files\Vim\vim91`

  - 預設使用者自定義的配置檔位置，
    - @Linux，`~/.vimrc`
    - @Windows，`~/_vimrc`

- 用戶自定義的 vim配置、插件、腳本和其他相關文件的預設位置
  
  - 要自定義任何配置、安裝任何插件都需要放在此目錄下

  - @Linux，
    - 路徑，`~/.vim`
    - 路徑，`~/.vim/vimfiles`

  - @Windows，
    - 路徑，`C:\Users\使用者\_vimfiles`
    - 路徑，`C:\Users\使用者\vimfiles`

- 基本概念
  - 緩衝區
    - 作用 : 在當前文件內容`寫入磁碟前`，用於暫存文件內容的內存區域稱為緩衝區，
      可視為是文件在保存之前的`臨時編輯環境`，或理解為`正在編輯的文件`

    - 透過 `:ls` 可以查看目前有那些緩衝區

      範例
      ```
      :ls
      1 #a   "Desktop\test.py"              line 0
      2 %a   "Desktop\test.c"               line 43
      ```
      - `1`：緩衝區編號，代表文件1
      - `#`：表示當前活動的緩衝區。
      - `%`：表示當前正在編輯的緩衝區。
      - `a`：表示緩衝區是通過編輯命令（如 :edit）打開的。目前處在 append 附加內容的狀態
      - `h`：表示緩衝區是幫助文件。
      - `+`：表示緩衝區是只讀的，不能進行編輯操作。
      - `*`：表示緩衝區已經被修改過，但是尚未保存。
      - `line 0`：顯示當前緩衝區的遊標位置。這裏是在第0行
    
    - 透過 `:setlocal` 可以設置當前緩衝區(當前的臨時編輯環境)的各種選項
      - 注意，`:setlocal`和`_vimrc`的功能類似，差別在:setlocal只對當前的緩衝區有效
      - 範例，`:setlocal expandtab`，設定縮進選項
      - 範例，`:setlocal autoindent`，啓用當前緩沖區的自動縮進功能
      - 範例，`:setlocal fileencoding=utf-8`，將當前緩沖區的文件編碼設定為 UTF-8
      - 範例，`:setlocal syntax=on`，啓用當前緩沖區的文法高亮
      - 範例，`:setlocal omnifunc=syntaxcomplete#Complete`，設定當前緩沖區的自動補全函數的內容，由第三方插件的syntaxcomplete#Complete()函數提供
      - 範例，`:setlocal signcolumn=yes`，設定當前緩衝區的 sign-column（標誌列）可見，並且只有當前視窗的標誌列有效，注意，行號列前方的區域才是標誌列
   
  - 暫存器(")

  - 巨集(#)
  
  - 標記('或meta)，用於指定位置跳轉
    - step1，在 normal-mode下，按下 `<m> -> <標記字母>`，例如，在 Line-17 的位置按下 `<ma>`
    - step2，在 normal-mode下，按下 `<'或meta> -> <標記字母>`，例如，
      - 狀況1，在 Line-36 的位置按下 `<'a>`，會立刻回到 Line-17 的行首
      - 狀況2，在 Line-36 的位置按下 `<meta> + <a>`，會立刻回到 Line-17，且最後出現的游標位置

    - 查看所有已經建立的標記，`:marks`
    - 只列出特定的標記，`:marks abc`
    - 刪除指定標記，`:delmarks <字母>`
    - 刪除部分標記，`:delmarks a-c`，刪除標記a、標記b、標記c
    - 刪除所有標記，`:delmarks a-zA-Z0-9` 或 `:delmarks!`
    
    - 快速鍵
      - `d'a`，刪除下方的內容，直到遇到 'a 的位置
    
    - 注意
      - 透過 marks 可以實現文件跳轉
  
  - 自動命令(autocmd)
    - 用途，

      用於`在特定的事件發生時`，自動執行指定的 Vim命令、Vim腳本、或其他第三方插件提供的功能，

      事件可以是編輯器本身的行為，也可以是用戶對文件的操作，比如打開文件、保存文件、進入插入模式等等

    - 格式 : autocmd [group，可省略] {events} {file_pattern} {command}

    - 常用的事件
      - BufRead：當文件被讀取到緩沖區時觸發
      - BufNewFile：當打開一個新文件時觸發，但該文件不存在於磁盤上
      - BufEnter：當緩沖區成為Active-buffer時觸發
      - BufLeave：當緩沖區不再是Active-buffer時觸發
      - FileType：當確定文件類型時觸發
      - Syntax：當啓用或禁用文法高亮時觸發
      - CursorMoved：當遊標移動時觸發
      - VimEnter：當 Vim 啓動完成後觸發
      - InsertEnter：當進入插入模式時觸發
      - InsertLeave：當離開插入模式時觸發
      - WinEnter：當視窗成為活動視窗時觸發
      - WinLeave：當視窗不再是活動視窗時觸發
      - User: 使用者自定義事件，見以下範例
  
    - 範例 : `autocmd FileType python setlocal expandtab shiftwidth=4 softtabstop=4`
      - event = FileType，代表打開文件事件被觸發時，執行上述命令
      - file_pattern = python，代表適用於 .py 的文件
      - command = setlocal expandtab shiftwidth=4 softtabstop=4
    
    - 範例 : 使用者自定義事件

      格式，`autocmd User 自定義事件名 {command}`
      
      ```vim
      " 建立自定義事件名的autocmd
      autocmd User MyCustomEvent call ShowMessage()

      " 定義一個函數來顯示消息
      function! ShowMessage()
          echo "Hello! This is a custom event message."
      endfunction

      " 按下 F5 觸發 MyCustomEvent 事件
      noremap <F5> :doautocmd User MyCustomEvent<CR>
      ```

  - 自動命令組(augroup)
    - 用途，是用來組織自動命令(autocmd)的
    - 格式，
    - 注意，自動命令組(augroup)不是由事件觸發的，必須依靠內部 autocmd 與事件綁定，或是在函數中間接調用

    - 範例，簡單範例

      ```vim
      " 創建一個名為 PythonSettings 的自動命令組
      augroup PythonSettings
          " 清除之前可能存在的同名自動命令組
          autocmd!
          
          " 當打開 Python 文件時，設定縮進選項
          autocmd FileType python setlocal expandtab shiftwidth=4 softtabstop=4
          
          " 當保存 Python 文件後，運行 Pylint 來檢查文法
          autocmd BufWritePost *.py silent! !pylint % 
          
          " 當進入 Python 編輯器時，啓用文法高亮
          autocmd BufEnter *.py syntax enable
      augroup END
      ```

    - 範例，錯誤的用法

      augroup 內部沒有任何 autocmd 的命令，因此 augroup 內的命令不會被執行，
      也沒有任何的函數手動調用augroup
      ```vim
      function! s:on_lsp_buffer_enabled() abort
          setlocal omnifunc=lsp#complete
          setlocal signcolumn=yes
          nmap <buffer> gi <plug>(lsp-definition)
          nmap <buffer> gd <plug>(lsp-declaration)
          nmap <buffer> gr <plug>(lsp-references)
          nmap <buffer> gl <plug>(lsp-document-diagnostics)
          nmap <buffer> <f2> <plug>(lsp-rename)
          nmap <buffer> <f3> <plug>(lsp-hover)
      endfunction

      augroup lsp_install
          call s:on_lsp_buffer_enabled()
      augroup END
      ```  

    - 範例，正確的用法

      以下的 augroup 可以透過以下兩種方式觸發
      - 1_F5
      - 2_BufEnter *.txt，開啟 txt 類型的文件時觸發
  
      ```vim
      " step1，自定义 augroup
      augroup MyGroup
          autocmd!
          autocmd BufEnter *.txt echo "You entered a text file."
      augroup END

      " step2，自定义函数，用于在按下 <F5> 键时触发自定义 augroup 中的命令
      function! MyCustomFunction()
          " 在这里执行你想要触发的命令
          " 比如，调用自动命令组中的命令
          doautocmd MyGroup BufEnter
      endfunction

      " step3绑定 <F5> 键触发自定义 augroup 中的命令
      nnoremap <F5> :call MyCustomFunction()<CR>
      ```

- [建立自己的自動補全](https://youtu.be/NUr-VvaOEHQ?si=bh1jGRgZ0D4osXse&t=725)

- <font color=red>注意</font>
  - gvim使用的語法不支援 lua，
  - 若有問題時，不容易找到解決方案，網路上關於gvim的資源太少，大部分是neovim的，請參考[遇到的問題](#gvim-on-wsl-ubuntu)
  - 部分distro不支援新版gvim，或者ubuntu，例如，ubuntu2204
  - 改用 neovim + gvim + vim-plug

## 快速鍵

- 基本概念
  - 快速鍵主要由三個區塊組成 : `[操作operation][移動方向motion]`或 `[操作operation][移動方向motion][目標target]` 
    - 例如，`<dw>` = [del刪除][word]，從當前游標位置開始，刪除整個單字
    - 例如，`<da">` = [del刪除][arround周圍的]["引號]，此命令會將最近的引號(含引號內文字)刪除
  
  - 透過`:help 快速鍵`或`:h 快速鍵`，可快速查詢快速鍵的功能
    - 例如，`:help j`，查詢 j 鍵的功能
    - 例如，`:h v_d`，查詢 visual-mode下 d 鍵的功能

- 切換不同的MODE
  - 預設使用NORMAL-MODE
  - 按下`<i>`，進入INSERT-MODE
  - 按下`<shit+v>`，進入VISUAL-MODE，並選取整行
  - 按下`<ctrl+v>`，進入VISUAL-MODE，選取當前位置

- 行內移動，需要在 common-mode 下
  - `<e>`，快速移動到下一個單詞的`字尾`或`特殊符號`，忽略空白，e for end
  - `<E>`，快速移動到下一個單詞的`字尾`，忽略特殊符號和空白
  - 
  - `<w>`，快速移動到下一個單詞的`字首`或`特殊符號`，忽略空白，w for whitespace or word
  - `<W>`，快速移動到下一個單詞的`字首`，忽略特殊符號和空白
  
  - `<0>`，快速移動到行首
  
  - `<shift> + <$>`，快速移動到行尾
  - `<shift> + <^>`，快速移動到行尾
  
  - `<f> + <使用者指定字母>`，快速往後移動到第一個指定字母的位置，注意，按下 `<;>` 可以直接跳下一個出現的位置
  - `<T> + <使用者指定字母>`，快速往前移動到第一個指定字母的位置，注意，按下 `<;>` 可以直接跳下一個出現的位置

- 段落移動
  - 跨頁面移動
    - `<ctrl+d>`，跨頁面快速移動游標，往下移動半頁
    - `<ctrl+u>`，跨頁面快速移動游標，往上移動半頁
    - 
    - `<gg>`，快速跳轉到頁首，或利用 `<1G>`也可以實現同樣的效果(需要開啟絕對行號)
    - `<G>`，快速跳轉到頁尾
    - `<數字G>`，快速跳轉到指定行，`<數字j>`，往下移動指定的行數，`<數字k>`，往上移動指定的行數

  - 當前頁面快速移動游標
    - `<shift + l>` ，將游標移動到當前頁面的下方1/3行處 (low)
    - `<shift + m>` ，將游標移動到當前頁面的中間1/2行處 (middle)
    - `<shift + h>` ，將游標移動到當前頁面的上方1/3行處 (high)
    - `H`，移動到當前頁面首行的第一個字符
    - `M`，移動到當前頁面中間行的第一個字符
    - `L`，移動到當前頁面首行的第一個字符

  - 透過括號移動
    - `<shift> + <{>`，往上移動到上一個空行
    - `<shift> + <{>`，往下移動到上一個空行
    - `<[> + <[>`，往上移動到前一個`{`的位置
    - `<]> + <]>`，往下移動到下一個`{`的位置
    - `<[> + <shift+(>`，往上移動到前一個`(`的位置
    - `<]> + <shift+)>`，往下移動到下一個`)`的位置
    - 快速移動到成對的括號中，`<shift> + <%>`，不限制哪一種符號，可行內移動，可段落移動

- 複製(yank)
  - 複製單字，`<yw>`
  - 複製整行，`<shift+v> + <y>`
  - 複製指定範圍，`<ctrl+v> + <y>`

- 搜索字串
  - `</字串>`，進行字串搜索，`<n>` 跳轉到下一個結果，`<N>` 跳轉到前一個結果
  
    - 小技巧，`</> -> <ctrl+r> -> <ctrl+w>`，會將當前游標的詞，快速複製到命令行
      - `ctrl+r`，代表進入暫存器(register)插入字串模式，進入後需要`選擇暫存器`或`選擇文字來源`
      - `ctrl+w`，代表代表當前游標所在的詞

      - 一般狀況下，`<y>`複製的內容會放入默認的 register 中，
        - 透過 `$ :register` 可以查看當前所有暫存器內容，
        - 暫存器以 " 開頭，有 "a ~ "z，""是默認的暫存器
        - 複製到指定的暫存器，`<"ay>`將選取的內容複製到 "a 暫存器
    
    - 小技巧，`<yw> -> </> -> <ctrl+r> -> <"">`，手動複製詞之後，在透過 ctrl+r 和 " 快速貼上到命令行
      - step1，`<yw>`，複製單字
      - step2，`</>`，進入搜尋模式
      - step3，`<ctrl+r>`，進入暫存器選擇模式
      - step4，`<">`，選擇預設暫存器

- 替換
  - 語法，`[選取範圍] [s] [/要搜尋的pattern] [/要替換的pattern] [/flags]`

  - 狀況，使用絕對行號，`:2,5s/12/34`，第二行到第五行中，將所有的12替換成34
  
  - 狀況，使用相對行號，`:.,5s/12/34`，當前行(.)到第五行中，將所有的12替換成34

  - 狀況，將選取範圍中的所有空白替換成空字串
    - `ctrl+v`，選取範圍
    - `:'<,'>s/\s\+//g`
      - `:'<,'>`，針對選取範圍
      - `s`，進行替換 (substitute)
      - `/\s\+`，/要搜尋的pattern，pattern為 `\s\+`，多個非字符的字元
      - `/`，/替換的pattern，此處pattern為空字串
      - `/g`，全局替換，代表替換在當前行找到的所有目標

- 在 VISUAL-MODE 下的常見操作
  - 按下`<v>`，從當前位置開始，選取`水平範圍內`的所有字符，至移動處為止
  - 按下`<shit+v>`，選取當前行，並選取移動範圍內的所有行
  - 按下`<ctrl+v>`，從當前位置開始，選取`垂直範圍內`的所有字符，至移動處為止

  - 考慮`("abcde")`
    - 按下`<v+a+w>`或`<v+i+shift+">`，選取 abcde，選取整個單字
    - 按下`<v+a+shift+">`，選取""內的所有單字，包含"
    - 按下`<v+a+shift+(>`，選取()內的所有單字，包含()

- 刪除
  - `<x>`，刪除當前游標的字符
  - `<daw>`，將當前位置所在的整個單詞刪除

  - `<dG>`，刪除以下所有內容，用於刪除整段內容
  - `<dgg>`，刪除以上所有內容，用於刪除整段內容
  
  - `<da">`，將最近的引號(含引號內文字)刪除
  - `<di">`，將引號內的文字刪除

  - 在 insert-mode 下
    - `<ctrl+w>`，刪除前一個單詞
    - `<ctrl+u>`，單除游標前的所有字符
    - `<ctrl+o>`，執行 normal-mode 的操作，但不跳出 inser-mode

- 修改
  - `<A>`，移動到當前行最後，並進入 insert-mode
  - `<I>`，移動到當前行第一個字符，並進入 insert-mode
  - `<o>`，跳到新行，並進入編輯模式，open-new-line

  - `<caw>`，將當前位置所在的整個單詞刪除，並進入insert-mode進行編輯
  - `<ci">`，將引號內的文字刪除，並立刻進入insert-mode進行編輯，change-inside-"
  - `<ci(>`，將引號內的文字刪除，並立刻進入insert-mode進行編輯，change-inside-(
  
- 縮進
  - `<=>`，設置當前行使用正確縮進
  - `<=gg>`，設置游標以上使用正確縮進
  - `<=G>`，設置游標以下使用正確縮進
  - `<<`，當前行向左縮進，`>>`，當前行向右縮進

  - 狀況，自動調整整份文件的縮進
    - `<gg>`，回到文件開頭
    - `<=G>`，設置整份文件使用正確縮進
  
  - 狀況，選取範圍使用縮進
    - `<shift+V>`，選取範圍
    - `<=>`，設置縮進

  - 狀況，將所有選取行，一次性向左縮進到行首
    - `<shift+V>`，選取範圍
    - `:left` 或 `:le`

  - 狀況，將所有選取行，一次性向右縮進到指定位置
    - `<shift+V>`，選取範圍
    - `數字>`，多次向右縮進

- 文件跳轉
  - 方法1，在當前視窗開啟
    - step1，將游標移動到檔案路徑的文字中，按下 `<gf>`
    - step2，回到原來的檔案，按下 `<ctrl+o>`，come back to original
  
  - 方法2，在新視窗開啟
    - step1，將游標移動到檔案路徑的文字中，按下 `<ctrl+w>`
    - step2，按下`<f>`

- 多視窗
  - 循環切換視窗，`<ctrl+w> + <w>`，將焦點切換至下一個視窗
  - 切換到右側視窗，`<ctrl+w> + <l>`，將焦點切換至右視窗
  - 關閉當前視窗，`<ctrl+w> + <c>`

- 實現多行同時編輯
  
  - 方法，選取多行，並在行首插入特定字串
    - `<shift+v>`，選取整行，並進入 VISUAL-MODE
    - `<G>` 往下選取所有，或`手動選取`範圍
    - 按下`:`，在命令行會自動補齊`:'<,'>`，代表所有選取的範圍 (只有在VISUAL-MODE下才會自動補齊)
    - 輸入，`:'<,'>normal I要插入的字串`，或使用簡化命令 `:'<,'>norm I要插入的字串`
      - normal 表示回到 NORMAL-MODE 後執行後方的命令
      - I代表要執行插入
    - 按下`<ENTER>`後，所有選取的行，都會自動插入輸入的字串
  
  - 方法，選取多行，並在行首插入特定字串
    - `<ctrl+v>`，選取單一字符，並進入 VISUAL-MODE
    - `手動選取`範圍
    - 按下`<I>`，會進入 INSERT-MODE
    - 按下`<ESC>`，退出 INSERT-MODE 後，上一步所有的修改，都會自動應用到所有行

  - 方法，選取多行，並在行尾添加;
    - `<ctrl+v>`，選取單一字符，並進入 VISUAL-MODE
    - `手動選取`範圍
    - 按下`<$>`，移動到行尾
    - 按下`<shift+a>`，進入INSERT-MODE，按下要添加的字串;
    - 按下`<ESC>`，退出 INSERT-MODE 後，上一步所有的修改，都會自動應用到所有行

- 實現數值增加/減少值鈕的功能
  - 將游標置於數值中的任一位置
  - `<ctrl+a>`，依序增加數值 (a for accumulate)
  - `<ctrl+x>`，依序減少數值 (x for reduce)

- 在VISUAL-MODE 中，按下`<o>`改變選取方向 (o for opposite)
  - 向下移動的狀態下，按下`<o>`，會將游標跳到選取範圍最上行的位置
  - 向上移動的狀態下，按下`<o>`，會將游標跳到選取範圍最下行的位置

- 選取代碼塊內的所有內容
  
  考慮以下內容
  ```
  {
    111
    222
    333
  }
  ```
  - 按下 `<v+i+shift+{>`，選取v 內部i 目標{，選取{}內部的所有行，不包含{}
  - 按下 `<v+a+shift+{>`，選取v 全部a 目標{，選取{}內部的所有行，包含{}

## 常用命令

- 注意
  - 輸入命令時，透過 `<tab>` 有命令的自動補全
  - 重複上一個命令，`<.>`

- 顯示行號
  - 方法1，顯示一般的行號，`$ :set number`
  - 方法2，顯示相對行號，`$ :set relativenumber`

- 文件列表
  - 顯示正在編輯的文件列表，`$ :args`
  - 對所有正在編輯的文件執行指定巨集，`$ :argdo 動作`
    - 例如，`$ :argdo $s/test/123/g`，對所有正在編輯的文件執行替換，將test替換成123
  
- 設置 readonly-mode
  - 以 read-only 模式檢視檔案，避免修改到檔案，可以在檔案開啟後，透過命令改成只讀模式
  - 方法1，`$ :view 檔案名`，
  - 方法2，`$ :set readonly`
  - 取消只讀模式，`$ :set noreadonly`

- quick-fix
  - 打開 quick-fix 視窗，`$ :copen`
  - 跳到下一個結果，`$ :cnext`
  - 跳到前一個結果，`$ :cprevious`

- 設置快速鍵
  - 格式，`{模式代號，v|n|i|c}noremap 按鍵組合 要執行的命令`，注意，特殊按鍵需要加上 <>
    - noremap，適用於所有模式
    - vnoremap，僅適用於visual-mode
    - nnoremap，僅適用於normal-mode
    - inoremap，僅適用於insert-mode
    - cnoremap，僅適用於command-mode

  - 例如，`noremap <F3> :set nohlhighlight`，在任何模式下，按下F3，會執行 :set nohlhighlight 的命令
  - 例如，`nnoremap nl :set nohlhighlight`，在normal-mode下，按下`<n> -> <l>`，會執行 :set nohlhighlight 的命令

- 查看變數值
  - 方法1，`:let`，打印所有變數
  - 方法2，`:echo g:lsp_format_sync_timeout`，打印指定的全局變數
  - 方法3，`:help 插件名`，查看該插件有哪些變數和函數
  
  - 各種變數類型
    - buffer-variable(b:)，代表 Local to the current buffer.
    - window-variable(w:)，代表 Local to the current window.
    - tabpage-variable(t:)，代表 Local to the current tab page.
    - local-variable(l:)，代表 Local to a function.
    - script-variable(s:)，代表 Local to a |:source|'ed Vim script.
    - global-variable(g:)，代表 Global.
    - vim-variable(v:)，代表Global, predefined by Vim.
    - function-argument(a:)，代表 Function argument (only inside a function).

- 在不離開 vim 的狀況下，調用外部命令(任何可在batch/shell中執行的命令)，`:!命令`，例如，`:!dir`

- 打開求助頁面，`:help 主題`
- 打開新聞健，`:new 文件名或路徑`，支援文件名的auto-complete
- 打開指定文件，`:edit 文件路徑`，或 `:e 文件路徑`
- 變更工作目錄，`:cd 目錄路徑`
- 跳轉的指定行，`:行號`

- 執行外部命令
  - 方法，`:!命令`，不打開 shell，而是 
    - 1，暫停正在編輯的文件
    - 2，在當前畫面打開shell(會覆蓋全部畫面)，並將使用者輸入的命令傳遞給 shell 執行
    - 3，使用者按下 Enter 後，立刻關閉 shell 並返回原來的 vim 畫面
    - 此方法常用於快速執行和檢視結果後，立刻返回原來的vim編輯畫面
 
  - 方法，`:shell`或`:sh`，會將 vim 編輯畫面推到背景，將顯示新打開的shell畫面 (暫時完全覆蓋vim編輯畫面)

  - 方法，`:term`，在vim編輯畫面中，以新的split分割視窗打開外部的 shell
    - 透過 `<ctrl+w> + <shift+n>`，可以將 terminal 變成 normal-mode後，可以複製 terminal 中的內容

- 寫入檔案，
  - 寫入所有開啟的檔案，`:wa`，write-all

- 緩衝區(buffer)
  - 列出緩衝區，`:ls` 或 `:buffers`
  - 刪除緩衝區，`:bd`
    - 需要檔案沒有變更才能執行，執行前需要先寫入檔案
    - 執行後，當前檔案的buffer檔(*.swap)會被刪除，當前檔案也會被關閉

## 配置顏色主題

- 下載[monokai](https://github.com/ku1ik/vim-monokai/blob/master/colors/monokai.vim)

- 複製檔案到
  - @Windows，`C:\Users\ching\vimfiles\colors\monokai.vim`
  - @Linux，

- 在vimrc中添加，`$color monokai`

## 在 gvim 中使用 ctag|cscope

參考，[ctags和cscope的使用](../ctags_cscope.md#在gvim中使用ctags和cscope以linux-kernel為例)

## 推薦的 .vimrc 內容

- [_vimrc](config-example/_vimrc)
- 透過 source 語句，將配置文件分類後，導入 _vimrc 檔案中

## [插件] 插件管理 vim-plug + LSP 安裝

- 以 `vim-lsp(server)` + `pyright-language-server(client)` 為例

- 套件管理，[vim-plug](https://github.com/junegunn/vim-plug?tab=readme-ov-file)

- 重要檔案路徑
  - 透過 LspInstallServer 安裝的server，會保存在 `~\AppData\Local\vim-lsp-settings\servers`
  - 手動配置 vim-lsp
    - 在vim中執行 `:LspSettingsLocalEdit` 後，會建立 `~\.vim-lsp-settings\settings.json`
    - 修改 settings.json

- <font color=red><b>step1</b></font>，安裝 vim-plug
  
  ```batch
  @REM for linux
  @REM curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

  @REM for windows
  curl -fLo ~/vimfiles/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  ```
  
  - 此命令會將 plug.vim 複製到 `C:\Users\ching\vimfiles\autoload\plug.vim`
  - plug.vim 提供 `plug#begin()`、`Plug`、`plug#end()` 等函數和指令在 .vimrc 中使用，
    或在vim中使用，例如，:PlugInstall
  - 加載 _vimrc 的內容後，第一次調用 plug.vim 提供的函數或命令時，才會加載 plug.vim 的內容

- <font color=red><b>step2</b></font>，修改 _vimrc，自動安裝套件

  在 _vimrc 中添加以下內容
  ```vim
    call plug#begin()   " 調用 vim-plug 提供的plug#begin()，用於指定插件位置

    " 執行:PlugInstall，會自動安裝以下插件
    Plug 'prabirshrestha/vim-lsp'
    Plug 'mattn/vim-lsp-settings'

    call plug#end()                   # 進行 vim-plug 的初始化

    " 設置自動命令

    function! s:on_lsp_buffer_enabled() abort
      " 將當前文件的自動補全函數，由vim-lsp的lsp#complete函數提供
      setlocal omnifunc=lsp#complete
      " 啟用當前文件的標記欄
      setlocal signcolumn=yes
    endfunction

    augroup lsp_install
      " 清除之前可能存在的同名自動命令組
      autocmd!
      
      " 設置自動命令
      " 當vim-lsp插件自定義的 lsp_buffer_enabled 事件觸發時，執行 s:on_lsp_buffer_enabled()
      autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
    augroup END  
  ```
  
  - 注意
    - plug.vim 放置在正確的目錄中，_vimrc 中調用 plug#begin() 等函數才會有效
    - 若 plug.vim 正確被載入
      - 進入 vim，不會有錯誤的提示
      - 可使用 :PlugInstall 的命令
    - 若 plug.vim 不正常工作，透過 `$vim -V20log`，會在當前目錄紀錄啟動流程發生的錯誤

- <font color=red><b>step3</b></font>，執行自動安裝
  
  進入 vim，輸入 `:PlugInstall`開啟介面後，vim-plug 會自動安裝_vimrc中指定的套件

- <font color=red><b>step4</b></font>，打開任意的 test.py
  - 進入後會提示 "Please do :LspInstallServer to enable Language Server pylsp-all"
  - 輸入`:LspInstallServer pylsp`後，等待安裝程序完成後，手動關閉新開的視窗 (pylsp-all會安裝太多不必要的功能，改手動安裝 pylsp)
  - 關閉後，重新進入 test.py
  - 注意，不推薦使用 pylsp，改用`:LspInstallServer pyright-langserver`問題較少

- <font color=red><b>step5</b></font>，使用自動補全

  - 注意，以下操作需要在insert-mode下進行
  - `<ctrl+x> -> <ctrl+o>`，啟用語法自動補全
  - `<ctrl+x> -> <ctrl+f>`，啟用檔案路徑自動補全
  - `<ctrl+x> -> <ctrl+s>`，啟用拼字自動補全，需要 `:set spell` 開啟拼字檢查功能
  - `<ctrl+x> -> <ctrl+l>`，自動補全輸入過的Line
  - 其他補全，可透過 `:help ins-completion` 查詢

- [issue] 為什麼不使用 pylsp
  
  在編寫 python 代碼時，出現了以下的提示
  
  ```
  W391 Blank line at end of file 的提示
  E305 expected 2 blank lines after class or function definition 的提示
  W293 blank line contains whitespace
  E303 too many blank lines
  ```
  
  透過下面的設置，都無法關閉這些提示
  - 設置1，`:help vim-lsp`，確認vim-lsp 沒有 `g:lsp_settings` 的全局變數
  - 設置2，`:help vim-lsp-settings`，確認有 `g:lsp_settings` 的全局變數，並提供設置範例

  參考資料
  - [Disable linting](https://github.com/python-lsp/python-lsp-server/issues/37)
  - [vim-lsp](https://github.com/prabirshrestha/vim-lsp)
  - [vim-lsp-settings如何註冊pylsp-all](https://github.com/mattn/vim-lsp-settings/blob/master/settings/pylsp-all.vim)
  - [pylsp可用的config值](https://github.com/python-lsp/python-lsp-server/blob/develop/CONFIGURATION.md)

- [issue] autocomplete 啟動緩慢的問題
 
  vim-lsp 會在開啟文件後才會掃描當前路徑下的所有檔案，
  因此，若是專案目錄中有很多檔案，就會造成autocomplete 啟動緩慢的問題

  例如，gvim 在 windows 下預設的路徑為`C:\Users\<使用者>`，
  若是在此路徑開啟特定檔案，例如，`C:\Users\usr> vim ~/Desktop/ttt.py`，
  
  就會造成 autocomplete 啟動緩慢的問題，因為需要掃描`C:\Users\usr`路徑下的所有檔案 

## [插件] 其他推薦插件

- 推薦插件
  - fzf
  - search
  - exploer
  - vim
    - vim-markdown，提供 markdown 的語法高量
    - vim-instant-markdown，提供 markdown 的文件預覽

- 推薦的 Snippet 插件
  - 'MarcWeber/vim-addon-mw-utils'
  - 'tomtom/tlib_vim'
  - 'garbas/vim-snipmate'
  - 'honza/vim-snippets' "massive common snippets

## Ref

- [vim插件查找與排名](https://vimawesome.com/)

- 配置範例
  - [vimrc @ gavinchou](https://github.com/gavinchou/vim_config/blob/master/_vimrc)

- lsp
  - [install lsp in vim](https://www.youtube.com/watch?v=hJCjb9dZjLY)

- markdown
  - [舒爽地編輯 Markdown 文件](https://fokayx.com/2018/01/21/markdown-extension-on-vim.html)
