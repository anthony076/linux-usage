
:'
  此 bash 需要複製 /share/bd.service 到指定位置
'
sudo apt update

# ==== install broadway ====

# gtk-3-examples 是測試用套件，用來執行 gtk3-demo
sudo apt install libgtk-3-bin gtk-3-examples

# 設置環境變數
# 注意，若不是使用 bash，依需要改成實際的shell設置檔
sed -i '$a export GDK_BACKEND=broadway' ~/.basrc
sed -i '$a export BROADWAY_DISPLAY=:5' ~/.bashrc

# ==== add broadway to user-service ====

# 建立目錄
mkdir -p ~/.config/systemd/user

# 建立 bd.service
cp /share/bd.service ~/.config/systemd/user/bd.service

# 添加到 user-service
systemctl --user enable bd.service

sudo reboot now

# 測試
#   ssh 登入遠端主機後，執行 $ gtk3-demo
#   在本機上使用 http://127.0.0.1:8085 瀏覽結果