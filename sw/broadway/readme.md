## broadway

- 在瀏覽器中顯示遠端桌面

- 使用限制
  - 限定使用 gtk-library 寫的GUI應用，若不是 gtk 的應用，仍然無法顯示
  - broadway 不是基於 session 的，同一個帳號的多名連線，都共享同一個畫面
  - broadwayd 命令 和 GDK_BACKEND 命令必須是同一個使用者執行
  - 只能針對單一應用，不能顯示整個桌面
  - 若在系統上同時安裝 x11 和 broadway，會優先使用 x11

## broadway 的使用 

注意，此方法不需要透過 ssh -Y 進行連線，在 
broadway 自身就是 display-server，提供 webUI 供外界存取，
而不是像 x11 需要在 SSH-Client(X-Server) 上安裝 xserver(xming)

```shell
step1 設置連接埠轉發

# for gui application demo
sudo apt install gtk-3-examples

# for broadwayd command
sudo apt install libgtk-3-bin

# start broadwayd-server @ localhost:808x   x=$BROADWAY_DISPLAY
# set $BROADWAY_DISPLAY to :5
broadwayd :5    

# 其他參數
#   全域選單關閉，UBUNTU_MENUPROXY= 
#   全域scrollbar關閉，LIBOVERLAY_SCROLLBAR=0

# ==== 執行GUI應用方法1 ====
GDK_BACKEND=broadway BROADWAY_DISPLAY=:5 應用名

# ==== 執行GUI應用方法2 ====
# 將參數 export 到環境變數後，直接執行 應用名
export GDK_BACKEND=broadway
export BROADWAY_DISPLAY=:5
應用名

# ==== 執行GUI應用方法3 ====
# 執行GUI應用，方法3，將變數添加到 ~/.bashrc 中
# 注意，不推薦 將 broadwayd 的命令添加到 .bashrc 中，不好管理

sudo nano ~/.bashrc
export GDK_BACKEND=broadway
export BROADWAY_DISPLAY=:5
應用名

#或在應用名後加上 &，避免 shell 阻塞
應用名 &

# 透過 http://127.0.0.1:8085 存取
```

## 開機自動執行 broadway 的兩種方法

- 方法1，添加到 user-service，推薦，方便使用 systemd 進行管理
  - [為什麼要添加號 user-service 而不是 system-service](../../service/user-service.md#範例-需要使用-user-unit-的範例以-broadway-為例建立-broadway-的使用者服務)

  - [完整代碼](install_broadway.sh)

  - `1-1`，建立 `/home/user/.config/systemd/user/bd.service`
    ```shell
    [Unit]
    Description=broadwayd-service

    [Service]

    Type=simple

    ExecStart=/usr/bin/broadwayd :5 &
    ExecReload=kill $MAINPID && /usr/bin/broadwayd :5 &
    ExecStop=kill $MAINPID

    [Install]
    WantedBy=default.target
    ```

  - `1-2`，添加到 user-service

    執行 `$ systemctl --user enable bd.service`，會在 `/home/user/.config/systemd/user/default.target.wants 的目錄`中，

    建立 `bd.service 的軟連結`，並指向` /home/user/.config/systemd/user/bd.service` 的檔案

  - `1-3`，添加全局的環境變數，`$ sudo nano ~/.bashrc`，添加以下內容

    注意，若登入時不是 bash 而是其他 tty，例如，使用zsh，就要改設置 ~/.zshrc

    添加以下
    `$ sed -e '$a GDK_BACKEND=broadway' ~/.bashrc`
    `$ sed -e '$a export BROADWAY_DISPLAY=:5' ~/.bashrc`
    ```shell
    export GDK_BACKEND=broadway
    export BROADWAY_DISPLAY=:5
    ```

  - `1-4`，測試
    - $ sudo reboot now
    - 利用 ssh 登入
    - $ gtk3-demo
    - 在瀏覽器上輸入，http://localhost:8085，並察看結果


- 方法2，添加以下到 ~/.bashrc

  ```shell
  export GDK_BACKEND=broadway
  export BROADWAY_DISPLAY=:5

  if ! $(pidof broadwayd > /dev/null)
  then
    broadwayd :5 > /dev/null 2>&1 &
  fi
  ```

## Ref
- [在 Ubuntu 安裝 Broadway Backend，在 HTML5 瀏覽器中操控 GTK3 應用程式](https://blog.gtwang.org/linux/ubuntu-broadway-backend-html5-gtk3/)

- [GTK](https://wiki.archlinux.org/title/GTK)

- [Run GTK applications within a browser](https://blog.desdelinux.net/en/broadway-runs-gtk-browser-apps/)

- [Using GTK with Broadway](https://docs.gtk.org/gtk3/broadway.html)

- [Broadway: Run GTK applications within a browser](https://blog.desdelinux.net/en/broadway-runs-gtk-browser-apps/)
  