<<comment
  # install.sh 提供2種非互動式的安裝 (詳見install.sh @ line:363)
  
  # 方法1，設置環境變數CHSH為no，
  #     export CHSH=no，或 sh install.sh --skip-chsh，代表不會將預設的shell替換為zsh

  # 方法2，變更還變數SHELL值
  #     $SHELL 是使用者登入預設的shell
  #     若 basename -- $SHELL 的結果為 zsh，安裝程序會跳過互動式詢問
  #     (不推薦使用) 手動修改，安裝 zsh 後，執行 export SHELL=/usr/bin/zsh，
  #     (推薦使用)   wget https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O - | zsh

comment

sudo apt install zsh -y
#sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O - | zsh

sudo git clone https://github.com/caiogondim/bullet-train.zsh.git
cp bullet-train.zsh/bullet-train.zsh-theme ~/.oh-my-zsh/themes
sed -i 's/robbyrussell/bullet-train/g' ~/.zshrc

git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
sed -i 's/git/git zsh-autosuggestions/g' ~/.zshrc

rm -rf bullet-train.zsh

echo  ===== oh-my-zsh setup done, please reopen the shell ======