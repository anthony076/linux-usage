## cut 移除不必要的欄位

- 常用參數
  - d 指定分隔符號 (delimiter)
  - f 選擇欄位 (fields)
    - 數值從1開始
    - 例如，-f1 或 -f2,3

## 範例

- 範例，指定分隔符號，並選擇指定欄位

  ```shell
  getent passwd root | cut -d: -f6    # 印出 /root
  ```

  - `getent passwd root` 印出 `root:x:0:0:root:/root:/bin/bash`
  - `-d` 以:為分隔線，得到 `root   x   0   0   root   /root   /bin/bash`
  - `-f` 選擇第6欄，得到 `/root`
