## dd 和 fallocate
- dd 和 fallocate 都可以用於創建文件，而不佔用硬碟空間
  
  兩者可以快速分配磁盘空间，而不是通過寫入數據來分配空間，可以提高磁盘 I/O 性能

- dd : 
  - 在某些檔案係統上，dd 的效率可能不如 fallocate，例如，tmpfs 或 不支援隨機寫入的檔案係統
  - dd 需要在記憶體中創建一個大小為文件大小的緩沖區，並將其寫入磁盤，
  - dd 需要更多的記憶體和 CPU 佔用

- fallocate:
  - 可以在文件係統上直接分配或刪除磁盤空間。
  - 主要用於在不需要寫入數據的情況下，快速創建大小固定的文件。
  - 直到寫入數據前，分配的空間不佔用實際磁盤空間，適合創建虛擬磁盤鏡像或其他類似的文件  
  - FAT 和 NTFS 不支援 fallocate 命令 (基於日誌的文件係統或支援空間預分配的文件係統都支援 fallocate 命令)

## dd 的使用

- 使用 dd 命令時，會覆蓋寫入位置內已經保存的數據，常用於清理磁碟空間

- dd 的效率高，可以直接訪問硬碟，並且可以指定塊大小，使得dd在清理硬碟時非常高效

- 範例，利用 dd 清除硬碟分區
  ```shell
  # 清空硬碟上的所有數據 (清除 /dev/sda 上的 magic-string 和 )
  #     if：輸入文件，/dev/zero是特殊文件，每次讀取其內容時都會得到 0 值
  #     of：輸出文件。此處為 /dev/sda，這是一個硬碟設備文件
  #     bs：設置讀取塊的大小
  #     count：讀取塊的數量
  
  #     從 /dev/zero 讀取2048塊0數據塊，每個數據塊大小為512 bytes的0，
  #     並將其寫入 /dev/sda 這個硬碟設備中。這樣就會清空 512*2048 = 1048576 = 1MB 的空間
  /usr/bin/dd if=/dev/zero of=/dev/sda bs=512 count=2048
  ```

- 範例，利用 dd 清理 10G 大小的磁碟空間
  - 10G = 10 * 1024 * 1024 * 1024 = 10,737,418,240 bytes
  - 1 count = 512 bytes
  - 10G = 10,737,418,240 bytes / 512 bytes = 20,971,520 count
  - 命令，`$ dd if=/dev/zero of=/dev/sda bs=512 count=2097150`

- 範例，清除整個硬碟，不需要指定 bs 和 count
  - 命令，`$ dd if=/dev/zero of=/dev/sda conv=notrunc,noerror`
  - conv=notrunc，如果輸出文件已經存在，不要截斷它
  - conv=noerror，如果遇到讀寫錯誤，不要終止程式

- 範例，用於減少 vagrant box 的大小 
  - 命令，`$ dd if=/dev/zero of=/EMPTY bs=1M`
  - 命令，`$ rm -rf /EMPTY`
  - 注意，若不指定 count 參數，有可能會遇到 dd 拋出空間不足的錯誤，
    改用以下方式動態偵測剩餘空間，在執行 dd 可避免
    ```shell
    # 取得 /dev/sda3 剩餘的 1M-block 的數量
    COUNT=$(df -m | grep /dev/sda3 | awk '{printf $4}')
    
    dd if=/dev/zero of=/EMPTY bs=1M count=$COUNT
    rm -f /EMPTY
    ```

  - 和清理整個硬碟不同，此命令不會覆蓋已經儲存數據資料的磁碟空間
    
    使用 dd 命令將空間全部清為零可以確保映像中不會保留任何敏感資訊或其他數據。
    此外，在映像建立完成後，這些空間將被壓縮或刪除，因此最終的映像將更小

- 範例，建立磁碟映像檔
  - 命令，$ dd if=/dev/sda of=sda.img

## fallocate 的使用
- 使用參數
  - -a: 分配空間 (allocate)
  - -d: 刪除空間 (delete)
  - -p: 預分配空間 (pre-allocate)
  - -s: 查詢文件大小 (size)

- 範例，在/data/test.img文件中分配1GB的空間，`$ fallocate -l 1G /data/test.img`