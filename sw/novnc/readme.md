
## NoVNC 
- 架構 

  <img src="doc/communication-flow.png" width=800 height=auto>

  - `組件1`，Display-Manager
    - 用於 系統啟動開載入 Desktop 系統、切換/管理桌面系統
    - 選擇 slim、lightdm、gtm3
    - 注意，Desktop 的套件包會安裝預設的 display-manager，若需要替換才需要手動安裝

  - `組件2`，Desktop 的選擇:
    - xfce4: 輕量且功能完整
    - lxde: 利用 gtk 寫的 Desktop，最輕量，但功能不如 xfce4 多
    - lxqt: 使用最新的 qt 編寫，會逐漸轉移到 lxqt，lxqt 比 lxde 更輕巧

  - `組件3`，VNC-server 的選擇: vnc4server、TightVNC、TigerVNC
  - `組件4`，Websockify: 用於將 noVNC 輸入的 websocket 流量，轉換為 TCP 流量給 VNC-server
  - `組件5`，noVNC: 實際上是 WebSocket-server，接收客戶端輸入的流量，並傳遞給 Websockify
  - `組件6`，Client: 用戶透過瀏覽器走 http 與 noVNC 的 WebSock-server 進行交握和身分驗證，驗證後轉換為 WebSocket 通訊

- 安裝
  - `step1`，Display-manager 和 Desktop 的安裝
    - 用於切換不同的桌面系統，可以不必安裝 ?

    - 選項1，slim，`$ sudo apt install slim`
    - 選項2，lightdm 安裝，`$ sudo apt install lightdm`
    - 選項3，lxdm 安裝，`$ sudo apt install lxdm`
    - 選項4，gdm，不推薦，體積占用大，啟動慢
    - 選項5，kdm，不推薦，體積占用大，啟動慢

  - `step2`， Desktop 的安裝
    - 選項1，lxde 最小安裝，`$ sudo apt install lxde-core --no-install-recommends`
    - 選項2，gnome，`$ sudo apt install ubuntu-desktop`
    - 選項3，kde-plasma，`$ sudo apt install kde-plasma-desktop`
    - 選項4，xfce4，`$ sudo apt install xfce4-session xfce4-goodies`
    - 選項5，mate，`$ sudo apt install ubuntu-mate-desktop`
    - 安裝後 reboot，會自動進入 Desktop
    
  - `step3`，VNC-server 的選擇
    - [TightVNC](https://www.serverself.com/setup-lubuntu-tightvnc-and-novnc-for-a-vps/)
    - [x11VNC](https://www.footmark.com.tw/news/embedded-systems/raspberry-pi/ubuntu-server-x11vncr-raspberry-pi/)
    - [TigerVNC](https://www.796t.com/content/1544517424.html)

  - `step4`，novnc 和 websockify 的安裝，`$ sudo apt install novnc websockify`

## [範例] vagrant + ubuntu + slime + lxde + tightvnc

- step1，在 vagrantfile 中，設置 port-forwarding
  - 選用: `config.vm.network :forwarded_port, id: "vnc", guest: 5901, host: 5901`
  - 必要: `config.vm.network :forwarded_port, id: "novnc", guest: 6080, host: 6080`

- step2，登入vm系統並進行安裝

  ```shell
  sudo apt update

  # display-manager 推薦使用slim，
  #   lxde-core 預設安裝 lightdm，並且會建立 lightdm 帳號，需要額外設定才能登入
  #   lightdm 配置，詳見 https://wiki.archlinux.org/title/LightDM#Enabling_autologin
  #   lightdm 配置，詳見 https://github.com/Canonical/lightdm/blob/main/data/lightdm.conf
  #   安裝登入套件，詳見 https://www.addictivetips.com/ubuntu-linux-tips/set-up-lightdm-on-arch-linux/
  #sudo apt install xorg lxde-core

  sudo apt install xorg slim lxde-core

  sudo apt install tightvncserver

  # 產生配置並啟動 vnc-server (自動背景執行)
  # :1代表DISPLAY值
  vncserver -geometry 800x600 :1

  # ---- 非必要，手動配置vnc -----
  # 非必要，停止 vnc-server 以手動進行配置
  vncserver -kill :1

  # 非必要，編輯配置文件
  sudo nano ~/.vnc/xstartup
  
  # 非必要，添加以下
  export XKL_XMODMAP_DISABLE=1  # 非必要，解決連線時的灰屏問題
  xsetroot -solid grey          # 非必要，設置根視窗的背景為灰色
  lxterminal &                  # 非必要，在背景執行 lxterminal
  openbox &                     # 非必要，在背景執行 openbox，使 openbox 作為x視窗的管理器
  /usr/bin/lxsession -s LXDE &  # 非必要，使用 lxsession 管理 lxde 和 x11 之間的連線

  vncserver -geometry 800x600 :1  # 完成配置後需要手動重新啟動vncserver

  # 測試 vnc
  # 在本機上，可透過 vnc-viewer，連線到 127.0.0.1:5900+1 進行連線
  # 在手機上，可透過 vnc-viewer，連線到 本機ip:5900+1 進行連線

  # 安裝 novnc、websockify
  sudo apt install -y install novnc websockify

  # 建立公私鑰

  #變更公私鑰存取權限
  sudo chmod 644 /etc/ssl/novnc.pem

  # 啟動 websockify-server (自動背景執行)
  #   -D 指定 vnc.html 的目錄
  #   --cert 指定公私鑰的位置
  #   6080 輸入的端口位置   
  #   localhost:5901 輸出的端口位置(指vncserver的通訊位置)
  websockify -D --web=/usr/share/novnc/ --cert=/etc/ssl/novnc.pem 6080 localhost:5901

  # 測試 novnc
  # 若啟動命令為 `$ vnc-server :1`，則 DISPLAY值=1
  # 在主機上輸入 `http://127.0.0.1:8500+DISPLAY值`，例如，`http://127.0.0.1:8505`
  ```

- (選用) 透過 snap 添加為 service
  - 安裝 novnc，`$ sudo snap install novnc`
  
  - 查看 service 的狀態，`$ sudo snap services`
    ```shell
    Service          Startup  Current   Notes
    # 預設 snap 安裝的套件都會自動生效，不需要額外執行 enable 指令
    novnc.novncsvc   enabled  active    -
    ```
  
  - 使應用生效，`$ sudo snap disable novnc`
    
    執行後，novnc 移出 /snap/bin/ 的目錄

  - 使應用失效，`$ sudo snap enable novnc`

    執行後，novnc 移入 /snap/bin/ 的目錄
  
  - 設置開機啟動 (設置應用程式的啟動參數)
    - 注意，只有應用程式生效(enable)，且透過 set 設置啟動參數的應用，才會開機自動啟動

    - 定義應用參數的語法，`sudo snap set 應用名 參數名=參數值`
      - [注意] 每個應用的參數名，可`自訂前綴`以避免參數重複
      - [注意] 部分應用的`參數名有限制`，參考各應用程式的文檔
      - [注意] 只有透過 `set` 設置啟動參數的應用，才會`開機自動啟動`

      - [語法] 參數名命名語法，`自定義名稱1.自定義名稱2.參數名`

      - [例如] 定義 novnc 應用的 listen 參數，`$ sudo snap set novnc services.n6080.listen=6080`，用於瀏覽器連接埠
      - [例如] 定義 novnc 應用的 vnc 參數，`$ sudo snap set novnc services.n6080.vnc=5901`，用於VNC連接埠

  - 查詢應用程式的啟動參數，
    - 查詢參數語法，`$ sudo snap get 應用名 參數名=參數值`
    - 查詢 listen、vnc 的參數值，`sudo snap get novnc services.n6080`

## Ref
- [How to Install TightVNC Server on Ubuntu 20.04](https://serverspace.io/support/help/install-tightvnc-server-on-ubuntu-20-04/)

- [How to Install a Desktop (GUI) on an Ubuntu Server](https://phoenixnap.com/kb/how-to-install-a-gui-on-ubuntu)

- [使用 lightdm 充當 vncserver](https://linoxide.com/install-lightdm-arch-linux/)

- [實戰Ubuntu Server上配置LXDE+VNC環境](https://www.zendei.com/article/2819.html)