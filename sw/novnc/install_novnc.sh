sudo apt update

# ==== install dependency ====
sudo apt install -y expect xorg slim lxde-core tightvncserver novnc websockify

# ==== config vnc-server ====
# create /home/user/.vnc/passwd
pw="useruser"

/usr/bin/expect << EOF

spawn vncserver :1

expect "Password:"
send "$pw\r"

expect "Verify:"
send "$pw\r"

expect "Would you like to enter a view-only password (y/n)?"
send "n\r"

expect eof

EOF

touch ~/.vnc/.Xauthority

touch ~/.vnc/xstartup
#echo export XKL_XMODMAP_DISABLE=1 > ~/.vnc/xstartup

vncserver -kill :1

# ==== generate ssl ====
# create /etc/ssl/novnc.pem

cmd_genssl="sudo openssl req -x509 -nodes -newkey rsa:2048 -keyout /etc/ssl/novnc.pem -out /etc/ssl/novnc.pem -days 365"

/usr/bin/expect << EOF

spawn $cmd_genssl

expect -exact "Country Name (2 letter code) \[AU\]:"
send -- "TW\r"

expect -exact "State or Province Name (full name) \[Some-State\]:"
send -- "\r"

expect -exact "Locality Name (eg, city) \[\]:"
send -- "Taichung\r"

expect -exact "Organization Name (eg, company) \[Internet Widgits Pty Ltd\]:"
send -- "NA\r"

expect -exact "Organizational Unit Name (eg, section) \[\]:"
send -- "NA\r"

expect -exact "Common Name (e.g. server FQDN or YOUR name) \[\]:"
send -- "user\r"

expect -exact "Email Address \[\]:"
send -- "123@example.com\r"

expect eof

EOF

sudo chmod 644 /etc/ssl/novnc.pem

echo Press anykey to reboot 
read response

sudo reboot now

# run novnc: 
#   $ vncserver -geometry 800x600 :1
#   $ websockify -D --web=/usr/share/novnc/ --cert=/etc/ssl/novnc.pem 6080 localhost:5901

# verify on host: 
# http://127.0.0.1:6080/vnc.html
