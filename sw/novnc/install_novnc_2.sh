<<comment
  - 此版本不使用加密憑證連線，只適合內網使用
  - 移除不必要的套件安裝，
  - 改由 snap 下載新版的 novnc，
    - 新版 novnc v1.3.0，會自動安裝 websockify
    - 新版 novnc v1.3.0，只需要啟動 vncserver 和 novnc，不需要手動啟動 websockify
comment

sudo apt update

# ==== install dependency ====

sudo apt install -y expect slim lxde-core tightvncserver
sudo snap install novnc

# ==== config vnc-server ====
# create /home/user/.vnc/passwd
pw="useruser"

/usr/bin/expect << EOF

spawn vncserver :1

expect "Password:"
send "$pw\r"

expect "Verify:"
send "$pw\r"

expect "Would you like to enter a view-only password (y/n)?"
send "n\r"

expect eof

EOF

#echo Press anykey to reboot 
#read response

#sudo reboot now

# run novnc: 
#   $ vncserver :1
#   $ novnc --listen 6080 --vnc localhost:5901 > /dev/null 2>&1 &

# verify on host: 
# http://127.0.0.1:6080/vnc.html
