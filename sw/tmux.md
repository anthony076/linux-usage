## tmux 的使用

- 基本概念
  - session : 使用一次 tmux 指令就會建立一個 session
  - window : 類似 windows 的桌面，代表一整個螢幕
  - panel : 分割視窗，將當前終端切成多個小塊

## Session

- 記憶
  - a = attach 或 all
  - d = detach
  - s = session
  - t = tag
  - z = zoom-in / zoom-out

- 建立
  - 建立新的 session，`$ tmux` 或 `$ tmux new`，
    進入後，底下會出現綠色的狀態欄條
  - 建立具名的session，`$ tmux new -s 名稱`
   - 重新命名 session 名稱，`<ctrl+b> -> $`

- 關閉
  - 關閉 session，`<ctrl+b> -> &`
  - 關閉最後一個session，`$ tmux kill-session`
  - 關閉具名的session，`tmux kill-session -t 名稱`
  - 關閉所有其他的session，只保留目前的，`tmux kill-session -a`
  - 背景執行當前 session，`<ctrl+b> -> d`，會保留session到背景執行

- 返回
  - 返回最後一次 session，`$ tmux a` 或 `$ tmux at` 或 `$ tmux attach` 或 `$ tmux attach-session`
  - 返回指定的 session，`$ tmux at -t 名稱或編號`
  - 列出所有 session，`$ tmux ls` 或 `$ tmux list-sessions` 或 `<ctrl+b> -> d`

- 切換
  - 快速切換到上一個 session，`<ctrl+b> -> (`
  - 快速切換到下一個 session，`<ctrl+b> -> )`

# Panel

- 建立
  - 建立水平分割面板，`<ctrl+b> -> "`
  - 建立垂直分割面板，`<ctrl+b> -> %`

- 調整 
  - 調整分割面板大小，按住`<ctrl+b>`後，依序點擊多次方向鍵，
  - 調整分割面板一次性放大，`<ctrl+b> -> z`，第一次zoom-out，第二次zoom-in

- 將當前分割面板移動到左右側，`<ctrl+b> -> {` 或 `<ctrl+b> -> }`

- 切換
  - 切換到其他分割面板，`<ctrl+b> -> 方向鍵`
  - 切換到下一個分割面板，`<ctrl+b> -> o`
  - 顯示分割面板編號，`<ctrl+b> -> q`
  - 根據分割面板編號進行切換，`<ctrl+b> -> q -> 編號`

- 切換布局，`<ctrl+b> -> 空白鍵`
- 關閉當前分割面板，`<ctrl+b> -> x`

## 配置範例

- 配置文件位置，`~/.tmux.conf`

- 配置範例

  ```shell

  # 設置前綴鍵為 Ctrl+t
  set -g prefix C-t
  unbind C-b
  bind C-t send-prefix

  # 啟用滑鼠支援 : 可切換視窗、調整大小
  set -g mouse on

  # ----------------

  # 重新加載配置文件的快捷鍵，透過 Ctrl+t -> r 調用
  bind r source-file ~/.tmux.conf \; display-message "Config reloaded!"

  # ----------------

  # 設置窗口切換時的顏色
  #set -g status-bg colour235
  #set -g status-fg white
  #set -g window-status-current-bg green
  #set -g window-status-current-fg black

  # ----------------

  # 分割窗格的快捷鍵
  bind | split-window -h
  bind - split-window -v

  # 調整窗格大小的快捷鍵
  #bind -r M-Left resize-pane -L
  #bind -r M-Right resize-pane -R
  #bind -r M-Up resize-pane -U
  #bind -r M-Down resize-pane -D

  # 切換窗格的快捷鍵
  #bind h select-pane -L
  #bind j select-pane -D
  #bind k select-pane -U
  #bind l select-pane -R

  # ----------------

  # 顯示時間
  #set -g status-right "%H:%M:%S %d-%b-%y"

  # 顯示主機名
  #set -g status-left "#h "

  # 窗口列表顯示
  #set -g window-status-current-format "#I:#W"
  #set -g window-status-format "#I:#W"

  ```
## 其他

- 進入命令模式，`<ctrl+b> -> :`

- 顯示快速鍵，`$ tmux list-keys` 或 `:list-keys`

## ref

- [tmux cheatsheet](https://tmuxcheatsheet.com/)
- [tmux 使用技巧](https://github.com/tmux/tmux/wiki)
- [tmux相關插件](https://github.com/orgs/tmux-plugins/repositories?type=all)
- [tmux 配置範例 @ Murderlon](https://github.com/Murderlon/dotfiles/blob/main/tmux/.tmux.conf)
- [tmux 配置範例 @ g6ai](https://github.com/g6ai/dotfiles/blob/main/private_dot_config/tmux/tmux.conf)