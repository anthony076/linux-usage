## awk 的使用

- 使用場景 : 可以用來處理多欄(Field)的數據

- 欄位變數

  考慮以下透過`$ ls -al`得到的資料

  ```
  $1            $2  $3       $4            $5   $6  $7  $8    $9
  drwxr-sr-x    5   ching    wheel         4096 Sep 12  14:55 ./
  drwxr-xr-x    4   root     root          4096 Jul  9  02:45 ../
  drwxr-sr-x    3   ching    wheel         4096 Jul  9  03:14 .cache/
  drwx--S---    4   ching    wheel         4096 Jul  9  03:14 .config/
  drwx--S---    3   ching    wheel         4096 Jul  9  02:46 .local/
  -rw-r--r--    1   ching    wheel          128 Sep 12  14:55 data.csv
  -rwxr-xr-x    1   ching    wheel          121 Sep 12  14:48 ttt.sh*
  ```

  在 awk 中，`$0` 代表整行的資料，`$1 ~ $n` 代表第n欄的資料，
  
  例如，`$ ls -al | awk '{print $9}'`，代表打印第9欄的資料，得到

  ```
  .
  ..
  .cache
  .config
  .local
  data.csv
  ttt.sh
  ```

- 語法，`awk 選項 '{awk程式}' 檔案`
  - 選項，`-F`，設置分隔符號
  - 選項，`-f`，從檔案讀取awk程式

  - awk程式
    - 函數，`print "打印內容"`，打印欄位資料，
      - 例如，{print $2, $3}，打印第2、3欄的數據，輸出數據以空白區隔
      - 例如，{print $2 $3}，打印第2、3欄的數據，輸出數據沒有區隔
  
    - 函數，`toupper(欄位變數)`，轉換為大寫
  
    - 函數，`gsub(搜尋目標 替換文字)`，替換字串
  
  - 內建變數
    - `NR 變數`，number-of-row，用於獲取行號

## 範例集

- 範例，只打印第5、8欄的資料，`ls -l | awk '{print $5,$8}'`
- 範例，自定義輸出內容，`ls -l | awk '{print "File",$8,"size =",$5,"Byte"}'`，輸出，`File Desktop size = 4096 Byte`，
- 範例，進行運算，`ls -l | awk '{print "File",$8,"size =",$5/1024,"KB"}'`，輸出，`File Desktop size = 4 KB`
- 範例，由使用者輸入數據，`awk '{print $1*$2}' -`，使用者輸入`5 6`，輸出`30`
- 範例，篩選，`awk '$2 > 30 {print $0}' file.txt`，打印第2欄 > 30 的所有行
- 範例，篩選，`ls -al | awk 'NR>3 {print $9}'`，跳過前三row，NR=no-row
- 範例，替換文本，`awk '{gsub(/old/, "new"); print}' file.txt`，將文件中所有的 "old" 替換為 "new"
- 範例，轉換為大寫，`awk '{print toupper($1)}' file.txt`

## ref

- [awk使用教學](https://www.hy-star.com.tw/tech/linux/awk/awk.html)