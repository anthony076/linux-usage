## lazygit 常用操作

- 基本概念
  - 分為 Status區、Files區、Local-branches區、Commits區、Stash區
  - 透過每個區域左上角的`數字鍵`或透過`tab鍵`，可以`切換不同的功能區`
  - 每個區域都有限制可進行的操作，畫面`左下角`顯示該區可進行操作的快速鍵，
    或是透過`<?>鍵`，顯示完整的快速鍵

- Status區 : 顯示當前的目錄名

- Files區 : 顯示有修改過的檔案，沒有修改過的不會顯示

  - `<A>` : Amend last commit
  - `<c>` : 為 stage 中的所有檔案建立 commit
  - `<space>` : 將當前檔案添加到 stage
  - `<a>` : 將所有檔案添加到 stage
  - `<d>` : 放棄檔案變更
  - `<D>` : 執行 reset 操作
  
  - 將檔案添加到工作暫存區(stash)
    - 先將檔案添加到 stage 中
    - 按下`<s>`，將 starge 的檔案推送到 stash 區

- Local-branches區 : 顯示所有的 branches
  - `<space>` : 切換到指定的 branch
  - `<n>` : 建立新的 branch
  - `<d>` : 刪除指定的 branch
  
- Commits區 : 顯示所有的 commits
  - 對選中的commit按下`<enter>`，可以查看該commit下所有修改的檔案內容，
    按下`<esc>`返回

- Stash區 : 工作暫存區
  - 從stash區還原到files區的兩種方式
    - 方法，Apply，還原 + 保留stash區中的項目
      - 選中 stash 中的項目
      - 按下 <space> 進行 apply 的操作

    - 方法，Pop，還原 + 不保留stash區中的項目
      - 選中 stash 中的項目
      - 按下 <g> 進行 pop 的操作
  
  - 從stash區刪除項目的兩種方法
    - 注意，stash區的項目不能直接刪除
    - 方法，將stash區的項目pop到files區，stash區會自動刪除
    - 方法，將stash區的項目apply到files區，再透過<d>鍵刪除stash區的項目
