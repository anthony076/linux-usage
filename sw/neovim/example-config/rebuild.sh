
cp -r ~/.config/nvim ~

rm -rf ~/.config/nvim
rm -rf ~/.local/share/nvim
rm -rf ~/.cache/nvim

sudo pacman -Rsn neovim --noconfirm
sudo pacman -S neovim --noconfirm

mv ~/nvim ~/.config/
