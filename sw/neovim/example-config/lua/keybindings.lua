local function map(mode, key, value)
	vim.keymap.set(mode, key, value, { noremap = true, silent = true })
end

vim.g.mapleader = " "

-- keybindings
map("n", "<C-Left>", ":vertical resize +3<CR>")   -- 調整視窗左右
map("n", "<C-Right>", ":vertical resize -3<CR>")
map("n", "<C-Up>", ":resize +3<CR>")              -- 調整視窗上下
map("n", "<C-Down>", ":resize -3<CR>")

map("i", "<C-a>", "<C-v>")                        -- 替換 ctrl+v鍵

-- map("n", "<C-t>", ":ToggleTerm<CR>")              -- 開啟 terminal
map("n", "<C-t>", ":lua fish_toggle()<CR>")          -- 開啟 fish-terminal

map("n", "<C-s>", ":w<CR>")                       -- 存檔

map("n", "nh", ":nohlsearch<CR>")                 -- 關閉 search 高亮選中項

-- bufferline keybindings

map("n", "<C-h>", ":BufferLineCyclePrev<CR>")
map("n", "<c-l>", ":BufferLineCycleNext<CR>")

-- lsp-keybindings

local lsp_keybinds = {}

lsp_keybinds.set_lsp_keymap = function(bufnr)
	local opts = { noremap = true, silent = true, buffer = bufnr }

	-- 以下建立 lspsage 的快速鍵，利用 lspsaga 取代 lsp 內建的功能

	local keymap = vim.keymap

	-- 顯示 definition和 references
	keymap.set("n", "gf", "<cmd>Lspsaga lsp_finder<CR>", opts)
	-- 跳到聲明處
	keymap.set("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
	-- 檢視定義和在視窗中編輯
	keymap.set("n", "gd", "<cmd>Lspsaga peek_definition<CR>", opts)
	-- 跳轉到實現
	keymap.set("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)
	-- 檢視可用的 code actions
	keymap.set("n", "<leader>ca", "<cmd>Lspsaga code_action<CR>", opts)
	-- 智慧更名，一次性變更所有相同的符號
	keymap.set("n", "<leader>rn", "<cmd>Lspsaga rename<CR>", opts)

	-- 顯示當前行的 diagnostics，只對有問題的行有效，用於顯示詳細錯誤訊息
	keymap.set("n", "<leader>D", "<cmd>Lspsaga show_line_diagnostics<CR>", opts)
	-- 顯示當前滑鼠位置的 diagnostics  只對有問題的位置有效，用於顯示詳細錯誤訊息
	keymap.set("n", "<leader>d", "<cmd>Lspsaga show_cursor_diagnostics<CR>", opts)
	-- 跳轉到當前文件中的上一個錯誤
	keymap.set("n", "[d", "<cmd>Lspsaga diagnostic_jump_prev<CR>", opts)
	-- 跳轉到當前文件中的下一個錯誤
	keymap.set("n", "]d", "<cmd>Lspsaga diagnostic_jump_next<CR>", opts)

	-- 顯示當前游標位置的說明文件，需要安裝 makdown 解析器
	keymap.set("n", "K", "<cmd>Lspsaga hover_doc<CR>", opts)
	-- 在右側顯示當前文件的綱要
	keymap.set("n", "<leader>o", "<cmd>Lspsaga outline<CR>", opts)
end

return lsp_keybinds
