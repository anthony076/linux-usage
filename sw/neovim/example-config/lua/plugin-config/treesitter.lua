local treesitter_status, treesitter = pcall(require, "nvim-treesitter.configs")
if not treesitter_status then
	return
end

local hl = vim.api.nvim_set_hl
local bufnr = vim.api.nvim_get_current_buf()
hl(bufnr, "TSFunctionCall", { foreground = "#F2CE87" })
hl(bufnr, "TSConstructor", { foreground = "#F2CE87" })
hl(bufnr, "TSMethod", { foreground = "#F2CE87" })
hl(bufnr, "TSTypeFunction", { foreground = "#F2CE87" })
hl(bufnr, "TSPunctDelimiter", { foreground = "#F2CE87" })

treesitter.setup({
	-- 指定要使用語法高亮的語言
	ensure_installed = {"markdown", " markdown_inline"},
	-- 是否自動安裝 ensure_installed 列表中的 language-parser
	sync_install = true,
	-- 是否自動安裝缺失的 language-parser
	auto_install = true,

	-- 語法高亮模塊
	hightlight = {
		enable = true,
		additional_vim_regex_highlighting = false,
	},

	-- 增量選擇模塊
	incremental_selection = {
		enable = false,
		keymaps = {
			init_selection = "<CR>",
			node_incremental = "<CR>",
			node_decremental = "<BS>",
			scope_incremental = "<TAB>",
		},
	},

	-- 縮進格式化模塊
	indent = {
		enable = true,
	},
})
