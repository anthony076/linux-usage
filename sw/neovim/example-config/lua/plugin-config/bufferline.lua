
local bufferline_status, bufferline = pcall(require, "bufferline")
if not bufferline_status then
	return
end

bufferline.setup({
	options = {

		-- 是否顯示diagnostic狀態，可用值: fase | "nvim_lsp" | "coc"
		diagnostics = "nvim_lsp",

		-- tab 之間的分隔器樣式
		separator_style = "thin",

		-- 是否顯示檔案類型圖示
		show_buffer_icons = false,

		-- 只有一個檔案時, 是否顯示 bufferline
		always_show_bufferline = false,

		-- 啟用nvimtree時，預留nvimtree的顯示空間
		-- 避免 tab 從左上角開始顯示
		offsets = {
			{
				filetype = "NvimTree",
				text = "",
			},
		},
	},
})


