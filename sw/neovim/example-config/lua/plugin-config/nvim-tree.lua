
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
vim.opt.termguicolors = true

local tree_status, tree = pcall(require, "nvim-tree")
if not tree_status then
  return
end

tree.setup {
}
