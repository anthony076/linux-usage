-- local lsp_keybinds = require("keybindings")

require("plugin-config.lsp.lua")
require("plugin-config.lsp.python")

----------------
-- 導入插件
----------------

local mason_status, mason = pcall(require, "mason")
if not mason_status then
	return
end

mason.setup()

---- 安裝 lsp-server
local masonconfig_status, masonconfig = pcall(require, "mason-lspconfig")
if not masonconfig_status then
	return
end

masonconfig.setup({
	ensure_installed = { "lua_ls" }, -- 自動安裝 lua 的 lsp-server
})

---- 安裝 formatter、linter 相關套件
local mason_null_ls_status, mason_null_ls = pcall(require, "mason-null-ls")
if not mason_null_ls_status then
	return
end

-- 自動安裝 formmatting 和 linting 相關的指定套件
--    check https://github.com/jay-babu/mason-null-ls.nvim/commit/e2144bd62b703c1fa298b9e154296caeef389553
--    或在 :Mason 中檢查可用的 formmatting 和 linting 插件
-- 注意，自動安裝不是必要的，也可以手動透過 :Mason 安裝
mason_null_ls.setup({
	ensure_installed = {
		"flake8", -- python linter    , need python3
		"black", -- python formatter  , need python3
		"stylua", -- lua formatter
		--"prettier", -- multi-language formatter, focus on web application,  need npm
	},
})

----------------
-- 變更 diagnostics symbols 的顯示樣式
----------------

local signs = { Error = " ", Warn = " ", Hint = "ﴞ ", Info = " " }

for type, icon in pairs(signs) do
	local hl = "DiagnosticSign" .. type
	vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
end
