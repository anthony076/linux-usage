--------------
-- 導入 
--------------

local lsp_keybinds = require("keybindings")

local cmp_nvim_lsp_status, cmp_nvim_lsp = pcall(require, "cmp_nvim_lsp")
if not cmp_nvim_lsp_status then
  return
end

local lspconfig_status, lspconfig = pcall(require, "lspconfig")
if not lspconfig_status then
  return
end

--------------
-- 設置 pyright
--------------

-- 添加 python 庫的位置
local cur_env_path = vim.env.PATH or ""
local new_env_path = "/home/vagrant/.local/bin:" .. cur_env_path
vim.env.PATH = new_env_path

local on_attach = function(_, bufnr)
  lsp_keybinds.set_lsp_keymap(bufnr)
end

-- 使用 cmp_nvim_lsp 取代 lsp 內建的自動補全
local capabilities = cmp_nvim_lsp.default_capabilities()

lspconfig.pylsp.setup {
  -- 以 cmp_nvim_lsp 替換 lsp 原本的功能
  capabilities = capabilities,
  -- 設置 keybinding
  on_attach = on_attach,
}

