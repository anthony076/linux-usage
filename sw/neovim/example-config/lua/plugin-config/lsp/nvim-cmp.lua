
-- 設置 insert-mode 下可用的自動補全
vim.opt.completeopt = "menu,menuone,noselect"

---------------
-- 導入插件
---------------

local cmp_status, cmp = pcall(require, "cmp")
if not cmp_status then
  return
end

local luasnip_status, luasnip = pcall(require, "luasnip")
if not luasnip_status then
  return
end

local lspkind_status, lspkind = pcall(require, "lspkind")
if not lspkind_status then
  return
end

---------------
-- config luasnip
---------------

-- 建立 luasnip 的相關按鍵

-- vim.keymap.set({"i", "s"}, "<a-e>", function()  -- 展開 snippet，獨立使用 luasnip 或變更擴展的按鍵才需要
--   if luasnip.expand_or_jumpable() then
--     luasnip.expand()
--   end
-- end)

vim.keymap.set({"i", "s"}, "<a-j>", function()  -- 跳到下一個輸入點
  if luasnip.jumpable(1) then
    luasnip.jump(1)
  end
end)

-- vim.keymap.set({"i", "s"}, "<a-k>", function()  -- 跳到上一個輸入點
--   if luasnip.jumpable(-1) then
--     luasnip.jump(-1)
--   end
-- end)

-- 透過 luasnip-api 建立自定義模板

-- luasnip.add_snippets("all", {
--   luasnip.snippet(
--     -- snippet info
--     { name = "myname", trig = "sa",  dscr = "say hello" },
--
--     -- real snippet，寫法1，不分行寫法
--     --{ ls.text_node("hello"), ls.text_node("world") }  -- helloworld
--
--     -- real snippet，寫法2，自動分行寫法
--     --{ ls.text_node({ "hello", "world" }) }  -- hello\nworld
--
--     -- real snippet，寫法3，含有輸入點的snippet
--     { luasnip.text_node("hello "), luasnip.insert_node(1), luasnip.text_node(" nice "), luasnip.insert_node(2) } -- hello $1 nice $2
--   )
--
-- })

-- 透過 vscode 語法建立自定義模板
-- 參考，https://code.visualstudio.com/api/language-extensions/snippet-guide

require("luasnip.loaders.from_vscode").load({
  paths = vim.fn.stdpath "config" .. "/mysnippet",
})

---------------
-- config cmp
---------------

cmp.setup({

  -- 將 snippet-engine 指定為 luasnip
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)

      -- For `ultisnips` snippets-engine plugin
      -- vim.fn["UltiSnips#Anon"](args.body)

      -- For `snippy` snippets-engine plugin
      -- require'snippy'.expand_snippet(args.body)

    end,
  },

  -- 設置自動補全的來源
  sources = cmp.config.sources({
    { name = "nvim_lsp" }, -- get source from neovim lsp
    { name = "luasnip" }, -- get source from luasnip plugin
    { name = "buffer" },
    { name = "path" },
    { name = "cmd" },
  }),


  -- 設置快速鍵
  mapping = cmp.mapping.preset.insert({
    ["<C-k>"] = cmp.mapping.select_prev_item(), -- 在自動補全的選單中移動
    ["<C-j>"] = cmp.mapping.select_next_item(), -- 在自動補全的選單中移動
    ["<C-b>"] = cmp.mapping.scroll_docs(-4),    -- 在自動補全的選單中捲動
    ["<C-f>"] = cmp.mapping.scroll_docs(4),     -- 在自動補全的選單中捲動

    ["<C-Space>"] = cmp.mapping.complete(),     -- show completion suggestions
    ["<C-e>"] = cmp.mapping.abort(),            -- close completion window
    ["<CR>"] = cmp.mapping.confirm({ select = false }),
  }),


  -- 變更自動補全選單的顯示格式，
  -- 將代碼提示的分類說明，改為顯示圖標
  formatting = {
    format = lspkind.cmp_format({
      maxwidth = 50,
      ellipsis_char = "...",
    }),
  },

})

-- command-mode(/) 下的自動補全
cmp.setup.cmdline('/', {
  mapping = cmp.mapping.preset.cmdline(),
  sources = {
    { name = 'buffer' }
  }
})

-- command-mode(:) 下的自動補全
cmp.setup.cmdline(':', {
  mapping = cmp.mapping.preset.cmdline(),
  sources = cmp.config.sources({
    { name = 'path' }
  }, {
    {
      name = 'cmdline',
      option = {
        ignore_cmds = { 'Man', '!' }
      }
    }
  })
})


