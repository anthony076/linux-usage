local setup, null_ls = pcall(require, "null-ls")
if not setup then
	return
end

local formatting = null_ls.builtins.formatting -- to setup formatter
-- local diagnostics = null_ls.builtins.diagnostics  -- to setup linter

null_ls.setup({
	sources = {
		--  設置提供 formatting 功能的來源插件
		formatting.black, -- 需要透過mason安裝外部的 black 套件
		formatting.stylua, -- 需要透過mason安裝外部的 stylua 套件
		--formatting.prettier, -- 需要透過mason安裝外部的 prettier 套件

		-- 設置 eslint_d 套件，只有在root目錄具有 .eslintrc.js 的檔案時才生效
		-- diagnostics.eslint_d.with({ -- js/ts linter
		--   -- only enable eslint if root has .eslintrc.js (not in youtube nvim video)
		--   condition = function(utils)
		--     return utils.root_has_file(".eslintrc.js") -- change file extension if you use something else
		--   end,
		-- }),
	},

	-- 建立自動化命令, 寫入保存時，自動執行格式化
	-- on_attach = function(current_client, bufnr)
	-- 	local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
	--
	-- 	-- 判斷當前文件是否支援 formatting
	-- 	if current_client.supports_method("textDocument/formatting") then
	-- 		-- 清除當前文件的 LspFormatting 命令，改用 null-ls 執行
	-- 		vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
	--
	-- 		-- 建立新的 formatting 自動命令
	-- 		vim.api.nvim_create_autocmd("BufWritePre", { -- buffer 寫入前
	-- 			group = augroup,
	-- 			buffer = bufnr,
	--
	-- 			callback = function() -- 定義要執行的內容
	-- 				vim.lsp.buf.format({
	-- 					filter = function(client)
	-- 						--  only use null-ls for formatting instead of lsp server
	-- 						return client.name == "null-ls"
	-- 					end,
	-- 					bufnr = bufnr,
	-- 				})
	-- 			end,
	-- 		})
	-- 	end
	-- end,
})
