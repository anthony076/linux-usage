
local indent_status, indent = pcall(require, "indent_blankline")

if not indent_status then
  return
end

vim.opt.termguicolors = true
vim.opt.list = true
vim.opt.listchars:append("space:⋅")   -- 變更空白的顯示樣式
--vim.opt.listchars:append("eol:↴")   -- 變更EndOfLine的顯示樣式

indent.setup({

  -- 是否使用 treesitter 確定縮進線的位置
  use_treesitter = true,

  -- 是否固定顯示第一行的縮進線
  show_first_indent_level = true,

  -- 空白行是否顯示縮進線
  show_trailing_blankline_indent = false,

  -- 突出顯示游標所屬的縮進 (游標所在行的縮進線進行高亮)
  show_current_context = true,

  -- 將當前游標行加上底線
  show_current_context_start = true,

  -- 禁用 indent 的類型, 減少資源的浪費
  buftype_exclude = { "terminal", "nofile" },
  bufname_exclude = { "README.md" },
  filetype_exclude = {
    "alpha",
    "log",
    "gitcommit",
    "dapui_scopes",
    "dapui_stacks",
    "dapui_watches",
    "dapui_breakpoints",
    "dapui_hover",
    "LuaTree",
    "vimwiki",
    "markdown",
    "vista",
    "NvimTree",
    "git",
    "TelescopePrompt",
    "undotree",
    "help",
    "startify",
    "packer",
    "neogitstatus",
    "NvimTree",
    "Outline",
    "Trouble",
    "lspinfo",
    "", -- for all buffers without a file type
  },
})

