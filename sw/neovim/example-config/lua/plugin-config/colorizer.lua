
local color_status, color = pcall(require, "colorizer")
if not color_status then
  return
end

color.setup()
