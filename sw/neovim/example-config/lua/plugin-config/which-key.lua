
-- wk 會自動抓取透過 lua-api 宣告的快速鍵

local wk_status, wk = pcall(require, "which-key")
if not wk_status then
  return
end

wk.setup {
  plugins = {
    marks = false,
    registers = false,
    spelling = { enabled = false, suggestions = 20 },

    -- 是否顯示neovim內鍵的快速鍵
    presets = {
      operators = false,
      motions = false,
      text_objects = false,
      windows = false,
      nav = false,
      z = false,
      g = false,
    },

  },
}

-- 建立自定義選單和快速鍵
local mappings = {

  e = { ":NvimTreeToggle<CR>", "Explorer" },    -- 打開檔案管理器
  s = { ":Dashboard<CR>", "Starter" },   -- 打開歡迎介面(打開最近開啟的檔案)
  p = { ":lua python_toggle()<CR>", "Python" },  -- 打開 python-terminal
  q = { ":bdelete<CR>", "Close Buffer and Tab" },     -- 關閉 buffer 的同時，也關閉對應的 tab
  u = { ":UndotreeToggle<>", "Undotree" }, -- 打開回朔樹

  L = {
    name = "lsp",
    n = { vim.lsp.buf.rename, "rename" },
    c = { vim.lsp.buf.code_action, "code action" },
    d = { vim.lsp.buf.definition, "definition" },
    i = { vim.lsp.buf.implementation, "implementation" },
    r = { require("telescope.builtin").lsp_references, "references" },
    k = { vim.lsp.buf.hover, "hover" },
  },

  S = {
    name = "session",
    w = { ":lua MiniSessions.write('mysession')<CR>", "MiniSessions.write()" },
    r = { ":lua MiniSessions.read('mysession')<CR>", "MiniSessions.read()" },
    d = { ":lua MiniSessions.delete('mysession')<CR>", "MiniSessions.delete()" },
  },

  T = {
    name = "telescope",
    f = { ":Telescope find_files<CR>", "find files", },
    b = { ":Telescope buffers<CR>", "find buffers"},
    s = { ":Telescope live_grep<CR>", "find file on any", },
    c = { ":Telescope grep_string<CR>", "find file on cursor", },
    h = { ":Telescope help_tags<CR>", "help tags"},
  },

}

local opts = {
  prefix = "<leader>",  -- 觸發自定義選單的按鍵
  nowait = true,
}

-- 註冊自義選單
wk.register(mappings, opts)

