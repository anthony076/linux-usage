
local dashboard_status, dashboard = pcall(require, "dashboard")
if not dashboard_status then
  return
end

dashboard.setup {
  theme = 'hyper',

  config = {
    week_header = {
     enable = true,   -- 顯示時間
    },

    -- header = {
    -- '',
    -- ' ███╗   ██╗ ███████╗ ██████╗  ██╗   ██╗ ██╗ ███╗   ███╗',
    -- ' ████╗  ██║ ██╔════╝██╔═══██╗ ██║   ██║ ██║ ████╗ ████║',
    -- ' ██╔██╗ ██║ █████╗  ██║   ██║ ██║   ██║ ██║ ██╔████╔██║',
    -- ' ██║╚██╗██║ ██╔══╝  ██║   ██║ ╚██╗ ██╔╝ ██║ ██║╚██╔╝██║',
    -- ' ██║ ╚████║ ███████╗╚██████╔╝  ╚████╔╝  ██║ ██║ ╚═╝ ██║',
    -- ' ╚═╝  ╚═══╝ ╚══════╝ ╚═════╝    ╚═══╝   ╚═╝ ╚═╝     ╚═╝',
    -- '' ,
    -- },
    
    Packages = { enable = false },    -- 顯示載入插件的數量
    
    shortcut = {    -- 顯示自定義快速鍵
      {
        desc = ' Explorer',
        group = 'Label',
        action = 'NvimTreeToggle',
        key = 'e'
      },

      {
        icon = ' ',
        icon_hl = '@variable',
        desc = 'Files',
        group = 'DiagnosticHint',
        action = 'Telescope find_files',
        key = 'f',
      },
      {
        icon = ' ',
        icon_hl = '@variable',
        desc = 'Help',
        group = 'DiagnosticHint',
        action = 'Telescope help_tags',
        key = 'h',
      },

      {
        desc = ' WhichKey',
        group = 'Number',
        action = 'e ~/.config/nvim/lua/plugin-config/which-key.lua',
        key = 'w',
      },

      {
        desc = ' Packages',
        group = 'Number',
        action = 'e ~/.config/nvim/lua/install-plugin.lua',
        key = 'i',
      },
    },
    
    -- 顯示最近開啟的檔案
    mru = {
      limit = 5,   -- 限制 most-recent-used 的數量
      lable = "Recent files",
    },
    
    -- 顯示頁尾訊息
    footer = {},

  },
}
