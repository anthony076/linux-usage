
local lualine_status, lualine = pcall(require, "lualine")
if not lualine_status then
  return
end


lualine.setup {
  options = {
    theme = 'material',
    component_separators = { left = "", right = ""},
    section_separators = { left = "", right = ""}
  },

  sections = {
    lualine_c = { 
      {'filename', path=2 }, -- 顯示完整檔案路徑
    },
  }
}


