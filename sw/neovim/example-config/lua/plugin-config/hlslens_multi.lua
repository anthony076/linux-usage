
---------------
-- 配置 hlslens
---------------

local hlslens_status, hlslens = pcall(require, "hlslens")
if not hlslens_status then
  return
end

local kopts = {noremap = true, silent = true}

vim.api.nvim_set_keymap('n', 'n', [[<Cmd>execute('normal! ' . v:count1 . 'n')<CR><Cmd>lua require('hlslens').start()<CR>]], kopts)

vim.api.nvim_set_keymap('n', 'N', [[<Cmd>execute('normal! ' . v:count1 . 'N')<CR><Cmd>lua require('hlslens').start()<CR>]], kopts)

vim.api.nvim_set_keymap('n', '*', [[*<Cmd>lua require('hlslens').start()<CR>]], kopts)
vim.api.nvim_set_keymap('n', '#', [[#<Cmd/lua require('hlslens').start()<CR>]], kopts)
vim.api.nvim_set_keymap('n', 'g*', [[g*<Cmd>lua require('hlslens').start()<CR>]], kopts)
vim.api.nvim_set_keymap('n', 'g#', [[g#<Cmd>lua require('hlslens').start()<CR>]], kopts)
vim.api.nvim_set_keymap('n', '<Leader>l', '<Cmd>noh<CR>', kopts)

hlslens.setup({
  calm_down = false,   -- 當游標離開關鍵字的範圍時，自動關閉高亮
})

---------------
-- 配置 vim-visual-multi
---------------

local multi_status, multi = pcall(require, "multi")
if not multi_status then
  return
end

-- 建立自動命令，
--    當 visual_multi_start 事件觸發時，執行 lua require('vmlens').start()
--    當 visual_multi_exit 事件觸發時，執行 lua require('vmlens').exit()

vim.cmd([[
    aug VMlens
        au!
        au User visual_multi_start lua require('vmlens').start()
        au User visual_multi_exit lua require('vmlens').exit()
    aug END
]])

multi.setup {}

