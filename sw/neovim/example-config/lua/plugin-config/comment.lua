
local comment_status, comment = pcall(require, "Comment")
if not comment_status then
  return
end

comment.setup {
  padding = false,
  toggler = {
   line = 'gcc',
   block = 'gc',
  },
  opleader = {},
  mappings = {
   basic = true,
   extra = false,
  }
}

