
local lastplace_status, lastplace = pcall(require, "nvim-lastplace")
if not lastplace_status then
  return
end

lastplace.setup()
