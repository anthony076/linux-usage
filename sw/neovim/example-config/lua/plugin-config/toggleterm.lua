
local toggleterm_status, toggleterm = pcall(require, "toggleterm")
if not toggleterm_status then
  return
end

toggleterm.setup({
  -- 啟動後直接進入 insert-mode
  start_in_insert = true,
  -- 以 float 的方式顯示 terminal
  direction = 'float',
  -- terminal 退出後直接關閉視窗
  close_on_exit = true,
})

-------------
-- 打開terminal，並直接執行特定命令
-------------
local Terminal  = require('toggleterm.terminal').Terminal

-- 執行python
local pyterm = Terminal:new({
    cmd = 'python',
    direction = 'float'
})

function python_toggle()
  pyterm:toggle()   -- 透過 :lua python_toggle() 執行
end

-- 執行 fish
local fishterm = Terminal:new({
    cmd = 'fish',
    direction = 'float'
})

function fish_toggle()
  fishterm:toggle()   -- 透過 :lua python_toggle() 執行
end


