
------------------
-- mini.starter
------------------
-- local starter_status, starter = pcall(require, "mini.starter")
-- if not starter_status then
--   return
-- end
--
-- starter.setup()

------------------
-- mini.comment
------------------

-- local comment_status, comment = pcall(require, "mini.comment")
-- if not comment_status then
-- 	return
-- end
--
-- comment.setup()

------------------
---- mini.sessions
------------------

local sessions_status, sessions = pcall(require, "mini.sessions")
if not sessions_status then
	return
end

sessions.setup({
  -- 若啟動 nvim 時沒有指定檔案參數，就自動讀取最新的 session
  autoread = false,

  -- 是否在離開 nvim 前, 自動寫入當前的 session
  autowrite = false,

  -- mini.sessions執行操作後是否打印session路徑
  verbose = { read=true, write=true, delete=true },

})

------------------
---- mini.completion
------------------

-- local completion_status, completion = pcall(require, "mini.completion")
-- if not completion_status then
--   return
-- end
--
-- completion.setup()

------------------
---- mini.cursorword
------------------

local cursorword_status, cursorword = pcall(require, "mini.cursorword")
if not cursorword_status then
  return
end

cursorword.setup()

------------------
---- mini.move
------------------

local move_status, move = pcall(require, "mini.move")
if not move_status then
	return
end

move.setup()

------------------
---- mini.trailspace
------------------

-- local trailspace_status, trailspace = pcall(require, "mini.trailspace")
-- if not trailspace_status then
-- 	return
-- end
--
-- trailspace.setup({
--  only_in_normal_buffers = true,
--})
--
-- vim.api.nvim_exec([[
--   augroup mini.trim
--     autocmd!
--     autocmd BufWritePre * lua MiniTrailspace.trim()
--   augroup END
-- ]], false)  -- 寫入檔案前，自動清除行尾空白
--

------------------
---- mini.pairs
------------------

local pair_status, pair = pcall(require, "mini.pairs")
if not pair_status then
	return
end

pair.setup()

------------------
---- mini.surround
------------------

-- local surround_status, surround = pcall(require, "mini.surround")
-- if not surround_status then
-- 	return
-- end
-- surround.setup()

----------------
---- mini.indentscope
------------------

-- local is_status, is = pcall(require, "mini.indentscope")
-- if not is_status then
-- 	return
-- end
--
-- is.setup({ -- show vertical indent line
-- 	draw = {
-- 		delay = 0,
-- 	},
--
-- 	symbol = "|",
-- })
--
-- is.gen_animation.none()

------------------
---- mini.jump2d
-----------------

local jump2d_status, jump2d = pcall(require, "mini.jump2d")
if not jump2d_status then
	return
end

jump2d.setup({
	allowed_lines = {
		blank = false,
	},

	mappings = {
		start_jumping = "f",
	},
})
