local o = vim.o

-- general settings 
o.termguicolors = true
o.number = true
o.numberwidth = 2
o.relativenumber = true
o.signcolumn = "auto:1"
o.cursorline = true

o.splitright = true
o.splitbelow = true

o.tabstop = 2
o.shiftwidth = 2
o.expandtab = true
o.smarttab = true

o.cindent = true
o.autoindent = true
o.textwidth = 300
o.wrap = true
o.list = true
o.listchars = "trail:·,nbsp:◇,tab:→ ,extends:▸,precedes:◂"

o.clipboard = "unnamedplus"
o.ignorecase = true
o.smartcase = true
o.backup = false
o.swapfile = false
o.writebackup = false
o.mouse = "a"

-- show complete path on window-title
vim.cmd('set title')
vim.cmd('set titlestring=%f')

-- hight on yank
vim.cmd([[
  augroup YankHighlight
    autocmd!
    autocmd TextYankPost * silent! lua vim.highlight.on_yank()
  augroup end
]])


