
-- neovim 啟動時檢查 packer 是否存在，若否，自動下載安裝

local fn = vim.fn
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"

if fn.empty(fn.glob(install_path)) > 0 then
  packer_bootstrap = fn.system({ "git", "clone", "--depth", "1", "https://github.com/wbthomason/packer.nvim", install_path })
  vim.cmd([[packadd packer.nvim]])
end

-- 判斷 packer 是否安裝成功
local status, packer = pcall(require, "packer")

if not status then
  print("Packer is not installed")
  return
end

