
vim.cmd([[
  augroup packer_user_config
  autocmd!
  autocmd BufWritePost install-plugin.lua source <afile> | PackerSync
  augroup END
]])

local packer = require("packer")

-- 啟動 packer
packer.startup(function(use)
	-- 避免 packer 被刪除
	use({ "wbthomason/packer.nvim" })

	-- 追蹤並安裝第三方插件

  -------------
	-- tokyonight
  -------------
	use({ "folke/tokyonight.nvim", config = 'require("plugin-config/tokyonight")' })

  -------------
	-- lualine
  -------------
	use({ "nvim-lualine/lualine.nvim", config = 'require("plugin-config/lualine")' })

  -------------
	-- whichkey
  -------------
	use({
		"folke/which-key.nvim",
		event = "BufWinEnter",
		config = function()
			vim.o.timeout = true
			vim.o.timeoutlen = 300
			require("plugin-config/which-key")
		end,
	})

  -------------
	-- mini.nvim 多種功能的組合包
  -------------
	use({ "echasnovski/mini.nvim", config = 'require("plugin-config/mini-nvim")' })

  -------------
	-- telescope 各種模糊搜尋
  -------------
  --需要外部套件: fd ripgrep
	use({
		"nvim-telescope/telescope.nvim",
		config = 'require("plugin-config/telescope")',
		requires = {
			"nvim-lua/plenary.nvim",
			"nvim-treesitter/nvim-treesitter",
			{ "nvim-telescope/telescope-fzf-native.nvim", run = "make" },
		},
	})

  -------------
	-- nvim-tree 檔案瀏覽器
  -------------
	use({
		"nvim-tree/nvim-tree.lua",
		requires = { "nvim-tree/nvim-web-devicons" },
		config = 'require("plugin-config/nvim-tree")',
	})

  -------------
	-- telescope 版的檔案瀏覽器
  -------------
	-- use {
	--   "nvim-telescope/telescope-file-browser.nvim",
	--   requires = { "nvim-telescope/telescope.nvim", }
	-- }

  -------------
	-- 預覽 markdown
  -------------
	-- use({
	-- 	"ellisonleao/glow.nvim",
	--    -- config = 'require("plugin-config/glow")'   -- not work
	-- 	config = function()   -- work
	-- 		require("glow").setup()
	-- 	end,
	-- })

  -------------
	-- 記住編輯的最後位置
  -------------
	use({
		"ethanholz/nvim-lastplace",
		config = 'require("plugin-config/lastplace")',
	})

  -------------
  -- 以標籤頁顯示開啟的buffer
  -------------
  use {
    'akinsho/bufferline.nvim',
    requires = 'nvim-tree/nvim-web-devicons',
    config = 'require("plugin-config/bufferline")',
  }

  -------------
  -- 強化語法高亮 + 增量選擇 + 代碼摺疊
  -------------
  use {
    "nvim-treesitter/nvim-treesitter",
    config = 'require("plugin-config/treesitter")',
  }

  -------------
  -- 顯示縮進
  -------------
  use {
    "lukas-reineke/indent-blankline.nvim",
    config = 'require("plugin-config/indent")',
  }

  -------------
  -- 顯示色碼顏色
  -------------
  use {
    "norcalli/nvim-colorizer.lua",
    config = 'require("plugin-config/colorizer")',
  }

  -------------
  -- 進階顯示終端
  -------------
  use {
    "akinsho/toggleterm.nvim",
    config = 'require("plugin-config/toggleterm")',
  }

  -------------
  -- search 增強 + 多游標修改
  -------------
  use {
    "kevinhwang91/nvim-hlslens",
    requires = "mg979/vim-visual-multi",
    config = 'require("plugin-config/hlslens_multi")',
  }

  -------------
  -- 代碼摺疊
  -------------
  use {
    'kevinhwang91/nvim-ufo',
    requires = 'kevinhwang91/promise-async',
    config = 'require("plugin-config/ufo")',
    --run = ":TSUpdate",
  }

  -------------
  -- dashboard
  -------------
  use {
    'glepnir/dashboard-nvim',
    event = 'VimEnter',
    requires = {'nvim-tree/nvim-web-devicons'},
    config = 'require("plugin-config/dashboard")',
  }

  -------------
  -- 括號自動補全
  -------------
  use {
    'kylechui/nvim-surround',
    config = 'require("plugin-config/surround")',
  }

  -------------
  -- 一鍵comment
  -------------
  use {
      'numToStr/Comment.nvim',
      config = 'require("plugin-config/comment")',
  }

  -------------
  -- 回朔樹 
  -------------
  use { 'mbbill/undotree', }

  -------------
	-- lsp related
  -------------

	-- 安裝 lsp 基本功能
	use({
		"williamboman/mason.nvim",
		event = "BufRead",
		requires = {
			"williamboman/mason-lspconfig.nvim",
			"neovim/nvim-lspconfig",
		},

		config = 'require("plugin-config/lsp/mason")',
	})

	-- 替換內建功能的UI和效能
	use({
		"glepnir/lspsaga.nvim",
		event = "LspAttach",
		requires = {
			{ "nvim-treesitter/nvim-treesitter" },
			-- make sure you install markdown and markdown_inline parser
			{ "nvim-tree/nvim-web-devicons" },
		},
		config = 'require("plugin-config/lsp/lspsaga")',
	})

	-- 讓自動補全選單顯示圖示
	use({ "onsails/lspkind.nvim" })

	-- 代碼片段系列
  use({
    "L3MON4D3/LuaSnip",
    requires = {
      "saadparwaiz1/cmp_luasnip",
      "rafamadriz/friendly-snippets",
    },
  })

	-- 自動補全系列
	use({
    "hrsh7th/nvim-cmp",
    requires = {
      "hrsh7th/cmp-buffer",   -- 提供自動補全的buffer源
      "hrsh7th/cmp-path",     -- 提供自動補全的path源
      "hrsh7th/cmp-cmdline",  -- 提供自動補全的cmd源
      "hrsh7th/cmp-nvim-lsp", -- 提供自動補全的lsp源
    },
    config = 'require("plugin-config/lsp/nvim-cmp")',
  })

	-- formatting and linting
	use({ "jay-babu/mason-null-ls.nvim" }) -- 用於透過 mason 自動安裝 formatting、linting 相關插件
	use({ "jose-elias-alvarez/null-ls.nvim", config = 'require("plugin-config/lsp/null-ls")' }) -- formatting 和 linting 主要插件

  -- 顯示 lsp 操作的進度
  use {
    "j-hui/fidget.nvim",
    config = 'require("plugin-config/fidget")',
  }

  -------------
	-- git相關
  -------------

  -- 顯示git修改狀態
	use({
		"lewis6991/gitsigns.nvim",
		config = 'require("plugin-config/gitsigns")',
	})

  -- 執行git相關操作
  use({
    "rbong/vim-flog",
    requires = "tpope/vim-fugitive",
    config = 'require("plugin-config/flog")'
  })

	-- 啟動 packer 後，檢查並安裝第三方插件
	if packer_bootstrap then
		packer.sync()
	end
end)


