
-- neovim-config
require("ensure-packer")
require("install-plugin")
require("keybindings")
require("settings")

