## 配置 neovim

- neovim 的配置文件，
  - 使用 vim-script 的語法進行配置，`~/.config/nvim/init.vim`
  - 使用 lua 的語法進行配置，`~/.config/nvim/init.lua`
  - 注意，兩種配置文件不能同時存在，只能二選一

- 配置文件模塊化的組織方式
  - 組織方法1，使用 require("aa/bb")
    - 在aa目錄下，尋找 `bb目錄中的 init.lua`
    - 在aa目錄下，尋找 `bb.lua`

  - 組織方法2，使用 require("aa.bb") + 需要使用 return 語句
    - 在aa目錄下，尋找 `bb.lua`，在 bb.lua 檔案中，必須使用 return 的語句，返回 bb.lua 的內容
    - 參考，[NvChad的範例](https://github.com/NvChad/NvChad/tree/609b5450d5e12fd3c86dd5b5906737071ebdbd2f)

  - 組織方法3，使用 require("aa.bb") + 不需要使用 return 語句
    - 參考，[在init.lua啟用插件](https://github.com/LunarVim/Neovim-from-scratch/blob/master/lua/user/cmp.lua)

- 幾個特殊的目錄
  - after目錄，`~/.config/nvim/after/plugin`，用於添加插件的額外配置，或覆血原有的預設配置
    - after目錄的使用場景

      用於 neovim 原生插件/第三方插件的額外配置，或修改已經生效的預設配置
      
      例如，vim-fugitive 插件沒有提供自動化命令，用戶想為 vim-fugitive 添加額外的自動化命令

      例如，在插件載入後，添加額外的自動化命令

      例如，[dotfile @ dwt1](https://gitlab.com/dwt1/dotfiles/-/tree/master/.config/nvim/after/plugin)


    - 比較，`after目錄`和`插件管理器的config參數`有什麼區別(以packer為例)
      - config 參數會在插件修改時就生效
      - after目錄不會馬上覆蓋預設的配置，而是在插件載入後，才會讀取 after目錄的內容，並進行修改

    - 注意，若第三方插件使用了自己的配置目录，例如，~/.config/nvim/plugged/[plugin-name]/after/plugin，
      此時，需要將覆蓋配置文件放置在該目錄中

    - 範例，[whichkey](https://github.com/folke/which-key.nvim/tree/main/plugin)
      - 預設配置文件位置，`which-key.nvim/plugin/which-key.nvim`
      - 使用 after 目錄，`.config/nvim/after/plugin/which-key.rc.lua`

    - 範例，[telescope](https://github.com/nvim-telescope/telescope.nvim)
      - 預設配置文件位置，`telescope.nvim/plugin/telescope.lua`
      - 使用 after 目錄，`.config/nvim/after/plugin/telescope.rc.lua`
    
  - ftplugin目錄，`~/.config/nvim/ftplugin`，file-type-plugin，文件類型特定的配置，例如，
    - ~/.config/nvim/ftplugin/py.lua
    - ~/.config/nvim/ftplugin/py_*.lua
    - ~/.config/nvim/ftplugin/py/aa.lua
    - 以上三中方式，都會在開啟 python 讀取以上的配置

  - plugin目錄，`~/.config/vim/plugin`，用於取代並替換內建的插件

## lua-api 的使用

- 執行單行 lua 代碼，`:lua echo "123"`

- [lua-api 的使用](https://neovim.io/doc/user/lua.html#vim.wo)
  - `vim.api.x` 是通用的api，不會再變動，推薦使用，可以透過以下方式簡化
    ```lua
    local Api = vim.api 
    ```

  - `vim.x` 是新api，有可能會變動
  
- 導入外部的 lua 文件
  ```lua
  -- @ ~/.config/nvim/init.lua

  -- 該語句會尋找 lua 目錄，並導入 ~/.config/nvim/lua/numToStr/settings.lua 的檔案
  require('numToStr.settings')
  ```

- 使用 `use(插件名, 配置選項)` 指定使用的插件

- 常用屬性
  - `vim.api.nvim_set_option_value(屬姓名, 屬性值)`，通用，可設置任意作用域

  - `vim.g.x`，設置global的相關屬性
    - vim.g.mapleader = " "，設置 leader-key 
    - 關閉預設的文件瀏覽器，安裝第三方的文件瀏覽器時才需要
      - vim.g.loaded_netrw = 1
      - vim.g.loaded_netrwPlugin = 1

  - `vim.b.x`，設置 buffer-scope 的相關屬性
  - `vim.w.x`，設置 window-scope 的相關屬性
  - `vim.t.x`，設置 TabPage-scope 的相關屬性
  - `vim.o.x`，設置 buffer-scope 和 window-scope 的相關屬性，
    - 顯示相關
      - vim.o.termguicolors = true，是否使用 24bit 的真實色彩
      - vim.o.number = true，是否顯示行號
      - vim.o.numberwidth = true，設置行號的顯示位數
      - vim.o.relativenumber = true，是否顯示相對行號
      - vim.o.signcolumn = "yes"，是否顯示gutter 符號列
      - vim.o.cursorline = true，是否高亮當前行
      - vim.o.winbar = "%= %m %f"，顯示文件的修改狀態和文件
        - %= 向右對齊
        - %m 顯示修改文件的狀態
        - %f 顯示檔案路徑

    - 分割視窗相關
      - vim.o.splitright = true，是否將分割視窗預設放在右側
      - vim.o.splitbelow = true，是否將分割視窗預設放在下方

    - 編輯相關
      - vim.o.expandtab = true，是否將 \t 轉換為空格
      - vim.o.smarttab = true，是否自動根據上一行的縮進，調整當前行的縮進
      - vim.o.cindent = true，是否使用C語言的縮進規則
      - vim.o.autoindent = true，是否自動縮進新的一行
      - vim.o.shiftwidth = 2
      - vim.o.wrap = true，是否自動換行
      - vim.o.textwidth，控制自動換行的字數長度
      - vim.o.listchars，定義非可見字符的顯示，例如，空白時顯示.
        > vim.o.listchars = "trail:·,nbsp:◇,tab:→ ,extends:▸,precedes:◂"
    
    - 編輯器功能相關
      - vim.o.clipboard = "unnamedplus"，設置VIM使用哪一種剪貼版，unnamed | unnamedplus
      - `unnamed`，使用 " 字符剪貼版，預設值
      - `unnamedplus`，使用 + 字符剪貼版，為系統剪貼板
      - vim.o.ignorecase = true，查找和替換文本時，是否忽略大小寫
      - vim.o.smartcase = true，是否根據查找的字符串是否包含大寫字母，來決定是否區分大小寫
      - vim.o.backup = false，是否創建備份文件
      - vim.o.swapfile = false，是否創建 swapfile
      - vim.o.writebackup = false，在寫入文件時是否創建備份文件
      - vim.o.mouse = "a"，啟用滑鼠操作的時機，a | n | v
        - n，啟用滑鼠移動功能
        - v，啟用文本選擇功能
        - a，同時啟用以上兩種功能

- 設置按鍵，
  - 方法1，`vim.api.nvim_set_keymap(模式, 新按鍵, 舊按鍵或命令, 其他選項)`
  - 方法2，`vim.keymap.set(模式, 新按鍵, 舊按鍵或命令, 其他選項)`
  - 方法3，透過 `<Plug>(自定義名稱)`設置鍵位
    - 使用場景
      
      若`插件A`和`插件B`都提供了 c-n 的按鍵

      - 若使用一般的按鍵設置方式，`vim.keymap.set("n","p", "<c-n>", {})`，
        此設置方式無法看出插件A或插件B的 c-n 按鍵被取代

      - 若插件A為c-n按鍵提供了別名，`<Plug>(plug-a)`，設置方式就可以改成，`vim.keymap.set("n","p", "[[<Plug>(plug-a)<c-n>]]", {})`
        此設置方式可以看出插件A的 c-n 按鍵被取代為 p

      注意`<Plug>特殊鍵位名`通常由插件作者定義，可參考repo中的plugin目錄，
      例如，[vim-asterisk](https://github.com/haya14busa/vim-asterisk/blob/master/plugin/asterisk.vim)

      此方法適用於發生按鍵衝突時，為具有相同按鍵的鍵位，添加不同的鍵名

    - 自定義特殊鍵位名
      ```lua
      -- 定義 <Plug>MyCustomMapping 的實際內容
      vim.keymap.set('', '<Plug>MyCustomMapping', function()
        print("Hello, world!")
      end, {noremap = true})

      -- 在插入模式下将 <C-l> 映射到 <Plug>MyCustomMapping
      vim.api.nvim_set_keymap('i', '<C-l>', '<Plug>MyCustomMapping', {silent = true})
      ```

- 執行 vim-script 語句，`vim.cmd()`

- 建立自動命令(autocmd)，
  - 格式
    ```lua
    augroup 自定義群組名稱    -- 定義名稱
      autocmd!               -- 清除現有的自動命令，以確保沒有重複的自動命令被執行
      -- autocmd 事件         作用文件的表達式 (可選)嵌套標記 要執行的命令
      autocmd    BufWritePre  *                             lua MiniTrailspace.trim()
    augroup END
    ```
  - 觸發事件的種類
    - BufWritePost: 寫入 buffer 後觸發，常用於寫入檔案後的處理
    - BufWinEnter: 在 window 顯示 buffer 前
    - 其他觸發事件，見 [autocmd文檔](https://yianwillis.github.io/vimcdoc/doc/autocmd.html)
  
  - 範例，以[安裝 Packer 插件](plugin-manager.md#插件管理-packernvim)為例
    ```lua
    vim.api.nvim_create_autocmd('BufWritePost', {
        group = vim.api.nvim_create_augroup('PACKER', { clear = true }),
        pattern = 'plugins.lua',
        command = 'source <afile> | PackerSync',
    })
    ```

- 範例，[使用lua語法配置packer插件](plugin-manager.md#插件管理-packernvim)

- 範例，寫入檔案前，自動觸發行尾空白清除
  ```lua
  vim.api.nvim_exec([[
    augroup mini.trim
      autocmd!
      autocmd BufWritePre * lua MiniTrailspace.trim()
    augroup END
  ]], false)  -- 寫入檔案前，自動清除行尾空白
  ```

## 常用配置範例

- `範例`，變更 `<leader> 鍵`的定義，以設置為空白鍵為例
  ```lua
  -- init.vim 的版本
  let mapleader = " "

  -- init.lua 的版本
  vim.g.mapleader = " "
  ```

- `範例`，建立自定義的快速鍵
  - 建立命令的快速鍵必須在命令的後方加上`<CR>`
  - 組合鍵之間`不要有空白`，除非真的需要輸入空白鍵
  - 按鍵代號
    - `<C-c>`: ctrl + c
    - `C`: shift + c
    - `Left`: 左方向鍵
    - 其他可參考，`:h keycode`

  - nnoremap 的意義
    - n 代表只會在 normal-mode 生效
    - nore 代表不會觸發已經映射的按鍵，remap 代表修改已經映射的按鍵
    - map 代表將該按鍵映射到其他按鍵或命令上

  - 範例，自定義命令的快速鍵
    ```lua
    -- init.vim 的版本
    -- 格式: nnoremap 按鍵 命令
    nnoremap <leader>c :nohlsearch<CR>

    -- init.lua 的版本
    
    -- 透過 ctrl + n + h 觸發
    vim.api.nvim_set_keymap("n", '<C-n>h', ':nohlsearch<CR>', { noremap = true, silent = true })  
    
    -- 透過 keymap 設置按鍵
    vim.keymap.set(
      'n',  -- 在 normal-mode 下生效
      '<leader>sx', -- 建立按鍵
      require("telescope.builtin").resume, -- 按鍵觸發後要執行的函數
      { noremap = true, silen = true, desc = "my keybinding"} -- 函數需要的參數
    )
    ```

- `範例`，建立 autocmd，以偵測 plugin.lua 為例
  - `版本1`，利用 lua-api 語法
    ```lua
    vim.api.nvim_create_autocmd('BufWritePost', {
      group = vim.api.nvim_create_augroup('PACKER', { clear = true }),
      pattern = 'plugins.lua',
      command = 'source <afile> | PackerSync',
    })
    ```

  - `版本2`，利用 vim-script 語法
    ```vim
    augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
    augroup END
    ```

- `範例`，修改組合鍵的判定時間
  ```lua
  -- init.vim 的版本
  set timeout timeoutlen=500

  -- init.lua 的版本
  vim.cmd('set timeout timeoutlen=500')
  ```

- `範例`，顯示 line-number
  ```lua
  vim.cmd('set number')
  ```

- `範例`，設置 tab 的縮排為兩個空白
  ```lua
  vim.o.tabsto
  vim.o.shiftwidth = 2
  ```

- `範例`，將`預設新的水平分割視窗`開在下方，將`預設新的垂直分割視窗`開在右側
  ```lua
  vim.cmd("set splitbelow splitbright")
  ```

## 推薦配置和配置結構

- 推薦配置結構
  - step1，在 `~/.config/nvim/init.lua` 引入`頂層配置檔`，並依照檔名進行分類
    ```lua
    -- 頂層配置檔
    require("ensure-packer")  -- 用於安裝、設置packer
    require("install-plugin") -- 利用 packer 安裝第三方插件
    require("keybindings")  -- 按鍵配置
    require("settings")     -- 編輯器配置
    ```

  - step2，將配置文件依照下述方式放置
    - `頂層配置檔`放在 `~/.config/nvim/lua` 的目錄下
    - `插件配置文件`放在 `~/.config/nvim/lua/plugin-config` 的目錄下
    - 所有要使用的插件，必須
      - 2-1，在 `~/.confing/nvim/lua/install-plugin.lua` 中，透過`use 語句`進行安裝
      - 2-2，在 `~/.confing/nvim/lua/plugin-config` 的目錄中，建立對應的配置檔
  
    - 目錄結構
      ```shell
      /home/vagrant/.config/nvim
      ├── lua
      │   ├── plugin-config
      │   │   ├── lualine.lua
      │   │   ├── mini-nvim.lua
      │   │   ├── nvim-tree.lua
      │   │   ├── telescope.lua
      │   │   ├── tokyonight.lua
      │   │   └── whichkey.lua
      │   ├── ensure-packer.lua
      │   ├── install-plugin.lua
      │   ├── keybindings.lua
      │   └── settings.lua
      └── init.lua
      ```

- 插件管理器配置，`~/.config/nvim/lua/ensure-packer.lua`
  - 負責檢查 packer 是否安裝，若否，自動下載並安裝 packer
  - 參考，[packer.nvim的使用](plugin-manager.md#插件管理-packernvim)

- 利用 packer 安裝插件，`~/.config/nvim/lua/install-plugin.lua`
  - 建立自動命令，當 `install-plugin.lua` 的檔案有變動時，自動執行 `:PackerSync`
  - 負責啟動 packer
  - 負責指定要安裝那些插件
  - 參考，[packer.nvim的使用](plugin-manager.md#插件管理-packernvim)
  - 參考，[插件的配置](plugin.md)

- 按鍵配置，`~/.config/nvim/lua/keybindings.lua`
  ```lua
  local function map (mode, key, value)
    vim.keymap.set(mode, key, value, {noremap = true, silent = true})
  end

  vim.g.mapleader = " "

  map("i", '<C-a>', '<C-v>')
  map("n", 'nh', ':nohlsearch<CR>')
  map("n", '<C-Left>', ':vertical resize +3<CR>')
  map("n", '<C-Right>', ':vertical resize -3<CR>')
  map("n", '<C-Left>', ':vertical resize +3<CR>')
  map("n", '<C-Up>', ':resize +3<CR>')
  map("n", '<C-Down>', ':resize -3<CR>')
  map("n", '<C-t>', ':vs|terminal<CR>')
  ```

- 編輯器配置，`~/.config/nvim/lua/settings.lua`
  ```lua
  local o = vim.o

  o.termguicolors = true  -- 使用 24bit 的真實色彩
  o.number = true         -- 顯示行號
  o.numberwidth = 2       -- 設置行號寬度
  o.relativenumber = true -- 行號顯示相對位置

  o.signcolumn = "auto:1" -- 自動選擇是否顯示狀態列，並設置狀態列寬度為1
  o.cursorline = true     -- 高亮當前選擇行

  o.splitright = true     -- 將預設的水平分割視窗，在右側開起
  o.splitbelow = true     -- 將預設的垂直分割視窗，在下方開起

  o.tabstop = 2           -- tab 的寬度為兩個空格
  o.shiftwidth = 2        -- shift 的移動距離為兩個空格
  o.expandtab = true      -- 將 \t 轉換為空格
  o.smarttab = true       -- 自動根據上一行的縮進，調整當前行的縮進

  o.cindent = true        -- 套用C語言的縮進規則
  o.autoindent = true     -- 開啟自動縮進

  o.wrap = true           -- 開啟自動換行
  o.textwidth = 300       -- 產生自動換行的字數限制

  o.list = true           -- 顯示非字符
  o.listchars = "trail:·,nbsp:◇,tab:→ ,extends:▸,precedes:◂"  -- 設置非字符的替代顯示方式

  o.clipboard = "unnamedplus" -- 使用系統剪貼板
  o.ignorecase = true         -- 查找和替換文本時，忽略大小寫
  o.smartcase = true          -- 查找的字符串時，自動判斷是否需要區分大小寫

  o.backup = false            -- 是否創建備份文件
  o.writebackup = false       -- 在檔案寫入後，不建立 backup 檔案
  o.swapfile = false          -- 不建立 swapfile 檔案

  o.mouse = "a"               -- 啟用滑鼠的移動和文本選擇功能
  ```

## 添加插件流程

- 注意，此流程僅使用於[推薦配置和配置結構](#推薦配置和配置結構)

- step1，在 `~/.config/nvim/lua/install-plugin.lua` 檔案中，在 packer 中，利用 `use { ... }` 語句進行安裝
- step2，在 use { ... } 語句中，透過 `config參數`，可以`直接配置插件`，或`指定配置檔的路徑`
- step3，使用 `:PackerSync` 進行安裝並重新編譯 (若有配置自動更新的命令，此步驟可省略)
- step4，重新啟動 neovim

## ref

- [編輯器配置範例](https://github.com/elken/nvim/blob/master/init.lua)
- [Configuring Neovim With Lua](https://www.youtube.com/watch?v=m62UCkdQ8Ck&list=PL5--8gKSku15tivUyt0D-mERePLEzrWUz&index=1)
