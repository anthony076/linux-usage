## [插件] lsp (language-server-protocol) 的相關配置

- 功能，提供程式語言多種便利的功能，例如，自動補全、語法高亮、定義跳轉、追蹤引用、變數一次性變更 ... 等

- neovim 內建 `lsp-client`，需要支援程式語言常見的功能，
  需要安裝該語言的 lsp-server，`由 lsp-server 提供該語言的支持`

- nvim-lsp-installer 插件，用於簡化 lsp-client 對 lsp-server 的安裝和設置，
  已經停止維護，改用 `mason.nvim` 插件取代

- 插件
  - lsp必要插件
    - ~~williamboman/nvim-lsp-installer，用於簡化 lsp-server 的安裝，已停止維護~~
    - williamboman/mason.nvim，新版本的 lsp-server 安裝插件，用於取代 nvim-lsp-installer 插件
    - williamboman/mason-lspconfig.nvim，作為 mason 和 nvim-lspconfig 之間的橋樑
    - neovim/nvim-lspconfig，用於簡化 lsp-client 對 lsp-server 的配置

  - lsp進階插件(推薦插件)
    - [glepnir/lspsaga.nvim](https://github.com/glepnir/lspsaga.nvim)，
      替換內建功能的UI和效能，並提供額外的新功能，選用

    - onsails/lspkind.nvim，在代碼提示中，以圖示代替文字分類說明，選用

    - 自動補全系列
      - hrsh7th/nvim-cmp，自動補全引擎，自動補全的主要插件，必要
      - hrsh7th/cmp-buffer，字串的自動補全，提供字串源，選用
      - hrsh7th/cmp-path，路徑的自動補全，提供路徑源，選用
      - hrsh7th/cmp-cmdline，命令模式下的自動補全，提供命令源，選用
      - hrsh7th/cmp-nvim-lsp，提供語法的自動補全，提供語法源，選用

    - 代碼片段系列 (snippets)
      - 關係圖: `nvim-cmp` <- `cmp_luasnip` <- `LuaSnip` <- `friendly-snippets`
      - L3MON4D3/LuaSnip，代碼片段引擎，提供代碼片段的主要插件
      - rafamadriz/friendly-snippets，為 LuaSnip 提供代碼片段的來源
      - saadparwaiz1/cmp_luasnip，代碼片段的自動補全

  - 其他插件
    - 顯示縮進線，[indent-blankline.nvim](https://github.com/lukas-reineke/indent-blankline.nvim)
    - 顯示色碼的顏色，[nvim-colorizer.lua](https://github.com/norcalli/nvim-colorizer.lua)
    - 為單字添加括號，[vim-surround](https://github.com/tpope/vim-surround)，推薦，使用更直覺
    - 為單字添加括號，[mini.surround](https://github.com/echasnovski/mini.surround)
    - 括號自動補全，[mini.pairs](https://github.com/echasnovski/mini.pairs)
    - 語法高亮，[nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter) 

- Mason 提供的功能
  - `LSP`: Language-Server-Protocol 的安裝和管理
  - `DAP`: Debug-Adapter-Protocol 的安裝和管理
    - 功能1，可使應用程式啟動至 debug-mode
    - 功能2，將一個執行中的應用程式附加到 dap 中進行 debug
    - 功能3，設置中斷點
    - 功能4，檢測應用程式狀態
  - `Linter`: 代碼提示插件的安裝與管理
  - `Formatter`: 格式代碼插件的安裝與管理

- lsp是懶加載的，`非專案的單一文件`需要手動執行`:LspStart`以啟動 lsp-server
  
  當 lsp-client (neovim) 偵測到特定類型的文件，並且確認根目錄 (配置 root_dir 或 root_file 或 root_pattern) 後，
  
  才會將 lsp-client attach 到對應的 lsp-server，此時，lsp 的功能才會有作用

  可以透過 `:LspInfo` 檢查 lsp-client 是否 attach lsp-server，
  若是，會出現 `1 client(s) attached to this buffer`

  - 對於能夠偵測到 root directory 的檔案，lsp 會`自動` attach lsp-server
  - 對於不能偵測到 root directory 的檔案，可以`手動`透過 `:LspStart` 啟動

- 設置`lsp快速鍵`的兩種方式，
  - 概念，對於 lsp 相關的快速鍵，預期的效果是
    - 只有開啟 lsp-server 對應的文件類型時，例如，*.py 或 *.lua，lsp 相關的快速鍵才有效
    - 若開啟一般文件時，lsp 相關的快速鍵應該是無效的
    - 有以下兩種方法可以實現 lsp 快速鍵的設置
   
  - 方法1，在 setup() 中設置 on_attach 參數，並透過 `vim.keymap.set()` 設置快速鍵

  - 方法2，透過 `vim.api.nvim_create_autocmd()` 綁定 `LspAttach 事件`，
    參考，[nvim-lspconfig提供的範例](https://github.com/neovim/nvim-lspconfig)

- 常用快速鍵
  - `<ctrl> + <o>` 或 `<ctrl> + <i>`: 跳轉後的返回，返回原文件

## 範例，基本安裝(最小安裝)

- 必要插件
  - williamboman/mason.nvim
  - williamboman/mason-lspconfig.nvim
  - neovim/nvim-lspconfig

- 安裝，`~/.config/nvim/lua/install-plugin.lua`，以 packer 安裝為例
  ```lua
  use {
    "williamboman/mason.nvim",

    requires = {
      "williamboman/mason-lspconfig.nvim",
      "neovim/nvim-lspconfig",
    },

    config = 'require("plugin-config/lsp")'
  }
  ```

- 配置，`~/.config/nvim/lua/plugin-config/lsp.lua`
  ```lua
  require("mason").setup()

  require("mason-lspconfig").setup {
    ensure_installed = { "lua_ls" }
  }

  require("lspconfig").lua_ls.setup {
    settings = {
      Lua = {
        diagnostics = {
          -- 關閉 lsp 無法偵測到 vim 的提醒
          globals = { 'vim' }
        },
      },
    },
  }
  ```

- 設置按鍵
  ```lua
  local wk = require("which-key")

  -- 建立自定義選單和快速鍵
  local mappings = {
    ... 內容省略 ...
    l = {
      name = "lsp",
      n = { vim.lsp.buf.rename, "rename" },
      c = { vim.lsp.buf.code_action, "code action" },
      d = { vim.lsp.buf.definition, "definition" },
      i = { vim.lsp.buf.implementation, "implementation" },
      r = { require("telescope.builtin").lsp_references, "references" },
      k = { vim.lsp.buf.hover, "hover" },
    },
  }

  local opts = {
    prefix = "<leader>",  -- 觸發自定義選單的按鍵
    nowait = true,
  }

  -- 註冊自義選單
  wk.register(mappings, opts)
  ```

- 使用
  - `:Mason`，打開安裝介面，檢視已經安裝的 lsp-server
  - `:LsoInstall <lsp-server名>`，安裝指定的lsp-server
  - `<leader>ln`，一次性變更當前文件內所有變數名  (rename)
  - `<leader>lc`，執行 code action  (code-action)
  - `<leader>ld`，查看定義  (definition)
  - `<leader>li`，查看實現  (implementation)
  - `<leader>lr`，以 telescope 查看所有的引用  (references)
  - `<leader>lk`，顯示類型  (hover)

## 範例，基本安裝 + 進階安裝

- 完整範例，見 [example-config](example-config/)

- 安裝的插件
  - [基本安裝](#範例基本安裝最小安裝)
    - williamboman/mason.nvim
    - williamboman/mason-lspconfig.nvim
    - neovim/nvim-lspconfig

  - 進階安裝
    - glepnir/lspsaga.nvim
    - onsails/lspkind.nvim
    - 自動補全系列
      - hrsh7th/nvim-cmp，使用範例見，[nvim-cmp的使用](plugin.md#插件-nvim-cmp-自動補全)
      - hrsh7th/cmp-buffer
      - hrsh7th/cmp-path
      - hrsh7th/cmp-cmdline
      - hrsh7th/cmp-nvim-lsp
    - 代碼片段系列 (snippets)
      - L3MON4D3/LuaSnip
      - rafamadriz/friendly-snippets
      - saadparwaiz1/cmp_luasnip
  
- 以 packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    ----------
    -- 基本安裝: 提供 lsp 基本功能
    ----------
    use {
      "williamboman/mason.nvim",
      event = 'BufRead',
      requires = {
        "williamboman/mason-lspconfig.nvim",
        "neovim/nvim-lspconfig",
      },

      config = 'require("plugin-config/lsp/mason")'
    }
    
    ----------
    -- 進階安裝: 增強 lsp 功能
    ----------

    -- 替換內建功能的UI和效能
    use { 
      "glepnir/lspsaga.nvim",
      requires = {
        { "nvim-treesitter/nvim-treesitter" },
        { "nvim-tree/nvim-web-devicons" },
      },
    }

    -- 讓自動補全選單顯示圖示
    use { "onsails/lspkind.nvim" }

    -- 代碼片段系列
    use { "L3MON4D3/LuaSnip" }
    use { "saadparwaiz1/cmp_luasnip" }
    use { "rafamadriz/friendly-snippets" }

    -- 自動補全系列
    use { "hrsh7th/nvim-cmp" }
    use { "hrsh7th/cmp-buffer" }
    use { "hrsh7th/cmp-path" }
    use { "hrsh7th/cmp-cmdline" }
    use { "hrsh7th/cmp-nvim-lsp", config = 'require("plugin-config/lsp/nvim-cmp")'}  -- code syntax autocompletion source for nvim-cmp

    -- formatting and linting

    -- 用於透過 mason 自動安裝 formatting、linting 相關插件
    use { "jay-babu/mason-null-ls.nvim" }  
    -- formatting 和 linting 主要插件
    use { "jose-elias-alvarez/null-ls.nvim", config = 'require("plugin-config/lsp/null-ls")' } 
    ```

  - lspsaga配置，`~/.config/nvim/lua/plugin-config/lsp/lspsaga.lua`
    ```lua
    local saga_status, saga = pcall(require, "lspsaga")
    if not saga_status then
      return
    end

    saga.setup({
      -- keybinds for navigation in lspsaga window
      scroll_preview = { scroll_down = "<C-f>", scroll_up = "<C-b>" },
      -- use enter to open file with definition preview
      definition = {
        edit = "<CR>",
      },
      ui = {
        colors = {
          normal_bg = "#022746",
        },
      },
    })
    ```

  - nvim-cmp自動補全相關設置，，`~/.config/nvim/lua/plugin-config/lsp/nvim-cmp.lua`
    ```lua

    -- 設置 insert-mode 下可用的自動補全
    vim.opt.completeopt = "menu,menuone,noselect"

    local from_vscode_status, from_vscode = pcall(require, "luasnip.loaders.from_vscode")

    if not from_vscode_status then
      return
    end

    from_vscode.lazy_load()

    ----
    local cmp_status, cmp = pcall(require, "cmp")
    if not cmp_status then
      return
    end

    ----
    local luasnip_status, luasnip = pcall(require, "luasnip")
    if not luasnip_status then
      return
    end

    ----
    local lspkind_status, lspkind = pcall(require, "lspkind")
    if not lspkind_status then
      return
    end

    -- 或加載自定義的 snippets
    -- 建立自定義的 snippet，參考，https://github.com/honza/vim-snippets/blob/master/snippets/lua.snippets
    --require("luasnip.loaders.from_vscode").lazy_load({ paths = { "./my-cool-snippets" } })

    cmp.setup({

      -- 將 snippet-engine 指定為 luasnip
      snippet = {
        expand = function(args)
          luasnip.lsp_expand(args.body)

          -- For `ultisnips` snippets-engine plugin
          -- vim.fn["UltiSnips#Anon"](args.body)

          -- For `snippy` snippets-engine plugin
          -- require'snippy'.expand_snippet(args.body)

        end,
      },

      -- 設置自動補全的來源
      sources = cmp.config.sources({
        { name = "nvim_lsp" }, -- get source from neovim lsp
        { name = "luasnip" }, -- get source from luasnip plugin
        { name = "buffer" },
        { name = "path" },
        { name = "cmd" },
      }),

      -- 設置快速鍵
      mapping = cmp.mapping.preset.insert({
        ["<C-k>"] = cmp.mapping.select_prev_item(), -- 在自動補全的選單中移動
        ["<C-j>"] = cmp.mapping.select_next_item(), -- 在自動補全的選單中移動 
        ["<C-b>"] = cmp.mapping.scroll_docs(-4),    -- 在自動補全的選單中捲動
        ["<C-f>"] = cmp.mapping.scroll_docs(4),     -- 在自動補全的選單中捲動
        ["<C-Space>"] = cmp.mapping.complete(),     -- show completion suggestions
        ["<C-e>"] = cmp.mapping.abort(),            -- close completion window
        ["<CR>"] = cmp.mapping.confirm({ select = false }),
      }),

      -- 變更自動補全選單的顯示格式，
      -- 將代碼提示的分類說明，改為顯示圖標
      formatting = {
        format = lspkind.cmp_format({
          maxwidth = 50,
          ellipsis_char = "...",
        }),
      },

    })

    -- command-mode(/) 下的自動補全
    cmp.setup.cmdline('/', {
      mapping = cmp.mapping.preset.cmdline(),
      sources = {
        { name = 'buffer' }
      }
    })

    -- command-mode(:) 下的自動補全
    cmp.setup.cmdline(':', {
      mapping = cmp.mapping.preset.cmdline(),
      sources = cmp.config.sources({
        { name = 'path' }
      }, {
        {
          name = 'cmdline',
          option = {
            ignore_cmds = { 'Man', '!' }
          }
        }
      })
    })
    ```

  - mason相關配置，`~/.config/nvim/lua/plugin-config/lsp/mason.lua`
    ```lua
    
    -- 配置 python 和 lua
    require("plugin-config.lsp.lua")
    require("plugin-config.lsp.python")

    ----------------
    -- 導入插件
    ----------------

    local mason_status, mason = pcall(require, "mason")
    if not mason_status then
      return
    end

    mason.setup()

    ---- 安裝 lsp-server
    local masonconfig_status, masonconfig = pcall(require, "mason-lspconfig")
    if not masonconfig_status then
      return
    end

    masonconfig.setup {
      ensure_installed = { "lua_ls" }   -- 自動安裝 lua 的 lsp-server
    }

    ---- 安裝 formatter、linter 相關套件
    local mason_null_ls_status, mason_null_ls = pcall(require, "mason-null-ls")
    if not mason_null_ls_status then
      return
    end

    -- 自動安裝 formmatting 和 linting 相關的套件
    --    check https://github.com/jay-babu/mason-null-ls.nvim/commit/e2144bd62b703c1fa298b9e154296caeef389553
    --    或在 :Mason 中檢查可用的 formmatting 和 linting 插件
    -- 注意，自動安裝不是必要的，也可以手動透過 :Mason 安裝
    mason_null_ls.setup({
      ensure_installed = {
        "flake8", -- python linter
        "black", -- python formatter
        "stylua", -- lua formatter
        "prettier", -- multi-language formatter, focus on web application
      },
    })

    ----------------
    -- 變更 diagnostics symbols 的顯示樣式
    ----------------

    local signs = { Error = " ", Warn = " ", Hint = "ﴞ ", Info = " " }

    for type, icon in pairs(signs) do
      local hl = "DiagnosticSign" .. type
      vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
    end
    ```

  - lua配置，`~/.config/nvim/lua/plugin-config/lsp/lua.lua`
    ```lua
    --------------
    -- 導入 
    --------------

    local lsp_keybinds = require("keybindings")

    local cmp_nvim_lsp_status, cmp_nvim_lsp = pcall(require, "cmp_nvim_lsp")
    if not cmp_nvim_lsp_status then
      return
    end

    local lspconfig_status, lspconfig = pcall(require, "lspconfig")
    if not lspconfig_status then
      return
    end

    --------------
    -- 設置 lua
    --------------

    local on_attach = function(_, bufnr)
      lsp_keybinds.set_lsp_keymap(bufnr)
    end

    -- 使用 cmp_nvim_lsp 取代 lsp 內建的自動補全
    local capabilities = cmp_nvim_lsp.default_capabilities()

    lspconfig.lua_ls.setup {
      -- 以 cmp_nvim_lsp 替換 lsp 原本的功能
      capabilities = capabilities,

      -- 設置 lsp 的 keybinding
      on_attach = on_attach,

      settings = {
        Lua = {
          diagnostics = {
            globals = { 'vim' }
          },
          workspace = {
            -- 讓lsp-server 知道lua的源碼位置，才能進行代碼追蹤
            library = {
              [vim.fn.expand("$VIMRUNTIME/lua")] = true,
              [vim.fn.stdpath("config") .. "/lua"] = true,
            },
          }
        },
      },
    }
    ```
  
  - python配置，`~/.config/nvim/lua/plugin-config/lsp/python.lua`
    ```lua
    --------------
    -- 導入 
    --------------

    local lsp_keybinds = require("keybindings")

    local cmp_nvim_lsp_status, cmp_nvim_lsp = pcall(require, "cmp_nvim_lsp")
    if not cmp_nvim_lsp_status then
      return
    end

    local lspconfig_status, lspconfig = pcall(require, "lspconfig")
    if not lspconfig_status then
      return
    end

    --------------
    -- 設置 pyright
    --------------

    -- 添加 python 庫的位置
    local cur_env_path = vim.env.PATH or ""
    local new_env_path = "/home/vagrant/.local/bin:" .. cur_env_path
    vim.env.PATH = new_env_path

    local on_attach = function(_, bufnr)
      lsp_keybinds.set_lsp_keymap(bufnr)
    end

    -- 使用 cmp_nvim_lsp 取代 lsp 內建的自動補全
    local capabilities = cmp_nvim_lsp.default_capabilities()

    lspconfig.pylsp.setup {
      -- 以 cmp_nvim_lsp 替換 lsp 原本的功能
      capabilities = capabilities,
      -- 設置 keybinding
      on_attach = on_attach,
    }
    ```

  - lsp按鍵配置，`~/.config/nvim/lua/keybindings.lua`

    此檔案會由 lua.lua 和 python.lua 進行調用，
    按鍵配置寫在 keybindings.lua 中統一管理按鍵
    ```lua
    local lsp_keybinds = {}

    lsp_keybinds.set_lsp_keymap = function(bufnr)

      local opts = { noremap = true, silent = true, buffer = bufnr }

      -- 以下建立 lspsage 的快速鍵，利用 lspsaga 取代 lsp 內建的功能

      local keymap = vim.keymap

      -- 顯示 definition和 references
      keymap.set("n", "gf", "<cmd>Lspsaga lsp_finder<CR>", opts)
      -- 跳到聲明處
      keymap.set("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
      -- 檢視定義和在視窗中編輯
      keymap.set("n", "gd", "<cmd>Lspsaga peek_definition<CR>", opts)
      -- 跳轉到實現
      keymap.set("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)
      -- 檢視可用的 code actions
      keymap.set("n", "<leader>ca", "<cmd>Lspsaga code_action<CR>", opts)
      -- 智慧更名，一次性變更所有相同的符號
      keymap.set("n", "<leader>rn", "<cmd>Lspsaga rename<CR>", opts)

      -- 顯示當前行的 diagnostics，只對有問題的行有效，用於顯示詳細錯誤訊息
      keymap.set("n", "<leader>D", "<cmd>Lspsaga show_line_diagnostics<CR>", opts)
      -- 顯示當前滑鼠位置的 diagnostics  只對有問題的位置有效，用於顯示詳細錯誤訊息
      keymap.set("n", "<leader>d", "<cmd>Lspsaga show_cursor_diagnostics<CR>", opts)
      -- 跳轉到當前文件中的上一個錯誤
      keymap.set("n", "[d", "<cmd>Lspsaga diagnostic_jump_prev<CR>", opts)
      -- 跳轉到當前文件中的下一個錯誤
      keymap.set("n", "]d", "<cmd>Lspsaga diagnostic_jump_next<CR>", opts)

      -- 顯示當前游標位置的說明文件，需要安裝 makdown 解析器
      keymap.set("n", "K", "<cmd>Lspsaga hover_doc<CR>", opts)
      -- 在右側顯示當前文件的綱要
      keymap.set("n", "<leader>o", "<cmd>Lspsaga outline<CR>", opts)

    end

    return lsp_keybinds
    ```
  
  - null-ls配置，`~/.config/nvim/lua/plugin-config/lsp/null-ls.lua`
    ```lua
    local setup, null_ls = pcall(require, "null-ls")
    if not setup then
      return
    end

    local formatting = null_ls.builtins.formatting    -- 用於設置 formatter
    -- local diagnostics = null_ls.builtins.diagnostics  -- 用於設置 linter

    null_ls.setup({
      sources = {
        --  設置提供 formatting 功能的來源插件
        formatting.black,     -- 需要透過mason安裝外部的 black 套件
        formatting.stylua,    -- 需要透過mason安裝外部的 stylua 套件
        formatting.prettier,  -- 需要透過mason安裝外部的 prettier 套件

        -- 設置 linting 功能的來源插件，並進行配置
        -- diagnostics.eslint_d.with({ -- js/ts linter  
        --   -- only enable eslint if root has .eslintrc.js (not in youtube nvim video)
        --   condition = function(utils)
        --     return utils.root_has_file(".eslintrc.js") -- change file extension if you use something else
        --   end,
        -- }),
      },

      -- 建立自動化命令, 寫入保存時，自動執行格式化
      on_attach = function(current_client, bufnr)

        local augroup = vim.api.nvim_create_augroup("LspFormatting", {})

        -- 判斷當前文件是否支援 formatting
        if current_client.supports_method("textDocument/formatting") then

          -- 清除當前文件的 LspFormatting 命令，改用 null-ls 執行
          vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })

          -- 建立新的 formatting 自動命令
          vim.api.nvim_create_autocmd("BufWritePre", {  -- buffer 寫入前
            group = augroup,
            buffer = bufnr,

            callback = function()   -- 定義要執行的內容
              vim.lsp.buf.format({
                filter = function(client)
                  --  only use null-ls for formatting instead of lsp server
                  return client.name == "null-ls"
                end,
                bufnr = bufnr,
              })
            end,

          })
        end

      end,
    })
    ```

## 範例，安裝 pyright 的 lsp-server

- 注意，完整範例，參考，[基本安裝+進階安裝](#範例基本安裝--進階安裝以-packer-安裝為例)

- step1，(選用) 透過Masony自動安裝 pyright，`:MasonInstall pyright`

- step1，(選用) 手動安裝 pyright
  ```shell
  sudo pacman -S npm python-pip python
  sudo npm i -g pyright 
  ```

- step2，將python庫的路徑添加到環境變數
  預設情況下，安裝的 python 庫會放在 `~/.local/bin` 的路徑中，
  需要確保該路徑在環境變數中，lsp 才能正確啟動 pyright 
  ```shell
  export PATH="/home/vagrant/.local/bin:$PATH"
  ```

- step3，利用 lspconfig 設置 pyright
  ```lua
  local lspconfig_status, lspconfig = pcall(require, "lspconfig")
  if not lspconfig_status then
    return
  end

  -- 設置 pylsp
  lspconfig.pylsp.setup {
    -- 以 lspsaga 替換 lsp 原本的功能
    capabilities = capabilities,
    -- 設置 keybinding
    on_attach = on_attach,
  }
  ```

## 範例，安裝 c/c++ 的 lsp-server

- [關於 c++ 的配置](https://www.jianshu.com/p/2d54e3774cde)

## 範例，透過 LuaSnip 建立自定義的代碼模板

- 概念
  - 幾個與snippet相關的按鍵配置
    
    - 按鍵1，展開 snippet

      注意，當 luasnip 獨立使用的時候才需要此按鍵，

      一般情況下，luasnip 會與 nvim-cmp 一起使用，此時，nvim-cmp 會管理 snippet 的展開，
      因此，只要選中指定的 snippet 並按下 Enter 鍵就會自動展開

      ```lua
      vim.keymap.set({"i", "s"}, "<a-j>", function()
        if ls.expand_or_jumpable() then
          ls.expand()
        end
      end)
      ```

    - 按鍵2，跳到下一個輸入點 (snippet有輸入點才需要)
      ```lua
      vim.keymap.set({"i", "s"}, "<a-j>", function()
        if ls.jumpable(1) then
          ls.jump(1)
        end
      end)
      ```
    
    - 按鍵3，跳到上一個輸入點 (snippet有輸入點才需要)
      ```lua
      vim.keymap.set({"i", "s"}, "<a-k>", function()
        if ls.jumpable(-1) then
          ls.jump(-1)
        end
      end)
      ```

  - 建立代碼模板的幾種方式
    - 方法1，使用 vscode 的語法建立代碼模板
      ```json
      // https://github.com/rafamadriz/friendly-snippets/blob/main/snippets/lua.json
      {
        "local": {
              "prefix": "l",
              "body": [
                  "local ${0}"
              ],
              "description": "create a variable"
          },
      }

      // 必須透過以下語句讀取 snippets，
      require("luasnip.loaders.from_vscode").lazy_load( ... )
      ```
    
    - 方法2，使用 SnipMate 的語法建立代碼模板
      ```s
      # https://github.com/honza/vim-snippets/blob/master/snippets/lua.snippets
      snippet fun
      function ${1:fname}(${2:...})
        $0
      end
      
      # 必須透過以下語句讀取 snippets，
      require("luasnip.loaders.from_snipmate").lazy_load( ... )
      ```
    
    - 方法3，使用 luasnip 提供的api，見以下範例

- 範例，使用 `luasnip-api`，建立自訂義模板
  ```lua
  -- https://github.com/L3MON4D3/LuaSnip/blob/b5a72f1fbde545be101fcd10b70bcd51ea4367de/Examples/snippets.lua#L501

  local ls = require("luasnip")

  ls.add_snippets("all", {
    ls.snippet(
      -- snippet info
      { name = "myname", trig = "sa",  dscr = "say hello" },

      -- real snippet，寫法1，不分行寫法
      --{ ls.text_node("hello"), ls.text_node("world") }  -- 印出 helloworld

      -- real snippet，寫法2，自動分行寫法
      --{ ls.text_node({ "hello", "world" }) }  -- 印出 hello\nworld

      -- real snippet，寫法3，含有輸入點的snippet
      { ls.text_node("hello "), ls.insert_node(1), ls.text_node(" nice "), ls.insert_node(2) } -- 印出 hello $1 nice $2
    )

  })
  ```

- 範例，使用 `vscode` 的語法，建立自訂義模板
  - step1，建立 `~/.config/nvim/mysnippet` 的目錄
  
  - step2，建立 `~/.config/nvim/mysnippet/package.json` 的檔案

    package.json 是入口點，必要，用來映射語言含實際snippet檔案(*.json)位置之間的映射
    ```json
    {
      "contributes": {
        "snippets": [
          {
            "language": "lua",
            "path": "./lua.json"
          },
          {
            "language": "all",
            "path": "./all.json"
          }
        ]
      }
    }
    ```

  - step3，建立 `~/.config/nvim/mysnippet/lua.json` 的檔案，放置 lua 語言相關的代碼模板
    ```json
    {
      "sayWorld": {
        "prefix": "sw",
        "body": ["world"],
        "description": "say world"
      }
    }
    ```

  - step4，建立 `~/.config/nvim/mysnippet/all.json` 的檔案，放置 all 相關的代碼模板
    ```json
    {
      "sayHi": {
        "prefix": "sh",
        "body": ["Hi"],
        "description": "say hi"
      }
    }
    ```

  - step5，在配置文件中，利用 `luasnip.loaders.from_vscode` 指定 package.json 的位置
    ```lua
    -- 透過 :lua print(vim.fn.stdpath "config" .. "/mysnippet") 確認實際位置
    -- 實際位置為 ~/.config/nvim/mysnippet
    require("luasnip.loaders.from_vscode").load({
      paths = vim.fn.stdpath "config" .. "/mysnippet",
    })
    ```
  
  - step6，檢查，透過 `:LuaSnipListAvailable` 查看已經載入的 snippet

  - step7，使用，在有配置按鍵的狀況下，透過 alt+j 跳到下一個輸入點

## ref

- [安裝教學](https://zhuanlan.zhihu.com/p/444836713)
- [自動代碼補全](https://zhuanlan.zhihu.com/p/445331508)
- [Make Neovim BETTER than VSCode - LSP tutorial](https://www.youtube.com/watch?v=lpQMeFph1RE)
- [Neovim 内置 LSP 配置 ](https://zhuanlan.zhihu.com/p/444836713)
- [從 nvim-lsp-installer 升級到 mason.nvim](https://juejin.cn/post/7154005621887631396)
- [How I Setup Neovim On My Mac To Make It Amazing - Complete Guide](https://www.youtube.com/watch?v=vdn_pKJUda8)
  
- 配置範例
  - [lsp配置範例1](https://github.com/numToStr/dotfiles/blob/master/neovim/.config/nvim/lua/numToStr/plugins.lua)
  - [lsp配置範例2](https://github.com/josean-dev/dev-environment-files/blob/main/.config/nvim/lua/josean/plugins-setup.lua)
  - [lsp配置範例3](https://blog.csdn.net/qq_19078137/article/details/125694657)
  - [lsp配置範例4](https://www.gushiciku.cn/dl/18CYq/zh-tw)
  
  - [pyright設置範例](https://www.jianshu.com/p/2d54e3774cde)
  
  - 配置範例 @ amy-juan-li
    - [github](https://github.com/amy-juan-li/nvim-lua)
    - [webpage](https://levelup.gitconnected.com/a-step-by-step-guide-to-configuring-lsp-in-neovim-for-coding-in-next-js-a052f500da2)

- luasnip
  - [透過 luasnip-api 建立 snippet ](https://www.youtube.com/watch?v=ub0REXjhpmk)

