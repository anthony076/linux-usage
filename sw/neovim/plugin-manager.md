## [插件管理] vim-plug

- [官方網站](https://github.com/junegunn/vim-plug)

- 優缺點
  - 簡單易用，透過 init.vim 就可以安裝和配置
  - 支援異步操作、並行下載、延遲加載、條件加載
  - 容易管理，可配置性高
  - 缺，需要手動管理插件的依賴關係
  - 缺，需要手動清理不需要的插件
  - 缺，需要手動解決插件衝突的問題

## [插件管理] packer.nvim

- 安裝和配置 (lua-version)
  - 注意，要安裝的插件，需要寫在 `packer.startup( ... )` 中
  - 注意，安裝後的插件，可能會以下幾種方式啟動
    - 方法1，透過 `require().setup( ... )` 啟動
    - 方法2，以`命令`啟動
    - 方法3，同時使用以上兩種方式啟動
    - 方法4，透過插件提供的api啟動，
      > 例如，telescope-file-browser 需要透過 `telescope.load_extension("file_browser")` 啟動
    - 注意，正確啟動插件的方式，請參考插件使用說明

  - step1，在 `~/.config/nvim/init.lua` 導入插件相關的配置檔
    ```lua
    require("ensure-packer")  -- 用於安裝、設置packer
    require("install-plugin") -- 利用 packer 安裝第三方插件
    ```
  
  - step2，下載和配置 packer，`~/.config/nvim/lua/ensure-packer.lua`
    - 此配置檔會在neovim 啟動時檢查 packer 是否存在，若否，自動下載安裝
    - 利用 `:echo stdpath("data")` 可查看下載位置為 `~/.local/share/nvim`
    - 若 clone 成功，會在 `~/.local/share/nvim/site/pack/packer/start/packer.nvim` 看到下載的目錄
    - 若 packadd 的命令執行成功，會將 packer 添加到 `~/.local/share/nvim/plugins/packer.nvim`
    
    ```lua
    -- neovim 啟動時檢查 packer 是否存在，若否，自動下載安裝

    local fn = vim.fn
    local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"

    if fn.empty(fn.glob(install_path)) > 0 then
      packer_bootstrap = fn.system({ "git", "clone", "--depth", "1", "https://github.com/wbthomason/packer.nvim", install_path })
      vim.cmd([[packadd packer.nvim]])
    end

    -- 判斷 packer 是否安裝成功
    local status, packer = pcall(require, "packer")

    if not status then
      print("Packer is not installed")
      return
    end
    ```

  - step3，透過 packer 安裝第三方插件，，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    -- 建立自動命令，當 install-plugin.lua 變更時，自動觸發 PackerSync
    vim.cmd([[
      augroup packer_user_config
      autocmd!
      autocmd BufWritePost install-plugin.lua source <afile> | PackerSync
      augroup END
    ]])

    -- 啟動 packer
    local packer = require("packer")

    packer.startup(function(use)

      -- 避免 packer 被刪除
      use { "wbthomason/packer.nvim" }
      
      -- 追蹤並安裝第三方插件

      use { "folke/tokyonight.nvim", config = 'require("plugin-config/tokyonight")' }

      use { "nvim-lualine/lualine.nvim", config = 'require("plugin-config/lualine")' }

      -- 啟動 packer 後，檢查並安裝第三方插件
      if packer_bootstrap then
        packer.sync()
      end

    end)
    ```

## [插件管理] lazy.nvim
  
- [官方網站](https://github.com/kdheepak/lazygit.nvim)

- 優缺點
  - 不需要像 packer 一樣需要編譯插件
  - 由 lazy.nvim 管理插件的加載，只有在需要的時候才加載必要的插件
  - 由 lazy.nvim 管理插件的依賴，確保插件可以安裝並執行
  - 具有插件預緩存，提高插件加載速度

- 手動安裝 lazy.nvim
  - `方法1`，原生的lazy.nvim，未進行任何優化
    - 建立 `~/.config/nvim/init.lua`
    - 添加以下內容
      ```
      local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
      if not vim.loop.fs_stat(lazypath) then
        vim.fn.system({
          "git",
          "clone",
          "--filter=blob:none",
          "https://github.com/folke/lazy.nvim.git",
          "--branch=stable", -- latest stable release
          lazypath,
        })
      end
      vim.opt.rtp:prepend(lazypath)

      require("lazy").setup({
      })
      ```

  - `方法2`，使用 LazyVim 配置框架使用的預設配置和套件
    ```lua
    -- step1，下載配置文件
    git clone https://github.com/LazyVim/starter ~/.config/nvim
    -- step2，重啟 nvim
    ```
  
  - `方法3`，使用 LazyVim 配置框架使用的預設配置
    - 優點，可快速建立 lazy.nvim 的檔案結構和配置，但不安裝 LazyVim 的預設插件，

    - 複製 LazyVim 配置框架預設的配置，並進行修改
      `$ git clone https://github.com/LazyVim/starter ~/.config/nvim`，
      
      注意，此配置會安裝並載入 LazyVim 配置框架預設的插件，
      因此需要手動修改 lazy.lua

    - 修改 lazy.lua，以禁用LazyVim 配置框架預設的插件
  
      在 nano ~/.config/nvim/int.lua 中，會有 `require("config.lazy")` 的語句，
      會主動載入 `~/.config/nvim/lua/config/lazy.lua` 的檔案內容
      
      使用 nano 修改 lazy.lua，`nano ~/.config/nvim/lua/config/lazy.lua`
      ```lua
      ... 內容省略 ...
      
      -- 取消 LazyVim 的配置
      -- { "LazyVim/LazyVim", import = "lazyvim.plugins" },
      
      ... 內容省略 ...
      -- 確保啟動後，會自動載入 ~/.config/nvim/lua/plugins 目錄下的所有插件
      { import = "plugins" },
      ```
    
    - 變更主題，以 tokyonight 為例
      - 方法1，lazy.nvim 安裝，lazy.nvim 載入
        ```lua
        -- step1，建立 `~/.config/nvim/lua/plugins/tokyonight.lua`，並添加以下內容
        return {
          {
            "folke/tokyonight.nvim", 
            lazy = false,    -- 啟動 nvim 後立即載入
            config = function() -- 載入後執行以下設置
              vim.cmd([[colorscheme tokyonight]]) -- 將系統主題設置為 tokyonight
            end,
          }
        }
        -- step2，啟動 nvim > `:Lazy` > `<shift> + <i>` 進行安裝
        -- step3，重新啟動 nvim
        ```
      
      - 方法2，方法1，lazy.nvim 安裝，neovim 載入，
        
        優點，載入的速度更快
        ```lua
        -- step1，建立 ~/.config/nvim/lua/plugins/tokyonight.lua，並添加以下內容
        return {
          {
            "folke/tokyonight.nvim", 
            lazy = false,    -- 啟動 nvim 後立即載入
          }
        }
        -- step2，修改 ~/.config/nvim/init.lua，並添加以下
        vim.cmd([[colorscheme tokyonight]])

        -- step3，啟動 nvim > `:Lazy` > `<shift> + <i>` 進行安裝
        -- step4，重新啟動 nvim
        ```

- 插件
  - 插件安裝腳本: lazy.nvim 會根據 `~/.config/nvim/lua/plugins/套件名.lua` 的內容進行安裝
  - 插件安裝路徑: lazy.nvim 安裝好的插件會放在，`~/.local/share/nvim/lazy/套件名` 的路徑下

  - 注意，不是所有的 *.nvim 的插件都能直接安裝
    - 不能直接安裝，[markdown-preview.nvim](https://github.com/iamcco/markdown-preview.nvim)
    - 可以直接安裝，[symbols-outline.nvim](https://github.com/simrat39/symbols-outline.nvim)
      
  - 範例，安裝新插件範例
    - step1，修改配置，確保啟動 neovim 時會自動載入 plugin 目錄中的套件
      ```lua
      -- 修改 ~/.config/nvim/lua/config/lazy.lua
      
      ... 內容省略 ...

      require("lazy").setup(
        {
          spec = {
            ... 內容省略 ...
            { import = "plugins"}   -- 確保有添加此行
          }
        }
      )
      ... 內容省略 ...

      ```

    - step2，建立安裝插件的腳本，以 `markdown-preview.nvim 插件` 為例
      - 建立 `~/.config/nvim/lua/plugins/markdown-preview.lua`
      - 添加以下內容
        ```lua
        return {
          {
            "simrat39/symbols-outline.nvim"
          }
        }
        ```

    - step3，打開 lazy 的安裝頁面，`<space> + <l> + <I>` 或 `:Lazy instll`，lazy 會自動偵測並進行安裝

    - step4，(選用) 刪除插件，刪除 plugin 的安裝腳本後，
      - 打開 lazy 頁面 `<space> + <l> + <X>` 或 `:Lazy clean`，lazy 會自動偵測並進行移除

  - 範例，安裝插件 + 定義插件觸發按鍵
    - 建立 `~/.config/nvim/lua/plugins/file-browser.lua`

    - step1，添加以下內容
      ```lua
      return {
        -- 安裝 telescope-file-browser 插件
        "nvim-telescope/telescope-file-browser.nvim",

        -- 指定依賴 
        dependencies = { 
          "nvim-telescope/telescope.nvim", 
          "nvim-lua/plenary.nvim" 
        },
        
        -- 定義觸發按鍵
        keys = {
          {
            "<leader>sB",
            ":Telescope file_browser path=%:p:h=%:p:h<cr>",
            desc = "Browser Files",
          }
        },
        
        -- 配置 telescope-file-browser 插件
        -- 載入 telescope 時，一起加載 file_browser
        config = function()
          require("telescope").load_extension("file_browser")
        end,
      }
      ```
    
    - step2，按下 `<space> + <l>` 呼叫 lazy.nvim 套件管理頁面後，`<shift> + <I>`進行安裝
    
    - step3，寫入檔案後，重啟 nvim
    
    - step4，在 neovim 中，透過 `<space> + <s> + <B>`，進行測試

  - 範例，修改已安裝套件的行為，以 alpha-nvim 插件為例
    - alpha-nvim 是起始頁面的歡迎詞
      
      alpha-nvim 是 lazyvim 內建的插件，預設會自動安裝，
      透過自定義的腳本修改 alpha-nvim 的行為

    - step1，建立 `~/.config/nvim/lua/plugins/myalpha.lua`

    - step2，添加以下內容
      ```lua
      return {
        -- 指定要修改的插件
        "goolord/alpha-nvim",
        
        -- 修改行為
        opts = function(_, opts)
          local logo = [[
            Power by Anthony.Y
          ]]

          opts.section.header.val = vim.split(logo, "\n", {trimempty = true})
        end,
        
      }
      ```

    - step3，寫入檔案後，重啟 nvim，歡迎詞會改成 Power by Anthony.Y

  - 範例，禁用插件
    - step1，建立 `~/.config/nvim/lua/plugins/disabled.lua`

    - step2，添加以下內容
      ```lua
      return {
        {"aa/bb", enabled = false},
        {"cc/dd", enabled = false},
      }
      ```
      
    - step3，寫入檔案後，重啟 nvim

## ref

- lazy.nvim
  - [Migrating from Packer.nvim to Lazy.nvim](https://www.youtube.com/watch?v=aqlxqpHs-aQ)
  - [lazy.nvim官網](https://github.com/folke/lazy.nvim)
  - [使用lazy.nvim作為Neovim插件管理器](https://zhuanlan.zhihu.com/p/599306319?utm_id=0)

- packer
  - [Packer插件管理与配置](https://zhuanlan.zhihu.com/p/438380547)