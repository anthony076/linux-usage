## 檢查插件依賴和使用插件

- 執行插件提供的檢查功能
  - 部分插件會提供 `:checkhealth 插件名`功能，可以檢查插件依賴是否具備，或是其他檢查功能
  - 例如，`:checkhealth`，檢查所有具 checkhealth 功能的插件
  - 例如，`:checkhealth telescope`，檢查 telescope 的依賴
  - 例如，`:checkhealth which_key`，檢查衝突的按鍵

- 執行插件的幾種方式
  - `方法1`，透過插件提供的`命令`，
    > 注意，命令僅提供插件常用的功能

  - `方法2`，透過插件提供的`API`
    - 插件提供的lua-api可執行任意的功能
    - 查詢插件提供的api，`:h 插件名.功能函數`
    - 執行插件提供的api，`:lua 插件名.功能函數`

## 推薦插件

- 插件比較
  - [Awesome Neovim Overview](https://www.trackawesomelist.com/rockerBOO/awesome-neovim/readme/)
  - [Collections of awesome neovim plugins](https://github.com/rockerBOO/awesome-neovim)
  
- 插件管理
  - 利用[vim-plug](plugin-manager.md#插件管理-vim-plug)管理插件
  - 利用[layz.nvim](plugin-manager.md#插件管理-lazynvim)管理插件
  - 利用[packer.nvim](plugin-manager.md#插件管理-packernvim)管理插件

- 模糊搜尋
  - [telescope.nvim](https://vimawesome.com/plugin/telescope-nvim-care-of-itself)
  - [fzf.vim](https://youtu.be/1f7l2-Fap2s?t=313)

- 狀態列
  - [lualine.nvim](https://github.com/nvim-lualine/lualine.nvim)
  - [其他選擇](https://github.com/rockerBOO/awesome-neovim#statusline)

- theme相關
  - [folke/tokyonight.nvim](https://github.com/folke/tokyonight.nvim)
  - 多種主題集合，[RRethy/nvim-base16](https://github.com/RRethy/nvim-base16)

- 文件內快速移動
  - [leap.nvim](https://github.com/ggandor/leap.nvim)
  - [hop.nvim](https://github.com/phaazon/hop.nvim)
  - [vim-easymotion](https://github.com/easymotion/vim-easymotion)
  - [neovim-sneak](https://www.bilibili.com/video/BV1mo4y1R7rx)

- 歡迎畫面
  - dashboard-nvim，歡迎畫面 + 歷史專案 + 歷史文件
  - alpha-nvim
  - vim-startify

- 與選取字串相關
  - [建立自己的 text-objects，vim-textobj-user](https://github.com/kana/vim-textobj-user)
  - [更細膩的 text object，targets.vim](https://github.com/wellle/targets.vim)
  - [與語法相關的 text-object，treesitter](https://github.com/nvim-treesitter/nvim-treesitter)
  - [介紹 text-object 相關插件](https://amikai.github.io/2020/09/22/vim-text-object/)

- mini.nvim 系列
  - mini.starter，顯示歡迎頁面，可顯示最近開啟的檔案，推薦
  - mini.comment，快速添加/移除註解，推薦
  - mini.completion，自動補全頁面中出現過的文字，推薦
  - mini.cursorword，自動高亮頁面中所有同樣的單字(根據滑鼠的位置)，推薦
  - mini.indentscope，顯示縮進線
  - mini.jump2d，兩個按鍵就可快速跳到可視區域的任何位置，推薦
  - mini.move         快速移動選取行的位置
  - mini.pairs        括號自動補全，推薦
  - mini.surround     為單字快速添加/移除括號

- Telescope 的插件
  - [nvim-telescope/telescope-symbols.nvim](https://github.com/nvim-telescope/telescope-symbols.nvim)，可利用 telescope 選擇並插入符號
  - [nvim-telescope/telescope-file-browser.nvim](https://github.com/nvim-telescope/telescope-file-browser.nvim)，可利用 telescope 作為檔案瀏覽器
  - [telescope-live-grep-args.nvim](https://github.com/nvim-telescope/telescope-live-grep-args.nvim)，live_grep 時可傳遞要檢視的路徑
  - [telescope-zoxide](https://github.com/jvgrootveld/telescope-zoxide)，快速切換目錄
  - [nvim-neoclip.lua](https://github.com/AckslD/nvim-neoclip.lua)，剪貼簿管理器
  - [octo.nvim](https://github.com/pwntester/octo.nvim)，執行git命令

- LSP 相關插件
  
  參考，[lsp的安裝與設置](plugin-lsp.md)

- git 相關插件
  - [gitsigns.nvim](https://github.com/lewis6991/gitsigns.nvim)，在側邊欄顯示 git 的狀態
  - vim-fugitive，提供 git 的操作
  - [vim-flow](https://github.com/rbong/vim-flog)，類似 vscode git graph 的插件，並提供 git 的操作 (by vim-fugitive)
  - gv.vim，類似 vscode git graph 的插件
  - git-blame-virt.nvim，顯示 git blame 訊息

- markdown
  - [glow.nvim](https://github.com/ellisonleao/glow.nvim)，neovim 內可直接顯示的 markdown-preview

- Terminal 相關
  - ToggleTerminal
  - FTerm.nvim，floating terminal
  - [vim-terminal](https://github.com/tc50cal/vim-terminal)，將terminal直接打開在分割視窗

- 其他推薦插件
  - nvim-lastplace，記住最後編輯位置
  - bufferline，將已經打開的 buffer，以 tab 的方式顯示
  - impatient.nvim，改善 neovim 啟動速度
  - neogen，注釋產生器
  - nvim-hlslens，提高搜尋體驗，增加搜尋數 + 簡化語法 + 可與 Multiple cursors 插件一起使用
  - vim-visual-multi，類似 vscode 的批量選取和修改
  - [which-key.nvim](#插件-which-keynvim-顯示快速鍵)，顯示快速鍵
  - vim-repeat，利用 `.` 進行重複命令
  - bullets.vim，自動產生條列符號
  - Navigator.nvim，透過命令控制 window 位置
  - auto-save.nvim，自動保存修改過的檔案
  - vim-silicon，將複製的代碼轉存為圖片

## [插件] which-key.nvim 顯示快速鍵

- 一般會透過`<leader>鍵`觸發 whichkey 的自定義選單，
  必須要在配置中設置`<leader>鍵`的觸發鍵，並且建立自定義選單的內容，觸發後才會跳出選單

- whichkey 會自動抓取透過 lua-api 全域宣告的快速鍵，
  也可以利用 whichkey 建立快速鍵，但只能透過`<leader>鍵`觸發

- 特定插件定義的快速鍵不一定會顯示在 which-key 的選單中，
  - 在特定插件中定義的快速鍵，只有在該插件被觸發且處於有效狀態下，該插件配置的快速鍵才會有效
  - 例如，nvim-cmp 的自動補全功能，只有跳出自動補全的選單時，nvim-cmp 定義的快速鍵只在自動補全的選單中有效，
    因此，nvim-cmp 定義的快速鍵不會出現在 whichkey 中
  - 若特定定義的快速鍵不顯示在 which-key 的選單中，可以手動添加標籤的方式添加到 whichkey 的選單中

- 在 whichkey 之後安裝的插件，若有設置快速鍵，也能自動被 whichkey 捕獲
  
- 以 `lazy.nvim` 安裝為例
  - step1，建立 `~/.config/nvim/lua/plugins/whichkey.lua`
    ```s
    # 參考，[which-key的配置](https://github.com/LazyVim/LazyVim/blob/1f7be0bbad3012046a53edb649b3cdc085e7ed54/lua/lazyvim/plugins/editor.lua)
    ```
  - `:Lazy` > `<shift> + <i>` 進行安裝

  - 使用，透過`空白鍵`觸發 whichkey

- 以 `packer` 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    
    packer.startup(function(use)
      ...

      -- 安裝 whichkey
      use {
        "folke/which-key.nvim",
        event = "BufWinEnter",
        config = function()
          vim.o.timeout = true
          vim.o.timeoutlen = 300
          require("plugin-config/which-key")
        end,
      }
      ...
    end)
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/whichkey.lua`
    ```lua
    -- wk 會自動抓取透過 lua-api 宣告的快速鍵

    local wk_status, wk = pcall(require, "which-key")
    if not wk_status then
      return
    end

    wk.setup {
      plugins = {
        marks = false,
        registers = false,
        spelling = { enabled = false, suggestions = 20 },

        -- 是否顯示neovim內鍵的快速鍵
        presets = {
          operators = false,
          motions = false,
          text_objects = false,
          windows = false,
          nav = false,
          z = false,
          g = false,
        },

      },
    }

    -- 建立自定義選單和快速鍵
    local mappings = {

      e = { ":NvimTreeToggle<CR>", "Open File Explorer" },    -- 打開檔案管理器
      s = { ":Dashboard<CR>", "Open Starter Page" },   -- 打開歡迎介面(打開最近開啟的檔案)
      p = { ":lua python_toggle()<CR>", "Open Python" },  -- 打開 python-terminal
      q = { ":bdelete<CR>", "Close Buffer and Tab" },     -- 關閉 buffer 的同時，也關閉對應的 tab

      L = {
        name = "lsp",
        n = { vim.lsp.buf.rename, "rename" },
        c = { vim.lsp.buf.code_action, "code action" },
        d = { vim.lsp.buf.definition, "definition" },
        i = { vim.lsp.buf.implementation, "implementation" },
        r = { require("telescope.builtin").lsp_references, "references" },
        k = { vim.lsp.buf.hover, "hover" },
      },

      S = {
        name = "session",
        w = { ":lua MiniSessions.write('mysession')<CR>", "MiniSessions.write()" },
        r = { ":lua MiniSessions.read('mysession')<CR>", "MiniSessions.read()" },
        d = { ":lua MiniSessions.delete('mysession')<CR>", "MiniSessions.delete()" },
      },

      T = {
        name = "telescope",
        f = { ":Telescope find_files<CR>", "find files", },
        b = { ":Telescope buffers<CR>", "find buffers"},
        s = { ":Telescope live_grep<CR>", "find file on any", },
        c = { ":Telescope grep_string<CR>", "find file on cursor", },
        h = { ":Telescope help_tags<CR>", "help tags"},
      },

    }

    local opts = {
      prefix = "<leader>",  -- 觸發自定義選單的按鍵
      nowait = true,
    }

    -- 註冊自義選單
    wk.register(mappings, opts)
    ```

- 檢查按鍵衝突，`:checkhealth which_key`

## [插件] comment.nvim 快速添加/移除註釋

- [官方網站](https://github.com/numToStr/Comment.nvim)

- 不使用插件添加多行註解的方式，參考，[lazyvim的常用快速鍵](#配置框架-lazyvim-簡化neovim的配置和使用)

- 以 lazy.nvim 安裝為例
  - step1，建立 `~/.config/nvim/lua/plugins/comment.lua`
    ```s
    return {
      'numToStr/Comment.nvim',

      config = function()
        require('Comment').setup()
      end,
    }
    ```
  
  - step2，`:Lazy` > `<shift> + <i>` 進行安裝

- 以 Packer 安裝為例

  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    use {
          'numToStr/Comment.nvim',
          config = 'require("plugin-config/comment")',
      }
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/comment.lua`
  ```lua
  local comment_status, comment = pcall(require, "Comment")
  if not comment_status then
    return
  end

  comment.setup {
    padding = false,
    toggler = {
    line = 'gcc',
    block = 'gc',
    },
    opleader = {},
    mappings = {
    basic = true,
    extra = false,
    }
  }
  ```

- 使用 comment.nvim
  - 對多行添加註釋/取消註解
    - `<shift> + <V>`，進入visual模式
    - 上下移動以選擇多行
    - `<g> + <c>`，添加註解，或取消註解

  - 對單行添加註釋/取消註解，`<g> + <c> + <c>`

## [插件] dashboard.nvim 歡迎畫面 + 歷史專案 + 歷史文件

- [官方網站](https://github.com/glepnir/dashboard-nvim)

- 以 lazy.nvim 安裝為例
  - step1，建立 `~/.config/nvim/lua/plugins/dashboard.lua`
    ```lua
    return {
      'glepnir/dashboard-nvim',
      event = 'VimEnter',
      config = function()
        require('dashboard').setup {
        
        }
      end,
      dependencies = { {'nvim-tree/nvim-web-devicons'}}
    }
    ```
  - step2，`:Lazy` > `<shift> + <i>` 進行安裝

- 以 Packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    use {
      'glepnir/dashboard-nvim',
      event = 'VimEnter',
      requires = {'nvim-tree/nvim-web-devicons'},
      config = 'require("plugin-config/dashboard")',
    }
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/dashboard.lua`
    ```lua
    local dashboard_status, dashboard = pcall(require, "dashboard")
    if not dashboard_status then
      return
    end

    dashboard.setup {
      theme = 'hyper',

      config = {
        week_header = {
        enable = true,   -- 顯示時間
        },

        -- header = {
        -- '',
        -- ' ███╗   ██╗ ███████╗ ██████╗  ██╗   ██╗ ██╗ ███╗   ███╗',
        -- ' ████╗  ██║ ██╔════╝██╔═══██╗ ██║   ██║ ██║ ████╗ ████║',
        -- ' ██╔██╗ ██║ █████╗  ██║   ██║ ██║   ██║ ██║ ██╔████╔██║',
        -- ' ██║╚██╗██║ ██╔══╝  ██║   ██║ ╚██╗ ██╔╝ ██║ ██║╚██╔╝██║',
        -- ' ██║ ╚████║ ███████╗╚██████╔╝  ╚████╔╝  ██║ ██║ ╚═╝ ██║',
        -- ' ╚═╝  ╚═══╝ ╚══════╝ ╚═════╝    ╚═══╝   ╚═╝ ╚═╝     ╚═╝',
        -- '' ,
        -- },
        
        Packages = { enable = false },    -- 顯示載入插件的數量
        
        shortcut = {    -- 顯示自定義快速鍵
          {
            desc = ' Explorer',
            group = 'Label',
            action = 'NvimTreeToggle',
            key = 'e'
          },

          {
            icon = ' ',
            icon_hl = '@variable',
            desc = 'Files',
            group = 'DiagnosticHint',
            action = 'Telescope find_files',
            key = 'f',
          },
          {
            icon = ' ',
            icon_hl = '@variable',
            desc = 'Help',
            group = 'DiagnosticHint',
            action = 'Telescope help_tags',
            key = 'h',
          },

          {
            desc = ' WhichKey',
            group = 'Number',
            action = 'e ~/.config/nvim/lua/plugin-config/which-key.lua',
            key = 'w',
          },

          {
            desc = ' Packages',
            group = 'Number',
            action = 'e ~/.config/nvim/lua/install-plugin.lua',
            key = 'i',
          },
        },
        
        -- 顯示最近開啟的檔案
        mru = {
          limit = 5,   -- 限制 most-recent-used 的數量
          lable = "Recent files",
        },
        
        -- 顯示頁尾訊息
        footer = {},

      },
    }
    ```

- 使用 dashboard
  - `:Dashboard`，開啟 dashboard

## [插件] lualine 狀態列

- 以 packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    packer = require("packer")

    packer.startup(function(use)
      ... 
      -- 安裝 lualine
      use { "nvim-lualine/lualine.nvim", config = 'require("plugin-config/lualine")' }
      ... 
    end)
    ```
  
  - 配置，`~/.config/nvim/lua/plugin-config/lualine.lua`
    ```lua
    require("lualine").setup {
      options = {
        theme = 'material',
        component_separators = { left = "", right = ""},
        section_separators = { left = "", right = ""}
      }
    }
    ```

## [插件] tokyonight 主題

- 以 packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    packer = require("packer")

    packer.startup(function(use)
      ... 
      -- 安裝 
      use { "folke/tokyonight.nvim", config = 'require("plugin-config/tokyonight")' }
      ... 
    end)
    ```
  
  - 配置，`~/.config/nvim/lua/plugin-config/tokyonight.lua`
    ```lua
    -- 啟動方法1，使用預設配置
    -- vim.cmd("colorscheme tokyonight-storm")

    -- 啟動方法2，發生錯誤時以通知取代拋出錯誤
    -- local ok, _ = pcall(vim.cmd, 'colorscheme tokyonight-storm')

    -- 啟動方法3，修改配置
    require("tokyonight").setup({
      style = "storm",
    })

    vim.cmd("colorscheme tokyonight-storm") -- 配置完成後需要手動啟動
    ```

## [插件] mini.nvim 提供多種插件功能

- 以 packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    packer.startup(function(use)
      ...

      -- 安裝 mini.nvim
      use { "echasnovski/mini.nvim", config = 'require("plugin-config/mini-nvim")' }

      -- 若要每個獨立安裝某個包
      -- use { "echasnovski/mini.starter" }

      ...
    end)
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/mini-nvim.lua`
    ```lua
    ------------------
    ---- mini.sessions
    ------------------

    local sessions_status, sessions = pcall(require, "mini.sessions")
    if not sessions_status then
      return
    end

    sessions.setup({
      -- 若啟動 nvim 時沒有指定檔案參數，就自動讀取最新的 session
      autoread = false,

      -- 是否在離開 nvim 前, 自動寫入當前的 session
      autowrite = false,

      -- mini.sessions執行操作後是否打印session路徑
      verbose = { read=true, write=true, delete=true },

    })

    ------------------
    ---- mini.cursorword
    ------------------

    local cursorword_status, cursorword = pcall(require, "mini.cursorword")
    if not cursorword_status then
      return
    end

    cursorword.setup()

    ------------------
    ---- mini.move
    ------------------

    local move_status, move = pcall(require, "mini.move")
    if not move_status then
      return
    end

    move.setup()

    ------------------
    ---- mini.trailspace
    ------------------

    local trailspace_status, trailspace = pcall(require, "mini.trailspace")
    if not trailspace_status then
    	return
    end
    
    trailspace.setup()
    
    vim.api.nvim_exec([[
      augroup mini.trim
        autocmd!
        autocmd BufWritePre * lua MiniTrailspace.trim()
      augroup END
    ]], false)  -- 寫入檔案前，自動清除行尾空白

    ------------------
    ---- mini.pairs
    ------------------

    local pair_status, pair = pcall(require, "mini.pairs")
    if not pair_status then
      return
    end

    pair.setup()

    ------------------
    ---- mini.jump2d
    -----------------

    local jump2d_status, jump2d = pcall(require, "mini.jump2d")
    if not jump2d_status then
      return
    end

    jump2d.setup({
      allowed_lines = {
        blank = false,
      },

      mappings = {
        start_jumping = "f",
      },
    })
    ```

- 使用
  - 除了以下幾個插件需要手動觸發，其他插件會立即生效，不需要額外觸發
  - mini.comment: `<V>` 選擇行，`<gcc>` 添加/移除註解
  - mini.move: `<V>` 選擇行，`<alt> + <hjkl>` 進行移動
  - mini.surround: 
    - `<saiw>` 為cursor所在的字添加括號
    - `<saiw>` 為cursor所在的字添加括號
    - `<sa> + <enter>` 為整行添加括號
  - mini.starter
    - 重新打開 starter，`:lua MiniStarter.open()`
  - mini.sessions
    - 建立session，`:lua MiniSessions.write('自定義session名')`
    - 刪除session，`:lua MiniSessions.delete('自定義session名')`

## [插件] neo-tree.nvim 檔案瀏覽器

- 以 lazy.nvim 安裝為例
  - 建立 `~/.config/nvim/lua/plugins/neotree.lua`
  - 參考[配置](https://github.com/LazyVim/LazyVim/blob/1f7be0bbad3012046a53edb649b3cdc085e7ed54/lua/lazyvim/plugins/editor.lua)
  - `:Lazy` > `<shift> + <i>` 進行安裝

- 以 packer 安裝為例，
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    -- 提供不同檔案類型的圖示
    use { "nvim-tree/nvim-web-devicons" }
    use { "nvim-tree/nvim-tree.lua", config = 'require("plugin-config/neo-tree.lua")' }
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/nvim-tree.lua`
    ```lua
    -- 關閉預設的文件瀏覽器，安裝第三方的文件瀏覽器時才需要
    vim.g.loaded_netrw = 1
    vim.g.loaded_netrwPlugin = 1

    -- 啟用真彩顏色
    vim.opt.termguicolors = true

    require("nvim-tree").setup()
    ```

- 使用
  - `:h NvimTree`，查詢使用方式
  - `:NvimTreeOpen`，打開nvim-tree
  - `:NvimTreeClose`，關閉nvim-tree
  - `:NvimTreeToggle`，打開和關閉都可以使用同一個指令
  - `:NvimTreeCollapse`，關閉已經打開的目錄 (摺疊所有打開的階層)
  - 在文件瀏覽視窗中
    - `a`，add，添加檔案
    - `r`，rename，修改檔名
    - `d`，delete，刪除檔案
    - `f`，filter，建立過濾器
  
## [插件] telescope 檔案模糊搜尋

- [官方網站](https://github.com/nvim-telescope/telescope.nvim)

- 優點，
  - 內建 pickers、sorters、previewers，不須額外安裝

- 依賴
  - 依賴插件
    - [nvim-lua/plenary.nvim](https://github.com/nvim-lua/plenary.nvim)
    - [nvim-telescope/telescope-fzf-native.nvim](https://github.com/nvim-telescope/telescope-fzf-native.nvim)
    - [nvim-treesitter/nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter)
  
  - 依賴外部應用
    - fzf
    - fd
    - ripgrep

- 以 lazy.nvim 安裝為例
  ```lua
  -- https://github.com/numToStr/dotfiles/blob/master/neovim/.config/nvim/lua/numToStr/plugins.lua
  use({
      {
          'nvim-telescope/telescope.nvim',
          event = 'CursorHold',
          config = function()
              require('numToStr.plugins.telescope')  -- 配置文件
          end,
      },
      {
          'nvim-telescope/telescope-fzf-native.nvim',
          after = 'telescope.nvim',
          run = 'make',
          config = function()
              require('telescope').load_extension('fzf')
          end,
      },
      {
          'nvim-telescope/telescope-symbols.nvim',
          after = 'telescope.nvim',
      },
  })
  ```

- 以 packer 安裝為例，
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    use {
      "nvim-telescope/telescope.nvim", config = 'require("plugin-config/telescope")' ,
      requires = {
        "nvim-lua/plenary.nvim",  -- 必要插件
        "nvim-treesitter/nvim-treesitter",  -- 推薦插件，可預覽選擇的檔案，並顯示語法高亮
        { "nvim-telescope/telescope-fzf-native.nvim", run = "make" }, -- 推薦插件，可以增強搜尋速度
      },
    }
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/telescope.lua`
    ```lua
    telescope = require("telescope")
    actions = require("telescope.actions")

    telescope.setup({
      defaults = {
        mappings = {
          i = {
            -- move to pre result
            ["<C-k>"] = actions.move_selection_previous,
            -- move to next result
            ["<C-j>"] = actions.move_selection_next,
            --send selected to quickfixlist
            ["<C-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
          },
        },
      },
    })

    telescope.load_extension("fzf")
    ```

  - 配置按鍵，`~/.config/nvim/lua/plugins/whichkey.lua`
    ```lua
    ... 內容省略 ...

    local mappings = {
      ... 內容省略 ...
      t = {
        name = "telescope",
        f = { ":Telescope find_files<CR>", "find files", },  
        b = { ":Telescope buffers<CR>", "find buffers"},
        s = { ":Telescope live_grep<CR>", "find file on any", },
        c = { ":Telescope grep_string<CR>", "find file on cursor", },
        h = { ":Telescope help_tags<CR>", "help tags"},
      },
    }

    local opts = {
      prefix = "<leader>",  -- 觸發自定義選單的按鍵
      nowait = true,
    }

    -- 註冊自義選單
    wk.register(mappings, opts)
    ```

- 使用
  - 透過 `:checkhealth telescope` 檢查依賴
  - `<leader>tf`，模糊搜尋當前目錄下的任何檔案
  - `<leader>tb`，模糊搜尋neovim已開啟的buffer 檔案
  - `<leader>ts`，根據字串，模糊搜尋當前目錄下，包含該字串的任何檔案
  - `<leader>tc`，根據當前滑鼠所在的字串，模糊搜尋當前目錄下(目前專案中)，包含該字串的任何檔案
  - `<leader>th`，搜尋 help tag，相當於 `:h <lua-api>`

## [插件] telescope-file-browser 檔案瀏覽器

- [官方網站](https://github.com/nvim-telescope/telescope-file-browser.nvim)

- 優缺點
  - 作為 telescope 的插件，啟動速度快，ESC或滑鼠左鍵快速關閉
  - 可瀏覽當前目錄以外的路徑
  - 使用 insert/normal mode，支援的功能更多
  - 優秀的 preview 功能，支援語法高亮，以 `ls -la` 的形式顯示檔案資訊

- 以 packer 安裝為例，
  - 注意，需要安裝 telescope 插件，參考，[telescope安裝](#插件-telescope-模糊搜尋)

  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    -- 安裝 telescope
    use {
      "nvim-telescope/telescope.nvim", config = 'require("plugin-config/telescope")' ,
      requires = {
        "nvim-lua/plenary.nvim",
        "nvim-treesitter/nvim-treesitter",
        { "nvim-telescope/telescope-fzf-native.nvim", run = "make" },
      },
    }

    -- 安裝 telescope-file-browser
    use {
      "nvim-telescope/telescope-file-browser.nvim",
      requires = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" }
    }
    ```

  - 配置+啟用，`~/.config/nvim/lua/plugin-config/telescope.lua`
    
    注意，telescope-file-browser 作為 telescope 的插件，會隨著 telescope 一起啟用，配置檔也使用同一個
    ```lua
    telescope = require("telescope")
    actions = require("telescope.actions")

    telescope.setup({
      defaults = {
        mappings = {
          i = {
            -- move to pre result
            ["<C-k>"] = actions.move_selection_previous,
            -- move to next result
            ["<C-j>"] = actions.move_selection_next,
            --send selected to quickfixlist
            ["<C-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
          },
        },
      },

      -- 設置 telescope-file-browser
      extensions = {
        file_browser = {
          theme = "ivy",
          hijack_netrw = true,
        }
      },

    })

    telescope.load_extension("fzf")
    telescope.load_extension("file_browser")  -- 啟動 telescope-file-browser
    ```
  
  - 配置按鍵，`~/.config/nvim/lua/plugins/whichkey.lua`
    ```lua
    ... 內容省略 ...

    local mappings = {
      ... 內容省略 ...
      b = { ":Telescope file_browser<CR>", "Open File Explorer" },
      
      -- 或瀏覽指定路徑
      -- b = { ":Telescope file_browser path=%:p:h select_buffer=true<CR>", "Open Specific folder" },
    }

    local opts = {
      prefix = "<leader>",  -- 觸發自定義選單的按鍵
      nowait = true,
    }

    -- 註冊自義選單
    wk.register(mappings, opts)
    ```

- 使用
  - `<leader>b`，開啟檔案瀏覽器
  - 在檔案瀏覽器中
    - 需要在 normal-mode 按下以下按鍵
    - `<h>`，顯示隱藏的檔案或目錄
    - `<c>`，建立檔案或建立目錄
    - `<d>`，刪除檔案或建立目錄
    - `<?>`，查詢可用按鍵
    
## [插件] lsp (language-server-protocol) 相關

參考，[lsp插件的配置](plugin-lsp.md)

## [插件] nvim-cmp 自動補全

- 相關插件
  - nvim-cmp，自動補全的主要插件
  - cmp-buffer，nvim-cmp的輔助插件，提供當前檔案字串的自動補全
  - cmp-path，nvim-cmp的輔助插件，提供路徑的自動補全
  - cmp-cmdline，nvim-cmp的輔助插件，提供command-mode (:或/) 的自動補全

- 以 packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    ... 內容省略 ...
    -- autocompletion
    use { "hrsh7th/cmp-buffer" }
    use { "hrsh7th/cmp-path" }
    use { "hrsh7th/cmp-cmdline" }
    use { "hrsh7th/nvim-cmp" }
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/nvim-cmp.lua`
    ```lua
    local cmp = require("cmp")

    -- (選用) 設置 insert-mode 下可用的自動補全
    --    menu: 使用menu來顯示可能的補全
    --    menuone: 即使只有一個匹配，也使用menu
    --    noselect: 用戶選擇前不插入匹配的文本
    --vim.opt.completeopt = "menu,menuone,noselect"

    cmp.setup({
      -- 設置自動補全的來源
      sources = cmp.config.sources({
        { name = "buffer" },
        { name = "path" },
      }),

      -- 設置快速鍵
      mapping = cmp.mapping.preset.insert({
        ["<C-k>"] = cmp.mapping.select_prev_item(), -- 在自動補全的選單中移動
        ["<C-j>"] = cmp.mapping.select_next_item(), -- 在自動補全的選單中移動 
        ["<C-b>"] = cmp.mapping.scroll_docs(-4),    -- 在自動補全的選單中捲動，需要有捲動條才有效
        ["<C-f>"] = cmp.mapping.scroll_docs(4),     -- 在自動補全的選單中捲動，需要有捲動條才有效
        ["<C-Space>"] = cmp.mapping.complete(),     -- show completion suggestions
        ["<C-e>"] = cmp.mapping.abort(),            -- close completion window
        ["<CR>"] = cmp.mapping.confirm({ select = false }),
      }),
    })

    -- command-mode(/) 下的自動補全
    cmp.setup.cmdline('/', {
      mapping = cmp.mapping.preset.cmdline(),
      sources = {
        { name = 'buffer' }
      }
    })

    -- command-mode(:) 下的自動補全
    cmp.setup.cmdline(':', {
      mapping = cmp.mapping.preset.cmdline(),
      sources = cmp.config.sources({
        { name = 'path' }
      }, {
        {
          name = 'cmdline',
          option = {
            ignore_cmds = { 'Man', '!' }
          }
        }
      })
    })
    ```

- 使用
  - 使用1，在檔案內輸入同樣的字串，會自動彈出高亮的自動補全選單
  - 使用2，在 "" 之間輸入路徑，例如，"~/"，會自動彈出有效路徑
  - 使用3，在搜尋模式下(/)，輸入字串會顯示自動跳出選單並高亮
  - 使用4，在搜尋模式下(:)，輸入字串會顯示自動跳出選單並高亮
  - 使用5，在跳出自動補全的選單後，透過配置的快速鍵移動獲取捲動

## [插件] nvim-lastplace 記住最後編輯位置(不會記住最後打開文件)

- 以 packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    use({
      "ethanholz/nvim-lastplace",
      config = 'require("plugin-config/lastplace")',
    })
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/lastplace.lua`
    ```lua
    local lastplace_status, lastplace = pcall(require, "nvim-lastplace")
    if not lastplace_status then
      return
    end

    lastplace.setup()
    ```

## [插件] bufferline.nvim，將已經打開的 buffer，以 tab 的方式顯示

- 以 Packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    use {
      'akinsho/bufferline.nvim',
      requires = 'nvim-tree/nvim-web-devicons',
      config = 'require("plugin-config/bufferline")',
    }
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/bufferline.lua`
    ```lua
    local bufferline_status, bufferline = pcall(require, "bufferline")
    if not bufferline_status then
      return
    end

    bufferline.setup({
      options = {
        -- 是否顯示diagnostic狀態，可用值: fase | "nvim_lsp" | "coc"
        diagnostics = "nvim_lsp",

        -- tab 之間的分隔器樣式
        separator_style = "thin",

        -- 是否顯示檔案類型圖示
        show_buffer_icons = false,

        -- 只有一個檔案時, 是否顯示 bufferline
        always_show_bufferline = false,

        -- 啟用nvimtree時，預留nvimtree的顯示空間
        -- 避免 tab 從左上角開始顯示
        offsets = {
          {
            filetype = "NvimTree",
            text = "",
          },
        },
      },
    })
    ```
    
  - 設置按鍵，`~/.config/nvim/lua/keybindings.lua`
    ```lua
    -- 使用 shift+h 和 shift 切換 buffer-tab
    map("n", "<S-Left>", ":BufferLineCyclePrev<CR>")
    map("n", "<S-Right>", ":BufferLineCycleNext<CR>")
    ```

## [插件] treesitter 語法高亮

- 優缺點
  - 提供更友善的語法高亮
  - 提供縮進線、增量選擇、代碼摺疊等進階功能
  - 配置簡單

- 以 Packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    use {
      "nvim-treesitter/nvim-treesitter",
      config = 'require("plugin-config/treesitter")',
    }
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/treesitter.lua`
    ```lua
    local treesitter_status, treesitter = pcall(require, "nvim-treesitter.configs")
    if not treesitter_status then
      return
    end

    treesitter.setup({
      -- 指定要使用語法高亮的語言
      ensure_installed = {},
      -- 是否自動安裝 ensure_installed 列表中的 language-parser
      sync_install = true,
      -- 是否自動安裝缺失的 language-parser
      auto_install = true,

      -- 語法高亮模塊
      hightlight = {
        enable = true,
        additional_vim_regex_highlighting = false,
      },

      -- 增量選擇模塊
      incremental_selection = {
        enable = false,
        keymaps = {
          init_selection = "<CR>",
          node_incremental = "<CR>",
          node_decremental = "<BS>",
          scope_incremental = "<TAB>",
        },
      },

      -- 縮進格式化模塊
      indent = {
        enable = true,
      },
    })
    ```

- 使用
  - `:TSInstall`，安裝指定的 language-paser
  - `:TSInstallInfo`，檢查 language-paser 的安裝情形
  
## [插件] indent-blankline.nvim 顯示縮進

- 以 Packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    use { 
      "lukas-reineke/indent-blankline.nvim",
      config = 'require("plugin-config/indent")',
    }
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/indent.lua`

    以高亮顯示當前行的縮進線為例
    ```lua
    local indent_status, indent = pcall(require, "indent_blankline")

    if not indent_status then
      return
    end

    vim.opt.termguicolors = true
    vim.opt.list = true
    vim.opt.listchars:append("space:⋅")   -- 變更空白的顯示樣式
    --vim.opt.listchars:append("eol:↴")   -- 變更行尾(EndOfLine)的顯示樣式

    indent.setup({

      -- 是否使用 treesitter 確定縮進線的位置
      use_treesitter = true,

      -- 是否固定顯示第一行的縮進線
      show_first_indent_level = true,

      -- 空白行是否顯示縮進線
      show_trailing_blankline_indent = false,

      -- 突出顯示游標所屬的縮進 (游標所在行的縮進線進行高亮)
      show_current_context = true,
      
      -- 將當前游標行加上底線
      show_current_context_start = true,

      -- 禁用 indent 的類型, 減少資源的浪費
      buftype_exclude = { "terminal", "nofile" },
      bufname_exclude = { "README.md" },
      filetype_exclude = {
        "alpha",
        "log",
        "gitcommit",
        "dapui_scopes",
        "dapui_stacks",
        "dapui_watches",
        "dapui_breakpoints",
        "dapui_hover",
        "LuaTree",
        "vimwiki",
        "markdown",
        "vista",
        "NvimTree",
        "git",
        "TelescopePrompt",
        "undotree",
        "help",
        "startify",
        "packer",
        "neogitstatus",
        "NvimTree",
        "Outline",
        "Trouble",
        "lspinfo",
        "", -- for all buffers without a file type
      },
    })
    ```

- 範例，以色塊的方式取代縮進線
  ```lua
  -- 設置區塊顏色(注意，是guibg，不是guifg)
  vim.cmd [[highlight IndentBlanklineIndent1 guibg=#40433d gui=nocombine]]
  vim.cmd [[highlight IndentBlanklineIndent2 guibg=#384341 gui=nocombine]]
  vim.cmd [[highlight IndentBlanklineIndent3 guibg=#3e3a4b gui=nocombine]]
  vim.cmd [[highlight IndentBlanklineIndent4 guibg=#3b4852 gui=nocombine]]

  require("indent_blankline").setup({
    char = "",    -- 關閉顯示縮進線的字符, 改顯示區塊
    char_highlight_list = {
      "IndentBlanklineIndent1",
      "IndentBlanklineIndent2",
      "IndentBlanklineIndent3",
      "IndentBlanklineIndent4",
    },

    space_char_highlight_list = {
      "IndentBlanklineIndent1",
      "IndentBlanklineIndent2",
      "IndentBlanklineIndent3",
      "IndentBlanklineIndent4",
    },
  })
  ```

## [插件] nvim-colorizer.lua 顯示色碼的顏色

- 以 Packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    use {
      "norcalli/nvim-colorizer.lua",
      config = 'require("plugin-config/colorizer")',
    }
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/colorizer.lua`
    ```lua
    local color_status, color = pcall(require, "colorizer")
    if not color_status then
      return
    end

    color.setup()
    ```

- 使用，透過 `:ColorizerToggle` 開關顏色的顯示

## [插件] fidget.nvim 顯示 lsp 操作的進度

- 以 Packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    use {
      "j-hui/fidget.nvim",
      config = 'require("plugin-config/fidget")',
    }
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/fidget.lua`
    ```lua
    local fidget_status, fidget = pcall(require, "fidget")
    if not fidget_status then
      return
    end

    fidget.setup()
    ```

- 使用，透過 `:LspRestart` 進行測試，會在當前視窗的右下角顯示進度

## [插件] toggleterm.nvim 顯示終端

- 以 Packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    use {
      "akinsho/toggleterm.nvim",
      config = 'require("plugin-config/toggleterm")',
    }
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/toggleterm.lua`
    ```lua
    local toggleterm_status, toggleterm = pcall(require, "toggleterm")
    if not toggleterm_status then
      return
    end

    toggleterm.setup({
      -- 啟動後直接進入 insert-mode
      start_in_insert = true,
      -- 以 float 的方式顯示 terminal
      direction = 'float',
      -- terminal 退出後直接關閉視窗
      close_on_exit = true,
    })

    -------------
    -- 打開terminal，並直接執行特定命令
    -------------
    local Terminal  = require('toggleterm.terminal').Terminal

    local pyterm = Terminal:new({
        cmd = 'python',
        direction = 'horizontal'
    })

    -- 透過 :lua python_toggle() 執行
    function python_toggle()
      pyterm:toggle()
    end
    ```
  
  - 設置按鍵，
    - `~/.config/nvim/lua/keybinding.lua`
      ```lua
      map("n", "<C-t>", ":ToggleTerm<CR>")
      ```

    - 透過 which_key 直接打開終端的 python，`~/.config/nvim/lua/plugin-config/which-key.lua`
      ```lua
      p = {
        name = "show python in terminal",
        y = { ":lua python_toggle()<CR>", "open python" }
      }
      ```

## [插件] Flow + vim-fugitive Git相關操作

- 以 Packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    use({
      "rbong/vim-flog",
      requires = "tpope/vim-fugitive",
      config = 'require("plugin-config/flog")'
    })
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/flog.lua`
    ```lua
    local flog_status, flog = pcall(require, "flog")
    if not flog_status then
      return
    end

    flog.setup { }
    ```
  
- 使用
  - `:Git`，等效於 git status，用來查看狀態
    - `<->鍵`，將 unstage/untrack 的檔案，變成 staged/tracked
    - `<+>鍵`，展開檔案內容，可用來檢視修改的差異
    - `<shift>+v`，選中多個檔案，可對多個檔案進行相同的操作
  
  - 檢視差異(支援同步滾動檢視)
    - `:Gdiffsplit`，等同於 Ghdiffsplit
    - `:Gvdiffsplit`，垂直比對
    - `:Ghdiffsplit`，水平比對
    - 注意，需要在開啟檔案的狀態下，輸入上述的命令
  
  - Gwrite/:Gread 可以用於 commit 前的暫存和還原
    - `:Gwrite` 等效於 `git add <currentfilename>`，
      
      用於將當前檔案內容暫時寫入 staged buffer (工作區緩衝，尚未commit)，並放在 staged 區

    - `:Gread` 等效於 `git reset <currentfilename>`

      用於從staged buffer還原先前寫入的檔案，與 :Gwrite 一起使用

  - 查看紀錄，`:Git log`

  - 顯示刪除的內容，`:lua package.loaded.gitsigns.toggle_deleted()`
  
  - 提交 commit
    - 流程，`:Git commit 提交 commit` > `編輯 commit message` > `:wq` > 成功後，提交紀錄會放置於 unpushed
    - AMEND，`:Git commit --amend`

  - 利用 Telescope 檢視 git 訊息
    - `:Telescopy git_files`，對所有檔案進行模糊搜尋
    - `:Telescopy git_stash`，對所有staged檔案進行模糊搜尋"
    - `:Telescopy git_status`，對所有staus進行模糊搜尋
    - `:Telescopy git_commits`，對所有commit進行模糊搜尋
    - `:Telescopy git_branches`

## [插件] nvim-hlslens + vim-visual-multi 強化search + 多游標修改

- 以 Packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    use {
      "kevinhwang91/nvim-hlslens",
      requires = "mg979/vim-visual-multi",
      config = 'require("plugin-config/hlslens_multi")',
    }
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/hlslens_multi.lua`
    ```lua
    ---------------
    -- 配置 hlslens
    ---------------

    local hlslens_status, hlslens = pcall(require, "hlslens")
    if not hlslens_status then
      return
    end

    local kopts = {noremap = true, silent = true}

    vim.api.nvim_set_keymap('n', 'n', [[<Cmd>execute('normal! ' . v:count1 . 'n')<CR><Cmd>lua require('hlslens').start()<CR>]], kopts)

    vim.api.nvim_set_keymap('n', 'N', [[<Cmd>execute('normal! ' . v:count1 . 'N')<CR><Cmd>lua require('hlslens').start()<CR>]], kopts)

    vim.api.nvim_set_keymap('n', '*', [[*<Cmd>lua require('hlslens').start()<CR>]], kopts)
    vim.api.nvim_set_keymap('n', '#', [[#<Cmd/lua require('hlslens').start()<CR>]], kopts)
    vim.api.nvim_set_keymap('n', 'g*', [[g*<Cmd>lua require('hlslens').start()<CR>]], kopts)
    vim.api.nvim_set_keymap('n', 'g#', [[g#<Cmd>lua require('hlslens').start()<CR>]], kopts)
    vim.api.nvim_set_keymap('n', '<Leader>l', '<Cmd>noh<CR>', kopts)

    hlslens.setup({
      calm_down = false,   -- 當游標離開關鍵字的範圍時，自動關閉高亮
    })

    ---------------
    -- 配置 vim-visual-multi
    ---------------

    local multi_status, multi = pcall(require, "multi")
    if not multi_status then
      return
    end

    -- 建立自動命令，
    --    當 visual_multi_start 事件觸發時，執行 lua require('vmlens').start()
    --    當 visual_multi_exit 事件觸發時，執行 lua require('vmlens').exit()

    vim.cmd([[
        aug VMlens
            au!
            au User visual_multi_start lua require('vmlens').start()
            au User visual_multi_exit lua require('vmlens').exit()
        aug END
    ]])

    multi.setup {}
    ```

- 使用
  - 進行搜尋時，在選中行的後方會出現[跳轉鍵 index]，例如，[2n 3]，
    代表當前行是第3個選中行，並且可以透過輸入2n，跳轉到指定位置

  - 兩種方式進行多游標選取
    - 方法1，`<ctrl>+n` 依序選取，每次選取`相同一個字串`
      - 按 q 可掠過當前的單字
      - 注意，`<ctrl>+n`用於選取整個單字
  
    - 方法2，`<ctrl>+上下鍵` 依序選取，每次可選取`不同的字符`
      - 若要略過某一個單字，不要按住 ctrl，只透過上下鍵移動
      - 注意，`<ctrl>+上下鍵`每次只選取整一個字符

    - 被 multi 插件選取的 word 會高亮顯示，
      - 在 normal-mode 下，按下1次 ESC 取消 multi 的選擇模式
      - 在 insert-mode 下，按下2次 ESC 取消 multi 的選擇模式
  
  - 範例，手動選擇，但每次只能選取同一字串
    - 連續按下`<ctrl>+n`，進入 multi 的選取模式
    - 按 q 取消當前選擇的單字 
    - 進入 insert-mode > 批量修改 > 兩次 ESC 回到 normal-mode

  - 範例，手動選擇，可以選取不同字符
    - 連續按下`<ctrl>+上下鍵`，進入 multi 的選取模式
    - 不按ctrl，以避免選擇當前的單字 
    - 進入 insert-mode > 批量修改 > 兩次 ESC 回到 normal-mode

  - 範例，移動建立的光標
    - 透過`<ctrl>+上下鍵`建立光標
    - 方法1，透過 `hjkl` 移動光標
    - 方法2，透過 `f<字符>` 將光標移動到指定字符的位置

  - 範例，批量選擇
    - 按下`<ctrl>+上下鍵`或`<ctrl>+n`，進入 multi 選取模式
    - 輸入 `m5j` 往下選擇5行內所有出現的位置

  - 範例，段落內選擇
    - 按下`<ctrl>+上下鍵`或`<ctrl>+n`，進入 multi 選取模式
    - 輸入 `mip` (mark in paragraph)，選中當前段落內，所有出現的位置

  - 範例，行內選擇
    - 按下`<ctrl>+上下鍵`或`<ctrl>+n`，進入 multi 選取模式
    - 輸入 `mas` (mark all in sentence)，選中當前行中，所有出現的位置

## [插件] nvim-ufo 代碼摺疊

- 以 Packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    use {
      'kevinhwang91/nvim-ufo',
      requires = 'kevinhwang91/promise-async',
      config = 'require("plugin-config/ufo")',
      --run = ":TSUpdate",
    }
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/ufo.lua`
    ```lua
    -----------------
    -- 導入庫
    -----------------
    local ufo_status, ufo = pcall(require, "ufo")
    if not ufo_status then
      return
    end

    -----------------
    -- 全局配置
    -----------------

    -- 配置摺疊相關屬性
    vim.o.foldenable = true   -- 啟用摺疊功能
    vim.o.foldcolumn = '1'    -- 在側邊顯示摺疊欄，不顯示仍然可以折疊，但無法手動展開
    vim.o.foldlevel = 99
    vim.o.foldlevelstart = 99
    vim.o.fillchars = [[eob: ,fold: ,foldopen:,foldsep: ,foldclose:]]

    -- 配置按鍵
    vim.keymap.set('n', 'zR', require('ufo').openAllFolds)
    vim.keymap.set('n', 'zM', require('ufo').closeAllFolds)

    -----------------
    -- 配置ufo
    -----------------

    local api = vim.api

    local my_fold_virt_text_handler = function(virtText, lnum, endLnum, width, truncate)
      local newVirtText = {}
      local suffix = ('  %d '):format(endLnum - lnum)
      local sufWidth = vim.fn.strdisplaywidth(suffix)
      local targetWidth = width - sufWidth
      local curWidth = 0

      for _, chunk in ipairs(virtText) do
        local chunkText = chunk[1]
        local chunkWidth = vim.fn.strdisplaywidth(chunkText)

        if targetWidth > curWidth + chunkWidth then
            table.insert(newVirtText, chunk)
        else
            chunkText = truncate(chunkText, targetWidth - curWidth)
            local hlGroup = chunk[2]
            table.insert(newVirtText, {chunkText, hlGroup})
            chunkWidth = vim.fn.strdisplaywidth(chunkText)

            -- str width returned from truncate() may less than 2nd argument, need padding
            if curWidth + chunkWidth < targetWidth then
                suffix = suffix .. (' '):rep(targetWidth - curWidth - chunkWidth)
            end

            break
        end

        curWidth = curWidth + chunkWidth
      end

      table.insert(newVirtText, {suffix, 'MoreMsg'})

      return newVirtText
    end

    ufo.setup {
      provider_selector = function(bufnr, filetype, buftype)
        return {'treesitter', 'indent'}
      end,

      close_fold_kinds = {
        'imports', "comment"
      },

      enable_get_fold_virt_text= { default = true },

      fold_virt_text_handler = my_fold_virt_text_handler,
    }
    ```

## [插件] nvim-surround 添加括號

- 以 Packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
    use {
      'kylechui/nvim-surround',
      config = 'require("plugin-config/surround")',
    }
    ```

  - 配置，`~/.config/nvim/lua/plugin-config/surround.lua`
    ```lua
    local surround_status, surround = pcall(require, "nvim-surround")
    if not surround_status then
      return
    end

    surround.setup {}
    ```

- 使用
  - 支援 normal-mode / insert-mode / visual-mode

  - 在 normal-mode 下
    - `ysw + <要添加的surround符號>`， yank(add) surround for word，從當前位置到字尾添加指定括號
    - `ysiw + <要添加的surround符號>`   yank(add) surround for inner-word，為當前word添加指定括號
    - `ysiw + t + <HTML Tag>`，為當前word添加添加html-tag

    - `cs + <要更換的surround符號> + <新的surround符號>`，替換surround
    - `cs + <要更換的surround符號> + t + <HTML Tag>`  將surround替換為html-tag

    - `ds + <要刪除的surround符號>`，刪除指定的 surround
    - `ds + t  刪除 html-tag`，刪除html-tag

## [插件] undotree 回朔樹

- 以 Packer 安裝為例
  - 安裝，`~/.config/nvim/lua/install-plugin.lua`
    ```lua
     use { 'mbbill/undotree', }
    ```

  - 設置按鍵，，`~/.config/nvim/lua/plugin-config/which-key.lua`
    ```lua
    u = { ":UndotreeToggle<>", "Open Undotree" }, -- 打開回朔樹
    ```

## ref

- 配置範例
  - [各種配置範例總匯](https://github.com/topics/neovim-dotfiles)
  - [dotfile @ numToStr](https://github.com/numToStr/dotfiles/tree/master/neovim/.config/nvim/lua/numToStr/plugins)
  - [dotfile @ dwt1](https://gitlab.com/dwt1/dotfiles/-/tree/master/.config/nvim)
  - [dotfile @ hackorum](https://github.com/hackorum/nfs/blob/master/lua/plugins/init.lua)
  - [dotfile @ josean-dev](https://github.com/josean-dev/dev-environment-files/tree/main/.config/nvim/lua/josean/plugins)
  - [dotfile @ whatsthatsmell](https://github.com/whatsthatsmell/dots/tree/master/public%20dots/vim-nvim)
  - [How I Setup Neovim On My Mac To Make It Amazing - Complete Guide](https://www.youtube.com/watch?v=vdn_pKJUda8)
  - [配置範例 @ ichunyu](https://ichunyu.github.io/neovim/#%E5%9F%BA%E6%9C%AC%E9%85%8D%E7%BD%AE)
  - [配置範例 @ quintin-lee](https://github.com/quintin-lee/NVCode)
  - [配置範例 @ neovim-from-scratch](https://github.com/LunarVim/Neovim-from-scratch/tree/master/lua/user)

- cmp
  - [Neovim內置LSP配置(二)：自動代碼補全](https://zhuanlan.zhihu.com/p/445331508)
  - [官方文檔對completeopt的說明](https://yianwillis.github.io/vimcdoc/doc/options.html#'completeopt')
  - [nvim-cmp提供補全來源的相關插件](https://github.com/hrsh7th/nvim-cmp/wiki/List-of-sources)

- bufferline
  - [bufferline官網](https://github.com/akinsho/bufferline.nvim#configuration)
  - [bufferline配置範例 @ Nshen](https://zhuanlan.zhihu.com/p/440349051)
  - [bufferline配置範例 @ bigshans](https://bigshans.github.io/post/config-bufline-nvim/)
  - [bufferline配置範例 @ liranuxx](https://github.com/liranuxx/nvea/blob/master/lua/plugins/tools/init-bufferline.lua)
  - [bufferline配置範例 @ SmithJson](https://github.com/SmithJson/nvim/blob/master/lua/modules/ui/config.lua)

- treesitter
  - [treesitter配置範例 @ Nshen](https://zhuanlan.zhihu.com/p/441818052)
  - [treesitter配置範例 @ liranuxx](https://github.com/liranuxx/nvea/blob/master/lua/plugins/ui/treesitter.lua)
  - [treesitter配置範例 @ quintin-lee](https://github.com/quintin-lee/NVCode/blob/master/lua/configs/nvim-treesitter.lua)
  
- indent_blankline
  - [indent_blankline配置範例 @ SmithJson](https://github.com/SmithJson/nvim/blob/master/lua/modules/ui/config.lua)

- vim-fugitive
  - [fugitive的配置 @ omerxx](https://github.com/omerxx/dotfiles/blob/master/nvim/init.lua)

- nvim-ufo
  - [nvim-ufo配置範例 @ kevinhwang91](https://github.com/kevinhwang91/nvim-ufo/blob/main/doc/example.lua)
  - [nvim-ufo配置範例 @ yuki-yano](https://github.com/yuki-yano/dotfiles/blob/d6ce19522ed3308d2671f39e7c205a9ea54aca21/.vim/lua/plugins/ui.lua#L746)
