## vim/neovim 的使用

- 幾種模式
  - command模式(e-mode)，`<:>`，用於執行命令
  - common模式(c-mode)，`<Esc>`，除了編輯之外的各種操作
  - insert模式(i-mode)，`<i>`，編寫文件用
  - visual模式(v-mode)，
    - `<v>`，visual-character模式，用於選取字元
    - `<V>`，visual-line模式，用於選取整行
    - `<ctrl> + <v>` 或 `<ctrl> + <q>`，visual-block模式，用於選取多行，並進行批次處理

  - replace模式(r-mode)，
    - `<r>`，單次替換，
    - `<R>`，連續替換

- 同樣的按鍵，`在大寫下有反向`的意思，例如，
  `<w>` 跳前到下一個 word，`<W>` 跳前到上一個 word

- `刪除鍵`也可以做為`剪下鍵`使用

- 若要處理多次，例如多行或多個字元，`先輸入次數，再輸入操作`

- 組合鍵有時效限制，預設為 1秒

- 對於複雜的組合鍵，可以使用 `<.>` 簡化使用

- 編輯頁面、window、tab 的差別
  - 每個開啟的檔案只有一個獨立的編輯頁面，每個編輯頁面有專屬對應的檔案
  - 編輯頁面是`以檔名顯示`的小標籤，帶`有關閉按鈕`的頁面，，並顯示在neovim的`左上角`

  - tab 是`以數字標記`的小標籤，`沒有關閉按鈕`，並顯示於 neovim 的`右上角`
  - tab 是 window 的集合，每個 tab 都有自己的 window layout
  - tab 沒有固定所屬的編輯頁面，可以顯示任何編輯頁面的內容

  - window 是 tab 內不同的檢視區域
  - window 有固定所屬的tab
  - window 沒有固定所屬的編輯頁面，可以顯示任何編輯頁面的內容，也可以顯示同一份編輯頁面的不同區域

- 需要 neovim 啟動後立刻執行的功能，直接寫在 `~/.config/nvim/init.lua` 會啟動的更快，
  此類不需要透過插件管理器啟動

- 安裝neovim，
  - archlinux，`$ sudo pacman -S neovim`
  - windows，`$ choco install neovim`

- 其他主題
  - [配置文件的使用](config.md)
  - [配置框架插件](plugin-framework.md)
  - [插件管理器](plugin-manager.md)
  - [推薦插件 + 插件的安裝與配置](plugin.md)

## 重新安裝 neovim 

- 完整移除
  - 保存配置，`$ cp -r ~/.config/nvim ~`
  - 刪除自定義配置，`$ rm -rf ~/.config/nvim`
  - 刪除已安裝的插件，`$ rm -rf ~/.local/share/nvim`
  - 刪除快取，`$ rm -rf ~/.cache/nvim`
  - 移除 neovim，`$ sudo pacman -Rsn neovim --noconfirm`

- 重新安裝
  - 安裝 neovim，`$ sudo pacman -S neovim --noconfirm`
  - 還原配置，`$ mv ~/nvim ~/.config/`
  - 啟動 neovim，`$ nvim`
    > 注意，重新安裝後的第一次啟動會報錯屬於正常現象，因為插件尚未安裝
  - 重啟 neovim，`:q`
  
## 常用命令

- 列出可用命令，
  - 查詢所有可用命令，`:` + `<ctrl> + d`
  - 查詢該字母可用的命令，`:字母` + `<ctrl> + d`

- 顯示快速鍵，
  - 注意，若使用 whichkey，可透過 `:checkhealth which_key` 檢查衝突的按鍵
  - 顯示所有快速鍵，`:map`
  - 顯示 normal-mode下的快速鍵，`:nmap`

- 查詢命令如何使用，
  - `:help 命令`
  - `:h 命令`
  - [線上查詢](https://neovim.io/doc/user/vimindex.html#ex-cmd-index)

- 手動更換主題，`:colorscheme <tab>`
  
- 顯示配置檔的路徑，`:echo stdpath('config')`
  
- 檔案
  - 在下方水平視窗開啟新檔案，`:new`
  - 在垂直視窗開啟新檔案，`:vnew`，或 `:vs`
  - 開啟檔案，`:edit 檔案路徑` 或 `:e 檔案路徑`，若路徑是目錄，會跳出列表供使用者選擇檔案

- 執行 bash 命令，
  - 方法1，`:terminal`，在新視窗開啟 terminal，需要 `<i>` 才能輸入命令
    > 此方法會取代原本的視窗，將原本的視窗退到背景，直到輸入exit並輸入q退出
  - 方法2，`:!命令`，直接在當前視窗執行，且會立刻執行
  - 方法3，`:vs|:terminal`，打開分割視窗後，開啟 terminal，適合同時需要編輯代碼和檢視terminal
    > 和 :terminal 不同，此方法不會取代原本的視窗

- 直接執行lua-api
  - 可用於插件沒有提供命令時，可以透過 lua-api 執行特定的命令
  - 查詢可用的 `lua-api，:h 套件名.方法()`，例如，`:h MiniStarter.open()`
  - 執行，`:lua MiniStarter.open()`

- 離開 neovim
  - 離開，`:quit` 或 `:q` 或 `:exit` 或 `:x` 或 `<ZZ>`
  - 離開並放棄存檔，`:q!`
  - 離開所有視窗，`:qa`，若有未存檔會有提示
  - 離開所有視窗並放棄存檔，`:qa!`
  - 寫入並離開，`:wq`

- snippet
  - 建立 snippet，`:ab <略縮詞> <擴展詞>`
  - 使用，當輸入略縮詞後，會自動將略縮詞變更為擴展詞
  - 對於不需要擴展的略縮詞，使用 `<略縮詞><ctrl+v>` 可以取消當前略縮詞的擴展
  - 注意，除非將 snippet 加速配置檔中，否則 snippet 只有在當前 buffer 中有效，
    buffer 關閉後就自動失效

## 常用快速鍵

- 特殊按鍵
  - `<S-Tab>` == Shift + tab
  - `<C-n>` == Ctrl + n
  - `<M-k>` == Meta + k，注意，Meta鍵有`可能是 window鍵或alt鍵`

- 複製，
  - `<y>`，複製選擇的目標
  - `<yw>`，複製當前位置，到`字尾`
  - `<yy>`，整行複製
  
- 貼上，`<p>`，paste 或 put
- 縮進，`<shift> + > + >` 或 `<shift> + < + <`
- 變更內容，`<c> + <w>`，change word
- 剪下行 & 貼上行，`<dd> + <p>`
- 複製行 & 貼上行，`<yy> + <p>`
- 重複上一個命令，`<.>`
- 復原(undo)，按下`<u>`
- 重做(redo)，按下`<ctrl> + <r>`
- 將當前buffer，回復到上一次/下一次的內容，`<ctrl> + <o>` 或 `<ctrl> + <i>`

- 直接進入 insert-mode 的幾種方式
  - `<s>`，刪除當前位置的字元，並進入 insert-mode
  - `<S>`，刪除當前行的所有字元，並進入 insert-mode

  - `<I>`，移動到最前方，並進入 insert-mode

  - `<a>`，移動到目前位置的後方，並進入 insert-mode
  - `<A>`，移動到最後方，並進入 insert-mode

  - `<O>`，移動到上一行，並進入 insert-mode
  - `<o>`，移動到下一行，並進入 insert-mode

  - `<r>`，進入 replace-mode，修改一個字元後，會立刻回到 normal-mode
  - `<R>`，進入 replace-mode，可修改多個字元，需要手動按下`<esc>`，才會回到 normal-mode

- 移動
  - 行內移動
    - 依 word `向右`進行行內移動，每次移動到`字首`，`<w>`，忽略特殊符號，`<W>` (word)
    - 依 word `向右`進行行內移動，每次移動到`字尾`，`<e>`，忽略特殊符號，`<E>` (end)
    - 依 word `向左`進行行內移動，每次移動到`字首`，`<b>`，忽略特殊符號，`<B>` (back)
    - 向右跳到指定字母，`<f> + <字母>` 自動移動到後一個字母出現的位置 (forward)
    - 向左跳到指定字母，`<F> + <字母>` 自動移動到前一個字母出現的位置
    - 向右跳到指定字母之前，`<t> + <字母>` 自動移動到後一個字母出現的位置之前 (till)
    - 向左跳到指定字母之後，`<t> + <字母>` 自動移動到前一個字母出現的位置之後

  - 跳到指定行
    - 按下 `<%>` 直接移動到括號
    - 按下`<數字> + j`，或`<數字> + <下>`，或`<數字> + G`，
      或`<數字> + <Enter>`，向下跳到相對行數
    
    - 按下`<數字> + k`，或`<數字> + <上>`，向上跳到相對行數

  - 移動到第一行，`<gg>`
  
  - 移動到最後一行，`<G>`

  - 移動到空白行，`<{>` 或 `<}>`

  - 移動到前一個位置，`<g> + <->`
  
  - 透過命令移動行
    - 格式，`:/要移動的字串/<移動選擇對象>m<指定移動位置>`，m 代表 move 移動
    - 範例，`:/aaa/m$`，將包含 aa 的行，移動到最後一行
    - 例如，`:/aaa/+1m-3`，
      - 尋找包含 aa 的行，為目標行
      - +1 選擇目標行往下一行，為新目標行
      - m  進行移動操作
      - -3 將新目標行往上移動3行

- 捲動
  - 換下一頁(PageDown)，`<ctrl> + <f>`，大跳
  - 往下跳到當前螢幕1/2的位置，`<ctrl> + <d>`，中跳
  
  - 換上一頁(PageUp)，`<ctrl> + <b>`，大跳
  - 往上跳到當前螢幕1/2的位置，`<ctrl> + <u>`，中跳

- 選取
  - 速記
    - `<i>` == inner 有不包含的意思，
    - `<a>` == around 有包含的意思

  - 進入選取模式，不選取當前行，按下`<v>`
  - 進入選取模式，選取當前整行，按下`<V>`，可以簡化選取多行的操作
  - 選取多行/多字，`<v> + <方向鍵>` 或 `<v> + <hjkl>`
  - 選取當前位置的單字，不包含其他符號，`<v> + <i> + <w>`
  - 選取當前位置的單字，包含其他符號，`<v> + <a> + <w>`
  - 從目前位置開始選取，直到指定字母停止，`<v> + <f> + <字母>`
  - 選取括號內的所有符號，不包含符號，`<v> + <i> + <(>`
  - 選取括號內的所有符號，包含符號，`<v> + <a> + <(>`
  - 選取HTML tag內的文字，，`<v> + <a> + <t>`

- 刪除(剪下)，
  - 往前刪除(剪下)1個字元，`<X>`
  - 刪除整行，`<dd>`
  - 刪除到行首，`<d^>` 或 `<d0>`
  - 刪除到行尾，`<d$>`
  - 往前刪除一個 word，`<dw>`
  - 往前刪除一個 word，`<db>`
  - 刪除以下三行，`<d3d>`

- 變更(Change)，自動進入 insert-mode
  - 注意，change 比 dd 更好用的是，不需要還要手動i才能進入 insert-mode

  - 從目前位置到`字尾`進行變更，`<c> + <e>` 或 `<c> + <w>` 
  - 變更當前word，`<c> + <i> + <w>`，會刪除目前位置的word，並進入insert-mode

  - 從目前位置到`行尾`進行變更，`<C>` 或，`<c> + <$>`
    會刪除目前位置到字尾的所有字元，並進入insert-mode

- 搜尋
  - 兩種搜尋方式
    - `</>`，向下搜尋
    - `<?>`，向上搜尋

  - 建立搜尋，
    - 方法1，按下 `</> + 搜尋字串 + <Enter>`
    - 方法2，`<#>`或`<*>`
      - 連續按下`<#>`，會將游標移動到上一個高亮的word
      - 連續按下`<*>`，會將游標移動到下一個高亮的word

  - 在搜尋結果中移動
    - `<n>`，移動到下一個搜尋結果
    - `<N>`，移動到上一個搜尋結果
  
  - 取消搜尋結果 (取消高亮)，`:nohlsearch`

- 搜尋並取代字串
  - 注意，透過 / 搜尋是全局的，透過 global-substitute 才有區分向上搜尋或向下搜尋
  
  - 範例，透過搜尋
    - step1，`/搜尋字串`
    - step2，`<enter>`，跳到第一個搜尋結果
    - step3，`<c> + <g> + <n>`，進入 insert-mode 並變更內容，變更後，透過`<esc>`回到 normal-mode
      > cgn == change goto next == change the next search match
    - step4，`<n>` 或 `<N>` 移動到下一個搜尋結果
    - step5，若當前搜尋結果需要修改，按下 `<.>` 重複執行 `<c> + <g> + <n>`，否則繼續移動
  
  - 範例，透過全局取代(global-substitute)的命令

    - 狀況1，只對當前行有效，`:s/搜尋的字串/替換的字串/g`，s代表 substitute，行尾的指令可以是
      - g 代表 global，表示會執行多次，可以不添加，預設會加上，
      - 1 代表只取代第一次出現的地方，
      - gc 代表取代前進行詢問(check)，使用者需要輸入按鍵決定是否取代
  
    - 狀況2，對當前文件的所有行都有效，`:%s/搜尋的字串/替換的字串`
      - % 代表整個文件

    - 狀況3，只對選取的行進行修改
      - 方法1，
        - `<ctrl> + <q>` 進入 visual-block模式
        - 選擇要處理的行
        - 按下`<:>`
        - 執行 `:'<,'> s/123/456/g`

      - 方法2，`:10,20 s/123/456/g`

- 搜尋並執行命令(g)
  - 範例，搜尋並執行刪除，
    - `:g/123/d`，尋找所有符合123的行，並執行d(刪除命令)
    - `:g!/123/d`，尋找所有不符合123的行，並執行d(刪除命令)，或直接執行 `:v/123/d`，以 v 取代 g!

  - 範例，為每一行，都添加 \n
    - `:g/^/put="\n"`，錯誤，" 是特殊用字，需要加上跳脫符號
    - `:g/^/put=\"\n\"`，正確

  - 範例，將所有開頭的test行，都移動到最後，`:g/^test/m$`

- 標記(mark)
  - 用途，標記用於在文件中快速移動
  - 建立標記，按下 `m+<字母>`，會對當前行建立標記
  - 移動到指定標記，按下 `<Meta> + <字母>`
  - 刪除指定標記，`:delmarks <字母>`
  - 刪除所有標記，`:delmarks a-zA-Z0-9` 或 `:delmarks!`

- 分割視窗(split-window)
  - 建立分割視窗
    - 建立下方水平分割視窗，`<ctrl> + <w> + <h>`，或 `:new`，或 `:sp`，或 `<ctrl> + <w> + <n>`
    - 建立右側垂直視窗，`<ctrl> + <w> + <s>`，或 `:vnew`，或 `:vs`，或 `<ctrl> + <w> + <v>`
  
  - 移動到不同的分割視窗，
    - `<ctrl> + <w> + <hjkl>`
    - `<ctrl> + <w> + <上下左右>`
    - 循環切換，`<ctrl> + <w> + <w>`
  
  - 調整分割視窗的大小，
    - 調整左右，`<ctrl> + <w>` 後，按下 `<shift> + <` 或 `<shift> + >`
    - 調整上下，`<ctrl> + <w> + <+>` 或 `<ctrl> + <w> + <->`
    - 透過命令調整視窗大小
      - 調整水平視窗的高低，`:resize +5`，或 `:resize -5`，或 `:res -5`
      - 調整垂直視窗的左右，`:vertical resize +5`，或 `:vertical resize -5`，或 `:vert res -5`
  
    - 放到最大，`<ctrl> + <w> + <_>`
    - 調整為等寬，`<ctrl> + <w> + <=>`
    - 交換分割視窗的內容，`<ctrl> + <w> + <R>`
  
  - 更換布局
    - 更換為水平布局，`<ctrl> + <w> + <K>`
    - 更換為垂直布局，`<ctrl> + <w> + <H>`
    - 將當前的分割視窗切換到指定位置，`<ctrl> + <w> + <HJKL>`
  
  - 關閉分割視窗，
    - 方法1，`<ctrl> + <w> + <q>`，或 `<ctrl> + <w> + <c>` for windows close
    - 方法2，在分割視窗中輸入，`:q`
  
  - 範例，將檔案開起在新的分割視窗，並指定視窗尺寸，`:10sp ttt.log`

- tab管理
  - 建立新的分頁和空編輯頁面，`:tabnew`
  - 切換 tab
    - 跳到指定 tab，`<N> + <g> + <t>`，N為tab上的數字
    - 跳到下一個 tab，`<g> + <t>`，或 `<ctrl> + <PageDown>`
    - 跳到前一個 tab，`<g> + <T>`，或 `<ctrl> + <PageUp>`

- buffer
  - 查看 buffer，`:buffers` 或 `:ls`
  - 透過編號選擇 buffer，`:buffer 編號` 或 `:buffer 檔名`
  - 透過指令選擇 buffer，`:bnext`、`:bprev`、`:bfirst`、`:blast`
  - 刪除 buffer `:bdelete 檔名` 或 `:1,4bd`，刪除 1-4 的buffer

- 批量處理
  - 命令模式進行批量處理的格式: `起始行號,終止行號 操作按鍵`

  - 範例，`批量縮進`，visual-block | 命令 | 巨集，三種方法比較
    - 方法1，透過 visual-block模式
      - `<ctrl> + <q>` 進入 visual-block模式
      - 選擇要處理的行
      - 按下`<tab>`

    - 方法2，透過命令模式
      - `<:>` 進入命令模式
      - 例如，`:10,12 >`，對 10 - 12 行進行縮進處理

    - 方法3，透過 macro
      - 建立 macro
        - 按下 `<q> + <自定義巨集編號>` 開始錄製，此時，左下角會出現 recording @1
        - 按下縮進的操作
        - 按下`<q>`停止錄製
  
      - 回放 macro
        > 不需要進入命令模式，直接輸入 `執行次數@巨集編號`，

  - 範例，透過 virsual-block 進行`多行註解`
    - 添加註解
      - step1，`<ctrl> + <q>`，進入 visual-block 模式
      - step2，移動 `<上下左右>`，選擇要處理的行，
      - step3，`<shift> + <i>`，進入 insert 模式，此時，游標會跳到選取範圍的最上方
      - step4，輸入要註解的符號，例如，#
      - step5，`<esc>`，vim 會自動為選取的每一行都添加註解
    
    - 取消註解
      - step1，`<ctrl> + <q>`，進入 visual-block 模式
      - step2，移動 `<上下左右>`，選擇要處理的行，
      - step3，`<x>`，取消選取行前方的註解
    - 或使用 [comment.nvim](#插件-commentnvim-快速添加移除註釋) 為多行添加註解

  - 範例，添加雙引號" (不同長度的行)
    - 目的，將 abc 變成 "abc"
  
    - 添加前置引號
      - step1，`<ctrl> + <q>`，進入 visual-block 模式
      - step2，選中要處理的行
      - step3，`<:>` 進入命令模式，此時會顯示 `:'<,'>`
        - '< 代表被選中行中的第一行的行號
        - '< 代表被選中行中的最後一行的行號
        - 在 visual-block 之後按下 `<:>`，會自動帶入被選中的行號
      - step4，將命令修改為`:'<,'> norm I"`，並執行
        - norm 代表切換到 normal-mode
        - I    移動到最前方，並進入 insert-mode
        - "    插入"
    
    - 添加後置引號
      - step1，`<ctrl> + <q>`，進入 visual-block 模式
      - step2，選中要處理的行
      - step3，`<:>` 進入命令模式，此時會顯示 `:'<,'>`
        - '< 代表被選中行中的第一行的行號
        - '< 代表被選中行中的最後一行的行號
        - 在 visual-block 之後按下 `<:>`，會自動帶入被選中的行號
      - step4，將命令修改為`:'<,'> norm A"`，並執行
        - norm 代表切換到 normal-mode
        - A    移動到最後方，並進入 insert-mode (注意，在normal-mode按下i，並不會出現在最後)
        - "    插入"

  - 範例，搜尋與取代(s)，請參考[搜尋並取代]()一節

  - 範例，搜尋與取代(g)，請參考[搜尋並執行]()一節

## 解決按鍵衝突

- 查看所有按鍵映射，
  - `:map`，查看所有模式
  - `:imap`，只查看insert-mode
  - `:nmap`，只查看normal-mode

- 查看衝突的映射，
  - 方法1，`:checkhealth which_key`
  - 方法2，`:WhichKey 按鍵`

- 查看使用該按鍵的所有配置，
  - `:verbose map 按鍵`，查看所有模式
  - `:verbose imap 按鍵`，只查看 insert-mode

## 進階操作

- 範例，將 shell 的輸入複製到當前文件中，
  - `:read !cat /etc/xrdp/startwm.sh`，複製到當前游標位置
  - `:12read !cat /etc/xrdp/startwm.sh`，移動到第12行 > 讀取 > shell 輸出，將結果輸出到第13行
  - `:$read !cat /etc/xrdp/startwm.sh`，移動到最後 > 讀取 > shell 輸出，將結果輸出到最後

## ref
- [vim 使用教學](http://www.study-area.org/tips/vim/Vim-7.html)
- [Zero to IDE with neovim + LazyVim](https://www.youtube.com/watch?v=N93cTbtLCIM)
- [vim插件搜尋，VimAwesome](https://vimawesome.com/)
- [awesome-nvim @ github](https://github.com/rockerBOO/awesome-neovim)
- [避免發生錯誤，改用notify 進行通知](https://youtu.be/RtNPfJKNr_8?t=479)
- [Colorschemes and how to set them](https://www.youtube.com/watch?v=RtNPfJKNr_8)
- [VIM學習筆記，含效果圖](https://yyq123.github.io/learn-vim/)
- [Full Neovim Setup from Scratch in 2025](https://www.youtube.com/watch?v=KYDG3AHgYEs)

- 系列文章
  - [Neovim配置實：從0到1打造自己的IDE @ 之乎](https://zhuanlan.zhihu.com/p/571617696)
  - [Neovim配置實：從0到1打造自己的IDE @ github](https://github.com/nshen/learn-neovim-lua)
  - [從零開始配置vim * 31](https://zhuanlan.zhihu.com/p/554650619)
  - [學習neovim全lua配置](https://test.ocom.vn/?url=github.com/nanofrog/learn-neovim-lua)
