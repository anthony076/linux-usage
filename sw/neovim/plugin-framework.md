## framework 配置框架

- 配置框架，指的是預先配置和安裝的一系列插件，安裝後就可以直接使用的插件
- 每一個配置框架對 keybindings 有重新配置和優化過，與原生 neovim 的配置不同

## [配置框架] LazyVim

- [官方網站]((https://github.com/folke/lazy.nvim#-usage))

- 優缺點
  - 預安裝 50 個常用插件，開箱即用
  - 功能多但不夠精簡

- 安裝 
  - `step0`，安裝依賴
    - sudo pacman -S neovim unzip 
    - sudo pacman -S lazygit，選用，有使用 lazygit 才需要
    - sudo pacman -S ripgrep，選用，有使用 telescope 才需要
    - sudo pacman -S fd，選用，有使用 telescope 才需要
    - sudo pacman -S ripgrep，選用，有使用 telescope 才需要

  - `step1`，備份 neovim 的配置文件
    ```shell
    mv ~/.config/nvim ~/.config/nvim.bak
    mv ~/.local/share/nvim ~/.local/share/nvim.bak
    mv ~/.local/state/nvim ~/.local/state/nvim.bak
    mv ~/.cache/nvim ~/.cache/nvim.bak
    ```

  - `step2`，複製 lazygit 的配置檔，
    `$ git clone https://github.com/LazyVim/starter ~/.config/nvim`

  - `step3`，啟動 neovim，`$ nvim`
   
- 插件
  - lazyvim 使用lazy.nvim作為套件管理，參考，[lazy.nvim 的使用](#插件管理-lazynvim)
  - [LazyVim預設安裝的插件和設置](https://github.com/LazyVim/LazyVim/blob/1f7be0bbad3012046a53edb649b3cdc085e7ed54/lua/lazyvim/plugins/editor.lua)

- 命令
  - `:Lazy`，打開 lazynvim 的主頁面
  - `:Lazy Install`，打開 lazynvim 的 Install 頁面
  - `:Lazy Update`，打開 lazynvim 的 Udpate 頁面

- 常用快速鍵

  - `檔案`
    - 在水平視窗開啟新檔案，`<space> + <->`
    - 在垂直視窗開啟新檔案，`<space> + <|>`
    - 開啟 terminal 執行 bash 命令，`<space> + <f> + <t>`

  - 使用 which-key 的快速鍵提示
    - 按下`空白鍵`，開啟 which-key.nvim，用於提示 key-bindings
      - `<space>`，搜尋檔案
      - `e`，打開當前目錄的文件瀏覽，`q` 離開
      - `backspace`，顯示編輯文件的快速鍵
      - `w`，進入Window相關命令
      - `c`，進入code相關命令
      - `u`，進入UI相關命令
        - `l`，Line Numbers 的開關
  
  - 選中變數，並進行一次性全部修改
    - `方法1`，透過 lsp (language-server-protocol) 實現，以 python 為例
      - step，`:LspInstall python` 後，選擇 `pylsp` 進行安裝，安裝後重新啟動 neovim
      - step，將游標移動到變數後，`<space> + <c> + <r>`，對變數一次性變更
    
    - `方法2`，透過 nvim-spectre 套件，參考，`常用快速鍵 > 搜尋 > 搜尋並取代`

  - 添加/取消多行的註解
    - `<V>`，選取多行
    - `<g>`，goto
    - `<c>`，comment selection

  - `緩衝區(buffer)`，未存檔的檔案
    - 檢查 buffer 工作區，`<space> + <f> + <b>`
    - 跳轉前一個 buffer，`<[>` + `<b>`，或直接使用滑鼠點選 tab
    - 跳轉後一個 buffer，`<]>` + `<b>`，或直接使用滑鼠點選 tab

  - `分割視窗(Split-windows)`
    - 建立下方水平分割視窗，`<space> + <->`
    - 建立右側垂直視窗，`<space> + <|>`
    - 移動到不同的分割視窗，`<ctrl> + <hjkl>`
    - 調整分割視窗的大小，`<ctrl> + <上下左右>`
    - 刪除當前 window，`<space> + <w> + <d>`

  - `tab管理`
    - 建立新的分頁和空編輯頁面，`:tabnew` 或 `<space> + <Tab> + <Tab>`
    - 使用 `:e 檔案路徑`，也會開起在新分頁中
    - 切換 tab
      - 跳到指定 tab，`<N> + <g> + <t>`，N為tab上的數字
      - 跳到前一個 tab，`<g> + <T>`，或 `<ctrl> + <PageUp>`
      - 跳到下一個 tab，`<g> + <t>`，或 `<ctrl> + <PageDown>`

  - 切換功能開關，`<space> + <u> + <功能鍵>`

  - 搜尋工作區內的所有 todo，`<space> + <s> + <t>`，
    注意，需要使用 `TODO:` 才能被識別

  - 搜尋
    - 搜尋快速鍵，`<space> + <s> + <k>`
    - 搜尋並取代，`<space> + <s> + <r>` (需要安裝 nvim-spectre 套件)
      - Search 欄位填入要搜尋的字串，
      - Replace 欄位填入要替換的字串，
      - Path 欄位填入要作用的檔案，例如，ttt.py 或 *.py
      - 完成輸入後，會在下方出現搜尋結果，按下 <?> 顯示可對指定項進行的操作，例如，排除某個檔案
      - 按下 `<space> + <R>` 進行替換

  - `Git`: 使用 lazygit
    - 注意，主機上需要安裝 lazygit 套件，以下功能會調用外部的 lazygit 命令
    - 按下`<Esc>`取消歡迎畫面
    - 利用滑鼠切換區域
    - 按下`<?>`顯示可用功能鍵 (不同的區域有不同的命令)

  - `代碼相關`
    - 顯示函數定義，`<K>`

    - 分析代碼和錯誤跳轉，
      - 對開啟的文件進行分析，`<space> + <x> + <x>`
      - 對工作區的所有文件進行分析，`<space> + <x> + <X>`
      
    - 對有問題的代碼進行處理，
      - `<space>`，leading-key
      - `<c>`，code-functions
      - `<a>`，action

## [配置框架] LunarVim

- [lunarvim](https://www.lunarvim.org/docs/installation)

## [配置框架] SpaceVim

- [官方網站](https://spacevim.org/)

- [介紹SpaceVim](https://www.youtube.com/watch?v=iXPS_NHLj9k&list=PL5--8gKSku15tivUyt0D-mERePLEzrWUz&index=5)

## [配置框架] VapourNvim

- [官方網站](https://github.com/VapourNvim/VapourNvim)

## [配置框架] NvChad

- [官方網站](https://github.com/NvChad/NvChad)
