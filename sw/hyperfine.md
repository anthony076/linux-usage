## hyperfine : benchmark-tool

- [hyperline官網](https://github.com/sharkdp/hyperfine)



- 語法，`hyperfine 命令1 命令2`

- 範例，`$ hyperfine 'sleep 0.3' 'sleep 0.5'`

- 範例，`$ hyperfine 'grep -rl "cheat*" .' 'rg cheat' --shell=none --runs=10`
  - --shell=none，設置要執行命令的 shell，若設置為none，會直接執行命令
    
    此選項可避免以下提示
    ```
    Warning: Command took less than 5 ms to complete. Note that the results might be inaccurate because hyperfine can not calibrate the shell startup time much more precise than this limit. You can try to use the `-N`/`--shell=none` option to disable the shell completely.
    ```
  --run=次數，指定執行的次數