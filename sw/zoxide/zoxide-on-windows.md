
## zoxide + nushell on Windows

- 需求(測試過的環境)
  - nushell v0.94.2
  - zoxide v0.9.2

- 安裝，`$ choco install zoxide`

- 配置
  - step，產生 zoxide 配置文件，`$ zoxide init nushell | save -f ~/.zoxide.nu`
  - step，修改 nushell 配置
    - 打印 nushell 配置文件的位置，`$ nu.config-path`，通常是，`%AppData%\Roaming\nushell\config.nu`
    - 將 `source ~/.zoxide.nu` 添加到 config.nu 的末尾中
  - step，修改 .zoxide.nu 文件，[修改後的版本](.zoxide.nu)
  - step，重啟 nushell

- ref 
  - [Zoxide's init-script generated for nu-shell fails with error upon sourcing](https://github.com/ajeetdsouza/zoxide/issues/808)