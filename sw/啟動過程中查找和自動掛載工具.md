## 啟動過程中查找和自動掛載工具

- 此類工具通常提供 initramfs 生成工具，
  並提供啟動過程中`自動查找`和`掛載initramfs`的功能

- nlplug-findfs : 專用於 alpine

- initramfs-tools : 專用於 Debian/Ubuntu
  - 配置文件位於/etc/initramfs-tools/initramfs.conf

- dracut : 專用於 Fedora/RHEL/CentOS
  - 配置文件位於 /etc/dracut.conf 和 /etc/dracut.conf.d/

- mkinitcpio : 專用於 Arch Linux
  - 配置文件位於 /etc/mkinitcpio.conf

- BusyBox-initramfs : 常用於嵌入式系統

- systemd-boot : 常用於 管理 UEFI 啟動過程