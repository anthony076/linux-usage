## trap 命令的使用

- 語法，`trap 要執行的命令 觸發信號1 觸發信號2 ...`

- 範例，`$ trap 'chattr -Rf -i "$root" || true && rm -rf "$root" || true' INT TERM EXIT`

  - 目的 : 
    
    捕獲`INT TERM EXIT`等訊號時，執行`chattr -Rf -i "$root" || true && rm -rf "$root" || true`命令

    此命令常用於安裝過程中，當遇到 (按下Ctrl+C|執行kill|腳本執行結束) 的情況時，都會執行定義的清理命令，
    有助於保持系統的乾淨狀態，防止腳本運行後留下殘留文件或目錄。
  
  - 觸發訊號，`INT TERM EXIT`是三種不同的訊號
    - INT：使用者按下`Ctrl+C`按鍵時觸發
    - TERM：使用者透過`kill`命令時觸發
    - EXIT:`腳本正常執行完畢`後觸發

  - 執行命令中，`chattr -Rf -i "$root"`
    - chattr : change attribute，用於變更指定目錄的屬性
    - -R : 遞歸操作，變更屬性的操作同樣適用於子目錄
    - -f : 強制執行，忽略任何錯誤
    - -i : immutable，指移除目錄的不可變屬性，+i指為目錄添加不可變的屬性
  
  - 執行命令中，`chattr -Rf -i "$root" || true`，確保命令會返回 true，讓命令即使發生錯誤也會繼續執行下一個命令
  
  - 執行命令中，`rm -rf "$root`，強制刪除指定目錄
  
  - 注意，若目錄有不可變屬性是不可刪除的，必須先移除不可變屬姓後，在執行刪除目錄的操作
  

trap 常見的使用範例

範例

trap 
