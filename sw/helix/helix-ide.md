## helix-ide 的使用

- why helix
  - 由 rust 編寫的ide，由 vim 啟發
  - 內建多個 LSP
  - 沒有插件系統，但是已經把常用功能內建，開箱即用
  - 和 vim 的差異
    - 沒有插件系統
    - 具有多光標模式
    - 先選取，再按下動作
    - 更多模式
    - f字元，可針對多行，在vim中，只能針對當前行

- 安裝 helix
  - windows: `$ choco install helix`
  
  - alpine: `$ apk add helix`，缺點，預設沒有 runtime 目錄，部分功能會失效

  - <a id='build-from-source'>build from source</a>

    ```shell
    git clone https://github.com/helix-editor/helix
    cd helix

    # 設置 HELIX_RUNTIME，才能找的到主題檔，請能使內建功能正常
    echo "export HELIX_RUNTIME=~/.config/helix/runtime" > ~/.bashrc

    # 或者，若預設shell為fish，在 ~/.config/fish/config.fish 中加入以下
    #set -x HELIX_RUNTIME ~/.config/helix/runtime

    # 執行編譯
    cargo install --path helix-term --locked
    cp -R runtime/ ~/.config/helix/runtime/
    ```

    [移除](https://github.com/helix-editor/helix/discussions/5228)
    
    ```shell
    cd helix/helix-term
    cargo uninstall
    ```

- [設置 runtime 目錄](#runtime-目錄的建立與設置)

- 配置文件:
  - 文檔
    - [配置 config.toml](https://docs.helix-editor.com/configuration.html)
    - [配置languages.toml](https://github.com/helix-editor/helix/wiki/Language-Server-Configurations)
  
  - 預設目錄
    - 類型1，不需要同步的配置文件，預設會放在`Local目錄`中，`%AppData%\Local\helix\`
    - 類型2，需要同步的配置文件，預設會放在`Roaming目錄`中，`%AppData%\Roaming\helix\`
      > 透過指定RUNTIME變數(%HELIX_RUNTIME%)，可以變更此預設位置
    - 類型3，專案用目錄: `.\.helix\`
    - 同步目錄的優先權 : 專案用目錄 > 手動指定的RUNTIME目錄 > 預設的Roaming目錄

  - 文件
    - 主配置文件: `%AppData%\Roaming\helix\config.toml` 或 `~/.config/helix/config.toml`
    - language-server 配置文件: `%AppData%\Roaming\helix\languages.toml` 或 `~/.config/helix/languages.toml`
    - log : `%AppData%\Local\helix` 或 `~/.cache/helix/helix.log`

## runtime 目錄的建立與設置

- runtime 目錄的功能
  - 正確讀取主題檔
  - 使內建的語法高亮生效
  - 實現內建的LSP功能

- step，設置 runtime 目錄
  - 透過全局的 HELIX_RUNTIME 變數可以設置runtime目錄的實際位置，例如，`$ export HELIX_RUNTIME=/home/ching/runtime`
  
  - 注意，`helix-repo中的runtime目錄`不能直接拿來使用，`必須是有效的runtime目錄`才能正常使用，設置完HELIX_RUNTIME變數後，還必須建立有效的runtime目錄

- step，建立有效的 runtime 目錄的位置

  - 什麼是有效的 runtime 目錄
    - `/runtime/theme 目錄`中有 *.toml，才能使 theme 生效
    - `/runtime/queries 目錄`中有 *.scm，才能使$ hx --health中顯示提供哪內建功能 (能顯示但不一定有作用)
    - `/runtime/grammar 目錄`中有 *.so 或 *.dll，才能使內建功能生效
  
  - 方法，[編譯 helix 源碼](#build-from-source)

  - 方法，下載 helix 源碼但不編譯 + 使用`hx --grammar`命令

    優點，不用下載rust編譯工具包，可以節省空間

    ```shell
    export HELIX_RUNTIME=/home/ching/.config/helix/runtime

    # 獲取 theme 和 queries 目錄
    git clone https://github.com/helix-editor/helix
    mv ~/helix/runtime/ ~/.config/helix/runtime

    # 必要，設置~/.config/helix/languages.toml，[以bash為例](languages.toml)，
    # 或參考[官方提供的範例](https://github.com/helix-editor/helix/blob/master/languages.toml)

    # 產出需要的檔案
    # 此命令默認目錄為 ~/.config/helix/runtime
    hx -g fetch
    hx -g build
    ```

    若出現問題，檢查`/.cache/helix/helix.log`

## 模式

- 可用模式
  - normal-mode : 
    - 根據按下的按鍵，會高亮並選取字符，新按鍵會取消之前的選取，以高亮識別是否以正確選取要操作的字串
    - 若要保留先前的選取，改用 visual-mode
  - insert-mode(i)
  - visual-mode(v)
  - space-mode : 用於打開快捷選單
  - goto-mode(g) : 用於游標的移動或跳轉
  - match-mode : 用於選擇字元
  - unimpaired-mode () : 由 [ 或 ] 提供與編程相關的功能

- Space-Mode 提供的功能

  ```
  f	Open file picker	file_picker
  F	Open file picker at current working directory	file_picker_in_current_directory
  b	Open buffer picker	buffer_picker
  j	Open jumplist picker	jumplist_picker
  g	Debug (experimental)	N/A
  k	Show documentation for item under cursor in a popup (LSP)	hover
  s	Open document symbol picker (LSP)	symbol_picker
  S	Open workspace symbol picker (LSP)	workspace_symbol_picker
  d	Open document diagnostics picker (LSP)	diagnostics_picker
  D	Open workspace diagnostics picker (LSP)	workspace_diagnostics_picker
  r	Rename symbol (LSP)	rename_symbol
  a	Apply code action (LSP)	code_action
  h	Select symbol references (LSP)	select_references_to_symbol_under_cursor
  '	Open last fuzzy picker	last_picker
  w	Enter window mode	N/A
  c	Comment/uncomment selections	toggle_comments
  C	Block comment/uncomment selections	toggle_block_comments
  Alt-c	Line comment/uncomment selections	toggle_line_comments
  p	Paste system clipboard after selections	paste_clipboard_after
  P	Paste system clipboard before selections	paste_clipboard_before
  y	Yank selections to clipboard	yank_to_clipboard
  Y	Yank main selection to clipboard	yank_main_selection_to_clipboard
  R	Replace selections by clipboard contents	replace_selections_with_clipboard
  /	Global search in workspace folder	global_search
  ?	Open command palette	command_palette
  ```

- Unimpaired-Mode 提供的功能

  ```
  Key	Description	Command
  ]d	Go to next diagnostic (LSP)	goto_next_diag
  [d	Go to previous diagnostic (LSP)	goto_prev_diag
  ]D	Go to last diagnostic in document (LSP)	goto_last_diag
  [D	Go to first diagnostic in document (LSP)	goto_first_diag
  ------------------
  ]f	Go to next function (TS)	goto_next_function
  [f	Go to previous function (TS)	goto_prev_function
  ------------------
  ]t	Go to next type definition (TS)	goto_next_class
  [t	Go to previous type definition (TS)	goto_prev_class
  ]T	Go to next test (TS)	goto_next_test
  [T	Go to previous test (TS)	goto_prev_test
  ------------------
  ]a	Go to next argument/parameter (TS)	goto_next_parameter
  [a	Go to previous argument/parameter (TS)	goto_prev_parameter
  ------------------
  ]c	Go to next comment (TS)	goto_next_comment
  [c	Go to previous comment (TS)	goto_prev_comment
  ------------------
  ]p	Go to next paragraph	goto_next_paragraph
  [p	Go to previous paragraph	goto_prev_paragraph
  ------------------
  ]g	Go to next change	goto_next_change
  [g	Go to previous change	goto_prev_change
  ]G	Go to last change	goto_last_change
  [G	Go to first change	goto_first_change
  ------------------
  ]Space	Add newline below	add_newline_below
  [Space	Add newline above	add_newline_above
  ```

## TOML 配置文件的語法

- 建立子鍵值的兩種寫法，

  <font color=blue>透過括號</font>

  `[主鍵名]`用於建立主鍵
  ```toml
  [d]
  x = 123
  y = 456
  ```

  <font color=blue>透過點</font>
  ```toml
  d.x = 123
  d.y = 456
  ```

  以上兩者皆等效於 JSON 格式
  ```json
  {
    "d": {
      "x": 123,
      "y": 456
    }
  }
  ```

- `[[鍵名]]`用於具有相同鍵名的列表

  例如，
  ```json
  {
    "a": [
      { "foo": 123, "bar": 456 },
      { "foo": 666, "bar": 777 }
    ]
  }
  ```
  
  等效於
  ```
  [[a]]
  foo = 123
  bar = 456

  [[a]]
  foo = 666
  bar = 777
  ```

## 快速鍵

- 大部分操作與vim相似，未列出者請參考[gvim](../gvim/gvim-usage.md)或[neovim](../neovim/readme.md)

- 打開快速功能選單，`space`

- 選取
  - 注意，以下命令需要在 NOR 模式下執行
  
  - 手動選取: 
    - `v`，進入選擇模式，
    - `方向鍵或hjkl`控制選取範圍

  - 選取整份文件的單字，`%`
  - 取消選取，`;`
  - 選取單詞，`miw`，match inside word，選取內部的單詞
  - 添加括號，`ms符號`，make surround，建立指定的括號符號
  
  - 選取整行:
    - 方法，在 NOR 模式下選取
      - 選取整行，`x`，連續按下x可選取多航
      - 選取多行，`數字x`
      - 注意，此方法在按下其他鍵後會取消選取，可以用來移動

    - 方法，透過 SEL 模式
      - `v`，進入選擇模式，
      - `x`，將選取範圍擴展到左右邊界
  
  - 多光標模式 (類似垂直選取，差異是，多光標是建立光標但未選取)
    - `shfit + c`，在當前位置下方的字串，建立新光標，
      - 若有空白行，會跳過空白行
      - 下方字串可以是任意字串
    - `,`，退出多光標模式，並停留在最後一個光標
    - `shift + ,`，逐次退出多光標模式

- 移動
  - 移動到第一行的起始位置，`gg`
  - 移動到最後一行的起始位置，`ge`，go end
  
  - 移動到當前行起始位置，`gh`，go home (go line start)
  - 移動到當前行最後位置，`gl`，go line (go line end)
  - 移動到當前行第一個非空白字元, `gs`，go start
  
  - 往後移動到下一個單詞的起始字元，`b`，begin
  - 往後移動到下一個單詞的結束字元，`e`，end

  - 移動到指定單詞 (類似vim-easymotion的移動方式)
    - `gw`，進入選擇模式
    - 按下單詞對應的快速鍵
    - 注意，helix 24.03 才有此功能

  - 往後尋找特定字元，並移動到該字元, `f字元`
  - 往後尋找特定字元，並移動到該字元之前(不包含指定字串)，`t字元`

  - 移動到指定行，
    - 方法，`5gg`
    - 方法，`5x`，往下選取15行，按下移動鍵後會取消選取，並停留在最後一行

- 打開當前游標位置的檔案，
  - `ctrl+w` -> `f`，打開於下方window
  - `ctrl+w` -> `F`，打開於右側window

- 文件跳轉
  - 方法，透過 buffer
    - `space` -> `b`，打開 buffer-list

  - 方法，透過 jumplist
    - `space` -> `j`，打開 jump-list
    - `ctrl + o`，跳到 jumplist 中的上一個
    - `ctrl + i`，跳到 jumplist 中的下一個

- LSP 跳轉
  - 到定義，`gd`
  - 到類型定義，`gy`
  - 到引用，`gr`
  - 到實現，`gi`
  - 到函數，`gf`

- 執行shell，`|`，並將結果打印在當前文件中

- 操作暫存區 (register)，用於暫存操作的臨時區域
  - `"`，用於手動選擇 register
  - 預設有以下暫存區
    - `@-register`，用於保存巨集的register
    - `_-register`，empty-register，任何保存於此的內容都會被清空
    - `#-register`，用於保存 selection-indices 內容的register
    - `.-register`，用於保存 selection-contents 內容的register
    - `%-register`，用於保存 document-path 的register
    - `*-register`，用於保存 system-clipboard 的 register，透過`ctrl+v`貼上
    - `+-register`，用於保存 primary-clipboard 的 register，透過`滑鼠中鍵或其他組合鍵`貼上

- 操作巨集 (Macro)
  - 進入巨集錄製，在 NOR 模式下，`shift + q`，將巨集錄製到`@-register`，右下角會有`[@]`的提示
  - 退出巨集錄製，在 NOR 模式下，且巨集錄製，再次按下`shift + q`後退出
  - 執行巨集，在 NOR 模式下，`q`，執行`@-register`的內容

- debug時的常用操作

  - 按鍵`<space + d>`，提供 diagnostic 相關操作的選單

  - 按鍵`<[>`或`<]>`，提供錯誤快速跳轉的選單

  - 複製 lsp-server 提供的錯誤訊息
    - 移動到出問題的行，確認有顯示LSP提供錯誤訊息
    - 透過`:yank-diagnostic`複製看到的訊息
  
  - jumplist
    - `<ctrl+o>`，跳轉到 jumplist 的下一個項目
    - `<ctrl+i>`，跳轉到 jumplist 的上一個項目

## 命令

- 參考，[所有可用命令的名字](https://github.com/helix-editor/helix/blob/master/helix-term/src/commands.rs)

- 開啟config.toml，`:config-open`

- 進入 tutorial，`:tutor`

- 將剪貼板的內容貼上至當前buffer，`:clipboard-paste-after` 或 `:clipboard-paste-after`

- 執行 shell
  - 方法，`:run-shell-command 命令` 或 `:sh 命令`，將結果顯示於臨時的分割視窗中
    - `ctrl+d`，往下捲動結果頁面
    - `ctrl+u`，往上捲動結果頁面
    - `esc`，關閉臨時的分割視窗

  - 方法，`:pipe 命令`，將結果寫入當前的文本中

  - 方法，`:insert-output 命令`，，將結果寫入當前的文本中

- 檢查 LSP 的狀態，在 shell 下，`$ hx --health`

## 按鍵綁定

- 幾種按鍵方式
  - 同時鍵 : 需要同時按下，透過`-`連接兩個鍵，
    例如，`C-a`，代表同時按下 Ctrl+a
    
  - 連續鍵 : 不需要同時按下，只需要依序按下，有以下幾種編寫方式
    
    以下幾種方法是等效的

    <font color=blue>透過<font color=red> 點(.) </font></font>
    ```toml
    [keys.normal]
    s.g = ["move_prev_word_start", "move_next_word_end", "change_selection"]
    s.s = ":write!"
    ```

    <font color=blue>透過<font color=red> 花括號 { }</font></font>
    ```toml
    # 缺點，不能使用換行
    [keys.normal]
    s = { g = ["move_prev_word_start", "move_next_word_end", "change_selection"], s = ":write!" }
    ```

    <font color=blue>透過<font color=red>標題</font>定義首鍵</font>
    ```toml
    # keys.模式名.首鍵名
    [keys.normal.s]   # 在 normal 下按下s鍵後
    q = ["move_prev_word_start", "move_next_word_end", "change_selection"]
    s = ":write!"
    ```

- 沒有 "Shift-v" 的用法，直接使用 V 來取代

- 命令，例如，`ove_prev_word_start`，可透過[官方提供的keymaps](https://docs.helix-editor.com/keymap.html)查詢

## 範例集

- 範例，多行同步修改 (可用於高亮所有搜尋到的字串)
  - 利用多光標模式實現
  - step，`v` -> `x` -> `方向鍵或hjkl`，選取範圍
  - step，`s`，輸入選取的字串，輸入 `Enter` 後，會以多光標自動選取多個匹配的位置，並進入選取模式(v)
  - step，進行修改
  - step，`,`，取消多光標模式

- 範例，刪除單字，`bwd`

- 範例，刪除整份文件內容，`%d`

## Snippets 

- 建立代碼片段
  - [討論](https://github.com/helix-editor/helix/issues/395#issuecomment-1671394603)
  - [透過 lsp](https://github.com/estin/simple-completion-language-server)
  - [透過 shell-script](htts://www.youtube.com/shorts/Ev1n7y_RVOE)

## LSP

- 檢查 LSP server 的安裝情況
  - `$ hx --health`
  - `$ hx --health bash`

- 檢視錯誤紀錄，`$ cat ~/.cache/helix/helix.log`

- [安裝和配置LSP](https://github.com/helix-editor/helix/wiki/Language-Server-Configurations)

- 可客製化snippet和code-action的LSP
  - [hx-lsp](https://github.com/erasin/hx-lsp)

- LSP server 的安裝與設置
  - [lsp for bash](lsp-bash.md)
  - [lsp for clangd](lsp-clangd.md)
  - [lsp for markdown](lsp-markdown.md)

## ref

- [中文文檔](htts://zjp-cn.github.io/helix-book/usage.html)

- config.toml 範例
  - [@hadronized](https://github.com/hadronized/config/blob/master/helix/config.toml)
  - [@ravsii](https://github.com/ravsii/.helix/blob/main/config.toml)
  - [@LGUG2Z](https://github.com/LGUG2Z/helix-vim/blob/master/config.toml)
  - [@erasin](https://github.com/erasin/helix-config/blob/main/config.toml)

- 使用範例
  - [Helix，the Rust Powered Development Environment](https://www.youtube.com/watch?v=xHebvTGOdH8)
