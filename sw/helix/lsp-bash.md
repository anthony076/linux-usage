## lsp for bash

- 安裝 bash-language-server @ alpine
  
  ```
  apk add nodejs npm
  npm install -g bash-language-server
  ```

- language.toml 的配置

  ```toml

  [language-server]

  bash-language-server = { command = "bash-language-server", args = ["start"] }

  [[language]]
  name = "bash"
  scope = "source.bash"
  injection-regex = "(shell|bash|zsh|sh)"
  file-types = [
    "sh",
    "bash",
    "ash",
    "dash",
    "ksh",
    "mksh",
    "zsh",
    "zshenv",
    "zlogin",
    "zlogout",
    "zprofile",
    "zshrc",
    "eclass",
    "ebuild",
    "bazelrc",
    "Renviron",
    "zsh-theme",
    "cshrc",
    "tcshrc",
    "bashrc_Apple_Terminal",
    "zshrc_Apple_Terminal",
    { glob = "i3/config" },
    { glob = "sway/config" },
    { glob = "tmux.conf" },
    { glob = ".bash_history" },
    { glob = ".bash_login" },
    { glob = ".bash_logout" },
    { glob = ".bash_profile" },
    { glob = ".bashrc" },
    { glob = ".profile" },
    { glob = ".zshenv" },
    { glob = ".zlogin" },
    { glob = ".zlogout" },
    { glob = ".zprofile" },
    { glob = ".zshrc" },
    { glob = ".zimrc" },
    { glob = "APKBUILD" },
    { glob = ".bash_aliases" },
    { glob = ".Renviron" },
    { glob = ".xprofile" },
    { glob = ".xsession" },
    { glob = ".xsessionrc" },
    { glob = ".yashrc" },
    { glob = ".yash_profile" },
    { glob = ".hushlogin" },
  ]
  shebangs = ["sh", "bash", "dash", "zsh"]
  comment-token = "#"
  language-servers = [ "bash-language-server" ]
  indent = { tab-width = 2, unit = "  " }

  [[grammar]]
  name = "bash"
  source = { git = "https://github.com/tree-sitter/tree-sitter-bash", rev = "f8fb3274f72a30896075585b32b0c54cad65c086" }
  ```

- 建構需要的檔案，
  - `$ hx -g fetch` 
  - `$ hx -g build` 