## lsp-clangd 的安裝與使用

- 安裝 clangd 後，打開源碼通常會出現錯誤，cland 需要進行配置才能找到正確的 headers，
  因此，`clangd 的報錯不一定是真的錯誤`，有可能只是沒有正確的配置 clangd

- clangd的配置文件分為兩種
  - `compile_commands.json`，
    - 用於提供編譯命令等訊息
    - 不執行編譯，只提供編譯信息、用於代碼分析、自動補全、語法檢查等
    - 真正執行編譯仍然需要 Makefile
    - 可透過 cmake、bear、Intercept-build(from clang)、

  - `.cland`，用於配置 cland 服務器的行為，可以用來抑制不需要的錯誤訊息，例如，

    ```shell
    # .clangd
    Diagnostics:
      Suppress: ['-Wvisibility']
    ```

  - 比較，兩種配置文件可以同時使用，也可以獨立使用
    - compile_commands.json 有套件可以幫忙產生，.clangd 目前還沒有
    - compile_commands.json 配置較繁雜，但是可控制的粒度較細，
    - .clangd 偏向通用配置，可控制的粒度較粗，通常用於簡化設置
    - 兩者同時存在時，.clangd 可以做為 compile_commands.json 的補充或優化，兩者配置的選項會合併

- 注意，`compile_commands.json |.clangd` 配置文件和 Makefile 有什麼區別
  - Makefile 中定義的`實際的編譯命令`，compile_commands.json 的內容`只會影響 ide 的代碼分析結果是否正確`
  - compile_commands.json 不會影響 Makefile 編譯命令的執行 (<font color=blue>修改 compile_commands.json 也不會影響實際執行的編譯命令</font>)，
    但是 Makefile 的內容會影響compile_commands.json 生成的內容
    - 例如，若 Makefile 中指定-O2、-Wall，這些選項也會出現在 compile_commands.json 中的 command 欄位
    - 例如，若 Makefile 中定義頭文件搜索路徑，這些路徑也會被記錄到 compile_commands.json 中的 command 欄位中

## 安裝 cland @ alpine

- 以 alpine 為例

  ```shell
  apk update
  apk add clang clang-extra-tools

  # 確認 clangd 是否存在
  which clangd    # clangd = /usr/bin/clangd 是軟連結，指向 /usr/lib/llvm17/bin/clangd*
  ```

- ./config/helix/language.toml 的配置

  ```toml
  [language-server]

  clangd = { command = "clangd" }

  [[language]]
  name = "c"
  scope = "source.c"
  injection-regex = "c"
  file-types = ["c"] # TODO: ["h"]
  comment-token = "//"
  block-comment-tokens = { start = "/*", end = "*/" }
  language-servers = [ "clangd" ]
  indent = { tab-width = 2, unit = "  " }

  [[grammar]]
  name = "c"
  source = { git = "https://github.com/tree-sitter/tree-sitter-c", rev = "7175a6dd5fc1cee660dce6fe23f6043d75af424a" }
  ```

- 建構需要的檔案，
  - `$ hx -g fetch` 
  - `$ hx -g build`

## compile_commands.json 配置文件的使用

- 透過 bear 套件，可以自動產生 compile_commands.json

  - 安裝bear並產生 compile_commands.json 
    
    ```shell
    apk add bear  # 安裝
    bear -- make  # 先產生 compile_commands.json，再執行 make 命令

- 配置選項
  - directory : 指定編譯命令執行時的工作目錄
  - command : 完整的編譯命令，包括編譯器、所有標誌和選項，提供給 IDE 或代碼分析工具，以正確解析代碼
  - arguments : array 版的 command，以 array 取代字串形式的 command
  - file : 指定要編譯的源文件的路徑，部分IDE會優先使用此欄位來建立索引，推薦使用絕對路徑來減少錯誤
  - output : 指定編譯產生的輸出文件

  - 注意，command 和 file 都可以指定要編譯的源文件，`兩者應保持一致`，`且兩者應該同時存在`
    - 沒有在 command 指定要編譯的源文件，編譯器無法進行編譯
    - 沒有在 file 指定要編譯的源文件，ide 可能無法正確建立索引 (解析command欄位的ide不受影響)

## .clangd 配置文件的使用

- 基本概念
  - 預設會自動索引`專案內的頭文件`
  - 在`專案目錄外的頭文件`，才需要在CompileFlags指定目錄位置，但有以下幾種例外，
    - 狀況，若專案內的頭文件不在 include 目錄中時，需要在 CompileFlags 中指定實際路徑
    - 狀況，想減少不必要的搜索時間時，可以指定精確的頭文件路徑
    - 狀況，明確指定路徑可以避免潛在的問題

- 配置選項
  - CompileFlags : 設置編譯標誌和額外的頭文件搜尋路徑 | 控制編譯器的行為
    - 執行編譯才會使用此區的選項
    - `Add` : 添加編譯器的編譯標誌
    - `Remove` : 移除編譯器的編譯標誌
    - `Compiler` : 告訴 clangd 如何解釋編譯命令
    - `CompilationDatabase` : 

  - Index : 
    - 功能
      - 用於代碼補全、跳轉到定義、查找引用、符號搜尋、重命令、代碼分析、 ... 等
      - 用於更精細地控制索引行為
      - 如果沒有特別配置，clangd 會使用`默認的索引策略`
    - `ThreadCount` : 指定後臺任務的線程數
    - `Strategy` : 指定索引策略，可用值 heuristic | full 
    - `Background` : 是否在背景執行索引，可用值 Build | Skip | Auto
    - `StandardLibrary` : 調整標準庫的索引行為，可用值 Yes | No
    - `Exclude` : 用於排除自動生成的代碼或測試文件
    - `ExternalSource` : 控制外部源碼的索引行為
      - FromSource : 是否從源代碼索引外部依賴，可用值 Always | Never | Auto
      - SkipExternalSymbols : 是否跳過外部符號的索引，可用值 true | false
      - IncludePaths : 需要索引的外部包含路徑
      - ExcludePaths : 不需要進行外部索引的路徑
      - MaxDepth : 限制外部索引的深度，控制遞歸索引的層級
    - `FilePaths` : 用於索引指定路徑中的文件

  - Diagnostics : 控制ide中的診斷信息的顯示和行為 | 設置警告和錯誤的級別
    - `Suppress` : 抑制特定的診斷
    - `UnusedIncludes` : 是否檢查未使用的 include，可用值 Strict | None
    - `SuppressAfterInclusions` : 是否抑制在 include 指令之後的所有診斷，，可用值 true | false
    - `Warnings` : 將特定診斷設置為警告
    - `Errors` : 將特定診斷視為錯誤
    - `MissingIncludes` : 是否檢查缺失include，可用值 Strict | None
    - `SizeLimit` : 限制單個文件的最大的診斷數量
  
  - InlayHints : 啟用或控制 Clangd 顯示內聯提示（Inlay Hints）功能
    - `ParameterNames` : 是否顯示函數調用中的參數名稱，例如，foo(10, 20) 顯示為 foo(x: 10, y: 20)
    - `DeducedTypes` : 是否顯示類型推導的結果，例如，auto x = 10; 顯示為 auto x: int = 10;
    - `Designators` : 是否顯示結構體或聯合體的成員初始化名稱，例如，Point{1, 2} 顯示為 Point{x: 1, y: 2}

- 範例

  ```yaml
  CompileFlags:
    Add: [
      -std=c++17, -Wall,
      # 添加頭文件的搜尋目錄，添加順序也是搜尋順序
      -I/usr/include", "-I${workspaceFolder}/include",
      -Wall, -Wextra, -Werror
    ]
    Remove: [-W*] # 刪除所有與警告相關的標誌
    CompilationDatabase: build/

  Index:
    Strategy: heuristic
    FilePaths: ["/usr/src/linux-6.6.48"]
    Background: Build
    StandardLibrary: Yes
    Exclude: 
      - path/to/generated/*
      - test/*
    ExternalSource:
      FromSource: Auto
      SkipExternalSymbols: false
      IncludePaths:
        - /usr/include/boost
        - /opt/local/include
      ExcludePaths:
        - /usr/include/deprecated
      MaxDepth: 5

  Diagnostics:
    Suppress: ['-Wvisibility', unused-variable', 'unused-parameter']
    UnusedIncludes: Strict
    MissingIncludes: Strict
    SuppressAfterInclusions: false
    SizeLimit: 100
  ```
