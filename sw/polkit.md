## polkit 的使用

- 使用場景
  - 和 sudo 一樣都是管理權限的套件，但 polkit 沒有賦予進程完全的 root 權限，
    而是通過一個集中的策略系統進行更精細的授權

  - polkit 是一個`應用程式級別的工具集`，通過定義和審核權限規則，
    實現`不同優先級進程間的通訊`：控制決策集中在統一的框架之中，決定低優先級進程是否有權訪問高優先級進程。
  
  - Polkit 在系統層級進行權限控制，提供了一個低優先級進程和高優先級進程進行通訊的系統
  
  - 範例，Polkit 定義出一系列運行 GParted 操作，將用戶按照群組或用戶名進行劃分
    
    例如 wheel 群組用戶。然後定義每個操作是否可以由某些用戶執行，
    
    執行操作前是否需要一些額外的確認，例如通過輸入密碼確認用戶是不是屬於某個群組

- 比較，polkit 和 sudo 的差異
  - polkit 是一個會話管理器，專門用於服務和驅動程式，允許指定用戶使用某些權限而不需要輸入 root has 密碼，
    適合用於`不會更改系統配置`的任務
  - Sudo 是一個需要用戶登錄時輸入 root 密碼的工具。它專門用於用戶的操作，可以用於更改系統配置

- 範例，輸出請求訪問的詳細訊息
  ```shell
  # /etc/polkit-1/rules.d/00-log-access.rules
  polkit.addRule(function(action, subject) {
      polkit.log("action=" + action);
      polkit.log("subject=" + subject);
  });
  ```

- 範例，允許 admin 群組中的使用者執行 GParted 而不需要權限
  ```shell
  # /etc/polkit-1/rules.d/00-early-checks.rules
  
  polkit.addRule(function(action, subject) {
      if (action.id == "org.archlinux.pkexec.gparted" &&
          subject.isInGroup("admin")) {
          return polkit.Result.YES;
      }
  });
  ```

## ref

- [polkit @ arch wiki](https://wiki.archlinuxcn.org/zh-tw/Polkit)
- 