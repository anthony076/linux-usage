
## 帳號相關

- [概念] 建立帳戶時，可選擇是否建立 home目錄
  - 若有home目錄，就可透過 ~ 存取的家用目錄
  - 類似 windows 的 User目錄，可儲存該用戶的安裝的軟體和各種配置，並與其他使用者區隔開來
  
- 列出所有帳號，`$ less /etc/passwd`

- 添加使用者 by useradd
  - step1，建立使用者，`$ sudo useradd -m -u 自定義使用者ID 帳號名`
    - m: 建立家目錄，M: 不建立家目錄
    - u: 定義使用者ID
    - g: 指定群組
    - s: 指定預設的shell，推薦使用 /bin/bash，若不添加，`預設使用 sh`，沒有 autocomplete 的功能
    - d: 預設家目錄路徑，若不設置，預設為 /home/帳號
    - c: 設置真實名稱
    
  - step2，設置密碼，必要，`$ sudo passwd 帳號名`，
    > 注意，新建立的帳號要設置密碼才會啟用，否則會處於停用狀態

  - step3，將使用者添加到 sudoers 名單中，並配置 sudo 命令的權限，
    - 注意，新增的使用者必須添加到 sudoers 名單中，才能使用 sudo 執行命令
    - 參考，[sudo的使用](#sudo-相關)

- 添加使用者 by adduser
  - 與useradd命令的差異
    - adduser 是一個更高級別、更易用的工具，它封裝了 useradd 等底層命令的複雜性，提供了一套更加友好的接口
    - 自動創建與新用戶同名的用戶組
    - 自動為新用戶創建一個同名的主目錄
    - 根據系統配置自動選擇合適的 shell
    - 提供了一個交互式界面來設置新用戶的密碼

  - 常見範例
    ```shell
    # 建立普通用戶 + 建立同名用戶組 + 同名主目錄
    adduser 用戶名
    
    # 建立系統用戶，不會為其建立主目錄，也不會要求設置密碼
    sudo adduser -S 用戶名

    # 手動指定主目錄
    adduser -h /path/to/custom/home 用戶名

    # 手動指定shell
    adduser -s usr/bin/fish 用戶名

    # 手動指定添加的群組
    sudo adduser 用戶名 -G 群組名1,群組名2

    # 綜合使用
    adduser -S -s /usr/bin/fish -G wheel aa
    ```

- 變更群組， `$ usermod 帳號名 -G 群組名`

- 變更預設shell，`$ sudo usermod -s /bin/bash 帳號名`

- 變更帳號切換後的預設路徑，修改該使用者的 `~/.bashrc`

  例如，使用 `$ su ching` 後，預設目錄為 /home/ching ，
  
  將 `cd /home/ching` 添加到 `~/.bashrc` 的尾部

- 切換使用者(switch user)，`$ su 帳號名` 或 `$ su - 帳號名`
  - 注意，若不指定帳戶名，預設會進入 root 帳號

  - 使用`su - 帳號名`更好

    對於不同的使用者會有不同的環境變數和配置內容，`su - 用戶名`才會重置環境變量、工作目錄和 shell 設定為目標用戶的設定，
    使用`su - 用戶名`可以減少應用執行時出錯的問題

- 變更密碼的兩種方式
  - 方法1，互動式更改密碼，例如，`$ passwd 帳號`
  - 方法2，非互動式更改密碼，格視為"帳號:密碼"，例如，`$ echo "aa:aa" | chpasswd`
  - 方法3，利用 echo -e 和 | ，參考，[利用 echo 和 | 實現互動命令的自動化安裝](#利用-echo--e-和--實現互動命令的自動化安裝)

- 禁用帳號，`$ passwd -l 帳號名`，
- 解除禁用帳號，`$ doas passwd -u 帳號名`，

- 刪除帳號，`$ userdel -r 帳號`
  - r: 刪除使用者登入目錄以及目錄中所有檔案
  - 注意，此步驟不會刪除 sudoer.d 目錄中的配置文件

- 取得指定帳號的 passwd 內容，`$ getent passwd 帳號`

- 查詢所有用戶，`$ cat /etc/passwd`
  - 以 daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin 為例
    - daemon 用戶名
    - x 加密後的用戶組密碼，透過 `$ sudo grep user1 /etc/shadow`，可顯示密碼加密後的HASH值
    - 1 用戶ID
    - 1 群組ID
    - daemon 用戶真實名稱
    - /usr/sbin 用戶的主目錄
    - /usr/sbin/nologin 用戶登陸時執行的 shell

- 查詢群組訊息 
  - 方法，`$ getent group`
  - 方法，`$ cat /etc/group`
    - 以 adm:x:4:syslog,ching 為例
    - adm 群組名
    - x 無實際意義，只是為了和用戶訊息的欄位一致
    - 4 群組ID
    - syslog,ching 群組成員

## sudo 相關

- 指定特定使用者執行命令，`sudo -u 帳號 命令`

- 將使用者添加到 sudoers，並修改使用者執行sudo命令的權限
  - step1，添加到 sudoers
    - 語法: `$ sudo visudo -f /etc/sudoers.d/ttt`
    - 注意，只能透過 sudo 套件內建的 visudo 命令進行修改

  - step2，修改權限 
    - 語法: `使用者帳號 登入者來源主機=(可使用帳號1:可使用群組1) 可執行的指令1, 可執行的指令2 (可使用帳號1:可使用群組1) 可執行的指令1, 可執行的指令2`
      > (可使用帳號1:可使用群組1): 代表使用者帳號可執行 `sudo -u 帳號1 命令`，以帳號1的身分執行命令，
      透過 `sudo -u 帳號1` 可借用帳號1的所有權限來執行命令，就不受到可執行指令的限制 

      > 可執行的指令1: 代表使用者帳號可執行的命令

    - 範例: `ttt ALL=(ALL) ALL`
      - ALL=，代表任何主機都可登入 ttt 帳號
      - (ALL)，代表 ttt 可透過 `sudo -u 任意帳號` 執行命令
      - ALL，ttt 可執行任何指令

    - 範例: `ttt ALL=(user) /bin/ls`
      - ALL=，代表任何主機都可登入 ttt 帳號
      - (user)，代表 ttt 可透過 `sudo -u user` 執行命令
      - /bin/ls，ttt 只能執行 /bin/ls 的命令

- 設置免密碼執行 sudo 命令，以 root 為例

  - 注意，sudo 代表使用者透過 root 的身分執行命令，
  
    要確認當前使用者可以使用 root 的權限，免密碼執行 sudo 命令才會有效

    例如，`tt ALL=(aa) NOPASSWD:ALL` 是無效的，
    
    tt 本身只能使用 aa 的權限，並不具有 root 的權限，
    因此，tt 本來就無法執行 sudo 的命令，即使改用免密碼也是無效
    
  - 注意，添加使用者`要注意添加位置`，避免權限被覆蓋
    
    例如，以下範例中，第1個 user 會被第2個 user 覆蓋
    ```shell
    user ALL=(ALL) NOPASSWD:ALL
    user ALL=(ALL) ALL
    ```
    
    推薦將 NOPASSWD 添加到最後，避免權限被覆蓋

  - step1，`$ sudo nano -f /etc/sudoers.d/使用者名`
  
  - step2，將可執行指令的部分改為 NOPASSWD:ALL
    
    例如，`使用者名 ALL=(ALL) NOPASSWD:ALL`

## doas

- why doas
  - 更輕量
  - 減少配置複雜性，以更簡單的方式提供超級用戶權限的配置
  - 以更安全的方式來管理超級用戶權限

    例如，doas 默認情況下只允許以 root 身份運行那些明確指定為可用的程序，有助於防止未經授權的程序訪問敏感資源。

- 獨立配置文件，`/etc/doas.conf`

- 常見範例，

  需要先建立使用者，例如，`$ adduser -S -s /usr/bin/fish -G wheel 用戶名`，再透過`$ passwd 用戶名`設置密碼

  ```shell
  # /etc/doas.conf

  # 範例，讓特定用戶預設以root身分執行命令
  permit persist 用戶名 as root

  # 範例，允許 wheel 組的成員在同一會話中連續執行多個需要超級用戶權限的命令，而無需每次都輸入密碼
  permit persist :wheel

  # 設置完成後，需要重新登入才能生效
  ```
