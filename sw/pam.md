## PAM 的使用

- PAM，Pluggable Authentication Modules，可插入式認證模組

  是一種驗證身分(authentication)和授權(authorization)的中介應用程式，
  將早期 Unix-like 系統中，每個程式自己的驗證方式集中起來管理，
  
  - 為多個程式提供單一的驗證機制
  - 提供管理員和程式開開者單一彈性的管理方式

- 圖解 PAM
  ```mermaid
  flowchart TB
  sshd --> PAM
  login --> PAM
  popper --> PAM
  others --> PAM

  PAM --> passwd
  PAM --> LDAP
  PAM --> NIS
  ```

- pam 的特性
  - 可動態載入各種驗證庫
  - 安全機制集中管理
  - 配置檔在載入時才有效

- pam 的相關檔案
  - 動態函式庫的位置，`/lib/security/`
  - 配置檔的位置，`/etc/pam.d/`
  - log檔的位置，`/var/log/security/` 或 `/var/log/message`

## 配置檔的使用
- 將配置檔放置在 `/etc/pam.d/` 的目錄下，以 login 為例

- 格式，`<模組類型>  <控制旗標>  <模組路經>  <其他參數>`，例如，
  - 每個動態連結庫(*.so) 就是一個模組

  ```shell
  #%PAM-1.0

  auth       required     pam_securetty.so
  auth       requisite    pam_nologin.so
  auth       include      system-local-login
  account    include      system-local-login
  session    include      system-local-login
  password   include      system-local-login
  ```
  
- 參數，`<模組類型>`，module-type
  - auth:表示驗證類模組，依據使用者密碼或特殊方式來做驗證機制
  - account:表示帳戶類模組，限制或禁止使用者的存取機制
  - password:表示密碼類模組，讓使用者去更改,設定,驗證密碼
  - session:表示類模組，執行環境的設定

- 參數，`<控制旗標>`，control-flag
  - `required`:
    - 所有的 required libraries 回傳值要為 pass，最後的結果才會為 pass ，
    - required libraries 的結果並不會影響到下一個 libraries 是否執行
      > PAM 並不立刻將錯誤消息返回給應用程式，而是在所有模組都作完畢後才將錯誤消息返回它的程式

  - `requisite`:
    - 和 required 類似，Requisite 標記的模組返回 pass，用戶才算通過身分驗證
    - 差異，一旦失敗就不再執行堆中後面的其他模組，且終止整個身分驗證程序，並回傳驗證失敗

  - `sufficient`:
    - 若 sufficient 標記的 library 回傳值為 pass，
      PAM立刻向應用程式返回成功，不必嘗試接下來的其他模組

    - 若 sufficient 標記的 library 回傳值不是 pass，才會往下繼續執行其他驗證模組，
      此時 模組會被標器為 optional，而不是被判定為 fail

  - `optional`: 帶有該標記的模組失敗後將繼續處理下一模組
  
  - `include`: 載入其他的配置檔，例如，
    ```
    # 載入 /etc/pam.d/common-auth
    auth include common-auth
    ```

- 參數，`<模組路經>`，module-path

- 參數，`<其他參數>`，arguments，調用該模組需要提供的參數

- 範例，以 ubuntu 的 `/etc/pam.d/login` 為例
  ```shell
  #%PAM-1.0
  
  # 參考 /etc/securetty 檔案內容紀錄 root 可以經由哪些 terminal(ttys) 來登入
  auth       required pam_securetty.so

  # 讓系統轉調用 system-auth
  # 好處是可以讓多個設定檔共用他們相同的認證，不須為每個設定寫下相同的步驟
  auth       required pam_stack.so service=system-auth

  # 限制一般使用者的登入，會參考 /etc/nologin 決定一般使用者的登入限制
  auth       required pam_nologin.so

  account    required pam_stack.so service=system-auth
  password   required pam_stack.so service=system-auth
  session    required pam_stack.so service=system-auth

  # 允許非特權使用者使用系統控制指令
  session    optional pam_console.so
  ```
