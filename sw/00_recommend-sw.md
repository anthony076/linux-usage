## 推薦工具

- [GUI相關套件](../usage/desktop/desktop-wm-sw.md)

## 好用工具(不分類)

- rsync: 遠端檔案同步和備份工具

- xdotool: linux 版的按鍵精靈

- [expect](https://www.youtube.com/watch?v=gschjpbyxto): command/bash interaction 自動化，
  根據 command/bash 的輸出，進行自動輸入

- [Wudao-dict](https://github.com/ChestnutHeng/Wudao-dict): 命令行下的英文辭典

- cronie: 執行定期任務

- [wine/bottles](https://www.youtube.com/watch?v=VqDgrHCPWG8): 在 linux 上執行 windows 的應用

- mlocate: 透過索引更很找到文件，而不需要遍歷整個文件系統
  - 安裝套件 > sudo updatedb > locate 檔名

- Parcellite: 輕量，小巧且免費的剪貼板管理器，佔用內存小

- hyperfine: 測試命令執行的時間，可指定執行次數，並取平均值作為結果

- mproc: 啟動並追蹤管理(monitor)多個程式

- just: 類似 make，可以自定義命令以簡化CMD命令的輸入，可用於專案或全局

- entr: 監控檔案，當檔案有變化時，自動執行命令

- powertop: shell 版監控電池消耗的小工具，不占用資源

- doas: 用於取代sudo的套件，更輕量，使用更方便

- [pet](https://github.com/knqyf263/pet): CLI Snippet Manager，建立自定義的命令模板，可快速搜尋|執行命令|快速透過模板輸入完整命令

- clipboard: clipboard manager，管理 clipboard，可以透過命令查看或對 clipboard 進行操作，透過 cb 執行

- cmatrix: 顯示駭客帝國的文字簾幕

- [starship](https://starship.rs/) : 客製化 shell 樣式，輕巧快速

- bc : 用於任意精度計算的命令行計算器語言。它支持基本的算術運算、變量、條件語句、循環以及自定義函數

# 常見工具包

- procps : 提供查看和管理系統進程的工具包，包含 ps、top、free、kill 等

- shdow : 提供管理用戶密碼和認證的工具包，包含 passwd、useradd、usermod 等

## 下載工具 

- curl 
- Httpie : 取代 curl，更直觀易用，對返回的結果自動做了高亮和格式化

- wget
- axel: 多線程下載工具

## dev 工具

- devbox
  - 功能
    - 簡易版的nix，讓nix像python-uv或npm般使用
    - 可定義命令，所有的套件都在專案目錄中，不需要時可直接刪除
    - 在任何地方(本地或雲端)都可以建立隔離|可複製的開發環境，不需要安裝 docker
  - [官網](https://www.jetify.com/devbox)
  - [介紹](https://www.youtube.com/watch?v=WiFLtcBvGMU)

- [mkcert](https://github.com/FiloSottile/mkcert) : 建立本地端的https憑證，用於本地開發網路應用且需要https時使用

- [hexyl](https://github.com/sharkdp/hexyl) : terminal 版 hex viewer

- mosh : 用於取代 openssh 的 ssh 命令

- m4 : 用於生成配置文件或代碼的命令行工具

## 加密工具

- gnupg : 用於加密和簽名數據

## 字符工具

- figlet : 將普通文本轉換為由字符組成的大型藝術字，例如，figlet hello

- lolcat : 用以產生彩虹效果的輸出，例如，echo hello | lolcat

## 磁碟工具

- du: 內建工具，常用於檢視(目錄或檔案)的大小
- dust: 好用版的 du，可以查看磁碟占用的情況
- duf: 好讀版的 df

## 系統工具

- pciutils: lspci
- usbutils: lsusb

## 內核調試工具類

- strace: 追蹤程序或進程執行時的系統調用和所接收的信號。可打印所調用的系統調用的名稱、參數和返回值
- ltrace: 庫文件調用追蹤器
- ptrace: 進程追蹤器
- sysrq: 可透過組合鍵搜集系統內存使用、CPU任務處理、進程運行狀態等系統運行信息。
- lsof: list open files
- ftrace: 包含一系列跟蹤器，可跟蹤內核函數調用、跟蹤上下文切換、
  查看中斷被關閉的時長、跟蹤內核中的延遲以及性能問題等。

## ide

- neovim

- 簡化 neovim 的配置 (已訂製)
  - lazyvim插件: 簡化 neovim 的配置
  - lunarvim: 簡化 neovim 的配置
  - [jeezyVim](https://github.com/LGUG2Z/JeezyVim) : A fast, reliable NeoVim configuration

- helix: :內建 lsp、語法高亮、命令補全，開箱即用

## jsonViewer

- jq : Command-line JSON processor
- fx : Terminal JSON viewer & processor
- yq-go: lightweight and portable tool for YAML, JSON, XML, CSV, TOML

## dotfile: 配置檔管理

- yadm: 優雅管理你的應用配置和數據
- chenzmoi: 可跨機器同步dotfile

## git 相關

- tig: 推薦，文字版的 git-graph
- gitui: 推薦，比lazygit更好用的 git 工具，檔案小、速度快、使用 rust 編寫
- lazygit: git 命令簡化版
- onefetch: 顯示 repo 相關訊息的 cli tool

## Shell

- ble.sh: bash 增加工具，提供類似 fish 的功能
- fish: 推薦，類似 oh-my-zsh，開箱即用，不需要配置
- zsh
- nushell: 為結構化數據而生的 shell + 可利用管道簡化命令
- kitty: 
  - GUI版的terminal，支援GPU加速，配置簡單，可透過命令(kitten)進行快速配置
  - 支援 terminal-graphical-protocol，可直接在 terminal 中檢視圖片，不需要額外添加套件
  - 可在 terminal 中透過滑鼠點擊直接開啟檔案，或透過 `ctrl-shift-p y` 透過按鍵快速選擇檔案
  - 透過 `ctrl-shift-t` 建立類似 tmux 的分割視窗，並且可在多個視窗同步輸入
  - 詳見，[kitty features](https://sw.kovidgoyal.net/kitty/)

## Terminal-App (tui，text-based-interface) 

- [tty-clock](https://github.com/xorg62/tty-clock/tree/master):在 terminal 顯示時間
- calcurs: 在 terminal 顯示日曆、鬧鐘、todo-list
- tmux: 多視窗管理器，實現終端復用的工具 (分割 terminal 視窗)
- [zellij](https://www.bilibili.com/video/BV1NL411A77c): 比 tmux 更容易上手的終端復用工具
- [scriptreplay](https://wangchujiang.com/linux-command/c/scriptreplay.html): 錄製終端操作
- feh: 在gui-終端下，可用於瀏覽圖片的工具，一般終端不可用

- 終端版的網頁瀏覽器
  - links: 更接近瀏覽器的terminal版瀏覽器，提供工具列
  - nyxt
  - w3m: (browser + img-preview)
    - [配置參考](https://www.youtube.com/watch?v=R2bMUtCOGko)

## 檔案管理相關

- ranger: terminal 版的檔案管理，用py編寫，支援終端圖像預覽
- [lf和lf-img](lf.md): terminal 版的檔案管理 (lf==list files)
- mc: midnight-commander，terminal版的檔案管理器，有操作提示
- [nnn](https://www.bilibili.com/video/BV1Tz4y1y72r): 輕量的檔案總管，無操作提示
- fff: 輕量的檔案總管
- vifm: vi鍵位控制的檔案瀏覽工具，不限於在 vim 中使用，支援 image-preview
  - [設置參考](https://www.bilibili.com/video/BV1TB4y1T7xk/?vd_source=3c0cf410b123e705f0c291052893ff16)
- sz/rz: 透過 gui 選擇檔案並進行檔案傳輸
- pcmanfm: gui 版檔案總管，輕巧快速
- nautilus: gui 版檔案總管
- yazi: terminal-filemanager by rust:
- broot : 具有 rg 功能的terminal版檔案管理

## 進程相關

- procps: 好用的進程相關工具包，包含以下套件
  - pgrep: 透過命令名查詢pid 
  - pkill: 透過命令名刪除進程
  - top: 實時更新正在執行的進程
  - free: 查詢記憶體使用量
  
## 系統資源監控: 

- btop: 比 top|htop 更好用的系統資源監測工具，畫面更漂亮，terminal-app
- iotop :　監控硬碟I/O狀態
- iftop:網路流量監控工，即時資料流量、連接埠串連資訊、反向解析IP 等
- bmon: 在 terminal 監控有線網路流量
- bottom(btm) : 跨平台的terminal應用，可自訂顯示項目的系統監視器
- nmon:豪華版的 htop
- lm_sensors:監控溫度、電壓和風扇的工具和驅動程序
- sysstat:包含監測系統性能及效率的一組工具
  - 提供 iostat、mpstat、pidstat、sar、sadc、sa1、sa2、sadf、sysstat、nfsiostat、cifsiostat
  - CPU 使用率、
  - 硬盤
  - 網絡吞吐數據
  
## 模糊查找

- [fzf](https://www.bilibili.com/video/BV1bJ411s74r): 全域模糊檔案查找，或命令查找

## 檔案預覽

- bat: 高階版的 cat，具有語法高亮，顯示行號的功能
- pistal: 具有語法高亮的檔案預覽

## VM 相關

- Packer: 將雲服務或vm環境打包為image，例如，image-for-docker、box-for-vagrant、ami-for-aws

## 網路相關

- traceroute: 追蹤 netctl-route
- nmap: 用於掃描網絡以識別活動主機、開放端口以及運行的服務
- wavemon: wifi-monitor
- mtr:跨平台、簡單易用的命令列網路檢測工具，結合了 ping 與 traceroute ，並提供了更詳細的資訊
- ethtool:顯示或修改以太網卡的配置信息
- iperf3:測量 IP 網路上可達到的最大帶寬的工具
- socat:netcat的替代品，支持TCP、UDP、SOCKET，並能夠在不同的網路連接之間進行數據轉發
- ipcalc:一個 IPv4 和 IPv6 地址計算器

## DNS 工具

- bind-tools
- dnsutils:包含 dig 和 nslookup 命令
  - dig:提供詳細的DNS查詢結果和靈活的查詢選項
  - nslookup:適合快速查詢 DNS 記錄
- ldns:替代dig的工具，專注於簡化 DNS 查詢過程，並提高查詢的性能和安全性

## cheatsheet

- cheat.sh: 可免安裝
- cheat
- tldr: 常用套件的快速使用範例，用於快速查找命令的使用範例，避免看又臭又長的man說明，
  需要透過 npm 或 python3 安裝
- navi: 互動式cheatsheet工具，用於command-line，powered by rust
- beavr: A command-line autocompleter with steroids

## 閱讀源碼

- 推薦，[universal-ctags](https://www.youtube.com/watch?v=4f3AENLrdYo)
- [SourceNavigator](https://wiki.tcl-lang.org/page/Source+Navigator): 類似 SourceInsight，需要gui
- [Sourcetrail](https://github.com/CoatiSoftware/Sourcetrail): 開源免費的源碼閱讀工具，需要gui

## 替代

- autojump: 用於取代 cd
- zoxide: 更好用的 cd，不需要輸入完整路徑就可以直接跳轉目錄

- sd : 用於取代sed，使用更簡單的搜尋語法，以取代re表達式進行搜尋

- ripgrep: grep 替代
  - 可搜尋文件內容
  - 更高搜尋速度的 grep，
  - 默認忽略搜尋不必要的二進制文件、彩色輸出、跨平台

- skim: grep 替代品 (by rust)

- [fd](https://github.com/sharkdp/fd): 
  find 的替代品，更簡單、快速、使用者友好，使用顏色來突出不同的文件類型

- eza: 進階版的 ls (alias ls='eza --long --tree')
- atuin: 更好的 history，可生成shell 歷史記錄使用活躍圖，可在不同機器之間同步history，可同步不同機器上的 shell-history

## 軟體源沒有目標軟體時

- Ubuntu Packages Search
  - 當發生 `Package 軟體名 has no installation candidate` 的時候使用

    代表該軟體未出現在當前的軟體源列表中，需要透過 `UbuntuPackages-Search` 查詢可用的軟體源

  - 進入 [UbuntuPackages-Search](https://packages.ubuntu.com/)的網頁
    
    - 查看 Linux Distribution 的版本，`$ lsb_release -a`
    - Keyword欄位，輸入軟體名
    - Distribution欄位，輸入當前的linux版本

## ref

- [7 Amazing CLI Tools You Need To Try](https://www.youtube.com/watch?v=mmqDYw9C30I)
