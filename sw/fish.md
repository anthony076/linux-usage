## fish 的使用

- 配置檔位置，`~/.config/fish/config.fish`

- 語法，添加到環境變數到配置檔中
  ```shell
  set PATH <路徑> $PATH
  ```

- 配置，取消歡迎訊息
  ```shell
  # ~/.config/fish/config.fish
  set fish_greeting ''
  ```

- 工具，fisher: fish 的插件管理器
  - 安裝，
    - on Arch，`$ sudo pacman -S fisher`
    - on Alpine，`$ curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher`

  - 查看已安裝的插件，`$ fisher list`

- 推薦插件
  - 安裝 tida 主題，`$ fisher install IlanCosman/tide@v5`

  - 安裝 autojump，`$ fisher install jethrokuan/z`
  
  - 安裝 sponge，避免失敗或打錯的指令添加到歷史清單中，
    `$ fisher install andreiborisov/sponge`
  
  - 安裝 autopair，括號自動補全，
    `$ fisher install jorgebucaran/autopair.fish`
  
  - 安裝 fzf.fish (fcsi，Fuzzy Completion Shell Integration)，在 fish 中增強fzf的功能
    - 功能:
      - 更好的顯示
      - Search Git Log、
      - Search Git Status、
      - Search Processes、
      - Search Variables

    - 安裝依賴套件，`$ sudo pacman -S fish fzf fd bat`
    - 安裝依賴套件，`$ sudo pacman -S fd`
    - 安裝 fzf.fish 插件，`$ fisher install PatrickF1/fzf.fish`

## 使用範例

- 範例，臨時需要 fish-shell 執行特定命令

  - 使用場景 : 
    
    例如，僅在 fish-shell 中設置 lg = lazggit 的命令別名，
    lg 僅在 fish-shell 中有效，且預設的shell不是fish時

  - 狀況，不需要加載 config.fish ，`$ fish -c 'echo 123'`
  - 狀況，需要加載 config.fish ，`$ fish -i -c 'lg --help'`