## just - command runner (replacement for makefile)

- 基本概念
  - 在 recipe 中，每一行都是獨立的命令由獨立的 shell 執行
  - 在配置文件.justfile檔案中定義自定義的命令後，透過`just 配方名`執行

- 環境設置
  - 建立 .justfile，`$ just --init`
  - 設置環境變數後可全局使用 just 命令，設置後需要須重啟shell
    - windows，`$ setx JUST_JUSTFILE C:\path\to\.justfile`
    - linux，`$ export JUST_JUSTFILE /path/to/.justfile`  

- 注意事項
  - <font color=blue>\`命令`</font> 或 <font color=blue>\```命令```</font> 專門用於指定命令，會立刻解析該命令是否存在
  - 雙引號 " 預設會轉譯為 \"，可使用 {{quote{"hello world"}}} 自動添加單引號

# 配置文件 .justfile 的使用

- 指定用於執行命令的 shell，`set shell := ["cmd.exe", "/c"]`
- 關閉執行命令前打印命令內容，`set quiet`

- 設置區域變數，
  - 區域變數僅在 justfile 內可用
  - 例如，`JUST_FILE := env_var('JUST_JUSTFILE')`
  - 透過`{{區域變數名}}`存取

- 設置just執行配方時的環境變數，
  - 注意，是just執行配方時的環境變數，`不是用戶日常使用的環境變數`
  - `set export`，啟用環境變數功能，啟用後，export 語句才有效
  - 設置環境變數，`export HELLO := "world"`，需要加上雙引號，否則會辨識別為變數
  - 環境變數對配方中的每一個命令都有效

- 為配方設置別名，`alias he := hello`
  - 透過`$ just he`可以執行 hello 配方
  - 別名也會自動顯示在 just --list 的結果中

- `建立一般配方`
  
  ```
  配方名 [參數]: [依賴配方名]
    命令1        執行前打印命令
    @命令2       執行前不打印命令
    -命令        忽略命令錯誤
  ```

  - 冒號需要緊跟配方名，
  - 此方式建立的每一行命令，都是獨立執行的
  - 透過 $0 $1 .... 可以獲取位置參數

  - @ 不會打印該命令，直接執行，預設會先打印命令後才執行
  - 若@寫在配方名前方，則該配方下的所有命令在執行前都不會被打印出來 

  - 寫在配方名上方，利用 # 添加的註解，會被顯示在 just --list 中
  - 或利用 [doc(註解)] 建立註解，就會忽略 # 的內容

- `建立共用 context 的配方`
  
  ```
  配方名: [依賴配方名]
    #!/bin/sh
     命令1
     命令2
  ```

  - 此方式建立的每一行命令，都會寫進同一個shell-script 後，再執行

- `為配方建立群組`

  ```
  [group('群組名')]
  配方名:
     命令
  ```

  - 群組名只是用於標記和區分命令，沒有實際作用
  - 執行方法和一般配方沒有區別
  - 每一個 `[group('群組名')]` 只能為一個配方建立標記

- `建立私密配方`

  ```
  [private]
  配方名:
     命令
  ```
  
  - 該配方不會出現在 just --list 命令的結果中

- `為命令添加註解`
  
  ```
  方法1
  [doc("echo hello")]
  hi:
    echo hello
  
  # ==============
  方法2

  # echo hello
  hi:
    echo hello
  ```

  - 透過以下兩種方式都可以添加註解，並顯示在 just --list 的結果中
    - 方法，透過 `[doc(註解)]`
    - 方法，在配方上面添加`# 註解內容`


## just-cli 的使用
- 建立 justfile，$ just --init
- 執行多個配方，$ just 配方名1 配方名2，對於有參數的配方，可能造成非預期結果

## ref

- [介紹](https://www.youtube.com/watch?v=wQCV0QgIbuk)
- [官網](https://github.com/casey/just)
- [文檔](https://just.systems/man/zh/chapter_29.html)
- [cheatsheet](https://cheatography.com/linux-china/cheat-sheets/justfile/)
