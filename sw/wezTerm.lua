
-- ~/.wezterm.lua

-- 文檔
-- 簡易配置，https://wezfurlong.org/wezterm/config/files.html
-- 完整配置，https://wezfurlong.org/wezterm/config/lua/general.html

-- 快速鍵
-- ctrl+shift+t    create new tab

-- ref
-- [How To Create An AMAZING Terminal Setup With Wezterm](https://www.youtube.com/watch?v=TTgQV21X0SQ)
-- [Linux终端神器“wezterm”，带你从0开始配置一个漂亮丝滑的terminal](https://www.youtube.com/watch?v=sS5g40K00do)


local wezterm = require 'wezterm'
local config = wezterm.config_builder()

-- 設置顏色主題
-- 參考，https://wezfurlong.org/wezterm/colorschemes/index.html
config.color_scheme = 'Afterglow'

-- 手動調整顏色
config.colors = {
	foreground = "#CBE0F0",
	background = "#011423",
	cursor_bg = "#47FF9C",
	cursor_border = "#47FF9C",
	cursor_fg = "#011423",
	selection_bg = "#033259",
	selection_fg = "#CBE0F0",
	ansi = { "#214969", "#E52E2E", "#44FFB1", "#FFE073", "#0FC5ED", "#a277ff", "#24EAF7", "#24EAF7" },
	brights = { "#214969", "#E52E2E", "#44FFB1", "#FFE073", "#A277FF", "#a277ff", "#24EAF7", "#24EAF7" },
}

-- 設置字形
-- 下載 Noto Sans Mono 字型，https://fonts.google.com/noto/specimen/Noto+Sans+Mono
config.font = wezterm.font('Noto Sans Mono')
config.font_size = 14

-- 設置預設啟動程式
config.default_prog = { 'C:/Users/ching/AppData/Local/Programs/nu/bin/nu.exe', '-l' }

-- 關閉上方的 tab-bar
config.enable_tab_bar = true

-- 控制視窗的標題欄
--config.window_decorations = "NONE"  -- 完全隱藏，無法透過滑鼠關閉視窗，也無法調整視窗大小
--config.window_decorations = "RESIZE"  -- 可調整視窗大小
config.window_decorations = "TITLE | RESIZE"  -- 預設值

-- 設置透明度
config.window_background_opacity = 0.8		-- value : 0 ~ 1，值越高越透明
-- 設置背景模糊程度
config.macos_window_background_blur = 10		-- value : 0 ~ 100，值越低越模糊

return config
