## openrc 服務管理系統

- 運行級別(runlevel)
  
  openrc 將開機、進入系統、休眠、關機 等分為各種不同的階段來管理服務，
  這些階段稱為runlevel，預設有以下的runlevel

  - `sysinit階段`，系統初始化運行級別，用於系統啟動的最早期階段
    - 功能，掛載文件系統、設置系統時鐘、加載內核模塊
    - 典型服務，例如，hwdrivers、devfs、sysfs

  - `boot階段`，系統啟動運行級別
    - 功能，系統完成基礎初始化後，進一步啟動系統關鍵服務
    - 典型服務，例如，network、localmount、swap、fsck 

  - `defalt階段`，系統正常運行時的主要運行級別，
    - 功能，啟動用戶應用程序和服務
    - 典型服務，例如，如 dbus、sshd、cron、ntpd、sshd 

  - `shutdown階段`，系統關機運行級別
    - 功能，在系統關機前，停止所有服務，確保系統安全關閉
    - 典型服務，停止所有已啟動的服務

  - `reboot階段`，系統重啟運行級別
    - 功能，在系統重啟前，停止所有服務，確保系統安全重啟
    - 典型服務，停止所有已啟動的服務
  
  - `nonetwork階段`，無網絡運行級別
    - 功能，啟動基本服務，但不啟動網絡相關服務

- openrc用於管理init系統、服務的工具包，提供 `rc-update`、`rc-status`、`rc-service`、`openrc` 等命令

## rc-update，添加或刪除不同級別的服務

- 語法，`rc-update 選項 add|delete 服務名 級別`

- 列出所有已註冊服務和其級別，`$ rc-update show`
- 列出sysinit級別中所有啟用的服務及其狀態，`$ rc-update show 級別`

- 添加服務，`$ rc-update add sshd default`
- 刪除服務，`$ rc-update delete sshd default`

## rc-status，查看服務的狀態

- 語法，rc-status 級別

- 分組顯示所有服務的狀態，`$ rc-status --all`
- 不分組顯示所有服務的狀態，`$ rc-status --servicelist`

- 顯示所有服務的狀態，並顯示為ini格式，`$ rc-status -f ini --all`
- 顯示sysinit級別下的所有服務的狀態，`$ rc-status sysinit`
- 顯示未分配給任何級別的服務，`$ rc-status --unused`
- 顯示崩潰的服務，`$ rc-status --crashed`

## rc-service，管理服务的状态，例如，啟動|停止|重啟 服務

- 語法，`rc-service 服務名 start|stop|restart`

- 列出所有可用的服務，`$ rc-service --list|-l`

- 測試服務是否存在 `$ rc-service --exists|-e 服務名`，
  - 若存在，螢幕不會有任何輸出，命令返回0
  - 若不存在，螢幕不會有任何輸出，命令返回1

- 服務如果存在，就執行指定命令，`$ doas rc-service --ifexists sshd start`
- 服務如果存在，就執行指定命令，靜音版，`$ doas rc-service --ifexists --quite sshd start`

- 查看單個服務的依賴關係，`$ rc-service --resolve 服務名`