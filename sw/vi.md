## vi 的使用

- 常用快速鍵 (emacs keybinding)
  - alt+f (ctrl+右方向鍵): 前進到下一個 word
  - alt+b (ctrl+左方向鍵): 後退到前一個 word
  - ctrl+y: undo
  - ctrl+u: 刪除 cursor 之前的所有字符
  - ctrl+k: 刪除 cursor 之後的所有字符
  - ctrl+w: 往前刪除一個 word
  - ctrl+a: 回到第一個字符
  - ctrl+e: 回到最後的字符
  - ctrl+p: 恢復前一個命令 (previous)
  - ctrl+n: 恢復前一個命令 (next)
  - ctrl+l: 清除螢幕，僅保留當前的命令行
  - ctrl+d: 退出交互模式 (exit)

- 常用快速鍵 (vi keybinding)
  - 注意，查看預設的快速鍵，`$ sudo nano /usr/share/fish/functions/fish_vi_key_bindings.fish`，
    或參考[源碼](https://github.com/fish-shell/fish-shell/blob/master/share/functions/fish_vi_key_bindings.fish)

  - 注意，以下快速鍵需要透過 Esc 退出 Insert-mode
  - 注意，不是所有語法都支援
    - 不支援 !$ !!

  - `w` : 前進到下一個 word
  - `b` : 後退到前一個 word
  - `f<字母>` : 直接跳到指定的字母
  - `u` : undo
  - `d+shift+^`: 刪除 cursor 之前的所有字符
  - `d+shift+$`: 刪除 cursor 之後的所有字符
  - `d+b`: 往前刪除一個 word
  - `d+w`: 往後刪除一個 word
  - `shift+^` 或 `數字0`: 回到第一個字符
  - `shift+$`: 回到最後的字符
  - `k` : 恢復前一個命令 (previous)
  - `j` : 恢復前一個命令 (next)
  - `ctrl+l`: 清除螢幕，僅保留當前的命令行
  - `ctrl+d`: 退出交互模式 (exit)

