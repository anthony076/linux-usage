## fzf 的使用

- fzf 為 fish 提供預設的配置檔
  - 透過 `$ fzf --fish` 可以查看內容，
  - 透過 `$  fzf --fish > ~/.config/fish/conf.d/key-bindings.fish`，當內容添加到 fish 自動啟動的目錄中
  
  - 該配置檔提供以下的預設函數，將key-bindings.fish添加到自動加載目錄後，可以shell中直接執行
    - `fzf_key_bindings()`，
    - `fzf-file-widget()`，利用fzf顯示當前目錄下的所有檔案
    - `fzf-cd-widget()`，利用fzf顯示當前目錄下的所有目錄
    - `fzf-history-widget()`，利用fzf顯示shell-history
  
  - 該配置檔提供以下的快速鍵，將key-bindings.fish添加到自動加載目錄後，可以shell中直接執行
    - `Ctrl+R`：使用 fzf 搜索和選擇命令歷史，相當於執行 fzf-history-widget()
    - `Ctrl+T`：使用 fzf 查找文件並插入到命令行中，相當於執行 fzf-file-widget()
    - `Alt+C`：使用 fzf 來搜索目錄並切換到選定的目錄，相當於執行 fzf-cd-widget()

- 可用的環境變數
  - fzf 預設使用 find 進行搜索，可以透過 FZF_DEFAULT_COMMAND 改用更快的 fd 命令
  - `FZF_DEFAULT_COMMAND`，執行 fzf 時，實際執行的命令
  - `FZF_DEFAULT_OPTS`，執行 fzf 時，自動添加的配置參數

  - `FZF_CTRL_T_COMMAND`，按下CTRL+t後，實際執行的命令
  - `FZF_CTRL_T_OPTS`，按下CTRL+t後，自動添加的配置參數

  - `FZF_ALT_T_COMMAND`，按下ALT+t後，實際執行的命令
  - `FZF_ALT_T_OPTS`，按下ALT+t後，自動添加的配置參數
  
- 基礎使用
  - `$ fzf`，在`當前目錄`下搜尋並索引所有可用的文件
  - 支援re語法
    - conf$，篩選並顯示在尾部出現的conf
    - ^conf$，篩選並顯示在頭部出現的conf
    - abc | def ，顯示具有 abc 或 def 的結果

## 配置

- 常用配置參數
  - `--height 40%`，設定結果列表的高度為全畫面的 40%
  - `--layout=reverse`，設定反向結果列表，結果列表會出現在下方
  - `--prompt="select to kill: "`，在當前輸入行顯示提示語
  - `--header="select to kill: "`，在當前輸入行的上方，顯示提示語
  - `--cycle`，使用循環，當選項到達邊界時，在按下同一個方向鍵，就會自動進入循環，而不需要更改方向鍵才能移動
  - `--border`，為結果列表添加邊框
  - [--bind的使用範例](https://youtu.be/tDM3GrfE7Ns?si=DppsAbErh1MfWWvq&t=216)
  - `--query`，設置預設的搜尋字串，用於取代fzf選單跳出後才手動輸入要搜尋的目標

  - `--multi`，啟用選取多個選項
    - 使用`<tab>`進行選取
    - 使用`<shift+tab>`取消選取

  - `--info`，搜尋數量的顯示位置
    - default，顯示於提示行的下一行
    - inline，顯示於提示行中
    - hidden，不顯示搜尋數量
    - right，顯示於右側

  - --margin，設置結果列表內縮的邊界
    - `--margin=10%`，上下左右都內縮10%
    - `--margin=5%,10%`，上下內縮5%，左右內縮10%
    - `--margin=5%,10%,3%`，上內縮5%，左右內縮10%，下內縮3%
    - `--margin=1%,2%,3%,4%`，上內縮1%，右內縮2%，下內縮3%，左內縮4%

  - --preview，設置預覽
    - --preview 接受檔案|單行|多行命令
    - `--preview "bat --style=numbers --color=always {}`，設置預覽命令和參數
    - `--preview-window=right:80%`，設置預覽視窗的大小

- 配置範例，利用 enter 鍵打開檔案，`$ fzf --bind='enter:execute(nano {})+abort'`
  - 預覽功能，`fzf --preview 'bat --style=numbers --color=always {}' --preview-window=right:80%`
  - 預覽捲動，`fzf --height 40% --layout=reverse --preview "bat --style=numbers --color=always {} | less -RF"`
  - 以tree預覽所有檔案和子目錄，`find . | fzf --preview 'tree {}'`
  - 利用 shell 配置文件的全域變數來控制 fzf 的功能，例如，

    ```shell
    # 打開檔案
    OPEN_FILE="--bind='enter:execute(nano {})+abort'" 
    export FZF_DEFAULT_OPTS
    ```

## 使用範例

- 範例，自定義選項，在 fish-shell 中輸入，`$ echo -e 'red\ngreen\nblue' | fzf`

- 範例，輸入檔案路徑時的自動補全，
  - 狀況，`輸入 nano 後，按下 ctrl+t 透過fzf選擇檔案，選中並自動補全檔案路徑`
  - 狀況，`輸入 nano /etc > 按下 ctrl+t 後會以 /etc 為預設目錄 > 選中並自動補全檔案路徑`

- 範例，利用 fzf 快速變更目錄，
  - 狀況，快速切換到子目錄，`ALT+C > 選中目錄 > 自動切換到該目錄`
  - 狀況，快速切換到指定目錄，`輸出 /etc > ALT+C > 選中目錄 > 自動切換到該目錄`

- 範例，透過 fzf 快速執行程式，`$ nano $(fzf)`，執行後，使用者需要選中檔案後，會自動開啟該檔案

- 範例，將 shell 輸出結果放入 fzf 進行搜尋，`$ ls -la | fzf`

- 範例，限定搜索深度，`$ find / -maxdepth 2 | fzf`

- 範例，透過 fzf 列出所有PID，選擇後執行刪除
  - 命令，`$ ps aux | fzf --height 40% --layout=reverse --prompt="select to kill: "| awk '{print $1}' | xargs -r doas kill`
  - 參數，`awk '{print $1}'`，fzf選中後，輸出第一個欄位
  - 參數，`xargs -r 要執行的命令`，為多個輸出執行命令，`-r`，若命令不存在就不執行
  - 命令，`$ kill > 輸出空白 > tab > 選中`

- 範例，fzf + tmux，選擇目錄後，透過 tmux 開啟

  ```shell
  function create_tmux_session(){
    local RESULT="$1"
    local SESSION_NAME = $(basename "$RESULT" | tr ' .:' '_')
    tmux new-session -d -s "$SESSION_NAME" -c "$RESULT"
    tmux attach -t "$SESSION_NAME"
  }
  ```

- 範例，[透過 fzf 開啟 bookmark](https://youtu.be/gDPhsQEcY_c?si=QSz5cjOFXUZQL0Ex&t=334)

- 範例，[fzfm，透過 fzf 作為 terminal-based 的檔案管理器](https://youtu.be/tDM3GrfE7Ns?si=25IeO7lAw9SmcMRx&t=300)

- 範例，fzf和tmux搭配使用
  - [fzf-tmux](https://youtu.be/tB-AgxzBmH8?si=UTbRY14BImnAdZ5s&t=1307)

- 範例，fzf和git-repo搭配使用
  - [fzf+Git branches](https://github.com/exosyphon/dotfiles/blob/main/scripts/fsb.sh)
  - [fzf+Git commits](https://github.com/exosyphon/dotfiles/blob/main/scripts/fshow.sh)
  - [demo](https://www.youtube.com/watch?v=-B5MTJXW-vA)

## ref

- [FuZzy Finder Tutorial](https://www.youtube.com/watch?v=tB-AgxzBmH8)
