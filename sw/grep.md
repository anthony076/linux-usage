## grep，搜尋檔案內容

- 語法，`輸入 | xargs [參數] 要執行的命令`
  - `-d 分隔符號` : 指定分隔符號 
  - `-n 傳遞參數數量` : 指定每次最多傳遞給命令的參數數量
  - `-I 佔位符號` : 設置佔位符號
  - `-l 傳遞行數` : 每次讀取指定數量的行，而不是默認的單行
  - `-s 最大參數列表大小` : 設置允許的最大參數列表大小，例如，`printf 'a%.0s' {1..100} | xargs -s 50 echo`
  - `-x` : 如果生成的命令超過指定大小，則退出，例如，`printf 'a%.0s' {1..100} | xargs -s 50 -x echo`

- 範例，`echo 'aa  bb cc' | xargs mkdir`，預設使用空白為分隔符號
  - step，將 'aa  bb cc' 拆分為 [aa bb cc]
  - step，將 [aa bb cc] 分別傳遞給 mkdir 
    - mkdir aa
    - mkdir bb
    - mkder cc

- 範例，`$ echo 'apple,orange,banana' | xargs -d ',' echo`，手動設置分隔符號

- 範例，，強制每次只處理一個 item

  `$ echo 'apple orange banana' | xargs -n 1 echo 'Fruit:'`

  ```
  Fruit: apple
  Fruit: orange
  Fruit: banana
  ```

  `$ echo 'aa  bb cc' | xargs -n 1 echo`

  ```
  aa
  bb
  cc
  ```

- 範例，`$ echo 'apple orange banana' | xargs -I {} echo 'Fruit: {}'`，將 item 插入佔位符號中