## sed 的使用
- 語法: `$ sed 寫入模式 'pattern' 檔案`

- 寫入模式
  - `-e`: 預設值，將修改後的結果，包含檔案內容，全部顯示在shell，會顯示目標行，但不會執行寫入
  - `-n`: 沉默模式，將修改後的結果，只針對修改行，顯示在shell，不會顯示目標行，但不會執行寫入
  - `-i`: 常用，直接修改檔案，但不會將結果顯示於 shell
  - `-f`: 從檔案讀取 pattern 內容，需要指定檔案路徑
  - 建議: 可利用 `-e` 或 `-n` 驗證語法是否正確，再利用 `-i` 進行實際修改

- pattern 語法
  - 根據要執行的操作有不同的語法

  - `語法1`，用於`搜尋取代`操作的語法(s for search)，`'s/尋找目標/替換字串/標記'`
    - 可用標記
      - 0-9: 表示(只搜尋/只取代)第N個發現的目標
      - g: 全部取代
      - i: 忽略大小寫
      - w: 把符合的結果寫入檔案

  - `語法2`，用於`插入後方`操作的語法(a for append)，`'指定行數a/插入字串'`
    - 找到指定行數後，將插入字串插入到目標行的`後方`
    - 多個指定行以 `,` 區隔，代表`區間`的意思，區間內的所有行都會被處理
      
      例如，`$ sed '1,3a/hello' ttt.log`，第1、2、3行的後方，都插入hello
    
    - 最後一行以 `$` 表示，例如，`$ sed '$a/hello' ttt.log`，最後一行的後方插入hello

  - `語法3`，用於`插入前方`操作的語法(i for insert)，`'指定行數i/插入字串'`
    - 找到指定行數後，將插入字串插入到目標行的`前方`
    - 其餘操作與語法2的append相同

  - `語法4`，用於`整行替換`操作的語法(c for column)，`'指定行數c/替換字串'`
    - 用於多行時，不是將每一行都替換為指定字串，而是將指定區間內的行都會取代成一行
    - 例如，`$ sed '3c hello ttt.log`，把第三行整行替換為字串 hello
    - 例如，`$ sed '1,5c hello ttt.log`，把第一至第五行替換為字串 hello

  - `語法5`，用於`刪除整行`操作的語法(d for delete)，`'/搜尋字串/d'`
    - 將有包含搜尋字串的行刪除
    - 例如，`$ sed '/pen/d' ttt.log`，刪除所有含有 pen 這個字的行

- 特殊字符
  - 特殊字符包含: `\ # & [] `，遇到特殊字符的前方要加上 \ 避免轉義

  - 遇到含有路徑的字串
  
    在 sed 中使用 / 作為分隔分號，來區分不同的區域

    若是字串中包含路徑，就會使原有的 / 失效，
    `可改用 # 或 | 或 ; `取代 sed 預設的 / 分隔符號，原來路徑中的 / 還是保留

    或將搜尋字串中的 / 改為 \/，sed 就不會將字串中的 / 視為是特殊字，
    
    例如，`#XferCommand = /usr/bin/curl -L -C - -f -o %o %u` 取代為 `XferCommand = /usr/bin/axel -n 5 -a -o %o %u`

    ```shell
    sed -i 's/#XferCommand = \/usr\/bin\/curl -L -C - -f -o %o %u/XferCommand = \/usr\/bin\/axel -n 5 -a -o %o %u/g' /etc/pacman.conf
    ```

## 注意事項
- 若檔案內容為空，內容沒有第n行之分，sed 就會失效
  
## 範例集

- 範例，搜尋並取代
  - 將第一次出現的have，替換為 had，`$ sed -i 's/have/had/1' ttt.log`
  - 將所有的pen，都替換為 pencil，`$ sed -i 's/pen/pencil/' ttt.log`
  - 注意，搜尋並取代不需要完整的命令行，
    - 例如，要將 #opacity-rule = ["80:class_g = 'Alacritty'"]; 更換為 opacity-rule = ["80:class_g = 'Alacritty'"];
    - 只需要 `$ sed -e "s/#opacity-rule/opacity-rule/1" ttt.log`，
      可以只針對部分字串處理，不需要處理整行的字串

- 範例，插入後方範例
  - 在第一行後方，插入指定字串，`$ sed -i '1a hello' ttt.log`
  - 在最後一行後方，插入指定字串，`$ sed -i '$a 123' ttt.log`
  - 找尋`含有特定字串的行`後，在該行後方插入特定字串，
    - 範例，`$ sed '/^root.*/a 123' ttt.log`
    - `$ /^root.*`，尋找 root開頭的行
    - `$ /a 123`，在 root 行後方插入

- 範例，整行替換範例
  - 把第三行整行，替換為指定字串，`$ sed '3c hello' ttt.log`
  - 將第1行至第5行，合併並替換為單行的指定字串，`$ sed '1,5c hello' ttt.log`

- 範例，使用 re 指定搜尋目標
  ```shell
  $ sed -i 's/\;\[include\]/[include]/' ttt.log
              ^^^^^^^^^^^^^ 搜尋目標
                            ^^^^^^^^^ 替換字串
  ```

- 範例，使用 re 指定目標行
  ```shell
  $ sed -i '/^anothervalue=.*/a after=me' ttt.log
              ^^^^^^^^^^^^^^^ 目標行
                              ^ append
                                ^^^^^^^^ 插入字串
  ```
