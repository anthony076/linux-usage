## 啟用 UEFI 的 security-boot，取代傳統的 BIOS
- SecureBoot 提供了對抗許多針對主機攻擊的防範能力 (Rootkits 惡意程式、Evil Maid 邪惡女僕)，而不會造成過多額外的困擾
  
  它無法阻止那種完全針對性的攻擊，而且有很高的可能性會被國家級的資安組織給攻破，但是有 SecureBoot 保護總比什麼都沒有要好的多。

## 建立內核模組的黑名單

把 Firewire 跟 Thunderbolt 模組加入黑名單，把下面這幾行加到 /etc/modprobe.d/blacklist-dma.conf 檔案裡：
blacklist firewire-core
blacklist thunderbolt

重新開機後，這些模組就會被列入黑名單。對系統不會有任何傷害，就算你沒有這些介面 (它本來也就不會生效)。

## wheel Group

- 使用場景
  
  當伺服器需要進行一些日常系統管理員無法執行的高級維護時，往往就要用到 root 權限，
  而「wheel」 組就是一個包含這些特殊權限的群組，若用戶不是「wheel」組的成員，就無法取得 root 權限進行一些特權的操作

  透過 wheel 群組界定獲得特殊權限的用戶，避免直接使用擁有最高權限的 root 帳號

- 命令: `$ sudo useradd -aG wheel 帳戶名`
  - -a 添加新帳戶
  - -G 指定帳戶群組
  - -f 強制執行，忽略錯誤訊息
  - -q 執行命令時不顯示訊息
  - -v 執行命令時顯示更多訊息

- 注意，wheel == 管理員群組，在某些 linux 發行版中有可能稱為 sudo 或 admin

- 注意，群組ID的大小與管理權限無關，
  - 一般管理組群為 adm 或 admin 或 wheel 或 sudo
    
    wheel 在 UNIX 中代表管理員，以船上掌舵的人為代表意義

  - 一般管理組群ID為 0 或 10

## 與身分驗證和權限管理相關的套件

- [sudo](../sw/sudo.md)
- [polkit](../sw/polkit.md)
- [pam](../sw/pam.md)
  
## Ref
- [如何強化主機的資訊安全](http://blog.itist.tw/2015/09/Linux-workstation-security-checklist-for-traditional-chinese.html)