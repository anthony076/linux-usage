## Systemd 配置檔(unitfile) 基礎
- 配置文件用來告訴 Systemd 如何啟用對應的 Unit 資源

- 幾個放置配置文件的重要目錄

  - `位置1`，(主要啟動目錄) `系統或用戶自定義`的配置文件目錄: `/etc/systemd/system/`

    - 注意，初始化過程中Systemd只執行此目錄裏面的配置文件，若要實現開機自動執行，需要把配置檔或軟連結放在此目錄中

    - 注意，`/etc/systemd/system/` 目錄主要存放指向 `/usr/lib/systemd/system/` 的`軟連結`
      
      對安裝的軟體來說，`/etc/systemd/system` 目錄下放的資源配置檔都不是真正的資源配置檔，
      而是指向資源配置檔的軟連結，`/etc/systemd/system/` 僅僅是連結到實際的資源配置檔的軟連接

      實際的資源配置檔放在 `/usr/lib/systemd/system/` 或 `/lib/systemd/system` 中，
      可透過 `$ ls -l /etc/systemd/system` 查看

      軟連結會根據資源配置檔中的 WantedBy 欄位，在執行 `$ systemctl enable example.service` 時自動建立

    - 子目錄 /etc/systemd/system/example.service.`wants`/，用於存放 example.service `之後執行`的配置文件

    - 子目錄 /etc/systemd/system/example.service.`requires`/，用於存放 example.service `之前執行`的配置文件
  
  - `位置2`，(運行時目錄) 資源`運行時`生成的配置文件目錄: `/run/systemd/system/`

  - `位置3`，(軟體更新目錄) 軟體安裝時，軟體預設的配置文件目錄: `/usr/lib/systemd/system/` 或 `/lib/systemd/system`
    
    - 類似 SysV 的 /etc/init.d 底下的檔案

    - 注意，只有支持 systemd 的軟體才會自動在此目錄添加軟體對應的配置文件
    
    - 透過 `$ ls -l /etc/systemd/system`，可確認當前系統使用的是`/usr/lib/systemd/system/` 或 `/lib/systemd/system`

    - 注意，若軟件進行更新，軟體預設的配置文件目錄內，對應的配置文件也會同步被更新
      
      若 `/usr/lib/systemd/system/` 內的配置文件人為修改過，則手動修改的配置，會在軟體更新時被重新覆蓋，
      若不希望手動修改的配置被覆蓋，正確方式應該將`手動修改後的同名配置文件`，存放在 `/etc/systemd/system/` 目錄中，

      範例見，[手動修改軟體預設配置文件的正確方式](#範例-手動修改軟體預設配置文件的正確方式以-vsftpdservice-為例)

- 配置文件的優先權

  依照優先權，軟體用於systemd的配置文件，有可能會出現在多個目錄中，例如，
  - 路徑1: /`etc`/systemd/system/example.service
  - 路徑2: /`run`/systemd/system/example.service
  - 路徑3: /`usr`/lib/systemd/system/example.service
  - 路徑4: /etc/`init.d`/example.service

  則優先權 路徑1(系統目錄) > 路徑2(運行時目錄) > 路徑3(軟體目錄) > 路徑4(SystemV目錄)

- 配置文件的覆寫和添加
  - 注意，不建議直接手動修改 /lib/systemd/system/ 目錄下的原始配置文件
    
    該目錄下的配置文件一般是透過套件安裝軟件(apt、pacman) 時自動建立的，
    該目錄下的配置文件會在軟體更新時被自動覆蓋，
    
    因此，若直接修改該目錄的配置文件，容易在軟件更新時遺失手動修改的配置

  - 兩種解決方法: 覆蓋和新增
    - 方法1，[覆蓋](#範例-覆蓋-service-的配置更新軟體時檔案不會被覆蓋)，
      透過在 `/etc/systemd/system 目錄`中，建立`同名的配置文件`可以進行覆蓋

    - 方法2，[添加](#範例-添加-service-的配置只能添加不能覆蓋原始欄位更新軟體時檔案不會被覆蓋)，
      透過在 `/etc/systemd/<unit名>.d/ 目錄`中，建立 `*.conf 檔案`可以進行添加

## 配置文件概述
- 概述
  - 必要，[Unit] 區塊: 當前 Unit 的相依姓
  - 選用，[Service] 區塊: 當前 Unit 的執行方式和配置執行相關的參數
  - 選用，[Socket] 區塊
  - 選用，[Timer] 區塊
  - 選用，[Mount] 區塊
  - 選用，[Path] 區塊
  - 必要，[Install] 區塊: 當前 Unit 要掛載的 target 底下
  
  - 注意，選用的區塊，需要根據副檔名(unit-type)決定是否啟用當前區塊，
    
    例如，example.service 會啟用 [Service] 的區塊

  - 設定的欄位是可以重複的
    
    例如，相同的兩個 After 的欄位，後面的欄位會覆蓋前面的欄位

  - 布林欄位的有效值
    - 若要將布林蘭位設置為True，可以設置為 1 | yes | true | on
    - 若要將布林蘭位設置為False，可以設置為 0 | no | false | off
  
  - 配置文件中的抑制錯誤的符號 (-)
  
    所有的啟動設置(Exec開頭的設置)之前，都可以加上一個連詞號（-），表示"抑制錯誤"，代表發生錯誤的時候，不影響其他命令的執行。
    
    例如，EnvironmentFile=-/etc/sysconfig/sshd，
    表示即使 /etc/sysconfig/sshd文件不存在，也不會拋出錯誤

  - 配置文件模板 (@) 用於簡化配置文件

    參考，[配置文件模板的使用](#技巧-使用--的配置文件模板用於簡化配置文件的產生)

- 存取環境變數
  
  - 注意，此處的環境變數 <font color=red>僅限在 unitfile 內使用</font>，登入的使用者無法透過 `echo $變數名` 進行存取

    登入使用者使用的環境變數，需要在 `~/.bashrc` 中進行配置

  - `方法1`，直接寫在 unitfile 中，透過 `Environment選項` 或 `EnvironmentFile選項` 進行設置

  - `方法2`，透過 conf 將環境變數動態添加到 service 中
    
    - (option1) 手動建立 conf 檔

      範例，[conf檔的使用](#範例-添加-service-的配置只能添加不能覆蓋原始欄位更新軟體時檔案不會被覆蓋)

    - (option2) 透過命令 `$ systemctl edit <service名不需要副檔名>` 快速建立 conf 檔

    - 注意
      - option1 和 option2 會在相同的位置建立 conf 檔
      - 此方法會覆蓋方法1建立的同名環境變數
      - 使用此方法需要`符合 unitfile 的寫法`，將環境變數添加到 Environment 欄位中

  - `方法3`，透過命令添加，且只對執行後啟動的用戶有效，對已經啟動的 systemd 實例無效
    - 適用場景: 用於憑證或機密的環境變數不適合寫在文件中，可以透過命令進行添加
    - 透過 `$ systemctl set-environment` 命令 或 `$ systemctl import-environment` 命令，進行設置
    - 透過 `$ systemctl ( set-environment | unset-environment | import-environment )` 設置的環境變數的有效期
      - 在主機 reboot 之後才會失效，
      - `$ systemctl (daemon-reexec | daemon-reload)` 的命令，都不會使設置的環境變數失效

  - 測試方法: 
    - 方法1，透過 `$ systemctl show-environment` 查看
      
      靜態方法，從已知的檔案載入環境變數值，

      與實際環境變數不一定準確，無法反映 conf 動態覆寫環境變數的結果

    - 方法2，在unitfile中，透過 `ExecStart=echo $變數名` 進行測試

      動態方法，與實際環境變數一致

## [配置] Unit 區塊

- `Description 欄位`: Unit 的簡易說明
  
- `Documentation 欄位`: 當前 Unit 的文檔，可以是檔案|man|網址
  
- `Before 欄位`: (unit -> daemon) 設置順序，`建議`啟動指定的 daemon 之前，先啟動當前的 Unit，`沒有強制性`
  
- `After 欄位`: (daemon -> unit) 設置順序`建議`在哪一個 daemon 啟動之後，才啟動當前 Unit，`沒有強制性`
  
- `Requires 欄位`: (daemon -> unit) 設定`強`相依性，`限制`指定的 daemon 起動後，才能啟動當前的 Unit ，`有強制性`
  
- `Wants 欄位`: (unit -> daemon) 設定`弱`相依性，`建議`當前的 Unit 啟動後，最好還要啟動那些 daemon，`沒有強制性`
  
  注意，若指定的 daemon 未啟動，對當前的Unit沒有影響

- `BindsTo 欄位`: 若指定的 daemon 退出，當前的 Unit 也會退出

- `PartOf 欄位`: 若指定的 daemon 失敗或重新啟動時，當前的 Unit 也會終止或重新啟動，但 daemon 不會隨著當前Unit的啟動而啟動

- `Conflicts 欄位`: 指定會衝突的 daemon，若指定的 daemon 存在，當前的Unit就不啟動，`有強制性`

- `Condition 欄位`: 當前 Unit 運行必須滿足的條件，否則不會運行，不運行不會報錯

- `Assert 欄位`: 當前 Unit 運行必須滿足的條件，否則不會運行並報錯

## [配置] Service 區塊
- `Type 欄位`: 當前 service 的啟動方式，會影響 ExecStart
  - simple 值: 預設值，
    - 表示由 ExecStart 欄位的指令串來啟動 daemon，啟動後的 deamon 為主進程會常駐於記憶體中 
    - 若當前 Unit 會啟動其他服務，不要使用此類型啟動，除非該服務是 socket 激活類型的 Unit
  
  - forking 值

    由 ExecStart 欄位啟動的程序，透過 spawns 延伸出其他子程序來作為此 daemon 的主要服務，
    原生的父程序在啟動結束後就會被終止運作，可以避免主進程被阻塞

    對於一般的 daemon，除非確定此啟動方式無法滿足需求，否則一般使用此方式啟動

  - oneshot 值
    - 一次性服務，和 simple 值類似，但程序關閉後就結束，不會常駐在記憶體中
    - 通常需要指定 RemainAfterExit=yes，systemd 退出後才會認為該服務仍然處於激活狀態

  - dbus 值: 通過 D-bus 啟動服務，但程序必須取得 D-bus 名稱後才能繼續運作

    若使用 Type=dbus，必須也要設置 BusName 的欄位

  - idle 值: 和 simple 值類似，必須等到所有工作都順利執行完畢，進入 idle 後才會執行當前的 service

    通常是開機到最後才執行即可的服務

  - notify 值: 服務啟動完畢後，會先通知 systemd ，再繼續往下執行

- `PIDFile 欄位`: 指定PID檔案的路徑
- `User 欄位`: 指定當前應用程式要執行的身分 
- `Group 欄位`: 限定執行身分所在的群組
  
- `Environment 欄位`: 指定啟動腳本的環境變數
- `EnvironmentFile 欄位`: 指定啟動腳本的環境設定檔路徑

- `ExecStartPre 欄位`:啟動執行命令`前`的額外指令 
- `ExecStart 欄位`: 
  - systemctl start 指令實際執行的欄位，為啟動應用程式的實際命令，可指定為 shell-script 位置
  - 此欄位只接受 `指令 參數1 參數2` 的形式，不支援完整的 bash 語法，若需要完整語法，可使用 shell-script，或使用內建變數
    > 例如，ExecStop=kill $MAINPID
- `ExecStartPost 欄位`: 啟動執行命令`後`的額外指令
- `ExecStop 欄位`: systemctl stop 指令實際執行的欄位，用於關閉 service
- `ExecStopPost 欄位`: 服務停止後額外執行的指令
- `ExecReload 欄位`: systemctl reload 指令實際執行的欄位

- `Restart 欄位`: Restart=1 時，則當此 daemon 服務終止後，會再次的啟動此服務
  - always 值: 總是重啟 
  - on-success 值: 只有正常退出時（退出狀態碼為0），才會重啟
  - on-failure 值: 非正常退出時（退出狀態碼非0），包括被信號終止和超時，才會重啟 (推薦守護進程使用)
  - on-abnormal 值: 只有被信號終止和超時，才會重啟 (推薦允許發生錯誤退出的服務使用)
  - on-abort 值: 只有在收到沒有捕捉到的信號終止時，才會重啟
  - on-watchdog 值: 超時退出，才會重啟
  - no 值: 退出後不會重啟

- `RestartSec 欄位`: 應用程式重新啟動前的 sleep 時間
- `RemainAfterExit 欄位`: RemainAfterExit=1 時，則當這個 daemon 所屬的所有程序都終止之後，此服務會再嘗試啟動
- `TimeoutSec 欄位`: 若服務無法正常啟動或正常結束，進入強制結束前的等待時間
- `KillMode 欄位`: 可以是 process | control-group | none
  - process 值: 終止時，只會終止ExecStart欄位啟動的應用程式 (適用於 sshd-service)
  - control-group 值: 終止時，此 daemon 所產生的其他 control-group 的程序，也都會被關閉
  - mix 值: 發送`SIGTERM信號`給主進程，發送`SIGKILL信號`給子進程
  - none 值: 沒有進程會被關閉，只是執行服務的 stop 命令

## [配置] Install 區塊
- 通常放在配置文件中的最後一個區塊

- `WantedBy 欄位`
  - 用於配置當前的unit是附掛在哪一個 target-unit，通常是一個或多個 target-unit 的值

    若配置了WantedBy欄位，執行 systemctl enable後 ，會在 etc 目錄中建立指向當前unit的軟連接，例如
    
    - step1，在 `/lib/systemd/system/aa.service` 的檔案中，設置了 `WantedBy=bb.target`，
    - step2，執行 `$ systemctl enable aa.service`
    - step3，systemd 自動在 `/etc/systemd/system/bb.target.wants` 路徑中，添加 `aa.service` 的軟連接，
      且軟鏈接會指向 `/lib/systemd/system/aa.service` 的檔案
    
  - 大多數服務性質的unit都是附掛在 multi-user.target 下

- `RequiredBy 欄位`
  - 用法和 WantedBy 類似，軟連接會放在 `/etc/systemd/system/<target名>.required` 的目錄中

- `Also 欄位`: 當前unit被enable時，需要一起致能的unit，一般填寫具有相依性的服務

- `Alias 欄位`: 為建立etc目錄和lib目錄之間的軟連接

  和 WantedBy欄位類似，會在 etc 目錄中建立指向當前unit的軟連接，
  但是不像WantedBy欄位使用當前unit名作為軟連接名，而是以 Alias欄位的值做為軟連接的檔案名

  - step1，在 `/lib/systemd/system/aa.service` 的檔案中，設置了 `WantedBy=bb.target`，且 `Alias=mybb.target`
  - step2，執行 `$ systemctl enable aa.service`
  - step3，systemd 自動在 `/etc/systemd/system/bb.target.wants` 路徑中，添加 `mybb.target` 的軟連接，
    且軟鏈接會指向 `/lib/systemd/system/aa.service` 的檔案

## [配置] Timer 區塊
- `OnActiveSec欄位`:	當 timers.target 啟動多久之後才執行當前的unit

- `OnBootSec欄位`:	當開機完成後多久之後才執行

- `OnStartupSec欄位`:	當 systemd 第一次啟動之後過多久才執行

- `OnUnitActiveSec欄位`:	這個 timer 設定檔所管理的那個 unit 服務在最後一次啟動後，隔多久後再執行一次的意思

- `OnUnitInactiveSec欄位`:	這個 timer 設定檔所管理的那個 unit 服務在最後一次停止後，隔多久再執行一次的意思。

- `OnCalendar欄位`:	使用實際時間 (非循環時間) 的方式來啟動服務的意思

- `Unit欄位`:	指定是哪一個 service-unit

- `Persistent欄位`:	當使用 OnCalendar 的設定時，指定該功能要不要持續進行的意思。通常是設定為 yes，才能夠實現類似 anacron 的功能

## [參考] 可用的 Target-Unit
- Target-Unit == Unit 的集合

  一個 Target-Unit 包含許多相關的Unit 。啟動某個 Target-Unit 時，Systemd 會啟動裡面所有的Unit

- 官方提供的 [Target 關係圖](https://www.freedesktop.org/software/systemd/man/bootup.html#System%20Manager%20Bootup)
  
- 幾種常見的 Target-Unit
  - `default.target`
    - 開機後第一個執行的 Unit，用於定義系統開機後要自動啟動那些服務，

    - 注意，`/lib/systemd/system/default.target` 只是個軟連接，
      - 可透過 `$ systemctl get-default`，查看實際連結的 target
      - 或透過 `$ systemctl set-default target名`，重新設置 default.target 的指向

  - `graphical.target`：就是文字加上圖形界面
    > 注意，graphical.target 也會啟動 multi-user.target 

  - `multi-user.target`：純文字模式

  - `rescue.target`：在無法使用 root 登入的情況下，systemd 在開機時會多加一個額外的暫時系統
  - `emergency.target`：緊急處理系統的錯誤，還是需要使用 root 登入的情況，在無法使用 rescue.target 時，可以嘗試使用這種模式

  - `shutdown.target`：關機用

  - `getty.target`：設定需要幾個 tty，若想要降低 tty 的項目，可以修改這個東西的設定檔！
  
## [技巧] 使用 @ 的配置文件模板，用於簡化配置文件的產生
- 使用場景
  
  對於服務需要啟動多個應用實例，而每個應用實例的內容相似，僅僅只是`單一參數`的改變時，
  可透過 `@變數值` 簡化多個類似的 service 的啟動

  - 使用 @ 的配置文件稱為`配置文件模板`，例如，example@service
  - 透過配置文件模板產生實際的配置文件，稱為`配置文件實例`，例如 example@`instance1`.service
  - 在 linux 中，example@`instance1`.service 不存在於檔案系統中，而是透過 example@service 動態產生的
    
    配置文件模板裡包含`變數`，不能直接使用，
    
    利用配置文件模板 example@service 動態產生 example@instance1.service 的實際配置文件

- 範例，以 tty 為例
  - tty 的服務是透過 getty.target 來啟動，linux 預設會啟用多個 tty 實例來供使用者連線

  - 檢視 getty.target 的內容，`$ systemctl show getty.target`
    ```shell
    Names=getty.target
    Wants=getty@tty1.service
    WantedBy=multi-user.target
    Conflicts=shutdown.target
    Before=multi-user.target
    # 啟動 getty.target 之後，會接續啟動 getty@tty1.service ... getty@tty5.service，共 5 個配置檔
    After=getty@tty1.service getty@tty2.service getty@tty3.service getty@tty4.service getty@tty6.service getty@tty5.service
    ```

  - 查看 `/lib/systemd/system` 目錄，會發現目錄中只有 getty@.service，
    並沒有 getty@tty1.service ... getty@tty5.service 的配置檔

    ```shell
    $ ls -l | grep getty

    lrwxrwxrwx 1 root root   14 Jun 27 18:28 autovt@.service -> getty@.service
    -rw-r--r-- 1 root root 1073 Jun 27 18:28 console-getty.service
    -rw-r--r-- 1 root root 1254 Jun 27 18:28 container-getty@.service
    -rw-r--r-- 1 root root  517 Mar 11  2022 getty-pre.target
    `-rw-r--r-- 1 root root 1966 Jun 27 18:28 getty@.service`  # <---- 唯一接近目標的檔案
    -rw-r--r-- 1 root root  366 Jun 27 18:28 getty-static.service
    -rw-r--r-- 1 root root  508 Mar 11  2022 getty.target
    drwxr-xr-x 2 root root 4096 Aug  9 11:58 getty.target.wants
    -rw-r--r-- 1 root root 1483 Jun 27 18:28 serial-getty@.service
    ```
  
  - getty@.service 是使用 %I 變數的配置檔
  
    - getty@.service 是內部使用 %I 變數的配置檔
    - getty@`tty2`.service 是指定 %I 變數值的檔案名，等效於 getty@`%I`.service，
    
    因此，<font color=blue>變數%I = tty2</font>
    
    實際上不會有 getty@tty2.service，當 system 遇到帶有@的檔名時，例如，getty@tty2.service 
    - step1，將 @ 後方的字串視為是%I的變數值，例如，%I = tty2
    - step2，將 %I = tty2 帶入 getty@tty2.service 的檔案中
    - step3，將 getty@.service 檔案中的 %I 替換成 tty2

    ```shell
    # /lib/systemd/system/getty@.service

    [Service]
    # 若解析 getty@tty2.service 的檔案時，將 getty@.service 中的變數替換成 tty2
    ExecStart=-/sbin/agetty -o '-p -- \\u' --noclear %I $TERM
    Type=idle
    ```

  - 透過此方式不需要建立 getty@tty1.service ... getty@tty5.service 五個檔案，
    只需要一個 getty@.service 就可以建立多個類似的配置檔

## [技巧] 配置文件內可使用的變數
- $MAINPID: 應用程式時實例的PID

- 配置文件模板相關變數

  詳見，[使用 @ 的配置文件模板，簡化配置文件的產生](#技巧-使用--的配置文件模板用於簡化配置文件的產生)

  以下變數以 `ttt@hello.service` 為範例

  - %n: 當前 Unit 名的完整名稱，含副檔名，例如，`ttt@hello.service`
  - %N: 當前 Unit 名的完整名稱，不含副檔名，例如，`ttt@hello`
  - %p: @ 符號前的前綴字串，例如，`ttt`  
  - %i: @ 符號前的後綴字串，例如，`hello`
  - %f: @ 符號前的後綴字串，並加上/的前綴，例如，`/hello`
  - %c: 指示當前unit的cgroup路徑 ，但不包含/sys/fs/cgroup/ssytemd/，例如，`/ttt@hello.service`
  - %u: 配置當前 unit 的使用者名稱，例如，`root`
  - %U: 配置當前 unit 的使用者ID
  - %H: 執行當前 unit 的主機名

## [範例] 建立 service 的最小範例

  ```shell
  [Unit]
  Description=My custom startup script

  [Service]

  ExecStart=/home/transang/startup.sh start

  [Install]
  WantedBy=multi-user.target
  ```

## [範例] 覆蓋 service 的配置，更新軟體時檔案不會被覆蓋
- 基本概念
  
  當使用 systemctl enable 服務添加到 target 後，會在 /etc/systemd/system/<target名>.wants/ 目錄中添加軟連接，

  當 systemctl 要執行服務時，
  - 路徑1，先在 /etc/systemd/system/ 目錄下尋找是否有指定的配置文件，若有，啟動該配置文件，若無，
  - 路徑2，接著在 /etc/systemd/system/<target名>.wants/ 目錄下，尋找與指定的配置文件同名的軟連接並執行
  
  透過此方式，可以實現路徑1覆蓋路徑2的配置文件，且手動的配置文件不會在軟體更新被覆寫內容
  
- 範例
  - step1，建立自定義的 myservice.service，用於模擬軟體自動建立的配置檔

    ```shell
    # /lib/systemd/system/myservice.service
    [Unit]
    Description=My custom startup script

    [Service]
    type=oneshot
    ExecStart=echo 123

    [Install]
    WantedBy=multi-user.target
    ```

  - step2，`$ systemctl enable myservice.service`
    
    此命令建立 /etc/systemd/system/multi-user.target.wants/myservice.service 的軟連接，
    並指向  /lib/systemd/system/myservice.service

  - step3，建立手動配置檔，`/etc/systemd/system/myservice.service`

    ```shell
    [Unit]
    Description=My custom startup script

    [Service]
    type=oneshot
    ExecStart=echo 456  # <--- 修改此行

    [Install]
    WantedBy=multi-user.target
    ```

  - step4，重新載入
    - 重新載入配置，`$ systemctl deamon-reload`
    - 重新啟動服務，`$ systemctl restart myservice.service`
    
  - step5，查看手動配置檔是否覆蓋成功，`$ systemctl status myservice.service`

## [範例] 添加 service 的配置，只能添加不能覆蓋原始欄位，更新軟體時檔案不會被覆蓋
- 結論
  - 檔案1，`/etc/systemd/system/<unit名>.service.d/*conf` 檔案只能`添加`原來配置文件中沒有的欄位，不能進行覆蓋
    > 注意，此目錄下的內容，會被添加到 /lib/systemd/system/<unit名>.service 的配置文件中
  - 檔案2，`/etc/systemd/system/<unit名>.service` 檔案，用來`覆蓋` /lib/systemd/system/<unit名>.service 的內容
  - 檔案1和檔案2的作用不衝突，可以同時存在

- step1，建立 /lib/systemd/system/ttt.service
  ```shell
  [Unit]
  Description=My custom startup script

  [Service]

  ExecStart=echo I am lib

  [Install]
  WantedBy=default.target
  ```

- step2，建立 /etc/systemd/system/multi-user.target.wants/ttt.service 的軟連結
  - 執行 `$ systemctl enable ttt.service`
  - `/etc/systemd/system/multi-user.target.wants/ttt.service` 軟連結會指向 `/lib/systemd/system/ttt.service`

  - 測試
    - `$ systemctl start ttt.service`
    - `$ systemctl status ttt.service` 
    - 得到
      ```shell
      Dec 02 11:32:54 user systemd[1]: Started My custom startup script.
      Dec 02 11:32:54 user echo[1674]: I am lib   # <--- 來自 /lib/systemd/system/ttt.service 的內容
      Dec 02 11:32:54 user systemd[1]: ttt.service: Deactivated successfully.
      ```

- step3，建立用於添加配置欄位的 conf 檔
  - `$ sudo mkdir /etc/systemd/system/ttt.service.d`
  - `$ sudo nano /etc/systemd/system/ttt.service.d/ttt.conf`
  - 填入以下內容
    ```shell
    [Service]
    ExecStart= echo i am conf
    ExecStartPost=echo done
    ```
  - 重新載入 daemon，`$ systemctl daemon-reload`

  - 測試
    - 重新執行 service，`$ systemctl start ttt.service`
    - 察看結果，`$ systemctl status ttt.service` 
      ```shell
      ○ ttt.service - My custom startup script
          Loaded: loaded (/lib/systemd/system/ttt.service; enabled; vendor preset: enabled)
          Drop-In: /etc/systemd/system/ttt.service.d  # <--- 多了 Drop-IN 行，將 conf 的內容載入
                  └─ttt.conf
          Active: inactive (dead) since Fri 2022-12-02 11:53:08 UTC; 6s ago
          Process: 2495 ExecStart=echo I am lib (code=exited, status=0/SUCCESS)
          Process: 2496 ExecStartPost=echo done (code=exited, status=0/SUCCESS)
        Main PID: 2495 (code=exited, status=0/SUCCESS)
              CPU: 3ms

      Dec 02 11:53:08 user systemd[1]: Starting My custom startup script...
      Dec 02 11:53:08 user echo[2495]: I am lib
      Dec 02 11:53:08 user echo[2496]: done   # <--- 執行添加的內容
      Dec 02 11:53:08 user systemd[1]: ttt.service: Deactivated successfully.
      Dec 02 11:53:08 user systemd[1]: Started My custom startup script.
      ```

- step4，建立用於覆蓋的 /etc/systemd/system/ttt.service
  - 填入以下
    ```shell
    [Unit]
    Description=My custom startup script

    [Service]

    ExecStart=echo I am etc

    [Install]
    WantedBy=default.target
    ```

  - 重新載入 daemon，`$ systemctl daemon-reload`

  - 測試
    - 重新執行 service，`$ systemctl start ttt.service`
    - 察看結果，`$ systemctl status ttt.service` 
      ```shell
      ○ ttt.service - My custom startup script
          Loaded: loaded (/etc/systemd/system/ttt.service; enabled; vendor preset: enabled) # <--- step1，改從 etc 載入配置
          Drop-In: /etc/systemd/system/ttt.service.d  # <--- step2，將 conf 的內容載入
                  └─ttt.conf
          Active: inactive (dead) since Fri 2022-12-02 12:10:03 UTC; 5s ago
          Process: 2760 ExecStart=echo I am etc (code=exited, status=0/SUCCESS) # <--- step3 etc 覆蓋的新內容
          Process: 2761 ExecStartPost=echo done (code=exited, status=0/SUCCESS) # <--- step4 conf 添加的內容
        Main PID: 2760 (code=exited, status=0/SUCCESS)
              CPU: 3ms

      Dec 02 12:10:03 user systemd[1]: Starting My custom startup script...
      Dec 02 12:10:03 user echo[2760]: I am etc
      Dec 02 12:10:03 user echo[2761]: done
      Dec 02 12:10:03 user systemd[1]: ttt.service: Deactivated successfully.
      Dec 02 12:10:03 user systemd[1]: Started My custom startup script.
      ```

## Ref
- [timer-unit的使用](https://linux.vbird.org/linux_basic/centos7/0560daemons.php#systemctl_timer) 