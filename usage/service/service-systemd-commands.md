## Systemd 常用命令

- 注意，systemd 不是一個命令，而是`一組命令的代稱`
  - systemctl: Systemd 的主命令，用於管理系統
  - systemd-analyze: 用於查看啟動耗時  
  - hostnamectl: 用於查看當前主機的信息
  - localectl: 用於查看本地化設置
  - timedatectl: 用於查看當前時區設置
  - loginctl: 用於查看當前登錄的用戶
  - journalctl: 查看所有日誌

## [systemctl] Systemd 的主命令，用於管理系統
- unit 相關
  - 列出正在運行的unit資源，`$ systemctl`，等同於 `$ systemctl list-units`
  - 列出所有的unit資源，`$ systemctl list-units -all`
  - 列出正在運行的unit資源(簡化版)，`$ systemctl list-unit-files`，只有 unitfile 和 state 兩欄
  - 列出正在運行的service-unit資源，`$ systemctl list-units --type=service`
  - 列出服務開啟的 socket `$ systemctl list-sockets`

  - 查看所有unit運行的狀態(包含service)，`$ systemctl status`，可查看service之間的進程關係
    
    詳見，[systemctl-status 的解讀](#systemctl-status-的解讀)

  - 查看指定unit的狀態，`$ systemctl status example.service`

- service 相關
  - 注意，以下命令只對 service-unit 有效，對 target-unit 無效
  - 啟動指定的service，`$ systemctl start example.service`
  - 停止指定的service，`$ systemctl stop example.service`
  - 重啟指定的service，`$ systemctl restart example.service`
  - 殺死指定service的所有子進程，`$ systemctl kill example.service`
  
  - 顯示指定service的配置，`$ systemctl show example.service`，顯示所有預設的配置欄位
  - 顯示指定service的配置文件內容，`$ systemctl cat example.service`，只顯示配置文件的內容
  - 顯示指定欄位，`$ systemctl show -p Slice example.service`
  - 變更指定欄位的值，`$ systemctl set-property example.service Slice=新值 `
  
  - 重新載入指定service的配置文件，`$ systemctl reload example.service`
  - 重新載入所有修改過的配置文件，`$ systemctl daemon-reload`

- target 相關
  - 注意，避免對 target-unit 使用 start|top|restart 等用於 service-unit 的指令
  - 查看 default.target 的指向，`$ systemctl get-default`
  - 修改 default.target 的指向，`$ systemctl set-default 新目標.target`

- isolate: 切換 target
  - 切換到圖形模式，`$ systemctl isolate graphical.target`，只保留 graphical.target
  - 切換到文字模式，`$ systemctl isolate multi-user.target`，只保留 multi-user.target
  - isolate 的簡化指令
    - `$ sudo systemctl poweroff`: 系統關閉
    - `$ sudo systemctl halt`: CPU停止工作
    - `$ sudo systemctl reboot`: 重新關機
    - `$ sudo systemctl suspend`: 進入暫停模式，將系統的狀態資料保存到記憶體中，然後關閉掉大部分的系統硬體
    - `$ sudo systemctl hibernate`: 進入休眠模式，將系統狀態保存到硬碟當中，保存完畢後，將電腦關機
    - `$ sudo systemctl rescue`: 進入救援模式
    - `$ sudo systemctl emergency`: 進入緊急救援模式

- 開關機自動啟動
  - 開機自動啟動，`$ systemctl enable example.service`
    - 注意，此命令會建立 `/etc/systemd/system` 與 `/lib/systemd/system` 之間的軟連結
    - 將 example.service 添加到 default.target (multi-user.target) 中
    - 建立 /etc/systemd/system/multi-user.target.wants/example.service 的軟連結

  - 停止開機自動啟動，`$ systemctl enable example.service`，`$ systemctl stop example.service`
    - 將 example.service 移出 default.target (multi-user.target) 中
    - 刪除 /etc/systemd/system/multi-user.target.wants/example.service 的軟連結
  
- 分析依賴關係
  - 查看所有unit的依賴關係，`$ systemctl list-dependencies`
  - 查看指定unit的依賴關係，`$ systemctl list-dependencies --all example.service`
  - 查看誰使用了指定的 unit，`$ systemctl list-dependencies example.service --reverse`

## [systemd-analyze] 用於查看啟動耗時
- 查看啟動耗時，`$ systemd-analyze`
- 查看每個服務的啟動耗時，`$ systemd-analyze blame`
- 以瀑布狀的方式啟動過程流，`$ systemd-analyze critical-chain`
- 顯示指定服務的啟動劉，`$ systemd-analyze critical-chain atd.service`

## [hostnamectl] 用於查看當前主機的信息
- 查看主機訊息，`$ hostnamectl`
- 設置主機名，`$ sudo hostnamectl set-hostname rhel7`

## [localectl] 用於查看本地化設置
- 查看本地畫設置，`$ localectl`
- 設置本地化參數
  - 例如，設置語言，`$ sudo localectl set-locale LANG=en_GB.utf8`
  - 例如，設置keymap，`$ sudo localectl set-keymap en_GB`

## [timedatectl] 用於查看當前時區設置

## [loginctl] 用於查看當前登錄的用戶  
- 列出當前 session，`$ loginctl list-sessions`，可查用戶sessionID
- 列出當前登入用戶，`$ loginctl list-users`，可查用戶ID

- 顯示指定用戶所有的相關訊息，`$ loginctl show-user 用戶名`
- 顯示指定使用者ID的詳細訊息，`$ loginctl show-session 用戶sessionID`
- 顯示session開啟的子進程狀態，`$ loginctl session-status 用戶sessionID`
- 顯示登入用戶的狀態，`$ loginctl user-status 用戶名`

- 關閉指定的 session，`$ loginctl terminate-session 用戶sessionID`
- 關閉指定的 用戶，`$ loginctl terminate-user 用戶名`
- 透過 kill-session 關閉指定的 session，`$ loginctl kill-session 用戶sessionID --signal=SIGSTOP`

## [journalctl] 查看所有日誌
- 統一管理所有Unit 的啟動日誌，透過 journalctl 命令可以查看所有日誌
- 日誌的配置文件，`/etc/systemd/journald.conf`

- 查看所有日誌，`$ sudo journalctl`，預設只保存本次啟動的日誌
- 查看kernel日誌，`$ sudo journalctl -k`
- 查看本次啟動(boot)的日誌，`$ sudo journalctl -b`
- 查看指定時間的日誌，`$ sudo journalctl --since yesterday`
- 查看最新日誌的最後20行，`$ sudo journalctl -n 20`
- 查看指定服務的日誌，`$ sudo journalctl /usr/lib/systemd/systemd`
- 查看指定PID的日誌，`$ sudo journalctl _PID=1`
- 查看指定腳本的日誌，`$ sudo journalctl /usr/bin/bash`
- 查看指定用戶的日誌，`$ sudo journalctl _UID=33 --since today`
- 查看某個資源(unit)的日誌，`$ sudo journalctl -u nginx.service --since today`


## systemctl status 的解讀
- 範例

  systemctl status 執行的結果如下
  ```shell
  # 執行
  $ systemctl status atd.service

  # 回應
  atd.service - Job spooling tools
    # 開機載入狀態
    Loaded: loaded (/usr/lib/systemd/system/atd.service; enabled) # enabled 為啟動狀態
    
    # 當前狀態
    Active: active (running) since Mon 2015-08-10 19:17:09 CST; 5h 42min ago

  Main PID: 1350 (atd)
    CGroup: /system.slice/atd.service
            └─1350 /usr/sbin/atd -f
  ```

- 開機載入狀態
  - `Loaded`: ... enabled，代表開機會啟動當前的 service
  - `Loaded`: ... disabled，代表開機不會啟動當前的 service

- 啟動狀態
  - `enable`  : 開機自動啟動
  - `disable` : 開機不會自動啟動
  - `static`  : 開機不可以自己啟動，必須由其他 service 啟動
  - `mask`    : 禁止啟動中，需要透過 `systemctl unmask` 回復啟動

- 當前狀態
  - Active: `inactive` (dead) since ...，代表　daemon 沒有在運作中
  - Active: active (`running`) since ...，代表　daemon 正在執行中
  - Active: active (`exited`) since ...，代表　daemon 僅執行一次就正常結束
  - Active: active (`waiting`) since ...，代表　daemon 正在執行中，但需要等待其他事件才能繼續

## Ref
- [關閉服務/禁用服務範例 @ 鳥哥](https://linux.vbird.org/linux_basic/centos7/0560daemons.php#systemctl_start)
