## 用戶服務 (user-unit)

- Service 依照作用的範圍，可分為以下兩種
  - 系統性服務(system-unit): `系統開機後就會自動啟動的服務`，所有的使用者都可使用的服務
    > 一般的服務都屬於此類

  - 使用者服務(user-unit): 只有在`使用者登入後`才啟用的服務，限制只有特定使用者可使用
    > 例如，sshd 開啟的 tty 屬於此類，每個使用者登入後都有屬於自己的 tty 應用程式，
  
- user-unit 的使用場景
  
  systemd會`為用戶生成一個 systemd --user 實例`，並且在`該用戶首次登錄`時，才將 systemd --user 實例化，
  用戶可以在這個實例下管理服務，可以啟動、停止、啟用、禁用他們自己的 unit

  注意，systemd --user 實例`不是根據 session 的數量而創建`，不是每次一個新的 session 連線後就建立一個新的 systemd --user 實例，
  只要用戶還有session進行連線中，systemd --user 實例的進程就不會退出，直到用戶所有的 session 都斷線退出後，systemd --user 實例的進程才會被銷毀

  範例，[需要用到 user-service 的範例，以 braodway 為例](#範例-需要使用-user-unit-的範例以-broadway-為例建立-broadway-的使用者服務)

- 所有用戶登出也不銷毀 systemd --user 的實例

  對於部分應用，希望即使所有用戶登出也不銷毀 systemd --user 的實例，

  需要對帳號設置用戶逗留(linger)的選項，例如，`$ loginctl enable-linger` 帳號名，
  使用戶能常駐在系統中，避免因所有用戶離線而刪除了 systemd --user 的實例

## user-unit 和 system-unit 的差異
- `差異1`，命令差異

  user-unit 的任何操作，都需要加上 `--user` 參數，例如，`$ system --user enable example.service`

- `差異2`，目錄差異
  - 軟體安裝1，`~/.local/share/systemd/user/`，適用於`沒有Home目錄的用戶`的軟體安裝目錄
  - 軟體安裝2，`/usr/lib/systemd/user/`，適用於`有Home目錄的用戶`的軟體安裝目錄
  - 系統目錄3，`/etc/systemd/user/`，由 root-user 維護，system-scope 的 user-unit目錄
  - 用戶目錄4，`~/.config/systemd/user/`，用戶自己建立的 user-unit目錄
    
    當用戶執行 `$ systemctl --user enable example.service` 後，
    
    會在` ~/.config/systemd/user/<target名>.wants` 目錄中建立軟連結，且軟連結會指向 `~/.config/systemd/user/example.service`

- `差異3`，default.target 的位置，`/usr/lib/systemd/user/default.target`
  
- `差異4`，執行環境的差異
  
  注意，systemctl --system 啟動的服務 和 systemctl --user 啟動的服務`運行在不同的進程中`，
  user-unit <font color=red>不應該依賴 system-unit 或是其他用戶的 user-unit</font>

## user-unit 存取環境變數
- 對於一般使用者登入後，os 會在執行 tty 之前，會先執行 `~/.bashrc`，在執行 tty 之前的環境初始化，

  注意，此處的環境變數是在 unitfile 內可見，僅限在 unitfile 內使用，登入的使用者無法透過 `echo $變數名` 進行存取

  因此，systemd --user 的實例`不會繼承 ~/.bashrc 內的環境變數`，必須透過以下幾種方式獲取環境變數

- `方法1`，直接寫在 unitfile 中，透過 `Environment選項` 或 `EnvironmentFile選項` 進行設置

- `方法2`，在 `~/.config/environment.d 目錄`中，建立 *.conf 的檔案
  
  以 KEY=VALUE 的形式添加環境變數，不需要符合 unitfile 的格式

- `方法3`，建立 `~/.config/systemd/user/unit名.service.d/*.conf`，將環境變數動態添加到用戶服務中
  
  - (option1) 手動建立 conf 檔

  - (option2) 透過命令 `$ systemctl --user edit <service名不需要副檔名>` 快速建立 conf 檔

  - 注意
    - option1 和 option2 會在相同的位置建立 conf 檔
    - 此方法會覆蓋方法1建立的同名環境變數
    - 使用此方法需要`符合 unitfile 的寫法`，將環境變數添加到 Environment 欄位中
  
  - 範例
    - 此方法類似 system-unit 的 [添加 service 的配置](service-systemd-unitfile.md#範例-添加-service-的配置只能添加不能覆蓋原始欄位更新軟體時檔案不會被覆蓋)，將環境變數改寫在 conf 檔案中
    - 完整範例，見 [利用 systemctl --user edit 添加環境變數 (快速建立 conf 檔案)](#範例-利用-systemctl---user-edit-添加環境變數-快速建立-conf-檔案)

- `方法4`，透過命令添加，且只對執行後啟動的用戶有效，對已經啟動的 systemd --user 實例無效
  - 適用場景: 用於憑證或機密的環境變數不適合寫在文件中，可以透過命令進行添加
  - 透過 `$ systemctl --user set-environment` 命令 或 `$ systemctl --user import-environment` 命令，進行設置
  - 透過 `$ systemctl --user ( set-environment | unset-environment | import-environment )` 設置的環境變數的有效期
    - 在主機 reboot 之後才會失效，
    - `$ systemctl --user (daemon-reexec | daemon-reload)` 的命令，都不會使設置的環境變數失效

- 優先權: 方法4 > 方法3 > 方法2 > 方法1

- 測試方法: 
  - 方法1，透過 `$ systemctl --user show-environment` 查看
    
    靜態方法，從已知的檔案載入環境變數值，

    與實際環境變數不一定準確，無法反映 conf 動態覆寫環境變數的結果

  - 方法2，在unitfile中，透過 `ExecStart=echo $變數名` 進行測試

    動態方法，與實際環境變數一致

## [範例] 建立用戶服務(user-unit)
- step1，在 `~/.config/systemd/user` 目錄下建立配置檔，例如，`$ touch ~/.config/systemd/user/ttt.service`

- step2，ttt.service 的內容
  ```shell
  [Unit]
  Description=My custom startup script

  [Service]

  ExecStart=echo i am ttt

  [Install]
  WantedBy=default.target
  ```

- step3，添加到 user-unit(啟用 ttt.service)，例如，`$ systemctl --user enable ttt.service`
  - 注意，unitfile檔案(ttt.service)必須存在於 /home/user/.config/systemd/user目錄中，此命令才有效
  
  - 注意，此命令會在 /home/user/.config/systemd/user/<target名>.wants 目錄中，
    建立指向 /home/user/.config/systemd/user/ttt.service 的軟連結，軟連結與 unit同名，都是 ttt.service

- 測試
  - `$ exit`，退出 ssh 登入

  - 重新登入後，馬上執行 `$ systemctl --user status ttt.service` 得到
    ```shell
    Active: inactive (dead) since Sat 2022-12-03 13:52:10 UTC; 3s ago   # 使用者第一次登入，user-unit 重新執行
    ```

  - 等待一分鐘後，建立新的 ssh 登入(舊的登入不登出)，並執行 `$ systemctl --user status ttt.service` 得到
    ```shell
    Active: inactive (dead) since Sat 2022-12-03 13:52:10 UTC; 2min 15s ago   # 若使用者已登入，user-unit 不會重新執行
    ```

  - 將兩個登入都斷線後重新登入，並執行 `$ systemctl --user status ttt.service` 得到
    ```shell
    Active: inactive (dead) since Sat 2022-12-03 13:56:34 UTC; 1s ago # 使用者第一次登入，user-unit 重新執行
    ```

## [範例] 利用 systemctl --user edit 添加環境變數 (快速建立 conf 檔案)

- 注意，`$ systemctl --user edit` 是快速建立 conf 的命令，但不是必需的，
  可以手動添加 conf 檔，且可以自定義檔案名

- step1，在 `~/.config/systemd/user` 目錄下建立配置檔，例如，`$ touch ~/.config/systemd/user/ttt.service`

- step2，ttt.service 的內容
  ```shell
  [Unit]
  Description=My custom startup script

  [Service]
  ExecStart=echo i am $myname

  [Install]
  WantedBy=default.target
  ```

- step3，添加到 user-unit(啟用 ttt.service)，例如，`$ systemctl --user enable ttt.service`
  - 注意，unitfile檔案(ttt.service)必須存在於 /home/user/.config/systemd/user目錄中，此命令才有效
  
  - 注意，此命令會在 /home/user/.config/systemd/user/<target名>.wants 目錄中，
    建立指向 /home/user/.config/systemd/user/ttt.service 的軟連結，軟連結與 unit同名，都是 ttt.service

- step4，執行 `$ systemctl --user edit ttt`，建立 conf 檔案
  - 此命令會自動建立 /home/user/.config/systemd/user/bd.service.d/override.conf
  - conf 檔案中添加以下內容
    ```shell
    [Service]
    Environment="myname=aa"
    ```

- step5，`$ systemctl --user start ttt.service`，重新執行服務，得到
  ```shell
  Dec 03 14:06:40 user echo[4114]: i am aa
  ```

## [範例] 需要使用 user-unit 的範例，以 broadway 為例，建立 broadway 的使用者服務

- 為什麼 braodway 這麼特別

  broadway 是遠端GUI的軟體，
  
  在遠端電腦安裝 broadway 後，使用者可以透過 ssh 連線後，並執行 GUI 的應用後，
  在本機上可透過瀏覽器檢視遠端主機的畫面
  
  `本機使用者 <--> SSH bash <--> broadway <--> IPC <--> GUI 應用` 

  - step1，在遠端主機上需要在背景執行 broadway-server
  - step2，當執行GUI後，GUI的畫面會透過 IPC 與 broadway-server 進行通訊
  - step3，broadway-server 將輸入的畫面透過 http-server 供外界存取

  在此範例中，GUI 應用會透過 IPC 與 broadway 進行通訊

- `broadway-server + system-service + GUI 應用` 的問題

  GUI 應用透過 IPC 和 broadway-server 進行通訊，因此，GUI 和 broadway-server 必須處於`同一個作用域`，通訊才會有效，

  broadway-server 是透過 `system-service` 啟動，

  GUI 是透過 ssh 啟動的 bash 執行的，而啟動 ssh-bash 的服務`不屬於 system-service`，而是屬於 `user-service`，
  兩種不同的 service 屬於不同的systemd實例(屬於不同的進程)，造成 GUI 無法直接與 broadway-server 進行通訊，

  <font color=red>  透過 system-service 啟動的服務，也無法透過 unitfile 中的 Slice欄位、User欄位、Group欄位，切換到 user-service 的進程
</font>

- Trouble-shooting
  
  - <font color=blue>不將braodway添加到system-service，且和GUI應用在同一個bash中執行時，是正常運行的</font>

    此時 ssh-bash 和 broadway 是在同一個存取權限(scope)

    ```shell
    ├─user.slice
    │ └─user-1000.slice
    │   ├─user@1000.service
    │   │ ├─app.slice
    │   │ │ ├─at-spi-dbus-bus.service
    │   │ │ │ ├─1048 /usr/libexec/at-spi-bus-launcher
    │   │ │ │ ├─1054 /usr/bin/dbus-daemon --config-file=/usr/share/defaults/at-spi2/accessibility.conf --nofork --print-addre>
    │   │ │ │ └─1082 /usr/libexec/at-spi2-registryd --use-gnome-session
    │   │ │ └─dbus.service
    │   │ │   └─1046 /usr/bin/dbus-daemon --session --address=systemd: --nofork --nopidfile --systemd-activation --syslog-only
    │   │ └─init.scope
    │   │   ├─879 /lib/systemd/systemd --user
    │   │   └─880 (sd-pam)
    │   └─session-1.scope
    │     ├─ 876 sshd: user [priv]    # <----- ssh-bash
    │     ├─ 969 sshd: user@pts/0     
    │     ├─ 970 -zsh
    │     ├─1014 broadwayd :5         # <---- broadway
    │     ├─1286 systemctl status
    │     └─1287 pager
    ```

  - <font color=blue>添加到 system-service 後，braodway 和 bash 屬於不同的進程</font>
    ```shell
    ├─user.slice
    │ └─user-1000.slice
    │   ├─user@1000.service
    │   │ └─init.scope
    │   │   ├─1643 /lib/systemd/systemd --user
    │   │   └─1645 (sd-pam)
    │   └─session-12.scope
    │     ├─1640 sshd: user [priv]
    │     ├─1746 sshd: user@pts/0
    │     ├─1747 -bash                # <-- ssh-bash 屬於 user-service
    │     ├─2541 systemctl status
    │     └─2542 pager
    └─system.slice
        ├─broadwayd.service          # <-- braodway 屬於 system-service 
        │ └─2031 /usr/bin/broadwayd :5 &
    ```

  - <font color=blue>即使將 broadway 和 ssh 切換到同一個 slice ，仍然無法開啟</font>
    ```shell
    CGroup: /
            ├─user.slice
            │ └─user-1000.slice         # <--------- at same slice
            │   ├─user@1000.service 
            │   │ └─init.scope
            │   │   ├─901 /lib/systemd/systemd --user
            │   │   └─902 (sd-pam)
            │   ├─broadwayd.service     # <--------- broadway
            │   │ └─1032 /usr/bin/broadwayd :5 &
            │   └─session-1.scope       # <--------- user ssh
            │     ├─ 898 sshd: user [priv]
            │     ├─1004 sshd: user@pts/0
            │     ├─1005 -bash
            │     ├─1036 systemctl status
            │     └─1037 pager
    ```
  
  - <font color=blue>無法開啟的原因，由於不同進程，造成用於IPC通訊的socket檔，開啟的位置不同</font>
    ```shell
    # 手動啟動(正確)
    Listening on /run/user/1000/broadway6.socket    # <-- 啟動在 /run/user/1000

    # 透過 service 啟動在 system.slice(錯誤)
    Dec 01 15:14:16 user systemd[1]: Started broadwayd-service.
    Dec 01 15:14:16 user broadwayd[1712]: Listening on /root/.cache/broadway6.socket    # <-- 啟動在 /root/.cache
    
    # 透過 service 啟動在 user-1000.slice(錯誤)
    Dec 01 15:14:16 user systemd[1]: Started broadwayd-service.
    Dec 01 15:14:16 user broadwayd[1712]: Listening on /root/.cache/broadway6.socket    # <-- 啟動在 /root/.cache

    # 在 service-unit 中添加 User欄位後(錯誤)
    Dec 01 15:39:30 user systemd[1]: Started broadwayd-service.
    Dec 01 15:39:30 user broadwayd[1333]: Listening on /home/user/.cache/broadway6.socket   # <-- 啟動在 /home/user/
    ```

- 解決方法，將 broadway 添加到 user-service，不要添加到 system-service
  
  完整範例，見 [建立 broadway 的使用者服務](../sw/broadway/install_broadway.sh)

## Ref
- 理論
  - [systemd/User](https://wiki.archlinux.org/title/Systemd/User)
  - [systemd/User 中文](https://wiki.archlinux.org/title/Systemd_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)/User_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87))
  - [User-manager startup flow](https://www.freedesktop.org/software/systemd/man/bootup.html#User%20manager%20startup)

- 範例
  - [x11 as a systemd user service](https://wiki.archlinux.org/title/Systemd_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)/User_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87))

  - [python http server as user-service](https://www.unixsysadmin.com/systemd-user-services/)
  
- 其他
  - [systemd user services](https://www.unixsysadmin.com/systemd-user-units/)
  - [Access to the display when loading the systemd service](https://bbs.archlinux.org/viewtopic.php?id=276336)
  - [bash可執行，但 service 無法執行](https://superuser.com/questions/1487204/bash-script-as-linux-service-wont-run-but-executed-from-terminal-works-perfect)