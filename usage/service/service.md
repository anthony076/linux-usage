## Service 基本概念

- Service 歷史
  SysV(init腳本) -> Systemd(service配置文件)

  <font color=blue>SysV 的缺點</font>
  - 啟動時間長: init腳本是串行啟動服務，只有前一個進程啟動完，才會啟動下一個進程
  - 啟動腳本複雜: init只是執行啟動腳本，不管其他事情。腳本需要自己處理各種情況，使得腳本的內容很長

  <font color=blue>Systemd 的優點</font>
  - 按需啟動進程，減少系統資源消耗
  - 并行啟動進程，提高系統啟動速度
    - Ubuntu 的 Upstart 解決了沒有直接依賴的啟動之間的并行啟動
    - Systemd 通過 Socket 緩存、DBus 緩存和建立臨時掛載點等方法進一步解決了啟動進程之間的依賴，做到了`所有系統服務併發啟動`
    - 對於用戶自定義的服務，Systemd `允許配置其啟動依賴項目`，從而確保`服務按必要的順序運行`

  - 使用 CGroup 監視和管理進程的生命周期，避免父子關係的進程脫離追蹤
  - 統一管理服務日誌
  - 支持快照和系統恢復

- SysV 和 Systemd 是部分相容的
  - systemd 是可以相容於 init 的啟動腳本的
    
    舊的 init 啟動腳本也能夠透過 systemd 來管理，只是更進階的 systemd 功能就沒有辦法支援就是了

  - runlevel 只能部分對應

    大概僅有 runlevel 1, 3, 5 有對應到 systemd 的某些 target 類型而已，沒有全部對應

## SysV
- SysV 的`init 腳本`，是系統核心第一支呼叫的程式， init 去喚起所有的系統所需要的服務 (本機服務或網路服務)

- 所有的服務啟動腳本通通放置於`/etc/init.d/`，
  
  所有的腳本都是使用 bash shell script 所寫成的腳本程式，並透過以下方式對要啟動的服務進行管理
  - 啟動：/etc/init.d/daemon start
  - 關閉：/etc/init.d/daemon stop
  - 重新啟動：/etc/init.d/daemon restart
  - 狀態觀察：/etc/init.d/daemon status

- [Linux 初始化系统 SystemV Upstart](https://www.cnblogs.com/usmile/p/13065566.html)

## Systemmd 

- Systemd 架構
  <img src="doc/service/systemd-related-component.png" width=800 height=auto>

- systemd 不是一個命令，而是`一組命令的代稱`，詳見，[常用命令](#命令-systemd-命令集)
  
  systemd 自身也是一個service，依賴 systemctl 應用程式來管理其他服務，
  除了 systemctl 的命令外，systemd 也提供其他的命令工具來輔助 systemd 執行

- 名詞解釋
  - Service 和 Daemon

    service 是作業系統透過`服務管理程式(SysV 或 systemd)`啟動的應用程式，

    每一個service都會對應一個不同的應用程式，每個實現service功能的應用程式，都會有自己的應用程式名，
    在 systemd 中將這些提供 service 的應用程式，統稱為守護程式(daemon)

    簡單說，service == daemon 

    systemd 的 d 代表 daemon ，即用於管理服務的守護進程，systemd 用於取代 initd，
    使得 systemd 成為系統的第一個進程，其他服務進程都是 systemd 的子進程 (都是由systemd啟動的)

  - Unit: 資源的代稱，Service 也是資源的其中一種
  - Journal: 日誌
  - CGroup: 
    - 在 systemd 之前，都是透過進程樹來管理不同進程之間的繼承關係，透過兩次 fork 的方法使進程脫離追蹤
    - 使用 system 之後，透過 CGroup 跟蹤進程關係，`避免繼承關係脫離追蹤`
    - 除了避免繼承關係脫離追蹤，`可實現訪問的隔離`，能更精確管理服務的生命週期

- systemd 除了用於管理service外，也用於配置service`需要的所有資源`
  - `service -> unit -> unit-type -> unit-file `
  - systemd 將所有的 service 進行分類管理，每一個 service 稱為一個服務實體的單位(unit)，
  - 每一個 unit 都有所屬的分類(type)，例如，mount-unit，指負責 mount 類型的 service，負責檔案系統的掛載
  - 每一種類型的 unit 都會有對應的[配置文件](service-systemd-unitfile.md)，稱為 unit-file，
    
    不同類型的 unit-file 以`副檔名進行區分`，例如，Device-Unit 使用 .device 的結尾作為配置文件，
    在 unitfile 中，會描述啟動此服務的相關配置，並告訴 systemd `如何啟動此服務`或`如何存取資源`

  - service 可區分為以下 12 種 unit-type

    | 資源名         | 用途                                                                                | 配置文件名 |
    | -------------- | ----------------------------------------------------------------------------------- | ---------- |
    | Service-Unit   | 一般性系統服務                                                                      | .service   |
    | Target-Unit    | unit 的集合，定義target信息及依賴關係                                               | .target    |
    | Device-Unit    | 硬體資源服務，主要用於定義設備之間的依賴關係                                        | .device    |
    | Mount-Unit     | 檔案系統掛載相關的服務，可以取代過去的 /etc/fstab 配置文件                          | .mount     |
    | Automount-Unit | 檔案系統自動掛載相關的服務，相當於 SysV-init 的 autofs 服務                         | .automount |
    | Path-Unit      | 偵測特定檔案或目錄的變化，並觸發其它 Unit 運行，例如最常見的列印服務                | .path      |
    | Scope-Unit     | 這種 Unit 文件不是用戶創建的，而是 Systemd 運行時產生的，描述一些系統服務的分組信息 | .scope     |
    | Slice-Unit     | 進程組，用於表示一個 CGroup 的樹                                                    | .slice     |
    | Snapshot-Unit  | Systemd 的快照，用於不同環境的切換                                                  | .snapshot  |
    | Socket-Unit    | 內部程序資料交換的socket服務，主要是 IPC 的傳輸訊息插槽檔 (socket file) 功能        | .socket    |
    | Swap-Unit      | swap 文件，虛擬內存的交換分區                                                       | .swap      |
    | Timer-Unit     | 定時器，用於配置在特定時間觸發的任務，替代了 Crontab 的功能                         | .timer     |

- `service命令`是 `systemctl命令` 的簡化版，service命令會重定向到 systemctl 命令

- Service 依照作用的範圍，可分為以下兩種
  - 系統性服務(system-service): 系統開機後就會自動啟動的服務，所有的使用者都可使用的服務
    > 一般的服務都屬於此類

  - 使用者服務(user-service): 只有在`使用者登入後`才啟用的服務，限制只有特定使用者可使用
    - 例如，sshd 開啟的 tty 屬於此類，每個使用者登入後都有屬於自己的 tty 應用程式
    - 使用方式參考，[使用者服務的使用](user-service.md)

- Systemd 的啟動過程
  - step1，讀取 /boot 目錄下的內核文件(kernel)
  - step2，執行第一個程序 /sbin/init 初始化進程，由 systemd 初始化系統引導，完成相關的初始化工作
  - step3，systemd 執行 default.target 指向的 target，並執行該 target下所有的配置文件
  - step4，根據配置文件中的配置，依序執行相關的 target
  - step5，繼續執行 /etc/systemd/system 內的其他配置文件

- [配置文件的使用](service-systemd-unitfile.md)
- [systemctl命令的使用](service-systemd-commands.md)
- [使用者服務的使用](user-service.md)

## 建立 service 的其他選擇

- 為什麼需要其他選擇
  
  systemd 會占用系統的第一個程式，因此不是所有的 linux 的發行版或容器會使用 systemd

- `選擇1`，使用 [supervisor](https://philipzheng.gitbook.io/docker_practice/cases/supervisor) ，常用於 docker

- `選擇2`，使用 [Snap] 套件管理庫，適用於 Ubuntu
  - 缺點: Snap 套件庫收錄的軟件以新的套件為主，舊版的套件可能會`未收錄`
  - 缺點: 僅限於 Snap 安裝的套件
  
  - 範例，[以 novnc 套件為例](../sw/novnc/readme.md#範例-vagrant--ubuntu--slime--lxde--tightvnc)

## 其他常用的 service

- 開機相關的 service
  - step1，`local-fs.target` + `swap.target`： 主要在掛載本機 /etc/fstab 裡面所規範的檔案系統與相關的記憶體置換空間

  - step2，`sysinit.target`： 初始化系統，主要在偵測硬體，載入所需要的核心模組等動作
    
    - 基本的核心功能、檔案系統、檔案系統裝置的驅動等等，都在此服務處理，包含以下服務
    - 載入額外的核心模組：透過 /etc/modules-load.d/*.conf 檔案的設定，讓核心額外載入管理員所需要的核心模組，重要
    - 載入額外的核心參數設定：包括 /etc/sysctl.conf 以及 /etc/sysctl.d/*.conf 內部設定，重要
    - 啟動動態裝置管理員：就是 udevd，用在動態對應實際裝置存取與裝置檔名對應的一個服務，重要
    - 特殊檔案系統裝置的掛載：包括 dev-hugepages.mount dev-mqueue.mount 等掛載服務，
    - 特殊檔案系統的啟用：包括磁碟陣列、網路磁碟 (iscsi)、LVM 檔案系統、檔案系統對照服務 (multipath) 等等，
    - 開機過程的訊息傳遞與動畫執行：使用 plymouthd 服務搭配 plymouth 指令來傳遞動畫與訊息
    - 日誌式登錄檔的使用：就是 systemd-journald 這個服務的啟用啊！
    - 啟動系統的亂數產生器：亂數產生器可以幫助系統進行一些密碼加密演算的功能
    - 設定終端機 (console) 字形

  - step3，`basic.target`： 準備系統，載入主要的週邊硬體驅動程式與防火牆相關任務
    - 載入 alsa 音效驅動程式： 這個 alsa 是個音效相關的驅動程式
    - 載入 firewalld 防火牆： CentOS 7.x 以後使用 firewalld 取代 iptables 的防火牆設定
    - 載入 CPU 的微指令功能
    - 啟動與設定 SELinux 的安全本文
    - 將目前的開機過程所產生的開機資訊寫入到 /var/log/dmesg 當中
    - 由 /etc/sysconfig/modules/*.modules 及 /etc/rc.modules 載入管理員指定的模組！
    - 載入 systemd 支援的 timer 功能；

  - step4，`default.target`: 執行 multi-user.target 或 graphical.target，一般系統或網路服務的載入
    - `multi-user.target`: 一般系統或網路服務的載入，並提供 tty 介面的登入服務
    - ` graphical.target`: 一般系統或網路服務的載入，並提供 GUI 介面的登入服務

  - step5，`gdm.service`: 圖形界面相關服務如  等其他服務的載入

- `rc-local.service`: 提供開機自動啟動的服務
  - 在 systemV 的時代，
  
    要讓系統額外執行某些程式的話，可以將該程式指令或腳本的絕對路徑名稱寫入到 /etc/rc.d/rc.local 的檔案中

  - 舊版的 systemd

    直接寫一個 systemd 的啟動腳本設定檔到 /etc/systemd/system 底下，然後使用 systemctl enable 的方式來設定啟用它，
    就不需要直接使用 rc.local 的檔案

  - 新版的 systemd
    
    預設提供 rc-local.service 這個服務，這個服務不需要啟動，它會自己判斷 /etc/rc.d/rc.local 是否具有可執行的權限來判斷要不要啟動這個服務

    透過 `$ chmod a+x /etc/rc.d/rc.local` 的步驟，需要開機啟動的腳本就可以放在 /etc/rc.d/rc.local 這個檔案內， 系統在每次開機都會去執行這檔案內的指令

## Ref
- 理論
  - [鳥哥第十七章、認識系統服務 (daemons)](https://linux.vbird.org/linux_basic/centos7/0560daemons.php)
  - [Linux Systemd 詳細介紹: Unit、Unit File、Systemctl、Target](https://www.cnblogs.com/usmile/p/13065594.html)
  - [systemd](https://github.com/chuanchuan11/systemd/tree/e8c762a2685a4e6899d8ec6ff3a05bc6e6606851)
  - [Systemd 入门教程-命令篇@阮一峰](https://www.ruanyifeng.com/blog/2016/03/systemd-tutorial-commands.html)
  - [Systemd 入门教程-實戰篇@阮一峰](https://www.ruanyifeng.com/blog/2016/03/systemd-tutorial-part-two.html)
  - [system入門教程](https://fengbo4213.github.io/2018/08/18/%E5%85%B6%E5%AE%83%E4%B9%8BLinux%E4%BD%BF%E7%94%A8/)
  - [Understanding Systemd Units and Unit Files](https://www.digitalocean.com/community/tutorials/understanding-systemd-units-and-unit-files)
  - [Systemd系列文章](https://systemd-book.junmajinlong.com/systemd_bootup.html)

- 範例
  - [讓 Linux 的 systemd 幫我們管理 API 程式](https://ithelp.ithome.com.tw/articles/10262738)
  - [指定 service 啟動的身分和群組](https://www.golinuxcloud.com/run-systemd-service-specific-user-group-linux/)
  - [conf 範例](https://www.cnblogs.com/usmile/p/13065594.html)