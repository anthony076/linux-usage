## 基本概念

- shell 是一個統稱，泛指所有透過命令行執行腳本的終端程式，
  每個終端程式的語法相近，但可能有使用上的差異，

  例如，bash 和 fish 都有 set 命令，但兩者的語法略有差異

- 每個 shell 預設的配置文件不同
  - 配置文件用於 shell 啟動前的初始化腳本，可以用於配置環境變數或執行自定義的命令
  - `ash`: ~/.profile，輕量的shell
  - `sh`: ~/.profile，Unix 系統預設的 shell
  - `bash`: ~/.bash
  - `fish`: ~/.config/fish/config.fish
  - `zsh`: ~/zshrc

- 讓 shell script 可直接執行，不需要指定 shell程式的名稱
  - 未修改前，`$ bash test.sh`
  - step1，在開頭添加 `#!/bin/bash`，以 bash 開啟
  - step2，變更 shell-script 的權限為可執行，`$ chmod +x test.sh`
  - 修改後，`$ ./test.sh`

- script 中的預設變數

  - `$#` 輸入參數的數量
  - `$?` 表示上一個指令的執行結果
  - `$0` 代表當前script的完整
  - `$n` 
    - 取得參數值，$0，檔名或命令名，$1，第一個參數值，$2，第二個參數值
    - 例如，`$ ./test.sh aa bb cc`
      - $0 = ./test.sh
      - $1 = aa 
      - $2 = bb
      - $3 = cc
  - `$@` 取得參數列表，例如，`$1 $2 $3 ....`
  - `$*` 將參數列表變成單一字串，例如，`"$1 $2 $3 ...."`


- 變更預設的 shell，`$ chsh -s <shell路徑> 帳號`，或直接修改/etc/passwd，例如，`$ vi /etc/passwd`
  
- 每個 shell command 都有自己的執行空間

  export 環境變數 只針對當前的 bash 環境有效，
  bash 關閉後就會失效
  
  bashrc 是 bash 執行前預先載入的命令，
  每個shell 開啟前都會執行一次 .bashrc 中的命令

- `eval`、`exec`、`.`，三種方式執行bash命令的區別
  - 子shell和子進程的差別
    - 在shell中執行的命令，不一定都會建立子進程
    - 建立的子進程不一定都是shell
    - 但是子shell一定是一個子進程，且該子進程還是一個shell
  
  - eval 會根據評估命令的字串，若該命令包含子命令時，eval 會建立子shell，並將最後的結果回傳給當前的shell
    - 不會建立子shell的範例，`$ eval "echo 123"`
    - 會建立子shell的範例，`$ eval "echo $(ls)"`

  - exec 會建立新進程來執行命令，並取代當前的 shell 進程
    - exec 不會建立子進程，因此也不會建立子shell，而是將當前進程的控制權轉移給新命令
    - exec 執行新命令，原本的 shell 就被關閉，但是`進程還是同一個`
    - 因為 exec 不會建立子shell，無法執行有管道、重定向等功能

  - `. bash檔路徑`，會在當前的 shell 環境下執行 bash檔的內容，不會建立新的進程，例如，
    ```shell
    # /etc/xrdp/startwm.sh
    (內容省略)
    
    # 透過 . 執行 xinitrc 的內容
    . /etc/X11/xinit/xinitrc

    (內容省略)
    ```
  
- [設置環境變數要注意執行的環境](desktop/desktop.md#遠端桌面和本機桌面設置環境變數的差異)
  
- 部分命令會限制執行身分
  
  可以透過 `su - 使用者名稱 -c "命令"` 指定使用該使用者執行，而不需要切換身分
  
  或者透過 `sudo` 強制使用 root 身分執行

- 取得命令執行的 status-code
  - 方法1，間接方法，`$ echo 123 || echo fail`，若執行失敗會印出 fail
  - 方法2，透過 $?
    ```shell
    echo 123
    echo $?   # 印出 0
    ```

## 幾種背景執行的方式
- [How to Run Linux Commands in the Background](https://www.makeuseof.com/run-linux-commands-in-background/)

- [幾種背景執行的方式，以 web-server 為例](https://www.ruanyifeng.com/blog/2016/02/linux-daemon.html)

## 幾種開機自動啟動
- [Three ways to create a startup script in Ubuntu](https://transang.me/three-ways-to-create-a-startup-script-in-ubuntu/)

## 變量相關操作 : 設置變量 | 設置環境變量 | 條件式設置變量 | 刪除變量中的特定字串

- 設置變數，`變量名=值`，不需要透過 set 設置變量

- 透過 export 設置環境變量
  - `AA=123`，只存在當前的 shell-scritp 中
  - `export AA=123`，在`當前的 shell-scritp`中可用，且`在子shell或其他進程中`可用

- 範例，條件式設置變量，`${變數:+回傳值}` 或 `${變數:-回傳值}`

  - 語法1，`${環境變數名:+回傳值}`，判斷環境變數是否存在，+ 代表若環境變數存在，${} 會返回後方的回傳值給變數

  - 語法2，`${環境變數名:-回傳值}`，判斷環境變數是否存在，- 代表若環境變數不存在，${} 會返回後方的回傳值變數
  
  - 注意，若回傳值傳遞給其他環境變數，可用於設置環境變數值
    > 例如，AA=${BB:-123}，判斷變數BB是否存在，若變數BB不存在，返回 123 給變數AA

  - 注意，若回傳值不傳遞給其他環境變數，會作為命令來執行
    > 例如， ${BB:-whoami}，判斷環境變數BB是否存在，若環境變數BB不存在，執行 whoami 的命令

  - `範例1`，若環境變數不存在

    尋找`環境變數BB`是否存在，`若不存在`，回傳 `123` 給`環境變數AA`

    ```shell
    # 
    AA=${BB:-123}
    ^^^             要設置的變數名，AA
         ^^^        要尋找的變數，BB
            ^       若尋找的變數不存在，-
             ^^^    返回值123給變數AA 
    ```

  - `範例2`，若環境變數存在

    尋找`環境變數BB`是否存在，`若存在`，回傳 `456` 給`環境變數AA`

    ```shell
    
    AA=${BB:+456}
    ^^^             要設置的環境變數名，AA
         ^^^        要尋找的環境變數，BB
            ^       若尋找的環境變數存在，+
             ^^^    返回值，456
    ```

  - `範例3`，根據A環境變數是否存在的結果，設置B環境變數的值
    ```shell
    CC=${DD:+exist}
    ```

  - `範例4`，從 passwd 取得家用目錄的路徑，並設置為 HOME 變數
    ```shell
    HOME="${HOME:-$(getent passwd $USER 2>/dev/null | cut -d: -f6)}"
    ```

- 範例，判斷變數是否存在，若參數不存在就拋出錯誤，`變數="${1:?}"`
  ```shell
  hello(){
    # ? 代表對 $1 參數判斷是否存在，
    #   若存在，將 $1 傳遞給 name
    #   若不存在，拋出錯誤
    name="${1:?}"
    echo "${name}"
  }

  hello "ching"   # print ching
  hello           # throw error
  ```

- 範例，刪除變數中的特定字串，`${變數##要刪除字串}`

  ```shell
  foo=/dev/sda1
  echo $foo    # 得到 /dev/sda1
  echo ${foo##/dev/}   # 刪除 foo 變數中的 /dev/ 字串，得到 sda1
  ```

## [語法] set 用於設置(位置變數)或(script行為)，不是拿來設置變量

- 語法，利用set設置script的行為
  - `$ set -e`，如果腳本中的任何命令在執行過程中失敗，則整個腳本都將立即停止執行
  - `$ set -x`，將每個命令在執行之前顯示出來
  - `$ set -u`，使用未定義的變數時結束腳本執行，可以避免因為變敄沒有設定而導致的錯誤
  - `$ set -o 選項`，設置其他選項 (option)
    - `$ set -o errexit `，等效於 set -e
    - `$ set -o xtrace`，等效於 set -x
    - `$ set -o nounset`，等效於 set -u
    - `$ set -o pipefail`，任何管道中的命令返回錯誤碼，則整個管道的狀態碼將返回錯誤碼
    - `$ set -o allexport`，將所有定義的變數設為 export
    - `$ set -o braceexpand`，啟用大括號展開，例如 echo {A,B} 會顯示 A B
    - `$ set -o emacs`，使用 emacs 風格的命令行編輯
    - `$ set -o vi`，使用 vi 風格的命令行編輯

- 語法，設置位置變數，`set 值1 值2 ...`，危險的語法
  - 得到 $1=值1，$2=值2 的結果，
  - 若值是以-開頭，容易發生非預期的結果，例如，`set -e=123`，執行後，得到 $1=null

- 語法，設置位置變數，`set -- 值1 值2 ...`，安全的語法
  - 得到 $1=值1，$2=值2 的結果，
  - `--` 代表後面全部都是值，不會出現 set 選項，
  - 執行 `set -- -e=123` 後，得到 $1="-e=123"

- 範例，`set -- $(echo aa=1 bb=2)`，得到 $1="aa=1"，$2="bb=2"，正確
- 範例，`set -- aa=1 bb=2`，得到 $1="aa=1"，$2="bb=2"，正確
- 範例，`set -- "aa=1 bb=2"`，得到 $1=aa=1 bb=2，$2=null，錯誤

## [語法] test 判斷式語法 ( [] )，可用於 shell

- [ ] 是 test 的語法糖，[ 或 ] 與參數需要距離一個空白

  以下兩種寫法是等效的
  - test -e /123
  - [-e /123]  

- 語法1，對`檔案名`的判斷
  
  `test 檔案標記 檔案` 或 `[檔案標記 檔案]`

  - 檔案標記
    - `-e` 檔案是否存在(isExist)
    - `-d` 檔案是否為目錄(isDirectory)
    - `-f` 檔案是否為檔案(isFile)
    - `-r` 檔案是否可讀(isReadable)
    - `-n` 檔案內容或變數是否不為空 non-empty|not-null

    - `-a` 串接多個條件式，相當於 and
    - `-o` 串接多個條件式，相當於 or

    - `-b` 檔案是否為 block-device
    - `-c` 檔案是否為 character-device
    - `-S` 檔案是否為 Socket 檔案
    - `-p` 檔案是否為 FIFO (pipe) 檔案
    - `-L` 檔案是否為 連結檔
  
- 語法2，對`權限`的判斷
  
  `test 權限標記 檔案` 或 `[權限標記 檔案]`

  - 權限標記
    - `-r` 檔案是否有可讀屬性
    - `-w` 檔案是否有可寫屬性
    - `-x` 檔案是否有可執行屬性
    - `-u` 檔案是否有SUID屬性
    - `-g` 檔案是否有SGID屬性
    - `-k` 檔案是否有Sticky-bit屬性
    - `-s` 檔案是否為非空白檔案

- 語法3，檔案之間的比較
  
  `test 檔案1 檔案比較標記 檔案2` 或 `[檔案1 檔案比較標記 檔案2]`

  - 檔案比較標記
    - `-nt` 檔案1是否比檔案2更新(newer-than)
    - `-ot` 檔案1是否比檔案2更舊(older-than)
    - `-ef` 檔案1和檔案2是否是同一個檔案(equal-file)

- 語法4，數值之間的比較
  
  `test 數值1 數值比較標記 數值2` 或 `[數值1 數值比較標記 數值2]`

  - 數值比較標記
    - `-eq` 數值1 == 數值2
    - `-ne` 數值1 != 數值2

    - `-gt` 數值1 > 數值2
    - `-lt` 數值1 < 數值2

    - `-ge` 數值1 >= 數值2
    - `-le` 數值1 <= 數值2

- 範例，多重條件判斷
  - 例如，`$ [ -e ttt.sh -a -e tt.sh ] || echo false`，-a 代表 AND
  - 例如，`$ [ -e ttt.sh -o -e tt.sh ] || echo false`，-o 代表 OR

- 範例，取反向結果，`$ test ! -e 檔案`，檔案不存在時返回 true
  
- 範例，判斷當前的 stdout 是 termianl 或是 subshell (${} 或 ())
  
  `$ test -t `或 [-t]

## [語法] source(.) 載入並調用其他 .sh 檔案中的函數

- 在 fndef.sh 檔案中，定義以下函數

  ```shell
  # fndef.sh
  greet() {
      local name=$1
      echo "Hello, $name!"
  }
  ```

- 在 main.sh 檔案中進行調用

  ```shell
  # main.sh

  . ./fndef.sh    # 加載 fndef.sh 檔案的內容到當前腳本中，或 source ./fndef.sh  

  greet ching     # 調用函數
  ```

## [語法] 透過命令行將內容寫入檔案中的幾種方法

- 方法，透過 echo，適合單行
  - 例如，`$ echo "hello" > test.log`，覆寫檔案
  - 例如，`$ echo "hello" >> test.log`，插入檔案
  - 此方法遇到特殊符號`不會`進行轉譯

- 方法，透過 printf，例如，`$ printf "hello\nAnthony" >> test.log`，適合多行且格式化字串
  - 此方法遇到特殊符號`會`進行轉譯

- 方法，透過 `cat > 檔案路徑 << EOF .... EOF` 
  
  - 語法，將 EOF 標誌之間的所有內容寫入指定檔案中

    - 注意，若不指定檔案名，就會將內容直接輸出到螢幕上

    - 可拆分為兩個區塊
      - `cat > 檔案路徑`，用於建立檔案
      - `檔案路徑 << EOF .... EOF`，用於將 EOF 之間的內容寫入檔案
  
    - EOF 可替換為其他字，只要保持前後一致

    ```shell
    cat > 覆寫到檔案的檔案路徑 << EOF
    內容1
    內容2
    EOF
    ```

  - 注意，若 shell 不支援`cat << EOF`的語法，例如，fish，可透過`sh -c`來執行

    ```shell
    sh -c "cat << EOF >> 添加到檔案的檔案路徑
    多行內容1
    多行內容2
    EOF
    "
    ```

  - 注意，若多行內容包含變數
    - 變數會被立即求值，並替換成對應的變數值
    - 若不希望被立即求值，應使用 \禁止轉譯，例如
      
      ```shell
      cat << EOF
      # ${profile_abbrev} 會被替換成對應的變數值
      search --no-floppy --set=root --label "alpine-${profile_abbrev} $RELEASE $ARCH"
      
      # 寫入 $root/boot/grub，$root不會被替換成變數值
      set prefix=(\$root)/boot/grub   
      EOF
      ```

## [語法] install，複製檔案並設定額外屬性(可設置擁有者|群組|權限)

- 範例，`$ install -m755 aa.123 /tmp/bb.456`，將(檔案aa.123)複製為(檔案/tmp/bb.456)，並設置檔案權限為755

## [語法] 內建函數 getopts 的使用

- 語法，`getopts "建立可用的選項" 儲存選項的變數`

  - 建立可用的參數選項
    - 例如，"a:bcd:"，代表會建立 a、b、c、d 四個選項，若`該選項需要額外的參數`，需要在字母後面加上`:`
    - 例如，`a:、d:`，代表 a、d 選項是需要輸入額外的參數，例如，`-a 123`
    - 例如，`b、c`，代表 b、c 選項是開關選項，不需要額外的參數可直接使用，例如，`-b`
  
  - 儲存選項名的變數 : 當 getopts 找到匹配的選項時，會將該選項的字母存儲在這個變量中

    例如，若`getopts "a:bcd:" opt`，匹配到`-b 123`時，opt會保存b

  - 內建的 $OPTARG 變數 : 由 getopts 設置，用於存儲`當前選項的參數值`

    例如，若`getopts "a:bcd:" opt`，匹配到`-b 123`時，opt會保存123

  - 使用getopts函數時，需要搭配其他語法使用
    - 搭配 `while` 使用，getopts 會依序處理使用者輸入的選項，直到所有選項都處理完才會跳出迴圈
    - 搭配 `case ... esac` 使用，用來對每一個匹配的選項進行處理
  
  - 注意，使用時，只有符合`-選項名 參數值`的選項才會進行處理
  - 注意，當跳出迴圈後，(使用者自定義用於保存選項名的變數) 和 (內建的$OPTARG變數) 都不再有效

- 完整範例

  若bash檔，ttt.bash，的內容如下

  ```shell
  #!/bin/bash

  # ttt.sh

  # 定義一個函數來顯示使用說明
  usage() {
      echo "Usage: $0 [-a arg] [-b] [-c arg]"
  }

  # 初始化變量
  opt_a_arg=""
  opt_b=false
  opt_c_arg=""

  # 使用 getopts 解析命令行參數
  while getopts "a:bc:" opt; do
      case ${opt} in
          a)
              opt_a_arg=$OPTARG
              ;;
          b)
              opt_b=true
              ;;
          c)
              opt_c_arg=$OPTARG
              ;;
          
          \?) # 用於處理不可用的選項
              echo "Invalid option: -$OPTARG" >&2
              usage
              exit 1
              ;;
          
          :)  # 用於處理缺少參數的選項
              echo "Option -$OPTARG requires an argument." >&2
              usage
              exit 1
              ;;
      esac
  done

  # 顯示解析結果
  echo "a = $opt_a_arg, b = $opt_b, c = $opt_c_arg"
  ```

  測試
  - 輸入`$ ./test_getopts.sh -a 123 -b -c 456`，得到 a = 123, b = true, c = 456
  - 輸入`$ ./test_getopts.sh aa`，得到 a = , b = false, c = 
  - 輸入`$ ./test_getopts.sh -a 123 -f`，得到 Invalid option: -f
  - 輸入`$ ./test_getopts.sh -a`，得到 Option -a requires an argument.

## 範例集

- 範例，將程式的輸出隱藏，`/dev/null`
  `執行的程式 > /dev/null`，只將輸出隱藏
  `執行的程式 > /dev/null 2>&1`，將輸出和錯誤訊息都隱藏
  
- 範例，判斷程式是否執行，若否，才執行指定程式，`pidof`
  ```shell
  if ! $(pidof broadwayd > /dev/null)
  then
    broadwayd :5 > /dev/null 2>&1 &
  fi
  ```

- 範例，展開列表變數
  ```shell
  LIST=(123 456 789)

  # LIST[@] 代表 LIST 是一個列表變數
  # 列表變數必須透過 #{列表變數} 才會將列表展開
  echo "${LIST[@]}" # 123 456 789
  ```

- 範例，`command` 執行單一命令，常用判斷指令是否存在
  - 使用場景
    
    用於執行單一命令，和 ${} 不同，command 不能串接多個命令，
    例如，`$ command echo 123`

  - 透過 -v 參數，判斷命令是否存在，

    若命令`執行成功`，會打印出被執行命令的名稱，若命令為`執行失敗`，會打印出空內容，
    
    例如，`$ command -v sudo || echo false`，
    若 sudo 命令存在，打印出 sudo，否則打印出 false
  
- 範例，`||`，選擇性執行命令，當A命令無法執行時，執行B命令
  ```shell
  aa || echo 123
  ```

- 範例，`> 和 >> 的區別`，> 是覆蓋，>> 是添加
  ```shell  
  # 保留原來/mnt/etc/fstab的內容，genfstab產生新的文件系统表的內容，會添加到/mnt/etc/fstab的尾部
  /usr/bin/genfstab -p /mnt >> "/mnt/etc/fstab"

  # genfstab產生新的文件系统表的內容，會直接覆蓋/mnt/etc/fstab的檔案內容
  /usr/bin/genfstab -p /mnt > "/mnt/etc/fstab"
  ```

- 範例，`--` 標記選項結束，用於區隔`命令選項`和`命令參數`
  - 執行命令時，一般`命令選項在前，命令參數在後`
    - 命令選項: 用於控制命令的行為，依據需求添加
    - 命令參數: 是執行命令必要的數據，必須添加，不添加無法執行
    - 若命令參數帶有 - 的前綴時，容易與命令選項混淆，可以透過 -- 區分2者
  
  ```shell
  # rm <rm的選項> <指定檔案名的參數>
  # 若要刪除的檔案名為 -f  

  # 錯誤用法
  rm -rf -f

  # 正確用法
  #   -rf 為rm命令的選項
  #   -- 代表命令選項的結束，-- 後方為 rm 命令的必要參數 
  rm -rf -- -f
     ^^^        命令選項
         ^^     分隔符號，用於區分命令選項，和命令參數
            ^^  命令參數，-f 為要刪除檔案的檔案名
  ```

- 範例，`shift`，將輸入參數往左移動

  每次執行 shift 命令時，所有位置參數都會向左移動一個位置，原來的 $1 會被丟棄，$2 變成 $1，依此類推

  考慮以下範例，執行 `$ ./shift_example aa bb cc dd`

  ```shell
  # shift_example.sh
  while [ $# -gt 0 ]; do
      echo "all parameters = $*"
      echo "Parameter 1: $1"
      shift  # 向左移動參數
  done
  ```

  輸出結果為
  ```shell
  all parameters = aa bb cc dd
  Parameter 1: aa
  all parameters = bb cc dd
  Parameter 1: bb
  all parameters = cc dd
  Parameter 1: cc
  all parameters = dd
  Parameter 1: dd
  ```

- 範例，`local`，在函數內設置區域變數

- 範例，`:`，佔位符，`強制執行`後方的語句

  - 特性，
    - 確保變數初始化在腳本的各種情況下都能正確執行
    - 避免腳本中的語法錯誤、未定義變數錯誤、非預期的退出狀態以及跳過執行等問題
    - : 始終返回成功的退出狀態碼 0

  - 例如，保證變數設置時能成功執行
    - `${KOPT_init:=/sbin/init}`，檢查KOPT_init變數是否存在，若否，進行賦值，若是，不做任何事
    - ` : ${KOPT_init:=/sbin/init}`，強制執行後方 ${KOPT_init:=/sbin/init} 的語句，
  
  - 例如，即使條件不能成立也能強制執行後方語句
    
    ```shell
    if false; then
      ${KOPT_init:=/sbin/init}  # 不會被執行
    fi

    if false; then
      : ${KOPT_init:=/sbin/init}  # 強制被執行
    fi
    ```

  - 例如，用於創建無限循環

    ```shell
    while :; do
      # 代碼
    done
    ```

  - 例如，在條件句中佔位，但不需要進行任何操作

    ```shell
    if [ -f "/path/to/file" ]; then
      :   # 佔位符，不進行任何操作
    else
      echo "File does not exist"
    fi
    ```

- 範例，透過管道執行shell-script並傳遞參數，`$ 腳本內容 | bash -s -- 參數1 參數2 ...`
  - `$ 腳本內容 | bash -s -- 參數1 參數2 ...`，代表腳本由 bash 執行，並透過 -s 傳遞參數 (sent)

  - 用於透過單一行指令，實現 取得腳本內容 + 執行腳本 + 傳遞腳本參數
  - 腳本內容可以透過 curl 或 cat 或其他命令取得

  ```shell
  # 若 ttt.sh 的內容為，echo hello $1 $2

  # 透過管道執行命令
  #   step1，取得 shell-script 的內容
  #   step2，透過 | bash 執行
  #   step3，透過 -s -- 參數1 參數2 傳遞參數給前方的腳本
  #   -s 代表傳遞參數 -- 用於和一般的命令參數進行區別，-- 代表後方接的是要傳遞的參數，而不是要執行命令的參數
  cat ttt.sh | bash -s -- 123 456     # 得到 hello 123 456
  ```

- 範例，判斷當前使用者是否有添加到 /etc/sudoers 
  ```shell
  # -v 參數，若沒有添加到 /etc/sudoers 的使用者執行 sudo，會拋出 Sorry, user ttt may not run sudo ... 的錯誤
  # -n 參數，設置不要詢問密碼，只有設置 NOPASSWD:ALL 的用戶有效，否則拋出錯誤
  # 若執行此命令完全沒有錯誤，代表當前使用者有添加 sudoers 且 有設置 NOPASSWD
  sudo -n -v
  ```

- 範例，遍歷位置遍數的簡便語法
  
  ```shell
  #!/bin/bash

  set -- aa=1 bb=2

  # 若未透過 in 獲取來源，預設從位置變數獲取
  # 此語法只能用於迴圈
  for opt; do
    echo $opt
  done

  # 等效於
  for opt in $@; do
    echo $opt
  done
  ```

- 範例，將多欄數據中的每一個數據，分別保存到獨立的變數中

  - 方法，適用於單行

    ```
    read a b c d <<< $(echo "1 2 3 4")
    echo $a $b $c $d
    ```

  - 方法，適用於多行

    ```
    echo "1 2 3 4" | while read a b c d ; do
      echo $a $b $c $d
    done
    ```

## Ref
- [基礎linux指令&簡單的shell語法和腳本編寫](https://www.bilibili.com/video/BV1d4411v7Ah)
