
## [de] KDE-Desktop 安裝

- 依照內建軟體的多寡，KDE可分為
  - kde-full: 完整安裝，安裝所有的依賴項，共 4.1GB
  - kde-standard: 標準安裝，安裝常見的套件，例如，信箱、瀏覽器、檔案總管 等，共 1.6GB
  - kde-plasma-desktop: 核心安裝，僅安裝必要依賴，共 1.2GB
  - plasma-desktop: 桌面系統相關的工具和小套件，不是完整的桌面系統，共 2GB