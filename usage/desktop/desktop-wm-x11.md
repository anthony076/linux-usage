## desktop-wm-x11

- 基於x11架構的wm

## [wm] bspwm 配置範例

- 完整範例，[在virtualbox透過startx或lightdm啟動bspwm範例](#範例-在-virtualbox-透過-startx-或-lightdm-啟動-bspwm-範例)

- [Bspwm quick setup: polybar + dmenu + picom + nitrogen + Arch Linux](https://www.youtube.com/watch?v=3qceiiBSA-4)
- [bspwm Arch Linux Install](https://www.youtube.com/watch?v=ZbXQUOwcH08)
- [Introduction to BSPWM, and how to configure it](https://www.youtube.com/watch?v=_55HGnz422M)
- [archlinux+bspwm](https://link.zhihu.com/?target=https%3A//github.com/MiraculousMoon/bspwm-dotfiles)
- [bspwm設置範例](https://bxiehq.github.io/bspwm-on-arch)

## [wm] awesome 配置範例

- 安裝命令，
  - 安裝字體
  - `$ paru -S awesome dmenu picom --noconfirm`
  
- [官網](https://awesomewm.org/screenshots/)
- [Getting Started With Awesome Window Manager @ DistroTube](https://www.youtube.com/watch?v=qKtit_B7Keo)
- [配置說明 @ Mathews.it](https://zhuanlan.zhihu.com/p/58254611)
- [配置範例 @ ayamir](https://github.com/ayamir/dotfiles/tree/master/catppuccin)
- [配置範例 @ lcpz](https://www.youtube.com/watch?v=3VctjbvWJtc)
- [簡單配置 @ NOISNEMID-PKM](https://www.bilibili.com/video/BV17u411X7We)
- [介紹+配置範例](https://www.bilibili.com/video/BV1m5411H7tA)
- [各種配置範例](https://mipmip.github.io/awesomewm-screenshots/)
- [awesome 配置，中文說明](https://wiki.gentoo.org/wiki/Awesome/zh-cn)

## [wm] leftwm 配置範例

- [官方網站](https://github.com/leftwm/leftwm)

## [wm] wayfirewm 配置範例

- [介紹 wayfire 和配置](https://www.bilibili.com/video/BV1yT4y1i73B)

## [wm] dwm 配置範例

- [Archlinux-dwm配置](https://zhuanlan.zhihu.com/p/346719806)
- [dwm(dynamic-window-manager) get started](https://www.youtube.com/watch?v=3QA0TdnE4IU)
- [Installing Xorg And A Window Manager In Arch Linux](https://www.youtube.com/watch?v=pouX5VvX0_Q)
- [配置範例 @ ayamir](https://github.com/ayamir/dotfiles/tree/master/catppuccin)

## [wm] qtile 配置範例

- [配置範例 @ ayamir](https://github.com/ayamir/dotfiles/tree/master/catppuccin)


## [wm] suckless = dwm + st + dmenu + slock 配置範例

- [手動安裝 suckless](https://blog.51cto.com/u_11090813/5343974)

- [Suckless--極簡主義者的Linux世界](https://juejin.cn/post/7027387291005878309)

## [範例] 在 virtualbox 透過 startx 啟動 awesome 範例

- 在 virtualbox 中是有配置顯示器的，因此可以透過 startx 啟動桌面系統，
  但預設不能透過遠端執行 startx

- 僅使用 `xf86-video-fbdev` 顯示器驅動

- step1，安裝相關套件
  ```shell
  sudo pacman -S xf86-video-fbdev \
    xorg-server xorg-xinit \
    ttf-mononoki-nerd xterm awesome
  ```

- step2，配置 `xorg.conf`
  
  若無法正常顯示才需要配置，實測即使不添加也可以正常啟動 awesome

  ```shell
  # sudo nano /etc/X11/xorg.conf
  Section "Device"
  Identifier "fbdev"
  Driver "fbdev"
  EndSection
  ```

- step3，配置 `xinitrc`
  ```shell
  # sudo nano /etc/X11/xinit/xinitrc
  ... 內容省略 ...
  #twm &
  #xclock -geometry 50x50-1+1 &
  #xterm -geometry 80x50+494+51 &
  #xterm -geometry 80x20+494-0 &
  #exec xterm -geometry 80x66+0+0 -name login
  awesome
  ```

- Trouble-Shooting
  
  使用錯誤驅動，例如，在 virtualbox 內僅使用 xf86-video-vmware 驅動
  ```shell
  # sudo nano /var/log/Xorg.0.log
  ... 省略 ...
  # 嘗試使用 fbdev 驅動
  [ 13798.935] (EE) Failed to load module "fbdev" (module does not exist, 0)
  # 嘗試使用 vesa 驅動
  [ 13798.936] (EE) Failed to load module "vesa" (module does not exist, 0)
  # 嘗試使用 vmware 驅動，可以使用 vmware 驅動，但是找不到 vmware 對應的硬件而出錯
  [ 13798.940] (EE) vmware(0): Failed to open drm.
  [ 13799.112] (EE) vmware(0): Unable to map frame buffer BAR. Invalid argument (22)
  ```

## [範例] 在 virtualbox 透過 startx 或 rdp 啟動 bspwm 範例

```shell
# ------------------
# 安裝相關套件
#   - xf86-video-fbdev: 顯示器驅動，勘用，不是最好的驅動
#   - sxhkd: bspwm 推薦的按鍵管理
#   - archlinux-wallpaper: 桌面背景集
#   - nitrogen: 桌面背景管理
#   - alacritty: GUI下的終端
#   - lightdm: 登入管理
# ------------------

sudo pacman -S xf86-video-fbdev sxhkd archlinux-wallpaper nitrogen alacritty bspwm \
xorg-server xorg-xinit \
xrdp xorgxrdp 

# ------------------
# 修改 bspwmrc、sxhkdrc 配置檔
# ------------------

# 複製 bspwmrc、sxhkdrc 配置檔
install -Dm755 /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/bspwmrc
install -Dm644 /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/sxhkdrc

# 修改 bspwmrc、sxhkdrc 的配置
sudo nano ~/.config/bspwm/bspwmrc，使用預設設定值

sudo nano ~/.config/sxhkd/sxhkdrc，添加以下
  # terminal emulator
  super + Return
          alacritty

# ------------------
# 修改alacritty配置
# ------------------

mkdir -p ~/.config/alacritty
cp /usr/share/doc/alacritty/example/alacritty.yml ~/.config/alacritty/alacritty.yml
sudo nano ~/.config/alacritty/alacritty.yml

# ------------------
# 透過 nitrogen 設置桌面
# nitrogen --restore 命令需要 bg-saved.cfg
# ------------------

mkdir -p /home/vagrant/.config/nitrogen

echo "[xin_-1]
file=/usr/share/backgrounds/archlinux/simple.png
mode=0
bgcolor=#000000" > /home/vagrant/.config/nitrogen/bg-saved.cfg

# ------------------
# 修改 x11 相關配置，使 startx 生效
# 使用正確驅動就不需要配置 xorg.conf，保持空白即可
# ------------------

sudo nano /etc/X11/xorg.conf 

sudo nano /etc/X11/xinit/xinitrc，添加以下
  #twm
  nitrogen --set-zoom-fill /usr/share/backgrounds/archlinux/simple.png
  sxhkd &
  exec bspwm

# ------------------
# 修改 Xwrapper 的設置，使一般使用者也可以連線
# ------------------

sudo nano /etc/X11/Xwrapper.config，添加以下
  allowed_users = anybody

# ------------------
# 設置 rdp 連線時，透過 startwm.sh 啟動 bspwm
# ------------------

sudo nano /etc/xrdp/startwm.sh，添加以下
  #!/bin/bash

  #若使用以下語句，就不需要建立 bg-saved.cfg
  #nitrogen --set-zoom-fill /usr/share/backgrounds/archlinux/simple.png  
  nitrogen --restore
  sxhkd &
  bspwm

# ------------------
# 啟動服務
# ------------------
sudo systemctl enable xrdp
sudo systemctl start xrdp

# ------------------
# 重新啟動和測試
# ------------------

sudo reboot now

# 手動透過 startx 啟動bspwm
startx

# rdp 測試
# 在 windows 上，透過 Remote-Desktop-Connection 連接 127.0.0.2 進行測試
```

## [範例] 在 virtualbox 透過 startx/rdp 共用配置，啟動 bspwm 範例，推薦

- 上一個[範例](#範例-在-virtualbox-透過-startx-或-rdp-啟動-bspwm-範例)的缺點
  - 缺點1，startx 使用的 xinitrc 的配置，而 rdp 使用 startwm.sh 的配置，分為兩個配置檔不好維護
  
  - 缺點2，在 startwm.sh 中設置的全局變數，必須透過 `$ . /etc/environment` 才有效，
    參考，[遠端桌面/本機桌面設置環境變數的差異](desktop.md#遠端桌面和本機桌面設置環境變數的差異)

  - 缺點3，對於部分使用 dbus 與其他通訊桌面應用進行通訊的軟體，
    在 rdp 下會失效，參考，[dbus庫一節](desktop.md#xorg-x11-基礎)

- 推薦 startx/rdp 共同配置的方式如下，
  - 以安裝 fcitx5 為例，該套件在配置時會使用 dbus 進行通訊
  
  - 完整範例，[bspwm + rdp 的環境下啟動桌面環境 + fcitx5](desktop-wm-sw.md#輸入法-fcitx5)
  
  - `step1`，將啟動寫在 xinitrc 檔案中
    ```shell
    ... 內容省略 ...
    nitrogen --set-zoom-fill /usr/share/backgrounds/archlinux/simple.png
    sxhkd &

    export GTK_IM_MODULE=fcitx
    export QT_IM_MODULE=fcitx
    export XMODIFIERS=@im=fcitx

    fcitx5 &

    # (選用) 若安裝 dbus-x11 後仍然沒有效，才需要加入此行
    eval $(dbus-launch --sh-syntax)

    exec bspwm
    ```

  - `step2`，透過 startwm.sh 執行 xinitrc 的內容
    ```shell
    . /etc/X11/xinit/xinitrc
    exit 0
    ```

## [範例] 在 virtualbox 透過 startx 或 lightdm 啟動 bspwm 範例

```shell
# 安裝相關套件
#   - xf86-video-fbdev: 顯示器驅動，勘用，不是最好的驅動
#   - sxhkd: bspwm 推薦的按鍵管理
#   - archlinux-wallpaper: 桌面背景集
#   - nitrogen: 桌面背景管理
#   - alacritty: GUI下的終端
#   - lightdm: 登入管理
sudo pacman -S xf86-video-fbdev sxhkd archlinux-wallpaper nitrogen alacritty bspwm \
lightdm lightdm-gtk-greeter-settings xorg-server xorg-xinit

# 複製配置檔
install -Dm755 /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/bspwmrc
install -Dm644 /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/sxhkdrc

sudo nano ~/.config/bspwm/bspwmrc，使用預設設定值

sudo nano ~/.config/sxhkd/sxhkdrc，添加以下
  # terminal emulator
  super + Return
          alacritty

# 修改終端配置
mkdir -p ~/.config/alacritty
cp /usr/share/doc/alacritty/example/alacritty.yml ~/.config/alacritty/alacritty.yml
sudo nano ~/.config/alacritty/alacritty.yml

# 修改 x11 相關配置，使 startx 生效
sudo nano /etc/X11/xorg.conf，使用正確驅動就不需要配置 xorg.conf，保持空白即可

sudo nano /etc/X11/xinit/xinitrc，添加以下
  #twm
  nitrogen --set-zoom-fill /usr/share/backgrounds/archlinux/simple.png
  sxhkd &
  exec bspwm

# 透過 nitrogen 設置桌面
# nitrogen --restore 命令需要 bg-saved.cfg
mkdir -p /home/vagrant/.config/nitrogen

echo "[xin_-1]
file=/usr/share/backgrounds/archlinux/simple.png
mode=0
bgcolor=#000000" > /home/vagrant/.config/nitrogen/bg-saved.cfg

# 建立自定義的 /usr/local/bin/bspwm-session
#   - bspwm.desktop 會調用此檔案
#   - 若 .desktop 直接執行命令，會有語法上的限制，因此改用 shell-script 調用
#   - shell-script 放在 /usr/local/bin 目錄，是預設有效的目錄
#   - 可以自訂 shell-script 的路徑，該路徑必須事先添加到 env 變數中
sudo nano /usr/local/bin/bspwm-session，添加以下
  #!/bin/bash

  #若使用以下語句，就不需要建立 bg-saved.cfg
  #nitrogen --set-zoom-fill /usr/share/backgrounds/archlinux/simple.png  
  nitrogen --restore

  sxhkd &
  bspwm

# 必要，將 /usr/local/bin/bspwm-session 變更為可執行
sudo chmod +x /usr/local/bin/bspwm-session

# 修改 lightdm 啟動 bspwm 的選項
sudo nano /usr/share/xsessions/bspwm.desktop，添加以下
  [Desktop Entry]
  Name=bspwm
  Comment=Binary space partitioning window manager
  Exec=bspwm-session  # 執行自定義的 shell-script
  Type=Application

# 手動透過 startx 啟動bspwm
startx

# 啟用 lightdm 服務
sudo systemctl enable lightdm

# 重開機後會透過 lightdm 啟動登入畫面
sudo reboot now
```