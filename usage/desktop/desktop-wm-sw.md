## 常見 WM 的配置與使用

- 推薦套件
  - 字體
    - [wqy-microhei 中文字體](https://blueskyson.github.io/2021/09/16/archlinux-setup) 
	  - [Nerd 字體](https://www.nerdfonts.com/font-downloads)
	  - Noto Sans Mono 編程用字體
  	  - `$ sudo pacman -S noto-fonts`
  	  - Noto: 思源字體，免費字體
  	  - Sans: 無襯線
  	  - Mono: 中英文等寬
  	  - CJK : 中日韓
  	  - Regular: 粗細程度一般
  
  - GUI終端
    - termite: 
    - Konsole: KDE-Desktop 用的 terminal，可自訂命令的快速按鈕
    - st: simple-terminal
    - Alacritty: 支援連結跳轉、即時套用配置變更
    - termite、terminus-font
    - foot
    - kitty（GPU加速）
    - alacritty（GPU加速）

  - 程式啟動:
    - [dmenu](#程式啟動-dmenu): 推薦，不需要輸入完整的程式名，就可以啟動程式
    - [rofi](#程式啟動-rofi): 可自定義右鍵的內容
    - [fuzzel](#程式啟動-fuzzel-for-wayland)
    - bmenu: Dynamic menu library and client program inspired by dmenu

  - 工具欄: 
    - title-git: 用於從指定的 X11 視窗 ID 中獲取視窗標題，使用 wname 來顯示當前活動視窗的標題
    - sutils-git: 包含了多個輕量級的 X11 工具的套件，例如 dmenu、slurp、grabc、xgetres 等等
    - lemonbar-xft-git: 一個類似 dzen2 的 X11 工具欄程式，用於在桌麵上顯示各種系統信息，
      如時間、日期、網路狀態等等
    - 配置參考
      - https://youtu.be/ZbXQUOwcH08?t=1022
      - https://youtu.be/ZbXQUOwcH08?t=1700

  - 主題: 
    - arc-gtk-theme
    - arc-icon-theme

  - 主題設定器: 
    - lxappearance
    - qt5ct
    - xsettingsd

  - 狀態欄:
    - polybar
    - Lemonbar
    - xmobar
    - Tint2
    - trayer: 輕量級的系統托盤，它可以在任務欄上顯示各種應用程式的圖示，
      支援透明度、自動隱藏和點選事件等功能
    - stalonetray: 輕量級的系統托盤，它也可以在任務欄上顯示應用程式的圖示，
      支援多個托盤實例、自動重啓和圖示縮放等功能

  - 桌面集: 
    - archlinux-wallpaper

  - 圖片瀏覽:
    - feh: 簡易、輕量
    - gwenview: 功能完整，占用體積大
  
  - 網頁瀏覽器
    - surf: 極簡瀏覽器，簡單但功能強大，佔用空間
    - lynx: 佔用空間 5.76MB
    - w3m:  佔用空間 2.47MB
    - librewolf: A custom version of Firefox, focused on privacy, security and freedom.

  - 背景設定器: 
    - nitrogen
    - feh

  - 系統監視器
    - Conky: 推薦，輕量級的系統監視器，可在桌面顯示自定義的快捷鍵
    
  - 螢幕解析度調整: xrandr
  - 文件管理器:thunar、nemo
  - 截圖工具:flameshot、scrot
  - 輸入法:fcitx5
  - 剪切板管理器:copyq
  - 通知管理器:dunst
  - 鎖屏管理器:betterlockscreen、xautolock（用於自動鎖屏）
  - xorg-xset: 用於設定 X11 服務器參數的工具，它可以設定螢幕的亮度、螢幕保護程式等參數
  - xorg-xsetroot:
    - 用於設定 X11 根視窗屬性的工具，它可以設定根視窗的顔色、位圖和名稱等屬性。
    - xorg-xsetroot 常用於更換鼠標
  - remmina: 類似 vnc或rdp，用於linux遠端連接windows桌面的套件

- wayland 推薦應用
  - from [SolDoesTech/HyprV2](https://github.com/SolDoesTech/HyprV2)
  - hyprland-bin: the Hyprland compositor
  - wofi: a launcher/menu program for wlroots based wayland compositors
  - wlogout: A wayland based logout menu
  - mako: A lightweight Wayland notification daemon

## [必要] 啟動 dbus

dbus 是桌面系統的應用程序之間通訊用的 IPC 系統，
- 若在本地端執行桌面，使用 dbus 庫
- 若在遠地端執行桌面，使用 dbus-x11 庫
- dbus 詳見，[x11基礎](desktop.md#xorg-x11-基礎) 的 dbus 庫一節

## [compositor] picom(原compton)

- 安裝，`$ sudo pacman -S picom`

- 建立配置檔，
  - `$ mkdir -p ~/.config/picom`
  - `$ cp /usr/share/doc/picom/picom.conf.example ~/.config/picom/picom.conf`
  - 配置 picom.conf
    ```shell
    # 若沒有 nvidia gup，應該關閉 vsync
    vsync=false
    ```

- 範例，設置指定應用的透明度，以 alacritty 為例

  - step1，執行 `$ xprop | grep "CLASS"` 後，點擊 alacritty 的視窗，會出現以下
    ```shell
    # 第一個值為 res_name，第二個值為 res_class，使用第二個值
    # ref: https://tronche.com/gui/x/xlib/ICC/client-to-window-manager/wm-class.html
    WM_CLASS(STRING) = "Alacritty", "Alacritty"
    ```

  - step2，修改 picom.conf，添加以下
    - `$ mkdir -p ~/.config/picom`
    - `$ cp /usr/share/doc/picom/picom.conf.exmaple ~/.config/picom/picom.conf` 
    - `$ sudo nano ~/.config/picom/picom.conf` 
      ```shell
      # 添加以下內容
      opacity-rule = [ "80:class_g = 'Alacritty'" ];
      ```

## [桌面背景] nitrogen 
- 優點: 安裝僅 0.44MB
  
- 安裝，
  - 安裝桌布集，`$ sudo pacman -S archlinux-wallpaper`
  - 安裝 nitrogen，`$ sudo pacman -S nitrogen`

- 使用
  - 透過 gui 手動設置桌布，`$ nitrogen`，
    選擇後，會建立 `/home/vagrant/.config/nitrogen/bg-saved.cfg` 配置檔

  - 透過 gui 自動設置桌布，`$ nitrogen --set-zoom-fill /usr/share/backgrounds/archlinux/simple.png`

  - 若要開機自動啟動 nitrogen，
    - 方法1，`$ nitrogen --restore`，注意，此命令需要先建立 bg-saved.cfg

    - 方法2，，`$ nitrogen --set-zoom-fill /usr/share/backgrounds/archlinux/simple.png`

- 範例，
  - [在virtualbox透過startx或lightdm啟動bspwm範例](desktop.md#範例-在-virtualbox-透過-startx-或-lightdm-啟動-bspwm-範例)

## [桌面背景] feh

- 優點: 
  - 安裝僅 0.43M、
  - 可作為背景設置或圖片瀏覽器

- 安裝，
  - 安裝桌布集，`$ sudo pacman -S archlinux-wallpaper`
  - 安裝 feh，`$ sudo pacman -S feh`

- 使用
  - 設置桌面，`$ feh --bg-center /usr/share/backgrounds/archlinux/simple.png`

  - 瀏覽圖片
    - 常用按鍵
      - 放大圖片: 向上鍵
      - 縮小圖片: 向下鍵
      - 換張: Space鍵 | 滑鼠中鍵 | 圖片左右側
  
    - 單張瀏覽，`$ fet /usr/share/backgrounds/archlinux`
    - 單張瀏覽，`$ fet --keep-zoom-vp -Z -p 1200x800 /usr/share/backgrounds/archlinux`
    - 以縮略圖方式瀏覽，`$ fet -t /usr/share/backgrounds/archlinux`
    - 鎖定瀏覽尺寸，`圖片 > 右鍵 > Options > keep-viewport-zoo & pos`

## [按鍵管理] sxhkd

- 預設按鍵
  - win+Enter:  打開 terminal
  - win+Space:  打開 dmenu
  - win+Esc:  重新載入 bspwm 配置
  - win+w:  關閉視窗
  - win+alt+q:  關閉 bspwm
  - win+滑鼠左鍵拖曳: 調整視窗位置 (注意，必須先透過 win+s 將視窗縮小)

- 修改 sxhkd 的配置
  - sxhkd 的配置文件，`$ sudo nano /home/vagrant/.config/sxhkd/sxhkdrc`

  - 變更預設的 terminal
    ```shell
    # terminal emulator
    super + Return
            alacritty
    ```

## [程式啟動] dmenu

- 設置 sxhkd 啟動 dmenu + 變更 dmenu 的啟動樣式
  - `step1`，在任意位置建立 dmenu.sh
    ```shell
    sudo nano ~/dmenu.sh，添加以下
      dmenu_run -p "Run:" -fn 'Droid Sans Mono-20'
    
    # 將 dmenu.sh 變更為可執行
    chmod +x ~/dmenu.sh
    ```
  
  - `step2`，設置 sxhkd 啟動 dmenu
    ```shell
    # 將 dmenu.sh 添加到 sxhkd 按鍵管理器
    sudo nano ~/.config/sxhkd/sxhkdrc，添加以下
      super + space
              ~/dmenu.sh
    ```

## [程式啟動] rofi

- [使用 rofi 取代 dmenu，rofi 可顯示快速鍵](https://fedops.codeberg.page/dmenurofi-scripts.html)

- 安裝，`$ sudo pacman -S rofi`

- 配置，
  - [官方配置說明](https://davatorium.github.io/rofi/CONFIG/)

  - 取得預設的配置，
    - 注意，需要再 gui 環境下執行
    - 查看預設配置，`$ rofi -dump-config`
    - 建立自定義配置文件，`$ rofi -dump-config > ~/.config/rofi/config.rasi`

  - 自定義的配置文件: `~/.config/rofi/config.rasi`
    ```shell
    # 配置 rofi 的行為
    configuration {
      modi: "window,drun,combi";  # 設置可使用的模式
      combi-modi: "window,drun,run";  # 設置 rofi -show combi 命令下可使用的模式
      
      opacity: 50;  # 設置透明度
      font: "Nerd 16"; # 設置字體和大小

      icon-theme: "Papirus"; # 設置 icon-theme
      show-icons: true; # 顯示圖標
    }

    # 或 @theme "/usr/share/rofi/themes/Paper.rasi"
    @theme "Arc-Dark"
    
    # 修改輸入框
    entry {
      placeholder: "請輸入命令";
    }

    # 修改樣式
    # ┌─────────────────────────────────────────────────────────────────────┐ 
    # │ element                                                             │ 
    # │ ┌─────────────────────────────────────────────┐ ┌─────────────────┐ │ 
    # │ │element─text                                 │ │ element─icon    │ │ 
    # │ └─────────────────────────────────────────────┘ └─────────────────┘ │ 
    # └─────────────────────────────────────────────────────────────────────┘ 
    
    # 修改 element 的樣式
    element {
      orientation: horizontal;  # 子元素水平排列
      children: [ element-text, element-icon];  # 子元素出現的順序
      spacing: 8px; # children 的間距
    }

    # 修改 element-icon 的樣式
    element-icon {
      size: 2.5em;  # 設置圖標大小
    }

    # 變更 entry 和 element-text 的背景
    entry, element-text {
      text-color: black;
      location: center;
      // background-color: rgb(0,0,255);
    }
    ```

  - 等效配置命令
    ```shell
    rofi -show combi \    # 顯示混合模式
      -theme Arc-Dark \   # 設置主題
      -font "Nerd 10" \   # 設置字體
      -icon-theme "Papirus" -show-icons # 指定 icon-theme 並顯示圖標
    ```

  - 預覽主題，`$ rofi-theme-selector`
    - 選擇主題，按下 Enter 可預覽
    - 選中主題，按下 alt+a 可套用

- 啟動，`$ rofi -show 模式`
  - `$ rofi -show window`，開啟窗口搜尋列，顯示正在運行的窗口
  - `$ rofi -show run`，開啟命令搜尋列，顯示可執行文件的列表
  - `$ rofi -show drun`，開啟命令搜尋列，顯示已安裝的應用列表
  - `$ rofi -show combi`，開啟命令搜尋列 (混合模式)

- 範例，利用 rofi + sxhkd 顯示快速鍵
  - [show sxhkd keybindings with fuzzy search](https://www.reddit.com/r/bspwm/comments/aejyze/tip_show_sxhkd_keybindings_with_fuzzy_search/)
  - [BSPWM, Polybar and Rofi](https://www.youtube.com/watch?v=FZHjE5H-B-M)
  
- 工具，`rofi-collection`，使用已配置好的rofi主題
  - step1，下載，`$ git clone --depth=1 https://github.com/adi1090x/rofi.git`
  - step2，變更 setup.sh 的權限，`$ cd rofi && chmod +x setup.sh`
  - step3，以 type6|style6 為例
    ```shell
    # /home/vagrant/.config/rofi/launchers/type-6/launcher.sh
    
    # 修改 style
    theme='style-6'
    ```

    執行，`$ ~/.config/rofi/launchers/type-6/launcher.sh`

- 參考
  - [rofi on archlinux](https://wiki.archlinuxcn.org/wiki/Rofi)
  - [Themes Collection for Rofi Launcher](https://github.com/newmanls/rofi-themes-collection)

## [程式啟動] fuzzel (for wayland)
- [A great dmenu and rofi alternative for Wayland](https://mark.stosberg.com/fuzzel-a-great-dmenu-and-rofi-altenrative-for-wayland/)

## [終端] alacritty

- 設置 alacritty
  - 建立自定義配置檔，`$ cp /usr/share/doc/alacritty/example/alacritty.yml ~/.config/alacritty/alacritty.yml` 

  - 修改配置檔，`$ nano ~/.config/alacritty/alacritty.yml`
    ```shell
      window:
        # 設置透明度，需要安裝 picom 或 compton
        opacity: 0.8

      # 設置字體，設置為 noto-fonts 包的 NotoSansMono
      font:
        normal:
          # noto-fonts /usr/share/fonts/noto/NotoSansMono-Light.ttf
          family: Noto Sans Mono
          style: light

        size: 28.0
    ```

- 設置透明
  - 注意，需要 picom 才能有透明的效果，
    確保 picom 已經啟動才看的到效果，`$ pidof picom`，修改後會立刻生效，不需要重開機

  - `方法1`，透過`命令行參數`進行配置，`$ alacritty -o "window.opacity=0.9"`

  - `方法2`，透過 `alacritty.yml` 進行設置
    ```shell
    # nano ~/.config/alacritty/alacritty.yml
    window:
      # 設置透明度，需要安裝 picom 或 compton
      opacity: 0.8
    ```
  
  - `方法3`，透過 picom 設置透明，
    參考，[設置指定應用的透明度，以 alacritty 為例](#compositor-picom原compton)

  - 注意，
    - 在使用主機的狀況下，透過`方法1、方法2`配置的透明效果會立刻生效

    - 在使用虛擬機的狀況下，需要透過`方法3(picom)`設置透明
      
      VirtualBox本身的圖形驅動程序不支持直接設置透明度。 需要使用picom等軟件來實現透明度設置。

      因為 picom 是一種用於 X11 的透明度支持的合成管理器，可以在 X11 上創建透明窗口，它提供了對不透明度規則的支持，
      允許根據特定應用程序或窗口類別設置不透明度。
  
    - 參考，
      - [opacity doesn't work inside VM](https://github.com/alacritty/alacritty/issues/4217)
      - [picom transparency doesn't apply to alacritty (bspwm)](https://stackoverflow.com/questions/72070101/picom-transparency-doesnt-apply-to-alacritty-bspwm)

- alacritty.yml 範例
  - https://github.com/peterhpchen/dotfiles/blob/c185e895a94e4ac1445e9389e206134038019bd7/alacritty/alacritty.yml
  - https://sunnnychan.github.io/cheatsheet/linux/config/alacritty.yml.html

## [輸入法] fcitx5

- 配置，
  - 環境: archlinux + bspwm

  - 安裝: `$ sudo pacman -S fcitx5-im fcitx5-chewing`
    - fcitx5-im 是群組包，包含 fcitx5、fcitx5-configtool、fcitx5-gtk、fcitx5-qt 四個套件
    - fcitx5: 輸入法核心框架
    - fcitx5-chewing: 中文輸入法
    - fcitx5-configtool: GUI設置工具

- `範例`，透過 `startx` 啟動桌面環境 + fcitx5
  ```shell
  # sudo nano /etc/X11/xinit/xinitrc

  ... 內容省略 ...
  export GTK_IM_MODULE=fcitx
  export QT_IM_MODULE=fcitx
  export XMODIFIERS=@im=fcitx
  
  fcitx5 &
  
  # 啟動 dbus 和 bspwm
  dbus-launch --sh-syntax --exit-with-session bspwm

  # 執行 fcitx5 配置工具
  fcitx5-configtool

  # Input-Method > 選擇 Chewing > Global-Options 調整切換中英文的快速鍵
  # 進入 gedit 進行測試
  # 透過快速鍵進行切換
  ``` 

- `範例`，透過 `bspwm + rdp` 啟動桌面環境 + fcitx5
  
  - 注意，在 rdp 下需要安裝 dbus-x11 才能使 x11 正常工作，
    原因見，[dbus庫一節](desktop.md#xorg-x11-基礎)
  
  ```shell
  # 注意，dbus 必須要替換成 dbus-x11 庫
  sudo pacman -S xf86-video-fbdev sxhkd archlinux-wallpaper nitrogen alacritty bspwm \
  xorg-server xorg-xinit dbus-x11 \
  xrdp xorgxrdp \
  fcitx5-im fcitx5-chewing

  # ------------------
  # 修改 bspwmrc、sxhkdrc 配置檔
  # ------------------

  # 複製 bspwmrc、sxhkdrc 配置檔
  install -Dm755 /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/bspwmrc

  install -Dm644 /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/sxhkdrc

  # 修改 bspwmrc、sxhkdrc 的配置
  sudo nano ~/.config/bspwm/bspwmrc，使用預設設定值

  sudo nano ~/.config/sxhkd/sxhkdrc，添加以下
    # terminal emulator
    super + Return
            alacritty

  # ------------------
  # 修改alacritty配置
  # ------------------

  mkdir -p ~/.config/alacritty
  cp /usr/share/doc/alacritty/example/alacritty.yml ~/.config/alacritty/alacritty.yml
  sudo nano ~/.config/alacritty/alacritty.yml

  # ------------------
  # 透過 nitrogen 設置桌面
  # nitrogen --restore 命令需要 bg-saved.cfg
  # ------------------

  mkdir -p /home/vagrant/.config/nitrogen

  echo "[xin_-1]
  file=/usr/share/backgrounds/archlinux/simple.png
  mode=0
  bgcolor=#000000" > /home/vagrant/.config/nitrogen/bg-saved.cfg

  # ------------------
  # 修改 Xwrapper 的設置，使一般使用者也可以連線
  # ------------------

  sudo nano /etc/X11/Xwrapper.config，添加以下
    allowed_users = anybody

  # ------------------
  # 修改 x11 相關配置，使 startx 生效
  # 使用正確驅動就不需要配置 xorg.conf，保持空白即可
  # ------------------

  sudo nano /etc/X11/xorg.conf，保持空白

  sudo nano /etc/X11/xinit/xinitrc，添加以下
    nitrogen --set-zoom-fill /usr/share/backgrounds/archlinux/simple.png
    sxhkd &

    export GTK_IM_MODULE=fcitx
    export QT_IM_MODULE=fcitx
    export XMODIFIERS=@im=fcitx

    fcitx5 &

    # (選用) 若安裝 dbus-x11 後仍然沒有效，才需要加入此行
    eval $(dbus-launch --sh-syntax)

    exec bspwm

  # ------------------
  # 設置 rdp 連線時，透過 startwm.sh 執行 xinitrc
  # ------------------

  sudo nano /etc/xrdp/startwm.sh，添加以下
    # . 是在當前 shell 環境中執行的意思
    . /etc/X11/xinit/xinitrc
    exit 0

  # ------------------
  # 啟動服務
  # ------------------
  sudo systemctl enable xrdp
  sudo systemctl start xrdp

  # ------------------
  # 重新啟動和測試
  # ------------------

  sudo reboot now

  # 啟動bspwm

  # 本機啟動 bspwm
  startx

  # 或遠端啟動 bspwm
  # 在 windows 上，透過 Remote-Desktop-Connection 連接 127.0.0.2 進行測試

  # 執行配置工具，
  # 若 dbus 出現異常，會有 run fcitx5 的提示
  fcitx5-configtool

  # 執行 fcitx5 配置工具
  fcitx5-configtool

  # Input-Method > 選擇 Chewing > Global-Options 調整切換中英文的快速鍵
  # 進入 gedit 進行測試
  # 透過快速鍵進行切換
  ```

## [輸入法] Hime

- 優缺點，
  - 優，準確度高
  - 支援的輸入法較少，僅支援 中、日、韓
  - 配置較複雜
  - 穩定下較差

- 範例，[Hime 的安裝與配置](https://blueskyson.github.io/2021/09/16/archlinux-setup/#hime-%E8%BC%B8%E5%85%A5%E6%B3%95)


## ref

- 配置參考
  - [dotfiles @ Ziqi-Yang](https://github.com/Ziqi-Yang/dotfiles)
  
- 輸入法
  - [fcitx5 @ archwiki](https://wiki.archlinux.org/title/Fcitx5)
  - [fcitx @ archwiki](https://wiki.archlinuxcn.org/zh-tw/Fcitx)
  - [fcitx官方的配置說明](https://fcitx-im.org/wiki/Configure_(Other)#Use_Slim_.28.7E.2F.xinitrc.29.2Fstartx)