## desktop-wm-wayland

- 基於wayland架構的wm
  
  - `觀念`，XWayland-server 用於(在wayland架構的VM)中執行(x11應用)
    - 沒有XWayland的WM，無法直接執行X11應用程序，但`可以透過手動安裝XWayland的庫`來克服
    - 使用XWayland的WM，執行`X11應用程序`效能稍差，與使用`X11架構的WM`相比
    - 使用XWayland的WM，執行`wayland應用程序`效能相當，與使用`wayland架構的WM`相比
    - 使用XWayland的WM需要更多的資源，
      
      XWayland作為一個X11服務器運行在Wayland之上，需要額外的處理步驟來轉換X11請求和響應格式，
      使其能夠被Wayland協議理解

  - `wayland-only` (支持waylay應用 | `不支持x11應用`)
    - sway : 提供了i3wm的所有功能，並完全兼容Wayland協議
    - hikari : 一個基於wlroots的合成器
    - niri : 一個支持捲動的，[視窗左右捲動效果demo](https://www.youtube.com/watch?v=yVH1dczKzsI)

  - `wayland + xwayland` (支持waylay應用 | `支持x11應用`)
    - hyperland
    - wayfire : 一個基於wlroots的3D合成器
    - river : 目標是輕量級和高效的合成器

## 適合用於 wayland 的套件

- `wayland相關`:
  - dbus = 進程間通信系統 (必要)
  - wl-clipboard = 提供剪貼板支持
  - wlogout = 一個用於退出 Sway 的工具  
  - xwayland：使 X 應用程序可以在 Wayland 上運行。

- `sway相關`:
  - sway = 基於 wayland 協議的合成器 (必要)
  - swaybg = 設定背景圖片
  - swayidle = 空閒時的自動鎖屏或休眠功能
  - swaylock = 提供鎖屏功能
  - 登入管理 (必要)
    - elogind = 用戶會話管理守護進程，用於提供管理會話和電源狀態
    - polkit = 系統服務，處理權限管理和身份驗證，允許`非特權進程`與`特權進程`通信，以執行特權操作
    - polkit-elogind = 與 elogind 集成的 polkit 代理
    - 三個套件需要一起搭配使用，用於非systemd的alpine環境

- `硬體相關`:
  - kanshi = 用於控制顯示器亮度和色溫
  - playerctl = 控制音頻播放器的命令行工具
  - pulsemixer = 一個用於調整音量的工具

- `截圖`:
  - grim = 截圖工具

- `錄屏`:
  - slurp = 複製屏幕內容的工具
  - wf-recorder = 錄制屏幕錄像
  
- `Wallpapers`:
  - hyprpaper 
  - swaybg
  - mpvpaper = 可在桌布撥放影片
  - swww = 動態桌布
  - mako = 一個用於生成壁紙的工具

- `Status bars`:
  - waybar
  - eww
  - Hybrid =  a GTK status bar mainly focused on wlroots compositors
  - Waypaper = 提供桌面選擇的GUI

- `App launcher`: 
  - wofi = a GTK-based customizable launcher
  - rofi for wayland = rofi fork for wayland
  - bemenu = Wayland-native replacement for dmenu
  - fuzzel = similar to rofi’s drun mode, with icons
  - tofi = an extremely fast and simple yet highly customizable dmenu/rofi replacement
  - walker =  app launcher with icons

- `其他`:
  - light = 一個輕量級的桌面環境，可提供桌面環境特有如自動化的工作區切換、快捷鍵設置、系統托盤等功能
  - yad = 一個用於創建簡單對話框的工具
  - thunar = 一個文件管理器
  - viewnior = 一個快速的圖片查看器
  - xfce-polkit = 提供系統管理權限的授權
  - xorg-xwayland = 允許在 Wayland 上運行 X11 應用程序
  - xdg-desktop-portal-wlr = 提供桌面門戶到 Wayland 的通訊

## 手動編譯 sway

- 手動編譯 sway 需要的依賴庫
  - wlroots = Wayland 的核心庫
  - wlc = Wayland 合成器庫，雖然 sway 已經不再依賴 wlc，但舊版本sway可能需要
  - wayland = Wayland 顯示服務器協議 (必要)
  - wayland-protocols = Wayland 協議
  - libevdev = 處理輸入設備
  - libinput = 處理輸入設備
  - libcap = 處理能力
  - pango = 文本布局和渲染庫
  - cairo = 二維圖形庫
  - gdk-pixbuf = 圖像加載和操作庫
  - pixman = 低級像素操作庫
  - json-c = JSON 處理庫

## [範例] 在alpine上透過setup-desktop安裝sway

參考，[使用setup-desktup安裝sway](../../alpinelinux/readme.md#安裝桌面系統)

## [範例] 在alpine上透過setup-desktop安裝sway

參考，[手動安裝sway](https://wiki.alpinelinux.org/wiki/Sway)

## ref

- sway
  - [sway的安裝與配置 @ archlinux](https://wiki.archlinuxcn.org/zh-tw/Sway)
  - [手動安裝sway，以alpine為例](../../alpinelinux/alpine-desktop.md#手動安裝-sway)

  - 配置範例，Sway Installation @ JustAGuy Linux
    - [y2b](https://www.youtube.com/watch?v=sKOWqAm70jc)
    - [source](https://github.com/drewgrif/sway)

  - 配置範例，[Sway on Arch Linux: 2023 Edition (From Scratch)](https://www.youtube.com/watch?v=QAmTUkzpIiM)
    - install : 
      - sway
      - swaybg = set background
      - foot = default gui terminal
      - polkit 

    - others :
      - adobe-source-code-pro-fonts
      - wofi = app launcher
      - waybar = top staus bar
      - ttf-font-awesome = icons

  - 配置範例，How I Set Up My Sway Window Manager on Debian 12
    - [video](https://www.youtube.com/watch?v=e7bezUA6G4g)
    - [configuration](https://www.learnlinux.tv/how-i-set-up-the-sway-window-manager-on-debian-12/)
    - install : 
      - sway
      - swaybg = set background
      - swayimg = 用於 wayland的圖片顯示工具
      - swaylock = 鎖定屏幕
      - swayidle = 用戶長時間不活動後自動執行特定的操作，如鎖定屏幕、關閉顯示器或播放音頻提示
    
    - others :
      - alacritty = gui terminal
      - light = 修改顯示器亮度
      - waybar
      - wofi
      - fonts-font-awesome

- niri
  - [niri配置範例](https://github.com/tomysurya/dotfiles/blob/main/.config/niri/config.kdl)

- 適合用於 wayland 的套件
  - [Useful Utilities for hyperland](https://wiki.hyprland.org/Useful-Utilities/)
  - [awesome-wayland](https://github.com/rcalixte/awesome-wayland)
  - [Archcraft with Sway](https://wiki.archcraft.io/docs/wayland-compositors/sway)