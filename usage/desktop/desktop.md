## 桌面系統架構

- 名詞解釋
  - 圖形系統: 用於`驅動顯示器`，例如，XFree86 -> xorg -> wayland，且可以支援多種`輸入設備和輸出設備`
  - 遠端圖形框架(遠端桌面協議): 提供遠端用戶存取圖形系統，例如，ssh的x11-forward、rdp、vnc、xpra

- linux 的桌面系統是軟體，使用的是 Service / Client 架構

  開發桌面系統時，開發者希望這個視窗介面`不要與硬體有強列的相關性`，若具有強烈的相關性，
  就等於是一個作業系統，如此一來的桌面的應用性就會被侷限。
  
  因此在最開始開發時，桌面系統就是`以應用程式的概念來開發`，
  而不是作為作業系統的內建軟體來開發，因此桌面系統`對 Linux 來說僅僅是一個軟體`

- 桌面系統架構分為兩種
  - 基於 x11 架構
  - 基於 wayland 架構

- 基於 x11 的桌面系統架構

  xserver / xclient 專門指用於繪製視窗的服務端和客戶端
  ```mermaid
  flowchart LR
  
  subgraph xserver
    b1[display] <--> b2[xserver]
    b0[browser] <--> b2 
  end
  
  subgraph xclient
    b2 <--> b3[xclient] <--> b6[gui應用]
    b3 <--> b4[display-manager] <--> b5[desktop-environment]
    b3 <--> b7[window-manager]
  end
  ```

  <font color=blue>xserver / xclient 的識別 </font>

  - 區別: 執行應用程式的主機為 xclient 端，顯示畫面的為 xserver 端

  - `例如`: os 和 桌面在`同一個系統中` (在同一臺主機)

    無論是 linux-server 或 linux-desktop 的版本，大部分都已經內建了xserver，

    當`使用desktop`或`執行GUI應用`後，就會啟用 xclient 將畫面包裝為網路請求發送給xserver，
    xserver 收到繪製畫面的請求後，就會調用繪製的庫進行繪製，並顯示在顯示器上

    當 xclient 和 xserver 在同一臺主機時，xclient 可以直接透過 `localhost` 對 xserver 進行畫面的請求

  - `例如`: os 和 桌面在`不同系統`中 (虛擬主機或遠端主機)

    常見的使用場景，用戶透過ssh連接到vm或遠端主機，並在遠端執行GUI應用程式，此時，遠端主機會透過 xclient 嚮用戶所在的 xserver 傳遞繪製畫面的請求

    因此，遠端主機需要知道用戶的IP才能使畫面正常顯示，一般需要在遠端主機上設置`DISPLAY`環境變數設置 xserver 的位址

    範例見，[透過ssh執行gui程式](https://gitlab.com/anthony076/docker-projects/-/blob/main/ssh_proxy/doc/%E9%80%8F%E9%81%8Essh%E5%9F%B7%E8%A1%8Cgui%E7%9A%84%E6%87%89%E7%94%A8-x11.md)

- 基於 wayland 的桌面系統架構

  - X11
    - 概念 : 採用客戶端-服務器模型，客戶端負責採集數據圖像等，然後發送給服務器進行處理。所有的處理都發生在服務器端。
    - 流程 : 客戶端向服務器發送請求，服務器處理後再返回結果給客戶端。這種模式下，服務器承擔了大量的處理負擔。
    - 安全 : 由於客戶端和服務器之間的頻繁通信，可能存在信息洩露的風險。例如，未加密的通信可能被其他程序截獲。
    - 網絡透明性 : 支持網絡透明性，即可以通過網絡傳輸窗口內容到其他機器上顯示。
    - 兼容性 : 支持廣泛的舊有應用程序，通過XWayland可以在Wayland下運行X11應用程序。

  - Wayland
    - 概念 : 圖形計算的計算處理等，都放在客戶端端，服務器只負責混合圖形等。
    - 流程 : 客戶端直接與顯示設備交互，通過合成器進行圖形混合。這種模式簡化了整個圖形堆棧，提高了效率。
    - 安全 : 每個客戶端都運行在自己的沙箱中，通過合成器進行圖形混合，減少了潛在的安全風險。
    - 網絡透明性 : 網絡透明性被剝離，所有的圖形處理都在本地進行，不支持跨網絡顯示。
    - 兼容性 : 原生支持的應用程序較少，但通過XWayland可以運行X11應用程序。

## 桌面系統的組成元件 (軟體的選擇)

- wm 是桌面系統的另外一種實現方式，本質和de相同，是簡化版的de

  `通常 wm(window-manager) 或 de(desktop-environment) 就可以獨立運作`，
  dm(display-manager) 依個人需求選用，但通常 de 會同時包含 wm 和 dm

  根據架構選擇要安裝的工具包
  - 若使用x11架構，需要xorg，`xorg` 是提供x11協議相關功能的工具包
  - 若使用waylang架構，需要wayland，`wayland` 是提供Wayland協議相關功能的工具包

- `組件`，device-manager，管理與桌面系統連接的硬體

  桌面系統通常會有管理和配置硬體的需求，需要 device-manager 提供以下的功能
  - 複雜的設備規則管理
  - 事件處理和通知 : 對設備的添加、移除或變更作出及時響應
  - 設備持久性 : USB 设备每次连接时都应该挂载到相同的位置
  - 複雜的設備依賴管理 : 多块硬盘组成的 RAID 阵列、网络设备的依賴管理
  - 調試和診斷工具
  
  例如，偵測USB裝置的熱拔插，外接硬碟、打印設備...等，
  桌面系統連接的設備類型繁多，因此，通常需要具有高階功能的 device-manager 
  提供複雜的規則來正確管理這些設備，確保它們能夠被正確識別和配置
  
  以 alpinelinux 為例，內建的device-manager，mdev或mdevd，功能過於簡單，
  無法提供複雜顯示架構的需求，因此一般建議使用的 device-manager 為
  - 若系統`使用 systemd`，推薦 device-manager 可使用 `udev`
  - 若系統`不使用 systemd`，推薦 device-manager 可使用 `eudev`
  - 實務上，device-manager會選擇與該架構的依賴庫來決定，例如，
    - sway，使用libinput庫來處理輸入設備，而libinput庫通常依賴於 udev 提供的設備事件和屬性信息
    - sway，預設系統使用 udev 來管理設備，以獲取所需的設備節點或屬性信息

- `組件`，dbus，管理桌面系統中應用與應用、應用與系統之間的通訊

  dbus主要提供以下的功能
  - 進程間通信(IPC)，

    dbus 提供了一個簡單且高效的機制，讓桌面應用程序和系統服務可以頻繁地相互通信

  - 系統和會話總線

    <font color=blue>系統總線</font>
    
    dbus的系統總線，提供與系統級別服務的通信，如網絡管理、硬件事件等。系統服務（如網絡管理器、藍牙守護進程）通過系統總線與應用程序通信

    <font color=blue>會話總線</font>

    dbus的會話總線，用於用戶會話級別的通信，例如，桌面應用程序之間的交互，一個應用程序可以通過會話總線向通知守護進程發送通知消息

  - 服務發現和發布

    允許應用程序和服務註冊自己，其他應用程序可以發現並與之通信

  - 安全性和權限控制

    dbus 提供了一種安全的通信方式，支持細粒度的權限控制。
    應用程序和服務可以根據配置文件定義允許哪些通信和操作。這對於保護系統和用戶數據的安全性至關重要。
    
  - 事件通知和廣播

    例如電池電量變化、網絡連接狀態變化等。
    dbus 提供了一個機制，可以廣播這些事件通知，確保相關應用程序及時響應。

  - 標準化接口

    dbus 定義了一套標準化的接口，使不同的桌面環境和應用程序能夠互操作。例如，dbus 常用於與 systemd、NetworkManager、PulseAudio 等系統組件進行交互。

- `組件`，dm(display-manager)，登入顯示管理
  - 功能
    - 提供桌面系統的登入
    - 桌面系統初始化
    - 切換桌面系統
  
  - dm 的選擇
    - xdm       : for x11
    - gdm       : for GNOME-Desktop
    - lxdm      : for LDE-Desktop
    - sddm      : for KDE-Plasma-Desktop、LXQt-Desktop
    - lightdm   : 支援多種桌面
    - slim      : 支援多種桌面

- `組件`，de(desktop-environment)，桌面系統
  - 包含桌面常用的一系列工具，自帶 wm + dm
  - 視窗排佈採浮動式排列，適合滑鼠操作
  - 佔用資源多
  - de 的選擇
    - openbox
    - gnome
    - xfce4
    - kde
    - kde-plasma
    - lxde
    - lxqt

- `組件`，wm(window-manager)，視窗管理器
  - 管理多個視窗，隻有WM，沒有桌面環境
  - 視窗採平鋪式排列，適合鍵盤操作
    - 有預設 layout: dwm、xmonad、bspwm
    - 沒有預設 layout: i3wm、sway
  - 佔用資源少

  - wm 的選擇
    - openbox: 
      - stacking window manager
      - 輕量、功能強大、高度可配置
      - 內建滑鼠右鍵快速啟動程式
      - 有 obconf 可透過 GUI 設置桌面的各種參數
      - [設置參考](https://www.youtube.com/watch?v=r5HzpWK7SBY)
    
    - wayfire:
      - 使用wayland協議取代x11協議，效能高
      - 缺，部分套件可能不支援，例如，flameshot、smplayer、feh、
      - 缺，在 window 上沒有 wayland-server，無法在原生win透過 ssh 傳送畫面，需要改用 wsl
    
    - awesome: 推薦
      - c實現，lua配置
      - dwm 的分支
      - 支援桌面環境最小化、視窗標題可隱藏、滑鼠縮放視窗
      - 自帶 bar、系統托盤
      - 每個螢幕都有獨立的工作區
    
    - bspwm: 推薦
      - 手動 layout
      - 使用 shell 配置，配置簡單
      - 純C實現，性能優秀，資源佔用低
      - https://www.youtube.com/watch?v=uiJasZWXumk

    - leftwm:
      - 使用 rust 編寫的 wm
      - 支援多屏
      - 使用配置文件
    
    - dwm:
      - C實現，性能優秀，資源佔用低
      - 透過修改源碼進行配置
      - 有預設 layout
      - 優，客製化程度高、穩定、性能高
      - 缺，可透過 patch 簡化配置，但後面的 patch 有可能修改前面的 patch
    
    - xmonad:
      - 透過修改源碼(haskell)進行配置
      - 缺，需要安裝 haskell 的依賴，
      - 缺，性能一般
    
    - qtile:
      - 透過修改源碼(python)進行配置
      - 性能比不上用C實現的wm
      - 內存佔用較多
      - https://www.youtube.com/watch?v=K04J7r4kwKQ
    
    - twm:
    
    - i3wm:
      - 透過修改配置文件進行配置
      - 用戶決定 layout
      - 文檔詳盡、用戶多、穩定、兼容性佳、生態好


## [xorg] X11 基礎

- x11 的架構
  - 圖解
    <img src="doc/x11-architecture.png" width=800 height=auto>

  - x-server (例如，windows 上的vcxsrv)
  - x-client
  - compositor (例如，picom)

    合成器用於畫面需要高階的顯示功能，例如，需要透明度、過渡動畫、垂直同步和類似的美學特徵時，
    才會使用到合成器。
    
    合成器不是必需的，但是是高階顯示功能必要的

    de通常會內建自己的 compositor，wm通常需要手動安裝
    
- x11 相關套件
  - `xorg套件`: x11 的工具包，存放 xserver/xclient 和x11所有相關的工具，
    包含 xorg-xinit 和 xorg-xauth，使用方便但佔用空間大

  - `xorg-xinit套件`: 在`在地端螢幕`顯示GUI時使用，用於啟動X圖形系統的工具
    
    x-server的啓動程式，用於初始化 x-server，並驅動在地端的顯示介面和設置環境變量，
    此套件也會提供 starx 命令，用於讀取配置後，將啟動參數傳遞給 xinit，
    再由 xinit 驅動顯示

  - `xorg-xauth套件`: 在`遠端螢幕`顯示GUI時使用，用於驗證 x-server和xclient之間的連接

    是x11的認證工具，用於嚮x-server發送 auth-token，允許 x-client 連接到 x-server 並顯示GUI畫面

  - <font color=red>xorg-xinit 和 xorg-xauth 的選擇</font>
    - 一般狀況下 xorg-xinit 用於本地端顯示，xorg-xauth 用於SSH遠端顯示，二選一安裝，
      但只有需要本地+遠端才需要同時安裝 xorg-xinit + xorg-xauth，但不是遠端顯示都會用到 xorg-xauth

    - 若系統可`直接存取`顯示器，`選擇 xorg-xinit`
    - 若使用 `SSH` 遠端存取顯示器，只`需要 xorg-xauth`，限定SSH的遠端桌面連線
    - 若使用 `rdp` 遠端存取顯示器，需要 xorgxrdp 轉發給本地的 xserver，所以`需要 xorg-xinit`
    - 若使用 `vnc` 遠端存取顯示器，`不強制依賴 xorg-xauth`，若需要共享X應用程式的cookies，就需要安裝 xorg-xauth

- 啟動桌面概述

  - /etc/X11/xorg.conf 和 /etc/X11/xinit/xinitrc 的關係
    - /etc/X11/xorg.conf 是 Xorg 服務器的硬體設定，也是 Xorg 服務器的主要配置，
      包含了有關 X Window System 服務的各種設定，例如輸入設備、顯示器、分辨率、顔色深度、驅動程式等，
      當啓動 X Window System 時，Xorg 服務器將加載並使用該文件中的設定來配置係統硬體和圖形介面
    - /etc/X11/xinit/xinitrc 文件是
      - 用於啓動 X Window System 的初始化腳本文件，
      - 運行 startx 命令時，它將檢查該文件並執行其中的命令
      - xinitrc 會設置 DISPALY 變數，啟動 WM 或 DE 的命令應該放在此處，不應該獨立運行

  - 狀況1，開機進入桌面的流程，使用 Display-Manager 啟動桌面，以 lightdm 為例
    - graphical.target 服務啟用後，啟用 lightdm 提供的服務
    - lightdm 提供GUI介面供使用者輸入帳號密碼，
    - 輸入後，lightdm 根據 /etc/lightdm/Xsession 目錄下的 *.desktop 啟動指定的桌面，
      
      開機後，預設會在 /etc/lightdm/Xsession 讀取默認的 desktop，
      並將 /usr/share/xsessions 下的 desktop 作為可選項，以提供使用者選擇
      
    - 若有多個桌面係統，可以透過 /etc/lightdm/lightdm.conf 修改預設值

  - 狀況2，透過 startx 手動啟動桌面，不使用 Display-Manager
    執行 ~/.xinitrc
    若 ~/.xinitrc 不存在，檢查 /etc/X11/xinit/xinitrc
    不會執行 /etc/lightdm/Xsession 目錄的內容

  - 狀況3，遠端進入遠端桌面的流程，例如，透過 XDMCP 啟用遠端桌面
    執行 ~/.xsession
    https://shyuanliang.blogspot.com/2012/07/linux-xdmcp-windows.html

  - 本地端 x11 的啟動方式
    - `方法1`，透過 `startx命令` 啟動
      
      startx 實際上是 shell 檔，會讀取或建立 `xclient/xserver的配置檔` 後，
      調用 xinit命令 來啟動桌面系統

    - `方法2`，透過 systemd 的 `$ systemctl isolate graphical.target` 進行切換
      
      透過此方式，在開機後系統可以進入桌面系統，而不需要手動輸入 startx 命令
      
  - 遠端 x11 的啟動方式
    - 只需要安裝 xorg-xauth

    - 此方式 x-server在遠端(在ssh-client端)，因此，x-server端(ssh-client端)需要啟動x-server

      注意，windows 上的 vcxsrv `不要以 multiple-windows 開啟`，背景會破碎化，
      且隻有作用的視窗有控製權，因此改用 one-large-window 開啟

    - 在 x-client端(ssh-server端) 啟動 sshd-server
      
      x-server端(ssh-client端)會透過 ssh -X 或 ss -Y 告知 x-server 的位置，

    - 當 x-client端(ssh-server端) 執行GUI應用時，x-client 會調用 xauth 並發送請求給 x-server

- xclient 與 xserver 進行通訊的端口

  xserver 可以啟動多個不同的終端機給用戶使用，
  | 第n的顯示畫面 | 介面編號    | 終端機 | 連接埠    |
  | ------------- | ----------- | ------ | --------- |
  | 第1個顯示畫面 | xserverIP:0 | tty2   | port 6000 |
  | 第2個顯示畫面 | xserverIP:1 | tty3   | port 6001 |
  | 第3個顯示畫面 | xserverIP:2 | tty4   | port 6002 |

- 常用檔案
  - `檔案1`，xinit命令，用於啟動桌面系統，
    - 注意，不推薦直接手動調用 xinit命令，通常由 `startx命令` 調用，startx 會讀取配置並傳遞給 xinit 命令，可簡化xinit的使用
    - 語法，xinit <client端參數> -- <server端參數>
    - 例如，`$ xinit xterm -geometry +1+1 -n login -display :0 -- X :0`

  - `檔案2`，xserver 的配置檔，`.xserverrc`
    - 若沒有安裝桌面系統，會利用此檔案啟動簡易的終端畫面給使用者使用
    - 由於是 server/client 架構，server 可以同時接受多個 client 連線，

      利用 :n 的參數，可以分配不同的終端給連接的用戶，n 代表第n個顯示介面
      - :0 指 tty2
      - :1 指 tty3

  - `檔案3`，xclient 的配置檔，`.xinitrc`
    - 若有安裝桌面系統，會利用此檔案啟動 display-manager 和 桌面系統

  - 配置檔的尋找順序
    - 路徑1，startx命令後面的參數
    - 路徑2，家目錄，例如，`~/.xserverrc` 或 `~/.xinitrc`
    - 路徑3，`/etc/X11/xinit/`，例如，`/etc/X11/xinit/.xserverrc` 或 `/etc/X11/xinit/.xinitrc`
    - 優先權: 路徑1 > 路徑2 > 路徑3

- 幾個與顯示器相關的驅動
  - `xf86-video-fbdev`
    - 允許 Xorg 服務器在沒有特定硬體驅動程式的情況下使用 Linux 內核的通用幀緩沖設備（Framebuffer Device）來顯示圖形介面
    - 適用於 virtualbox，或在 virtualbox 使用 VirtualBox Guest Additions 安裝顯卡驅動
    - 性能低，不會針對硬體優化，因此不支持硬體加速
    - 有限的分辨率支持

  - `xf86-video-vmware`
    - 開源的 X Window System 顯示驅動程式，用於 VMware 虛擬機中的虛擬硬體
    - 支援 2D 和 3D 加速
    - 支援多個顯示器
    - 適用於 vmware

  - `xf86-video-intel`
    - 針對 Intel 集成顯卡的開源 X Window System 顯示驅動程式
    - 提供 2D 和 3D 加速
    - 優化了電源管理
  
  - `xf86-video-amdgpu`
    - AMD 顯卡的開源 X Window System 顯示驅動程式
    - 提供 2D 和 3D 加速
    - 支援多個顯示器
  
  - `xf86-video-nouveau`
    - 用於 NVIDIA 顯卡的開源 X Window System 顯示驅動程式
    - 2D 和 3D 加速
    - 支援多個顯示器
  
  - `xf86-video-vesa`
    - 通用的 X Window System 顯示驅動程式，適用於幾乎所有硬體。
    - 提供了基本的 2D 圖形支援，但不支援 3D 加速和高級特性

- dbus庫(desktop-bus)，桌面應用中的IPC通訊系統
  - 作用: 是IPC的其中一種實現，用於在桌面系統中，不同桌面組件之間的通訊

  - 兩個 dbus 庫: `dbus` 和 `dbus-x11`
    - `區別`，
      - 在`本地端`使用桌面系統(de或wm)，只需要 `dbus 庫`
      - 若使用 rdp 等`遠端連接的桌面`時，必須要安裝 `dbus-x11 庫` 
      - dbus-x11 庫是基於 dbus 庫，但額外添加 x11 的功能，
        dbus-x11 可以取代 dbus 的功能，`不需要兩個都安裝`

    - `dbus庫`

      在本地端使用桌面系統時，dbus 會透過`環境變數`，讓不同的程序進行通訊，
      因此本地端使用桌面系統時，只需要dbus庫，而不是 dbus-x11 庫

    - `dbus-x11庫`
      
      通過 RDP 連接到遠程桌面時，需要將 xserver的輸入和輸出，經網絡傳遞給客戶端，
      在傳遞的過程中，會導致環境變量和session的訊息丟失，

      例如，dbus 是透過 root 帳號啟動，但是 rdp 是透過一般user進行連接，
      不同帳號無法存取同一個環境變數

      因此，需要透過 dbus-x11 來確保應用程序可以正常地使用 D-Bus 總線，
      dbus-x11 庫，是 D-Bus 的 X11 後端，為 X11 應用程序提供了 D-Bus 通信的支持。

- 透過 /etc/X11/xorg.conf 配置要使用的驅動程式
  - 注意，一般情況下不需要手動配置 xorg.conf，Xorg 服務器在啟動時，可以自動探測和配置系統硬體
  - 注意，若使用正確的顯示驅動，只需要指定一種顯示驅動

  - 配置範例
    ```shell
    
    # 啟用3D加速 
    Section "Module"
    Load "glx"
    EndSection

    # 使用 xf86-video-vmware 驅動
    Section "Device"
      Identifier  "VMware Graphics"
      Driver      "vmware"
    EndSection

    # 使用 xf86-video-fbdev 驅動
    Section "Device"
      Identifier "fbdev"
      Driver "fbdev"
    EndSection
    ```

## x11 之外的其他選擇

- x11 的問題: ssh-x11-forward 在大型畫面或需要時時更新畫面的應用，有明顯的延遲感

    大型畫面或需要時時更新畫面的應用，往往代表需要`傳輸大量的數據`，而SSH傳輸數據又需要`加解密`而造成畫面傳輸的延遲，
    且透過網路傳輸畫面，還需要經過`網路協議的層層處理`，再加上`網路的波動`
    
    即使透過 SSH -C 將數據量壓縮以減少傳輸量，仍然有明顯的延遲感

    - `原因1`，x11 是過時的協議，x11 會將任何的變動重新傳輸，而不是只重繪有變化的內容

    - `原因2`，SSH 大量使用CPU支援，且不支援多線程

    - `原因3`，不支援硬體加速的應用
      
      對於靜態，更新率不是那麼頻繁的應用，x11 可以適應得很好，
      對於動態的影片，在原因1+原因2的場景下，chrome 可以達到堪用的程度

      對於不支援硬體加速的應用，例如，firefox，在 x11 上執行 firefox 就達到災難的程度，
      對於此類應用只能改用 x2go 或 VNC 或 NoMachine NX

- x11 之外其他選擇

  - `x11架構的改進`
    - wayland
    - xpra: 進階版的 ssh-x11-forwarding，解決自動斷線的問題，支援瀏覽器瀏覽，但資源少，安裝體積大

  - `透過瀏覽器傳輸畫面`:
    - Broadway: client 端使用既有的瀏覽器瀏覽畫面，不需要安裝圖形服務器(x-server)，只支援 gtk 的應用
    - noVNC
    - xpra
    - [Guacamole](https://cayden.blog.csdn.net/article/details/106609133)

  - `nx-protocol`: 
    - 例如: nomachine、freenx、x2go
    - 歷史: x11 -> DXPC -> NoMachine (商業軟體，閉源) -> freeNX (開源)
    - 獨立的壓縮技術(NX1)，並使用 ssh 協定來連線到遠端服務
    - X2Go 提供了更多的功能
      - 多用戶支援、多語言支援、詳細的文檔和幫助，支援更多操作系統
      - X2Go 的帶寬要求較低，因此在網路帶寬受限的環境中更為適用。
    - FreeNX 則更註重穩定性和性能

  - `rdp-protocol`: 
    - 例如: tightvnc、tigervnc、x11vnc、vnc4server、novnc(把client端以瀏覽器取代)

      注意，x11vnc 使用 X11 請求來獲取螢幕畫面，
      可通過Composite/Damage擴展或僅通過按時間間隔執行 GetImage 請求獲得畫面，
      並將其與在地副本進行比較後，再透過 rdp 進行傳輸

    - windows 默認的遠端桌面協議
    - 只把有變化的螢幕區塊到client端，傳輸繪製`指令`
    - 傳輸量比rfb協定小，但對客戶端的硬體需求大，適合低速網路下的高階設備
    
    - 比較
      - 依延遲分類，
        - TigerVNC/TightVNC > X11VNC、vnc4server > NoVNC
        - NoVNC 方便使用，不需要額外安裝客戶端軟體，但需要多一層瀏覽器的使用，延遲略高

      - 依功能性，
        - tigervnc提供更多功能，內建 xvnc 可以創建虛擬顯示器，用於需要渲染的GUI應用，
          [範例](https://blog.csdn.net/Nobody_Wang/article/details/60887659)

  - `rfb-protocol`:
    - vnc 系列用的傳輸協定
    - 只把有變化的螢幕區塊到client端，傳輸繪製`畫面`
    - 傳輸量比rfb協定大，但對客戶端的硬體需求小，適合高速網路下的低階設備
    - 畫面更新是由client端所驅動，server負責計算變化區塊、壓縮等工作

      當client端網路較差時，可以在client繪完圖後再要求更新，
      或是依網路情況調整更新速度，給予RFB協定一定程度的彈性

  - 比較
    - 依傳輸畫面效能: `rfb > x11 > rdp > nx`，rfb最單純隻傳輸畫面
    - 依延遲性: `rdp > nx > rfb > x11`，rdp延遲性最低
    - 依傳輸畫面效能+延遲性: `rdp > nx > rfb > x11`

## 虛擬顯示器 和 遠程控制圖形介面的差別

- 提供虛擬顯示的軟體，例如，
  - xvfb: 輕量、支援創建虛擬顯示器、支援虛擬桌面，僅用於虛擬顯示器
  - Xvfb-run: 用於在服務器上運行 Xvfb，方便啓動
  - Xvnc: 是一個 VNC 服務器，可以與 Xvfb 一起使用，用於遠程+虛擬顯示器
  - Xephyr: 用於創建虛擬桌面，可以在 Xvfb 的上層運行，用於遠程+虛擬顯示器

- `相同`: 兩者都可以用來顯示GUI應用的畫面

- `差異1`: 遠程控制圖形介面不涉及硬體渲染環境

  硬體渲染環境，代表支援圖形硬體加速，支援圖形介面的渲染，支援圖形圖像的顯示等功能，
  部分GUI應用需要調用顯示器的硬體渲染功能，`遠程控制圖形介面是不具有硬體渲染環境的`

- `差異2`: 虛擬顯示器並`不提供遠程存取`的功能
  
  使只有安裝 xvfb，安裝GUI應用時不會報錯，但使用者看不到正在執行的圖形測試，

  但若遠端用戶想要看到圖形介面，仍然需要透過`遠程控制圖形介面`的軟體

- `狀況1`: 使用 xvfb 創建了虛擬顯示器的功能，使得服務器上運行selenium自動化測試
  
  當 server 不具有實際的螢幕時，可透過 xvfb 建立虛擬顯示器，讓程式誤以為有顯示器，
  以便在沒有實際顯示設備的情況下進行圖形測試和自動化任務，

  實際運行時，圖形測試和自動化任務會正常執行，但使用者看不到實際畫面

- `狀況2`: 遠程控制圖形介面無法取代 xvfb 的功能

  若在 server 上只有設置VNC，但沒有設置 xvfb，也沒有實體的顯示器，
  遠程控制圖形介面是無法取代 xvfb 的功能的

## 遠端桌面和本機桌面，設置環境變數的差異

- 在本機透過 startx 啟動桌面系統(de或wm)，

  通常會在 `~/.bashrc` 或者 `~/.bash_profile` 文件中`設置用戶環境變量`，但是這些文件只有`在用戶通過 shell 登錄時才會被讀取和執行`

- 遠端透過 vnc 或 rdp 啟動桌面系統(de或dm)，
  
  遠端用戶在登入shell時，並不會執行 `~/.bashrc` 或者 `~/.bash_profile` 
  文件，

  RDP 會話(session)的環境變量，是`在登錄時由 PAM 程序設置的`。
  因此，如果需要在 RDP 會話中設置環境變量，應該在 PAM 配置文件中進行設置。

  PAM 為 Pluggable Authentication Modules，是一個 Linux/Unix 系統中的身份驗證框架，它允許系統管理員`在不修改應用程序的情況下更改認證方式`。
  
  PAM 的設計思想是將`身份驗證和授權過程`與`應用程序的實現`分離開來，使得不同的身份驗證方法可以被統一管理，而不必在每個應用程序中都編寫身份驗證代碼。

  pam_env 模塊，會從 `/etc/environment` 或者 `/etc/security/pam_env.conf` 文件中讀取環境變量，並將它們添加到當前會話的環境變量中。

- 設置環境變數要注意啟動的環境
  
  例如，利用 rdp 遠端啟用 bspwm 的環境中，當遠端使用者連接到 xrdp 時，
  - 環境1: xrdp 的 startwm.sh 會啟動 bspwm 的桌面環境，且 `startwm.sh` 設置了 `AA=123` 的環境變數
  - 環境2: bspwm 啟動前，會透過 `bspwmrc` 進行初始化，也可以在 bspwmrc中設置任意的環境變數
  - 環境3: 進入bspwm，bspwm 會透過 `sxhkdrc` 綁定了啟動 alacritty 的按鍵，由 sxhkdrc 啟動 alacritty
  - 最後，在 alacritty 執行 `$ echo $AA` 是空的，因為 `AA=123` 只存在 startwm.sh 中，並不會傳遞給 alacritty，2者是不一樣的執行環境

## [範例] ssh + x11 + GUI應用/桌面系統

- 注意，<font color=red>在遠端主機或vm要執行gui應用，不需要安裝整個桌面系統</font>

- `狀況1`，SSH + GUI應用

  GUI應用隻會顯示單一視窗，可以透過 x11 的 DISPLAY 進行繪製轉發，
  使用也簡單，隻需要以下幾個步驟

  - step1，在本機上安裝並啟動 xserver(vcxsrv)
  - step2，在容器中設置 `DISPLAY變數`，
  - step3，在容器中執行 gui 應用

  - 完整範例，見 [在docker中使用x11](https://gitlab.com/anthony076/docker-projects/-/blob/main/docker/readme.md)

- `狀況2`，SSH + 桌面系統

  桌面系統需要透過 startx 的命令啟動，startx 會啟動在地端的 xserver，
  且 `xserver 會驅動在地端的顯示器`，因此隻適合在地端有實體螢幕的狀況下使用，

  因此 startx `不適合遠端的轉發整個桌面`或`不適合用於vm的場景中使用`

  <font color=blue>解決方法1，ssh-client 需要安裝 x-server</font>

  <font color=blue>解決方法2，不要使用 ssh，改用其他 protocol</font>

  可以改用 `vnc 或 rdp 或 x2go`，
  
  x2go 用於可遠端的xserver，與 vnc 和 rdp 類似，使用獨立的協定，
  提供比x11更好的轉發，且使用ssh連線確保全全，但效能較VNC慢，詳見 [Ref](#ref)

- `範例`，vagrant + 啟用 ssh-forward-x11
  - 完整範例，見 [packer-vbox-myarch v0.3](https://gitlab.com/anthony076/docker-projects/-/tree/main/packer/example/packer-vbox-myarch)

  - step1，配置 x11-client on sshd-server
    ```shell
    # 啟用 X11Forwarding yes
    sed -i 's/^#X11Forwarding no/X11Forwarding yes/1' /etc/ssh/sshd_config

    # 安裝 xauth
    pacman -S xorg-xauth

    # 建立 /home/vagrant/.Xauthority
    touch /home/vagrant/.Xauthority
    chown vagrant:vagrant /home/vagrant/.Xauthority

    # 安裝GUI套件，用於測試
    paru -S xorg-xeyes

    # 重啟 sshd 或重啟pc
    sudo reboot now
    ```

  - step2，配置 x11-server on window
    ```shell 
    # 在 windows 上打開 VcXsrv

    # ==== 狀況1，使用 ssh 進行連線 ====
    # 透過 -Y 參數，ssh 會嚮 VM 設置 DISPLAY 參數
    ssh -Y vagrant@localhost -p 2222

    # ==== 狀況2，使用 vagrant 進行 ssh 連線需要以下配置 ====
    # 透過以下設置，執行 vagrant ssh 命令時，才會傳送 -Y 參數

    # 在 Vagrantfile 中添加以下
    config.ssh.forward_agent = true
    config.ssh.forward_x11 = true

    # 使用 vagrant 進行 SSH 連線
    vagrant ssh

    # 測試
    xeyes
    ```

## [範例] 使用 rdp 顯示 virtualbox 中的 linux 桌面

- 概念
  - rdp 是 windows 開發用於連接到 window 主機的遠端桌面協議，
  
    若被連接端不是使用 windows 而是 linux，必需要藉助 `xorg` 或 `vnc` 才能連線到桌面(de/wm)
    - 若要借助 xorg，安裝 xorgxrdp，用於轉接 rdp 連線到 xorg-server
    - 若要借助 vnc，安裝 tigervnc-server 

- xrdp 的使用
  - `/etc/X11/xorg.conf`: 用於配置遠端連線的硬體參數，顯示器，滑鼠，鍵盤，觸摸板，GPU等硬體的設定信息
  - `/etc/X11/xinit/xinitrc`: 用於本地端透過 startx 啟動桌面的初始化腳本
  - `/etc/X11/Xwrapper.config`: xorg-server 的相關配置，在此範例中，xorgxrdp 會與 xorg-server 進行通訊
  - `/etc/X11/sess.int`: 用於配置 xrdp
  - `/etc/xrdp/startwm.sh`: xrdp 遠程桌面協議的啓動腳本

- `範例`，xrdp + xorgxrdp + xorg-server
  - 通訊流程: win-rdp -> xrdp -> xorgxrdp -> xorg-server -> window-manager

  - Virtualbox 環境設置
    - 使用 archinstall 安裝系統
    - 勾選 VMSVGA 並啟用 3D 加速
    - system > Motherboard > Enable APIC 
    - system > Processor > Enable PAE/NX
    - system > Acceleration > Enable Nested Paging
    - Display > Screen > VMSVGA + Enable 3D Acceleration

  - step1，安裝 openssh sudo paru

  - step2，安裝必要軟體
    ```shell
    # 基礎套件
    SW=(git base-devel)
    
    # virtualbox 的顯示器驅動
    # xf86-video-fbdev: 啟用
    SW+=(virtualbox-guest-utils xf86-video-fbdev)

    # x 系統相關
    #   xorg-xinit: x系統初始化和啟動器
    #   xorg-apps: x系統基礎套件
    #   xorg-twm: x系統預設登入器
    SW+=(xorg-server xorg-xinit)
    
    # rdp 相關
    #   xrdp: 在 linux 上的 rdp-server，用於接受 rdp-client 的連線
    #   xorgxrdp: 用於串接 xrdp 和 x-server
    SW+=(xrdp xorgxrdp)

    # window-manager 相關
    #   ttf-mononoki-nerd: 避免字型無法正確顯示
    #   xterm: awesome 預設終端
    SW+=(ttf-mononoki-nerd xterm awesome)

    paru -S "${SW[@]}" --noconfirm --needed
    ```

  - step3，(選用) 修改 `/etc/X11/xorg.conf`，添加以下
    - 經過測試，在使用正確的驅動下，即使不修改 xorg.conf 也能正常啟動
    - glx 用於 3D 圖形加速，一般會自動安裝，若找不到時，需要手動安裝 mesa 套件
    - 使用 `xf86-video-fbdev` 顯示驅動，堪用，但不是最好的驅動
    - 以下配置用於啟用的模組和加載的顯示器驅動

    ```shell
    Section "Module"
    Load "glx"
    EndSection

    Section "Device"
      Identifier  "fbdev"
      Driver      "fbdev"
    EndSection
    ```
 
  - step4，修改 `/etc/X11/xinit/xinitrc`
    ```shell
    ... 內容省略 ...

    # 禁用以下
    #twm &    # 改用 awesome ，禁用 twm 作為桌面系統

    #xclock -geometry 50x50-1+1 &
    #xterm -geometry 80x50+494+51 &
    #xterm -geometry 80x20+494-0 &

    #exec xterm -geometry 80x66+0+0 -name login
    
    # 在本地端可以透過 startx 啟動 awesome
    # 若不需要在本地端啟用 awesome 可以不加
    awesome
    ```
  
  - step5，(選用) 備份 starttwm.sh，`$ sudo cp /etc/xrdp/startwm.sh /etc/xrdp/startwm.sh.bak`

  - step6，修改 `/etc/xrdp/startwm.sh`
    ```shell
    # 透過 xrdp 啟動 awesome 時需要
    awesome
    ```
    
  - step7，一般用戶的配置

    建立 `/etc/X11/Xwrapper.config`，新增以下內容
    ```shell
    # https://askubuntu.com/questions/797973/error-problem-connecting-windows-10-rdp-into-xrdp
    allowed_users = anybody
    ```
      
  - step8，啟用服務
    - `$ sudo systemctl enable xrdp`
    - `$ sudo systemctl start xrdp`
    - `$ sudo systemctl enable vboxservice`
    - `$ sudo systemctl start vboxservice`

  - step9，(選用) 執行 startx 後重啟，確認 x-server 啟動沒有問題 (確認 xinitrc 的配置正確)
    - 在VM中調用 startx，注意，不要透過SSH 執行 startx
    - 執行，`$ startx`，若以上配置正確，xserver 會顯示 sucess 的字樣，並在虛擬機中顯示桌面

  - step10，使用 windows-rdp 進行連線，連線到 `127.0.0.2`
    - session-manager 選擇 xorg
    - 使用 `root` 或 `一般使用者登入`，使用系統設置的密碼

- `範例`，xrdp + x11vnc，配置較簡單，
  參考，[x11vnc+xrdp 配置linux遠程桌面](https://www.cnblogs.com/liq07lzucn/p/14460431.html)

## [範例] rdp 的配置範例

- [xrdp + xorgxrdp-glamor 硬體加速的配置範例](https://alvin.red/2021/11/06/archlinux-xrdp/)
- [xrdp 出現黑屏的問題](https://v2ex.com/t/846163)
- [archlinux官方對 xrdp 的使用說明](https://wiki.archlinux.org/title/xrdp)
- [freerdp的使用和配置更為簡單](https://superuser.com/questions/1509055/manjaro-kde-running-as-a-rdp-server)

## [範例] 在沒有顯示器的環境中，透過 xvfb 建立桌面系統

參考，[termux + alpine + awesome + vnc](../linux_on_termux/readme.md#範例proot-distro--alpine--vnc--awesome)

## Ref
- [桌面系統@鳥哥](https://linux.vbird.org/linux_basic/centos7/0590xwindow.php)
- [常見名詞解釋](https://www.zhihu.com/question/47160292)
- [xserverrc 和 xinitrc 檔案的解析](https://www.796t.com/p/162041.html)
- [How to Install KDE Plasma on Ubuntu 22.04](https://linuxhint.com/install-kde-plasma-ubuntu/)

- x2go
  - [x2go官網](https://wiki.x2go.org/doku.php)
  - [X2Go:X11 和 VNC 的替代品](https://www.nas.nasa.gov/hecc/support/kb/x2go-an-alternative-to-x11-and-vnc_651.html)
  - [使用x2go遠端連接ubuntu桌面](https://www.jianshu.com/p/354f328b8568)
  - [x2go遠程連接桌面](https://www.twblogs.net/a/5baaa6062b7177781a0e5a47)
  - [在Ubuntu 16.04與18.04上面安裝X2Go Server](https://peterli.website/%E5%A6%82%E4%BD%95%E5%9C%A8ubuntu-16-04%E8%88%8718-04%E4%B8%8A%E9%9D%A2%E5%AE%89%E8%A3%9Dx2go-server/)
  - [在 Linux 上使用 x2go 設置遠程桌面](https://read01.com/zh-tw/ADd6Aj.html#.Y5bZe3ZBxD8)

- 改善x11(x11的替代品)
  - [Make Remote Firefox Run Faster over SSH with X11 Forwarding and X2Go](https://foxpa.ws/make-remote-firefox-run-faster-over-ssh-with-x11-forwarding-and-x2go)

- dbus
  - [install rdp on ubuntu server](https://blog.csdn.net/lujun9972/article/details/105354207)
  - [dbus 1.12.10-2 breaks dbus-launch --exit-with-x11](https://bugs.archlinux.org/task/60179)
  - [Why isn’t dbus-x11 being installed on Manjaro](https://forum.manjaro.org/t/why-isnt-dbus-x11-being-installed/114726)