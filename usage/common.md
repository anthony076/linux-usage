## 常見文件屬性

- linux 中的一切都是文件，如果不是文件，那一定是正在運行的進程
  - linux 中的所有的硬件組件都表示為文件，系統使用這些文件來與硬件通信。
  - Linux 的目錄的重要功能，有結構性的分組存儲其它文件，以方便查找訪問。

- 透過 `$ ls -la` 可以查看詳細的檔案屬性，並以10bit顯示文件屬性和權限
  - 例如，drw-r--r--，其中，第一位的d代表文件屬性

  - `文件屬性為d`，當前文件為`目錄`

  - `文件屬性為-`，當前文件為`檔案`

  - `文件屬性為b`，當前文件為`塊文件(block)`
    - 通常指`隨機存儲設備`，例如，/dev/sda
    - 對訪問系統硬件部件提供了緩存接口。它們提供了一種通過文件系統與設備驅動通信的方法。
    - 通常會在指定時間內傳輸大塊的數據和信息

  - `文件屬性為c`，當前文件為`字符文件(character)`，
    - 通常指`字符設備`，指一次性讀取不能截斷輸出的設備，例如，/dev/tty
    - 訪問系統硬件組件提供了非緩衝串行接口。它們與設備的通信工作方式是一次只傳輸一個字符的數據

  - `文件屬性為l`，當前文件為`軟連結(link)`
  
  - `文件屬性為p`，當前文件為`管道(pipe)`
    - 將一個進程的輸出連接到另一個進程的輸入，從而允許進程間通信（IPC）的文件

  - `文件屬性為s`，當前文件為`socket`
    - 提供進程間通信方法的文件，它們能在運行在不同環境中的進程之間傳輸數據和信息
    - 可以為運行網絡上不同機器中的進程提供數據和信息傳輸

## 常見目錄

- `/bin目錄`：放置在單人維護模式下還能被操作的指令。
- `/boot目錄`：放置`開機`會使用文件，包括Linux核心文件以及開機菜單與開機所需配置文件等。
- `/dev目錄`：在Linux系統中，任何`設備`都是以文件的形態存在於這個目錄中。
- `/etc目錄`：系統中主要的`配置文件`幾乎都放置在這個目錄中，例如人員的賬號密碼、各種服務的啟動配置等。
  > editable-text-configuration
- `/lib目錄`：放置開機時會用到的`函數庫`和/bin或/sbin目錄下面的指令會調用的函數庫。
- `/media目錄`：放置`可移除的設備`。包括軟盤、光盤、DVD等。
- `/mnt目錄`：一般用來`暫時掛載`某些額外的設備。
- `/run目錄`：FHS 規定系統開機後，所`產生的各項信息`應該要放置到該目錄。
- `/srv目錄`：作為一些`網絡服務`啟動後需要取用的數據的目錄。
- `/tmp目錄`：一般使用者或者`正在執行的程序暫時存放文件`的地方。這個目錄是任何人都能夠存取的，所以需要定期的清理一下。
- `/usr目錄`：其下的目錄結構和根目錄相似，但根目錄中的文件多是系統級的文件，而/usr中是用戶級的文件，一般與具體的系統無關。
- `/opt目錄`：第三方軟件安裝目錄，也可以用來當做用戶軟件安裝目錄，不過一般用戶軟件還是會安裝到/usr/local目錄中。
- `/var目錄`：包括了一些`數據文件`，如係統日誌等。/var的存放使得/usr被只讀掛載成為可能。
- `/home目錄`：系統默認的使用者主文件夾。
- `/root目錄`：系統管理員(root)的主文件夾。
- `/lost+found目錄`：使用標準的ext2/ext3/ext4文件系統格式才會產生的一個目錄，
  目的在於當文件系統發生錯誤時，將一些遺失的片段放置到這個目錄下。

- `/proc目錄`：是一個虛擬文件系統（virtual filesystem），此目錄中的數據都是在內存當中，
  
  例如系統核心、進程信息、周邊設備的狀態及網絡狀態等等，`不佔硬盤容量`。

- `/sys目錄`：和 /proc 類似，也是一個虛擬的文件系統，主要也是記錄核心與系統硬件信息較相關的信息。
  
  包括目前已載入的核心模塊與核心偵測到的硬件設備信息等等，`不佔硬盤容量`。

## 常用命令

- 查詢文檔，`$ man 要查詢的命令` 或 `$ man 章節 要查詢的命令`
  - 搜尋，`/ 鍵`
  - 下一個搜尋結果，`n鍵`
  - 前一個搜尋結果，`shift+n鍵`

- 快入建立檔案但不編輯，`$ touch 檔案路徑`

- 查詢指令歷史紀錄，`$ history`

- 查看系統啟動過程的日誌，`$ dmsg`

- 查看已安裝的軟體，`$ dpkg -l`

- 列出當前目錄
  - `$ ls`
  - `$ ls -l`，顯示權限和隱藏目錄
  
- 建立軟連接，`$ ln -s 被連接的檔案路徑 新建軟連結檔名`
  - s: 建立軟連接，若不添加此參數，預設建立硬連接

- 壓縮/解壓縮
  - x: 解壓縮
  - c: 壓縮
  - f: 指定檔案
  - v: 顯示正在處理的檔案
  - 解壓縮範例: `$ tar xf 壓縮檔名.rar`，解壓縮(x)並指定壓縮檔
  - 壓縮範例: `$ tar cf 壓縮檔名.rar 目錄名`，壓縮(c)並指定壓縮前的目錄和壓縮檔

- 使修改的配置文件不需要重新登錄立即生效，`$ source 配置文件名`
  
- 將命令放到後台運行，`$ nohup 命令`
  - 範例，`$ nohup 命令 > myout.file 2>&1`，將命令放到後台運行，並將輸出重定向到 myout.file

- 查找命令位置，`$ whereis 命令`

- 查看目前系統已打開的文件，`$ lsof`
  
- 查看環境變數，`$ printenv`

- 查看 Linux Distribution 的版本，`$ lsb_release -a`

- 開啟 bash 的進階文件匹配模式，`$ shopt -s extglob`，注意，僅適用於 bash

  - 透過 `$ shopt`，可查看 bash 其他可用選項

  - `-s extglob`，會設置 extglob=on，代表啟用進階文件匹配模式，可以使用更多的通配符進行文件名匹配，例如，
    - @() 和 !() 通配符匹配一組文件名
    - +() 通配符匹配一個或多個文件名
    - ?() 通配符匹配零個或一個文件名

  - 範例
    ```shell
    $ touch 123 456 789.txt
    $ ls   # 123  456  789.txt
    
    # 未啟用進階文件匹配模式前，無法使用 !
    $ rm !(*.txt)  # -bash: !: event not found
    
    # 開啟進階文件匹配模式
    $ shopt -s extglob

    # 啟用進階文件匹配模式後，可以利用類似re的語法選擇文件
    # 刪除除了 txt 文件之外的所有文件
    $ rm !(*.txt)

    $ ls   # 789.txt
    ```

- 去除路徑中的目錄和後綴名，只保留檔案名，`$ basename 路徑`
  ```shell
  
  # 去除目錄，只保留檔名和副檔名
  basename /usr/local/bin/arch-config.sh    # 得到 arch-config.sh

  # 去除目錄+副檔名，只保留檔名
  # 語法，basename 路徑 要去除的副檔名字串
   basename /usr/local/bin/arch-config.sh .sh   # 得到 arch-config
  ```

- 建立臨時檔案，`$ mktemp`
  ```shell
  
  # mktemp命令，用於建立臨時檔案
  # XXXXX 是檔名的模板，mktemp 會將 XXXXX 替換為隨機字串，避免檔名發生衝突
  
  mktemp aa.XXXXX   # 得到 aa.IidGw 的臨時檔

  mktemp /tmp/aa.XXXXX   # 得到 /tmp/aa.IidGw 的臨時檔
  ```

- 複製檔案+變更檔案權限，`$ install`
  - 語法，install 選項 來源 目的
  - -D，當目標文件為一個目錄時，將源文件複製到目標目錄下
  - -m，指定權限
  - 範例，`install -Dm 755 /etc/myconfig.conf ~/myconfig.conf`

- 取消歡迎訊息，`$ vi /etc/motd`，motd = message of the day

- 替換特定字符，tr (translate)，
  - 語法，`tr 要尋找的目標 要替換的文字`
  - 範例，`$ echo "a,b,c" | tr "," " "`

## 自定義 shell command

- 環境
  - alpine linux
  - fish shell

- 建立 `~/myscript.fish`
  
  ```
  #!/usr/bin/fish

  function mykill
    ps aux | fzf --height 40% --layout=reverse | awk '{print $1}' | xargs -r doas kill
  end
  ```

  變更權限
  chmode +x ~/myscript.fish

- 將 myscript 添加到 fish 預設會自動加載的目錄中，`$ mv ~/myscript.fish ~/.config/fish/conf.d/myscript.fish`

  添加後，重新登入，或執行 `$ exec fish`，使 fish 重新加載

- 執行，在 fish 中，輸入 `$ mykill`

## 磁碟相關
見 [磁碟分割](../linux-core/00_disk-partition.md)

## 進程相關
- 返回指定應用的 pid，`$ pidof 應用名` 或 `$ pgrep -x 應用名`
- 刪除指定應用名的進程，`$ kill $(pidof 應用名)` 或 或 `$ pkill 應用名`
- 列出所有執行中的進程，`$ ps aux`
- 顯示進程樹，`$ pstree`
- 查看環境變數，`$ ps auxe`

## 網路相關
- 以下命令需要安裝 net-tools 套件
- 查看路由表，`$ route -n`
- 查看監聽的端口服務，`$ netstat -nlpt`
- 查看100端口的連接數，`$ netstat -nta | grep 100 | wc -l`

## 檔案處理
- 刪除資料夾，`$ rm -rf 目錄路徑`

## 帳號相關

- 參考，[sudo和doas的使用](../sw/sudo_doas.md)

## 更改檔案權限 / 更改檔案擁有者
- 解讀檔案權限
  
  透過 `$ ls -la`，顯示完整的檔案權限

  ```shell
  -rw-r--r--
  ^           檔案類型(1bit): d代表目錄 -代表檔案
   ^^^       主權限(3bit)
      ^^^     同群組權限(3bit)
         ^^^  其他群組權限(3bit)
  
  3bit的權限分別代表 = (r:讀取權限)(w:寫入權限)(x:執行權限)
  - 可讀(r=1) / 不可寫(w=0) / 不可執行(x=0): 100 = 4
  - 可讀(r=1) / 可寫(w=1)   / 不可執行(x=0): 110 = 6
  - 可讀(r=1) / 可寫(w=1)   / 可執行(x=1): 111 = 7

  主權限 = rw- = 110 = 6，代表可讀可寫不可執行
  同群組權限 = r-- = 100 = 4，代表可讀不可寫不可執行
  不同群組權限 = r-- = 100 = 4，代表可讀不可寫不可執行

  因此 
  -rw-r--r-- 代表
  (-)當前路徑為檔案
  (rw-)檔案擁有者，可讀可寫不可執行
  (r--)同群組使用者，可讀不可寫不可執行
  (r--)非同群組使用者，可讀不可寫不可執行
  ```
  
  - 語法，`$ chmod 權限 <指定檔案或目錄>`
    - 範例，`$ chmod 664 ttt.sh`，手動添加權限 (-rw-rw-r--)
    - 範例，`$ chmod +r ttt.sh`，將所有讀取位設置為1 (若 -rw-----w- ，變成 -rw-r--rw-)
    - 範例，`$ chmod +w ttt.sh`，將所有寫入位設置為0 (若 -rw-r--rw- ，變成 -r--r--r--)
    - 範例，`$ chmod +rw ttt.sh`，將所有讀取位/寫入位都設置為1
  
- 更改檔案擁有者，`$ sudo chown 用戶 檔案`

- 更改檔案權限
  - 範例，為所有欄位添加可寫權限，`$ chmod +w file`
  - 範例，以數字型態修改權限，`$ chmod 755 file.txt`，不指定特殊權限
  - 範例，添加特殊權限，`$ chmod 6750 file.txt`
    - 6: 特殊權限位 ( setuid | setgid | sticky bit )，
      
      例如，6 = 110 = setuid | setgid
      例如，7 = 111 = setuid | setgid | sticky bit

    - 7: 擁有者權限位
    - 5: 同群組權限位
    - 0: 不同群組權限位

## 互動命令的自動化安裝

- 方法1，利用 echo 和 | 實現

  - 格式: echo -e "輸入值1\n輸入值2\n輸入值3" | 互動式命令
      - `-e` 表示選項允許轉義字符
      - 透過 | 將互動式命令需要的輸入值依序傳遞給命令

  - 範例，`$ echo -e "ching" | sudo pwd`

  - 範例，`$ echo -e 'vagrant\nvagrant' | /usr/bin/passwd vagrant`

  - 範例，以建立磁碟分區為例
    ```shell
    echo "n\np\n1\n2048\n+1048576\nw" | fdisk /dev/sda
    ```

- 方法2，透過 `命令 << EOF ... EOF`
  - 範例，以變更密碼為例
    ```shell
    passwd aaa << EOF
    aa
    123
    EOF
    ```

  - 範例，以建立磁碟分區為例
    ```shell

    # n         // add new partition
    # p         // primary-partition
    # 1         // partition-number, ex sda1
    # 2048      // start-sector
    # +1048576  // end-sector
    # w         // write and exit

    fdisk /dev/sda << EOF
    n
    p
    1
    2048
    +1048576
    w
    EOF
    ```

- 方法3，只透過管道命令 (只適用於所有使用者輸入的內容都一樣)，不推薦，限制多
  ```shell
  # 執行 pacman -Scc，所有的使用者輸入都輸入 yes
  yes | pacman -Scc
  ```

- 方法4，透過 [exptec](expect.md) 套件

## 開發相關

- 查看已安裝的 Library，`$ /sbin/ldconfig -p`

- 查看命令依賴的庫，`$ ldd 命令路徑`
  > 注意，必須使用絕對路徑

## 軟連結/硬連結

- 每個檔案都有自己的檔案系統中唯一的識別號，稱為inode

  透過 `$ ls -li` 或 `$ stat 檔案` 可查看 inode 值

  | 比較項           | 硬連結                                        | 軟連結                                |
  | ---------------- | --------------------------------------------- | ------------------------------------- |
  | 建立語法         | ln                                            | ln -s                                 |
  | 別名             |                                               | 又稱為符號連結(路徑連結) ，同 windows |
  | inode            | 共用inode，建立一個硬連接，原始檔案的計數值+1 | 不共用 inode，有自己的 inode          |
  | 是否需要原始檔案 | 需要原始檔案存在                              | 不需要，可指向null                    |
  | 限制             | 不能對目錄建立硬連接                          | 可對目錄建立軟連接                    |
  | 優點             | 不可跨文件系統                                | 可跨文件系統，可連接不存在的檔案      |

  注意，檔案存在本身就是一個硬連接，計數值為1，當計數值為0才會真正刪除檔案

- 範例
  ```shell
  $ ln main.log hard.log
  $ ln -s 123.log soft.log

  394355 -rw-rw-r-- 2 user user 0 Nov 29 13:39 hard.log   # <---- 硬連結，inode 與 原始檔案同
  394355 -rw-rw-r-- 2 user user 0 Nov 29 13:39 main.log   # <---- 原始檔案
  394354 lrwxrwxrwx 1 user user 7 Nov 29 13:53 soft.log -> 123.log    # <---- 軟連結，inode 與 原始檔案不同
  ```

## 更換軟體源

- 備份原始的軟體源列表，`$ sudo sed -i 's/archive.ubuntu.com/free.nchc.org.tw/g' /etc/apt/sources.list`

- 軟體源格式
  - 格式1:  deb 軟體源地址 發布版本 軟件包所在的子目錄1 子目錄2 子目錄3 ...
    - 例如: `deb http://us.archive.ubuntu.com/ubuntu/ jammy main restricted universe multiverse`
      - 參數1，`deb`，Debian 的軟體包源 
      - 參數2，`http://us.archive.ubuntu.com/ubuntu/`，從該地址下載
      - 參數3，`bionic`，代表下載bionic版本的軟體包
        - bionic 版本: 這個部分的軟體包在發布版本中已經穩定了
        - bionic-updates 版本:  為了解決已知的問題或者提升安全性而更新的軟體包
        - bionic-backports 版本: 新版本的軟體包，在發布版本中還冇有穩定，但是已經經過測試，可以用於增加新功能
        - bionic-backports 版本: 為了解決安全漏洞而發布的

      - 參數4，`main、restricted、universe、multiverse` 四個目錄
        - main目錄: 由 Ubuntu 開發團隊和社區成員直接維護的軟件目錄
        - restricted目錄: 受限製的軟體包，這些軟體包擁有著作權保護或者某些特殊限製，不能隨意的修改或分發。
        - universe目錄: 由社區成員維護的，質量和穩定性比較高
        - multiverse目錄: 主要由社區成員維護, 但是存在一些限製或者著作權問題，不能被自由的分發和使用

      - 實際軟體列表和軟體，會放在 http://us.archive.ubuntu.com/ubuntu/dists/bionic/ 的路徑下

  - 格式2:  deb-src 軟體源地址 發布版本 軟件包所在的子目錄1 子目錄2 子目錄3 ...
    - 用於下載軟件源碼
    - 在 sources.list 中預設是被註解的(預設是失效的)，若要下載源碼，取消註解
    - 透過 apt source 軟件名 來下載源碼

- 軟體源
  - [台灣軟體源](https://wiki.ubuntu-tw.org/index.php?title=%E5%A5%97%E4%BB%B6%E5%BA%AB%E4%BE%86%E6%BA%90)
  - [推薦，NCHC](https://launchpad.net/ubuntu/+mirror/free.nchc.org.tw-archive)

- 範例，快速替換
  語法，`$ sudo sed -i 's/舊網址/新網址/g' /etc/apt/sources.list`
  例如，`$ sudo sed -i 's/archive.ubuntu.com/free.nchc.org.tw/g' /etc/apt/sources.list`

## 安裝中文版的 man 
- `$ sudo apt install manpages-zh`
- `$ sudo nano vi ~/.bashrc`，添加以下內容
  ```shell
  # 將中文的man命令命名為cman命令
  alias cman='man -M /usr/share/man/zh_CN' 
  ```
- 透過 `$ cman 命令`，查詢中文版的man手冊

  注意，不是所有的命令都支援中文版的man，若沒有中文版時，改用 man 查詢英文版

## Ref
- [40個常用的linux命令](https://kinsta.com/blog/linux-commands/)
- [認識shell](https://www.cyut.edu.tw/~ywfan/1109linux/201109chapter11shell%20script.htm)
- [Linux 最全命令詳解](https://www.readfog.com/a/1632458490925649920)