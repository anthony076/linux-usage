## fish script 的使用

- 設置永久變數，`set -x 變數名 變數值`

- 移除永久變數，`set -e 變數名`，其中，-e == --erase

- 使 fish 兼容 bash 語法的方法

  以 bash 的 unset 為例

  - 方法，透過函數
    ```
    function unset
      set --erase $argv
    end

    funcsave unset
    ```
  
  - 方法，透過 alias 命令，`$ alias unset 'set --erase'`

  - 方法，透過 abbr 命令，`$ abbr --add unset 'set --erase'`
